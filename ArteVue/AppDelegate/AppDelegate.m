//
//  AppDelegate.m
//  Artegram
//
//  Created by Tanvir Palash on 6/5/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

@import GooglePlaces;
#import "Firebase.h"
#import "Flurry.h"

#import "AppDelegate.h"
#import "HNKGooglePlacesAutocompleteQuery.h"
#import "PersonalGalleryViewController.h"

#import "Constants.h"
#import "UserAccount.h"
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTCall.h>
@interface AppDelegate ()
@property (nonatomic, strong) CTCallCenter* callCenter;
@end

@implementation AppDelegate

static NSString *const kHNKDemoGooglePlacesAutocompleteApiKey = google_app_key;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    //[NSUserDefaults saveIncomingAvatarSetting:YES];
    //[NSUserDefaults saveOutgoingAvatarSetting:YES];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isInGallery"];
   // [Flurry setDebugLogEnabled:YES];

    [Flurry startSession:flurry_app_id];

    [GMSPlacesClient provideAPIKey:google_app_key];
    
    NSSetUncaughtExceptionHandler(&myExceptionHandler) ;
    
    [HNKGooglePlacesAutocompleteQuery setupSharedQueryWithAPIKey: kHNKDemoGooglePlacesAutocompleteApiKey];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    
    [FIRApp configure];
    
    
    //NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    //NSLog(@" refreshedToken %@",refreshedToken);
    
    NSString *bundledPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"PostPathImages"];
    [[SDImageCache sharedImageCache] addReadOnlyCachePath:bundledPath];
    [[SDImageCache sharedImageCache] setShouldDecompressImages:NO];
  //  [[SDImageCache sharedImageCache] setShouldCacheImagesInMemory:NO];
   // [[SDImageCache sharedImageCache] setMaxCacheSize:1024*1024*250];
   // [[SDImageCache sharedImageCache] setMaxMemoryCountLimit:400];
    
    [[SDWebImageDownloader sharedDownloader] setShouldDecompressImages:NO];
    //[[SDWebImageDownloader sharedDownloader] setMaxConcurrentDownloads:1];
    
    
    self.callCenter = [[CTCallCenter alloc] init];
    [self handleCall];
    
    return YES;
}

-(void) askForNotificationPermission
{
    UIUserNotificationType allNotificationTypes =
    (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
    UIUserNotificationSettings *settings =
    [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
    
    
    
}

-(void)handleCall
{
    self.callCenter.callEventHandler = ^(CTCall *call){
        
        if ([call.callState isEqualToString: CTCallStateConnected])
        {
            NSLog(@"call played");
             [[NSNotificationCenter defaultCenter] postNotificationName:@"updateCentreButton" object:nil];
        }
        else if ([call.callState isEqualToString: CTCallStateDialing])
        {
        }
        else if ([call.callState isEqualToString: CTCallStateDisconnected])
        {
            NSLog(@"call stopped");
             [[NSNotificationCenter defaultCenter] postNotificationName:@"resetCentreButton" object:nil];
        }
        else if ([call.callState isEqualToString: CTCallStateIncoming])
        {
            NSLog(@"call incoming");
        }
    };
}

- (void)tokenRefreshNotification:(NSNotification *)notification {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", refreshedToken);
    
    
    [UserAccount sharedManager].gcmRegKey=refreshedToken;
    
    
    // Connect to FCM since connection may have failed when attempted before having a token.
    [self connectToFcm];
    
    // TODO: If necessary send token to application server.
}

- (void)connectToFcm {
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
        }
    }];
}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken");
    
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken
                                            type:FIRInstanceIDAPNSTokenTypeUnknown];
    
   
    NSString *deviceTokenString = [[NSString stringWithFormat:@"%@",deviceToken] stringByReplacingOccurrencesOfString:@" " withString:@""];
    deviceTokenString = [deviceTokenString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    
    [UserAccount sharedManager].deviceToken=deviceTokenString;
    
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    
    NSLog(@"deviceToken %@",deviceTokenString);
    NSLog(@"refreshedToken %@",refreshedToken);
    
    [UserAccount sharedManager].gcmRegKey=refreshedToken;
    
    
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error{
   
    NSLog(@"Registration for remote notification failed with error: %@", error.localizedDescription);
    // [END receive_apns_token_error]
    NSDictionary *userInfo = @{@"error" :error.localizedDescription};
    
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@"userInfo %@",userInfo);
    
//    UIApplicationState state = [application applicationState];
//    if (state == UIApplicationStateActive) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:userInfo[@"aps"][@"alert"][@"title"]
//                                                        message:userInfo[@"aps"][@"alert"][@"body"]
//                                                       delegate:self cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
//        [alert show];
//    }

    completionHandler(UIBackgroundFetchResultNoData);
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [[FIRMessaging messaging] disconnect];
    NSLog(@"Disconnected from FCM");
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     [FBSDKAppEvents activateApp];
    NSLog(@"applicationDidBecomeActive");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateNotification" object:nil];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];
    // Add any custom logic here.
    return handled;
}

void myExceptionHandler(NSException *exception)
{
    NSArray *stack = [exception callStackReturnAddresses];
    NSLog(@"Stack trace: %@", stack);
}

-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{

//    if ([self.window.rootViewController isKindOfClass:[UITabBarController class]]) {
//        UITabBarController* tabBarController = (UITabBarController*)self.window.rootViewController;
//        NSLog(@"lastViewController tab%@ ",tabBarController.selectedViewController);
//         return UIInterfaceOrientationMaskPortrait;
//        
//    } else if ([self.window.rootViewController isKindOfClass:[UINavigationController class]]) {
//        UINavigationController* navigationController = (UINavigationController*)self.window.rootViewController;
//         NSLog(@"lastViewController %@ nav",(UITabBarController*)navigationController.visibleViewController);
//      return UIInterfaceOrientationMaskPortrait;
//       
//    } else if (self.window.rootViewController.presentedViewController) {
//        UIViewController* presentedViewController = self.window.rootViewController.presentedViewController;
//         NSLog(@"lastViewController pres%@ ",presentedViewController);
//         return UIInterfaceOrientationMaskPortrait;
//       
//    } else {
//        NSLog(@"lastViewController %@ ",self.window.rootViewController);
//    }
    
        if (self.shouldRotate)
            return UIInterfaceOrientationMaskAll;
        else
            return UIInterfaceOrientationMaskPortrait;
    

//      if ([self.window.rootViewController.presentedViewController isKindOfClass:[PersonalGalleryViewController class]])
//    {
//        return UIInterfaceOrientationMaskAll;
//        
////        SecondViewController *secondController = (SecondViewController *) self.window.rootViewController.presentedViewController;
////        
////        if (secondController.isPresented) // Check current controller state
////        {
////            return UIInterfaceOrientationMaskAll;
////        }
////        else return UIInterfaceOrientationMaskPortrait;
//    }
//    else
//        return UIInterfaceOrientationMaskPortrait;
}

-(void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
    NSLog(@"applicationDidReceiveMemoryWarning");
    [[SDImageCache sharedImageCache] clearMemory];
}

@end
