//
//  AppDelegate.h
//  Artegram
//
//  Created by Tanvir Palash on 6/5/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import  <FBSDKCoreKit/FBSDKCoreKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (assign, nonatomic) BOOL shouldRotate;
-(void) askForNotificationPermission;
@end

