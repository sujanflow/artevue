//
//  ServerManager.h
//  ArteVue
//
//  Created by Tanvir Palash on 1/4/17.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface ServerManager : NSObject

@property (nonatomic, readwrite) BOOL isNetworkAvailable;

+ (ServerManager *)sharedManager;

- (BOOL)checkForNetworkAvailability;

typedef void (^api_Completion_Handler_Status)(BOOL success);
typedef void (^api_Completion_Handler_Data)(BOOL success, NSMutableDictionary *resultDataArray);
typedef void (^api_Completion_Handler_Status_String)(BOOL success, NSString* resultString);



//User SignUp/Login
-(void) postSignUpWithUser:(NSString*)userName password:(NSString*)userPass email:(NSString*)email fullName:(NSString*)name userType:(NSString*)type completion:(api_Completion_Handler_Status)completion;
-(void) postLoginUser:(NSString*)userName password:(NSString*)userPass completion:(api_Completion_Handler_Status)completion;
-(void) postProfilePicture:(UIImage*)image completion:(api_Completion_Handler_Status)completion;

//Facebook signup
-(void) postSignUpForFBUser:(NSString*)userName socialId:(NSString*)social_id email:(NSString*)email withFirstName:(NSString*)fname lastname:(NSString*)lname withUserType:(NSString*)type completion:(api_Completion_Handler_Status)completion;
-(void) postFacebookLoginUsingEmail:(NSString*)email completion:(api_Completion_Handler_Status)completion;



//validation
-(void) checkUserName:(NSString*)name WithCompletion:(api_Completion_Handler_Status)completion;
-(void) checkEmail:(NSString*)email WithCompletion:(api_Completion_Handler_Status)completion;

//Update user data
-(void) updateSettingsWithCompletion:(api_Completion_Handler_Status)completion;
-(void) updateGalleryInfo:(NSString*)details withName:(NSString*)name completion:(api_Completion_Handler_Status)completion;

//Get user data
-(void) getCurrentUserDetailsWithCompletion:(api_Completion_Handler_Data)completion;
-(void) getUserDetailsForId:(int)userId WithCompletion:(api_Completion_Handler_Data)completion;

-(void) getAllUserTypeWithCompletion:(api_Completion_Handler_Data)completion;
-(void) getAllArtistWithCompletion:(api_Completion_Handler_Data)completion;

-(void) getAllArtPreference:(api_Completion_Handler_Data)completion;
-(void) getAllArtTypes:(api_Completion_Handler_Data)completion;



//Get Post in Profile

-(void) getCurrentUserPostwithPage:(int)pageIndex withCompletion:(api_Completion_Handler_Data)completion;
-(void) getUserPostForId:(int)userId inPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion;
-(void) getTaggedPostForId:(int)userId WithCompletion:(api_Completion_Handler_Data)completion;
-(void) getPostByPostId: (int)postId withCompletion:(api_Completion_Handler_Data)completion;
-(void) getPinnedPostForId: (int)userId inPage:(int)pageNum withCompletion:(api_Completion_Handler_Data)completion;



//create post
-(void) postNewImage:(UIImage*)image withDetails:(NSDictionary*)data withcompletion:(api_Completion_Handler_Status)completion;
-(void) updatePostForId:(int)postId withDetails:(NSDictionary*)data withcompletion:(api_Completion_Handler_Status)completion;

-(void) deletePostForPostId:(int)postId withcompletion:(api_Completion_Handler_Status)completion;

//Get Feed
-(void) getTopImageInFeedWithCompletion:(api_Completion_Handler_Data)completion;
-(void) getCurrentUserFeedForPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion;

// Comments
-(void) getCommentListForPost:(int)postId inPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion;
-(void) postCommentInPostId:(int)postId withComment:(NSString*)commentString withcompletion:(api_Completion_Handler_Status)completion;
-(void) deleteCommentForPostId:(int)commentId withcompletion:(api_Completion_Handler_Status)completion;

// Like
-(void) getLikeListForPost:(int)postId inPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion;
-(void) postLikePostId:(int)postId withcompletion:(api_Completion_Handler_Status)completion;
-(void) deleteLikeForPostId:(int)postId withcompletion:(api_Completion_Handler_Status)completion;

//Following/Followers
-(void) postFollowUserId:(int)userId WithCompletion:(api_Completion_Handler_Status)completion;
-(void) getFollowersListOfUser:(int)userId InPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion;
-(void) getFollowingListOfUser:(int)userId WithCompletion:(api_Completion_Handler_Data)completion;

//Pin Post
-(void) postPinForPostId:(int)postId withcompletion:(api_Completion_Handler_Status)completion;
-(void) deletePinForPostId:(int)postId withcompletion:(api_Completion_Handler_Status)completion;

//Events/News
-(void) getEventListForPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion;
-(void) getNewsListForPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion;

//Events/News
-(void) getDiscoverPostForPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion;
-(void) getDiscoverUsersForPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion;

//Details by Name
-(void) getUserByUserName:(NSString*) name withCompletion:(api_Completion_Handler_Data)completion;
-(void) getArtistPostByName:(NSString*) name withCompletion:(api_Completion_Handler_Data)completion;
-(void) getTopHashtagByName:(NSString*) name withCompletion:(api_Completion_Handler_Data)completion;
-(void) getLatestHashtagByName:(NSString*) name withCompletion:(api_Completion_Handler_Data)completion;

//Search
-(void) searchUserByString:(NSString*)searchString forPage:(int)pageIndex withcompletion:(api_Completion_Handler_Data)completion;
-(void) searchHashtagByString:(NSString*)searchString withcompletion:(api_Completion_Handler_Data)completion;
-(void) searchArtistByString:(NSString*)searchString withcompletion:(api_Completion_Handler_Data)completion;
-(void) searchAdvancedWithData:(NSDictionary*)data forPage:(int)pageIndex withcompletion:(api_Completion_Handler_Data)completion;

//Report/Block
-(void) postReportAgainstUserId:(int)userId WithCompletion:(api_Completion_Handler_Status)completion;
-(void) postBlockUserId:(int)userId WithCompletion:(api_Completion_Handler_Status)completion;

//Messaging

-(void) getMessageWithUser:(NSString*)userId inPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion;
-(void) getMessageHistoryforPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion;
-(void) postSingleMessageWithData:(NSMutableDictionary*)dataDic withCompletion:(api_Completion_Handler_Status)completion;
-(void) postSingleMessageWithImage:(UIImage*)image AndData:(NSMutableDictionary*)dataDic withCompletion:(api_Completion_Handler_Status)completion;
-(void) deleteMessageForId:(int)userId withCompletion:(api_Completion_Handler_Status)completion;

//gallery
-(void) getGalleryOfUser:(int)userId inPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion;
-(void) postGallerySequenceWithDetails:(NSDictionary*)data withCompletion:(api_Completion_Handler_Status)completion;
-(void) postCreatePDfWithDetails:(NSDictionary*)data withCompletion:(api_Completion_Handler_Status)completion;
-(void) updatePost:(int)postId Locked:(BOOL)isLocked withCompletion:(api_Completion_Handler_Status)completion;
-(void) updatePost:(int)postId withGalleryItem:(BOOL)isGallery withCompletion:(api_Completion_Handler_Status)completion;



//Activities
-(void) getFollowersActivitiesinPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion;
-(void) getOwnActivitiesinPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion;

//EditUser
-(void) updateUserDetailsWithData:(NSDictionary*)dataDic withCompletion:(api_Completion_Handler_Status)completion;

//Password
-(void) updatePasswordForOld:(NSString*)oldPass withNew:(NSString*)newPass withCompletion:(api_Completion_Handler_Status)completion;
-(void) getForgetPasswordForEmail:(NSString*)email withCompletion:(api_Completion_Handler_Data)completion;


@end
