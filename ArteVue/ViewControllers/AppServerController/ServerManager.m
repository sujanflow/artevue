//
//  ServerManager.m
//  ArteVue
//
//  Created by Tanvir Palash on 1/4/17.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "ServerManager.h"
#import "UserAccount.h"
#import "Constants.h"
#import "SynthesizeSingleton.h"
#import "AFNetworking.h"

#import "NSDictionary+NullReplacement.h"
#import "NSArray+NullReplacement.h"

#pragma mark - interface
@interface ServerManager(){
    Reachability *networkReachability;
}

DECLARE_SINGLETON_FOR_CLASS(ServerManager)

@end

#pragma mark - imlementation
@implementation ServerManager

SYNTHESIZE_SINGLETON_FOR_CLASS(ServerManager)

@synthesize isNetworkAvailable;

#pragma mark - init
- (id)init{
    if (self = [super init]){
    }
    return self;
}




/* ***** API ***** */

#pragma mark -  API - FUNCTIONS


- (void)postSignUpWithUser:(NSString*)userName password:(NSString*)userPass email:(NSString*)email fullName:(NSString*)name userType:(NSString*)type completion:(api_Completion_Handler_Status)completion{
    
    if ([self checkForNetworkAvailability]) {
        
        NSMutableDictionary *parameterDic = [[NSMutableDictionary alloc] init];
        
        [parameterDic setObject:name forKey:@"name"];
        [parameterDic setObject:userName forKey:@"username"];
        [parameterDic setObject:userPass forKey:@"password"];
        [parameterDic setObject:email forKey:@"email"];
        [parameterDic setObject:type forKey:@"user_type_id"];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            if([UserAccount sharedManager].accessToken.length<=0)
                [UserAccount sharedManager].accessToken=devAccessToken;
            
            [self postServerRequestWithParams:parameterDic forUrl:[NSString stringWithFormat:@"%@/api/user",ARTEVUE_SERVER_BASE_API_URL] withResponseCallback:^(NSDictionary *responseDictionary) {
                [UserAccount sharedManager].accessToken=@"";
                
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
                
            }];
            
            
        });
    }else{
        [self showAlertForNoInternet];
    }
    
}

-(void) postSignUpForFBUser:(NSString*)userName socialId:(NSString*)social_id email:(NSString*)email withFirstName:(NSString*)fname lastname:(NSString*)lname withUserType:(NSString*)type completion:(api_Completion_Handler_Status)completion
{
    if ([self checkForNetworkAvailability]) {
        
        NSMutableDictionary *parameterDic = [[NSMutableDictionary alloc] init];
        
      
        [parameterDic setObject:userName forKey:@"username"];
        [parameterDic setObject:fname forKey:@"first_name"];
        [parameterDic setObject:lname forKey:@"last_name"];
        [parameterDic setObject:social_id forKey:@"social_id"];
        [parameterDic setObject:email forKey:@"email"];
        [parameterDic setObject:type forKey:@"user_type_id"];
        
        //NSLog(@"parameterDic %@",parameterDic);
        
        if([UserAccount sharedManager].accessToken.length<=0)
            [UserAccount sharedManager].accessToken=devAccessToken;
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            
            
            
            [self postServerRequestWithParams:parameterDic forUrl:[NSString stringWithFormat:@"%@/api/facebook-signup",ARTEVUE_SERVER_BASE_API_URL] withResponseCallback:^(NSDictionary *responseDictionary) {
                
                [UserAccount sharedManager].accessToken=@"";
                //NSLog(@"responseDictionary %@",responseDictionary);
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [UserAccount sharedManager].accessToken = [responseDictionary objectForKey:@"access_token"];
                        
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
                
            }];
            
            
        });
    }else{
        [self showAlertForNoInternet];
    }

}
-(void) postFacebookLoginUsingEmail:(NSString*)email completion:(api_Completion_Handler_Status)completion
{

    if ([self checkForNetworkAvailability]) {
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        if([UserAccount sharedManager].accessToken.length<=0)
            [UserAccount sharedManager].accessToken=devAccessToken;
        
        dispatch_async(backgroundQueue, ^{
            
            
            [self getServerRequestForUrl:[NSString stringWithFormat:@"%@/api/facebook-login?email=%@",ARTEVUE_SERVER_BASE_API_URL,email] withResponseCallback:^(NSDictionary *responseDictionary) {
                [UserAccount sharedManager].accessToken=@"";
                
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [UserAccount sharedManager].accessToken = [responseDictionary objectForKey:@"access_token"];
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
                
            }];
            
            
        });
    }else{
        [self showAlertForNoInternet];
    }

}


- (void)postLoginUser:(NSString*)userName password:(NSString*)userPass completion:(api_Completion_Handler_Status)completion{
    
    if ([self checkForNetworkAvailability]) {
        
        NSMutableDictionary *parameterDic = [[NSMutableDictionary alloc] init];
        
        [parameterDic setObject:clientIdForApi forKey:@"client_id"];
        [parameterDic setObject:clientSecretForApi forKey:@"client_secret"];
        [parameterDic setObject:userName forKey:@"username"];
        [parameterDic setObject:userPass forKey:@"password"];
        [parameterDic setObject:scopeValue forKey:@"scope"];
        [parameterDic setObject:grantType forKey:@"grant_type"];
       
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
      
        dispatch_async(backgroundQueue, ^{
            
            [self postServerRequestWithParams:parameterDic forUrl:[NSString stringWithFormat:@"%@/oauth/token",ARTEVUE_SERVER_BASE_API_URL] withResponseCallback:^(NSDictionary *responseDictionary) {
                //[self validateResponseData:responseDictionary] &&
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [UserAccount sharedManager].accessToken = [responseDictionary objectForKey:@"access_token"];
                        [UserAccount sharedManager].refreshToken = [responseDictionary objectForKey:@"refresh_token"];
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }

            }];
            
            
        });
    }else{
        [self showAlertForNoInternet];
    }
    
}

- (void)postProfilePicture:(UIImage*)image completion:(api_Completion_Handler_Status)completion{
    
    if ([self checkForNetworkAvailability]) {
        
        
        NSString *urlString=[NSString stringWithFormat:@"%@/api/update-profile-picture",ARTEVUE_SERVER_BASE_API_URL];
        
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            NSData *imageData = [self compressImageForPP:image];
            
           [self postServerRequestForImage:imageData WithParams:nil forUrl:urlString keyValue:@"profile_picture" withResponseCallback:^(NSDictionary *responseDictionary) {
         
               if ( responseDictionary!=nil) {
                   //Valid Data From Server
                   
                   dispatch_async(dispatch_get_main_queue(), ^{
                     completion(TRUE);
                   });
                   
               }else{
                   
                   dispatch_async(dispatch_get_main_queue(), ^{
                       completion(FALSE);
                   });
                   
               }
               
           }];
            
            
        });
    }else{
        [self showAlertForNoInternet];
    }
    
}
-(void) postNewImage:(UIImage*)image withDetails:(NSDictionary*)data withcompletion:(api_Completion_Handler_Status)completion;
{
    if ([self checkForNetworkAvailability]) {
        
        
        NSString *urlString=[NSString stringWithFormat:@"%@/api/post",ARTEVUE_SERVER_BASE_API_URL];
        
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            
            NSData *imageData = [self compressImageForPost:image];
            
            [self postServerRequestForImage:imageData WithParams:data forUrl:urlString keyValue:@"post_image" withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
                
            }];
            
            
        });
    }else{
        [self showAlertForNoInternet];
    }

}

-(void) updatePostForId:(int)postId withDetails:(NSDictionary*)data withcompletion:(api_Completion_Handler_Status)completion
{
    if ([self checkForNetworkAvailability]) {
        
        NSString *urlString=[NSString stringWithFormat:@"%@/api/post/%d",ARTEVUE_SERVER_BASE_API_URL,postId];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            
            [self putServerRequestWithParams:data forUrl:urlString withResponseCallback:^(NSDictionary *responseDictionary) {
               // NSLog(@"responseDictionary %@",responseDictionary);
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                         completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }

            }];
            
            
        });
    }else{
        [self showAlertForNoInternet];
    }

}

-(void) deletePostForPostId:(int)postId withcompletion:(api_Completion_Handler_Status)completion
{
    if ([self checkForNetworkAvailability])
    {
        NSString *urlString=[NSString stringWithFormat:@"%@/api/post/%d",ARTEVUE_SERVER_BASE_API_URL,postId];
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            [self deleteServerRequestForUrl:urlString withResponseCallback:^(NSDictionary *responseDictionary) {
                //[self validateResponseData:responseDictionary] &&
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
                
            }];
            
            
        });
        
    }else{
        [self showAlertForNoInternet];
    }
}

-(void)getCurrentUserDetailsWithCompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/current-user",ARTEVUE_SERVER_BASE_API_URL];
        //NSString *httpUrl=[NSString stringWithFormat:@"%@/api/user/%i",ARTEVUE_SERVER_BASE_API_URL,userId];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[[responseDictionary mutableCopy] objectForKey:@"data"]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }

    
}

- (void)getUserDetailsForId:(int)userId WithCompletion:(api_Completion_Handler_Data)completion{
    
    if ([self checkForNetworkAvailability]) {
       
      //  NSString *httpUrl=[NSString stringWithFormat:@"%@/api/current-user",ARTEVUE_SERVER_BASE_API_URL];
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/user/%i",ARTEVUE_SERVER_BASE_API_URL,userId];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                    
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[[responseDictionary mutableCopy] objectForKey:@"data"]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }
}

-(void) getUserByUserName:(NSString*) name withCompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        //  NSString *httpUrl=[NSString stringWithFormat:@"%@/api/current-user",ARTEVUE_SERVER_BASE_API_URL];
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/user-by-username?username=%@",ARTEVUE_SERVER_BASE_API_URL,name];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[[responseDictionary mutableCopy] objectForKey:@"data"]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }

}
-(void) getArtistPostByName:(NSString*) name withCompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        //  NSString *httpUrl=[NSString stringWithFormat:@"%@/api/current-user",ARTEVUE_SERVER_BASE_API_URL];
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/artist-by-name?name=%@",ARTEVUE_SERVER_BASE_API_URL,name];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[[responseDictionary mutableCopy] objectForKey:@"data"]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }

}
-(void) getTopHashtagByName:(NSString*) name withCompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/hashtag/top-posts/%@",ARTEVUE_SERVER_BASE_API_URL,name];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }

}

-(void) getLatestHashtagByName:(NSString*) name withCompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/hashtag/latest-posts/%@",ARTEVUE_SERVER_BASE_API_URL,name];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }

}

-(void) searchUserByString:(NSString*)searchString forPage:(int)pageIndex withcompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/search-user?search_string=%@&limit=20",ARTEVUE_SERVER_BASE_API_URL,searchString];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }

}
-(void) searchHashtagByString:(NSString*)searchString withcompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/search-hashtag?search_string=%@",ARTEVUE_SERVER_BASE_API_URL,searchString];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }
}

-(void) searchArtistByString:(NSString*)searchString withcompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/search-artist?search_string=%@",ARTEVUE_SERVER_BASE_API_URL,searchString];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }
}

-(void) searchAdvancedWithData:(NSDictionary*)data forPage:(int)pageIndex withcompletion:(api_Completion_Handler_Data)completion
{

    if ([self checkForNetworkAvailability]) {
        
        
        NSString *urlString=[NSString stringWithFormat:@"%@/api/advance-search?artist=%@&keyword=%@&date_range=%@",ARTEVUE_SERVER_BASE_API_URL,[data objectForKey:@"artist"],[data objectForKey:@"keyword"],[data objectForKey:@"date_range"]];
        
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:urlString withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }

}

-(void) getCurrentUserPostwithPage:(int)pageIndex withCompletion:(api_Completion_Handler_Data)completion
{
    
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/post",ARTEVUE_SERVER_BASE_API_URL];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }

}

-(void) getUserPostForId:(int)userId inPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        //  NSString *httpUrl=[NSString stringWithFormat:@"%@/api/current-user",ARTEVUE_SERVER_BASE_API_URL];
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/post/%i",ARTEVUE_SERVER_BASE_API_URL,userId];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }

}

-(void) getTaggedPostForId:(int)userId WithCompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        //  NSString *httpUrl=[NSString stringWithFormat:@"%@/api/current-user",ARTEVUE_SERVER_BASE_API_URL];
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/post/tagged/%i",ARTEVUE_SERVER_BASE_API_URL,userId];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }

}

-(void) getGalleryOfUser:(int)userId inPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/gallery/%i?page=%d",ARTEVUE_SERVER_BASE_API_URL,userId,pageIndex];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }
    
}

-(void) postGallerySequenceWithDetails:(NSDictionary*)data withCompletion:(api_Completion_Handler_Status)completion
{
    if ([self checkForNetworkAvailability])
    {
        NSString *urlString=[NSString stringWithFormat:@"%@/api/arrange-gallery",ARTEVUE_SERVER_BASE_API_URL];
        
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            
            [self postServerRequestWithParams:data forUrl:urlString withResponseCallback:^(NSDictionary *responseDictionary) {
                //[self validateResponseData:responseDictionary] &&
                NSLog(@"responseDictionary %@",responseDictionary);
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
                
            }];
            
            
        });
        
    }else{
        [self showAlertForNoInternet];
    }

}

-(void) postCreatePDfWithDetails:(NSDictionary*)data withCompletion:(api_Completion_Handler_Status)completion
{
    if ([self checkForNetworkAvailability])
    {
        NSString *urlString=[NSString stringWithFormat:@"%@/api/email-gallery-pdf",ARTEVUE_SERVER_BASE_API_URL];
        
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            
            [self postServerRequestWithParams:data forUrl:urlString withResponseCallback:^(NSDictionary *responseDictionary) {
                //[self validateResponseData:responseDictionary] &&
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
                
            }];
            
            
        });
        
    }else{
        [self showAlertForNoInternet];
    }

}

-(void) updatePost:(int)postId withGalleryItem:(BOOL)isGallery withCompletion:(api_Completion_Handler_Status)completion
{
    if ([self checkForNetworkAvailability]) {
        
        
        NSString *urlString=[NSString stringWithFormat:@"%@/api/post/%i",ARTEVUE_SERVER_BASE_API_URL,postId];
        NSMutableDictionary *parameterDic = [[NSMutableDictionary alloc] init];
        
        [parameterDic setObject:[NSNumber numberWithBool:isGallery] forKey:@"is_gallery_item"];
        
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            
            [self patchServerRequestWithParams:parameterDic forUrl:urlString withResponseCallback:^(NSDictionary *responseDictionary) {
                
               // NSLog(@"responseDictionary %@",responseDictionary);
                
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
            }];
            
            
        });
    }else{
        [self showAlertForNoInternet];
    }

}
-(void) updatePost:(int)postId Locked:(BOOL)isLocked withCompletion:(api_Completion_Handler_Status)completion
{
    if ([self checkForNetworkAvailability]) {
        
        
        NSString *urlString=[NSString stringWithFormat:@"%@/api/post/%i",ARTEVUE_SERVER_BASE_API_URL,postId];
        NSMutableDictionary *parameterDic = [[NSMutableDictionary alloc] init];
        
        [parameterDic setObject:[NSNumber numberWithBool:isLocked] forKey:@"is_locked"];
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            
            [self patchServerRequestWithParams:parameterDic forUrl:urlString withResponseCallback:^(NSDictionary *responseDictionary) {
                
                //NSLog(@"responseDictionary %@",responseDictionary);
                
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
            }];
            
            
        });
    }else{
        [self showAlertForNoInternet];
    }
    
}
-(void) updatePasswordForOld:(NSString*)oldPass withNew:(NSString*)newPass withCompletion:(api_Completion_Handler_Status)completion
{
    if ([self checkForNetworkAvailability]) {
        
        
        NSString *urlString=[NSString stringWithFormat:@"%@/api/password",ARTEVUE_SERVER_BASE_API_URL];
        NSMutableDictionary *parameterDic = [[NSMutableDictionary alloc] init];
        
        [parameterDic setObject:oldPass forKey:@"old_password"];
        [parameterDic setObject:newPass forKey:@"new_password"];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            
            [self patchServerRequestWithParams:parameterDic forUrl:urlString withResponseCallback:^(NSDictionary *responseDictionary) {
                
                //NSLog(@"responseDictionary %@",responseDictionary);
                
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
            }];
            
            
        });
    }else{
        [self showAlertForNoInternet];
    }


}
-(void) getForgetPasswordForEmail:(NSString*)email withCompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/forgot-password?email=%@",ARTEVUE_SERVER_BASE_API_URL,email];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            if([UserAccount sharedManager].accessToken.length<=0)
                [UserAccount sharedManager].accessToken=devAccessToken;
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                [UserAccount sharedManager].accessToken=@"";
                
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }
}

-(void) updateUserDetailsWithData:(NSDictionary*)dataDic withCompletion:(api_Completion_Handler_Status)completion
{
    if ([self checkForNetworkAvailability]) {
        
        NSString *urlString=[NSString stringWithFormat:@"%@/api/user",ARTEVUE_SERVER_BASE_API_URL];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            
            [self putServerRequestWithParams:dataDic forUrl:urlString withResponseCallback:^(NSDictionary *responseDictionary) {
             
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
                
            }];
            
            
        });
    }else{
        [self showAlertForNoInternet];
    }

}

-(void) getFollowersActivitiesinPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/follower-activities?page=%d",ARTEVUE_SERVER_BASE_API_URL,pageIndex];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }
}
-(void) getOwnActivitiesinPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/user-activities?page=%d",ARTEVUE_SERVER_BASE_API_URL,pageIndex];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }
}


-(void) getPostByPostId: (int)postId withCompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        //  NSString *httpUrl=[NSString stringWithFormat:@"%@/api/current-user",ARTEVUE_SERVER_BASE_API_URL];
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/post/detail/%i",ARTEVUE_SERVER_BASE_API_URL,postId];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }

}

-(void) getPinnedPostForId: (int)userId inPage:(int)pageNum  withCompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        //  NSString *httpUrl=[NSString stringWithFormat:@"%@/api/current-user",ARTEVUE_SERVER_BASE_API_URL];
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/pin/posts/%i?page=%i",ARTEVUE_SERVER_BASE_API_URL,userId,pageNum];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }

}

-(void) getNewsListForPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion
{
    
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/news?page=%i",ARTEVUE_SERVER_BASE_API_URL,pageIndex];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }
    
}

-(void) getEventListForPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion
{
    
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/events?page=%i",ARTEVUE_SERVER_BASE_API_URL,pageIndex];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }
    
}

-(void) getDiscoverPostForPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/discover-posts?page=%i",ARTEVUE_SERVER_BASE_API_URL,pageIndex];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }

}
-(void) getDiscoverUsersForPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/discover-users?page=%i",ARTEVUE_SERVER_BASE_API_URL,pageIndex];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }

}



-(void) getTopImageInFeedWithCompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/feed-top-bar",ARTEVUE_SERVER_BASE_API_URL];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }

}

-(void) getCurrentUserFeedForPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/feed?page=%d",ARTEVUE_SERVER_BASE_API_URL,pageIndex];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }


}

-(void) getLikeListForPost:(int)postId inPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion;
{
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/post/likes/%d?page=%d",ARTEVUE_SERVER_BASE_API_URL,postId,pageIndex];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }

}

-(void) postLikePostId:(int)postId withcompletion:(api_Completion_Handler_Status)completion
{
    if ([self checkForNetworkAvailability])
    {
        NSString *urlString=[NSString stringWithFormat:@"%@/api/like/%d",ARTEVUE_SERVER_BASE_API_URL,postId];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            
            [self postServerRequestWithParams:nil forUrl:urlString withResponseCallback:^(NSDictionary *responseDictionary) {
                //[self validateResponseData:responseDictionary] &&
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
                
            }];
            
            
        });
        
    }else{
        [self showAlertForNoInternet];
    }

}
-(void) deleteLikeForPostId:(int)postId withcompletion:(api_Completion_Handler_Status)completion
{
    if ([self checkForNetworkAvailability])
    {
        NSString *urlString=[NSString stringWithFormat:@"%@/api/like/%d",ARTEVUE_SERVER_BASE_API_URL,postId];
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            [self deleteServerRequestForUrl:urlString withResponseCallback:^(NSDictionary *responseDictionary) {
                //[self validateResponseData:responseDictionary] &&
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
                
            }];
            
            
        });
        
    }else{
        [self showAlertForNoInternet];
    }
    

}

-(void) getCommentListForPost:(int)postId inPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/comment/%d?page=%d",ARTEVUE_SERVER_BASE_API_URL,postId,pageIndex];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }
    
    
}


-(void) postCommentInPostId:(int)postId withComment:(NSString *)commentString withcompletion:(api_Completion_Handler_Status)completion
{
    
    if ([self checkForNetworkAvailability])
    {
        NSString *urlString=[NSString stringWithFormat:@"%@/api/comment/%d",ARTEVUE_SERVER_BASE_API_URL,postId];
        
        NSMutableDictionary *parameterDic = [[NSMutableDictionary alloc] init];
        
        [parameterDic setObject:commentString forKey:@"comment"];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            
            [self postServerRequestWithParams:parameterDic forUrl:urlString withResponseCallback:^(NSDictionary *responseDictionary) {
                //[self validateResponseData:responseDictionary] &&
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
                
            }];
            
            
        });
        
    }else{
        [self showAlertForNoInternet];
    }
}

-(void) deleteCommentForPostId:(int)commentId withcompletion:(api_Completion_Handler_Status)completion
{
    if ([self checkForNetworkAvailability])
    {
        NSString *urlString=[NSString stringWithFormat:@"%@/api/comment/%d",ARTEVUE_SERVER_BASE_API_URL,commentId];
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            [self deleteServerRequestForUrl:urlString withResponseCallback:^(NSDictionary *responseDictionary) {
                //[self validateResponseData:responseDictionary] &&
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
                
            }];
            
            
        });
        
    }else{
        [self showAlertForNoInternet];
    }
    
}

-(void) getMessageWithUser:(NSString*)userId inPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/message/%@?page=%d",ARTEVUE_SERVER_BASE_API_URL,userId,pageIndex];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }
    
    
}

-(void) getMessageHistoryforPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/message-participants?page=%d",ARTEVUE_SERVER_BASE_API_URL,pageIndex];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }
    
}
-(void) postSingleMessageWithData:(NSMutableDictionary*)dataDic withCompletion:(api_Completion_Handler_Status)completion
{
    if ([self checkForNetworkAvailability])
    {
        NSString *urlString=[NSString stringWithFormat:@"%@/api/message",ARTEVUE_SERVER_BASE_API_URL];
        
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            
            [self postServerRequestWithParams:dataDic forUrl:urlString withResponseCallback:^(NSDictionary *responseDictionary) {
                //[self validateResponseData:responseDictionary] &&
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
                
            }];
            
            
        });
        
    }else{
        [self showAlertForNoInternet];
    }
    
}

-(void) postSingleMessageWithImage:(UIImage*)image AndData:(NSMutableDictionary*)dataDic withCompletion:(api_Completion_Handler_Status)completion
{
    if ([self checkForNetworkAvailability])
    {
        NSString *urlString=[NSString stringWithFormat:@"%@/api/message",ARTEVUE_SERVER_BASE_API_URL];
        
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            
            NSData *imageData = [self compressImageForPost:image];
            
            [self postServerRequestForImage:imageData WithParams:dataDic forUrl:urlString keyValue:@"url" withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
                
            }];
            
            
        });
        
    }else{
        [self showAlertForNoInternet];
    }

}


-(void) deleteMessageForId:(int)userId withCompletion:(api_Completion_Handler_Status)completion
{
    if ([self checkForNetworkAvailability])
    {
        NSString *urlString=[NSString stringWithFormat:@"%@/api/conversation/%d",ARTEVUE_SERVER_BASE_API_URL,userId];
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            [self deleteServerRequestForUrl:urlString withResponseCallback:^(NSDictionary *responseDictionary) {
                //[self validateResponseData:responseDictionary] &&
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
                
            }];
            
            
        });
        
    }else{
        [self showAlertForNoInternet];
    }
    
}




-(void) postPinForPostId:(int)postId withcompletion:(api_Completion_Handler_Status)completion
{
    if ([self checkForNetworkAvailability])
    {
        NSString *urlString=[NSString stringWithFormat:@"%@/api/pin/%d",ARTEVUE_SERVER_BASE_API_URL,postId];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            
            [self postServerRequestWithParams:nil forUrl:urlString withResponseCallback:^(NSDictionary *responseDictionary) {
                //[self validateResponseData:responseDictionary] &&
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
                
            }];
            
            
        });
        
    }else{
        [self showAlertForNoInternet];
    }

    
}
-(void) deletePinForPostId:(int)postId withcompletion:(api_Completion_Handler_Status)completion
{
    if ([self checkForNetworkAvailability])
    {
        NSString *urlString=[NSString stringWithFormat:@"%@/api/pin/%d",ARTEVUE_SERVER_BASE_API_URL,postId];
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            [self deleteServerRequestForUrl:urlString withResponseCallback:^(NSDictionary *responseDictionary) {
                //[self validateResponseData:responseDictionary] &&
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
                
            }];
            
            
        });
        
    }else{
        [self showAlertForNoInternet];
    }
}


-(void) getFollowingListOfUser:(int)userId WithCompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/following/%i",ARTEVUE_SERVER_BASE_API_URL,userId];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }
    
    
}

-(void) getFollowersListOfUser:(int)userId InPage:(int)pageIndex WithCompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/followers/%i?page=%d",ARTEVUE_SERVER_BASE_API_URL,userId,pageIndex];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[responseDictionary mutableCopy]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }
    
    
}

-(void) postFollowUserId:(int)userId WithCompletion:(api_Completion_Handler_Status)completion
{
    if ([self checkForNetworkAvailability])
    {
        NSString *urlString=[NSString stringWithFormat:@"%@/api/follower/%d",ARTEVUE_SERVER_BASE_API_URL,userId];
        
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            
            [self postServerRequestWithParams:nil forUrl:urlString withResponseCallback:^(NSDictionary *responseDictionary) {
                //[self validateResponseData:responseDictionary] &&
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
                
            }];
            
        });
        
    }else{
        [self showAlertForNoInternet];
    }

}



- (void)getAllUserTypeWithCompletion:(api_Completion_Handler_Data)completion{
    
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/usertypes",ARTEVUE_SERVER_BASE_API_URL];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
           
            if([UserAccount sharedManager].accessToken.length<=0)
                [UserAccount sharedManager].accessToken=devAccessToken;
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                //[UserAccount sharedManager].accessToken=@"";
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[[responseDictionary mutableCopy] objectForKey:@"data"]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }
}

-(void) getAllArtistWithCompletion:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/artist",ARTEVUE_SERVER_BASE_API_URL];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[[responseDictionary mutableCopy] objectForKey:@"data"]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }


}

-(void) getAllArtPreference:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/art-preferences",ARTEVUE_SERVER_BASE_API_URL];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[[responseDictionary mutableCopy] objectForKey:@"data"]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }

}
-(void) getAllArtTypes:(api_Completion_Handler_Data)completion
{
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/art-types",ARTEVUE_SERVER_BASE_API_URL];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE,[[responseDictionary mutableCopy] objectForKey:@"data"]);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        [self showAlertForNoInternet];
    }

}


- (void)checkUserName:(NSString*)name WithCompletion:(api_Completion_Handler_Status)completion{
    
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/check-username?username=%@",ARTEVUE_SERVER_BASE_API_URL,[name  stringByTrimmingCharactersInSet:
                                                                                                                       [NSCharacterSet whitespaceCharacterSet]]];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            [UserAccount sharedManager].accessToken=devAccessToken;
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                [UserAccount sharedManager].accessToken=@"";
                
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                    if([[responseDictionary objectForKey:@"is_taken"] intValue])
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            completion(TRUE);
                        });
                    }
                    else
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            completion(FALSE);
                        });
                        
                    }
                   
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
                
            }];
            
            
        });
    }else{
        [self showAlertForNoInternet];
    }
    
}

-(void)checkEmail:(NSString*)email WithCompletion:(api_Completion_Handler_Status)completion{
    
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/api/check-email?email=%@",ARTEVUE_SERVER_BASE_API_URL,[email stringByTrimmingCharactersInSet:
                                                                                                                 [NSCharacterSet whitespaceCharacterSet]]];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            [UserAccount sharedManager].accessToken=devAccessToken;
            
            [self getServerRequestForUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                [UserAccount sharedManager].accessToken=@"";
                
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                    if([[responseDictionary objectForKey:@"is_taken"] intValue])
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            completion(TRUE);
                        });
                    }
                    else
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            completion(FALSE);
                        });
                        
                    }
                    
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
            }];
            
        });
    }else{
        [self showAlertForNoInternet];
    }
    
}
- (void)updateGalleryInfo:(NSString*)details withName:(NSString*)name completion:(api_Completion_Handler_Status)completion{
    
    if ([self checkForNetworkAvailability]) {
        
        
        NSString *urlString=[NSString stringWithFormat:@"%@/api/update-gallery-info",ARTEVUE_SERVER_BASE_API_URL];
        NSMutableDictionary *parameterDic = [[NSMutableDictionary alloc] init];
        
        [parameterDic setObject:name forKey:@"gallery_name"];
        [parameterDic setObject:details forKey:@"gallery_description"];
        
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            
           [self patchServerRequestWithParams:parameterDic forUrl:urlString withResponseCallback:^(NSDictionary *responseDictionary) {
               
               if ( responseDictionary!=nil) {
                   //Valid Data From Server
                   
                   dispatch_async(dispatch_get_main_queue(), ^{
                       completion(TRUE);
                   });
                   
               }else{
                   
                   dispatch_async(dispatch_get_main_queue(), ^{
                       completion(FALSE);
                   });
                   
               }
           }];
            
            
        });
    }else{
        [self showAlertForNoInternet];
    }
    
}


- (void)updateSettingsWithCompletion:(api_Completion_Handler_Status)completion{
    
    if ([self checkForNetworkAvailability]) {
        
        
        NSString *urlString=[NSString stringWithFormat:@"%@/api/update-settings",ARTEVUE_SERVER_BASE_API_URL];
        NSMutableDictionary *parameterDic = [[NSMutableDictionary alloc] init];
        
        [parameterDic setObject:[NSNumber numberWithBool:[UserAccount sharedManager].isAccountPrivate] forKey:@"is_account_private"];
        [parameterDic setObject:[NSNumber numberWithBool:[UserAccount sharedManager].isSavetoAlbum] forKey:@"is_save_to_phone"];
        [parameterDic setObject:[NSNumber numberWithBool:[UserAccount sharedManager].isNotificationEnabled] forKey:@"is_notification_enabled"];
        
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            
            [self patchServerRequestWithParams:parameterDic forUrl:urlString withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
            }];
            
            
        });
    }else{
        [self showAlertForNoInternet];
    }
    
}

-(void) postReportAgainstUserId:(int)userId WithCompletion:(api_Completion_Handler_Status)completion
{
    if ([self checkForNetworkAvailability])
    {
        NSString *urlString=[NSString stringWithFormat:@"%@/api/report/%d",ARTEVUE_SERVER_BASE_API_URL,userId];
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            
            [self postServerRequestWithParams:nil forUrl:urlString withResponseCallback:^(NSDictionary *responseDictionary) {
                if ( responseDictionary!=nil) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
                
            }];
            
            
        });
        
    }else{
        [self showAlertForNoInternet];
    }
}
-(void) postBlockUserId:(int)userId WithCompletion:(api_Completion_Handler_Status)completion
{
    if ([self checkForNetworkAvailability])
    {
        NSString *urlString=[NSString stringWithFormat:@"%@/api/block/%d",ARTEVUE_SERVER_BASE_API_URL,userId];
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            
            [self postServerRequestWithParams:nil forUrl:urlString withResponseCallback:^(NSDictionary *responseDictionary) {
                if ( responseDictionary!=nil) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(FALSE);
                    });
                    
                }
                
            }];
            
            
        });
        
    }else{
        [self showAlertForNoInternet];
    }
    
    
}
//- (void) logOutUser:(api_Completion_Handler_Status)completion{
//    if ([self checkForNetworkAvailability]) {
//        [XIBActivityIndicator startActivity];
//        
//        NSString *parameterString = [NSString stringWithFormat:@"func_id=31&user_id=%@&access_token=%@",
//                                     [UserManager sharedManager].userID,
//                                     [UserManager sharedManager].userAccessToken];
//        XLog(@"API-31: %@",parameterString);
//        
//        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
//        
//        dispatch_async(backgroundQueue, ^{
//            
//            [self makeServerRequestWithStringParams:parameterString withResponseCallback:^(NSDictionary *responseDictionary) {
//                
//                if ([self validateResponseData:responseDictionary] && responseDictionary!=nil) {
//                    //Valid Data From Server
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        
//                        [UserManager sharedManager].userAccessToken = @"invalidAccessToken";
//                        [UserManager sharedManager].userID = @"0";
//                        [UserManager sharedManager].userName = @"";
//                        [UserManager sharedManager].userPassword = @"";
//                        
//                        completion(TRUE);
//                        [XIBActivityIndicator dismissActivity];
//                    });
//                    
//                }else{
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        completion(FALSE);
//                        [XIBActivityIndicator dismissActivity];
//                    });
//                    
//                }
//                
//            }];
//            
//        });
//    }else{
//        [self showAlertForNoInternet];
//    }
//    
//}

#pragma mark - Server Request
-(void)postServerRequestWithParams:(NSDictionary*)params forUrl:(NSString*)url withResponseCallback:(void (^)(NSDictionary *responseDictionary))callback
{
        
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    apiLoginManager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    [apiLoginManager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",[UserAccount sharedManager].accessToken] forHTTPHeaderField:@"Authorization"];
    //[apiLoginManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"content-Type"];
    

    
    [apiLoginManager POST:[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"responseObject %@",responseObject);
        if ([operation.response statusCode] == 200) {
           
            callback([responseObject dictionaryByReplacingNullsWithBlanks]);
            
        }
        else{
            callback(nil);
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error %@ ",error);
        callback(nil);
    }];
}

-(void)getServerRequestForUrl:(NSString*)url withResponseCallback:(void (^)(NSDictionary *responseDictionary))callback
{
    
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [apiLoginManager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",[UserAccount sharedManager].accessToken] forHTTPHeaderField:@"Authorization"];
   
    [apiLoginManager GET: [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"responseObject %@",responseObject);
        
        if ([operation.response statusCode] == 200) {
            
            callback([responseObject dictionaryByReplacingNullsWithBlanks]);
            
        }
        else{
            callback(nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error %@",error);
        callback(nil);
    }];
}

-(void)deleteServerRequestForUrl:(NSString*)url withResponseCallback:(void (^)(NSDictionary *responseDictionary))callback
{
    
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [apiLoginManager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",[UserAccount sharedManager].accessToken] forHTTPHeaderField:@"Authorization"];
    
    [apiLoginManager DELETE:[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"responseObject %@",responseObject);
        
        if ([operation.response statusCode] == 200) {
            
            callback([responseObject dictionaryByReplacingNullsWithBlanks]);
            
        }
        else{
            callback(nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error %@",error);
        callback(nil);
    }];
}

-(void)putServerRequestWithParams:(NSDictionary*)params forUrl:(NSString*)url withResponseCallback:(void (^)(NSDictionary *responseDictionary))callback
{
    
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [apiLoginManager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",[UserAccount sharedManager].accessToken] forHTTPHeaderField:@"Authorization"];
    [apiLoginManager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [apiLoginManager PUT:[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
       // NSLog(@"responseObject %@",responseObject);
        if ([operation.response statusCode] == 200) {
            
            callback([responseObject dictionaryByReplacingNullsWithBlanks]);
            
        }
        else{
            callback(nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error %@ %@",error,operation.responseString);
        callback(nil);;
    }];
}


-(void)patchServerRequestWithParams:(NSDictionary*)params forUrl:(NSString*)url withResponseCallback:(void (^)(NSDictionary *responseDictionary))callback
{
    
    //NSLog(@"params %@ url %@",params,url);
    
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [apiLoginManager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",[UserAccount sharedManager].accessToken] forHTTPHeaderField:@"Authorization"];
    [apiLoginManager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [apiLoginManager PATCH:[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([operation.response statusCode] == 200) {
            
            callback([responseObject dictionaryByReplacingNullsWithBlanks]);
            
        }
        else{
            callback(nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error %@",error);
        callback(nil);;
    }];
}

-(void)postServerRequestForImage:(NSData*)imageData WithParams:(NSDictionary*)params forUrl:(NSString*)url keyValue:(NSString*)keyName withResponseCallback:(void (^)(NSDictionary *responseDictionary))callback
{
    
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    
    [apiLoginManager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",[UserAccount sharedManager].accessToken] forHTTPHeaderField:@"Authorization"];
    
    [apiLoginManager POST:[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        //NSData *imageData = UIImageJPEGRepresentation(chosenImage, 0.5);
       // NSLog(@"keyname %@ mageData %@",keyName,imageData);
        [formData appendPartWithFileData:imageData
                                    name:keyName
                                fileName:[NSString stringWithFormat:@"%@.jpg,",keyName ] mimeType:@"image/jpeg"];
        
        
        
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
      
        if ([operation.response statusCode] == 200) {
            callback([responseObject dictionaryByReplacingNullsWithBlanks]);
            
        }
        else{
            callback(nil);
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error %@",error);
        callback(nil);
        
    }];
    
    
}

//Call to server for data
- (void) makeServerRequestWithStringParams:(NSString*)params withResponseCallback:(void (^)(NSDictionary *responseDictionary))callback {
    dispatch_queue_t apiQueue = dispatch_queue_create("API Queue", NULL);
    dispatch_async(apiQueue, ^{
        
        @autoreleasepool {
            NSURL *url = [NSURL URLWithString:SERVER_BASE_API_URL];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            request.HTTPMethod = @"POST";
            request.HTTPBody = [params dataUsingEncoding:NSUTF8StringEncoding];
            
            NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
            sessionConfiguration.timeoutIntervalForResource = 60.0;
            NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
            NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                if (!error) {
                    NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
                    //XLog(@"#### %@ ####", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                    if (httpResp.statusCode == 200) {
                        NSError *jsonError;
                        //XLog(@"Data 2: %@",data);
                        NSDictionary *jsonData =
                        [NSJSONSerialization JSONObjectWithData:data
                                                        options:NSJSONReadingAllowFragments
                                                          error:&jsonError];
                        if (!jsonError) {
                            //XLog(@"jsonData # %@ - Data - %@",params,jsonData);
                            callback(jsonData);
                        }else{
                            //XLog(@"JsonError # Error - %@",jsonError);
                            callback(nil);
                        }
                    }
                }else{
                    callback(nil);
                }
            }];
            
            [postDataTask resume];
        }
    });
    
}
#pragma mark -
#pragma mark - Server Request
//Call to server for data
- (void) makeServerRequestWithParams:(NSDictionary*)params withResponseCallback:(void (^)(NSDictionary *responseDictionary))callback {

    dispatch_queue_t apiQueue = dispatch_queue_create("API Queue", NULL);
    dispatch_async(apiQueue, ^{
        
        NSURL *url = [NSURL URLWithString:SERVER_BASE_API_URL];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        request.HTTPMethod = @"POST";
        request.HTTPBody = [[self urlStringFromDictionary:params] dataUsingEncoding:NSUTF8StringEncoding];
        
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (!error) {
                NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
                //XLog(@"#### %@ ####", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                if (httpResp.statusCode == 200) {
                    NSError *jsonError;
                    //XLog(@"Data 2: %@",data);
                    NSDictionary *jsonData =
                    [NSJSONSerialization JSONObjectWithData:data
                                                    options:NSJSONReadingAllowFragments
                                                      error:&jsonError];
                    if (!jsonError) {
                        //XLog(@"jsonData # %@ - Data - %@",params,jsonData);
                        callback(jsonData);
                    }else{
                        //XLog(@"JsonError # Error - %@",jsonError);
                        callback(nil);
                    }
                }
            }else{
                callback(nil);
            }
        }];
        
        [postDataTask resume];
    });
    
}





//Check status for valid data from server
- (BOOL)validateResponseData:(NSDictionary*)responseDictionary{
    
    if ([[responseDictionary objectForKey:@"status"] integerValue]==5) {

        dispatch_async(dispatch_get_main_queue(), ^{
            [self performSelector:@selector(present) withObject:nil afterDelay: 0.1];
        });
        
    }
    
    return [[responseDictionary objectForKey:@"status"] integerValue]!=1?FALSE:TRUE;
}

- (void)present{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"xxx" object:nil];
}

//Convert Parameter Dictionary to Single string parameter
#pragma mark - Dictionary to String
- (NSString *)urlStringFromDictionary:(NSDictionary*)dict{
    NSArray *keys;
    int i, count;
    id key, value;
    
    keys = [dict allKeys];
    count = (int)[keys count];
    
    NSString *paramString = @"";
    
    for (i = count-1; i >= 0; i--){
        key = [keys objectAtIndex: i];
        value = [dict objectForKey: key];
        if (![paramString isEqualToString:@""])paramString = [paramString stringByAppendingString:@"&"];
        paramString = [paramString stringByAppendingString:[NSString stringWithFormat:@"%@=%@",key,value]];
        
    }
    
    return paramString;
}

#pragma mark - Network Reachability
- (BOOL)checkForNetworkAvailability{
    networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if ((networkStatus != ReachableViaWiFi) && (networkStatus != ReachableViaWWAN)) {
        self.isNetworkAvailable = FALSE;
    }else{
        self.isNetworkAvailable = TRUE;
    }
    
    return self.isNetworkAvailable;
}


- (UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}
-(NSData*)compressImageForPP: (UIImage *)img
{
    float MAX_UPLOAD_SIZE=100;
    float MIN_UPLOAD_RESOLUTION=120*120;
    float factor;
    float resol = img.size.height*img.size.width;
    if (resol >MIN_UPLOAD_RESOLUTION){
        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
        img = [self scaleDown:img withSize:CGSizeMake(img.size.width/factor, img.size.height/factor)];
    }
    
    //Compress the image
    CGFloat compression = 1.0f;
    CGFloat maxCompression = 0.5f;
    
    NSData *imageData = UIImageJPEGRepresentation(img, compression);
    
    while ([imageData length] > MAX_UPLOAD_SIZE && compression > maxCompression)
    {
        compression -= 0.10;
        imageData = UIImageJPEGRepresentation(img, compression);
        NSLog(@"Compress : %lu",(unsigned long)imageData.length);
    }
    return imageData;
}

-(NSData*)compressImageForPost: (UIImage *)img
{
    float MAX_UPLOAD_SIZE=100;
    float MIN_UPLOAD_RESOLUTION=756*756;
    float factor;
    float resol = img.size.height*img.size.width;
    if (resol >MIN_UPLOAD_RESOLUTION){
        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
        img = [self scaleDown:img withSize:CGSizeMake(img.size.width/factor, img.size.height/factor)];
    }
    
    //Compress the image
    CGFloat compression = 1.0f;
    CGFloat maxCompression = 0.1f;
    
    NSData *imageData = UIImageJPEGRepresentation(img, compression);
    
    while ([imageData length] > MAX_UPLOAD_SIZE && compression > maxCompression)
    {
        compression -= 0.10;
        imageData = UIImageJPEGRepresentation(img, compression);
        NSLog(@"Compress : %lu",(unsigned long)imageData.length);
    }
    return imageData;
}

//server not available
- (void)showAlertForNoInternet{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet connection available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        alert = nil;
        
          });
}

@end
