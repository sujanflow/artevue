//
//  FollowingActivityTableViewCell.h
//  Artegram
//
//  Created by Sujan on 7/24/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FollowingActivityTableViewCell : UITableViewCell<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>


@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;

@property (weak, nonatomic) IBOutlet UILabel *activityLabel;

@property (weak, nonatomic) IBOutlet UIImageView *singleLikedImage;

@property (weak, nonatomic) IBOutlet UICollectionView *likedImageCollectionView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *singleImageHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *singleImageWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *activityLabelTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewtop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewBottom;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textTop;

@end
