//
//  ActivityListViewController.h
//  Artegram
//
//  Created by Tanvir Palash on 6/10/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"


@interface ActivityListViewController : UIViewController


@property(nonatomic,strong)NSMutableDictionary* userInfo;
@property(nonatomic,strong)NSMutableDictionary* viewPostInfo;

@end
