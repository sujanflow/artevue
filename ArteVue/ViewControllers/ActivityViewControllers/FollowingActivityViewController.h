//
//  FollowingActivityViewController.h
//  Artegram
//
//  Created by Sujan on 7/20/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"

@interface FollowingActivityViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,SDWebImageManagerDelegate>


@property (weak, nonatomic) IBOutlet UITableView *followingTableView;

@property (weak, nonatomic) IBOutlet UILabel *noFollowingActivityLabel;

@end
