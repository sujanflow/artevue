//
//  UserActivityTableViewCell.h
//  Artegrams
//
//  Created by Tanvir Palash on 8/14/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserActivityTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *activityLabel;
@property (weak, nonatomic) IBOutlet UIButton *followButton;

@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UIImageView *likedImage;


@end
