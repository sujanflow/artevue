//
//  UserActivityViewController.m
//  Artegram
//
//  Created by Sujan on 7/20/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "UserActivityViewController.h"
#import "Constants.h"
#import "UserAccount.h"
#import "HexColors.h"
#import "DGActivityIndicatorView.h"
#import "ProfileViewController.h"
#import "ViewPostViewController.h"

#import "UserActivityTableViewCell.h"
#import "UIImage+Resize.h"

#import <Realm/Realm.h>
#import "Posts.h"

#import "ServerManager.h"

@interface UserActivityViewController (){

    
    NSMutableArray* personalActivityList;
   
    RLMRealmConfiguration *realmv1Configuration;
    NSMutableArray *downloadingIds;
    
    int pageIndex;

}
@property (nonatomic) DGActivityIndicatorView *activityIndicatorView;

@end

@implementation UserActivityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    pageIndex=1;
    
    self.activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)DGActivityIndicatorAnimationTypeTriplePulse tintColor:[UIColor blackColor]];
    CGFloat width = self.view.bounds.size.width / 5.0f;
    CGFloat height = self.view.bounds.size.width / 5.0f;
    
    self.activityIndicatorView.frame = CGRectMake(self.view.bounds.size.width/2-width/2,self.view.bounds.size.height/2-height/2 - 104, width, height);
    [self.view addSubview:self.activityIndicatorView];
    
    [[NSFileManager defaultManager] removeItemAtURL:[RLMRealmConfiguration defaultConfiguration].fileURL error:nil];
    
    NSURL *defaultRealmURL = [RLMRealmConfiguration defaultConfiguration].fileURL;
    NSURL *defaultRealmParentURL = [defaultRealmURL URLByDeletingLastPathComponent];
    NSURL *realmv1URL = [defaultRealmParentURL URLByAppendingPathComponent:@"defaultv1.realm"];
    
    realmv1Configuration = [[RLMRealmConfiguration defaultConfiguration] copy];
    realmv1Configuration.fileURL = realmv1URL;
    downloadingIds=[[NSMutableArray alloc] init];
    
    //setting delegate
    
    
    self.userActivityTableView.delegate = self;
    self.userActivityTableView.dataSource = self;
    
    self.userActivityTableView.estimatedRowHeight = 61;
    self.userActivityTableView.rowHeight = UITableViewAutomaticDimension;
    
    self.userActivityTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.userActivityTableView.frame.size.width, 1)];
    
  
   // [SDWebImageManager sharedManager].delegate=self;
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated
{
 
    //api call
    
    [self makeRequest];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    
    downloadingIds=[[NSMutableArray alloc] init];
    [[SDWebImageDownloader sharedDownloader] cancelAllDownloads];
    
}

-(void) makeRequest{
    
    [self.activityIndicatorView startAnimating];
    
    
    [[ServerManager sharedManager] getOwnActivitiesinPage:pageIndex WithCompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
        if ( resultDataArray!=nil) {
            
            personalActivityList=[[NSMutableArray alloc] initWithArray:[resultDataArray objectForKey:@"data"]];
            
            if(personalActivityList.count)
            {
                self.userActivityTableView.hidden = NO;
                self.noActitivityLabel.hidden=YES;
                
                
            }
            else
            {
                self.userActivityTableView.hidden = YES;
                
                self.noActitivityLabel.hidden=NO;
            }
            
            
            [self.activityIndicatorView stopAnimating];
            
            [self.userActivityTableView reloadData];
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                //[[[UIAlertView alloc] initWithTitle:@"Sorry, unable to fetch like data. Please try again." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                
                
                [self.activityIndicatorView stopAnimating];
                
                self.userActivityTableView.hidden = YES;
                self.noActitivityLabel.hidden=NO;

                
            });
            
        }
        
    }];

    
}



#pragma mark - UITableView DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return personalActivityList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"userActivityCell";
    
    UserActivityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[UserActivityTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    
   // NSLog(@"personal activity %@",personalActivityList);
    
//    UIImageView *profileImage = (UIImageView*) [cell viewWithTag:1];
//    UILabel *activityLabel = (UILabel*) [cell viewWithTag:2];
//    UIButton *followButton = (UIButton*) [cell viewWithTag:3];
//    UIImageView *likedImage = (UIImageView*) [cell viewWithTag:4];
    
    //set tag
    cell.profilePic.tag = indexPath.row;
    cell.likedImage.tag = indexPath.row;
    
    //profile pic tap
    UITapGestureRecognizer* proPicTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnProfilePic:)];
    proPicTapGesture.numberOfTapsRequired = 1;
    [cell.profilePic addGestureRecognizer:proPicTapGesture];
    cell.profilePic.userInteractionEnabled = YES;
    
    //liked image tap
    UITapGestureRecognizer* likedImageTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnLikedImage:)];
    likedImageTapGesture.numberOfTapsRequired = 1;
    [cell.likedImage addGestureRecognizer:likedImageTapGesture];
    cell.likedImage.userInteractionEnabled = YES;
    
    
    NSMutableDictionary*tempDic = [personalActivityList objectAtIndex:indexPath.row];
    
    if ([[tempDic objectForKey:@"profile_picture"] isEqual:[NSNull null]]) {
        
        [cell.profilePic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,@"img/profile-holder.png"]]];
        
        
    }else{
    
       [cell.profilePic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[tempDic objectForKey:@"profile_picture"]]]];
    
    }
    if ([[tempDic objectForKey:@"type"] isEqualToString:@"like"]) {
        
        NSString* formatString = [tempDic objectForKey:@"created_at"];
        NSString* dateString =[self relativeDateStringForDate:[self getDateFromString:formatString]];
        NSString* user = [tempDic objectForKey:@"username"];
        NSString * activityText = @" liked your photo.";
        
        cell.activityLabel.attributedText = [self getActivityTextConfigured:user toActivity:activityText toDate:dateString];
        
        cell.followButton.hidden = YES;
        cell.likedImage.hidden = NO;
        
       // [cell.likedImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[tempDic objectForKey:@"image"]]]];
        
        //NSLog(@"query for in user%@",tempDic);
        
        
        RLMRealm *realmInExplore = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
        RLMResults *result=[Posts objectsInRealm:realmInExplore where:[NSString stringWithFormat:@"postId = %@",[tempDic objectForKey:@"post_id"]]];
        
        if(result.count)
        {
            Posts *post=[[Posts alloc] init];
            post=[result objectAtIndex:0];
            NSLog(@"result %@",result);
            
            
          //  [UIView transitionWithView:cell.likedImage duration:0.3f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                cell.likedImage.image=[UIImage imageWithData:post.imageData];
                
//            } completion:^(BOOL finished) {
//                ;
//            }];
        }
        else
        {
            cell.likedImage.image=nil;
            
            if([downloadingIds containsObject: [tempDic objectForKey:@"post_id"]])
            {
                //do nothing
                NSLog(@"downloading in explore collection");
            }
            else
            {
                
                [downloadingIds addObject:[tempDic objectForKey:@"post_id"]];
                
                SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
                [SDWebImageDownloader sharedDownloader].maxConcurrentDownloads=1;
                
                [downloader downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[tempDic objectForKey:@"image"] ]]
                                         options:0
                                        progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                            // progression tracking code
                                            // NSLog(@"receivedSize %ld expectedSize %ld",(long)receivedSize,(long)expectedSize);
                                            
                                        }
                                       completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                           if (image && finished) {
                                               // do something with image
                                               
                                               //   Posts *post = [[Posts alloc] init];
                                               
                                               dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                                               dispatch_async(queue, ^{
                                                   // Get new realm and table since we are in a new thread
                                                   
                                                   NSLog(@"done for %@",[tempDic objectForKey:@"post_id"]);
                                                   
                                                   //RLMRealm *realm = [RLMRealm defaultRealm];
                                                   
                                                   RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
                                                   
                                                   
                                                   [realm beginWriteTransaction];
                                                   //     [Posts createOrUpdateInRealm:realm withValue:@{@"postId": [NSNumber numberWithInt:[[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"id"] intValue]],
                                                   //                                            @"imageData": data}];
                                                   [Posts createOrUpdateInRealm:realm withValue:@{@"primId": [NSString stringWithFormat:@"%@",[tempDic objectForKey:@"post_id"]]
                                                                                                  ,@"postId": [NSNumber numberWithInt:[[tempDic objectForKey:@"post_id"] intValue]],
                                                                                                  @"imageData": data}];
                                                   
                                                   
                                                   [realm commitWriteTransaction];
                                                   
                                                   
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       //  cell.feedImage=(UIImageView*) [cell viewWithTag:indexPath.section] ;
                                                       //  cell.feedImage.image=image;
                                                       
                                                       [downloadingIds removeObject:[tempDic objectForKey:@"post_id"]];
                                                       //[self reloadData];
                                                       
                                                       [self.userActivityTableView reloadData];
                                                       
                                                       [self.userActivityTableView layoutIfNeeded];
                                                       
                                                       
                                                       //[self.feedTableView beginUpdates];
                                                       //[self.feedTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                                                       //[self.feedTableView endUpdates];
                                                       
                                                   });
                                                   
                                               });
                                           }
                                           
                                           
                                       }];
                
            }
            
        }

        
        
    }
    else if ([[tempDic objectForKey:@"type"] isEqualToString:@"following"])
    {
        cell.likedImage.image = nil;
        cell.likedImage.hidden=YES;
        cell.followButton.hidden = NO;
        //follow button tag
        cell.followButton.tag = indexPath.row;
        
        NSString* formatString = [tempDic objectForKey:@"created_at"];
        NSString* dateString =[self relativeDateStringForDate:[self getDateFromString:formatString]];
        NSString* user = [tempDic objectForKey:@"username"];
        NSString * activityText = @" started following you.";
        
        cell.activityLabel.attributedText = [self getActivityTextConfigured:user toActivity:activityText toDate:dateString];
        
        //NSLog(@"follow button %@",cell.followButton);
        //set follow button state
        if([[tempDic objectForKey:@"is_following"] integerValue])
        {
            cell.followButton.selected=YES;
            //followButton.backgroundColor = [HXColor hx_colorWithHexString:@"2DCC70"];
//            self.followButton.layer.borderColor = [[HXColor hx_colorWithHexString:@"2DCC70"]CGColor];
            
        }
        else
            cell.followButton.selected=NO;
        
        
        
    }
    else if ([[tempDic objectForKey:@"type"] isEqualToString:@"comment"]) {
        NSString* formatString = [tempDic objectForKey:@"created_at"];
        NSString* dateString =[self relativeDateStringForDate:[self getDateFromString:formatString]];
        NSString* user = [tempDic objectForKey:@"username"];
        NSString * activityText = @" commented on your photo.";
        
        cell.activityLabel.attributedText = [self getActivityTextConfigured:user toActivity:activityText toDate:dateString];
        
        
        cell.likedImage.hidden = NO;
        
       // [cell.likedImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[tempDic objectForKey:@"image"]]]];
        
        NSLog(@"query for in user%@",[tempDic objectForKey:@"post_id"]);
        
        
        RLMRealm *realmInExplore = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
        RLMResults *result=[Posts objectsInRealm:realmInExplore where:[NSString stringWithFormat:@"postId = %@",[tempDic objectForKey:@"post_id"]]];
        
        if(result.count)
        {
            Posts *post=[[Posts alloc] init];
            post=[result objectAtIndex:0];
            NSLog(@"result %@",result);
            
            
           // [UIView transitionWithView:cell.likedImage duration:0.3f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                cell.likedImage.image=[UIImage imageWithData:post.imageData];
                
//            } completion:^(BOOL finished) {
//                ;
//            }];
        }
        else
        {
            cell.likedImage.image=nil;
            
            if([downloadingIds containsObject: [tempDic objectForKey:@"post_id"]])
            {
                //do nothing
                NSLog(@"downloading in explore collection");
            }
            else
            {
                
                [downloadingIds addObject:[tempDic objectForKey:@"post_id"]];
                
                SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
                [SDWebImageDownloader sharedDownloader].maxConcurrentDownloads=1;
                
                [downloader downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[tempDic objectForKey:@"image"] ]]
                                         options:0
                                        progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                            // progression tracking code
                                            // NSLog(@"receivedSize %ld expectedSize %ld",(long)receivedSize,(long)expectedSize);
                                            
                                        }
                                       completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                           if (image && finished) {
                                               // do something with image
                                               
                                               //   Posts *post = [[Posts alloc] init];
                                               
                                               dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                                               dispatch_async(queue, ^{
                                                   // Get new realm and table since we are in a new thread
                                                   
                                                   NSLog(@"done for %@",[tempDic objectForKey:@"post_id"]);
                                                   
                                                   //RLMRealm *realm = [RLMRealm defaultRealm];
                                                   
                                                   RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
                                                   
                                                   
                                                   [realm beginWriteTransaction];
                                                   //     [Posts createOrUpdateInRealm:realm withValue:@{@"postId": [NSNumber numberWithInt:[[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"id"] intValue]],
                                                   //                                            @"imageData": data}];
                                                   [Posts createOrUpdateInRealm:realm withValue:@{@"primId": [NSString stringWithFormat:@"%@",[tempDic objectForKey:@"post_id"]]
                                                                                                  ,@"postId": [NSNumber numberWithInt:[[tempDic objectForKey:@"post_id"] intValue]],
                                                                                                  @"imageData": data}];
                                                   
                                                   
                                                   [realm commitWriteTransaction];
                                                   
                                                   
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       //  cell.feedImage=(UIImageView*) [cell viewWithTag:indexPath.section] ;
                                                       //  cell.feedImage.image=image;
                                                       
                                                       [downloadingIds removeObject:[tempDic objectForKey:@"post_id"]];
                                                       //[self reloadData];
                                                       
                                                       [self.userActivityTableView reloadData];
                                                       
                                                       [self.userActivityTableView layoutIfNeeded];
                                                       
                                                       
                                                       //[self.feedTableView beginUpdates];
                                                       //[self.feedTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                                                       //[self.feedTableView endUpdates];
                                                       
                                                   });
                                                   
                                               });
                                           }
                                           
                                           
                                       }];
                
            }
            
        }
        
    }
    if ([[tempDic objectForKey:@"type"] isEqualToString:@"pin"]) {
        
        NSString* formatString = [tempDic objectForKey:@"created_at"];
        NSString* dateString =[self relativeDateStringForDate:[self getDateFromString:formatString]];
        NSString* user = [tempDic objectForKey:@"username"];
        NSString * activityText = @" pinned your photo.";
        
        cell.activityLabel.attributedText = [self getActivityTextConfigured:user toActivity:activityText toDate:dateString];
        
        cell.followButton.hidden = YES;
        cell.likedImage.hidden = NO;
        
        // [cell.likedImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[tempDic objectForKey:@"image"]]]];
        
        //NSLog(@"query for in user%@",tempDic);
        
        
        RLMRealm *realmInExplore = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
        RLMResults *result=[Posts objectsInRealm:realmInExplore where:[NSString stringWithFormat:@"postId = %@",[tempDic objectForKey:@"post_id"]]];
        
        if(result.count)
        {
            Posts *post=[[Posts alloc] init];
            post=[result objectAtIndex:0];
            NSLog(@"result %@",result);
            
            
            //  [UIView transitionWithView:cell.likedImage duration:0.3f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            cell.likedImage.image=[UIImage imageWithData:post.imageData];
            
            //            } completion:^(BOOL finished) {
            //                ;
            //            }];
        }
        else
        {
            cell.likedImage.image=nil;
            
            if([downloadingIds containsObject: [tempDic objectForKey:@"post_id"]])
            {
                //do nothing
                NSLog(@"downloading in explore collection");
            }
            else
            {
                
                [downloadingIds addObject:[tempDic objectForKey:@"post_id"]];
                
                SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
                [SDWebImageDownloader sharedDownloader].maxConcurrentDownloads=1;
                
                [downloader downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[tempDic objectForKey:@"image"] ]]
                                         options:0
                                        progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                            // progression tracking code
                                            // NSLog(@"receivedSize %ld expectedSize %ld",(long)receivedSize,(long)expectedSize);
                                            
                                        }
                                       completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                           if (image && finished) {
                                               // do something with image
                                               
                                               //   Posts *post = [[Posts alloc] init];
                                               
                                               dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                                               dispatch_async(queue, ^{
                                                   // Get new realm and table since we are in a new thread
                                                   
                                                   NSLog(@"done for %@",[tempDic objectForKey:@"post_id"]);
                                                   
                                                   //RLMRealm *realm = [RLMRealm defaultRealm];
                                                   
                                                   RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
                                                   
                                                   
                                                   [realm beginWriteTransaction];
                                                   //     [Posts createOrUpdateInRealm:realm withValue:@{@"postId": [NSNumber numberWithInt:[[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"id"] intValue]],
                                                   //                                            @"imageData": data}];
                                                   [Posts createOrUpdateInRealm:realm withValue:@{@"primId": [NSString stringWithFormat:@"%@",[tempDic objectForKey:@"post_id"]]
                                                                                                  ,@"postId": [NSNumber numberWithInt:[[tempDic objectForKey:@"post_id"] intValue]],
                                                                                                  @"imageData": data}];
                                                   
                                                   
                                                   [realm commitWriteTransaction];
                                                   
                                                   
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       //  cell.feedImage=(UIImageView*) [cell viewWithTag:indexPath.section] ;
                                                       //  cell.feedImage.image=image;
                                                       
                                                       [downloadingIds removeObject:[tempDic objectForKey:@"post_id"]];
                                                       //[self reloadData];
                                                       
                                                       [self.userActivityTableView reloadData];
                                                       
                                                       [self.userActivityTableView layoutIfNeeded];
                                                       
                                                       
                                                       //[self.feedTableView beginUpdates];
                                                       //[self.feedTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                                                       //[self.feedTableView endUpdates];
                                                       
                                                   });
                                                   
                                               });
                                           }
                                           
                                           
                                       }];
                
            }
            
        }
        
        
        
    }

    
    return cell;
}

#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    

    
    
}

-(NSMutableAttributedString*) getActivityTextConfigured:(NSString*)user toActivity:(NSString*)activity toDate:(NSString*)date
{
    UIFont *activityFont = [UIFont fontWithName:@"AzoSans-Regular" size:13];
    UIFont *userFont = [UIFont fontWithName:@"AzoSans-Medium" size:13];
    
 
    NSDictionary *activityDict = [NSDictionary dictionaryWithObject: activityFont forKey:NSFontAttributeName];
    NSDictionary *userDict = [NSDictionary dictionaryWithObject:userFont forKey:NSFontAttributeName ];
    
    NSMutableAttributedString *uAttrString = [[NSMutableAttributedString alloc] initWithString:user attributes: userDict];
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:activity attributes: activityDict];
    NSMutableAttributedString *vAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",date] attributes:activityDict];
    
    [vAttrString addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor ] range:NSMakeRange(0, date.length +1)];
    
    
    [aAttrString appendAttributedString:vAttrString];
    [uAttrString appendAttributedString:aAttrString];
    
    return uAttrString;
}

-(void)tapOnProfilePic:(UITapGestureRecognizer *)recognizer
{
    NSLog(@"pro pic tapepes");
    
    
    NSMutableDictionary *peopleInfo = [[NSMutableDictionary alloc]init];
    
    
    [peopleInfo setObject:[[personalActivityList objectAtIndex:recognizer.view.tag ] objectForKey:@"user_id"] forKey:@"user_id"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"goProfileView" object:peopleInfo];
    
   
}

-(void)tapOnLikedImage:(UITapGestureRecognizer *)recognizer
{
      NSLog(@"liked pic tapepes");
    
     NSMutableDictionary *viewPostInfo = [[NSMutableDictionary alloc]init];

    [viewPostInfo setObject:[[personalActivityList objectAtIndex:recognizer.view.tag ] objectForKey:@"user_id"] forKey:@"user_id"];
    [viewPostInfo setObject:[[personalActivityList objectAtIndex:recognizer.view.tag ] objectForKey:@"post_id"] forKey:@"post_id"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"goViewPost" object:viewPostInfo];
    
}


-(NSDate *)getDateFromString:(NSString *)dateString
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy'-'MM'-'dd' 'HH':'mm':'ss"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    
    //[formatter setDateFormat:@"EEE, MMM dd, yyyy"];
    NSDate *currentDate=[formatter dateFromString:dateString];
    return currentDate;
}

-(NSString *)getStringFromDate:(NSDate *)date
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd, YYYY"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    NSLog(@"%@", stringFromDate);
    return stringFromDate;
}


- (NSString *)relativeDateStringForDate:(NSDate *)date
{
    
    NSCalendarUnit units = NSCalendarUnitDay | NSCalendarUnitWeekOfYear |
    NSCalendarUnitMonth | NSCalendarUnitYear|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond;
    
    // if `date` is before "now" (i.e. in the past) then the components will be positive
    NSDateComponents *components = [[NSCalendar currentCalendar] components:units
                                                                   fromDate:date
                                                                     toDate:[NSDate date]
                                                                    options:0];
    
    //NSLog(@"components %@",components);
    if (components.year > 0) {
        
        return [self getStringFromDate:date];
        
        //        if (components.year > 1) {
        //            return [NSString stringWithFormat:@"%ld years ago", (long)components.year];
        //        } else {
        //            return [NSString stringWithFormat:@"%ld year ago", (long)components.year];
        //
        //        }
        
        
    } else if (components.month > 0) {
        
        if (components.month > 1) {
            return [self getStringFromDate:date];
            
            // return [NSString stringWithFormat:@"%ld months ago", (long)components.month];
        } else {
            return [NSString stringWithFormat:@"%ld month ago", (long)components.month];
            
        }
        
        
        
    } else if (components.weekOfYear > 0) {
        if (components.weekOfYear > 1) {
            return [NSString stringWithFormat:@"%ld weeks ago", (long)components.weekOfYear];
        } else {
            return [NSString stringWithFormat:@"%ld week ago", (long)components.weekOfYear];
        }
        
        
        
    } else if (components.day > 0) {
        if (components.day > 1) {
            return [NSString stringWithFormat:@"%ld days ago", (long)components.day];
        } else {
            return @"Yesterday";
        }
    } else {
        if (components.hour > 1) {
            return [NSString stringWithFormat:@"%ld hours ago", (long)components.hour];
        }
        else if(components.minute > 1){
            return [NSString stringWithFormat:@"%ld minutes ago", (long)components.minute];
        }
        else
        {
            // return [NSString stringWithFormat:@"less than a minutes"];
            return [NSString stringWithFormat:@"%ld seconds ago", (long)components.second];
        }
        
    }
}

- (IBAction)followButtonAction:(UIButton *)sender {
    
    sender.userInteractionEnabled = NO;
    
    
    [[ServerManager sharedManager] postFollowUserId:[[[personalActivityList objectAtIndex:sender.tag ] objectForKey:@"user_id"]intValue] WithCompletion:^(BOOL success) {
        
        sender.userInteractionEnabled = YES;
        
        if (success) {
            
            
            
            if (sender.selected) {
                
                //sender.backgroundColor = [UIColor grayColor];
                
            }else{
                
                //sender.backgroundColor = [HXColor hx_colorWithHexString:@"2DCC70"];
                // sender.layer.borderColor = [[HXColor hx_colorWithHexString:@"2DCC70"]CGColor];
            }
            
            sender.selected=!sender.selected;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"loadAllFeed" object:nil];

        }
      
        
    }];
    
}

//-(UIImage *)imageManager:(SDWebImageManager *)imageManager transformDownloadedImage:(UIImage *)image withURL:(NSURL *)imageURL
//{
//    NSLog(@"imageURL : %@",imageURL);
//    UIImage *resizedImage;
//    return resizedImage  = [image resizedImage:CGSizeMake(512, 512) interpolationQuality:kCGInterpolationDefault];
//}

//-(UIImage *)imageManager:(SDWebImageManager *)imageManager transformDownloadedImage:(UIImage *)image withURL:(NSURL *)imageURL
//{
//    // NSLog(@"imageURL : %@",imageURL);
//    UIImage *resizedImage;
//    //  return resizedImage  = [image resizedImage:CGSizeMake(512, 512) interpolationQuality:kCGInterpolationDefault];
//    
//    float MIN_UPLOAD_RESOLUTION=756*756;
//    float factor;
//    float resol = image.size.height*image.size.width;
//    if (resol >MIN_UPLOAD_RESOLUTION){
//        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
//        resizedImage = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
//        return resizedImage;
//        
//    }
//    else
//        return image;
//}
//
//- (UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
//    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
//    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
//    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return scaledImage;
//}

@end
