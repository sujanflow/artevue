//
//  ActivityListViewController.m
//  Artegram
//
//  Created by Tanvir Palash on 6/10/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#define COLOR_LIGHT_RED         [UIColor colorWithRed:1 green:204.0/255 blue:204.0/255.0 alpha:1.0]
#define COLOR_LIGHT_GREEN       [UIColor colorWithRed:204.0/255 green:1 blue:204.0/255 alpha:1.0]
#define COLOR_LIGHT_BLUE        [UIColor colorWithRed:204.0/255 green:204.0/255 blue:1 alpha:1.


#import "ActivityListViewController.h"
#import "ADPageControl.h"
#import "FollowingActivityViewController.h"
#import "UserActivityViewController.h"
#import "Constants.h"
#import "UserAccount.h"
#import "ProfileViewController.h"
#import "ViewPostViewController.h"

@interface ActivityListViewController ()<ADPageControlDelegate>
{
    ADPageControl   *_pageControl;
  
    
}


@end

@implementation ActivityListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewPostView:) name:@"goViewPost" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(profileView:) name:@"goProfileView" object:nil];
    
    //setting view pager
    
    [self setupPageControl];
    
  

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)profileView:(NSNotification*) notification
{
    
    self.userInfo = [notification object];
   
    
    ProfileViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    
    controller.isUserOrFollower = YES;
    controller.userId = [[self.userInfo objectForKey:@"user_id"]intValue];
    
    [self.navigationController pushViewController:controller animated:YES];
    
    
}
-(void)viewPostView:(NSNotification*) notification
{
   //  NSLog(@".......");
    
    self.viewPostInfo = [notification object];
    
    ViewPostViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewPostViewController"];
    
    controller.isFromActivity = YES;
    controller.postId = [[self.viewPostInfo objectForKey:@"post_id"]intValue];
    
    [self.navigationController pushViewController:controller animated:YES];

    
}



-(void)setupPageControl
{
    
    /**** 1. Setup pages using model class "ADPageModel" ****/
    
    
    //page 0
    ADPageModel *pageModel0 =       [[ADPageModel alloc] init];
    //UIViewController *page0 =       [UIViewController new];
    
    FollowingActivityViewController *page0 =       [self.storyboard instantiateViewControllerWithIdentifier:@"FollowingActivityViewController"];
    
    pageModel0.strPageTitle =       @"Following";
    pageModel0.iPageNumber =        0;
    pageModel0.viewController =     page0;//You can provide view controller in prior OR use flag "bShouldLazyLoad" to load only when required
    
    //page 1
    ADPageModel *pageModel1 =       [[ADPageModel alloc] init];
    UserActivityViewController *page1 =       [self.storyboard instantiateViewControllerWithIdentifier:@"UserActivityViewController"];
    pageModel1.strPageTitle =       @"You";
    pageModel1.iPageNumber =        1;
    //pageModel1.bShouldLazyLoad =    YES;
    pageModel1.viewController =     page1;
    

    
    
    
    /**** 2. Initialize page control ****/
    
    _pageControl = [[ADPageControl alloc] init];
    _pageControl.delegateADPageControl = self;
    _pageControl.arrPageModel = [[NSMutableArray alloc] initWithObjects:
                                 pageModel0,
                                 pageModel1,nil];
    
    
    
    /**** 3. Customize parameters (Optinal, as all have default value set) ****/
    
    _pageControl.iFirstVisiblePageNumber =  1;
    _pageControl.iTitleViewHeight =         40;
    _pageControl.iPageIndicatorHeight =     2;
    _pageControl.fontTitleTabText =         [UIFont fontWithName:@"AzoSans-Regular" size:15];
    
    _pageControl.bEnablePagesEndBounceEffect =  NO;
    _pageControl.bEnableTitlesEndBounceEffect = NO;
    
    _pageControl.colorTabText =                     [UIColor blackColor];
    _pageControl.colorTitleBarBackground =          [UIColor whiteColor];
    _pageControl.colorPageIndicator =               [UIColor blackColor];
    _pageControl.colorPageOverscrollBackground =    [UIColor whiteColor];
    
    _pageControl.bShowMoreTabAvailableIndicator =   NO;
    _pageControl.bHideShadow                    =   NO;
    
    
    
    //Uncomment to check custom fixed width tabs
    
    _pageControl.iCustomFixedTabWidth    =               [[UIScreen mainScreen] bounds].size.width/2;
    
    
    /**** 3. Add as subview ****/
    
    [self.view addSubview:_pageControl.view];
    
    _pageControl.view.translatesAutoresizingMaskIntoConstraints = NO;
    UIView *subview = _pageControl.view;
    
    //Leading margin 0, Trailing margin 0
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[subview]-0-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(subview)]];
    
    //Top margin 20 for status bar, Bottom margin 0
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-66-[subview]-0-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(subview)]];
    
    
}

#pragma mark - ADPageControlDelegate

//LAZY LOADING

-(UIViewController *)adPageControlGetViewControllerForPageModel:(ADPageModel *) pageModel
{
    NSLog(@"ADPageControl :: Lazy load asking for page %d",pageModel.iPageNumber);
    
    if(pageModel.iPageNumber == 1)
    {
        UIViewController *page1 =       [UIViewController new];
        page1.view.backgroundColor =    COLOR_LIGHT_GREEN;
        
        return page1;
    }
//    else if(pageModel.iPageNumber == 2)
//    {
//        UIViewController *page2 =       [UIViewController new];
//        page2.view.backgroundColor =    COLOR_LIGHT_BLUE;
//        
//        return page2;
//    }
//    else if(pageModel.iPageNumber == 3)
//    {
//        UIViewController *page3 =       [UIViewController new];
//        page3.view.backgroundColor =    COLOR_LIGHT_YELLOW;
//        
//        return page3;
//    }
    
    return nil;
}

//CURRENT PAGE INDEX

-(void)adPageControlCurrentVisiblePageIndex:(int) iCurrentVisiblePage
{
    NSLog(@"ADPageControl :: Current visible page index : %d",iCurrentVisiblePage);
}
- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}


@end
