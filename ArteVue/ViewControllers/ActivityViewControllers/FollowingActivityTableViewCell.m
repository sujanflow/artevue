//
//  FollowingActivityTableViewCell.m
//  Artegram
//
//  Created by Sujan on 7/24/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "FollowingActivityTableViewCell.h"

@implementation FollowingActivityTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self layoutIfNeeded];
    self.profileImageView.layer.cornerRadius =  self.profileImageView.bounds.size.width/2;
    //self.profilePicture.layer.borderWidth = 1.1f;
    self.profileImageView.clipsToBounds = YES;
    self.profileImageView.layer.masksToBounds = YES;
    
    self.likedImageCollectionView.delegate =self;
    self.likedImageCollectionView.dataSource = self;
    
    self.collectionViewHeight.constant= (([UIScreen mainScreen].bounds.size.width-88)/5);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


#pragma mark - CollectionView data source

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return  5;
    
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    static NSString *identifier = @"followingPhotoCell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    
    //UIImageView *likedImage = (UIImageView*) [cell viewWithTag:1];

    
    // [postsImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL,[[userPosts objectAtIndex:indexPath.row] objectForKey:@"image"]]]];
    
    return cell;
    
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    return CGSizeMake(([UIScreen mainScreen].bounds.size.width-88)/5, ([UIScreen mainScreen].bounds.size.width-88)/5);
    
    
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
}
@end
