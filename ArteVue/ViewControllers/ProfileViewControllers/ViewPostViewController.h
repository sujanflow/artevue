//
//  ViewPostViewController.h
//  Artegram
//
//  Created by Sujan on 7/24/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"
#import "AFNetworking.h"

#import "DGActivityIndicatorView.h"

@interface ViewPostViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate,SDWebImageManagerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *viewPostTableView;

@property (weak, nonatomic) IBOutlet UIButton *refreshButton;


@property UILongPressGestureRecognizer *taplikeLabel;
@property UITapGestureRecognizer *tapFirstcommentLabel;
@property UITapGestureRecognizer *tapSecondcommentLabel;
@property UITapGestureRecognizer *tapThirdcommentLabel;
@property UITapGestureRecognizer *tapcommentCountLabel;


@property (nonatomic, retain) UIDocumentInteractionController *documentController;
@property(nonatomic,strong) UIImage *postImage;

//@property(nonatomic,strong) NSMutableDictionary *singlePostDic;

@property BOOL isFromGallery;
@property BOOL isFromProfile;
@property BOOL isFromProfileWithTaggedPost;

@property BOOL isFromActivity;//11.08
@property int postId;//11.08
@property int userId;//11.08
@property (nonatomic) DGActivityIndicatorView *activityIndicatorView; //11.08

@property int indexNumber;
@property(nonatomic,strong) NSMutableArray *userPostArray;

@end
