//
//  SettingViewController.m
//  SettingForArtegrams
//
//  Created by Sujan on 8/24/16.
//  Copyright © 2016 Sujan. All rights reserved.
//

#import "SettingViewController.h"
#import "TabBarViewController.h"
#import "EditProfileViewController.h"
#import "ChangePasswordViewController.h"

#import "UserAccount.h"
#import "Constants.h"
#import "ServerManager.h"


@interface SettingViewController (){

    NSMutableArray* settingItemArray;
    
    CGRect popupGoToFrame,popupGoFromFrame;
    UIView *termConditionView;
    UIWebView *webview;
}

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.settingTableView.delegate = self;
    self.settingTableView.dataSource = self;
    
     self.settingTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.settingTableView.frame.size.width, 1)];
    
    
//    settingItemArray = [[NSMutableArray alloc]initWithObjects:@"Edit Profile",@"Notifications",@"Change Password",@"Private account",@"Linked accounts",@"Save Original Photos",@"Terms and Conditions",@"Log Out", nil];
    settingItemArray = [[NSMutableArray alloc]initWithObjects:@"Edit Profile",@"Notifications",@"Change Password",@"Private Account",@"Save Original Photos",@"Terms and Conditions",@"Log Out", nil];

    NSLog(@"[UserAccount sharedManager].isNotificationOff %i",![UserAccount sharedManager].isNotificationEnabled);
    NSLog(@"[UserAccount sharedManager].isAccountPrivate %i",[UserAccount sharedManager].isAccountPrivate);
    NSLog(@"[UserAccount sharedManager].isSavetoAlbum %i",[UserAccount sharedManager].isSavetoAlbum);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableView DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return settingItemArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"settingCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    if (indexPath.row == 1 ) {
        
        UISwitch *toggleButton = [[UISwitch alloc] initWithFrame:CGRectZero];
        toggleButton.tag=indexPath.row;
        [toggleButton addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
        
         cell.accessoryView = toggleButton;
        
        if( [UserAccount sharedManager].isNotificationEnabled)
            [toggleButton setOn:YES animated:NO];
        else
            [toggleButton setOn:NO animated:NO];
        
    }
    else if ( indexPath.row == 3 ) {
        
        UISwitch *toggleButton = [[UISwitch alloc] initWithFrame:CGRectZero];
        toggleButton.tag=indexPath.row;
        [toggleButton addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
        
        cell.accessoryView = toggleButton;
        
        if( [UserAccount sharedManager].isAccountPrivate)
            [toggleButton setOn:YES animated:NO];
        else
            [toggleButton setOn:NO animated:NO];
        
    }
    else if (indexPath.row == 4) {
        
        UISwitch *toggleButton = [[UISwitch alloc] initWithFrame:CGRectZero];
        toggleButton.tag=indexPath.row;
        [toggleButton addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
        
        cell.accessoryView = toggleButton;
        
        if( [UserAccount sharedManager].isSavetoAlbum)
            [toggleButton setOn:YES animated:NO];
        else
            [toggleButton setOn:NO animated:NO];
        
    }else if (indexPath.row == 6){
    
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    }
    else{
    
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }

    
    cell.textLabel.text = [settingItemArray objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont fontWithName:@"AzoSans-Regular" size:13];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.textColor = [UIColor blackColor];
    
    return cell;
    
}

#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        
        EditProfileViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileViewController"];
        
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    else if (indexPath.row == 1){
    
    
    }else if (indexPath.row == 2){
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"socialLogin"]integerValue] == 1){
        
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
//                                                            message:@"Can not change password because you logged in by social media account"
//                                                           delegate:nil
//                                                  cancelButtonTitle:@"OK"
//                                                  otherButtonTitles:nil];
//            [alert show];
        
        
        }else{
            
           ChangePasswordViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
        
           controller.isFromSetting = YES;
        
           [self.navigationController pushViewController:controller animated:YES];
            
        }
        
    }else if (indexPath.row == 3){
        
        
    }else if (indexPath.row == 4){
        
        
    }else if (indexPath.row == 5){
        
        tableView.allowsSelection=NO;
        [self createTermsWebview:@"Terms of use"];
        
    }else if (indexPath.row == 6){
        
//        [UserAccount sharedManager].userId=0;
//        [UserAccount sharedManager].userFirstName=@"Guest";
//        [UserAccount sharedManager].userLastName=@"";
//        [UserAccount sharedManager].userImageName=@"/img/profile-holder.png";
//        
        TabBarViewController *tabBar = (TabBarViewController *) self.tabBarController;
        [tabBar logOutFromTabbar];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"socialLogin"];
        
        NSLog(@"%@ %@",tabBar,self.tabBarController);
        // [self.tabBarController.navigationController popViewControllerAnimated:YES];
        
    }
    
}

- (void)switchValueChanged:(UISwitch *)theSwitch
{
    BOOL flag = [theSwitch isOn];
    NSLog(@"theSwitch %li",(long)theSwitch.tag);
    if (theSwitch.tag==1) {
        [UserAccount sharedManager].isNotificationEnabled=flag;
    }
    else if (theSwitch.tag==3) {
        [UserAccount sharedManager].isAccountPrivate=flag;
    }
    else if (theSwitch.tag==4) {
        [UserAccount sharedManager].isSavetoAlbum=flag;
    }
    
  //  NSLog(@"[UserAccount sharedManager].isSavetoAlbum %i",[UserAccount sharedManager].isSavetoAlbum);
    
    [self updateSetting];
}

-(void) updateSetting{
    
   // [self.activityIndicatorView startAnimating];
    
    
    [[ServerManager sharedManager] updateSettingsWithCompletion:^(BOOL success) {
        if (success) {
            
            NSLog(@"Updated");
        }
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSLog(@"Not updated");
                
            });
        }
        
    }];

    
    
    
}

-(void) createTermsWebview:(NSString*) title
{
    
    
    popupGoToFrame = CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height);
    popupGoFromFrame = CGRectMake(0, SCREEN_SIZE.height, SCREEN_SIZE.width, SCREEN_SIZE.height);
    
    termConditionView = [[UIView alloc] initWithFrame:popupGoFromFrame];
    [termConditionView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
    //
    [self.view addSubview:termConditionView];
    //
    
    UIView *bgImageview = [[UIView alloc] initWithFrame:CGRectMake(15, IPHONE_5?92:48, SCREEN_SIZE.width-30,  SCREEN_SIZE.height-200)];
    //
    //[bgImageview setImage:[UIImage imageNamed:@"terms_use_popup.png"]];
    //
    bgImageview.layer.cornerRadius=3;
    bgImageview.layer.borderColor=[UIColor blackColor].CGColor;
    bgImageview.backgroundColor=[UIColor whiteColor];
    
    [termConditionView addSubview:bgImageview];
    
    webview = [[UIWebView alloc] initWithFrame:CGRectMake(2, 40, bgImageview.frame.size.width-4
                                                          , bgImageview.frame.size.height-45)];
    
    [webview setUserInteractionEnabled:YES];
    [webview setDelegate:self];
    webview.scalesPageToFit = YES;
    
    [webview setBackgroundColor:[UIColor clearColor]];
    [bgImageview addSubview:webview];

    NSError *error = nil;
    NSString *html = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"artevue_t&c" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    [webview loadHTMLString:html baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
        
    NSLog(@"error %@",error);
 
    
    
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(webview.frame.origin.x+15, -5 ,150, 50)];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    titleLabel.numberOfLines = 0;
    [titleLabel setText:title];
    [titleLabel setFont:[UIFont fontWithName:@"AzoSans-Regular" size:18.0f]];
    [bgImageview addSubview:titleLabel];
    
    
    UIButton *cancleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [cancleButton setBackgroundColor:[UIColor blackColor]];
    [cancleButton setImage:[UIImage imageNamed:@"blackCloseButton"] forState:UIControlStateNormal];
    [cancleButton setFrame:CGRectMake(bgImageview.frame.size.width - 60,-5, 50, 50)];
    [cancleButton addTarget:self
                     action:@selector(cancleAction:)
           forControlEvents:UIControlEventTouchUpInside];
    [bgImageview addSubview:cancleButton];
    
    [self openPopupAnimation];
}

-(IBAction)cancleAction:(id)sender{
    
    [self closePopupAnimation];
    
}

-(void)openPopupAnimation{
    [UIView animateWithDuration:0.6f animations:^{
        [termConditionView setFrame:popupGoToFrame];
        
    } completion:^(BOOL finished) {
        
    }];
    
}

-(void)closePopupAnimation{
    [UIView animateWithDuration:0.6f animations:^{
        [termConditionView setFrame:popupGoFromFrame];
    } completion:^(BOOL finished) {
        self.settingTableView.allowsSelection=YES;
        
    }];
}

- (IBAction)flowButtonAction:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://flowdigitalmedia.com/"]];

    
}

- (IBAction)backButtonAction:(id)sender {
   
    [self.navigationController popViewControllerAnimated:YES];
    
}


@end
