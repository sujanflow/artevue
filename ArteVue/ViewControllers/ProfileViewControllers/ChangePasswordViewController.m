//
//  ChangePasswordViewController.m
//  Artegrams
//
//  Created by Sujan on 8/17/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "Constants.h"
#import "UserAccount.h"
#import "ServerManager.h"

#import "DGActivityIndicatorView.h"

@interface ChangePasswordViewController ()

@property (nonatomic) DGActivityIndicatorView *activityIndicatorView;



@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)DGActivityIndicatorAnimationTypeTriplePulse tintColor:[UIColor blackColor]];
    CGFloat width = self.view.bounds.size.width / 5.0f;
    CGFloat height = self.view.bounds.size.width / 5.0f;
    
    //self.activityIndicatorView.backgroundColor=[UIColor redColor];
    self.activityIndicatorView.frame = CGRectMake(self.view.bounds.size.width/2-width/2,self.view.bounds.size.height/2-height/2, width, height);
    [self.view addSubview:self.activityIndicatorView];
    
    
    self.oldPasswordTextField.delegate = self;
    self.passwordTextField.delegate = self;
    self.conFirmPassTestField.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.oldPasswordTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.conFirmPassTestField resignFirstResponder];
    
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{

    if ([textField isEqual:self.passwordTextField]) {
        
        if (self.passwordTextField.text.length > 0) {
            
            NSString *str = self.passwordTextField.text;
            if (str.length < 8) {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                message:@"Passwords must be between 8 and 16 characters long"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }else if (!([str rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]].location != NSNotFound))
            {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                message:@"Passwords must contain at least one number"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                
                
            }
        }

    }else if ([textField isEqual:self.conFirmPassTestField]){
    
    
        if (![self.passwordTextField.text isEqualToString:self.conFirmPassTestField.text]) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"Passwords did not match."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
    
    
    
    
    }

    return YES;

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self.oldPasswordTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.conFirmPassTestField resignFirstResponder];
    
    return YES;
}

- (IBAction)doneButtonAction:(id)sender {
    
    
    if(self.oldPasswordTextField.text.length && self.passwordTextField.text.length && self.conFirmPassTestField.text.length && [self.passwordTextField.text isEqualToString:self.conFirmPassTestField.text]){
        [self.activityIndicatorView startAnimating];
        
        [[ServerManager sharedManager] updatePasswordForOld:self.oldPasswordTextField.text withNew:self.conFirmPassTestField.text withCompletion:^(BOOL success) {
            
            [self.activityIndicatorView stopAnimating];
            
            if (success) {
                [self.navigationController popViewControllerAnimated:YES];
                
                
            }
            else{
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                                message:@"Your old password didn't match our record"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                
            }
            
        }];
        
    }
   
}


- (IBAction)backButtonAction:(id)sender {
    
    if (self.isFromSetting) {
        
        [self.navigationController popViewControllerAnimated:YES];
    }else
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

@end
