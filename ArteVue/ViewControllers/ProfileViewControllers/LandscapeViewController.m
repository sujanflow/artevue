//
//  LandscapeViewController.m
//  Artegrams
//
//  Created by Tanvir Palash on 8/21/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "LandscapeViewController.h"
#import "AppDelegate.h"

#import "Constants.h"
#import <Realm/Realm.h>
#import "Posts.h"

@interface LandscapeViewController ()
{
    RLMRealmConfiguration *realmv1Configuration;
    NSMutableArray *downloadingIds;
}

@end

@implementation LandscapeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //self.hidesBottomBarWhenPushed=YES;
    
   // AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
   // [appDelegate setShouldRotate:YES]; // or NO to disable rotation
    
    [[NSFileManager defaultManager] removeItemAtURL:[RLMRealmConfiguration defaultConfiguration].fileURL error:nil];
    
    NSURL *defaultRealmURL = [RLMRealmConfiguration defaultConfiguration].fileURL;
    NSURL *defaultRealmParentURL = [defaultRealmURL URLByDeletingLastPathComponent];
    NSURL *realmv1URL = [defaultRealmParentURL URLByAppendingPathComponent:@"defaultv1.realm"];
    
    realmv1Configuration = [[RLMRealmConfiguration defaultConfiguration] copy];
    realmv1Configuration.fileURL = realmv1URL;
    
    downloadingIds=[[NSMutableArray alloc] init];
   
//    [[NSNotificationCenter defaultCenter]
//     addObserver:self
//     selector:@selector(deviceOrientationDidChangeNotification:)
//     name:UIDeviceOrientationDidChangeNotification
//     object:nil];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    // NSLog(@"self %@",self.view);
    [self setupCarousel];
}

-(void)setupCarousel
{
    self.carouselView.delegate = self;
    self.carouselView.datasource = self;
    self.carouselView.itemMargin = 10;

}

-(NSInteger)numberOfItemsInCarousel:(TGLParallaxCarousel *)carousel
{
    
    if(self.imageDetailsArray.count<=30)
        return self.imageDetailsArray.count;
    else
    //    NSMutableArray* smallArray = [[NSMutableArray alloc] initWithArray:[imageDetailsArray subarrayWithRange:NSMakeRange(0, 29)]];
        return 30;
}

-(TGLParallaxCarouselItem *)viewForItemAtIndex:(NSInteger)index carousel:(TGLParallaxCarousel *)carousel
{
    TGLCustomView* customView=[[TGLCustomView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.height-90,self.view.frame.size.height-90) number:[NSString stringWithFormat:@"%@",[[ self.imageDetailsArray objectAtIndex:index] objectForKey:@"artist"]]];
    
    //[customView.containerImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[ self.imageDetailsArray objectAtIndex:index] objectForKey:@"image"]]] placeholderImage:[UIImage imageNamed:@"logoWhite"]];
    
    
    RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
    
    RLMResults *result=[Posts objectsInRealm:realm where:[NSString stringWithFormat:@"postId = %@",[[ self.imageDetailsArray objectAtIndex:index] objectForKey:@"id"]]];
    
    if(result.count)
    {
        Posts *post=[[Posts alloc] init];
        post=[result objectAtIndex:0];
       // NSLog(@"result %@",result);
        
        
        [UIView transitionWithView:customView.containerImage duration:0.5f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            customView.containerImage.image=[UIImage imageWithData:post.imageData];
        } completion:^(BOOL finished) {
            ;
        }];
        
    }
    else
    {
        //customView.containerImage.image=nil;
        customView.containerImage.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"square_image" ofType:@"png"]];
        
        if([downloadingIds containsObject: [[ self.imageDetailsArray objectAtIndex:index] objectForKey:@"id"]])
        {
            //do nothing
        }
        else
        {
            
            [downloadingIds addObject:[[ self.imageDetailsArray objectAtIndex:index] objectForKey:@"id"]];
            
            SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
            [SDWebImageDownloader sharedDownloader].maxConcurrentDownloads=1;
            
            [downloader downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[ self.imageDetailsArray objectAtIndex:index] objectForKey:@"image"] ]]
                                     options:0
                                    progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                        // progression tracking code
                                        // NSLog(@"receivedSize %ld expectedSize %ld",(long)receivedSize,(long)expectedSize);
                                        
                                    }
                                   completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                       if (image && finished) {
                                           // do something with image
                                           
                                           //   Posts *post = [[Posts alloc] init];
                                           
                                           
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               //  cell.feedImage=(UIImageView*) [cell viewWithTag:indexPath.section] ;
                                               //  cell.feedImage.image=image;
                                               
                                               NSLog(@"done for %@",[[ self.imageDetailsArray objectAtIndex:index] objectForKey:@"id"]);
                                               
                                               //RLMRealm *realm = [RLMRealm defaultRealm];
                                               
                                               RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
                                               
                                               
                                               [realm beginWriteTransaction];
                                               [Posts createOrUpdateInRealm:realm withValue:@{@"primId": [NSString stringWithFormat:@"%@",[[ self.imageDetailsArray objectAtIndex:index] objectForKey:@"id"]]
                                                                                              ,@"postId": [NSNumber numberWithInt:[[[ self.imageDetailsArray objectAtIndex:index] objectForKey:@"id"] intValue]],
                                                                                              @"imageData": data}];
                                               
                                               [realm commitWriteTransaction];
                                               
                                               [downloadingIds removeObject:[[ self.imageDetailsArray objectAtIndex:index] objectForKey:@"id"]];
                                              
                                               
                                              // [self.carouselView reloadData];
                                               
                                               [UIView transitionWithView:customView.containerImage duration:0.5f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                                                   customView.containerImage.image=[UIImage imageWithData:data];
                                               } completion:^(BOOL finished) {
                                                   ;
                                               }];

                                               
                                               //[self.feedTableView beginUpdates];
                                               //[self.feedTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                                               //[self.feedTableView endUpdates];
                                               
                                           });
                                           
                                       }
                                       
                                   }];
            
        }
        
    }

    
    
   // NSLog(@"customView %@",customView);
    
    return customView;
}


-(void)didTapOnItemAtIndex:(NSInteger)index carousel:(TGLParallaxCarousel *)carousel
{
    NSLog(@"tap at index %li",(long)index);
}

-(void)didMovetoPageAtIndex:(NSInteger)index
{
    NSLog(@"move to index %li",(long)index);
}

//- (void)deviceOrientationDidChangeNotification:(NSNotification*)note
//{
//    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
//
//    if(orientation==1 || orientation ==2 )
//    {
//        
//        //[self.navigationController popViewControllerAnimated:NO];
//        [self.carouselView.timer invalidate];
//        [self dismissViewControllerAnimated:NO completion:nil];
//        
//    }
//}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator
{
    
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        
        // Stuff you used to do in willRotateToInterfaceOrientation would go here.
        // If you don't need anything special, you can set this block to nil.
        
        if (size.width > size.height)
        {
            NSLog(@"in landscape");
            
        }
        else
        {
            // Position elements for Portrait
            NSLog(@"in Portrait");
            
            downloadingIds=[[NSMutableArray alloc] init];
            [[SDWebImageDownloader sharedDownloader] cancelAllDownloads];
//
            [self.carouselView.timer invalidate];
            [self dismissViewControllerAnimated:NO completion:nil];
            
        }
        
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        
        // Stuff you used to do in didRotateFromInterfaceOrientation would go here.
        // If not needed, set to nil.
        
    }];
    
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
}

//-(void)dealloc
//{
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
