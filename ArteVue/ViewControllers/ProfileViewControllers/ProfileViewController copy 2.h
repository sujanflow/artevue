//
//  ProfileViewController.h
//  Artegram
//
//  Created by Tanvir Palash on 6/10/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"
#import "TOCropViewController.h"

#import "Pusher.h"

@interface ProfileViewController : UIViewController<UIScrollViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate,TOCropViewDelegate,PTPusherDelegate,UIActionSheetDelegate,SDWebImageManagerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *containerScrollView;

@property (weak, nonatomic) IBOutlet UIImageView *profilePicture;

@property (weak, nonatomic) IBOutlet UICollectionView *followersCountCollectionView;

@property (weak, nonatomic) IBOutlet UIView *followButtonView;

@property (weak, nonatomic) IBOutlet UIView *editButtonView;

@property (weak, nonatomic) IBOutlet UIButton *editProfileButton;

@property (weak, nonatomic) IBOutlet UIButton *followButton;
@property (weak, nonatomic) IBOutlet UIButton *refreshButton;

@property (weak, nonatomic) IBOutlet UIButton *downLoadButton;

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *userBioLabel;

@property (weak, nonatomic) IBOutlet UILabel *profileNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *profileNameLabelMain;
@property (weak, nonatomic) IBOutlet UILabel *userTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *webSiteUrlLabel;

@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (weak, nonatomic) IBOutlet UIButton *cameraButton;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewOfPosts;
@property (weak, nonatomic) IBOutlet UIView *viewForUserDetails;
//@property (weak, nonatomic) IBOutlet UIView *viewForMapView;
@property (weak, nonatomic) IBOutlet UIView *viewForUserPhotos;
@property (weak, nonatomic) IBOutlet UIView *viewForEvents;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewForPostHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewForMapHeight;

@property (weak, nonatomic) IBOutlet UIView *selectedBarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectedTabXPostiotion;

@property  int selectedTab;

@property BOOL isUserOrFollower;//if Follower
@property int userId;
@property int currentUserId;
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;
@property (weak, nonatomic) IBOutlet UIButton *optionButton;
@property (weak, nonatomic) IBOutlet UIButton *otherOptionButton;


@property (weak, nonatomic) IBOutlet UIButton *SecondTabButton;
@property (weak, nonatomic) IBOutlet UILabel *noPostLabel;
@property (weak, nonatomic) IBOutlet UILabel *privateAccountMsg;

@end
