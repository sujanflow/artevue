//
//  PersonalGalleryViewController.m
//  Artegram
//
//  Created by Sujan on 6/26/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//
#import "AppDelegate.h"
#import "PersonalGalleryViewController.h"
#import "ViewPostViewController.h"
#import "LandscapeViewController.h"
#import "Constants.h"
#import "HexColors.h"
#import "UserAccount.h"
#import "ServerManager.h"

#import "DGActivityIndicatorView.h"

#import "NSArray+NullReplacement.h"
#import "NSDictionary+NullReplacement.h"

#import <Realm/Realm.h>
#import "Posts.h"


@interface PersonalGalleryViewController ()
{
    //NSMutableArray<NSDictionary *> *_cellColors;
    NSMutableArray *galleryImages;
    
    NSMutableArray *selectedImages;
    LandscapeViewController* childViewController;
    
    RLMRealmConfiguration *realmv1Configuration;
    NSMutableArray *downloadingIds;
    
    int pageIndex;
}
@property (nonatomic) DGActivityIndicatorView *activityIndicatorView;

@end


@implementation PersonalGalleryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    pageIndex=1;
   // [self.galleryDetails setContentOffset:CGPointZero animated:NO];
    [self.galleryDetails scrollRangeToVisible:(NSMakeRange(0, 0))];
    
    
    self.activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)DGActivityIndicatorAnimationTypeTriplePulse tintColor:[UIColor blackColor]];
    CGFloat width = self.view.bounds.size.width / 5.0f;
    CGFloat height = self.view.bounds.size.width / 5.0f;
    
    self.activityIndicatorView.frame = CGRectMake(self.view.bounds.size.width/2-width/2,self.view.bounds.size.height/2-height/2, width, height);
    [self.view addSubview:self.activityIndicatorView];
    
   
    [[NSFileManager defaultManager] removeItemAtURL:[RLMRealmConfiguration defaultConfiguration].fileURL error:nil];
    
    NSURL *defaultRealmURL = [RLMRealmConfiguration defaultConfiguration].fileURL;
    NSURL *defaultRealmParentURL = [defaultRealmURL URLByDeletingLastPathComponent];
    NSURL *realmv1URL = [defaultRealmParentURL URLByAppendingPathComponent:@"defaultv1.realm"];
    
    realmv1Configuration = [[RLMRealmConfiguration defaultConfiguration] copy];
    realmv1Configuration.fileURL = realmv1URL;
    
    downloadingIds=[[NSMutableArray alloc] init];
    
//    [[NSNotificationCenter defaultCenter]
//     addObserver:self
//     selector:@selector(deviceOrientationDidChangeNotification:)
//     name:UIDeviceOrientationDidChangeNotification
//     object:nil];
    
    
    self.galleryName.hidden=YES;
    self.galleryDetails.hidden=YES;
    
//
    
   //NSLog(@"self.galleryName.text %@ %@",self.galleryName.text,self.galleryDetails.text);
    
    if(!self.userId)
    {
        self.userId=[UserAccount sharedManager].userId;
        
        if([UserAccount sharedManager].galleryTitle.length<=0 || [[UserAccount sharedManager].galleryTitle isEqual:[NSNull null]])
            
            self.galleryNameTitle=self.galleryName.text;
        else
        {
            self.galleryName.text=[UserAccount sharedManager].galleryTitle;
        }
        
        if([UserAccount sharedManager].galleryDescription.length<=0 || [[UserAccount sharedManager].galleryDescription isEqual:[NSNull null]])
            self.galleryDetailsText=self.galleryDetails.text;
        else
            self.galleryDetails.text=[UserAccount sharedManager].galleryDescription;
        
    }
    else
    {
        if(self.galleryNameTitle.length<=0)
            self.galleryNameTitle=self.galleryName.text;
        else
            self.galleryName.text=self.galleryNameTitle;
        
        if(self.galleryDetailsText.length<=0)
            self.galleryDetailsText=self.galleryDetails.text;
        else
            self.galleryDetails.text=self.galleryDetailsText;
        
    }
    
    if(self.userId != [UserAccount sharedManager].userId)
    {
        self.isSelectble=NO;
        self.pdfButton.hidden=YES;
        //self.galleryCollectionView.allowsSelection=NO;
        self.galleryDetails.userInteractionEnabled=YES;
        self.galleryName.userInteractionEnabled=NO;
        self.descriptionPenButton.hidden=YES;
        
        [self.backButton setHidden:NO];
    }
    else{
        
        self.galleryDetails.userInteractionEnabled=YES;
        self.galleryName.userInteractionEnabled=NO;
        
        [self.backButton setHidden:YES];
      //  NSLog(@"self.galleryName.text %@ %@",self.galleryNameTitle,self.galleryDetailsText);
        
    }
    
    self.galleryName.delegate=self;
    self.galleryDetails.delegate=self;
    
 //   UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
//    self.galleryCollectionView.gestureRecognizers = @[tapRecognizer];

    //[self.galleryCollectionView addGestureRecognizer:tapRecognizer];
    [self configCollectionView];
   
    //[self.galleryCollectionView registerClass:[DTCollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];

  //  [SDWebImageManager sharedManager].delegate=self;
    
    
//    self.galleryCollectionView.refreshControl=[[UIRefreshControl alloc] init];
//    self.galleryCollectionView.refreshControl.backgroundColor=[UIColor whiteColor];
//    self.galleryCollectionView.refreshControl.tintColor=[UIColor grayColor];
//    [self.galleryCollectionView.refreshControl addTarget:self action:@selector(loadImages) forControlEvents:UIControlEventValueChanged];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor=[UIColor whiteColor];
    self.refreshControl.tintColor=[UIColor grayColor];
    [self.refreshControl addTarget:self action:@selector(loadImages) forControlEvents:UIControlEventValueChanged];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 10.0) {
        
        NSLog(@"below ios 10");
        self.galleryCollectionView.backgroundView = self.refreshControl;
        
        
    } else {
        
        NSLog(@"above ios 10");
        self.galleryCollectionView.refreshControl = self.refreshControl;
    }

}

-(void)viewDidAppear:(BOOL)animated
{
    
    NSLog(@"user id :%d",self.userId);
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate setShouldRotate:NO];
    
    selectedImages=[[NSMutableArray alloc] init];
    [self loadImages];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isInGallery"];
    
}



-(void)viewDidDisappear:(BOOL)animated
{
//    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
//    [appDelegate setShouldRotate:NO]; // or NO to disable rotation
//    
    if(self.userId == [UserAccount sharedManager].userId)
    {
        [self updateIndex];

    }
    else
    {
         galleryImages=nil;

    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    
    NSLog(@"viewWillDisappear from gallery");
    downloadingIds=[[NSMutableArray alloc] init];
    [[SDWebImageDownloader sharedDownloader] cancelAllDownloads];
    
}

- (void)viewDidLayoutSubviews {
    [self.galleryDetails setContentOffset:CGPointZero animated:NO];
}

- (void)reloadData
{
    // Reload table data
    [self.galleryCollectionView reloadData];
    
    // End the refreshing
    if (self.refreshControl) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
        
        [self.refreshControl endRefreshing];
    }
    
}

-(void)configCollectionView
{

//    NSString *path = [[NSBundle mainBundle] pathForResource:@"CellData" ofType:@"plist"];
//    NSArray *array = [[NSArray alloc] initWithContentsOfFile:path];
//    
//    NSComparisonResult (^comparator) (id, id) = ^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
//        NSNumber *number1 = obj1[@"Sort"];
//        NSNumber *number2 = obj2[@"Sort"];
//        
//        return [number1 compare:number2];
//    };
//    
//    NSArray *sortedArray = [array sortedArrayUsingComparator:comparator];
//    
//    _cellColors = [[NSMutableArray alloc] initWithArray:sortedArray];
    
    DTCollectionViewCellMover *cellMover = [DTCollectionViewCellMover cellMoverWithDelegate:self];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    
    [self.galleryCollectionView setCellMover:cellMover];
    [self.galleryCollectionView setContentInset:contentInsets];
    [self.galleryCollectionView setDataSource:self];
    [self.galleryCollectionView setDelegate:self];
    
   
}

-(void) loadImages
{
    
    [self.activityIndicatorView startAnimating];

    [[ServerManager sharedManager] getGalleryOfUser:self.userId inPage:pageIndex WithCompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
        if ( resultDataArray!=nil) {
            
            NSMutableDictionary *responsedic= [[resultDataArray dictionaryByReplacingNullsWithBlanks] mutableCopy];
           
            galleryImages=[[NSMutableArray alloc] init];
            galleryImages=[[NSMutableArray alloc ] initWithArray:[responsedic objectForKey:@"data"]];
            
            NSLog(@"galleryImages %@",galleryImages);
            
            self.galleryName.hidden=NO;
            self.galleryDetails.hidden=NO;
           // self.galleryName.text=[[galleryInfo objectForKey:@"gallery_info"] objectForKey:@"gallery_name"];
           // self.galleryDetails.text=[[galleryInfo objectForKey:@"gallery_info"] objectForKey:@"gallery_description"];
            
            
            if(self.galleryName.text.length<=0)
                self.galleryName.text= self.galleryNameTitle;
            else
                self.galleryNameTitle=self.galleryName.text;
            
            if(self.galleryDetails.text.length<=0)
                self.galleryDetails.text= self.galleryDetailsText;
            else
                self.galleryDetailsText=self.galleryDetails.text;
            
            [self adjustFrames];
            
            //  NSLog(@"self.galleryName.text %@ %@",self.galleryName.text,self.galleryDetails.text);
            
            
            [self.activityIndicatorView stopAnimating];
            //[self.galleryCollectionView reloadData];
            
            if(galleryImages.count<=0 && (!self.userId || self.userId ==[UserAccount sharedManager].userId ))
            {
                
                self.noPostLabel.hidden=NO;
                
            }
            else
                self.noPostLabel.hidden=YES;
            
            
            [self reloadData];
            // AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
            // [appDelegate setShouldRotate:YES]; // or NO to disable rotation
            
            if (self.isViewLoaded && self.view.window) {
                NSLog(@"gallery is visible");
                AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                [appDelegate setShouldRotate:YES]; // or NO to disable rotation
                
            }else
            {
                NSLog(@"gallery Not visible");
                
                AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                [appDelegate setShouldRotate:NO]; // or NO to disable rotation
                
            }

            
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[UIAlertView alloc] initWithTitle:@"Sorry, unable to fetch data. Please try again." message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Refresh", nil] show];
                
                [self reloadData];
                
                [self.activityIndicatorView stopAnimating];
                
            
                
            });
            
        }
        
    }];


}
-(void)updateIndex
{
   
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    
    NSMutableOrderedSet *set1 = [NSMutableOrderedSet orderedSetWithArray:galleryImages];
    NSArray *resultArray = [set1 array];
    
    
    [postData setObject:resultArray forKey:@"posts"];
    NSLog(@"postData %@",postData);
    [[ServerManager sharedManager] postGallerySequenceWithDetails:postData withCompletion:^(BOOL success) {
        
        if (success) {
            
             galleryImages=nil;
        }
        else{
            
            galleryImages=nil;
         
        }
        
    }];
    
    

}

-(void) sendDataToGeneratePdf
{
    
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    [postData setObject:self.galleryName.text forKey:@"gallery_name"];
    [postData setObject:self.galleryDetails.text forKey:@"gallery_description"];
    
    NSMutableOrderedSet *set1 = [NSMutableOrderedSet orderedSetWithArray: galleryImages];
    NSOrderedSet *set2 = [NSOrderedSet orderedSetWithArray: selectedImages];
    [set1 intersectOrderedSet: set2];
    NSArray *resultArray = [set1 array];
    
    if(resultArray.count>30)
    {
        NSMutableArray* smallArray = [[NSMutableArray alloc] initWithArray:[resultArray subarrayWithRange:NSMakeRange(0, 29)]];
        [postData setObject:smallArray forKey:@"posts"];
        
    }else
        [postData setObject:resultArray forKey:@"posts"];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:@"The generated PDF will be sent to the user email address shortly"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    self.isSelectble=NO;
    selectedImages=[[NSMutableArray alloc] init];
    
    
    if(!galleryImages.count)
    {
        [self loadImages];
    }
    else
        [self.galleryCollectionView reloadData];
    self.view.userInteractionEnabled=YES;
    
    
    self.pdfButton.hidden=NO;
    self.pdfGenerateButton.hidden=YES;
    self.closeButton.hidden=YES;
    self.doneButton.hidden=YES;
    [self.pdfGenerateButton setTitle:@"Create PDF" forState:UIControlStateNormal];
    
    NSLog(@"postData %@",postData);
    [[ServerManager sharedManager] postCreatePDfWithDetails:postData withCompletion:^(BOOL success) {
        
        if (success) {
           
        }
        else{
            
           
            [self.pdfGenerateButton setTitle:@"Create PDF" forState:UIControlStateNormal];
            
            self.view.userInteractionEnabled=YES;

            
        }
        
    }];

    
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
   // NSLog(@"galleryImages.count %lu",(unsigned long)galleryImages.count);
    return galleryImages.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //NSString *cellIdentifier = [DTCollectionViewCell cellIdentifier];
    NSString *cellIdentifier = @"Cell";
    
    NSInteger index = indexPath.item;
  
    DTCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
   
    
    
    
    if(self.userId != [UserAccount sharedManager].userId || [[[[galleryImages objectAtIndex:index] objectForKey:@"owner"]objectForKey:@"id"] intValue] != [UserAccount sharedManager].userId)
    {
        //NSLog(@"self.userId %d",self.userId);
        cell.lockButton.hidden=YES;
        
        for (UIView *lbl in cell.contentView.subviews)
        {
            if (![lbl isEqual:cell.cellImage] && ![lbl isEqual:cell.lockButton] && ![lbl isEqual:cell.activityIndicator] && ![lbl isEqual:cell.dollerButton])
            {
                [lbl removeFromSuperview];
            }
        }

    }else
    {
       
            cell.lockButton.tag=index;
            cell.lockButton.hidden=NO;
            
            if([[[galleryImages objectAtIndex:index] objectForKey:@"is_locked"] intValue]==1)
            {
                
                cell.lockButton.selected=YES;
                for (UIView *lbl in cell.contentView.subviews)
                {
                    if (![lbl isEqual:cell.cellImage] && ![lbl isEqual:cell.lockButton] && ![lbl isEqual:cell.activityIndicator] && ![lbl isEqual:cell.dollerButton])
                    {
                        [lbl removeFromSuperview];
                    }
                }
                
                [cell lockCell];
                
                
                
            }else
            {
                cell.lockButton.selected=NO;
                
                for (UIView *lbl in cell.contentView.subviews)
                {
                    if (![lbl isEqual:cell.cellImage] && ![lbl isEqual:cell.lockButton] && ![lbl isEqual:cell.activityIndicator] && ![lbl isEqual:cell.dollerButton])
                    {
                        [lbl removeFromSuperview];
                    }
                }
                
            }
        
    }
    
   
    
    cell.activityIndicator.hidden=NO;
    [cell.activityIndicator startAnimating];
    
//    [cell.cellImage setShowActivityIndicatorView:YES];
//    [cell.cellImage setIndicatorStyle:UIActivityIndicatorViewStyleGray];
//    
    
    //[cell.cellImage  sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[galleryImages objectAtIndex:index] objectForKey:@"image"]]]];
    
    
    if(galleryImages.count)
    {
        RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
        
       // NSLog(@"query for %@",[[galleryImages objectAtIndex:index] objectForKey:@"id"]);
        
        RLMResults *result=[Posts objectsInRealm:realm where:[NSString stringWithFormat:@"postId = %@",[[galleryImages objectAtIndex:index] objectForKey:@"id"]]];
        
        
        if(result.count)
        {
            Posts *post=[[Posts alloc] init];
            post=[result objectAtIndex:0];
         //   NSLog(@"result %@",result);
            
     //       [UIView transitionWithView:cell.cellImage duration:0.5f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                
                [cell.activityIndicator stopAnimating];
                cell.cellImage.image=[UIImage imageWithData:post.imageData];
//            } completion:^(BOOL finished) {
//                ;
//            }];
//            
            
        }
        else
        {
            cell.cellImage.image=nil;
            //cell.cellImage.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"square_image" ofType:@"png"]];
            
            if([downloadingIds containsObject: [[galleryImages objectAtIndex:index] objectForKey:@"id"]])
            {
                //do nothing
            }
            else
            {
                
                [downloadingIds addObject:[[galleryImages objectAtIndex:index] objectForKey:@"id"]];
                
                SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
                [SDWebImageDownloader sharedDownloader].maxConcurrentDownloads=1;
                [downloader downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[galleryImages objectAtIndex:index] objectForKey:@"image"] ]]
                                         options:0
                                        progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                            // progression tracking code
                                            NSLog(@"receivedSize %ld expectedSize %ld",(long)receivedSize,(long)expectedSize);
                                            
                                        }
                                       completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                           if (image && finished) {
                                               // do something with image
                                               
                                               
                                               //                                           dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                                               //                                           dispatch_async(queue, ^{
                                               //                                               // Get new realm and table since we are in a new thread
                                               
                                               
                                               //RLMRealm *realm = [RLMRealm defaultRealm];
                                               
                                               
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   //searchImage.image=image;
                                                   
                                                   
                                                   if(galleryImages.count)
                                                   {
                                                       NSLog(@"done in gallery collection %@",[[galleryImages objectAtIndex:index] objectForKey:@"id"]);
                                                       
                                                       RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
                                                       
                                                       [realm beginWriteTransaction];
                                                       //                                                   [Posts createOrUpdateInRealm:realm withValue:@{@"postId": [NSNumber numberWithInt:[[[discoverPost objectAtIndex:indexPath.row] objectForKey:@"id"] intValue]],
                                                       //                                                                                          @"imageData": data}];
                                                       //
                                                       [Posts createOrUpdateInRealm:realm withValue:@{@"primId": [NSString stringWithFormat:@"%@",[[galleryImages objectAtIndex:index] objectForKey:@"id"]]
                                                                                                      ,@"postId": [NSNumber numberWithInt:[[[galleryImages objectAtIndex:index] objectForKey:@"id"] intValue]],
                                                                                                      @"imageData": data}];
                                                       
                                                       
                                                       
                                                       [realm commitWriteTransaction];
                                                       
                                                       [downloadingIds removeObject:[[galleryImages objectAtIndex:index] objectForKey:@"id"]];
                                                       [self.galleryCollectionView reloadItemsAtIndexPaths:@[indexPath]];
                                                       
                                                   }
                                                   //      });
                                                   
                                               });
                                           }
                                           
                                           
                                       }];
                
            }
            
        }

    }
    
    
    
    if (self.isSelectble) {
        if([selectedImages containsObject:[galleryImages objectAtIndex:index]]){
            
            for (UIView *lbl in cell.contentView.subviews)
            {
                if (![lbl isEqual:cell.cellImage] && ![lbl isEqual:cell.lockButton] && ![lbl isEqual:cell.activityIndicator] && ![lbl isEqual:cell.dollerButton])
                {
                    [lbl removeFromSuperview];
                }
            }
            
            [cell highlightCell];
            
            
        }else
        {
            for (UIView *lbl in cell.contentView.subviews)
            {
                if (![lbl isEqual:cell.cellImage] && ![lbl isEqual:cell.lockButton] && ![lbl isEqual:cell.activityIndicator] && ![lbl isEqual:cell.dollerButton])
                {
                    [lbl removeFromSuperview];
                }
            }
            
        }

    }
    //NSLog(@"self lockbutton %@",[[galleryImages objectAtIndex:index] objectForKey:@"has_buy_btn"]);
   
    if([[[galleryImages objectAtIndex:index] objectForKey:@"has_buy_btn"] intValue])
    {
        cell.dollerButton.hidden=NO;
    }
    else
    {
        cell.dollerButton.hidden=YES;
    }
    
    cell.lockButton.layer.zPosition=1000;
    cell.dollerButton.layer.zPosition=1000;
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = CGRectGetWidth(collectionView.bounds);
    CGFloat cellSapceWidth = 2.0f;
    
    CGFloat numberOfCellInRow = 3.0f;
    
    CGFloat cellWidth = (width - cellSapceWidth) / numberOfCellInRow ;
    
    //NSLog(@"cellWidth %f",cellWidth);
    
    return CGSizeMake(cellWidth, cellWidth);
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
}

#pragma mark - DTCollectionViewCellMoverDelegate

- (void)cellMover:(DTCollectionViewCellMover *)cellMover willMoveItemFromIndex:(NSInteger)index toIndex:(NSInteger)toIndex;
{
   // [_cellColors moveObjectAtIndex:index toIndex:toIndex];
    
    id object = [galleryImages objectAtIndex:index];
    [galleryImages removeObjectAtIndex:index];
    [galleryImages insertObject:object atIndex:toIndex];
    
   // NSLog(@"galleryImages %@",galleryImages);
    NSLog(@"moving");
    [self.galleryCollectionView reloadData];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"selectedImages.count %lu",(unsigned long)selectedImages.count);
    
    if(self.isSelectble)
    {
//        if (selectedImages.count<=29) {
            if([selectedImages containsObject:[galleryImages objectAtIndex:indexPath.item]]){
                
                [selectedImages removeObject:[galleryImages objectAtIndex:indexPath.item]];
                [self.galleryCollectionView reloadItemsAtIndexPaths:@[indexPath]];
                NSLog(@"removing %@",selectedImages);
                
            }
            else
            {
                if (selectedImages.count<=29)
                {
                    [selectedImages addObject:[galleryImages objectAtIndex:indexPath.item]];
                    [self.galleryCollectionView reloadItemsAtIndexPaths:@[indexPath]];
                    NSLog(@"adding %@",selectedImages);
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                    message:@"Please select a maximum of 30 frames for pdf"
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                    
                    
                }
            }
        
        
        
    }
    else
    {
        DTCollectionViewCell *cell = (DTCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
        
        ViewPostViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewPostViewController"];
        
       // controller.postImage = cell.cellImage.image;
       // controller.singlePostDic = [[NSMutableDictionary alloc] initWithDictionary:[galleryImages objectAtIndex:indexPath.item]];
        controller.userPostArray = galleryImages;
        controller.indexNumber = indexPath.item;
        controller.userId=self.userId;
        
        controller.isFromProfile=NO;
        controller.isFromGallery=YES;
        
        [self.navigationController pushViewController:controller animated:YES];
    }
    //For selecting image
//  NSLog(@"item %ld %@",(long)indexPath.item,[galleryImages objectAtIndex:indexPath.item]);
//
    
    //For View the Image
    
   
    
    
}


- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
//    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
//    
////    UIImageView* iv = [[UIImageView alloc] initWithImage:nil highlightedImage:[UIImage imageNamed:@""]];
////    iv.userInteractionEnabled = NO;
////    [cell addSubview: iv];
//    
//
//    CAGradientLayer *gradient = [CAGradientLayer layer];
//    
//    gradient.frame = cell.bounds;
//    gradient.colors = @[(id)[UIColor clearColor].CGColor, (id)[UIColor blackColor].CGColor];
//    // Here, percentage would be the percentage of the collection view
//    // you wish to blur from the top. This depends on the relative sizes
//    // of your collection view and the header.
//    gradient.locations = @[@0.0, @(100)];
//
//    cell.layer.mask = gradient;
    
//     NSLog(@"cell %@",cell);
//        if([selectedImages containsObject:[galleryImages objectAtIndex:indexPath.item]]){
//    
//            [self deselectSelectedImageFromIndexpath:indexPath];
//    
//    
//        }
//        else{
//            [self selectImageAtIndexPath:indexPath];
//    
//            }
}


-(void)selectImageAtIndexPath:(NSIndexPath* )indexPath
{
    
    [selectedImages addObject:[galleryImages objectAtIndex:indexPath.item]];
    [self.galleryCollectionView reloadItemsAtIndexPaths:@[indexPath]];
    NSLog(@"adding %@",selectedImages);
    
}

-(void)deselectSelectedImageFromIndexpath:(NSIndexPath *)indexPath
{
    [selectedImages removeObject:[galleryImages objectAtIndex:indexPath.item]];
    [self.galleryCollectionView reloadItemsAtIndexPaths:@[indexPath]];
    NSLog(@"removing %@",selectedImages);
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (IBAction)pdfButtonAction:(id)sender {
    
    selectedImages=[[NSMutableArray alloc] init];
    self.isSelectble=YES;
    
    self.galleryName.userInteractionEnabled=NO;
    self.galleryDetails.editable=NO;
    self.galleryDetails.selectable = YES;
    
    self.pdfButton.hidden=YES;
    self.backButton.hidden=YES;
    self.doneButton.hidden=YES;
    self.pdfGenerateButton.hidden=NO;
    self.closeButton.hidden=NO;
    
}
- (IBAction)pdfGenerateButtonAction:(id)sender {
   
    
    if(selectedImages.count){
        [self.pdfGenerateButton setTitle:@"Creating..." forState:UIControlStateNormal];
        self.isSelectble=NO;
        [self sendDataToGeneratePdf];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Please select at least one image for your PDF catalogue"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];

    }
    
}
- (IBAction)doneButtonAction:(id)sender {
    
       [[ServerManager sharedManager] updateGalleryInfo:self.galleryDetails.text withName:self.galleryName.text completion:^(BOOL success) {
            if (success) {
                
                [self.view endEditing:YES];
                
                self.galleryNameTitle=self.galleryName.text;
                self.galleryDetailsText=self.galleryDetails.text;
                
                
                self.galleryName.userInteractionEnabled=NO;
                self.galleryDetails.editable=NO;
                self.galleryDetails.selectable = YES;
                
                self.pdfButton.hidden=NO;
                //self.backButton.hidden=NO;
                self.pdfGenerateButton.hidden=YES;
                self.closeButton.hidden=YES;
                self.doneButton.hidden=YES;
                self.descriptionPenButton.hidden=NO;
                
                [UserAccount sharedManager].galleryTitle=self.galleryName.text;
                [UserAccount sharedManager].galleryDescription=self.galleryDetails.text;
                
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                message:[NSString stringWithFormat:@"Gallery info successfuly updated"]
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];

            }
            else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    self.view.userInteractionEnabled=YES;
                    
                    
                });
            }

        }];
    
}

- (IBAction)lockButtonAction:(UIButton*)sender {
    NSLog(@"sender item index %ld",(long)sender.tag);
    sender.selected=!sender.selected;
    
    NSMutableDictionary *temDic = [[NSMutableDictionary alloc]initWithDictionary:[galleryImages objectAtIndex:sender.tag]];
    [temDic removeObjectForKey:@"is_locked"];
    [temDic setObject:[NSNumber numberWithBool:sender.selected] forKey:@"is_locked"];
    
    [galleryImages replaceObjectAtIndex:sender.tag withObject:temDic];
    [self.galleryCollectionView reloadData];
    
    
    [[ServerManager sharedManager] updatePost:[[[galleryImages objectAtIndex:sender.tag] objectForKey:@"id"] intValue] Locked:sender.selected withCompletion:^(BOOL success)
    {
        
        sender.userInteractionEnabled = YES;
        
        if (success) {

        }
        else{
            
            sender.selected=!sender.selected;
            
            
            NSMutableDictionary *temDic = [[NSMutableDictionary alloc]initWithDictionary:[galleryImages objectAtIndex:sender.tag]];
            [temDic removeObjectForKey:@"is_locked"];
            [temDic setObject:[NSNumber numberWithBool:sender.selected] forKey:@"is_locked"];
            
            [galleryImages replaceObjectAtIndex:sender.tag withObject:temDic];
            [self.galleryCollectionView reloadData];
            
        }
        
    }];

 

}


- (IBAction)closeButtonAction:(id)sender {
    
    selectedImages=[[NSMutableArray alloc] init];
    
    self.isSelectble=NO;
    [self.view endEditing:YES];

    self.galleryName.userInteractionEnabled=NO;
    self.galleryDetails.editable=NO;
  //  self.galleryDetails.selectable = NO;

    
    self.galleryDetails.text=self.galleryDetailsText;
    [self adjustFrames];
    
    self.pdfButton.hidden=NO;
    //self.backButton.hidden=NO;
    self.pdfGenerateButton.hidden=YES;
    self.closeButton.hidden=YES;
    self.doneButton.hidden=YES;
    self.descriptionPenButton.hidden=NO;
    
    selectedImages=[[NSMutableArray alloc] init];
    [self.galleryCollectionView reloadData];
}

-(void)onTap:(UIGestureRecognizer*) recognizer
{
    NSLog(@"on tap");
    [self.view endEditing:YES];
//
    
  
    
    if(self.userId == [UserAccount sharedManager].userId && self.pdfGenerateButton.hidden)
    {
        if(self.galleryDetails.text.length == 0){
            self.galleryDetails.textColor = [UIColor darkGrayColor];
            self.galleryDetails.text = @"Add a personal statement, contact details, describe your art collection or portfolio";
            self.descriptionPenButton.hidden=NO;
            [self.galleryDetails resignFirstResponder];
        }
        
        NSLog(@"self.galleryName.text %@ %@",self.galleryName.text,self.galleryNameTitle);
        NSLog(@"self.galleryName.text %@ %@",self.galleryDetailsText,self.galleryDetails.text);
        
        
                if(![self.galleryNameTitle isEqualToString:self.galleryName.text] || ![self.galleryDetailsText isEqualToString:self.galleryDetails.text] )
                {
                    self.pdfButton.hidden=YES;
                    self.backButton.hidden=YES;
        
                    self.closeButton.hidden=NO;
                    self.doneButton.hidden=NO;
                }
                else
                {
                    self.closeButton.hidden=YES;
                    self.doneButton.hidden=YES;
        
                    self.pdfButton.hidden=NO;
                    //self.backButton.hidden=NO;
                }
                
        
    }

}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
       NSLog(@"touchesBegan");
    [self.view endEditing:YES];
    
    if(self.userId == [UserAccount sharedManager].userId && self.pdfGenerateButton.hidden)
    {
        self.descriptionPenButton.hidden=NO;
        self.galleryDetails.editable=NO;
        self.galleryName.userInteractionEnabled=NO;
        
        if(self.galleryDetails.text.length == 0){
            self.galleryDetails.textColor = [UIColor darkGrayColor];
            self.galleryDetails.text = @"Add a personal statement, contact details, describe your art collection or portfolio";
            [self.galleryDetails resignFirstResponder];
        }
        else
        {
        
            
        }
        
        NSLog(@"self.galleryName.text %@ %@",self.galleryName.text,self.galleryNameTitle);
        NSLog(@"self.galleryName.text %@ %@",self.galleryDetailsText,self.galleryDetails.text);
        
        
        if(![self.galleryNameTitle isEqualToString:self.galleryName.text] || ![self.galleryDetailsText isEqualToString:self.galleryDetails.text] )
        {
            self.pdfButton.hidden=YES;
            self.backButton.hidden=YES;
            
            self.closeButton.hidden=NO;
            self.doneButton.hidden=NO;
        }
        else
        {
            self.closeButton.hidden=YES;
            self.doneButton.hidden=YES;
            
            self.pdfButton.hidden=NO;
          //  self.backButton.hidden=NO;
        }
        
        
    }
    
}

-(void) adjustFrames
{
    
    //get text size
    CGSize constraint = CGSizeMake(self.galleryDetails.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [self.galleryDetails.text boundingRectWithSize:constraint
                                                            options:NSStringDrawingUsesLineFragmentOrigin
                                                         attributes:@{NSFontAttributeName:self.galleryDetails.font}
                                                            context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    CGFloat newHeight = size.height + 30 ;
    
    NSLog(@"newHeight %f",newHeight);
    self.galleryDetailsTextviewHeightConstraint.constant=newHeight;
    
    
//    CGRect textFrame = self.galleryDetails.frame;
//    textFrame.size.height = self.galleryDetails.contentSize.height;
//    self.galleryDetails.frame = textFrame;
//    self.galleryDetailsTextviewHeightConstraint.constant=textFrame.size.height;

    [self.galleryDetails layoutIfNeeded];
}

#pragma mark - TextField delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.descriptionPenButton.hidden=YES;
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= 20 || returnKey;
    
    
}

- (void)textViewDidChangeSelection:(UITextView *)textView {
    
    if(self.userId != [UserAccount sharedManager].userId){
        
       if(NSEqualRanges(textView.selectedRange, NSMakeRange(0, 0)) == NO) {
        textView.selectedRange = NSMakeRange(0, 0);
           
       }
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    [self.galleryName resignFirstResponder];
    
    NSLog(@"self.galleryNameTitle %@",self.galleryNameTitle);
     NSLog(@"self.galleryName.text] %@",self.galleryName.text);
    
    if(![self.galleryNameTitle isEqualToString:self.galleryName.text])
    {
        self.pdfButton.hidden=YES;
        self.backButton.hidden=YES;
        
        self.closeButton.hidden=NO;
        self.doneButton.hidden=NO;
    }
    else
    {
        self.closeButton.hidden=YES;
        self.doneButton.hidden=YES;
        
        self.pdfButton.hidden=NO;
     //   self.backButton.hidden=NO;
    }
    
    
    return YES;
}


#pragma mark - Textview delegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
//    if(![self.galleryDetailsText isEqualToString:self.galleryDetails.text])
//    {
//        self.pdfButton.hidden=YES;
//        self.backButton.hidden=YES;
//        
//        self.closeButton.hidden=NO;
//        self.doneButton.hidden=NO;
//    }
    
}


- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    
    self.descriptionPenButton.hidden=YES;
    if([textView.text isEqualToString:@"Add a personal statement, contact details, describe your art collection or portfolio"])
        textView.text = @"";
        
    textView.textColor = [UIColor blackColor];
        
    
    return YES;
}

-(void) textViewDidEndEditing:(UITextView *)textView
{
    
    self.descriptionPenButton.hidden=NO;
    [textView resignFirstResponder];
    
}
-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    
    return YES;
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    //return textView.text.length + (text.length - range.length) <= 250;
    
    [self adjustFrames];
    
    NSUInteger oldLength = [textView.text length];
    NSUInteger replacementLength = [text length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [text rangeOfString: @"\n"].location != NSNotFound;
    
    NSLog(@"%lu %lu %lu %lu",(unsigned long)oldLength, (unsigned long)replacementLength,(unsigned long)rangeLength,(unsigned long)newLength);
    
    if(newLength<=oldLength)
        return YES;
    else
        return newLength <= 250 || returnKey;
}


- (void)textViewDidChange:(UITextView *)textView{
    
     self.descriptionPenButton.hidden=YES;
//    if(textView.text.length == 0){
//            textView.textColor = [UIColor darkGrayColor];
//            textView.text = @"Display art which you have, like to have, or just want to sell or is part of your gallery exhibition.  Curate your gallery by holding down the tiles and moving them around. Lock your art if you wish to keep it private. Create your own pdf art catalogue, add a description, and then email to friends or clients.";
//            self.descriptionPenButton.hidden=NO;
//            [textView resignFirstResponder];
//    }
    
    if(![self.galleryDetailsText isEqualToString:self.galleryDetails.text])
    {
        self.pdfButton.hidden=YES;
        self.backButton.hidden=YES;
        
        self.closeButton.hidden=NO;
        self.doneButton.hidden=NO;
    }
//    else
//    {
//        self.closeButton.hidden=YES;
//        self.doneButton.hidden=YES;
//        
//        self.pdfButton.hidden=NO;
//       // self.backButton.hidden=NO;
//    }
    
    
}

- (IBAction)editDescriptionButtonAction:(id)sender {
    
    self.galleryName.userInteractionEnabled=YES;
    
    
    [self.galleryDetails becomeFirstResponder];
    self.galleryDetails.editable=YES;
    
    self.pdfButton.hidden=YES;
    self.backButton.hidden=YES;
    
    self.closeButton.hidden=NO;
    self.doneButton.hidden=NO;
    
    self.pdfGenerateButton.hidden=YES;
    
    self.descriptionPenButton.hidden=YES;
}

#pragma mark - orientation delegate
- (void)deviceOrientationDidChangeNotification:(NSNotification*)note
{
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    NSLog(@"orientation %li %lu",(long)orientation,(unsigned long)galleryImages.count);

    
    
    if(galleryImages.count && (orientation==3 || orientation ==4 ))
    {
      
        
//        NSLog(@"landscape");
//
        childViewController =
        childViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LandscapeViewController"];
        [childViewController.view setFrame:CGRectMake(0.0f, 0.0f,[UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width)];
        childViewController.imageDetailsArray=galleryImages;
        NSLog(@"%lf %lf",galleryImages.count,childViewController.imageDetailsArray.count);
        //[self.navigationController pushViewController:childViewController animated:NO];
        
//
        UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:childViewController];
        navCon.navigationBar.hidden=YES;
        
        [self presentViewController:navCon animated:NO completion:nil];
        
        
        
//        [self addChildViewController:childViewController];
//        [self.view addSubview:childViewController.view];
//        [childViewController didMoveToParentViewController:self];
        

//        childViewController.view.hidden=NO;
//        self.tabBarController.tabBar.hidden=YES;
        
    }
}

-(void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator
{
    
    NSLog(@"withTransitionCoordinator user id :%d inview %@",self.userId,[[[[UIApplication sharedApplication] keyWindow] subviews] lastObject]);

    
   

   
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        
        if (self.isViewLoaded && self.view.window) {
            NSLog(@"gallery is visible");
            if (size.width > size.height && galleryImages.count )
            {
                childViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LandscapeViewController"];
                [childViewController.view setFrame:CGRectMake(0.0f, 0.0f,[UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width)];
                childViewController.imageDetailsArray=[[NSMutableArray alloc] initWithArray:galleryImages];
                
                NSLog(@"viewWillTransitionToSize %lu %lu",(unsigned long)galleryImages.count,(unsigned long)childViewController.imageDetailsArray.count);
                
                
                //    UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:childViewController];
                //    navCon.navigationBar.hidden=YES;
                //    [self.navigationController pushViewController:childViewController animated:NO];
                
                //
                [self presentViewController:childViewController animated:NO completion:nil];
            }
            else
            {
                [self.view layoutIfNeeded];
            }
            
        }else
        {
            NSLog(@"gallery Not visible");
           // [self.view layoutIfNeeded];
        }
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        
        // Stuff you used to do in didRotateFromInterfaceOrientation would go here.
        // If not needed, set to nil.
        
    }];
    
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"buttonIndex %li",(long)buttonIndex);
    
    if(buttonIndex == 0)//back button pressed
    {
//        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isInGallery"];
//        [self.navigationController popViewControllerAnimated:YES];
//        
    }
    else if(buttonIndex == 1)//refresh button pressed.
    {
        
        [self viewDidAppear:YES];
    }
    
}

- (IBAction)backButtonAction:(id)sender {
    
    //    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    //    [appDelegate setShouldRotate:NO];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isInGallery"];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//-(UIImage *)imageManager:(SDWebImageManager *)imageManager transformDownloadedImage:(UIImage *)image withURL:(NSURL *)imageURL
//{
//    // NSLog(@"imageURL : %@",imageURL);
//    UIImage *resizedImage;
//    
//    float MIN_UPLOAD_RESOLUTION=756*756;
//    float factor;
//    float resol = image.size.height*image.size.width;
//    if (resol >MIN_UPLOAD_RESOLUTION){
//        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
//        resizedImage = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
//        return resizedImage;
//    }
//    else
//        return image;
//}
//
//- (UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
//   
//    @autoreleasepool {
//        
//    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
//    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
//    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    
//    return scaledImage;
//       
////        scaledImage=nil;
////        NSLog(@"scaledImage %@",scaledImage);
//    }
//}


@end
