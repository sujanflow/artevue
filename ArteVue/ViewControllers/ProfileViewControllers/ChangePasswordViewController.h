//
//  ChangePasswordViewController.h
//  Artegrams
//
//  Created by Sujan on 8/17/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"

@interface ChangePasswordViewController : UIViewController<UITextFieldDelegate>


@property (weak, nonatomic) IBOutlet UITextField *oldPasswordTextField;

@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (weak, nonatomic) IBOutlet UITextField *conFirmPassTestField;
@property (weak, nonatomic) IBOutlet UIButton *sendButtonAction;

@property BOOL isFromSetting;


@end
