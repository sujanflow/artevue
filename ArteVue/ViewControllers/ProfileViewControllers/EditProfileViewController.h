//
//  EditProfileViewController.h
//  Artegrams
//
//  Created by Sujan on 8/3/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"


@interface EditProfileViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate>{

  UIPickerView *userPickerView;
    
}

@property (weak, nonatomic) IBOutlet UITableView *editProfileTableView;




@end
