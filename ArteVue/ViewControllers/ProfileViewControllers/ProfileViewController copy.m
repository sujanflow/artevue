//
//  ProfileViewController.m
//  Artegram
//
//  Created by Tanvir Palash on 6/10/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//
#import "AppDelegate.h"
#import "ProfileViewController.h"

#import "TabBarViewController.h"
#import "PersonalGalleryViewController.h"
#import "EditProfileViewController.h"
#import "ViewPostViewController.h"
#import "LogInViewController.h"
#import "Constants.h"
#import "HexColors.h"
#import "UserAccount.h"
#import "LikersViewController.h"
#import "SettingViewController.h"
#import "PinPostsViewController.h"

#import "NSDictionary+NullReplacement.h"
#import "NSArray+NullReplacement.h"
#import "DGActivityIndicatorView.h"
#import "ActivityListViewController.h"
#import "DemoMessagesViewController.h"

#import "GIBadgeView.h"

#import <Realm/Realm.h>
#import "Posts.h"

@interface ProfileViewController (){


    NSArray* title;
    NSMutableArray *userPosts;
    NSMutableArray *userTagPosts;
    NSMutableDictionary *userInfo;
    UIImage *chosenImage;
    
    PTPusher *pusherClient;
    GIBadgeView* badgeView;
    
    RLMRealmConfiguration *realmv1Configuration;
    NSMutableArray *downloadingIds;

    CGPoint lastScrollViewContentOffset;
    CGFloat requiredCollectionViewSize;
    CGFloat expectedCollectionViewHeight;
}

@property (nonatomic) DGActivityIndicatorView *activityIndicatorView;

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    lastScrollViewContentOffset.y=0;
    self.containerScrollView.delegate=self;
    
    NSLog(@"user id :%d",self.userId);
    
    pusherClient = [PTPusher pusherWithKey:pusher_app_key delegate:self encrypted:YES cluster:@"ap1"];
    
    PTPusherChannel *channel = [pusherClient subscribeToChannelNamed:[NSString stringWithFormat:@"%i-activity-channel",[UserAccount sharedManager].userId]];
    
    [channel bindToEventNamed:@"all-activities" handleWithBlock:^(PTPusherEvent *channelEvent) {
        
        [UserAccount sharedManager].pendingActivityCount++;
        NSLog(@"message received: %i",  [UserAccount sharedManager].pendingActivityCount);
        
        [self updateBadgeValue];
        
    }];
    [pusherClient connect];
   
    
    self.activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)DGActivityIndicatorAnimationTypeTriplePulse tintColor:[UIColor blackColor]];
    CGFloat width = self.view.bounds.size.width / 5.0f;
    CGFloat height = self.view.bounds.size.width / 5.0f;
    
    self.activityIndicatorView.frame = CGRectMake(self.view.bounds.size.width/2-width/2,self.view.bounds.size.height/2-height/2, width, height);
    [self.view addSubview:self.activityIndicatorView];
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBadgeValue) name:@"updateActivityBubble" object:nil];
    
    
    [[NSFileManager defaultManager] removeItemAtURL:[RLMRealmConfiguration defaultConfiguration].fileURL error:nil];
    
    NSURL *defaultRealmURL = [RLMRealmConfiguration defaultConfiguration].fileURL;
    NSURL *defaultRealmParentURL = [defaultRealmURL URLByDeletingLastPathComponent];
    NSURL *realmv1URL = [defaultRealmParentURL URLByAppendingPathComponent:@"defaultv1.realm"];
    
    realmv1Configuration = [[RLMRealmConfiguration defaultConfiguration] copy];
    realmv1Configuration.fileURL = realmv1URL;
    downloadingIds=[[NSMutableArray alloc] init];
    
    
    
    badgeView = [GIBadgeView new];
    
    badgeView.font = [UIFont fontWithName:@"AzoSans-Medium" size:12];
    badgeView.rightOffset = 10.0f;
    badgeView.topOffset=10.0f;
    [badgeView setHidden:YES];
    
    [self.SecondTabButton addSubview:badgeView];
    NSLog(@"badgeView %@",badgeView);
   
    
    //setting corner radius and border width
    
  //  [self.view layoutIfNeeded];
    self.followButton.layer.cornerRadius = self.followButton.width/2;
    self.followButton.layer.borderWidth = 1.0f;
    //self.followButton.layer.borderColor = [[UIColor whiteColor]CGColor];
    self.followButton.layer.borderColor = self.followButton.tintColor.CGColor;
    
    self.downLoadButton.layer.cornerRadius = 3.0;
    self.downLoadButton.layer.borderWidth = 1.1f;
    self.downLoadButton.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    
    self.editProfileButton.layer.cornerRadius = self.editProfileButton.width/2;
   // self.editProfileButton.layer.borderWidth = 1.1f;
   //self.editProfileButton.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    
    [self.view layoutIfNeeded];
    self.profilePicture.layer.cornerRadius = self.profilePicture.width/2;
    //self.profilePicture.layer.borderWidth = 1.1f;
    self.profilePicture.clipsToBounds = YES;
    self.profilePicture.layer.masksToBounds = YES;
    //self.profilePicture.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    
    //setting delegate and datasource
    
    // followersCountCollectionView
    
    self.followersCountCollectionView.delegate = self;
    self.followersCountCollectionView.dataSource = self;
    
       // collectionViewOfPosts
    
    self.collectionViewOfPosts.delegate = self;
    self.collectionViewOfPosts.dataSource = self;
    
    
    UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedOnLink:)];
    // if labelView is not set userInteractionEnabled, you must do so
    [self.webSiteUrlLabel setUserInteractionEnabled:YES];
    [self.webSiteUrlLabel addGestureRecognizer:gesture];

    
    
    //hide views
    
    self.viewForEvents.hidden = YES;
    self.viewForUserPhotos.hidden = YES;
    
    if (self.isUserOrFollower) {
        
        if(self.userId == [UserAccount sharedManager].userId)
        {
            //self.editButtonView.hidden = NO;
            self.editProfileButton.hidden = NO;
            self.followButton.hidden = YES;
            self.backButton.hidden = NO;
            self.optionButton.hidden=NO;
            self.otherOptionButton.hidden = YES;
            [self.SecondTabButton setTitle:@"Activity" forState:UIControlStateNormal];
            [self.SecondTabButton setTitle:@"Activity" forState:UIControlStateSelected];
            
        }else
        {
            self.editProfileButton.hidden = YES;
            self.followButton.hidden = NO;
            self.backButton.hidden = NO;
            self.optionButton.hidden=YES;
            self.otherOptionButton.hidden = NO;
            self.cameraButton.hidden=YES;
            
            [self.SecondTabButton setTitle:@"Gallery" forState:UIControlStateNormal];
            [self.SecondTabButton setTitle:@"Gallery" forState:UIControlStateSelected];
            
        }
        
        
    }else{
        
        [self.SecondTabButton setTitle:@"Activity" forState:UIControlStateNormal];
        [self.SecondTabButton setTitle:@"Activity" forState:UIControlStateSelected];
        self.editButtonView.hidden = NO;
        self.followButtonView.hidden = YES;
        self.otherOptionButton.hidden = YES;
        self.optionButton.hidden=NO;
        self.userId = [UserAccount sharedManager].userId;
    }
    
    
    title = [[NSArray alloc]initWithObjects:@"Posts",@"Pins",@"Followers",@"Following", nil];

    userPosts=[[NSMutableArray alloc] init];
    userInfo = [[NSMutableDictionary alloc]init];
    userTagPosts=[[NSMutableArray alloc] init];
    //temporaty
    
    self.currentUserId = [UserAccount sharedManager].userId;
    
    
    self.selectedTab=1;
    
  //  [SDWebImageManager sharedManager].delegate=self;
    
}

-(void) viewDidAppear:(BOOL)animated
{
    
    self.refreshButton.hidden=YES;
    [self updateBadgeValue];
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate setShouldRotate:NO];
    
    for (int i=101; i<=104; i++) {
        
        UIButton *random1 = (UIButton *)[self.view viewWithTag:i];
        random1.selected=NO;
    }
    
    [self requestUserInfo];
    
    if(self.selectedTab==1)
    {
        UIButton *random = (UIButton *)[self.view viewWithTag:101];
        random.selected=YES;
        
        
        
        
    }else if(self.selectedTab==3)
    {
        UIButton *random = (UIButton *)[self.view viewWithTag:103];
        random.selected=YES;
        
        self.collectionViewOfPosts.hidden = YES;
        self.viewForEvents.hidden = NO;
        self.viewForUserPhotos.hidden = YES;
        self.viewForPostHeight.constant=requiredCollectionViewSize;
        self.containerScrollView.bounces=YES;
        //[self.activityIndicatorView stopAnimating];
        
    }
    else if(self.selectedTab==4)
    {
        UIButton *random = (UIButton *)[self.view viewWithTag:104];
        random.selected=YES;
        
        
        
    }
    
    
    
}


-(void)viewWillDisappear:(BOOL)animated
{
  //  [self.collectionViewOfPosts reloadData];
    downloadingIds=[[NSMutableArray alloc] init];
    [[SDWebImageDownloader sharedDownloader] cancelAllDownloads];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)userTappedOnLink:(UITapGestureRecognizer *)gesture
{
    UILabel* sender=(UILabel*) [gesture view];
    NSLog(@"sender.text %@",sender.text);
    
    UIAlertView *openSafariAlert=[[UIAlertView alloc]initWithTitle:nil message:@"Open Safari" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
   [openSafariAlert show];
    
  

}

-(void) requestUserInfo{
    
    [self.activityIndicatorView startAnimating];
    

    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    [postData setObject:ACCESS_KEY forKey:@"access_key"];
    
    if (self.isUserOrFollower) {
        
        [postData setObject:[NSNumber numberWithInt:self.userId] forKey:@"user_id"];
    }else{
        
        [postData setObject:[NSNumber numberWithInt:self.currentUserId] forKey:@"user_id"];
    }
    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
    
    //NSLog(@"postData %@",postData);
    
    [apiLoginManager POST:[NSString stringWithFormat:@"%@/users/apiGetUserDetail",SERVER_BASE_API_URL] parameters:postData
                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                      
                      
                      userInfo = [[NSMutableDictionary alloc] initWithDictionary:[responseObject dictionaryByReplacingNullsWithBlanks]];
                      //NSLog(@"Response: %@", userInfo);
                      
                      
                      //set data of user

                      self.profileNameLabel.text = [userInfo objectForKey:@"username"];
                      self.profileNameLabelMain.text = [userInfo objectForKey:@"username"];
                      self.userTypeLabel.text = [userInfo objectForKey:@"user_type"];
                      
                      self.userTypeLabel.hidden=NO;
                      
                      if(![[userInfo objectForKey:@"biography"] isEqual:[NSNull null]])
                      {
                          self.userBioLabel.text=[userInfo objectForKey:@"biography"];
                         // self.userBioLabel.text=@"snlbgnbfgvklbv sfbgvasgvlafgvas bskbgvflabsfvjasv skgv ksfb vjlsav svblsabfgvljsabgv vksbfjlvgbslbv skgv ksfb vjlsav svblsabfgvljsabgv vksbfjlvgbslbv";
                          
                      }
                      
                      if(![[userInfo objectForKey:@"website"] isEqual:[NSNull null]])
                      {
                          self.webSiteUrlLabel.text=[userInfo objectForKey:@"website"];
                         // self.webSiteUrlLabel.text=@"www.google.com";
                      }
                      
                      if(chosenImage)
                      {
                          self.profilePicture.image=chosenImage;
                      }else
                      {
                          if ([[userInfo objectForKey:@"profile_picture"] isEqual:[NSNull null]] || [[userInfo objectForKey:@"profile_picture"] isEqualToString:@""]) {
                              
                              [self.profilePicture sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL,@"img/profile-holder.png"]]];
                          }else{
                              
                              
                              [self.profilePicture sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL,[userInfo objectForKey:@"profile_picture"]]]];
                              
                              
                          }
                      }
                      
                     
                   //   [self.profilePicture sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL,[userInfo objectForKey:@"profile_picture"]]]];
                      
                      //set follow button state
                      if([[userInfo objectForKey:@"is_following"] integerValue])
                      {//2DCC70
                          self.followButton.tintColor = [HXColor hx_colorWithHexString:@"2DCC70"];
                          self.followButton.layer.borderColor = [[HXColor hx_colorWithHexString:@"2DCC70"]CGColor];
                          self.followButton.selected=YES;
                          
                          
                          
                      }
                      else{
                          //007AFF
                          self.followButton.tintColor = [HXColor hx_colorWithHexString:@"000000"];
                          self.followButton.layer.borderColor = [[HXColor hx_colorWithHexString:@"000000"]CGColor];
                          self.followButton.selected=NO;
                          
                          
                      }
                      
                      //[userInfo setObject:@"1" forKey:@"is_account_private"];
                      [self.view layoutIfNeeded];
                      requiredCollectionViewSize=ceil([UIScreen mainScreen].bounds.size.height-49-self.collectionViewOfPosts.frame.origin.y-self.viewForUserDetails.frame.size.height-66);
                      
                      
                      self.viewForPostHeight.constant=requiredCollectionViewSize;
                      self.containerScrollView.bounces=YES;
                      NSLog(@"requiredCollectionViewSize %lf",self.viewForUserDetails.frame.size.height);
                      
                      
                      if ([[userInfo objectForKey:@"is_account_private"] integerValue] &&  self.userId != [UserAccount sharedManager].userId && ![[userInfo objectForKey:@"is_following"] integerValue])
                      {
                          [self.activityIndicatorView stopAnimating];
                          self.privateAccountMsg.hidden=NO;
                          
                          self.refreshButton.hidden=YES;
                          
                          
                          
                      }
                      else
                      {
                          
                          self.privateAccountMsg.hidden=YES;
                          
                         if(self.selectedTab==1)
                          {
                              [self makeRequest];
                              
                          }else if ( self.selectedTab==4)
                          {
                              [self makeRequestForTag];
                              
                          }else
                              [self.activityIndicatorView stopAnimating];
                          
                      }

                      
                      [self.followersCountCollectionView reloadData];
                  }
     
                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                      
                      NSLog(@"Error: %@", error);
                      
                      self.view.userInteractionEnabled=YES;
                      self.privateAccountMsg.hidden=YES;
                      
                      self.refreshButton.hidden=NO;
                      [self.activityIndicatorView stopAnimating];
                      
                  }];


}

-(void) makeRequest {

    
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    [postData setObject:ACCESS_KEY forKey:@"access_key"];
    if (self.isUserOrFollower) {
        [postData setObject:[NSNumber numberWithInt:self.userId] forKey:@"user_id"];
         [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
    }else{
        [postData setObject:[NSNumber numberWithInt:self.currentUserId] forKey:@"user_id"];
         [postData setObject:[NSNumber numberWithInt:self.currentUserId] forKey:@"current_user_id"];
    }
    
    
    //NSLog(@"postData %@",postData);
    
    [apiLoginManager POST:[NSString stringWithFormat:@"%@/users/api-get-user-posts",SERVER_BASE_API_URL] parameters:postData
                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                     // NSLog(@"Response: %@", responseObject);
                      
                      //userPosts=responseObject;
                      userPosts=[[responseObject allObjects ] mutableCopy] ;
                      
                      //  userPosts=[[[responseObject reverseObjectEnumerator] allObjects ] mutableCopy];
                      
                      if(self.userId == [UserAccount sharedManager].userId)
                      {
                          if (userPosts.count) {
                              self.noPostLabel.hidden=YES;
                          }
                          else
                          {
                              
                              self.noPostLabel.hidden=NO;
                          }

                      }
                      else
                      {
                              self.noPostLabel.hidden=YES;
                      }
                      
                    //
                      CGFloat postCount=(CGFloat)userPosts.count;
                      expectedCollectionViewHeight=ceil((([UIScreen mainScreen].bounds.size.width-2)/3)*ceil(postCount/3)+floor(postCount/3));
                      
                      if(expectedCollectionViewHeight>([UIScreen mainScreen].bounds.size.height-66-49))
                      {
                          expectedCollectionViewHeight=([UIScreen mainScreen].bounds.size.height-66-49);
                          self.viewForPostHeight.constant=expectedCollectionViewHeight;
                          
                          self.containerScrollView.bounces=NO;
                      }
                      else
                      {
                          self.containerScrollView.bounces=YES;
                        //  self.viewForPostHeight.constant=requiredCollectionViewSize;
                          
                      }
                      
                      [self.activityIndicatorView stopAnimating];
                      [self.collectionViewOfPosts reloadData];
                      
                      self.refreshButton.hidden=YES;
                  }
     
                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
       
                      NSLog(@"Error: %@", error);
                      self.noPostLabel.hidden=YES;
                      self.view.userInteractionEnabled=YES;
         
                      self.refreshButton.hidden=NO;
         
                      [self.activityIndicatorView stopAnimating];
        
        
     }];

}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if([scrollView isEqual:self.collectionViewOfPosts])
    {
         if (scrollView.contentOffset.y==0)
             self.collectionViewOfPosts.scrollEnabled=NO;
        
    }
    else if(self.selectedTab==1 || self.selectedTab==4)
    {
        if (lastScrollViewContentOffset.y > scrollView.contentOffset.y)
        {
            NSLog(@"scrolling up %lf expected %lf required %lf constant %lf",scrollView.contentOffset.y, expectedCollectionViewHeight, requiredCollectionViewSize,self.viewForPostHeight.constant);
            
         
//            if (scrollView.contentOffset.y==0)
//                self.collectionViewOfPosts.scrollEnabled=NO;
//            
            if(self.viewForPostHeight.constant <= requiredCollectionViewSize)
            {
                self.viewForPostHeight.constant=requiredCollectionViewSize;
                
            }
            else{
                self.viewForPostHeight.constant=requiredCollectionViewSize+scrollView.contentOffset.y;
                lastScrollViewContentOffset=scrollView.contentOffset;
                
           //  self.collectionViewOfPosts.scrollEnabled=NO;
            }
            
                self.collectionViewOfPosts.scrollEnabled=NO;
            
        }
        else
        {
            NSLog(@"scrolling down %lf expected %lf required %lf constant %lf",scrollView.contentOffset.y, expectedCollectionViewHeight, requiredCollectionViewSize,self.viewForPostHeight.constant);
            
            if(expectedCollectionViewHeight <= requiredCollectionViewSize )
            {
                self.viewForPostHeight.constant=requiredCollectionViewSize;
                
            }
            
            else if(self.viewForPostHeight.constant < expectedCollectionViewHeight)
            {
                self.viewForPostHeight.constant=requiredCollectionViewSize+scrollView.contentOffset.y;
                lastScrollViewContentOffset=scrollView.contentOffset;
                
            }
            else{
                self.viewForPostHeight.constant=expectedCollectionViewHeight;
              
            }
            
//            if ( scrollView.contentOffset.y==0)
//                self.collectionViewOfPosts.scrollEnabled=NO;
//            
            if(scrollView.contentOffset.y>(self.viewForUserDetails.size.height+45))
            {
                self.collectionViewOfPosts.scrollEnabled=YES;
                
            }
            else
            {
                self.collectionViewOfPosts.scrollEnabled=NO;
            }
            
        }
    }
    
}


//-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    NSLog(@"scrollViewDidEndDecelerating ");
//    
//    if(self.viewForPostHeight.constant >= expectedCollectionViewHeight)
//    {
//    //    [self.collectionViewOfPosts reloadItemsAtIndexPaths:self.collectionViewOfPosts.indexPathsForVisibleItems];
////        [self.collectionViewOfPosts performBatchUpdates:^{
////            self.collectionViewOfPosts reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow: section:0]];
////        }];
//    }
//}


-(void) makeRequestForTag {
    
    
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    [postData setObject:ACCESS_KEY forKey:@"access_key"];
    if (self.isUserOrFollower) {
        [postData setObject:[NSNumber numberWithInt:self.userId] forKey:@"user_id"];
       // [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
    }else{
        [postData setObject:[NSNumber numberWithInt:self.currentUserId] forKey:@"user_id"];
       // [postData setObject:[NSNumber numberWithInt:self.currentUserId] forKey:@"current_user_id"];
    }
    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
    
    NSLog(@"postData %@",postData);
    
    [apiLoginManager POST:[NSString stringWithFormat:@"%@/users/api-get-tagged-photos",SERVER_BASE_API_URL] parameters:postData
                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                      
                       NSLog(@"Response tagged: %@", responseObject);
                      
                     // userTagPosts=[[[responseObject reverseObjectEnumerator] allObjects] mutableCopy];
                      userTagPosts=[[responseObject allObjects] mutableCopy];
                      
                      
                      //[self.collectionViewOfPosts reloadData];
//                      if (userTagPosts.count) {
//                          self.noPostLabel.hidden=YES;
//                      }
//                      else
//                      {
//                          
//                          self.noPostLabel.hidden=NO;
//                      }
//                      
                      self.collectionViewOfPosts.hidden = NO;
                      
                      CGFloat postCount=(CGFloat)userTagPosts.count;
//                      CGFloat expectedheight=(([UIScreen mainScreen].bounds.size.width-2)/3)*ceil(postCount/3)+floor(postCount/3);
//                      self.viewForPostHeight.constant=expectedheight;
                      
                      
                      expectedCollectionViewHeight=ceil((([UIScreen mainScreen].bounds.size.width-2)/3)*ceil(postCount/3)+floor(postCount/3));
                    
                      
                      if(expectedCollectionViewHeight>([UIScreen mainScreen].bounds.size.height-66-49))
                      {
                          expectedCollectionViewHeight=([UIScreen mainScreen].bounds.size.height-66-49);
                          self.viewForPostHeight.constant=expectedCollectionViewHeight;
                          
                          self.containerScrollView.bounces=NO;
                      }
                      else
                      {
                          self.containerScrollView.bounces=YES;
                          //  self.viewForPostHeight.constant=requiredCollectionViewSize;
                          
                      }
                      
                      
                      [self.activityIndicatorView stopAnimating];
                      [self.collectionViewOfPosts reloadData];
                      
                      
                  }
     
                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                      
                      NSLog(@"Error: %@", error);
                      
                    //  self.view.userInteractionEnabled=YES;
                      
                      NSLog(@"%@",operation.responseString);
                      
                      [self.activityIndicatorView stopAnimating];
                      self.viewForPostHeight.constant=requiredCollectionViewSize;
                      
                      
                      [self.activityIndicatorView stopAnimating];
                      [self.collectionViewOfPosts reloadData];
                      
                      
                  }];
    
}

#pragma mark - CollectionView data source

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
   if (collectionView.tag == 1)
    {
        
        return  4;
        
    }else
    {
        self.collectionViewOfPosts.hidden=NO;
        
        if (self.selectedTab== 1)
        {
            
            return  userPosts.count;
            
        }else if (self.selectedTab== 4)
        {
            return  userTagPosts.count;
        }
        else
            return 0;
    }
    
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSLog(@"cellForItemAtIndexPath %ld",(long)indexPath.item);
    if (collectionView.tag == 1)
    {
        
        static NSString *identifier = @"followerCountCell";
        
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        UILabel *countLabel = (UILabel*) [cell viewWithTag:1];
        UILabel *titleLabel = (UILabel*) [cell viewWithTag:2];
        
        titleLabel.text = [title objectAtIndex:indexPath.row];
        
        
        if (indexPath.row == 0) {
            
            countLabel.text = @"";
            if ([userInfo objectForKey:@"post_count"])
            {
                countLabel.text = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"post_count"]];
            }
            
        }else if (indexPath.row == 1){
            countLabel.text = @"";
            if ([userInfo objectForKey:@"pins"]) {
                
                countLabel.text = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"pins"]];
            }
            
            
        } else if (indexPath.row == 2){
            countLabel.text = @"";
            if ([userInfo objectForKey:@"follower_count"]) {
                
                countLabel.text = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"follower_count"]];
            }
            
            
        }else if (indexPath.row == 3){
            countLabel.text = @"";
            
            if ([userInfo objectForKey:@"following_count"]) {
                
                countLabel.text = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"following_count"]];
            }
            
            
        }
        
        return cell;
    }else
    {
        static NSString *identifier = @"postsPhotoCell";
        
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        UIImageView *postsImage = (UIImageView*) [cell viewWithTag:3];
        UIActivityIndicatorView *activityInd = (UIActivityIndicatorView*) [cell viewWithTag:2];
        
        activityInd.hidden=NO;
        [activityInd startAnimating];

        [postsImage setShowActivityIndicatorView:YES];
        [postsImage setIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        if (self.selectedTab== 1)
        {
            
           
            //[postsImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[userPosts objectAtIndex:indexPath.row] objectForKey:@"image"]]]];
       
            RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
            
            RLMResults *result=[Posts objectsInRealm:realm where:[NSString stringWithFormat:@"postId = %@",[[userPosts objectAtIndex:indexPath.row] objectForKey:@"id"]]];
            
            
            if(result.count)
            {
                Posts *post=[[Posts alloc] init];
                post=[result objectAtIndex:0];
               // NSLog(@"result %@",result);
                
                [activityInd stopAnimating];
                 postsImage.image=[UIImage imageWithData:post.imageData];
                
//                [UIView transitionWithView:postsImage duration:0.2f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
//                    [activityInd stopAnimating];
//                    postsImage.image=[UIImage imageWithData:post.imageData];
//                } completion:^(BOOL finished) {
//                    ;
//                }];
                
                
            }
            else
            {
                postsImage.image=nil;
                //postsImage.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"square_image" ofType:@"png"]];
                
              //  NSLog(@"downloadingIds %lu",(unsigned long)downloadingIds.count);
                if([downloadingIds containsObject: [[userPosts objectAtIndex:indexPath.row] objectForKey:@"id"]])
                {
                    //do nothing
                    NSLog(@"do nothing");
                }
                else
                {
                    
                    [downloadingIds addObject:[[userPosts objectAtIndex:indexPath.row] objectForKey:@"id"]];
                    
                    SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
                    [SDWebImageDownloader sharedDownloader].maxConcurrentDownloads=1;
                    [downloader downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[userPosts objectAtIndex:indexPath.row] objectForKey:@"image"] ]]
                                             options:0
                                            progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                // progression tracking code
                                                //NSLog(@"receivedSize %ld expectedSize %ld",(long)receivedSize,(long)expectedSize);
                                                
                                            }
                                           completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                               if (image && finished) {
                                                   // do something with image
                                                   
                                                   
                                                   //                                           dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                                                   //                                           dispatch_async(queue, ^{
                                                   //                                               // Get new realm and table since we are in a new thread
                                                   
                                                   NSLog(@"done in profile collection %@",[[userPosts objectAtIndex:indexPath.row] objectForKey:@"id"]);
                                                   
                                                   //RLMRealm *realm = [RLMRealm defaultRealm];
                                                   
                                                   
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       //searchImage.image=image;
                                                       
                                                       
                                                       RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
                                                       
                                                       [realm beginWriteTransaction];
                                                       //                                                   [Posts createOrUpdateInRealm:realm withValue:@{@"postId": [NSNumber numberWithInt:[[[discoverPost objectAtIndex:indexPath.row] objectForKey:@"id"] intValue]],
                                                       //                                                                                          @"imageData": data}];
                                                       //
                                                       [Posts createOrUpdateInRealm:realm withValue:@{@"primId": [NSString stringWithFormat:@"%@",[[userPosts objectAtIndex:indexPath.row] objectForKey:@"id"]]
                                                                                                      ,@"postId": [NSNumber numberWithInt:[[[userPosts objectAtIndex:indexPath.row] objectForKey:@"id"] intValue]],
                                                                                                      @"imageData": data}];
                                                       
                                                       
                                                       
                                                       [realm commitWriteTransaction];
                                                       
                                                       [downloadingIds removeObject:[[userPosts objectAtIndex:indexPath.row] objectForKey:@"id"]];
                                                       // [self reloadData];
                                                       
                                                       
                                                       postsImage.image=[UIImage imageWithData:data];
                                                       [activityInd stopAnimating];
                                                       
                                                       //[self.collectionViewOfPosts reloadItemsAtIndexPaths:@[indexPath]];
                                                       //      });
                                                       
                                                   });
                                               }
                                               
                                               
                                           }];
                    
                }
                
            }

            
        }else
        {
            
            //[postsImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[userTagPosts objectAtIndex:indexPath.row] objectForKey:@"image"]]]];
            
            RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
            //    NSLog(@"realmv1Configuration %@",realmv1Configuration.fileURL);
            
            RLMResults *result=[Posts objectsInRealm:realm where:[NSString stringWithFormat:@"postId = %@",[[userTagPosts objectAtIndex:indexPath.row] objectForKey:@"id"]]];
            
            
            if(result.count)
            {
                Posts *post=[[Posts alloc] init];
                post=[result objectAtIndex:0];
              //  NSLog(@"result %@",result);
                
                [UIView transitionWithView:postsImage duration:0.2f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                    [activityInd stopAnimating];
                    postsImage.image=[UIImage imageWithData:post.imageData];
                } completion:^(BOOL finished) {
                    ;
                }];
                
            }
            else
            {
                postsImage.image=nil;
                //postsImage.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"square_image" ofType:@"png"]];
                
                if([downloadingIds containsObject: [[userTagPosts objectAtIndex:indexPath.row] objectForKey:@"id"]])
                {
                    //do nothing
                }
                else
                {
                    
                    [downloadingIds addObject:[[userTagPosts objectAtIndex:indexPath.row] objectForKey:@"id"]];
                    
                    SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
                    [SDWebImageDownloader sharedDownloader].maxConcurrentDownloads=1;
                    [downloader downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[userTagPosts objectAtIndex:indexPath.row] objectForKey:@"image"] ]]
                                             options:0
                                            progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                // progression tracking code
                                              //  NSLog(@"receivedSize %ld expectedSize %ld",(long)receivedSize,(long)expectedSize);
                                                
                                            }
                                           completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                               if (image && finished) {
                                                   // do something with image
                                                   
                                                   //                                           dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                                                   //                                           dispatch_async(queue, ^{
                                                   //                                               // Get new realm and table since we are in a new thread
                                                   
                                                   //NSLog(@"done in explore collection %@",[[userTagPosts objectAtIndex:indexPath.row] objectForKey:@"id"]);
                                                   //RLMRealm *realm = [RLMRealm defaultRealm];
                                                   
                                                   
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       //searchImage.image=image;
                                                       
                                                       
                                                       RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
                                                       
                                                       [realm beginWriteTransaction];
                                                       //                                                   [Posts createOrUpdateInRealm:realm withValue:@{@"postId": [NSNumber numberWithInt:[[[discoverPost objectAtIndex:indexPath.row] objectForKey:@"id"] intValue]],
                                                       //                                                                                          @"imageData": data}];
                                                       //
                                                       
                                                       [Posts createOrUpdateInRealm:realm withValue:@{@"primId": [NSString stringWithFormat:@"%@",[[userTagPosts objectAtIndex:indexPath.row] objectForKey:@"id"]]
                                                                                                      ,@"postId": [NSNumber numberWithInt:[[[userTagPosts objectAtIndex:indexPath.row] objectForKey:@"id"] intValue]],
                                                                                                      @"imageData": data}];
                                                       
                                                       [realm commitWriteTransaction];
                                                       
                                                       [downloadingIds removeObject:[[userTagPosts objectAtIndex:indexPath.row] objectForKey:@"id"]];
                                                       //[self reloadData];
                                                       postsImage.image=[UIImage imageWithData:data];
                                                       [activityInd stopAnimating];
                                                       //[self.collectionViewOfPosts reloadItemsAtIndexPaths:@[indexPath]];
                                                       //});
                                                       
                                                   });
                                               }
                                               
                                               
                                           }];
                    
                }
                
            }

        }

    
       // NSLog(@"image link : %@/%@",SERVER_BASE_API_URL,[[userPosts objectAtIndex:indexPath.row] objectForKey:@"image"]);
        return cell;
    }
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.tag == 2)
    {
       //  NSLog(@" collection view %lf ",([UIScreen mainScreen].bounds.size.width-2)/3);
        return CGSizeMake(([UIScreen mainScreen].bounds.size.width-2)/3, ([UIScreen mainScreen].bounds.size.width-2)/3);
        
    }else
    {
        //  NSLog(@" collection view %@ ",collectionView);
        //  NSLog(@" followersCountCollectionView view %f ",(collectionView.layer.bounds.size.width-2)/3);
        return CGSizeMake((collectionView.layer.bounds.size.width-3)/4,40);
        
    }
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (collectionView.tag == 2)
    {
        //UICollectionViewCell *cell=[collectionView cellForItemAtIndexPath:indexPath];
        
        //UIImageView *postsImage = (UIImageView*) [cell viewWithTag:3];
        
        ViewPostViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewPostViewController"];
        
        //controller.postImage = postsImage.image;
        
        if(self.selectedTab==1)
        {
            controller.indexNumber = indexPath.row;
            controller.userPostArray = userPosts;
            controller.isFromProfileWithTaggedPost=NO;
            
         //   controller.singlePostDic = [[NSMutableDictionary alloc] initWithDictionary:[userPosts objectAtIndex:indexPath.row]] ;
            
            
        }else if(self.selectedTab==4){
        
            controller.indexNumber = indexPath.row;
            controller.userPostArray = userTagPosts;
            
            controller.isFromProfileWithTaggedPost=YES;
            
      //    controller.singlePostDic = [[NSMutableDictionary alloc] initWithDictionary:[userTagPosts objectAtIndex:indexPath.row]];
        }
        
        
        controller.isFromGallery=NO;
        controller.isFromProfile = YES;
        controller.userId=self.userId;
        
        
        [self.navigationController pushViewController:controller animated:YES];
    }else{
    
        if (indexPath.row == 1) {
            
            PinPostsViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PinPostsViewController"];
            
            
            if (self.isUserOrFollower) {
                controller.userId = self.userId;
            }else
            {
                controller.userId = self.currentUserId;
            }
            
            [self.navigationController pushViewController:controller animated:YES];
            
        }
    
        else if (indexPath.row == 2) {
            
            LikersViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"LikersViewController"];
            
            controller.isFollower = YES;
            if (self.isUserOrFollower) {
                controller.userId = self.userId;
            }else
            {
                controller.userId = self.currentUserId;
            }
            
            [self.navigationController pushViewController:controller animated:YES];
            
        }else if (indexPath.row == 3){
        
        
            LikersViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"LikersViewController"];
            
            controller.isFolloweing = YES;
            if (self.isUserOrFollower) {
                
                controller.userId = self.userId;
            }else
            {
                controller.userId = self.currentUserId;
            }
            
            [self.navigationController pushViewController:controller animated:YES];


        }

    
    }
    
}

//Button ACtion

- (IBAction)followButtonAction:(UIButton*)sender {
    
    sender.userInteractionEnabled = NO;
    
    
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    [postData setObject:ACCESS_KEY forKey:@"access_key"];
    [postData setObject:[NSNumber numberWithInt:self.userId] forKey:@"user_id"];
    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
    
    NSLog(@"postData %@",postData);
    
    [apiLoginManager POST:[NSString stringWithFormat:@"%@/users/apiSwapFollowing",SERVER_BASE_API_URL] parameters:postData
                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                      
                      NSLog(@"Response: %@", responseObject);
                      
                      NSString * successMsg = [responseObject objectForKey:@"success"];
                      
                      if ([successMsg integerValue] == 1 ) {
                          
                          self.followButton.userInteractionEnabled = YES;
                          
                          NSLog(@"success msg %@",[responseObject objectForKey:@"success"]);
                          
                           sender.selected=!sender.selected;
                          if (sender.selected) {
                              
                              self.followButton.tintColor = [HXColor hx_colorWithHexString:@"2DCC70"];
                              self.followButton.layer.borderColor = [[HXColor hx_colorWithHexString:@"2DCC70"]CGColor];
                              
                          }else{
                          
                              self.followButton.tintColor = [HXColor hx_colorWithHexString:@"000000"];
                              self.followButton.layer.borderColor = [[HXColor hx_colorWithHexString:@"000000"]CGColor];
                          }
                          
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"loadAllFeed" object:nil];
                         
                      }

                  }
     
                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                      
                      NSLog(@"Error: %@", error);
                      
                      self.followButton.userInteractionEnabled = YES;
                      
                      NSLog(@"%@",operation.responseString);
                      
                      
                      
                  }];
    
}

- (IBAction)downLoadButtonAction:(id)sender {
}

- (IBAction)editPrifileButtonAction:(id)sender {
    
    EditProfileViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileViewController"];
    
    
    [self.navigationController pushViewController:controller animated:YES];
    
}

- (IBAction)TabButtonAction:(UIButton*)sender {
    
    downloadingIds=[[NSMutableArray alloc] init];
    [[SDWebImageDownloader sharedDownloader] cancelAllDownloads];
    
    
    if ([[userInfo objectForKey:@"is_account_private"] integerValue] &&  self.userId != [UserAccount sharedManager].userId && ![[userInfo objectForKey:@"is_following"] integerValue])
    {
        NSLog(@"is_account_private in tabbar action");
        [UILabel animateWithDuration:0.4
                          animations:^{
                              self.privateAccountMsg.transform = CGAffineTransformMakeScale(1.1, 1.1);
                          }
                          completion:^(BOOL finished) {
                              [UILabel animateWithDuration:0.4
                                                animations:^{
                                                    self.privateAccountMsg.transform = CGAffineTransformIdentity;
                                                    
                                                }];
                          }];
        
      
    }
    else
    {
        self.noPostLabel.hidden=YES;
        
        if (sender.tag==101 && self.selectedTab!=1) {
            
            NSLog(@"self.selectedTab %d",self.selectedTab);
            self.selectedTab=1;
            
            
            [UIView animateWithDuration:0.5f animations:^{
                self.selectedTabXPostiotion.constant=0;
                [self.view layoutIfNeeded];
            }];
            
            self.collectionViewOfPosts.hidden = NO;
            self.viewForEvents.hidden = YES;
            self.viewForUserPhotos.hidden = YES;
            
            if(self.userId == [UserAccount sharedManager].userId)
            {
                if (userPosts.count) {
                    self.noPostLabel.hidden=YES;
                }
                else
                {
                    self.noPostLabel.hidden=NO;
                }
                
            }
            else
            {
                self.noPostLabel.hidden=YES;
            }
         
            

            CGFloat postCount=(CGFloat)userPosts.count;
            expectedCollectionViewHeight=ceil((([UIScreen mainScreen].bounds.size.width-2)/3)*ceil(postCount/3)+floor(postCount/3));
            if(expectedCollectionViewHeight>([UIScreen mainScreen].bounds.size.height-66-49))
            {
                expectedCollectionViewHeight=([UIScreen mainScreen].bounds.size.height-66-49);
                self.viewForPostHeight.constant=expectedCollectionViewHeight;
                
                self.containerScrollView.bounces=NO;
            }
            else
            {
                self.containerScrollView.bounces=YES;
                self.viewForPostHeight.constant=requiredCollectionViewSize;
            }
          
            [self.collectionViewOfPosts reloadData];
           
 
        }
        else if (sender.tag==102) {
            //self.selectedTab=2;
            
            
            if (self.isUserOrFollower) {
                
                if(self.userId == [UserAccount sharedManager].userId)
                {
                    [UserAccount sharedManager].pendingActivityCount=0;
                    
                    [badgeView setBadgeValue: [UserAccount sharedManager].pendingActivityCount];
                    [badgeView setHidden:YES];
                    [UserAccount sharedManager].lastActivityDate=[self getDateStringFromDate:[NSDate date]];
                    NSLog(@"[UserAccount sharedManager].lastActivityDate %@",[UserAccount sharedManager].lastActivityDate);
                    
                    ActivityListViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivityListViewController"];
                    [self.navigationController pushViewController:controller animated:YES];
                }else
                {
                    PersonalGalleryViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PersonalGalleryViewController"];
                    controller.userId=self.userId;
                    [self.navigationController pushViewController:controller animated:YES];
                    
                }
                
            }else{
                [UserAccount sharedManager].pendingActivityCount=0;
                [badgeView setBadgeValue: [UserAccount sharedManager].pendingActivityCount];
                [badgeView setHidden:YES];
                
                [UserAccount sharedManager].lastActivityDate=[self getDateStringFromDate:[NSDate date]];
                NSLog(@"[UserAccount sharedManager].lastActivityDate %@",[UserAccount sharedManager].lastActivityDate);
                
                ActivityListViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivityListViewController"];
                [self.navigationController pushViewController:controller animated:YES];
                
            }
            
        }
        else if (sender.tag==103) {
            
            NSLog(@"self.selectedTab %d",self.selectedTab);
            
            self.selectedTab=3;
            
            
            self.containerScrollView.bounces=YES;
            self.viewForPostHeight.constant=requiredCollectionViewSize;
            
           // self.containerScrollView.contentSize=CGSizeMake(self.containerScrollView.frame.size.width, self.containerScrollView.frame.size.height);
            
            NSLog(@"containerScrollView %d",self.containerScrollView.bounces);
            self.collectionViewOfPosts.hidden = YES;
            
            self.viewForEvents.hidden = NO;
            self.viewForUserPhotos.hidden = YES;
            
            [UIView animateWithDuration:0.5f animations:^{
                self.selectedTabXPostiotion.constant=([UIScreen mainScreen].bounds.size.width/4)*2;
                [self.view layoutIfNeeded];
            }];
            
        }
        else if (sender.tag==104 && self.selectedTab!=4) {
            
            
            self.selectedTab=4;
            
            self.viewForEvents.hidden = YES;
            self.viewForUserPhotos.hidden = YES;
            
            NSLog(@" userTagPosts %lu",(unsigned long)userTagPosts.count);
            if(userTagPosts.count)
            {
                
                self.collectionViewOfPosts.hidden = NO;
                
                CGFloat postCount=(CGFloat)userTagPosts.count;
               expectedCollectionViewHeight=ceil((([UIScreen mainScreen].bounds.size.width-2)/3)*ceil(postCount/3)+floor(postCount/3));
                if(expectedCollectionViewHeight>([UIScreen mainScreen].bounds.size.height-66-49))
                {
                    expectedCollectionViewHeight=([UIScreen mainScreen].bounds.size.height-66-49);
                    self.viewForPostHeight.constant=expectedCollectionViewHeight;
                    
                    self.containerScrollView.bounces=NO;
                }
                else
                {
                    self.containerScrollView.bounces=YES;
                    self.viewForPostHeight.constant=requiredCollectionViewSize;
                }
                [self.collectionViewOfPosts reloadData];
            }
            else
            {
                [self.activityIndicatorView startAnimating];
                [self makeRequestForTag];
            }
            
            
            [UIView animateWithDuration:0.5f animations:^{
                
                self.selectedTabXPostiotion.constant=([UIScreen mainScreen].bounds.size.width/4)*3;
                [self.view layoutIfNeeded];
            }];
            
        }
        
        for (int i=101; i<=104; i++) {
            
            UIButton *random1 = (UIButton *)[self.view viewWithTag:i];
            random1.selected=NO;
        }
        sender.selected=!sender.selected;

    }

}

-(void)updateBadgeValue
{
    
    NSLog(@"updateBadgeValue");
    [badgeView setBadgeValue: [UserAccount sharedManager].pendingActivityCount];
    
    if (self.userId == [UserAccount sharedManager].userId) {
        if([UserAccount sharedManager].pendingActivityCount==0 )
            [badgeView setHidden:YES];
        else
            [badgeView setHidden:NO];
    }
    else
    {
        [badgeView setHidden:YES];
        
    }
   
    
}

-(NSString *)getDateStringFromDate:(NSDate *)date
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    NSLog(@"%@", stringFromDate);
    return stringFromDate;
    
}


- (IBAction)ppCaptureAction:(id)sender {
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:NSLocalizedString(@"Open camera", nil),NSLocalizedString(@"Select from Library", nil), nil];
    
    
    sheet.tag = 1;
    [sheet showInView:self.view];
    
    
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    
    
//    chosenImage = info[UIImagePickerControllerEditedImage];
//    self.profilePicture.image=chosenImage;
//    
    [picker dismissViewControllerAnimated:NO completion:^{
       // [self updateProfilePic];
    }];
    
    TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:TOCropViewCroppingStyleCircular image:[info objectForKey:UIImagePickerControllerOriginalImage]];
    cropController.delegate = self;
    [self presentViewController:cropController animated:YES completion:nil];

    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    chosenImage=nil;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

#pragma mark - Cropper Delegate -
-(void)cropViewController:(TOCropViewController *)cropViewController didFinishCancelled:(BOOL)cancelled
{
    // [self updateImageViewWithImage:_capturedImageV.image fromCropViewController:cropViewController];
    [cropViewController dismissViewControllerAnimated:YES completion:^{
        NSLog(@"cancel");
        
    }];
    
}

- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    
    //
    //    self.croppedFrame = cropRect;
    //    self.angle = angle;
    //    [self updateImageViewWithImage:image fromCropViewController:cropViewController];
    //
    //
    
    
    [cropViewController dismissViewControllerAnimated:YES completion:^{
        
            chosenImage = image;

            self.profilePicture.image=chosenImage;
        
                [self updateProfilePic];
    }];
    
    //    [cropViewController dismissAnimatedFromParentViewController:self
    //                                               withCroppedImage:image
    //                                                         toView:_capturedImageV
    //                                                        toFrame:CGRectZero
    //                                                          setup:^{ [self layoutImageView]; }
    //                                                     completion:
    //     ^{
    //         _capturedImageV.hidden = NO;
    //
    //
    //     }];
    
    
}

- (IBAction)otherOptionButtonAction:(id)sender {
    
    NSLog(@"other option button clicked");
    
    NSString *blockActionString;
    if([[userInfo objectForKey:@"is_blocked"] integerValue])
        blockActionString=@"Unblock user";
    else
        blockActionString=@"Block user";
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:NSLocalizedString(@"Send Message", nil),NSLocalizedString(@"Report user", nil),NSLocalizedString(blockActionString, nil), nil];
    
    sheet.tag = 2;
    [sheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (actionSheet.tag == 1) {
        
        if (buttonIndex == 0) {
            
            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                
                UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                      message:@"Device has no camera"
                                                                     delegate:nil
                                                            cancelButtonTitle:@"OK"
                                                            otherButtonTitles: nil];
                
                [myAlertView show];
                
            }
            else
            {
                
                //        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                //        picker.delegate = self;
                //        picker.allowsEditing = NO;
                //        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                //
                //        [self presentViewController:picker animated:YES completion:NULL];
                
                
                
                UIImagePickerControllerSourceType source = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] ? UIImagePickerControllerSourceTypeCamera : UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                UIImagePickerController *cameraController = [[UIImagePickerController alloc] init];
                cameraController.delegate = self;
                cameraController.sourceType = source;
                //cameraController.allowsEditing = YES;
                [self presentViewController:cameraController animated:YES completion:^{
                    //iOS 8 bug.  the status bar will sometimes not be hidden after the camera is displayed, which causes the preview after an image is captured to be black
                    if (source == UIImagePickerControllerSourceTypeCamera) {
                        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
                    }
                }];
                
            }
            
            
        }else if(buttonIndex ==1)
        {
            UIImagePickerControllerSourceType source = UIImagePickerControllerSourceTypePhotoLibrary;
            UIImagePickerController *cameraController = [[UIImagePickerController alloc] init];
            cameraController.delegate = self;
            cameraController.sourceType = source;
            //cameraController.allowsEditing = YES;
            [self presentViewController:cameraController animated:YES completion:^{
                //iOS 8 bug.  the status bar will sometimes not be hidden after the camera is displayed, which causes the preview after an image is captured to be black
                if (source == UIImagePickerControllerSourceTypeCamera) {
                    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
                }
            }];
            
        }
        
    }
    else if (actionSheet.tag == 2)
    {
        if(buttonIndex == 0)
        {
            
            NSLog(@"send message button clicked");
            
            DemoMessagesViewController *vc = [DemoMessagesViewController messagesViewController];
            vc.chatFollowerId=[NSString stringWithFormat:@"%d",self.userId];
            vc.chatFollowerName=[userInfo objectForKey:@"username"];
            vc.isFromProfile = YES;
            
            UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:vc];
            navCon.navigationBar.hidden=YES;
            
            [self presentViewController:navCon animated:YES completion:nil];
            
        }
        else if (buttonIndex == 1) {
            
            //[self performSelector:@selector(secondMethod) withObject:nil afterDelay:1.0 ];
            
            AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
            apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
            
            NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
            [postData setObject:ACCESS_KEY forKey:@"access_key"];
            [postData setObject:[NSNumber numberWithInt:[[NSString stringWithFormat:@"%d",self.userId] intValue]] forKey:@"suspect_id"] ;
            [postData setObject:[NSNumber numberWithInt:[[NSString stringWithFormat:@"%d",self.userId] intValue]] forKey:@"user_id"];
            [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
          //  NSLog(@"postData %@",postData);
            
            [apiLoginManager POST:[NSString stringWithFormat:@"%@/users/api-report-user",SERVER_BASE_API_URL] parameters:postData
                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
                              
                              NSLog(@"response object %@",responseObject);
                              
                              if ([[responseObject objectForKey:@"success"] intValue] == 1 ) {
                                  
                                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                                  message:@"Your report has been submitted."
                                                                                 delegate:nil
                                                                        cancelButtonTitle:@"OK"
                                                                        otherButtonTitles:nil];
                                  [alert show];
                              }
                              
                              else if ([[responseObject objectForKey:@"success"] intValue] == 2 ) {
                                  
                                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                                  message:@"You have already reported this user."
                                                                                 delegate:nil
                                                                        cancelButtonTitle:@"OK"
                                                                        otherButtonTitles:nil];
                                  [alert show];
                              }
                              
                          }
                          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                              
                              NSLog(@"Error: %@", error);
                              
                              
                              NSLog(@"responseString: %@", operation.responseString);
                              
                          }];
            
        }
        else if (buttonIndex == 2)
        {
            //Block user
            
            AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
            apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
            
            NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
            [postData setObject:ACCESS_KEY forKey:@"access_key"];
            [postData setObject:[NSNumber numberWithInt:self.userId] forKey:@"blocked_user_id"];
            [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
            
            NSLog(@"postData %@",postData);
            
            [apiLoginManager POST:[NSString stringWithFormat:@"%@/blocked-users/apiSwapBlock",SERVER_BASE_API_URL] parameters:postData
                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
                              
                              NSLog(@"response object %@",responseObject);
                              
                              
                              if ([[responseObject objectForKey:@"success"] intValue] == 1 ) {
                                  
                                  if([[userInfo objectForKey:@"is_blocked"] integerValue])
                                  {
                                      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                                      message:[NSString stringWithFormat:@"%@ has been unblocked successfully",[userInfo objectForKey:@"username"]]
                                                                                     delegate:nil
                                                                            cancelButtonTitle:@"OK"
                                                                            otherButtonTitles:nil];
                                      [alert show];
                                      [userInfo setObject:[NSNumber numberWithBool:0] forKey:@"is_blocked"];
                                  }
                                  else
                                  {
                                      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                                      message:[NSString stringWithFormat:@"%@ has been blocked from being able to access your profile and posts. You will also no longer appear in any searches initiated by %@.",[userInfo objectForKey:@"username"],[userInfo objectForKey:@"username"]]
                                                                                     delegate:nil
                                                                            cancelButtonTitle:@"OK"
                                                                            otherButtonTitles:nil];
                                      [alert show];

                                      [userInfo setObject:[NSNumber numberWithBool:1] forKey:@"is_blocked"];
                                  }
                                  
                                 
                              }
                              
                          }
                          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                              
                              NSLog(@"Error: %@", error);
                              
                              
                             NSLog(@"responseString: %@", operation.responseString);
                              
                              
                              
                          }];
            
        }
            
        
    }
}
-(void)updateProfilePic
{
    [self.activityIndicatorView startAnimating];
    AFHTTPRequestOperationManager *infoManager = [AFHTTPRequestOperationManager manager];
    
    NSMutableDictionary * userInfoDictionary=[[NSMutableDictionary alloc] init];
    [userInfoDictionary setObject:[NSNumber numberWithInt:self.currentUserId] forKey:@"user_id"];
    [userInfoDictionary setObject:ACCESS_KEY forKey:@"access_key"];
    [userInfoDictionary setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
    
    [infoManager POST:[NSString stringWithFormat:@"%@/users/api-update-pro-pic",SERVER_BASE_API_URL] parameters:userInfoDictionary
       constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
         
         //NSData *imageData = UIImageJPEGRepresentation(chosenImage, 0.5);
         
        NSData *imageData = [self compressImage:chosenImage];
        [formData appendPartWithFileData:imageData
                                     name:@"profile_picture"
                                 fileName:@"Profile.jpg" mimeType:@"image/jpeg"];
         
         
         

     } success:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSLog(@"Response: %@", responseObject);
         
     
         [self.activityIndicatorView stopAnimating];
         
         if([[responseObject objectForKey:@"success"] integerValue]==1)
         {
//             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thank you!"
//                                   
//                                                             message:@"Your feedback has been sent"
//                                                            delegate:nil
//                                                   cancelButtonTitle:@"OK"
//                                                   otherButtonTitles: nil];
//             [alert show];
             
         }
         else
         {
//             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed!"
//                                                             message:@"Something went wrong"
//                                                            delegate:nil
//                                                   cancelButtonTitle:@"OK"
//                                                   otherButtonTitles: nil];
//             [alert show];
             
             self.profilePicture.image=nil;
             if ([[userInfo objectForKey:@"profile_picture"] isEqual:[NSNull null]]) {
                 
                 [self.profilePicture sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL,@"img/profile-holder.png"]]];
             }else{
                 
                 [self.profilePicture sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL,[userInfo objectForKey:@"profile_picture"]]]];
             }
             
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed!"
                                                             message:@"Couldnt change the profile pic, please try again"
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles: nil];
             [alert show];

             
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         
         [self.activityIndicatorView stopAnimating];
         
       
         self.profilePicture.image=nil;
         if ([[userInfo objectForKey:@"profile_picture"] isEqual:[NSNull null]]) {
             
             [self.profilePicture sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL,@"img/profile-holder.png"]]];
         }else{
             
             [self.profilePicture sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL,[userInfo objectForKey:@"profile_picture"]]]];
         }

         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed!"
                                                         message:@"Something went wrong, Please check your network connection"
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles: nil];
         [alert show];
         
     }];


}


- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)optionsButtonAction:(id)sender {
    
     SettingViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingViewController"];
    
    [self.navigationController pushViewController:controller animated:YES];
    
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"buttonIndex %li",(long)buttonIndex);
    
    if(buttonIndex == 0)//Yes button pressed
    {
          [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",self.webSiteUrlLabel.text]]];
        
    }
    else if(buttonIndex == 1)//No button pressed.
    {
       
        
    }
    
}


#pragma mark - Pusher Delegate Connection
//////////////////////////////////

- (void)pusher:(PTPusher *)pusher connectionDidConnect:(PTPusherConnection *)connection
{
    NSLog(@"[Pusher] connected to %@", [connection.URL absoluteString]);
}

- (void)pusher:(PTPusher *)pusher connection:(PTPusherConnection *)connection failedWithError:(NSError *)error
{
    if (error) {
        NSLog(@"[Pusher] connection failed: %@", [error localizedDescription]);
    } else {
        NSLog(@"[Pusher] connection failed");
    }
}

- (void)pusher:(PTPusher *)pusher connection:(PTPusherConnection *)connection didDisconnectWithError:(NSError *)error willAttemptReconnect:(BOOL)reconnect
{
    if (error) {
        NSLog(@"[Pusher] didDisconnectWithError: %@ willAttemptReconnect: %@", [error localizedDescription], (reconnect ? @"YES" : @"NO"));
    } else {
        NSLog(@"[Pusher] disconnected");
    }
}

- (IBAction)refreshPressed:(id)sender {
    
    [self viewDidAppear:YES];
}

- (UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}
-(NSData*)compressImage: (UIImage *)img
{
    float MAX_UPLOAD_SIZE=100;
    float MIN_UPLOAD_RESOLUTION=120*120;
    float factor;
    float resol = img.size.height*img.size.width;
    if (resol >MIN_UPLOAD_RESOLUTION){
        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
        img = [self scaleDown:img withSize:CGSizeMake(img.size.width/factor, img.size.height/factor)];
    }
    
    //Compress the image
    CGFloat compression = 1.0f;
    CGFloat maxCompression = 0.5f;
    
    NSData *imageData = UIImageJPEGRepresentation(img, compression);
    
    while ([imageData length] > MAX_UPLOAD_SIZE && compression > maxCompression)
    {
        compression -= 0.10;
        imageData = UIImageJPEGRepresentation(img, compression);
        NSLog(@"Compress : %lu",(unsigned long)imageData.length);
    }
    return imageData;
}

//-(UIImage *)imageManager:(SDWebImageManager *)imageManager transformDownloadedImage:(UIImage *)image withURL:(NSURL *)imageURL
//{
//    // NSLog(@"imageURL : %@",imageURL);
//    UIImage *resizedImage;
//    //  return resizedImage  = [image resizedImage:CGSizeMake(512, 512) interpolationQuality:kCGInterpolationDefault];
//    
//    float MIN_UPLOAD_RESOLUTION=756*756;
//    float factor;
//    float resol = image.size.height*image.size.width;
//    if (resol >MIN_UPLOAD_RESOLUTION){
//        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
//        resizedImage = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
//        return resizedImage;
//        
//    }
//    else
//        return image;
//}



//- (IBAction)logOutButtonAction:(id)sender {
//    
//    [UserAccount sharedManager].userId=0;
//    [UserAccount sharedManager].userFirstName=@"Guest";
//    [UserAccount sharedManager].userLastName=@"";
//    [UserAccount sharedManager].userImageName=@"/img/profile-holder.png";
//
//    TabBarViewController *tabBar = (TabBarViewController *) self.tabBarController;
//    [tabBar logOutFromTabbar];
//    
//    NSLog(@"%@ %@",tabBar,self.tabBarController);
//   // [self.tabBarController.navigationController popViewControllerAnimated:YES];
//}

@end
