//
//  DTCollectionViewCell.h
//  CollectionViewMoveCellDemo
//
//  Created by EdenLi on 2016/5/23.
//  Copyright © 2016年 Darktt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DTCollectionViewCell : UICollectionViewCell

+ (NSString *)cellIdentifier;

@property (weak, nonatomic) IBOutlet UIImageView *cellImage;
@property (weak, nonatomic) IBOutlet UIButton *lockButton;
@property (weak, nonatomic) IBOutlet UIButton *dollerButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic) bool isSelected;

-(void)highlightCell;
-(void)lockCell;
@end
