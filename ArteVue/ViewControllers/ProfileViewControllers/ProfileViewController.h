//
//  ProfileViewController.h
//  Artegram
//
//  Created by Tanvir Palash on 6/10/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"
#import "TOCropViewController.h"

#import "Pusher.h"

@interface ProfileViewController : UIViewController<UIScrollViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UICollectionViewDataSourcePrefetching,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate,TOCropViewDelegate,PTPusherDelegate,UIActionSheetDelegate,SDWebImageManagerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *refreshButton;
@property (weak, nonatomic) IBOutlet UILabel *profileNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewOfPosts;

//@property (weak, nonatomic) IBOutlet UIView *viewForMapView;
@property (weak, nonatomic) IBOutlet UIView *viewForUserPhotos;
@property (weak, nonatomic) IBOutlet UIView *viewForEvents;

@property  int selectedTab;

@property BOOL isUserOrFollower;//if Follower
@property int userId;
@property int currentUserId;
@property (weak, nonatomic) IBOutlet UIButton *optionButton;
@property (weak, nonatomic) IBOutlet UIButton *otherOptionButton;


@property (weak, nonatomic) IBOutlet UILabel *noPostLabel;
@property (weak, nonatomic) IBOutlet UILabel *privateAccountMsg;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *eventViewHeightConstraint;
@end
