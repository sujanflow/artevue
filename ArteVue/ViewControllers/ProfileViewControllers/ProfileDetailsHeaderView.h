//
//  RecipeCollectionHeaderView.h
//  CollectionViewDemo
//
//  Created by Simon on 22/1/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>


@interface ProfileDetailsHeaderView : UICollectionReusableView

@property (strong, nonatomic) NSMutableDictionary *userInfo;

@property int userId;
@property (weak, nonatomic) IBOutlet UIButton *cameraButton;
@property (weak, nonatomic) IBOutlet UIButton *editProfileButton;
@property (weak, nonatomic) IBOutlet UILabel *profileNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *userTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *userBio;
@property (weak, nonatomic) IBOutlet UILabel *webSiteUrl;
@property (weak, nonatomic) IBOutlet UICollectionView *followersCountCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *followButton;
@property (weak, nonatomic) IBOutlet UIImageView *profilePicture;

@property (weak, nonatomic) IBOutlet UIView *selectedBarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectedTabPosition;
@property (weak, nonatomic) IBOutlet UIButton *SecondTabButton;

-(void) populateData;

@end
