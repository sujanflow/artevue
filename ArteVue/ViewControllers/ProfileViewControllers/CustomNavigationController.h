//
//  CustomNavigationController.h
//  Artegrams
//
//  Created by Tanvir Palash on 2/20/17.
//  Copyright © 2017 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomNavigationController : UINavigationController

@end
