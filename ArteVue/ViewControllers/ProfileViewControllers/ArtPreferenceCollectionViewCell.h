//
//  ArtPreferenceCollectionViewCell.h
//  Artegrams
//
//  Created by Sujan on 9/19/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArtPreferenceCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *preferenceName;

@property (weak, nonatomic) IBOutlet UISwitch *toggleButton;

@end
