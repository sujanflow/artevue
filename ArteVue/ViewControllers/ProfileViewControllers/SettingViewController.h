//
//  SettingViewController.h
//  SettingForArtegrams
//
//  Created by Sujan on 8/24/16.
//  Copyright © 2016 Sujan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *settingTableView;


@end
