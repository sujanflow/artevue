//
//  DTCollectionViewCell.m
//  CollectionViewMoveCellDemo
//
//  Created by EdenLi on 2016/5/23.
//  Copyright © 2016年 Darktt. All rights reserved.
//

#import "DTCollectionViewCell.h"

@implementation DTCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.cellImage=[[UIImageView alloc] init];
        
         self.isSelected = false;
       
    }
    return self;
}

+ (NSString *)cellIdentifier
{
    static NSString *CellIdentifier = @"Cell";
    
    return CellIdentifier;
}

-(void)highlightCell{
    
   
    UIView *darkTint = [[UIView alloc] init];
    darkTint.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.6];
    darkTint.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    [self.contentView addSubview:darkTint];
    
    UIImageView *checkMark = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"doneIcon"]];
    checkMark.center = darkTint.center;
    [self.contentView addSubview:checkMark];
}

-(void)lockCell{
    
    
    UIView *darkTint = [[UIView alloc] init];
    darkTint.userInteractionEnabled=NO;
    darkTint.layer.zPosition=100;
    darkTint.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:.4];
    darkTint.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    [self.contentView addSubview:darkTint];
    
    
}



@end
