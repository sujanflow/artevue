//
//  EditProfileCollectionTableViewCell.m
//  Artegrams
//
//  Created by Sujan on 8/3/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "EditProfileCollectionTableViewCell.h"
#import "ArtPreferenceCollectionViewCell.h"

@implementation EditProfileCollectionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
   
    self.artPreferenceCollectionView.delegate = self;
    self.artPreferenceCollectionView.dataSource = self;
    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    
}


#pragma mark - CollectionView data source

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return  _artArray.count;
    
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
   // NSLog(@"artArray  %@",_artArray);
   // NSLog(@"selectedartArray  %@",_selectedArtArray);
    
    static NSString *identifier = @"artPreferenceCell";
    
    ArtPreferenceCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    
//    UILabel *preferenceName = (UILabel*) [cell viewWithTag:1];
//
//    UIView *toggleButtonView = (UIView *) [cell viewWithTag:2];
//    
//    UISwitch *toggleButton = (UISwitch *) [cell viewWithTag:3];
    
//    ToggleView *toggleButton = [[ToggleView alloc]initWithFrame:CGRectMake(0, 0, toggleButtonView.bounds.size.width, toggleButtonView.bounds.size.height) toggleViewType:ToggleViewTypeNoLabel toggleBaseType:ToggleBaseTypeDefault toggleButtonType:ToggleButtonTypeDefault];
    
//    ToggleView *toggleButton = [[ToggleView alloc]initWithFrame:CGRectMake(0, 0, toggleButtonView.bounds.size.width, toggleButtonView.bounds.size.height) toggleViewType:ToggleViewTypeNoLabel toggleBaseType:ToggleBaseTypeChangeImage toggleButtonType:ToggleButtonTypeDefault];
//    
//    toggleButton.toggleDelegate = self;
    
    //[toggleButton onLeftButton:nil];
    
    
    if (self.artArray.count == 5  && indexPath.row == 5) {
        
        cell.preferenceName.text = @"";
       // toggleButtonView.hidden =YES;
        cell.toggleButton.hidden = YES;
        
    }else{
        cell.toggleButton.hidden = NO;
        //toggleButtonView.hidden =NO;
        cell.preferenceName.text = [[self.artArray objectAtIndex:indexPath.row] objectForKey:@"title"];
       // [toggleButtonView addSubview:toggleButton];
        cell.toggleButton.tag= indexPath.item;
        
        
        
        if ([[self.selectedArtArray valueForKey:@"id"] containsObject:[[self.artArray objectAtIndex:indexPath.item ] objectForKey:@"id"]] ) {
            
           // [toggleButton onRightButton:toggleButton];
            
            cell.toggleButton.on = YES;
            
            
        }else{
        
           // [toggleButton onLeftButton:toggleButton];
            cell.toggleButton.on = NO;
        }
      
    }
    
    return cell;
    
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(([UIScreen mainScreen].bounds.size.width-20)/2, 46);
    
    
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
}

- (IBAction)toggleButtonAction:(UISwitch *)sender {
    
    if([sender isOn]){
        
//        NSLog(@"Switch is ON");
//        NSMutableDictionary *tempDic=[[NSMutableDictionary alloc] initWithDictionary:[self.artArray objectAtIndex:[sender tag]]];
//        [tempDic setObject:[NSNumber numberWithInt:1] forKey:@"is_selected"];
//        
//        [self.artArray replaceObjectAtIndex:[sender tag] withObject:tempDic];
//        
        
        [self.selectedArtArray addObject:[self.artArray objectAtIndex:[sender tag]]];
        
    } else{
        
//        NSLog(@"Switch is OFF");
//        NSMutableDictionary *tempDic=[[NSMutableDictionary alloc] initWithDictionary:[self.artArray objectAtIndex:[sender tag]]];
//        [tempDic setObject:[NSNumber numberWithInt:0] forKey:@"is_selected"];
//        
//        [self.artArray replaceObjectAtIndex:[sender tag] withObject:tempDic];
        
          [self.selectedArtArray removeObject:[self.artArray objectAtIndex:[sender tag]]];
    }
    
}


//#pragma mark - ToggleViewDelegate
//
//- (void)selectLeftButton:(id)sender
//{
//    NSLog(@"LeftButton Selected %ld",(long)[sender tag]);
//    
//    NSMutableDictionary *tempDic=[[NSMutableDictionary alloc] initWithDictionary:[self.artArray objectAtIndex:[sender tag]]];
//    [tempDic setObject:[NSNumber numberWithInt:0] forKey:@"is_selected"];
//    
//    [self.artArray replaceObjectAtIndex:[sender tag] withObject:tempDic];
//    
//    
//
//}
//
//- (void)selectRightButton:(id)sender
//{
//    NSLog(@"RightButton Selected %ld",[sender tag]);
//      NSMutableDictionary *tempDic=[[NSMutableDictionary alloc] initWithDictionary:[self.artArray objectAtIndex:[sender tag]]];
//    [tempDic setObject:[NSNumber numberWithInt:1] forKey:@"is_selected"];
//    
//    [self.artArray replaceObjectAtIndex:[sender tag] withObject:tempDic];
//    
//    
//}

@end
