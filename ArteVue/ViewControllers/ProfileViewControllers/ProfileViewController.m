//
//  ProfileViewController.m
//  Artegram
//
//  Created by Tanvir Palash on 6/10/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//
#import "AppDelegate.h"
#import "ProfileViewController.h"

#import "TabBarViewController.h"
#import "PersonalGalleryViewController.h"
#import "EditProfileViewController.h"
#import "ViewPostViewController.h"
#import "LogInViewController.h"
#import "Constants.h"
#import "HexColors.h"
#import "UserAccount.h"
#import "ServerManager.h"
#import "LikersViewController.h"
#import "SettingViewController.h"
#import "PinPostsViewController.h"

#import "NSDictionary+NullReplacement.h"
#import "NSArray+NullReplacement.h"
#import "DGActivityIndicatorView.h"
#import "ActivityListViewController.h"
#import "DemoMessagesViewController.h"


#import <Realm/Realm.h>
#import "Posts.h"
#import "UserPost.h"

#import "ProfileDetailsHeaderView.h"


@interface ProfileViewController (){

    NSArray* title;
    PTPusher *pusherClient;
    NSMutableArray *userPosts;
    NSMutableArray *userTagPosts;
    NSMutableDictionary *userInfo;
    UIImage *chosenImage;
    
    
    
    RLMRealmConfiguration *realmv1Configuration;
    NSMutableArray *downloadingIds;

    CGPoint lastScrollViewContentOffset;
    CGFloat requiredCollectionViewSize;
    CGFloat expectedCollectionViewHeight;
    
    NSMutableArray *postUserDataSource;
    NSMutableArray *postTagDataSource;
    
    BOOL islastItemReached;
    int pageIndex;
   
}

@property (nonatomic) DGActivityIndicatorView *activityIndicatorView;

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    pageIndex=1;
    
    lastScrollViewContentOffset.y=0;
   // self.containerScrollView.delegate=self;
    title = [[NSArray alloc]initWithObjects:@"Posts",@"Pins",@"Followers",@"Following", nil];

   
    pusherClient = [PTPusher pusherWithKey:pusher_app_key delegate:self encrypted:YES cluster:@"ap1"];
    
    PTPusherChannel *channel = [pusherClient subscribeToChannelNamed:[NSString stringWithFormat:@"%i-activity-channel",[UserAccount sharedManager].userId]];
    
    [channel bindToEventNamed:@"all-activities" handleWithBlock:^(PTPusherEvent *channelEvent) {
        
        [UserAccount sharedManager].pendingActivityCount++;
       // NSLog(@"message received: %i",  [UserAccount sharedManager].pendingActivityCount);
        
      //  [self updateBadgeValue];
        [self.collectionViewOfPosts.collectionViewLayout invalidateLayout];
 
    }];
    [pusherClient connect];
   
    
    self.activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)DGActivityIndicatorAnimationTypeTriplePulse tintColor:[UIColor blackColor]];
    CGFloat width = self.view.bounds.size.width / 5.0f;
    CGFloat height = self.view.bounds.size.width / 5.0f;
    
    self.activityIndicatorView.frame = CGRectMake(self.view.bounds.size.width/2-width/2,self.view.bounds.size.height/2-height/2+50, width, height);
    [self.view addSubview:self.activityIndicatorView];
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBadgeValue) name:@"updateActivityBubble" object:nil];
    
    
    [[NSFileManager defaultManager] removeItemAtURL:[RLMRealmConfiguration defaultConfiguration].fileURL error:nil];
    
    NSURL *defaultRealmURL = [RLMRealmConfiguration defaultConfiguration].fileURL;
    NSURL *defaultRealmParentURL = [defaultRealmURL URLByDeletingLastPathComponent];
    NSURL *realmv1URL = [defaultRealmParentURL URLByAppendingPathComponent:@"defaultv1.realm"];
    
    realmv1Configuration = [[RLMRealmConfiguration defaultConfiguration] copy];
    realmv1Configuration.fileURL = realmv1URL;
    downloadingIds=[[NSMutableArray alloc] init];
    
    
    
   
    //Reuse//
    //setting corner radius and border width
    
  //  [self.view layoutIfNeeded];
 
   // self.editProfileButton.layer.borderWidth = 1.1f;
   //self.editProfileButton.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    
    [self.view layoutIfNeeded];
 
    //self.profilePicture.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    
    //setting delegate and datasource
    
    // followersCountCollectionView
    
   
       // collectionViewOfPosts
    
    
    self.collectionViewOfPosts.delegate = self;
    self.collectionViewOfPosts.dataSource = self;
    self.collectionViewOfPosts.prefetchDataSource=self;
    
    //hide views
    
    self.viewForEvents.hidden = YES;
    self.viewForUserPhotos.hidden = YES;
    
    if (self.isUserOrFollower) {
        
        if(self.userId == [UserAccount sharedManager].userId)
        {
            //self.editButtonView.hidden = NO;
            self.backButton.hidden = NO;
            self.optionButton.hidden=NO;
            self.otherOptionButton.hidden = YES;
            
        }else
        {
            self.backButton.hidden = NO;
            self.optionButton.hidden=YES;
            self.otherOptionButton.hidden = NO;
        }
        
        
    }else{
        
        self.otherOptionButton.hidden = YES;
        self.optionButton.hidden=NO;
        self.userId = [UserAccount sharedManager].userId;
    }
    
    
   
    userPosts=[[NSMutableArray alloc] init];
    userInfo = [[NSMutableDictionary alloc]init];
    userTagPosts=[[NSMutableArray alloc] init];
    postUserDataSource=[[NSMutableArray alloc] init];
    postTagDataSource=[[NSMutableArray alloc] init];
    //temporaty
    
    self.currentUserId = [UserAccount sharedManager].userId;
    
    
    self.selectedTab=1;
    
  //  [SDWebImageManager sharedManager].delegate=self;
    
}

-(void)viewWillAppear:(BOOL)animated
{
   // NSLog(@"viewWillAppear");
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate setShouldRotate:NO];
    
    self.refreshButton.hidden=YES;
    // [self updateBadgeValue];
    
    
    
    for (int i=101; i<=104; i++) {
        
        UIButton *random1 = (UIButton *)[self.view viewWithTag:i];
        random1.selected=NO;
    }
    [self.activityIndicatorView startAnimating];
    [self requestUserInfo];
    
    
    
    if(self.selectedTab==1)
    {
        UIButton *random = (UIButton *)[self.view viewWithTag:101];
        random.selected=YES;
        
        
        
        
    }else if(self.selectedTab==3)
    {
        UIButton *random = (UIButton *)[self.view viewWithTag:103];
        random.selected=YES;
        
        self.collectionViewOfPosts.hidden = YES;
        //self.viewForEvents.hidden = NO;
        self.viewForUserPhotos.hidden = YES;
        //self.containerScrollView.bounces=YES;
        //[self.activityIndicatorView stopAnimating];
        
    }
    else if(self.selectedTab==4)
    {
        UIButton *random = (UIButton *)[self.view viewWithTag:104];
        random.selected=YES;
        
    }
    
}
-(void) viewDidAppear:(BOOL)animated
{
 //   NSLog(@"viewDidAppear");
    
    if ([[userInfo objectForKey:@"is_account_private"] integerValue] &&  self.userId != [UserAccount sharedManager].userId && ![[userInfo objectForKey:@"is_following"] integerValue])
    {
        [self performSelector:@selector(updateUserDetailsView)
                   withObject:nil
                   afterDelay:1.0];
    }
    
}
/*
-(void) viewDidAppear:(BOOL)animated
{
   // NSLog(@"user id :%d",self.userId);
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate setShouldRotate:NO];
 
    self.refreshButton.hidden=YES;
   // [self updateBadgeValue];
    
    
    
    for (int i=101; i<=104; i++) {
        
        UIButton *random1 = (UIButton *)[self.view viewWithTag:i];
        random1.selected=NO;
    }
    [self.activityIndicatorView startAnimating];
    [self requestUserInfo];
        
       
    
    if(self.selectedTab==1)
    {
        UIButton *random = (UIButton *)[self.view viewWithTag:101];
        random.selected=YES;
        
        
        
        
    }else if(self.selectedTab==3)
    {
        UIButton *random = (UIButton *)[self.view viewWithTag:103];
        random.selected=YES;
        
        self.collectionViewOfPosts.hidden = YES;
        self.viewForEvents.hidden = NO;
        self.viewForUserPhotos.hidden = YES;
        //self.containerScrollView.bounces=YES;
        //[self.activityIndicatorView stopAnimating];
        
    }
    else if(self.selectedTab==4)
    {
        UIButton *random = (UIButton *)[self.view viewWithTag:104];
        random.selected=YES;
        
    }
    
}
*/

-(void)viewWillDisappear:(BOOL)animated
{
  //  [self.collectionViewOfPosts reloadData];
    self.viewForEvents.hidden=YES;
    downloadingIds=[[NSMutableArray alloc] init];
    [[SDWebImageDownloader sharedDownloader] cancelAllDownloads];
    postUserDataSource=nil;
    postTagDataSource=nil;
    [self.collectionViewOfPosts reloadData];
   //  [[AFHTTPRequestOperationManager manager].operationQueue cancelAllOperations];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void) requestUserInfo{
    
    int userIdForLoading=0;
    if (self.isUserOrFollower) {
        
        userIdForLoading=self.userId;
    }else{
        
        userIdForLoading=self.currentUserId;
    }
   
    [[ServerManager sharedManager] getUserDetailsForId:userIdForLoading WithCompletion:^(BOOL success, NSMutableDictionary *responseObject) {
        
       
        if ( responseObject!=nil) {
            
            
          ///.///
            userInfo = [[NSMutableDictionary alloc] initWithDictionary:[responseObject dictionaryByReplacingNullsWithBlanks]];
            NSLog(@"Response: %@", userInfo);
      
            if ([[userInfo objectForKey:@"is_account_private"] integerValue] &&  self.userId != [UserAccount sharedManager].userId && ![[userInfo objectForKey:@"is_following"] integerValue])
            {
                
            //    dispatch_async(dispatch_get_main_queue(), ^(void){
                    //Run UI Updates
                    [self.activityIndicatorView stopAnimating];
                    self.privateAccountMsg.hidden=NO;
                    
                    self.refreshButton.hidden=YES;
                    
                    [self.collectionViewOfPosts reloadData];
                    [self.collectionViewOfPosts.collectionViewLayout invalidateLayout];
                    
             //   });
                
            }
            else
            {
                [self updateUserDetailsView];
                
                
                self.privateAccountMsg.hidden=YES;
                
                if(self.selectedTab==1)
                {
                    [self makeRequest];
                    
                }else if ( self.selectedTab==4)
                {
                    [self makeRequestForTag];
                    
                }else
                {
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //Run UI Updates
                        [self.activityIndicatorView stopAnimating];
                        
                    });
                    
                }
                
            }

        }
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.activityIndicatorView stopAnimating];
                
                self.view.userInteractionEnabled=YES;
                self.privateAccountMsg.hidden=YES;
                
                self.refreshButton.hidden=NO;
                
            });
            
        }
    }];
    
 

}

-(void) updateUserDetailsView
{
    CGFloat userNameLabelHeight=0;
    CGFloat userTypeLabelHeight=0;
    CGFloat bioLabelHeight=0;
    CGFloat webSiteLabelHeight=0;
    
    UIFont *NameFont=[UIFont fontWithName:@"AzoSans-Medium" size:16];
    UIFont *typeFont=[UIFont fontWithName:@"AzoSans-Regular" size:13];
    UIFont *websiteFont=[UIFont fontWithName:@"AzoSans-Italic" size:12];
    
    
    CGSize constraint = CGSizeMake(75+50+70, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [[userInfo objectForKey:@"username"] boundingRectWithSize:constraint
                                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                                               attributes:@{NSFontAttributeName:NameFont}
                                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    userNameLabelHeight = size.height;
    
    //NSLog(@"profileNameLabel newHeight %f",userNameLabelHeight);
    
    
    //get text size
    
    
    CGSize userTypeLabelconstraint = constraint;
    CGSize userTypeLabelsize;
    
    NSStringDrawingContext *userTypeLabelcontext = [[NSStringDrawingContext alloc] init];
    CGSize userTypeLabelboundingBox = [[userInfo objectForKey:@"user_type"] boundingRectWithSize:userTypeLabelconstraint
                                                                            options:NSStringDrawingUsesLineFragmentOrigin
                                                                         attributes:@{NSFontAttributeName:typeFont}
                                                                            context:userTypeLabelcontext].size;
    
    userTypeLabelsize = CGSizeMake(ceil(userTypeLabelboundingBox.width), ceil(userTypeLabelboundingBox.height));
    
    userTypeLabelHeight = userTypeLabelsize.height;
    
    //NSLog(@"userTypeLabel newHeight %f",userTypeLabelHeight);
    
    if(![[userInfo objectForKey:@"biography"] isEqual:[NSNull null]] && ![[userInfo objectForKey:@"biography"] isEqual:@""])
    {
        
        CGSize constraint = CGSizeMake([UIScreen mainScreen].bounds.size.width-16, CGFLOAT_MAX);
        CGSize size;
        
        NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
        CGSize boundingBox = [[userInfo objectForKey:@"biography"] boundingRectWithSize:constraint
                                                             options:NSStringDrawingUsesLineFragmentOrigin
                                                          attributes:@{NSFontAttributeName:typeFont}
                                                             context:context].size;
        
        size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
        
        bioLabelHeight = size.height;
        
        NSLog(@"userBioLabel newHeight %f",bioLabelHeight);
    }
    
    if(![[userInfo objectForKey:@"website"] isEqual:[NSNull null]] && ![[userInfo objectForKey:@"website"] isEqual:@""])
    {
        
        CGSize constraint = CGSizeMake([UIScreen mainScreen].bounds.size.width-16, CGFLOAT_MAX);
        CGSize size;
        
        NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
        CGSize boundingBox = [[userInfo objectForKey:@"website"] boundingRectWithSize:constraint
                                                                options:NSStringDrawingUsesLineFragmentOrigin
                                                             attributes:@{NSFontAttributeName:websiteFont}
                                                                context:context].size;
        
        size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
        
        webSiteLabelHeight = size.height;
        
        
    }
    
    CGFloat totlaLabelHeight = userNameLabelHeight + userTypeLabelHeight + bioLabelHeight + webSiteLabelHeight;
    
    //NSLog(@"totlaLabelHeight totlaLabelHeight %f",totlaLabelHeight + 25 );
    
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
   flowLayout.collectionView.collectionViewLayout= self.collectionViewOfPosts.collectionViewLayout;
  
    flowLayout.headerReferenceSize = CGSizeMake(0.0f, totlaLabelHeight+25+40+45+75+15);
    flowLayout.minimumLineSpacing=1.0f;
    flowLayout.minimumInteritemSpacing=1.0f;
    flowLayout.itemSize=CGSizeMake(([UIScreen mainScreen].bounds.size.width-2)/3, ([UIScreen mainScreen].bounds.size.width-2)/3);
    
    [UIView animateWithDuration:0.2 animations:^{
        
        [self.collectionViewOfPosts setCollectionViewLayout:flowLayout animated:YES];
        self.eventViewHeightConstraint.constant=[UIScreen mainScreen].bounds.size.height-49-66-( totlaLabelHeight+25+40+45+75+15);
        
        [self.collectionViewOfPosts.collectionViewLayout invalidateLayout];
        
        if(self.selectedTab==3)
        {
            self.viewForEvents.hidden=NO;
        }
        
       // [self.view layoutIfNeeded];
        
       // [self.collectionViewOfPosts reloadData];
    }];
   
    
   //     self.collectionViewOfPosts.collectionViewLayout=flowLayout;
        
        
    
    

}


-(void) makeRequest {

    if (pageIndex<=0) {
        pageIndex=1;
    }
    if (self.isUserOrFollower && self.userId != [UserAccount sharedManager].userId) {
        
        [[ServerManager sharedManager] getUserPostForId:self.userId inPage:pageIndex WithCompletion:^(BOOL success, NSMutableDictionary *responseObject) {
            
            
            if ( responseObject!=nil) {
                //NSLog(@"Response: %@", responseObject);
                
                //userPosts=responseObject;
                userPosts=[[responseObject  objectForKey:@"data"] mutableCopy];
                
                
//                if(userPosts.count)
//                {
//                    if(pageIndex==1)
//                    {
//                        userPosts=[[NSMutableArray alloc] init];
//                        userPosts=[[responseObject  objectForKey:@"data"] mutableCopy];
//                    }else
//                    {
//                        [userPosts addObjectsFromArray:[[responseObject  objectForKey:@"data"] mutableCopy]];
//                        
//                    }
//                    //pageIndex++;
//                    //islastItemReached=NO;
//                }
//                else
//                {
//                    //islastItemReached=YES;
//                }
                
                self.noPostLabel.hidden=YES;
                self.refreshButton.hidden=YES;
                
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                    //Background Thread
                    
                    [self configueUserPostModel];
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //Run UI Updates
                        [self.activityIndicatorView stopAnimating];
                        
                        
                        [self.collectionViewOfPosts reloadData];
                    });
                });
                
                //[self.collectionViewOfPosts reloadData];
                
               
            }
            else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    self.noPostLabel.hidden=YES;
                    self.view.userInteractionEnabled=YES;
                    
                    [self.activityIndicatorView stopAnimating];
                    
                    
                });
                
            }
        }];
        
        
    }else{
        
        [[ServerManager sharedManager] getCurrentUserPostwithPage:pageIndex withCompletion:^(BOOL success, NSMutableDictionary *responseObject) {
            
            
            if ( responseObject!=nil) {
                //NSLog(@"Response: %@", responseObject);
                
                //userPosts=responseObject;
                userPosts=[[responseObject  objectForKey:@"data"] mutableCopy];
                
                //  userPosts=[[[responseObject reverseObjectEnumerator] allObjects ] mutableCopy];
                
                   if (userPosts.count) {
                        self.noPostLabel.hidden=YES;
                    }
                    else
                    {
                        self.noPostLabel.hidden=NO;
                    }
              
                
                
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                    //Background Thread
                    
                    [self configueUserPostModel];
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //Run UI Updates
                        [self.activityIndicatorView stopAnimating];
                        
                        
                        [self.collectionViewOfPosts reloadData];
                    });
                });
                
                //[self.collectionViewOfPosts reloadData];
                
                self.refreshButton.hidden=YES;
            }
            else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    self.noPostLabel.hidden=YES;
                    self.view.userInteractionEnabled=YES;
                    
                    [self.activityIndicatorView stopAnimating];
                    
                    
                });
                
            }
        }];
        
    }
    
    

}




-(void) makeRequestForTag {
    
    if (pageIndex<=0) {
        pageIndex=1;
    }

    int userId;
    
    
    if (self.isUserOrFollower) {
        userId=self.userId;
    }else{
        userId=self.currentUserId;
    }
    
    [[ServerManager sharedManager] getTaggedPostForId:userId WithCompletion:^(BOOL success, NSMutableDictionary *responseObject) {
        
        
        if ( responseObject!=nil) {
            //NSLog(@"Response: %@", responseObject);
            
            
            userTagPosts=[[responseObject  objectForKey:@"data"] mutableCopy];
            
            
            self.collectionViewOfPosts.hidden = NO;
            
            CGFloat postCount=(CGFloat)userTagPosts.count;
            
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                //Background Thread
                
                [self configueUserPostModel];
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    //Run UI Updates
                    [self.activityIndicatorView stopAnimating];
                    
                    [self.collectionViewOfPosts reloadData];
                });
            });
        }
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                self.noPostLabel.hidden=YES;
                self.view.userInteractionEnabled=YES;
                
                [self.activityIndicatorView stopAnimating];
                
                
                [self.collectionViewOfPosts reloadData];
                
                
            });
            
        }
    }];
    
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    // Change 10.0 to adjust the distance from bottom
    
    if (maximumOffset - currentOffset <= 150.0 && !islastItemReached ) {
        
        NSLog(@"almost end");
//        if(self.selectedTab==1)
//        {
//            [self makeRequest];
//            
//        }
//        else if(self.selectedTab==4)
//        {
//            [self makeRequestForTag];
//            
//        }
    }
    
}

-(void)configueUserPostModel
{

    NSMutableArray *dataArray=[[NSMutableArray alloc] init];
    if( self.selectedTab==1)
    {
        postUserDataSource=[[NSMutableArray alloc] init];
        dataArray=userPosts;
    }
    else if( self.selectedTab==4)
    {
        postTagDataSource=[[NSMutableArray alloc] init];
        dataArray=userTagPosts;
        
    }
    
   // NSLog(@"configueUserPostModel dataArray %lu",(unsigned long)dataArray.count);
    for (int i=0; i<dataArray.count; i++) {
        
        UserPost *singlePost=[UserPost new];
        
        singlePost.postId=[[[dataArray objectAtIndex:i] objectForKey:@"id"] intValue];
        
        
        singlePost.aspectRatio=[[[dataArray objectAtIndex:i] objectForKey:@"aspect_ratio"] floatValue];
        
        if (![[[dataArray objectAtIndex:i] objectForKey:@"description"] isEqual:[NSNull null]] && ![[[dataArray objectAtIndex:i] objectForKey:@"description"] isEqualToString:@""]) {
            
            singlePost.postDescription = [[dataArray objectAtIndex:i] objectForKey:@"description"];
            
        }else{
            singlePost.postDescription = @"";
        }
        
        singlePost.artistName=[[dataArray objectAtIndex:i] objectForKey:@"artist"];
        // singlePost.userId;
        singlePost.userName=[[[dataArray objectAtIndex:i] objectForKey:@"owner"] objectForKey:@"username"];
        singlePost.addressTitle=[[dataArray objectAtIndex:i] objectForKey:@"address_title"];
        singlePost.postTime=[[dataArray objectAtIndex:i] objectForKey:@"created_at"];
        singlePost.price=[[[dataArray objectAtIndex:i] objectForKey:@"price"] floatValue];
        singlePost.has_buy_btn=[[[dataArray objectAtIndex:i] objectForKey:@"has_buy_btn"] intValue];
        
        
        if(![[[[dataArray objectAtIndex:i] objectForKey:@"owner"] objectForKey:@"profile_picture"] isEqual:[NSNull null]] && ![[[[dataArray objectAtIndex:i] objectForKey:@"owner"] objectForKey:@"profile_picture"]isEqualToString:@""])
            singlePost.userImage=[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[[dataArray objectAtIndex:i] objectForKey:@"owner"] objectForKey:@"profile_picture"]];
        
        
        
        //singlePost.postComments = [[NSMutableArray alloc] initWithArray: [[dataArray objectAtIndex:i] objectForKey:@"post_comments"]];
        
        singlePost.postLikesCount=[[[dataArray objectAtIndex:i] objectForKey:@"like_count"] intValue];
        singlePost.pinCount=[[[dataArray objectAtIndex:i] objectForKey:@"pin_count"] intValue];
        
        singlePost.is_liked=[[[dataArray objectAtIndex:i] objectForKey:@"is_liked"] intValue];
        singlePost.is_pinned=[[[dataArray objectAtIndex:i] objectForKey:@"is_pinned"] intValue];
        
        //
        //          singlePost.googlePlaceId;
        //          singlePost.longitude;
        //          singlePost.latitude;
        
        
        RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
        
        RLMResults *result=[Posts objectsInRealm:realm where:[NSString stringWithFormat:@"postId = %i",singlePost.postId]];
        
        
       // NSLog(@"result result %@",result);
        if(result.count)
        {
            Posts *post=[[Posts alloc] init];
            post=[result objectAtIndex:0];
            
            
            singlePost.postImage=[UIImage imageWithData:post.imageData];
            
        }
        else
        {
            singlePost.postImage=nil;
            
            
        }
        
        if( self.selectedTab==1)
        {
            [postUserDataSource addObject:singlePost];
        }
        else if( self.selectedTab==4)
        {
            [postTagDataSource addObject:singlePost];
            
        }
        
   //     [self.collectionViewOfPosts reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]]];
    }
}


#pragma mark - CollectionView data source

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if(collectionView.tag == 1)
    {
            return 4;
    }
    else
    {
        NSLog(@"numberOfItemsInSection");
        self.collectionViewOfPosts.hidden=NO;
        if([[userInfo objectForKey:@"is_account_private"] integerValue] &&  self.userId != [UserAccount sharedManager].userId && ![[userInfo objectForKey:@"is_following"] integerValue])
        {
            
            return 1;
            
        }

        else if (self.selectedTab== 1)
        {
            return  postUserDataSource.count;
            
        }else if (self.selectedTab== 4)
        {
            return  postTagDataSource.count;
        }
        else
            return 0;
        
    }
    
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
   // NSLog(@"cellForItemAtIndexPath %ld",(long)indexPath.item);
    if (collectionView.tag == 1)
    {
        
     //   NSLog(@"userInfo %@",userInfo);
        
        static NSString *identifier = @"followerCountCell";
        
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        UILabel *countLabel = (UILabel*) [cell viewWithTag:1];
        UILabel *titleLabel = (UILabel*) [cell viewWithTag:2];
        
        titleLabel.text = [title objectAtIndex:indexPath.row];
        
        
        if (indexPath.row == 0) {
            
            countLabel.text = @"";
            if ([userInfo objectForKey:@"post_count"])
            {
                countLabel.text = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"post_count"]];
            }
            
        }else if (indexPath.row == 1){
            countLabel.text = @"";
            if ([userInfo objectForKey:@"pin_count"]) {
                
                countLabel.text = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"pin_count"]];
            }
            
            
        } else if (indexPath.row == 2){
            countLabel.text = @"";
            if ([userInfo objectForKey:@"follower_count"]) {
                
                countLabel.text = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"follower_count"]];
            }
            
            
        }else if (indexPath.row == 3){
            countLabel.text = @"";
            
            if ([userInfo objectForKey:@"following_count"]) {
                
                countLabel.text = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"following_count"]];
            }
            
            
        }
        
        return cell;
    }else
    {
        static NSString *identifier = @"postsPhotoCell";
        
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
      
        
        
        UIImageView *postsImage = (UIImageView*) [cell viewWithTag:3];
        UIActivityIndicatorView *activityInd = (UIActivityIndicatorView*) [cell viewWithTag:2];
        UIButton *dollerButton=(UIButton*)[cell viewWithTag:1];
        dollerButton.layer.zPosition=1000;
        postsImage.image=nil;
        

       
        
        
        if (self.selectedTab== 1 && indexPath.row < postUserDataSource.count)
        {
            activityInd.hidden=NO;
            [activityInd startAnimating];

            UserPost *singlePost=[UserPost new];
            singlePost=[postUserDataSource objectAtIndex:indexPath.row];
            
            if(singlePost.has_buy_btn)
            {
                dollerButton.hidden=NO;
            }
            else
            {
                
                dollerButton.hidden=YES;
            }
            
            if(singlePost.postImage==nil)
            {
                
                if(![downloadingIds containsObject: [NSNumber numberWithInt:singlePost.postId]])
                {
                    [downloadingIds addObject:[NSNumber numberWithInt:singlePost.postId]];
                    
                    SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
                    [SDWebImageDownloader sharedDownloader].maxConcurrentDownloads=1;
                    
                    [downloader downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[userPosts objectAtIndex:indexPath.row] objectForKey:@"image"] ]]
                                             options:0
                                            progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                // progression tracking code
                                                // NSLog(@"receivedSize %ld expectedSize %ld",(long)receivedSize,(long)expectedSize);
                                                
                                            }
                                           completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                               if (image && finished) {
                                                   // do something with image
                                                   
                                                   //   Posts *post = [[Posts alloc] init];
                                                   
                                                   RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
                                                   
                                                   
                                                   [realm beginWriteTransaction];
                                                   [Posts createOrUpdateInRealm:realm withValue:@{@"primId": [NSString stringWithFormat:@"%i",singlePost.postId]
                                                                                                  ,@"postId": [NSNumber numberWithInt:singlePost.postId],
                                                                                                  @"imageData": data}];
                                                   
                                                   [realm commitWriteTransaction];
                                                   
                                                   [downloadingIds removeObject:[NSNumber numberWithInt:singlePost.postId]];
                                                   
                                                   
                                                   if(indexPath.row < postUserDataSource.count)
                                                   {
                                                       singlePost.postImage=[UIImage imageWithData:data];
                                                       
                                                       
                                                       [postUserDataSource replaceObjectAtIndex:indexPath.row withObject:singlePost];
                                                       
                                                   }
                                                   
                                                   NSLog(@"singlePost %i feedList %@",singlePost.postId,[[userPosts objectAtIndex:indexPath.row] objectForKey:@"id"]);
                                                   
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       
                                                       postsImage.image=image;
                                                       [activityInd stopAnimating];
                                                       
                                                       
                                                   });
                                                  
                                               }
                                               
                                           }];
                    
                }
                else
                    
                {
                  //  NSLog(@"downloading %@",downloadingIds);
                }
            
            }
            else
            {
                [activityInd stopAnimating];
                postsImage.image=singlePost.postImage;
                
            }
            

            
        }
        else  if (self.selectedTab== 4 && indexPath.row < postTagDataSource.count)
        {
            activityInd.hidden=NO;
            [activityInd startAnimating];

            
            UserPost *singlePost=[UserPost new];
            singlePost=[postTagDataSource objectAtIndex:indexPath.row];
            if(singlePost.has_buy_btn)
            {
                dollerButton.hidden=NO;
            }
            else
            {
                
                dollerButton.hidden=YES;
            }

            postsImage.image=singlePost.postImage;
            
            
            if(singlePost.postImage==nil)
            {
                if(![downloadingIds containsObject: [NSNumber numberWithInt:singlePost.postId]])
                {
                    [downloadingIds addObject:[NSNumber numberWithInt:singlePost.postId]];
                    
                    SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
                    [SDWebImageDownloader sharedDownloader].maxConcurrentDownloads=1;
                    
                    [downloader downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[userTagPosts objectAtIndex:indexPath.row] objectForKey:@"image"] ]]
                                             options:0
                                            progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                // progression tracking code
                                                // NSLog(@"receivedSize %ld expectedSize %ld",(long)receivedSize,(long)expectedSize);
                                                
                                            }
                                           completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                               if (image && finished) {
                                                   // do something with image
                                                   
                                                   //   Posts *post = [[Posts alloc] init];
                                                   
                                                   RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
                                                   
                                                   
                                                   [realm beginWriteTransaction];
                                                   [Posts createOrUpdateInRealm:realm withValue:@{@"primId": [NSString stringWithFormat:@"%i",singlePost.postId]
                                                                                                  ,@"postId": [NSNumber numberWithInt:singlePost.postId],
                                                                                                  @"imageData": data}];
                                                   
                                                   [realm commitWriteTransaction];
                                                   
                                                   [downloadingIds removeObject:[NSNumber numberWithInt:singlePost.postId]];
                                                   
                                                   
                                                   if(indexPath.row < postTagDataSource.count)
                                                   {
                                                       singlePost.postImage=[UIImage imageWithData:data];
                                                       
                                                       
                                                       [postTagDataSource replaceObjectAtIndex:indexPath.row withObject:singlePost];
                                                       
                                                   }
                                                   
                                                   NSLog(@"singlePost %i feedList %@",singlePost.postId,[[userTagPosts objectAtIndex:indexPath.row] objectForKey:@"id"]);
                                                   
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       
                                                       postsImage.image=image;
                                                       [activityInd stopAnimating];
                                                       
                                                       
                                                   });
                                                   
                                                   
                                                   
                                               }
                                               
                                           }];
                    
                }
                
            }
            else
            {
                [activityInd stopAnimating];
            }
            
            
            
        }

    
       // NSLog(@"image link : %@/%@",SERVER_BASE_API_URL,[[userPosts objectAtIndex:indexPath.row] objectForKey:@"image"]);
        return cell;
    }
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.tag == 2)
    {
     //    NSLog(@" collection view %lf ",([UIScreen mainScreen].bounds.size.width-2)/3);
        return CGSizeMake(([UIScreen mainScreen].bounds.size.width-2)/3, ([UIScreen mainScreen].bounds.size.width-2)/3);
        
    }else
    {
        //  NSLog(@" collection view %@ ",collectionView);
        //  NSLog(@" followersCountCollectionView view %f ",(collectionView.layer.bounds.size.width-2)/3);
        
        return CGSizeMake((collectionView.layer.bounds.size.width-3)/4,40);
        
        
    }
}

//-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
//{
//    return 1.0f;
//}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
   // NSLog(@"viewForSupplementaryElementOfKind %@",userInfo);
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        ProfileDetailsHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        headerView.userInfo=[[NSMutableDictionary alloc] initWithDictionary:userInfo];
        headerView.userId=self.userId;
        [headerView populateData];
        
        if(self.selectedTab==1)
        {
            headerView.selectedTabPosition.constant=0;
            
        }
        
        if (self.isUserOrFollower) {
            
            if(self.userId == [UserAccount sharedManager].userId)
            {
                //self.editButtonView.hidden = NO;
                headerView.editProfileButton.hidden = NO;
                headerView.followButton.hidden = YES;
                [headerView.SecondTabButton setTitle:@"Activity" forState:UIControlStateNormal];
                [headerView.SecondTabButton setTitle:@"Activity" forState:UIControlStateSelected];
                
            }else
            {
                headerView.editProfileButton.hidden = YES;
                headerView.followButton.hidden = NO;
                headerView.cameraButton.hidden=YES;
                
                [headerView.SecondTabButton setTitle:@"Gallery" forState:UIControlStateNormal];
                [headerView.SecondTabButton setTitle:@"Gallery" forState:UIControlStateSelected];
                
            }
            
            
        }else{
            
            [headerView.SecondTabButton setTitle:@"Activity" forState:UIControlStateNormal];
            [headerView.SecondTabButton setTitle:@"Activity" forState:UIControlStateSelected];
            headerView.editProfileButton.hidden = NO;
            headerView.followButton.hidden = YES;
        }
        
        headerView.profileNameLabel.text = [userInfo objectForKey:@"username"];
        headerView.profileNameLabel.text = [userInfo objectForKey:@"username"];
        headerView.userTypeLabel.text = [userInfo objectForKey:@"user_type"];
        
        headerView.userTypeLabel.hidden=NO;
        
        if(![[userInfo objectForKey:@"biography"] isEqual:[NSNull null]])
        {
            headerView.userBio.text=[userInfo objectForKey:@"biography"];
            
        }
        
        if(![[userInfo objectForKey:@"website"] isEqual:[NSNull null]])
        {
            headerView.webSiteUrl.text=[userInfo objectForKey:@"website"];
            // self.webSiteUrlLabel.text=@"www.google.com";
        }
        
        if(chosenImage)
        {
            headerView.profilePicture.image=chosenImage;
        }else
        {
            if ([[userInfo objectForKey:@"profile_picture"] isEqual:[NSNull null]] || [[userInfo objectForKey:@"profile_picture"] isEqualToString:@""]) {
                
                [headerView.profilePicture sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,@"img/profile-holder.png"]]];
            }else{
                
                
                [headerView.profilePicture sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[userInfo objectForKey:@"profile_picture"]]]];
                
                
            }
        }
        
        
        //   [self.profilePicture sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL,[userInfo objectForKey:@"profile_picture"]]]];
        
        //set follow button state
        if([[userInfo objectForKey:@"is_following"] integerValue])
        {//2DCC70
            headerView.followButton.tintColor = [HXColor hx_colorWithHexString:@"2DCC70"];
            headerView.followButton.layer.borderColor = [[HXColor hx_colorWithHexString:@"2DCC70"]CGColor];
            headerView.followButton.selected=YES;
            
            
            
        }
        else{
            //007AFF
            headerView.followButton.tintColor = [HXColor hx_colorWithHexString:@"000000"];
            headerView.followButton.layer.borderColor = [[HXColor hx_colorWithHexString:@"000000"]CGColor];
            headerView.followButton.selected=NO;
            
            
        }
        
        headerView.followersCountCollectionView.delegate = self;
        headerView.followersCountCollectionView.dataSource = self;
        [headerView.followersCountCollectionView reloadData];
        reusableview = headerView;
        
        
    }
    
    
    
    return reusableview;
}

//-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
//{
//    NSLog(@"[self updateUserDetailsView] %f",[self updateUserDetailsView]);
//    return CGSizeMake([UIScreen mainScreen].bounds.size.width, 250);
//}

-(void)collectionView:(UICollectionView *)collectionView prefetchItemsAtIndexPaths:(NSArray<NSIndexPath *> *)indexPaths
{
   // NSLog(@"indexPaths %@",indexPaths);
    
    NSMutableArray *dataArray=[[NSMutableArray alloc] init];
    if( self.selectedTab==1)
    {
        dataArray=userPosts;
    }
    else if( self.selectedTab==4)
    {
        dataArray=userTagPosts;
        
    }
    
    
    int startingIndex=(int)[indexPaths objectAtIndex:0].row;
    
    
    for (int i=startingIndex; i<indexPaths.count+startingIndex; i++) {
        
        UserPost *singlePost=[UserPost new];
        singlePost=[postUserDataSource objectAtIndex:i];
        
        
        
        if(singlePost.postImage==nil)
        {
            
            if(![downloadingIds containsObject: [NSNumber numberWithInt:singlePost.postId]])
            {
                [downloadingIds addObject:[NSNumber numberWithInt:singlePost.postId]];
                
                SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
                [SDWebImageDownloader sharedDownloader].maxConcurrentDownloads=1;
                
                [downloader downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[dataArray objectAtIndex:i] objectForKey:@"image"] ]]
                                         options:0
                                        progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                            // progression tracking code
                                            // NSLog(@"receivedSize %ld expectedSize %ld",(long)receivedSize,(long)expectedSize);
                                            
                                        }
                                       completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                           if (image && finished) {
                                               // do something with image
                                               
                                               //   Posts *post = [[Posts alloc] init];
                                               
                                               RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
                                               
                                               
                                               [realm beginWriteTransaction];
                                               [Posts createOrUpdateInRealm:realm withValue:@{@"primId": [NSString stringWithFormat:@"%i",singlePost.postId]
                                                                                              ,@"postId": [NSNumber numberWithInt:singlePost.postId],
                                                                                              @"imageData": data}];
                                               
                                               [realm commitWriteTransaction];
                                               
                                               [downloadingIds removeObject:[NSNumber numberWithInt:singlePost.postId]];
                                               
                                               
                                               if(i < postUserDataSource.count)
                                               {
                                                   singlePost.postImage=[UIImage imageWithData:data];
                                                   
                                                   
                                                   [postUserDataSource replaceObjectAtIndex:i withObject:singlePost];
                                                   
                                               }
                                               
                                         //      NSLog(@"singlePost %i feedList %@",singlePost.postId,[[userPosts objectAtIndex:indexPath.row] objectForKey:@"id"]);
                                               
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                  
                                                  // NSLog(@"indexPathsForVisibleItems %@",[self.collectionViewOfPosts indexPathsForVisibleItems]);
                                                 //  NSLog(@"NSIndexPath %@",[NSIndexPath indexPathForItem:i inSection:0]);
                                                   
                                                   if([[self.collectionViewOfPosts indexPathsForVisibleItems] containsObject:[NSIndexPath indexPathForItem:i inSection:0]])
                                                   {
                                                       NSLog(@"reloading %d",i);
                                                       [self.collectionViewOfPosts reloadItem:i inSection:0];
                                                       
                                                   }
                                                   else{
                                                       //NSLog(@"Downloaded");
                                                       
                                                   }
                                                   
                                              //     [self.collectionViewOfPosts reloadItemsAtIndexPaths:[self.collectionViewOfPosts indexPathsForVisibleItems]];
                                              //     [self.collectionViewOfPosts reloadItem:i inSection:1];
                                               //    [self.collectionViewOfPosts reloadItemsAtIndexPaths:indexPaths];
                                                   
//                                                   UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:i inSection:1]];
//                                                   UIImageView *postsImage = (UIImageView*) [cell viewWithTag:3];
//                                                 //  UIActivityIndicatorView *activityInd = (UIActivityIndicatorView*) [cell viewWithTag:2];
//                                                   postsImage.image=singlePost.postImage;
                                                   
                                                   
                                               });
                                               
                                           }
                                           
                                       }];
                
            }
            
            
        }
       
    }
    
    
}

-(void)collectionView:(UICollectionView *)collectionView cancelPrefetchingForItemsAtIndexPaths:(NSArray<NSIndexPath *> *)indexPaths
{
    NSLog(@"cancelPrefetchingForItemsAtIndexPaths");
   // [self.collectionViewOfPosts reloadItemsAtIndexPaths:indexPaths];
}


#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (collectionView.tag == 2)
    {
        //UICollectionViewCell *cell=[collectionView cellForItemAtIndexPath:indexPath];
        
        //UIImageView *postsImage = (UIImageView*) [cell viewWithTag:3];
        
        ViewPostViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewPostViewController"];
        
        //controller.postImage = postsImage.image;
        
        if(self.selectedTab==1)
        {
            controller.indexNumber = indexPath.row;
            controller.userPostArray = userPosts;
            controller.isFromProfileWithTaggedPost=NO;
            
         //   controller.singlePostDic = [[NSMutableDictionary alloc] initWithDictionary:[userPosts objectAtIndex:indexPath.row]] ;
            
            
        }else if(self.selectedTab==4){
        
            controller.indexNumber = indexPath.row;
            controller.userPostArray = userTagPosts;
            
            controller.isFromProfileWithTaggedPost=YES;
            
      //    controller.singlePostDic = [[NSMutableDictionary alloc] initWithDictionary:[userTagPosts objectAtIndex:indexPath.row]];
        }
        
        
        controller.isFromGallery=NO;
        controller.isFromProfile = YES;
        controller.userId=self.userId;
        
        
        [self.navigationController pushViewController:controller animated:YES];
        
        
    }else{
    
        if (indexPath.row == 1) {
            
            PinPostsViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PinPostsViewController"];
            
            
            if (self.isUserOrFollower) {
                controller.userId = self.userId;
            }else
            {
                controller.userId = self.currentUserId;
            }
            
            [self.navigationController pushViewController:controller animated:YES];
            
        }
    
        else if (indexPath.row == 2) {
            
            LikersViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"LikersViewController"];
            
            controller.isFollower = YES;
            if (self.isUserOrFollower) {
                controller.userId = self.userId;
            }else
            {
                controller.userId = self.currentUserId;
            }
            
            [self.navigationController pushViewController:controller animated:YES];
            
        }else if (indexPath.row == 3){
        
        
            LikersViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"LikersViewController"];
            
            controller.isFolloweing = YES;
            if (self.isUserOrFollower) {
                
                controller.userId = self.userId;
            }else
            {
                controller.userId = self.currentUserId;
            }
            
            [self.navigationController pushViewController:controller animated:YES];


        }

    
    }
    
}

//Button ACtion

//- (IBAction)followButtonAction:(UIButton*)sender {
//    
//    sender.userInteractionEnabled = NO;
//    
//    
//    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
//    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
//    
//    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
//    [postData setObject:ACCESS_KEY forKey:@"access_key"];
//    [postData setObject:[NSNumber numberWithInt:self.userId] forKey:@"user_id"];
//    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
//    
//    NSLog(@"postData %@",postData);
//    
//    [apiLoginManager POST:[NSString stringWithFormat:@"%@/users/apiSwapFollowing",SERVER_BASE_API_URL] parameters:postData
//                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                      
//                      NSLog(@"Response: %@", responseObject);
//                      
//                      NSString * successMsg = [responseObject objectForKey:@"success"];
//                      
//                      if ([successMsg integerValue] == 1 ) {
//                          
//                          self.followButton.userInteractionEnabled = YES;
//                          
//                          NSLog(@"success msg %@",[responseObject objectForKey:@"success"]);
//                          
//                           sender.selected=!sender.selected;
//                          if (sender.selected) {
//                              
//                              self.followButton.tintColor = [HXColor hx_colorWithHexString:@"2DCC70"];
//                              self.followButton.layer.borderColor = [[HXColor hx_colorWithHexString:@"2DCC70"]CGColor];
//                              
//                          }else{
//                          
//                              self.followButton.tintColor = [HXColor hx_colorWithHexString:@"000000"];
//                              self.followButton.layer.borderColor = [[HXColor hx_colorWithHexString:@"000000"]CGColor];
//                          }
//                          
//                        
//                        [[NSNotificationCenter defaultCenter] postNotificationName:@"loadAllFeed" object:nil];
//                         
//                      }
//
//                  }
//     
//                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                      
//                      NSLog(@"Error: %@", error);
//                      
//                      self.followButton.userInteractionEnabled = YES;
//                      
//                      NSLog(@"%@",operation.responseString);
//                      
//                      
//                      
//                  }];
//    
//}


- (IBAction)editPrifileButtonAction:(id)sender {
    
    EditProfileViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileViewController"];
    
    
    [self.navigationController pushViewController:controller animated:YES];
    
}

- (IBAction)TabButtonAction:(UIButton*)sender {
    
    NSLog(@"TabButtonAction %ld",sender.tag);
    
    downloadingIds=[[NSMutableArray alloc] init];
    [[SDWebImageDownloader sharedDownloader] cancelAllDownloads];
    
    
    if ([[userInfo objectForKey:@"is_account_private"] integerValue] &&  self.userId != [UserAccount sharedManager].userId && ![[userInfo objectForKey:@"is_following"] integerValue])
    {
        NSLog(@"is_account_private in tabbar action");
        [UILabel animateWithDuration:0.4
                          animations:^{
                              self.privateAccountMsg.transform = CGAffineTransformMakeScale(1.1, 1.1);
                          }
                          completion:^(BOOL finished) {
                              [UILabel animateWithDuration:0.4
                                                animations:^{
                                                    self.privateAccountMsg.transform = CGAffineTransformIdentity;
                                                    
                                                }];
                          }];
        
      
    }
    else
    {
      //  NSLog(@"sender.tag %ld %d",(long)sender.tag,self.selectedTab);

        self.noPostLabel.hidden=YES;
        
        if (sender.tag==101 && self.selectedTab!=1) {
            
            NSLog(@"self.selectedTab %d",self.selectedTab);
            self.selectedTab=1;
            
            
          
            
            self.collectionViewOfPosts.hidden = NO;
            self.viewForEvents.hidden = YES;
            self.viewForUserPhotos.hidden = YES;
            
            if(self.userId == [UserAccount sharedManager].userId)
            {
                if (userPosts.count) {
                    self.noPostLabel.hidden=YES;
                }
                else
                {
                    self.noPostLabel.hidden=NO;
                }
                
            }
            else
            {
                self.noPostLabel.hidden=YES;
            }
         
            

            
            if(postUserDataSource.count)
            {
                [self.collectionViewOfPosts reloadData];
            }
            else
            {
                [self.activityIndicatorView startAnimating];
                
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                    //Background Thread
                    
                    [self configueUserPostModel];
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //Run UI Updates
                        [self.activityIndicatorView stopAnimating];
                        [self.collectionViewOfPosts reloadData];
                        
                    });
                });
            }
            
            
//            [UIView animateWithDuration:0.7f animations:^{
//                self.selectedTabXPostiotion.constant=0;
//                [self.view layoutIfNeeded];
//            }];
           
 
        }
        else if (sender.tag==102) {
            
            
            pageIndex=1;
            
            if (self.isUserOrFollower) {
                
                if(self.userId == [UserAccount sharedManager].userId)
                {
                    [UserAccount sharedManager].pendingActivityCount=0;
                    
                    [UserAccount sharedManager].lastActivityDate=[self getDateStringFromDate:[NSDate date]];
                    NSLog(@"[UserAccount sharedManager].lastActivityDate %@",[UserAccount sharedManager].lastActivityDate);
                    
                    ActivityListViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivityListViewController"];
                    [self.navigationController pushViewController:controller animated:YES];
                }else
                {
                    PersonalGalleryViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PersonalGalleryViewController"];
                    controller.userId=self.userId;
                    controller.galleryNameTitle=[userInfo objectForKey:@"gallery_name"];
                    controller.galleryDetailsText=[userInfo objectForKey:@"gallery_description"];
                    
                    
                    [self.navigationController pushViewController:controller animated:YES];
                    
                }
                
            }else{
                [UserAccount sharedManager].pendingActivityCount=0;

                [UserAccount sharedManager].lastActivityDate=[self getDateStringFromDate:[NSDate date]];
                NSLog(@"[UserAccount sharedManager].lastActivityDate %@",[UserAccount sharedManager].lastActivityDate);
                
                ActivityListViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivityListViewController"];
                [self.navigationController pushViewController:controller animated:YES];
                
            }
            
            [self.collectionViewOfPosts.collectionViewLayout invalidateLayout];
            
            
        }
        else if (sender.tag==103) {
            
            NSLog(@"self.selectedTab %d",self.selectedTab);
            
            self.selectedTab=3;
            
            
           // self.containerScrollView.bounces=YES;
            
            
            [self.collectionViewOfPosts reloadData];
            
            self.viewForEvents.hidden = NO;
            self.viewForUserPhotos.hidden = YES;
            

            
        }
        else if (sender.tag==104 && self.selectedTab!=4) {
            
            pageIndex=1;
            
            self.selectedTab=4;
            
            self.viewForEvents.hidden = YES;
            self.viewForUserPhotos.hidden = YES;
            
            NSLog(@" userTagPosts %lu",(unsigned long)userTagPosts.count);
            if(userTagPosts.count)
            {
                
                self.collectionViewOfPosts.hidden = NO;
                
                
                if(postTagDataSource.count)
                {
                    [self.collectionViewOfPosts reloadData];
                }
                else
                {
                    [self.activityIndicatorView startAnimating];
                    
                    
                    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                        //Background Thread
                        
                        [self configueUserPostModel];
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            //Run UI Updates
                            [self.activityIndicatorView stopAnimating];
                            
                            [self.collectionViewOfPosts reloadData];
                        });
                    });

                }
                
            }
            else
            {
                [self.activityIndicatorView startAnimating];
                [self makeRequestForTag];
            }
            
            
//            [UIView animateWithDuration:0.7f animations:^{
//                
//                self.selectedTabXPostiotion.constant=([UIScreen mainScreen].bounds.size.width/4)*3;
//                [self.view layoutIfNeeded];
//            }];
            
        }
        
        for (int i=101; i<=104; i++) {
            
            UIButton *random1 = (UIButton *)[self.view viewWithTag:i];
            random1.selected=NO;
        }
        sender.selected=!sender.selected;

    }

}


-(NSString *)getDateStringFromDate:(NSDate *)date
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy'-'MM'-'dd' 'HH':'mm':'ss"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    NSLog(@"%@", stringFromDate);
    return stringFromDate;
    
}


- (IBAction)ppCaptureAction:(id)sender {
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:NSLocalizedString(@"Open camera", nil),NSLocalizedString(@"Select from Library", nil), nil];
    
    
    sheet.tag = 1;
    [sheet showInView:self.view];
    
    
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    
    
//    chosenImage = info[UIImagePickerControllerEditedImage];
//    self.profilePicture.image=chosenImage;
//    
    [picker dismissViewControllerAnimated:NO completion:^{
       // [self updateProfilePic];
    }];
    
    TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:TOCropViewCroppingStyleCircular image:[info objectForKey:UIImagePickerControllerOriginalImage]];
    cropController.delegate = self;
    [self presentViewController:cropController animated:YES completion:nil];

    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    chosenImage=nil;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

#pragma mark - Cropper Delegate -
-(void)cropViewController:(TOCropViewController *)cropViewController didFinishCancelled:(BOOL)cancelled
{
    // [self updateImageViewWithImage:_capturedImageV.image fromCropViewController:cropViewController];
    [cropViewController dismissViewControllerAnimated:YES completion:^{
        NSLog(@"cancel");
        
    }];
    
}

- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    
    //
    //    self.croppedFrame = cropRect;
    //    self.angle = angle;
    //    [self updateImageViewWithImage:image fromCropViewController:cropViewController];
    //
    //
    
    
    [cropViewController dismissViewControllerAnimated:YES completion:^{
        
            chosenImage = image;
        
        [self.collectionViewOfPosts.collectionViewLayout invalidateLayout];
        
 //       [self.collectionViewOfPosts reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 1)]];
//            self.profilePicture.image=chosenImage;
        
        [self updateProfilePic];
    }];
    
    //    [cropViewController dismissAnimatedFromParentViewController:self
    //                                               withCroppedImage:image
    //                                                         toView:_capturedImageV
    //                                                        toFrame:CGRectZero
    //                                                          setup:^{ [self layoutImageView]; }
    //                                                     completion:
    //     ^{
    //         _capturedImageV.hidden = NO;
    //
    //
    //     }];
    
    
}

- (IBAction)otherOptionButtonAction:(id)sender {
    
    NSLog(@"other option button clicked");
    
    NSString *blockActionString;
    if([[userInfo objectForKey:@"is_blocked"] integerValue])
        blockActionString=@"Unblock user";
    else
        blockActionString=@"Block user";
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:NSLocalizedString(@"Send Message", nil),NSLocalizedString(@"Report user", nil),NSLocalizedString(blockActionString, nil), nil];
    
    sheet.tag = 2;
    [sheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (actionSheet.tag == 1) {
        
        if (buttonIndex == 0) {
            
            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                
                UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                      message:@"Device has no camera"
                                                                     delegate:nil
                                                            cancelButtonTitle:@"OK"
                                                            otherButtonTitles: nil];
                
                [myAlertView show];
                
            }
            else
            {
                
                //        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                //        picker.delegate = self;
                //        picker.allowsEditing = NO;
                //        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                //
                //        [self presentViewController:picker animated:YES completion:NULL];
                
                
                
                UIImagePickerControllerSourceType source = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] ? UIImagePickerControllerSourceTypeCamera : UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                UIImagePickerController *cameraController = [[UIImagePickerController alloc] init];
                cameraController.delegate = self;
                cameraController.sourceType = source;
                //cameraController.allowsEditing = YES;
                [self presentViewController:cameraController animated:YES completion:^{
                    //iOS 8 bug.  the status bar will sometimes not be hidden after the camera is displayed, which causes the preview after an image is captured to be black
                    if (source == UIImagePickerControllerSourceTypeCamera) {
                        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
                    }
                }];
                
            }
            
            
        }else if(buttonIndex ==1)
        {
            UIImagePickerControllerSourceType source = UIImagePickerControllerSourceTypePhotoLibrary;
            UIImagePickerController *cameraController = [[UIImagePickerController alloc] init];
            cameraController.delegate = self;
            cameraController.sourceType = source;
            //cameraController.allowsEditing = YES;
            [self presentViewController:cameraController animated:YES completion:^{
                //iOS 8 bug.  the status bar will sometimes not be hidden after the camera is displayed, which causes the preview after an image is captured to be black
                if (source == UIImagePickerControllerSourceTypeCamera) {
                    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
                }
            }];
            
        }
        
    }
    else if (actionSheet.tag == 2)
    {
        if(buttonIndex == 0)
        {
            
            NSLog(@"send message button clicked");
            
            DemoMessagesViewController *vc = [DemoMessagesViewController messagesViewController];
            vc.chatFollowerId=[NSString stringWithFormat:@"%d",self.userId];
            vc.chatFollowerName=[userInfo objectForKey:@"username"];
            vc.isFromProfile = YES;
            
            UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:vc];
            navCon.navigationBar.hidden=YES;
            
            [self presentViewController:navCon animated:YES completion:nil];
            
        }
        else if (buttonIndex == 1) {
            
            //[self performSelector:@selector(secondMethod) withObject:nil afterDelay:1.0 ];
            
            [[ServerManager sharedManager] postReportAgainstUserId:self.userId WithCompletion:^(BOOL success) {
                
                if (success) {
                    
                    //  [self.feedTableView reloadData];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                    message:@"Your report has been submitted."
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                    
                }
                else{
                    
                }
                
            }];

        }
        else if (buttonIndex == 2)
        {
            //Block user
            [[ServerManager sharedManager] postBlockUserId:self.userId WithCompletion:^(BOOL success) {
                
                if (success) {
                    
                    if([[userInfo objectForKey:@"is_blocked"] integerValue])
                    {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                        message:[NSString stringWithFormat:@"%@ has been unblocked successfully",[userInfo objectForKey:@"username"]]
                                                                       delegate:nil
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        [alert show];
                        [userInfo setObject:[NSNumber numberWithBool:0] forKey:@"is_blocked"];
                    }
                    else
                    {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                        message:[NSString stringWithFormat:@"%@ has been blocked from being able to access your profile and posts. You will also no longer appear in any searches initiated by %@.",[userInfo objectForKey:@"username"],[userInfo objectForKey:@"username"]]
                                                                       delegate:nil
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        [alert show];
                        
                        [userInfo setObject:[NSNumber numberWithBool:1] forKey:@"is_blocked"];
                    }

                }
                else{
                    
                }
                
            }];
        
        }
        
    }
}
-(void)updateProfilePic
{
    [self.activityIndicatorView startAnimating];
    
    [[ServerManager sharedManager] postProfilePicture:chosenImage completion:^(BOOL success) {
        if (success) {
            
            [self.activityIndicatorView stopAnimating];
        
        }
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.activityIndicatorView stopAnimating];
                
                
                chosenImage = nil;
                
                [self.collectionViewOfPosts.collectionViewLayout invalidateLayout];
                
                
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed!"
                                                                message:@"Couldnt change the profile pic, please try again"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles: nil];
                [alert show];
            
            });
        }
        
    }];


}


- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)optionsButtonAction:(id)sender {
    
     SettingViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingViewController"];
    
    [self.navigationController pushViewController:controller animated:YES];
    
}



#pragma mark - Pusher Delegate Connection
//////////////////////////////////

- (void)pusher:(PTPusher *)pusher connectionDidConnect:(PTPusherConnection *)connection
{
    NSLog(@"[Pusher] connected to %@", [connection.URL absoluteString]);
}

- (void)pusher:(PTPusher *)pusher connection:(PTPusherConnection *)connection failedWithError:(NSError *)error
{
    if (error) {
        NSLog(@"[Pusher] connection failed: %@", [error localizedDescription]);
    } else {
        NSLog(@"[Pusher] connection failed");
    }
}

- (void)pusher:(PTPusher *)pusher connection:(PTPusherConnection *)connection didDisconnectWithError:(NSError *)error willAttemptReconnect:(BOOL)reconnect
{
    if (error) {
        NSLog(@"[Pusher] didDisconnectWithError: %@ willAttemptReconnect: %@", [error localizedDescription], (reconnect ? @"YES" : @"NO"));
    } else {
        NSLog(@"[Pusher] disconnected");
    }
}

- (IBAction)refreshPressed:(id)sender {
    
    [self viewWillAppear:YES];
}

- (UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}
-(NSData*)compressImage: (UIImage *)img
{
    float MAX_UPLOAD_SIZE=100;
    float MIN_UPLOAD_RESOLUTION=120*120;
    float factor;
    float resol = img.size.height*img.size.width;
    if (resol >MIN_UPLOAD_RESOLUTION){
        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
        img = [self scaleDown:img withSize:CGSizeMake(img.size.width/factor, img.size.height/factor)];
    }
    
    //Compress the image
    CGFloat compression = 1.0f;
    CGFloat maxCompression = 0.5f;
    
    NSData *imageData = UIImageJPEGRepresentation(img, compression);
    
    while ([imageData length] > MAX_UPLOAD_SIZE && compression > maxCompression)
    {
        compression -= 0.10;
        imageData = UIImageJPEGRepresentation(img, compression);
        NSLog(@"Compress : %lu",(unsigned long)imageData.length);
    }
    return imageData;
}


-(void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator
{
    self.collectionViewOfPosts.hidden=YES;
    
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        
        if (size.width > size.height )
        {
           
        }
        else
        {
         //   [self.view layoutIfNeeded];
            
            
        }
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        
        // Stuff you used to do in didRotateFromInterfaceOrientation would go here.
        // If not needed, set to nil.
        
    }];
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
}
- (IBAction)followButtonAction:(UIButton*)sender {
    
    sender.userInteractionEnabled = NO;
    
    
    [[ServerManager sharedManager] postFollowUserId:self.userId WithCompletion:^(BOOL success) {
        
        sender.userInteractionEnabled = YES;
        
        if (success) {
            
             sender.selected=!sender.selected;
            
            sender.hidden=YES;
            [userInfo setObject:[NSNumber numberWithBool: sender.selected] forKey:@"is_following"];
            if(sender.selected)
            {
                [self.activityIndicatorView startAnimating];
                
                for (int i=102; i<=104; i++) {
                    
                    UIButton *random1 = (UIButton *)[self.view viewWithTag:i];
                    random1.selected=NO;
                }
                
                [self makeRequest];
                self.selectedTab=1;
                
                self.privateAccountMsg.hidden=YES;
                
            }
            else
            {
                postUserDataSource=nil;
                postTagDataSource=nil;
                [self.collectionViewOfPosts reloadData];
                
                self.privateAccountMsg.hidden=NO;
                
                
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"loadAllFeed" object:nil];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"followActionHappened"];
            
        }
        
    }];
    
    
}


//-(UIImage *)imageManager:(SDWebImageManager *)imageManager transformDownloadedImage:(UIImage *)image withURL:(NSURL *)imageURL
//{
//    // NSLog(@"imageURL : %@",imageURL);
//    UIImage *resizedImage;
//    //  return resizedImage  = [image resizedImage:CGSizeMake(512, 512) interpolationQuality:kCGInterpolationDefault];
//    
//    float MIN_UPLOAD_RESOLUTION=756*756;
//    float factor;
//    float resol = image.size.height*image.size.width;
//    if (resol >MIN_UPLOAD_RESOLUTION){
//        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
//        resizedImage = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
//        return resizedImage;
//        
//    }
//    else
//        return image;
//}



//- (IBAction)logOutButtonAction:(id)sender {
//    
//    [UserAccount sharedManager].userId=0;
//    [UserAccount sharedManager].userFirstName=@"Guest";
//    [UserAccount sharedManager].userLastName=@"";
//    [UserAccount sharedManager].userImageName=@"/img/profile-holder.png";
//
//    TabBarViewController *tabBar = (TabBarViewController *) self.tabBarController;
//    [tabBar logOutFromTabbar];
//    
//    NSLog(@"%@ %@",tabBar,self.tabBarController);
//   // [self.tabBarController.navigationController popViewControllerAnimated:YES];
//}

@end
