//
//  EditProfileViewController.m
//  Artegrams
//
//  Created by Sujan on 8/3/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "EditProfileViewController.h"
#import "EditProfileTableViewCell.h"
#import "EditProfileCollectionTableViewCell.h"
#import "Constants.h"
#import "UserAccount.h"
#import "NSArray+NullReplacement.h"
#import "NSDictionary+NullReplacement.h"
#import "ChangePasswordViewController.h"
#import "ServerManager.h"

@interface EditProfileViewController (){

    NSMutableDictionary* userInfo;
    NSMutableArray *preferenceArray;
    NSMutableArray *arttypeArray;
    NSMutableArray *selectedPreferenceArray;
    NSMutableArray *selectedArttypeArray;
    
    NSMutableArray *userTypeArray;
    
    UIButton *userTypeButton;
    UIButton* changePasswordButton;
    
    UIView *viewForpicker;
    
    UISegmentedControl *genderSegmentControl;
}

@end

@implementation EditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.editProfileTableView.delegate = self;
    self.editProfileTableView.dataSource = self;
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapinTable:)];
    //self.editProfileTableView.gestureRecognizers = @[tapRecognizer];
    [self.editProfileTableView addGestureRecognizer:tapRecognizer];
    
    [self getUserType];
    
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) getUserType{
    
    [[ServerManager sharedManager] getAllUserTypeWithCompletion:^(BOOL success, NSMutableDictionary *responseObject) {
        
        
        if ( responseObject!=nil) {
            
            // NSLog(@"responseObject %@",responseObject);
            userTypeArray=[[NSMutableArray alloc] initWithArray:responseObject];
            
            NSLog(@"userTypeArray %@",userTypeArray);
            
             [self makeRequest];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
            });
            
        }
    }];


}

-(void) makeRequest{
    
    
    [[ServerManager sharedManager] getCurrentUserDetailsWithCompletion:^(BOOL success, NSMutableDictionary *responseObject) {
        
        if ( responseObject!=nil) {
            
            userInfo = [[NSMutableDictionary alloc ]initWithDictionary:[responseObject dictionaryByReplacingNullsWithBlanks]];
            
            
             NSLog(@"userInfo in edit pro %@",userInfo);
            
            [userInfo setObject:[responseObject objectForKey:@"name"] forKey:@"first_name"];
            
            
            selectedPreferenceArray = [[NSMutableArray alloc]init];
            selectedPreferenceArray = [[userInfo objectForKey:@"art_preferences"] mutableCopy];
            
            //NSLog(@"user_art_types %@",preferenceArray);
            
            selectedArttypeArray = [[NSMutableArray alloc]init];
            selectedArttypeArray = [[userInfo objectForKey:@"art_types"] mutableCopy];
            
            // NSLog(@"user_preferred_arts %@",arttypeArray);
            
            [self getArtPreference];
            
            [self getArtType];
            
            [self.editProfileTableView reloadData];
            
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[[UIAlertView alloc] initWithTitle:@"Sorry, unable to fetch user data. Please try again." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            });
            
        }
    }];

    
}



-(void) getArtPreference{

   
       [[ServerManager sharedManager] getAllArtPreference:^(BOOL success, NSMutableDictionary *resultDataArray) {
           
           
           if ( resultDataArray!=nil) {
           
           
               
               
               preferenceArray = [[NSMutableArray alloc]init];
               preferenceArray = [resultDataArray mutableCopy];

              // NSLog(@"preferenceArray %@",preferenceArray);
               
           
           }else{
               dispatch_async(dispatch_get_main_queue(), ^{
                   
                   //[[[UIAlertView alloc] initWithTitle:@"Sorry, unable to fetch user data. Please try again." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
               });
               
           }
       }];

}


-(void) getArtType{


    [[ServerManager sharedManager] getAllArtTypes:^(BOOL success, NSMutableDictionary *resultDataArray) {
        
        
        if ( resultDataArray!=nil) {
            
              arttypeArray = [[NSMutableArray alloc]init];
              arttypeArray = [resultDataArray mutableCopy];
            
           // NSLog(@"arttypeArray %@",arttypeArray);
            
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //[[[UIAlertView alloc] initWithTitle:@"Sorry, unable to fetch user data. Please try again." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            });
            
        }
    }];

}

#pragma mark - UITableView DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 4;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        
        return 6;
        
    }else if(section == 1){
    
        return 3;
        
    }else
        
        return 1;

    
  
   

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        
        return 10.0f;
        
    }else
        
    return 45.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
    
        UIView *firstView = [[UIView  alloc] init];
        
        return firstView;
        
    }else{
    
        UIView *othersView = [[UIView  alloc] init];
        
        if (section == 1) {
            
            othersView.backgroundColor =[UIColor lightGrayColor];
        }
        
        UILabel *headerLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 300, 25)];
        //headerLabel.font = [UIFont fontWithName:@"AzoSans-Regular" size:12];
        headerLabel.numberOfLines = 1;
        headerLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines; // or UIBaselineAdjustmentAlignCenters, or UIBaselineAdjustmentNone
        headerLabel.adjustsFontSizeToFitWidth = YES;
        headerLabel.adjustsLetterSpacingToFitWidth = YES;
        headerLabel.textColor = [UIColor blackColor];
        if (section == 1) {
            
            headerLabel.text = @"PRIVATE INFORMATION";
            headerLabel.font = [UIFont fontWithName:@"AzoSans-Regular" size:12];
        }else if (section == 2){
        
           headerLabel.text = @"Art Preference";
           headerLabel.font = [UIFont fontWithName:@"AzoSans-Regular" size:16];
            
        }else if (section == 3){
        
           headerLabel.text = @"Art Form";
           headerLabel.font = [UIFont fontWithName:@"AzoSans-Regular" size:16];
        }
        
        [othersView addSubview:headerLabel];
        
        return othersView;
    
    }
    
    return nil;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{


    if (indexPath.section == 0) {
        
        EditProfileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"editProfileCell"];
        
        
        cell.crossButton.hidden = YES;
        cell.textField.tag=indexPath.section*10+indexPath.row;
        cell.textField.delegate=self;
        
        [cell.textField addTarget:self
                      action:@selector(textFieldDidChange:)
            forControlEvents:UIControlEventEditingChanged];
        
        if (indexPath.row == 0) {
            
            cell.iconImage.image = [UIImage imageNamed:@"fullNameIcon"];
            NSString *fullName = [NSString stringWithFormat:@"%@", [userInfo objectForKey:@"first_name"]];
            cell.textField.text = fullName;
            cell.textField.placeholder = @"Name";
            cell.textField.keyboardType=UIKeyboardTypeDefault;
            cell.iconWidth.constant = 20;
            cell.iconTrailing.constant = 15;
            
        }else if (indexPath.row == 1){
        
            cell.iconImage.image = [UIImage imageNamed:@"userIcon"];
            //cell.textField.text = [userInfo objectForKey:@"username"];
            cell.textField.placeholder = [userInfo objectForKey:@"username"];
            
            cell.textField.keyboardType=UIKeyboardTypeDefault;
            
            cell.textField.userInteractionEnabled=NO;
            
        
        }else if (indexPath.row == 2){
        
            cell.iconImage.image = [UIImage imageNamed:@"webSiteIcon"];
            cell.textField.text = [userInfo objectForKey:@"website"];
            
            cell.textField.keyboardType=UIKeyboardTypeURL;
            cell.textField.placeholder = @"Website";
            cell.textField.userInteractionEnabled=YES;
            

        }else if (indexPath.row == 3){
            
            cell.iconImage.image = [UIImage imageNamed:@"bioIcon"];
            cell.textField.text = [userInfo objectForKey:@"biography"];
            
            cell.textField.keyboardType=UIKeyboardTypeDefault;
            cell.textField.placeholder = @"Bio";
            cell.textField.userInteractionEnabled=YES;
            
            
        }else if (indexPath.row == 4){
        
            cell.iconImage.image = [UIImage imageNamed:@""];
            cell.textField.text = @"How are you connected to the art world?";
            cell.textField.placeholder = @"How are you connected to the art world?";
            cell.textField.userInteractionEnabled=NO;
            
            
            
        }else if (indexPath.row == 5){
            
            cell.iconImage.image = [UIImage imageNamed:@""];
            //cell.textField.placeholder = @"User type";
            cell.textField.hidden = YES;
            cell.textField.userInteractionEnabled=NO;
            
            NSArray *filtered = [userTypeArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(title == %@)", [userInfo objectForKey:@"user_type"]]];
            NSDictionary *user;
            if(filtered.count)
                user = [filtered objectAtIndex:0];
            
            NSLog(@"item  %@",user);
            
         //   selectedUserId =[[user objectForKey:@"id"]intValue];
            
         //   NSLog(@"selectedUserId  %d",selectedUserId);
            
            if (userTypeButton) {
                
                [userTypeButton removeFromSuperview];
            }
            
            userTypeButton = [[UIButton alloc]init];
            userTypeButton.frame = CGRectMake(45, 8, cell.textField.layer.bounds.size.width, cell.textField.layer.bounds.size.height);
            userTypeButton.titleLabel.font = [UIFont fontWithName:@"AzoSans-Regular" size:12.0f];
            [userTypeButton setTitle:[user objectForKey:@"title"] forState:UIControlStateNormal];
            userTypeButton.backgroundColor = [UIColor whiteColor];
            [userTypeButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal ];
            userTypeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            userTypeButton.enabled=NO;
            //userTypeButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
            
            NSLog(@"buttun text %@",userTypeButton.titleLabel.text);
            
            [userTypeButton addTarget:self
                               action:@selector(dropDowm:)
               forControlEvents:UIControlEventTouchUpInside];
            
            [cell addSubview:userTypeButton];
          
            
            
        }
        
        return cell;
        
    }else if (indexPath.section == 1){
    
        EditProfileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"editProfileCell"];
        
        cell.crossButton.hidden = YES;
        cell.textField.tag=indexPath.section*10+indexPath.row;
        cell.textField.delegate=self;
        [cell.textField addTarget:self
                           action:@selector(textFieldDidChange:)
                 forControlEvents:UIControlEventEditingChanged];
        
        
        if (indexPath.row == 0) {
            
            cell.iconImage.image = [UIImage imageNamed:@"emailIcon"];
            cell.textField.text = [userInfo objectForKey:@"email"];
            
            cell.textField.keyboardType=UIKeyboardTypeEmailAddress;
            cell.textField.placeholder = @"Email";
            cell.iconHeight.constant = 20;

        }else if (indexPath.row == 1){
            
            cell.iconImage.image = [UIImage imageNamed:@"phoneIcon"];
            cell.textField.text = [userInfo objectForKey:@"phone"];
            
            cell.textField.keyboardType=UIKeyboardTypePhonePad;
            cell.textField.placeholder = @"Phone";
            cell.iconWidth.constant = 20;
            cell.iconTrailing.constant = 15;
            
        }else if (indexPath.row == 2){
            
            cell.iconImage.image = [UIImage imageNamed:@"genderIcon"];
//            cell.textField.placeholder = @"Gender";
//             cell.textField.text = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"sex"]];
            cell.textField.hidden = YES;
            
            if (genderSegmentControl) {
                
                [genderSegmentControl removeFromSuperview];
            }
            
            genderSegmentControl = [[UISegmentedControl alloc]initWithItems:@[@"Male",@"Female"]];
            
            [genderSegmentControl setSegmentedControlStyle:UISegmentedControlStyleBar];
            genderSegmentControl.frame = CGRectMake(45, 8,cell.layer.bounds.size.width - 60, cell.textField.layer.bounds.size.height);
            [genderSegmentControl addTarget:self action:@selector(segmentedControlValueDidChange:) forControlEvents:UIControlEventValueChanged];
            [genderSegmentControl setSelectedSegmentIndex:[[userInfo objectForKey:@"sex"]intValue]-1 ];
            
            genderSegmentControl.tintColor = [UIColor blackColor];

            [genderSegmentControl setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]} forState:UIControlStateNormal];
          
            [cell addSubview:genderSegmentControl];
 
            NSLog(@"gender  from api %@",[userInfo objectForKey:@"sex"]);
            
        }else if (indexPath.row == 3){
        
        
            cell.iconImage.image = [UIImage imageNamed:@"ic_lock"];
            //cell.textField.placeholder = @"User type";
            cell.textField.hidden = YES;
            cell.textField.userInteractionEnabled=NO;

            
            if (changePasswordButton) {
                
                [changePasswordButton removeFromSuperview];
            }
            
            changePasswordButton = [[UIButton alloc]init];
            changePasswordButton.frame = CGRectMake(45, 8, cell.textField.layer.bounds.size.width, cell.textField.layer.bounds.size.height);
            changePasswordButton.titleLabel.font = [UIFont fontWithName:@"AzoSans-Regular" size:12.0f];
            [changePasswordButton setTitle:@"Change password" forState:UIControlStateNormal];
            changePasswordButton.backgroundColor = [UIColor whiteColor];
            [changePasswordButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal ];
            changePasswordButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            //userTypeButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
            
            NSLog(@"buttun text   %@",changePasswordButton.titleLabel.text);
            
            [changePasswordButton addTarget:self
                               action:@selector(changePassword:)
                     forControlEvents:UIControlEventTouchUpInside];
            
            [cell addSubview:changePasswordButton];
        
        
        
        
        
        }
    
        return cell;
    
    }else if (indexPath.section == 2){
    
    
      EditProfileCollectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"collectionViewCell"];
        
//        NSMutableArray *preferenceArray=[[NSMutableArray alloc] init];
//        
//        NSMutableDictionary*artPreferenceDic = [[NSMutableDictionary alloc]init];
//        
//        [artPreferenceDic setObject:@"Renaissance" forKey:@"preferenceName"];
//        [artPreferenceDic setObject:[NSNumber numberWithBool:NO] forKey:@"state"];
//
//        NSMutableDictionary*artPreferenceDic2 = [[NSMutableDictionary alloc]init];
//        [artPreferenceDic2 setObject:@"Neoclassicaism" forKey:@"preferenceName"];
//        [artPreferenceDic2 setObject:[NSNumber numberWithBool:NO] forKey:@"state"];
//        
//        NSMutableDictionary*artPreferenceDic3 = [[NSMutableDictionary alloc]init];
//        [artPreferenceDic3 setObject:@"Romanticism" forKey:@"preferenceName"];
//        [artPreferenceDic3 setObject:[NSNumber numberWithBool:NO] forKey:@"state"];
//        
//        NSMutableDictionary*artPreferenceDic4 = [[NSMutableDictionary alloc]init];
//        [artPreferenceDic4 setObject:@"Modern Art" forKey:@"preferenceName"];
//        [artPreferenceDic4 setObject:[NSNumber numberWithBool:NO] forKey:@"state"];
//        
//        NSMutableDictionary*artPreferenceDic5 = [[NSMutableDictionary alloc]init];
//        [artPreferenceDic5 setObject:@"Contemporary Art" forKey:@"preferenceName"];
//        [artPreferenceDic5 setObject:[NSNumber numberWithBool:NO] forKey:@"state"];
//        
////        NSMutableDictionary*artPreferenceDic6 = [[NSMutableDictionary alloc]init];
////        [artPreferenceDic6 setObject:@"" forKey:@"preferenceName"];
////        [artPreferenceDic6 setObject:[NSNumber numberWithBool:NO] forKey:@"state"];
//        
//        [preferenceArray addObject:artPreferenceDic];
//        [preferenceArray addObject:artPreferenceDic2];
//        [preferenceArray addObject:artPreferenceDic3];
//        [preferenceArray addObject:artPreferenceDic4];
//        [preferenceArray addObject:artPreferenceDic5];
////        [preferenceArray addObject:artPreferenceDic6];
//        
//        NSLog(@"preference name %@",preferenceArray);
        
        
        
        cell.selectedArtArray = selectedPreferenceArray;
        cell.artArray = preferenceArray;
        cell.artPreferenceCollectionView.tag=1;
        
        [cell.artPreferenceCollectionView reloadData];
        
       // NSLog(@"preferenceArray %@",preferenceArray);
        
      return cell;
    
    
    }else if (indexPath.section == 3){
    
      EditProfileCollectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"collectionViewCell"];
        
//        NSMutableArray *preferenceArray=[[NSMutableArray alloc] init];
//        
//        NSMutableDictionary*artPreferenceDic = [[NSMutableDictionary alloc]init];
//        
//        [artPreferenceDic setObject:@"Painting" forKey:@"preferenceName"];
//        [artPreferenceDic setObject:[NSNumber numberWithBool:NO] forKey:@"state"];
//        
//        NSMutableDictionary*artPreferenceDic2 = [[NSMutableDictionary alloc]init];
//        [artPreferenceDic2 setObject:@"Prints" forKey:@"preferenceName"];
//        [artPreferenceDic2 setObject:[NSNumber numberWithBool:NO] forKey:@"state"];
//        
//        NSMutableDictionary*artPreferenceDic3 = [[NSMutableDictionary alloc]init];
//        [artPreferenceDic3 setObject:@"Drawing" forKey:@"preferenceName"];
//        [artPreferenceDic3 setObject:[NSNumber numberWithBool:NO] forKey:@"state"];
//        
//        NSMutableDictionary*artPreferenceDic4 = [[NSMutableDictionary alloc]init];
//        [artPreferenceDic4 setObject:@"Craft" forKey:@"preferenceName"];
//        [artPreferenceDic4 setObject:[NSNumber numberWithBool:NO] forKey:@"state"];
//        
//        NSMutableDictionary*artPreferenceDic5 = [[NSMutableDictionary alloc]init];
//        [artPreferenceDic5 setObject:@"Design" forKey:@"preferenceName"];
//        [artPreferenceDic5 setObject:[NSNumber numberWithBool:NO] forKey:@"state"];
//        
//        NSMutableDictionary*artPreferenceDic6 = [[NSMutableDictionary alloc]init];
//        [artPreferenceDic6 setObject:@"Cotemporary Art" forKey:@"preferenceName"];
//        [artPreferenceDic6 setObject:[NSNumber numberWithBool:NO] forKey:@"state"];
//        
//        [preferenceArray addObject:artPreferenceDic];
//        [preferenceArray addObject:artPreferenceDic2];
//        [preferenceArray addObject:artPreferenceDic3];
//        [preferenceArray addObject:artPreferenceDic4];
//        [preferenceArray addObject:artPreferenceDic5];
//        [preferenceArray addObject:artPreferenceDic6];
//        
//        NSLog(@"preference name %@",preferenceArray);
        
        cell.selectedArtArray = selectedArttypeArray;
        cell.artArray = arttypeArray;
        cell.artPreferenceCollectionView.tag=2;
        [cell.artPreferenceCollectionView reloadData];
        
       // NSLog(@"preferenceArray %@",arttypeArray);

      return cell;
    
    
    
    }
 
    
    
    return nil;
    
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 2 || indexPath.section == 3) {
        
        return 160.0;

    }else
        
      return 45.0;
}

#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"did select");
    
    
}




-(void)segmentedControlValueDidChange:(UISegmentedControl *)segment
{
    switch (segment.selectedSegmentIndex) {
        case 0:{
             [userInfo setObject:[NSNumber numberWithInt: 1 ] forKey:@"sex"];
            break;}
        case 1:{
             [userInfo setObject:[NSNumber numberWithInt: 2 ] forKey:@"sex"];
            break;}
        case 3:{
            [userInfo setObject:[NSNumber numberWithInt: 3 ] forKey:@"sex"];
            break;}
    }
    
    NSLog(@"selected segment %@",[userInfo objectForKey:@"sex"]);
}


-(void)dropDowm:(id)sender {

    NSLog(@"dropdown clicked");
    viewForpicker =[[UIView alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height - 175,self.view.frame.size.width,175)];
    viewForpicker.backgroundColor = [UIColor lightGrayColor];
    
    userPickerView=[[UIPickerView alloc] initWithFrame:CGRectMake(0,viewForpicker.frame.size.height - 140,viewForpicker.frame.size.width,140)];
    userPickerView.backgroundColor = [UIColor whiteColor];
    // userPickerView.transform = CGAffineTransformMakeScale(0.75f, 0.75f);
    userPickerView.dataSource = self;
    userPickerView.delegate = self;
    userPickerView.showsSelectionIndicator = YES;
    
    [viewForpicker addSubview:userPickerView];
    
    UIButton* closePickerButton = [[UIButton alloc]init];
    closePickerButton.frame = CGRectMake(15, 0, 45, 35);
    closePickerButton.titleLabel.font = [UIFont fontWithName:@"AzoSans-Regular" size:14.0f];
    [closePickerButton setTitle:@"Done" forState:UIControlStateNormal];
    
    [closePickerButton addTarget:self
                       action:@selector(hidePicker:)
             forControlEvents:UIControlEventTouchUpInside];
    
    [viewForpicker addSubview:closePickerButton];
    
    [self.view addSubview:viewForpicker];
    
    
}

-(void)changePassword:(id)sender{

  
    ChangePasswordViewController *vc  = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
    
    [self presentViewController:vc animated:YES completion:nil];



}

#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return [userTypeArray count];
}

#pragma mark- Picker View Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component{
    
    NSString* title = [[userTypeArray objectAtIndex:row] objectForKey:@"title"];
    
   // selectedUserId = [[[userTypeArray objectAtIndex:row] objectForKey:@"id"]intValue];
    
    [userInfo setObject:[NSNumber numberWithInt:[[[userTypeArray objectAtIndex:row] objectForKey:@"id"]intValue]] forKey:@"user_type_id"];
    
    [userTypeButton setTitle:title forState: UIControlStateNormal];
    
  //  NSLog(@"user title : %@ and id :%d",title,selectedUserId);
    
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    
    return [[userTypeArray objectAtIndex:row] objectForKey:@"title"];
    
}

-(void)hidePicker:(id)sender {

  //  NSLog(@"user  id :%d",selectedUserId);
    [viewForpicker removeFromSuperview];
   
}

- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)doneButtonAction:(id)sender {
    
    //[self performSelector:@selector(secondMethod) withObject:nil afterDelay:2.0 ];
    [self editDetailsAction];
    
}



-(void)editDetailsAction{
    
    
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    [postData setObject:ACCESS_KEY forKey:@"access_key"];
    [postData setObject:[NSNumber numberWithInt:[[userInfo objectForKey:@"user_type_id"] intValue]] forKey:@"user_type_id"];
    
    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"user_id"];
    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
    
    [postData setObject:[userInfo objectForKey:@"first_name"] forKey:@"name"];
    [postData setObject:[userInfo objectForKey:@"username"] forKey:@"username"];
    
    if([[userInfo objectForKey:@"website"] containsString:@"http"])
    {
        [postData setObject:[userInfo objectForKey:@"website"] forKey:@"website"];
        
    }
    else
        [postData setObject:[NSString stringWithFormat:@"http://%@",[userInfo objectForKey:@"website"]] forKey:@"website"];
    
    
    
    [postData setObject:[userInfo objectForKey:@"biography"] forKey:@"biography"];
    [postData setObject:[userInfo objectForKey:@"email"] forKey:@"email"];
    [postData setObject:[userInfo objectForKey:@"phone"] forKey:@"phone"];
    [postData setObject:[userInfo objectForKey:@"sex"] forKey:@"sex"];
    
    [postData setObject:selectedPreferenceArray forKey:@"art_preference"];
    [postData setObject:selectedArttypeArray forKey:@"art_types"];

        NSLog(@"postData %@",postData);
    
    [[ServerManager sharedManager] updateUserDetailsWithData:postData withCompletion:^(BOOL success) {
        
        if (success) {
            [self.navigationController popViewControllerAnimated:YES];
            
            
        }else
        {
            self.view.userInteractionEnabled=YES;
            
           
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed!"
                                                            message:@"Server error, please try again"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
            [alert show];

        }
    }];


    
    
    
}



-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    [self resignFirstResponder];
}

-(void)onTapinTable:(UIGestureRecognizer*) recognizer
{
    NSLog(@"on tap");
    [self.view endEditing:YES];
    
   
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    //[textField becomeFirstResponder ];
    //self.crossButton.hidden = NO;
    
  
}




-(void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@"end with tag %li",(long)textField.tag);
    
//    NSString *keyName;
//    
//    switch (textField.tag) {
//        case 0:
//            keyName=@"first_name";
//            break;
//        case 1:
//            keyName=@"username";
//            break;
//        case 2:
//            keyName=@"website";
//            break;
//        case 3:
//            keyName=@"biography";
//            break;
//        case 4:
//            keyName=@"nothing";
//            break;
//        case 5:
//            keyName=@"user_type_id";
//            break;
//        case 10:
//            keyName=@"email";
//            break;
//        case 11:
//            keyName=@"phone";
//            break;
//        case 12:
//            keyName=@"sex";
//            break;
//    
//        default:
//            keyName=@"Default";
//
//            break;
//    }
//    
//    [userInfo setObject:textField.text forKey:keyName];
//    

    
    if (textField.tag == 1) {
        
        if (textField.text.length > 0) {
            
            if (textField.text.length > 30 ) {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                message:@"Username can't more than 30 characters"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                
            }else if (![self NSStringIsValidUsername:textField.text])
            {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                message:@"Username can contain only letters, numbers, full stops and underscores"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                
            }
            
           // [self verifyUserName];
        }
    }
    
}

-(BOOL) NSStringIsValidUsername:(NSString *) checkString{
    
    NSString *userNameRegex = @"[A-Za-z0-9._]*";
    NSPredicate *userNameTextTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", userNameRegex];
    
    NSLog(@"username  %hhd",[userNameTextTest evaluateWithObject:checkString]);
    
    return [userNameTextTest evaluateWithObject:checkString];

}

//-(void)verifyUserName
//{
//    
//    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
//    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
//    
//    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
//    
//    [postData setObject:ACCESS_KEY forKey:@"access_key"];
//    [postData setObject:self.userNameTextField.text forKey:@"username"];
//    
//    [apiLoginManager POST:[NSString stringWithFormat:@"%@/users/api-is-username-taken",SERVER_BASE_API_URL] parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
//        
//        NSLog(@"result %@",responseObject);
//        NSString * successMsg = [responseObject objectForKey:@"success"];
//        
//        if ([successMsg integerValue] == 1 ) {
//            
//            NSLog(@"success msg %@",[responseObject objectForKey:@"success"]);
//            
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
//                                                            message:@"This username is already in use on Artegrams. Please use another username"
//                                                           delegate:nil
//                                                  cancelButtonTitle:@"OK"
//                                                  otherButtonTitles:nil];
//            [alert show];
//            
//            
//        }else{
//            
//            
//            
//        }
//        
//        
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"error %@",error);
//        
//    }];
//    
//    
//    
//    
//}
- (void)textFieldDidChange:(UITextField*)textField
{
    NSString *keyName;
    
    switch (textField.tag) {
        case 0:
            keyName=@"first_name";
            break;
        case 1:
            keyName=@"username";
            break;
        case 2:
            keyName=@"website";
            break;
        case 3:
            keyName=@"biography";
            break;
        case 4:
            keyName=@"nothing";
            break;
        case 5:
            keyName=@"user_type_id";
            break;
        case 10:
            keyName=@"email";
            break;
        case 11:
            keyName=@"phone";
            break;
        case 12:
            keyName=@"sex";
            break;
            
        default:
            keyName=@"Default";
            
            break;
    }
    
    [userInfo setObject:textField.text forKey:keyName];
    //NSLog(@"text %@",textField.text);
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if(textField.tag==3)
    {
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        return newLength <= 150 || returnKey;
    }
    else
       return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    
    return YES;
}

@end
