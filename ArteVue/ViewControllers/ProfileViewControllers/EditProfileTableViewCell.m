//
//  EditProfileTableViewCell.m
//  Artegrams
//
//  Created by Sujan on 8/3/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "EditProfileTableViewCell.h"

@implementation EditProfileTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.textField.delegate = self;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//-(void)textFieldDidBeginEditing:(UITextField *)textField{
//   
//    //[textField becomeFirstResponder ];
//    self.crossButton.hidden = NO;
//}
//
//
//-(void)textFieldDidEndEditing:(UITextField *)textField{
//    
//  
//}
//
//- (BOOL)textFieldShouldReturn:(UITextField *)textField {
//    
//    [textField resignFirstResponder];
//    
//    return YES;
//}

- (IBAction)crossButtonAction:(id)sender {
    
    self.textField.text = @"";
    self.crossButton.hidden = YES;
}



@end
