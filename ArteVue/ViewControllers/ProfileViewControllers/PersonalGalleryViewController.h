//
//  PersonalGalleryViewController.h
//  Artegram
//
//  Created by Sujan on 6/26/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"

#import <DTCategories/DTCategories.h>
#import "DTCollectionViewCellMover.h"
#import "DTCollectionViewCell.h"

@interface PersonalGalleryViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, DTCollectionViewCellMoverDelegate,UITextFieldDelegate,UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *galleryCollectionView;
@property int userId;

@property (strong, nonatomic)  NSString *galleryNameTitle;
@property (strong, nonatomic)  NSString *galleryDetailsText;

@property (weak, nonatomic) IBOutlet UITextField *galleryName;
@property (weak, nonatomic) IBOutlet UITextView *galleryDetails;
@property (weak, nonatomic) IBOutlet UIButton *pdfButton;
@property (weak, nonatomic) IBOutlet UIButton *pdfGenerateButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UIButton *descriptionPenButton;

@property BOOL isSelectble;

@property UIRefreshControl *refreshControl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *galleryDetailsTextviewHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *noPostLabel;

@end
