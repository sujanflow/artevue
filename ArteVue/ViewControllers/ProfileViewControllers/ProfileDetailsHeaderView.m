//
//  RecipeCollectionHeaderView.m
//  CollectionViewDemo
//
//  Created by Simon on 22/1/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import "ProfileDetailsHeaderView.h"
#import "LikersViewController.h"
#import "PinPostsViewController.h"
#import "Constants.h"
#import "UserAccount.h"
#import "GIBadgeView.h"
#import "HexColors.h"


@implementation ProfileDetailsHeaderView
{
  
    GIBadgeView* badgeView;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    NSLog(@"awakeFromNib");
    [self setupConfiguration];
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        NSLog(@"initWithFrame");
        [self setupConfiguration];
    }
    return self;
}

//
//- (id)initWithCoder:(NSCoder *)aDecoder
//{
//    self = [super initWithCoder:aDecoder];
//    if (self) {
//        [self initialization];
//    }
//    
//    return self;
//}
//


-(void)setupConfiguration
{
    
    NSLog(@"setupConfiguration");
   
    self.followButton.layer.cornerRadius = self.followButton.frame.size.width/2;
    self.followButton.layer.borderWidth = 1.0f;
    //self.followButton.layer.borderColor = [[UIColor whiteColor]CGColor];
    self.followButton.layer.borderColor = self.followButton.tintColor.CGColor;
    self.followButton.hidden=YES;
    
  //  self.selectedTabPosition.constant=0;
    
    
    
    self.editProfileButton.layer.cornerRadius = self.editProfileButton.frame.size.width/2;
    
    self.profilePicture.layer.cornerRadius = self.profilePicture.frame.size.width/2;
    NSLog(@"self.profilePicture.frame.size.width %lf",self.profilePicture.frame.size.width);
    //self.profilePicture.layer.borderWidth = 1.1f;
    self.profilePicture.clipsToBounds = YES;
    self.profilePicture.layer.masksToBounds = YES;
    
    
    UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedOnLink:)];
    // if labelView is not set userInteractionEnabled, you must do so
    [self.webSiteUrl setUserInteractionEnabled:YES];
    [self.webSiteUrl addGestureRecognizer:gesture];

    
}
-(void)populateData
{
    NSLog(@"userInfo %d pendingActivityCount %d",self.userId,[UserAccount sharedManager].pendingActivityCount);
    
    [badgeView removeFromSuperview];
    badgeView = [GIBadgeView new];
    
    badgeView.font = [UIFont fontWithName:@"AzoSans-Medium" size:12];
    badgeView.rightOffset = 10.0f;
    badgeView.topOffset=10.0f;
    //[badgeView setBadgeValue: 50];
    
    [badgeView setHidden:YES];
    
    [self.SecondTabButton addSubview:badgeView];
    
    NSLog(@"badgeView %@",badgeView);
    [badgeView setBadgeValue: [UserAccount sharedManager].pendingActivityCount];
    
    if (self.userId == [UserAccount sharedManager].userId) {
        if([UserAccount sharedManager].pendingActivityCount==0 )
            [badgeView setHidden:YES];
        else
            [badgeView setHidden:NO];
    }
    else
    {
        [badgeView setHidden:YES];
        
    }
    
}

-(CGFloat) updateUserDetailsView
{
    NSLog(@"updateUserDetailsView");
    CGFloat userNameLabelHeight;
    CGFloat userTypeLabelHeight;
    CGFloat bioLabelHeight;
    CGFloat webSiteLabelHeight;
    
    CGSize constraint = CGSizeMake(self.profileNameLabel.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [self.profileNameLabel.text boundingRectWithSize:constraint
                                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                                               attributes:@{NSFontAttributeName:self.profileNameLabel.font}
                                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    userNameLabelHeight = size.height;
    
    NSLog(@"profileNameLabel newHeight %f",userNameLabelHeight);
    
    
    //get text size
    
    CGSize userTypeLabelconstraint = CGSizeMake(self.userTypeLabel.frame.size.width, CGFLOAT_MAX);
    CGSize userTypeLabelsize;
    
    NSStringDrawingContext *userTypeLabelcontext = [[NSStringDrawingContext alloc] init];
    CGSize userTypeLabelboundingBox = [self.userTypeLabel.text boundingRectWithSize:userTypeLabelconstraint
                                                                            options:NSStringDrawingUsesLineFragmentOrigin
                                                                         attributes:@{NSFontAttributeName:self.userTypeLabel.font}
                                                                            context:userTypeLabelcontext].size;
    
    userTypeLabelsize = CGSizeMake(ceil(userTypeLabelboundingBox.width), ceil(userTypeLabelboundingBox.height));
    
    userTypeLabelHeight = userTypeLabelsize.height;
    
    NSLog(@"userTypeLabel newHeight %f",userTypeLabelHeight);
    
    
    
    
    self.userTypeLabel.hidden=NO;
    
    if(![[self.userInfo objectForKey:@"biography"] isEqual:[NSNull null]] && ![[self.userInfo objectForKey:@"biography"] isEqual:@""])
    {
        self.userBio.text=[self.userInfo objectForKey:@"biography"];
     
        
        CGSize constraint = CGSizeMake(self.userBio.frame.size.width, CGFLOAT_MAX);
        CGSize size;
        
        NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
        CGSize boundingBox = [self.userBio.text boundingRectWithSize:constraint
                                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                                               attributes:@{NSFontAttributeName:self.userBio.font}
                                                                  context:context].size;
        
        size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
        
        bioLabelHeight = size.height;
        
        NSLog(@"userBioLabel newHeight %f",bioLabelHeight);
    }
    
    if(![[self.userInfo objectForKey:@"website"] isEqual:[NSNull null]] && ![[self.userInfo objectForKey:@"website"] isEqual:@""])
    {
        self.webSiteUrl.text=[self.userInfo objectForKey:@"website"];
        // self.webSiteUrlLabel.text=@"www.google.com";
        
        //get text size
        
        CGSize constraint = CGSizeMake(self.webSiteUrl.frame.size.width, CGFLOAT_MAX);
        CGSize size;
        
        NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
        CGSize boundingBox = [self.webSiteUrl.text boundingRectWithSize:constraint
                                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                                  attributes:@{NSFontAttributeName:self.webSiteUrl.font}
                                                                     context:context].size;
        
        size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
        
        webSiteLabelHeight = size.height;
        
      
    }
    
    CGFloat totlaLabelHeight = userNameLabelHeight + userTypeLabelHeight + bioLabelHeight + webSiteLabelHeight;
    
    NSLog(@"totlaLabelHeight totlaLabelHeight %f",totlaLabelHeight + 25);
    
    return totlaLabelHeight+25+45+40+75;
}


- (IBAction)followButtonAction:(UIButton*)sender {
    
  //  NSLog(@"userinfo in follow button %@",self.userInfo);
    
    
}


- (void)userTappedOnLink:(UITapGestureRecognizer *)gesture
{
    UILabel* sender=(UILabel*) [gesture view];
    NSLog(@"sender.text %@",sender.text);
    
    UIAlertView *openSafariAlert=[[UIAlertView alloc]initWithTitle:nil message:@"Open Safari" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
    [openSafariAlert show];
    
    
    
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"buttonIndex %li",(long)buttonIndex);
    
    if(buttonIndex == 0)//Yes button pressed
    {
        
        if([self.webSiteUrl.text containsString:@"http"])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.webSiteUrl.text]];
        }
        else
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",self.webSiteUrl.text]]];
        
    }
    else if(buttonIndex == 1)//No button pressed.
    {
        
        
    }
    
}

- (IBAction)TabButtonSelection:(UIButton*)sender
{
    
        if (sender.tag==101 ) {
            
            [UIView animateWithDuration:0.5f animations:^{
                self.selectedTabPosition.constant=0;
                [self layoutIfNeeded];
            }];
            
            
        }
        else if (sender.tag==102) {
            
            
        }
        else if (sender.tag==103) {
            
       
            [UIView animateWithDuration:0.5f animations:^{
                self.selectedTabPosition.constant=([UIScreen mainScreen].bounds.size.width/4)*2;
                [self layoutIfNeeded];
            }];
            
            
        }
        else if (sender.tag==104 ) {
            
            [UIView animateWithDuration:0.5f animations:^{
                self.selectedTabPosition.constant=([UIScreen mainScreen].bounds.size.width/4)*3;
                [self layoutIfNeeded];
            }];
            
        }
    

    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
