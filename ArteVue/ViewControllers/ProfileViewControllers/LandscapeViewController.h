//
//  LandscapeViewController.h
//  Artegrams
//
//  Created by Tanvir Palash on 8/21/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArteVue-Swift.h"
#import "UIImageView+WebCache.h"


@interface LandscapeViewController : UIViewController<TGLParallaxCarouselDelegate, TGLParallaxCarouselDatasource>

@property (weak,nonatomic) IBOutlet TGLParallaxCarousel* carouselView;

@property (strong,nonatomic) NSMutableArray *imageDetailsArray;

@end
