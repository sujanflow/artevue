//
//  EditProfileCollectionTableViewCell.h
//  Artegrams
//
//  Created by Sujan on 8/3/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ToggleView.h"

@interface EditProfileCollectionTableViewCell : UITableViewCell <UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,ToggleViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *artPreferenceCollectionView;


@property (strong,nonatomic) NSMutableArray *artArray;

@property (strong,nonatomic) NSMutableArray *selectedArtArray;

@end
