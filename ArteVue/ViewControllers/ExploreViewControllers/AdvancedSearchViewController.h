//
//  AdvancedSearchViewController.h
//  Artegram
//
//  Created by Sujan on 7/20/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"
#import "AFNetworking.h"
#import "ArteVue-Swift.h"

@interface AdvancedSearchViewController : UIViewController<NIDropDownDelegate,UIScrollViewDelegate,UITextFieldDelegate>{


    IBOutlet UIButton *btnSelectDate;
    IBOutlet UIButton *btnSelectUserType;
    IBOutlet UIButton *btnSelectArtInterest;
    NIDropDown *dropDown;



}


@property (weak, nonatomic) IBOutlet UIView *hashTagView;

@property (weak, nonatomic) IBOutlet UIView *locationView;
@property (weak, nonatomic) IBOutlet UIView *artistView;

@property (weak, nonatomic) IBOutlet UIButton *dateRangeButton;

@property (weak, nonatomic) IBOutlet UIButton *userTypeButton;

@property (weak, nonatomic) IBOutlet UIButton *artInterestButton;

@property (weak, nonatomic) IBOutlet UITextField *hashTagTextField;

@property (weak, nonatomic) IBOutlet UITextField *locationTextField;

@property (weak, nonatomic) IBOutlet UIButton *locationButton;
@property (weak, nonatomic) IBOutlet UILabel *locationTitle;
@property (weak, nonatomic) IBOutlet UILabel *locationAddress;
@property (weak, nonatomic) IBOutlet UIButton *closeLocation;

@property (weak, nonatomic) IBOutlet UIScrollView *containerScrollView;


@property(nonatomic,strong) NSString *testDataSearch;

@property (weak, nonatomic) IBOutlet SearchTextField *artistTextField;
@property (weak, nonatomic) IBOutlet SearchTextField *dateRangeTextField;

@end
