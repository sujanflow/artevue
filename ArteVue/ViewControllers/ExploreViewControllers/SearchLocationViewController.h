//
//  SearchLocationViewController.h
//  Artegram
//
//  Created by Sujan on 7/20/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "HNKGooglePlacesAutocomplete.h"

@interface SearchLocationViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *searchLocationTableView;

@property(nonatomic,strong) NSString *testDataLocation;


@property(nonatomic,strong) NSMutableArray *searchResults;

@end
