//
//  LocationSearchResultViewController.h
//  Artegrams
//
//  Created by Sujan on 8/14/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"

@interface LocationSearchResultViewController : UIViewController<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,SDWebImageManagerDelegate>


@property (weak, nonatomic) IBOutlet UILabel *navigationLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *locationCollectionView;

@property (nonatomic,strong) NSMutableDictionary* locationInfoDic;


@property (weak, nonatomic) IBOutlet UILabel *noPostLabel;

@end
