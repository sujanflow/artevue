//
//  DiscoverPeopleViewController.h
//  Artegram
//
//  Created by Sujan on 7/17/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"

@interface DiscoverPeopleViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,SDWebImageManagerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *discoverPeopleTableView;


@end
