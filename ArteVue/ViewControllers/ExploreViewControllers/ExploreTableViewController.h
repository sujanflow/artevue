//
//  ExploreTableViewController.h
//  Artegram
//
//  Created by Sujan on 7/25/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"

#import <Social/Social.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <QuartzCore/QuartzCore.h>


@interface ExploreTableViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,SDWebImageManagerDelegate>


@property (weak, nonatomic) IBOutlet UITableView *exploreTableView;

@property int indexNumber;
@property UILongPressGestureRecognizer *taplikeLabel;
@property UITapGestureRecognizer *tapFirstcommentLabel;
@property UITapGestureRecognizer *tapSecondcommentLabel;
@property UITapGestureRecognizer *tapThirdcommentLabel;
@property UITapGestureRecognizer *tapcommentCountLabel;

@property(nonatomic,strong) NSMutableArray *discoverPostArray;

@property BOOL isComeFromTagView;
@property(nonatomic,strong) NSString *hashTagName;

@property BOOL isComeFromLocationView;
@property(nonatomic,strong) NSString *locationName;



@property (weak, nonatomic) IBOutlet UILabel *navigationLabel;

@property (nonatomic, retain) UIDocumentInteractionController *documentController;

@end
