//
//  AdvancedSearchResultViewController.h
//  Artegrams
//
//  Created by Tanvir Palash on 8/15/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"

@interface AdvancedSearchResultViewController : UIViewController<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,SDWebImageManagerDelegate>


@property (weak, nonatomic) IBOutlet UILabel *navTitle;

@property (weak, nonatomic) IBOutlet UICollectionView *advancedCollectionView;

@property (nonatomic,strong) NSMutableDictionary* searchInfoDic;

@property (weak, nonatomic) IBOutlet UILabel *noPostLabel;

@end
