//
//  AdvancedSearchViewController.m
//  Artegram
//
//  Created by Sujan on 7/20/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "AdvancedSearchViewController.h"
#import "Constants.h"
#import "UserAccount.h"
#import "ServerManager.h"

@interface AdvancedSearchViewController (){

    NSMutableArray* artistList;
    NSMutableArray* userTypes;
    NSMutableArray* artPreference;
    NSMutableDictionary* locationDictionary;
    
}

@end

@implementation AdvancedSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(retreivelocationData:) name:@"pushLocationToAdvancedView" object:nil];
    
    self.hashTagTextField.delegate = self;
    // Border in textfield view
    
    self.hashTagView.layer.borderWidth = 0.3f;
    self.hashTagView.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    
   
    self.locationView.layer.borderWidth = 0.3f;
    self.locationView.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    
    self.artistView.layer.borderWidth = 0.3f;
    self.artistView.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    
   
    self.dateRangeButton.layer.borderWidth = 0.3f;
    self.dateRangeButton.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    
   
    
    self.userTypeButton.layer.borderWidth = 0.3f;
    self.userTypeButton.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    
   
    self.artInterestButton.layer.borderWidth = 0.3f;
    self.artInterestButton.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    
    [self loadArtist];
    //[self configureDateRangeTextField];
    //[self loadTypes];
    //[self loadArtPreferences];
    
    self.containerScrollView.delegate=self;
    
//    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
//    [self.containerScrollView addGestureRecognizer:singleTap];
}

-(void) viewDidAppear:(BOOL)animated
{
    self.dateRangeButton.enabled=YES;
    NSLog(@"testing %@",self.testDataSearch);
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [dropDown hideDropDown:_dateRangeButton];
    [self rel];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [self.hashTagTextField resignFirstResponder];
    return YES;
}

- (IBAction)textFieldDidChange:(UITextField *)sender {
    
        self.dateRangeButton.enabled=YES;
//        self.dateRangeButton.titleLabel.textColor=[UIColor lightGrayColor];
//        btnSelectDate.titleLabel.textColor=[UIColor lightGrayColor];
    
//    if(self.hashTagTextField.text.length || self.artistTextField.text.length)
//    {
//        self.dateRangeButton.enabled=YES;
//         self.dateRangeButton.titleLabel.textColor=[UIColor blackColor];
//        btnSelectDate.titleLabel.textColor=[UIColor blackColor];
//        
//    }
//    else
//    {
//        self.dateRangeButton.enabled=NO;
//        self.dateRangeButton.titleLabel.textColor=[UIColor lightGrayColor];
//        
//        btnSelectDate.titleLabel.textColor=[UIColor lightGrayColor];
//        
//    }
}

- (IBAction)dateRangeTouched:(id)sender {
    NSLog(@"dateRangeTouched");
    
    [self.hashTagTextField resignFirstResponder];
    [self.artistTextField resignFirstResponder];
    [self.dateRangeTextField resignFirstResponder];

}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.hashTagTextField resignFirstResponder];
    [self.artistTextField resignFirstResponder];
    //[self.dateRangeTextField resignFirstResponder];
    
    [dropDown hideDropDown:_dateRangeButton];
    
    [self rel];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    NSLog(@"touchesBegan");
    [self.hashTagTextField resignFirstResponder];
    
    [self.artistTextField resignFirstResponder];
    [self.dateRangeTextField resignFirstResponder];
    
    
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    NSLog(@"touchesEnd");
    
    [self.view endEditing:YES];
}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture
{
    NSLog(@"tap gesture");
  
   // CGPoint touchPoint=[gesture locationInView:self.containerScrollView];

    
    [self.hashTagTextField resignFirstResponder];
    
    [self.artistTextField resignFirstResponder];
    [self.dateRangeTextField resignFirstResponder];
    
}

- (IBAction)dateRangeButtonAction:(id)sender {
    
    [self.view endEditing:YES];
    
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"Any",@"Last 1 month", @"Last 3 months",@"Last 6 months",@"Last 1 year",nil];

        
        if(dropDown == nil) {
            CGFloat height = 200;
            dropDown = [[NIDropDown alloc]showDropDown:sender :&height :arr :nil :@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
        
    
}


- (IBAction)userTypeButtonAction:(id)sender {
    
    NSArray * arr = [[NSArray alloc] init];
    
    if(userTypes.count)
    {
        arr=[userTypes valueForKey:@"title"];
        
        if(dropDown == nil) {
            CGFloat height = 160;
            dropDown = [[NIDropDown alloc]showDropDown:sender :&height :arr :nil :@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
        
    }
}

- (IBAction)artInterestButtonAction:(id)sender {
    
    NSArray * arr = [[NSArray alloc] init];
   // arr = [NSArray arrayWithObjects:@"Renaissance", @"Neoclassicism",@"Romanticism",@"Modern Art",@"Contemporary Art",nil];
    
    if(artPreference.count)
    {
        arr=[artPreference valueForKey:@"title"];
        
        if(dropDown == nil) {
            CGFloat height = 160;
            dropDown = [[NIDropDown alloc]showDropDown:sender :&height :arr :nil :@"down"];
            dropDown.delegate = self;
            
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }

    }
    
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
    NSLog(@"........%@", btnSelectDate.titleLabel.text);
}

-(void)rel{
    //    [dropDown release];
    dropDown = nil;
}

-(void)loadArtist
{

    [[ServerManager sharedManager] getAllArtistWithCompletion:^(BOOL success, NSMutableDictionary *responseObject) {
        
        
        if ( responseObject!=nil) {
            
            
            
            artistList=[responseObject valueForKey:@"name"];
            [self configureSimpleSearchTextField];
            

            //NSLog(@"artistList %@",artistList);
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
              
            });
            
        }
    }];
    
}

//-(void) loadTypes
//{
//    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
//    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
//    
//    
//    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
//    [postData setObject:ACCESS_KEY forKey:@"access_key"];
//    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
//    
//    [apiLoginManager POST:[NSString stringWithFormat:@"%@/users/api-get-user-types",SERVER_BASE_API_URL] parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
//        
//        
//        userTypes=[[NSMutableArray alloc] initWithArray:responseObject];
//        NSLog(@"userTypes %@",userTypes);
//        
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"error %@",error);
//        
//    }];
//}

//-(void) loadArtPreferences
//{
//    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
//    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
//    
//    
//    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
//    [postData setObject:ACCESS_KEY forKey:@"access_key"];
//    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
//    
//    [apiLoginManager POST:[NSString stringWithFormat:@"%@/users/api-get-art-prefs",SERVER_BASE_API_URL] parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject)
//    {
//        artPreference=[[NSMutableArray alloc] initWithArray:responseObject];
//      //  userTypes=[[NSMutableArray alloc] initWithArray:responseObject];
//        NSLog(@"ArtPreferences %@",responseObject);
//        
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"error %@",error);
//        
//    }];
//}

- (IBAction)searchButtonAction:(id)sender {
    
    NSMutableDictionary *searchInfo = [[NSMutableDictionary alloc]init];
    
    
    [searchInfo setObject:self.hashTagTextField.text forKey:@"keyword"];
    
    
    if([btnSelectDate.titleLabel.text isEqualToString:@"Last 1 month"])
        [searchInfo setObject:@"1" forKey:@"date_range"];
    
    else if([btnSelectDate.titleLabel.text isEqualToString:@"Last 3 months"])
        [searchInfo setObject:@"3" forKey:@"date_range"];
    
    else if([btnSelectDate.titleLabel.text isEqualToString:@"Last 6 months"])
        [searchInfo setObject:@"6" forKey:@"date_range"];
    
    else if([btnSelectDate.titleLabel.text isEqualToString:@"Last 1 year"])
        [searchInfo setObject:@"12" forKey:@"date_range"];
    else
        [searchInfo setObject:@"0" forKey:@"date_range"];
    
    NSString *placeId=[locationDictionary objectForKey:@"placeId"];
    
    if(placeId.length>0)
       [searchInfo setObject:placeId forKey:@"google_place_id"];
    else
      [searchInfo setObject:@"" forKey:@"google_place_id"];
    
    
    [searchInfo setObject:self.artistTextField.text forKey:@"artist"];

    //[searchInfo setObject:@"" forKey:@"google_place_id"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pushAdvancedResultView" object:searchInfo];
    
    
}

-(void)configureSimpleSearchTextField
{
    // Start visible - Default: false
    self.artistTextField.startVisible = true;
    
    // Set data source
   
    
    [self.artistTextField filterStrings:artistList];
    self.artistTextField.delegate=self;
}

-(void)configureDateRangeTextField
{
    // Start visible - Default: false
    self.dateRangeTextField.startVisible = true;
    self.dateRangeTextField.tag=301;
    // Set data source
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"Any",@"Last 1 month", @"Last 3 months",@"Last 6 months",@"Last 1 year",nil];
    
    
    [self.dateRangeTextField filterStrings:arr];
  
}


-(IBAction)getLocationButtonAction:(id)sender {
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pushLocationFromAdvancedView" object:nil];
    
}


- (IBAction)retreivelocationData:(NSNotification*) notification {
    
    locationDictionary =[[NSMutableDictionary alloc] init];
    locationDictionary=[[notification object] mutableCopy];
    
    NSLog(@"locationDictionary %@",locationDictionary);
    self.locationTitle.text=[locationDictionary objectForKey:@"title"];
    self.locationAddress.text=[locationDictionary objectForKey:@"address"];
    
    [self showLocationView];
    
}



-(void)showLocationView
{
    self.locationButton.hidden=YES;
    
    self.locationTitle.hidden=NO;
    self.locationAddress.hidden=NO;
    self.closeLocation.hidden=NO;
    

}

-(void)hideLocationView
{
    self.locationButton.hidden=NO;
    
    self.locationTitle.hidden=YES;
    self.locationAddress.hidden=YES;
    self.closeLocation.hidden=YES;
    
    self.locationTitle.text=@"";
    self.locationAddress.text=@"";

}

- (IBAction)closeLocationView:(id)sender {
    
    [self hideLocationView];
    
}

-(void)dealloc{
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}



@end
