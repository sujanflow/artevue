//
//  ExploreViewController.h
//  Artegram
//
//  Created by Tanvir Palash on 6/10/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"

#import <CoreLocation/CoreLocation.h>
#import "HNKGooglePlacesAutocomplete.h"


#import "LocationsViewController.h"

@interface ExploreViewController : UIViewController<UISearchBarDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,CLLocationManagerDelegate,LocationPostDelegate,SDWebImageManagerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (weak, nonatomic) IBOutlet UICollectionView *exploreCollectionView;

@property (weak, nonatomic) IBOutlet UIButton *discoverButton;

@property (weak, nonatomic) IBOutlet UIView *discoverButtonView;

@property (weak, nonatomic) IBOutlet UIView *pagerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBarLeading;

@property(nonatomic,strong) NSMutableDictionary *hashTagDictionary;
@property(nonatomic,strong) NSMutableDictionary *userDictionary;
@property(nonatomic,strong) NSMutableDictionary *locationDictionary;
@property(nonatomic,strong) NSMutableDictionary *advancedDictionary;

@property (strong, nonatomic) HNKGooglePlacesAutocompleteQuery *searchQuery;

@property (strong, nonatomic) NSArray *searchResults;


@property UIRefreshControl *refreshControl;
@end
