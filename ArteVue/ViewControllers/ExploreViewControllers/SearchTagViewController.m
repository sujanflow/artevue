//
//  SearchTagViewController.m
//  Artegram
//
//  Created by Sujan on 7/20/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "SearchTagViewController.h"

#import "ExploreViewController.h"

@interface SearchTagViewController ()

@end

@implementation SearchTagViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTableView:) name:@"reloadTableWithTagSearch" object:nil];
    
    self.searchTagTableView.delegate = self;
    self.searchTagTableView.dataSource = self;
    
    self.searchTagTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.searchTagTableView.frame.size.width, 1)];

}

-(void) viewDidAppear:(BOOL)animated
{
    
    NSLog(@"testing %@",self.testDataTags);
  //  NSLog(@"search result  %@",self.searchResult);
    
}

-(void)dealloc{
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

-(void)reloadTableView:(NSNotification*) notification
{
    self.searchResult=[[NSMutableArray alloc] init];
    self.searchResult=[[notification object] mutableCopy];
     NSLog(@"search result  %@",self.searchResult);
    
    if(self.searchResult.count)
    {
        [self.searchTagTableView setHidden:NO];

        [self.searchTagTableView reloadData];

    }else
    {
        [self.searchTagTableView setHidden:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.searchResult.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"searchTagsCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
   // UIImageView *tagIcon = (UIImageView*) [cell viewWithTag:1];
    UILabel *tagName = (UILabel*) [cell viewWithTag:2];
    UILabel *postNumber = (UILabel*) [cell viewWithTag:3];
    
    
    [tagName setText:[[self.searchResult objectAtIndex:indexPath.row] objectForKey:@"hashtag"]];
     postNumber.text =[NSString stringWithFormat:@"%d posts",[[[self.searchResult objectAtIndex:indexPath.row] objectForKey:@"use_count"]intValue]];
    
    return cell;
}

#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"table cell selected");
    
    NSMutableDictionary *hashTagInfo = [[NSMutableDictionary alloc]init];
    
    [hashTagInfo setObject:[[self.searchResult objectAtIndex:indexPath.row] objectForKey:@"hashtag"] forKey:@"tagName"];
    [hashTagInfo setObject:[[self.searchResult objectAtIndex:indexPath.row] objectForKey:@"id"] forKey:@"tagId"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pushHashTagView" object:hashTagInfo];
    

}

@end
