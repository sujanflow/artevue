//
//  HashTagViewViewController.m
//  Artegrams
//
//  Created by Sujan on 8/4/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "HashTagViewViewController.h"
#import "ExploreTableViewController.h"
#import "Constants.h"
#import "UserAccount.h"

#import "NSArray+NullReplacement.h"
#import "NSDictionary+NullReplacement.h"

#import "DGActivityIndicatorView.h"

#import <Realm/Realm.h>
#import "Posts.h"

#import "ServerManager.h"


@interface HashTagViewViewController (){

    NSMutableArray *topHashTagPost;
    NSMutableArray *latestHashTagPost;
    
    RLMRealmConfiguration *realmv1Configuration;
    NSMutableArray *downloadingIds;
}

@property (nonatomic) DGActivityIndicatorView *activityIndicatorView;


@end

@implementation HashTagViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)DGActivityIndicatorAnimationTypeTriplePulse tintColor:[UIColor blackColor]];
    CGFloat width = self.view.bounds.size.width / 5.0f;
    CGFloat height = self.view.bounds.size.width / 5.0f;
    
    self.activityIndicatorView.frame = CGRectMake(self.view.bounds.size.width/2-width/2,self.view.bounds.size.height/2-height/2 - 40, width, height);
    [self.view addSubview:self.activityIndicatorView];
    
    [[NSFileManager defaultManager] removeItemAtURL:[RLMRealmConfiguration defaultConfiguration].fileURL error:nil];
    
    NSURL *defaultRealmURL = [RLMRealmConfiguration defaultConfiguration].fileURL;
    NSURL *defaultRealmParentURL = [defaultRealmURL URLByDeletingLastPathComponent];
    NSURL *realmv1URL = [defaultRealmParentURL URLByAppendingPathComponent:@"defaultv1.realm"];
    
    realmv1Configuration = [[RLMRealmConfiguration defaultConfiguration] copy];
    realmv1Configuration.fileURL = realmv1URL;
    
    downloadingIds=[[NSMutableArray alloc] init];
    
    
    self.hashTagCollectionView.delegate = self;
    self.hashTagCollectionView.dataSource = self;
    
    topHashTagPost = [[NSMutableArray alloc]init];
    latestHashTagPost = [[NSMutableArray alloc]init];
    
    self.navigationLabel.text = [self.hashTagInfoDic objectForKey:@"tagName"];
    
    [self.activityIndicatorView startAnimating];
    
    NSLog(@"self.hashTagInfoDic %@",self.hashTagInfoDic);
    [self getHashTagId];
    
//    if([[self.hashTagInfoDic objectForKey:@"tagId"] intValue]<=0)
//    {
//        [self getHashTagId];
//    }
//    else{
//        [self makeRequest];
//    }
    
 //   [SDWebImageManager sharedManager].delegate=self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.hashTagCollectionView reloadData];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    
    downloadingIds=[[NSMutableArray alloc] init];
    [[SDWebImageDownloader sharedDownloader] cancelAllDownloads];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(void) getHashTagId{
    
    [[ServerManager sharedManager] getTopHashtagByName:[[self.hashTagInfoDic objectForKey:@"tagName"] substringFromIndex:1] withCompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
        if ( resultDataArray!=nil) {
            
            topHashTagPost=[[NSMutableArray alloc] initWithArray:[[resultDataArray objectForKey:@"data"] mutableCopy]];
            
            if(topHashTagPost.count || latestHashTagPost.count )
            {
                self.hashTagCollectionView.hidden = NO;
                self.noPostLabel.hidden=YES;
                
                
            }
            else
            {
                self.hashTagCollectionView.hidden = YES;
                
                self.noPostLabel.hidden=NO;
            }
            
            
            [self.hashTagCollectionView reloadData];
            
            [self.activityIndicatorView stopAnimating];

            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
            
                
                
            });
            
        }
    }];
    
    [[ServerManager sharedManager] getLatestHashtagByName:[[self.hashTagInfoDic objectForKey:@"tagName"]substringFromIndex:1] withCompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
        if ( resultDataArray!=nil) {
            
            latestHashTagPost=[[NSMutableArray alloc] initWithArray:[[resultDataArray objectForKey:@"data"] mutableCopy]];
            
            if(topHashTagPost.count || latestHashTagPost.count )
            {
                self.hashTagCollectionView.hidden = NO;
                self.noPostLabel.hidden=YES;
                
                
            }
            else
            {
                self.hashTagCollectionView.hidden = YES;
                
                self.noPostLabel.hidden=NO;
            }
            
            
            [self.hashTagCollectionView reloadData];
            
            [self.activityIndicatorView stopAnimating];

            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                
            });
            
        }
    }];

   
}

-(void) makeRequest{
    
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    [postData setObject:ACCESS_KEY forKey:@"access_key"];
    [postData setObject:[NSNumber numberWithInt:[[self.hashTagInfoDic objectForKey:@"tagId"]intValue]] forKey:@"hashtag_id"];
    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
    
    NSLog(@"postData %@",postData);
    
    [apiLoginManager POST:[NSString stringWithFormat:@"%@/posts/api-fetch-posts-by-hashtag-id",SERVER_BASE_API_URL] parameters:postData
                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                      
                      
                     // NSLog(@"Response: %@", responseObject);
                      NSMutableDictionary * hashtagPost = [[NSMutableDictionary alloc]initWithDictionary:[responseObject dictionaryByReplacingNullsWithBlanks]];
                      
                      
                      //hashTagPost = [responseObject mutableCopy];
                      topHashTagPost=[hashtagPost objectForKey:@"top_posts"];
                      latestHashTagPost=[hashtagPost objectForKey:@"latest_posts"];
                      
                      if(topHashTagPost.count || latestHashTagPost.count )
                      {
                          self.hashTagCollectionView.hidden = NO;
                          self.noPostLabel.hidden=YES;
                          
                          
                      }
                      else
                      {
                          self.hashTagCollectionView.hidden = YES;
                          
                          self.noPostLabel.hidden=NO;
                      }
                      

                      [self.hashTagCollectionView reloadData];
                      
                      [self.activityIndicatorView stopAnimating];
                  }
     
                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                      
                      NSLog(@"Error: %@", error);
                      
                      self.view.userInteractionEnabled=YES;
                      
                      NSLog(@"%@",operation.responseString);
                      
                      [self.activityIndicatorView stopAnimating];
                      
                  }];
    
    
    
}


#pragma mark - CollectionView data source

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    
    
    return 2;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if(section==0)
        return topHashTagPost.count;
    else
        return latestHashTagPost.count;
    
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *identifier = @"hashTagPhotoCell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    UIImageView *searchImage = (UIImageView*) [cell viewWithTag:1];
    UIActivityIndicatorView *activityInd = (UIActivityIndicatorView*) [cell viewWithTag:2];
    
    activityInd.hidden=NO;
    [activityInd startAnimating];
    
    [searchImage setShowActivityIndicatorView:YES];
    [searchImage setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    if(indexPath.section==0)
    {
//        [searchImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[topHashTagPost objectAtIndex:indexPath.row] objectForKey:@"image"]]]];
        
        RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
        //    NSLog(@"realmv1Configuration %@",realmv1Configuration.fileURL);
        
        RLMResults *result=[Posts objectsInRealm:realm where:[NSString stringWithFormat:@"postId = %@",[[topHashTagPost objectAtIndex:indexPath.row] objectForKey:@"id"]]];
        
        
        if(result.count)
        {
            Posts *post=[[Posts alloc] init];
            post=[result objectAtIndex:0];
            NSLog(@"result %@",result);
            
           // [UIView transitionWithView:searchImage duration:0.2f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                [activityInd stopAnimating];
                searchImage.image=[UIImage imageWithData:post.imageData];
//            } completion:^(BOOL finished) {
//                ;
//            }];
            
            
        }
        else
        {
            searchImage.image=nil;
            //searchImage.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"square_image" ofType:@"png"]];
            
            if([downloadingIds containsObject: [[topHashTagPost objectAtIndex:indexPath.row] objectForKey:@"id"]])
            {
                //do nothing
            }
            else
            {
                
                [downloadingIds addObject:[[topHashTagPost objectAtIndex:indexPath.row] objectForKey:@"id"]];
                
                SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
                [SDWebImageDownloader sharedDownloader].maxConcurrentDownloads=1;
                [downloader downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[topHashTagPost objectAtIndex:indexPath.row] objectForKey:@"image"] ]]
                                         options:0
                                        progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                            // progression tracking code
                                            NSLog(@"receivedSize %ld expectedSize %ld",(long)receivedSize,(long)expectedSize);
                                            
                                        }
                                       completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                           if (image && finished) {
                                               // do something with image
                                               
                                               
                                               //                                           dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                                               //                                           dispatch_async(queue, ^{
                                               //                                               // Get new realm and table since we are in a new thread
                                               
                                               NSLog(@"done in explore collection %@",[[topHashTagPost objectAtIndex:indexPath.row] objectForKey:@"id"]);
                                               
                                               //RLMRealm *realm = [RLMRealm defaultRealm];
                                               
                                               
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   //searchImage.image=image;
                                                   
                                                   
                                                   RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
                                                   
                                                   [realm beginWriteTransaction];
                                                   //                                                   [Posts createOrUpdateInRealm:realm withValue:@{@"postId": [NSNumber numberWithInt:[[[discoverPost objectAtIndex:indexPath.row] objectForKey:@"id"] intValue]],
                                                   //                                                                                          @"imageData": data}];
                                                   //
                                                   [Posts createOrUpdateInRealm:realm withValue:@{@"primId": [NSString stringWithFormat:@"%@",[[topHashTagPost objectAtIndex:indexPath.row] objectForKey:@"id"]]
                                                                                                  ,@"postId": [NSNumber numberWithInt:[[[topHashTagPost objectAtIndex:indexPath.row] objectForKey:@"id"] intValue]],
                                                                                                  @"imageData": data}];
                                                   
                                                   
                                                   
                                                   [realm commitWriteTransaction];
                                                   
                                                   [downloadingIds removeObject:[[topHashTagPost objectAtIndex:indexPath.row] objectForKey:@"id"]];
                                                   // [self reloadData];
                                                   
                                                   [self.hashTagCollectionView reloadItemsAtIndexPaths:@[indexPath]];
                                                   //      });
                                                   
                                               });
                                           }
                                           
                                           
                                       }];
                
            }
            
        }

        
    }
    else
    {
//        [searchImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[latestHashTagPost objectAtIndex:indexPath.row] objectForKey:@"image"]]]];
        
        RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
        //    NSLog(@"realmv1Configuration %@",realmv1Configuration.fileURL);
        
        RLMResults *result=[Posts objectsInRealm:realm where:[NSString stringWithFormat:@"postId = %@",[[latestHashTagPost objectAtIndex:indexPath.row] objectForKey:@"id"]]];
        
        
        if(result.count)
        {
            Posts *post=[[Posts alloc] init];
            post=[result objectAtIndex:0];
            NSLog(@"result %@",result);
            
       //     [UIView transitionWithView:searchImage duration:0.2f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                [activityInd stopAnimating];
                searchImage.image=[UIImage imageWithData:post.imageData];
//            } completion:^(BOOL finished) {
//                ;
//            }];
//            
            
        }
        else
        {
            searchImage.image=nil;
            //searchImage.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"square_image" ofType:@"png"]];
            
            if([downloadingIds containsObject: [[latestHashTagPost objectAtIndex:indexPath.row] objectForKey:@"id"]])
            {
                //do nothing
            }
            else
            {
                
                [downloadingIds addObject:[[latestHashTagPost objectAtIndex:indexPath.row] objectForKey:@"id"]];
                
                SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
                [SDWebImageDownloader sharedDownloader].maxConcurrentDownloads=1;
                [downloader downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[latestHashTagPost objectAtIndex:indexPath.row] objectForKey:@"image"] ]]
                                         options:0
                                        progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                            // progression tracking code
                                            NSLog(@"receivedSize %ld expectedSize %ld",(long)receivedSize,(long)expectedSize);
                                            
                                        }
                                       completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                           if (image && finished) {
                                               // do something with image
                                               
                                               
                                               //                                           dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                                               //                                           dispatch_async(queue, ^{
                                               //                                               // Get new realm and table since we are in a new thread
                                               
                                               NSLog(@"done in explore collection %@",[[latestHashTagPost objectAtIndex:indexPath.row] objectForKey:@"id"]);
                                               
                                               //RLMRealm *realm = [RLMRealm defaultRealm];
                                               
                                               
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   //searchImage.image=image;
                                                   
                                                   
                                                   RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
                                                   
                                                   [realm beginWriteTransaction];
                                                   //                                                   [Posts createOrUpdateInRealm:realm withValue:@{@"postId": [NSNumber numberWithInt:[[[discoverPost objectAtIndex:indexPath.row] objectForKey:@"id"] intValue]],
                                                   //                                                                                          @"imageData": data}];
                                                   //
                                                   [Posts createOrUpdateInRealm:realm withValue:@{@"primId": [NSString stringWithFormat:@"%@",[[latestHashTagPost objectAtIndex:indexPath.row] objectForKey:@"id"]]
                                                                                                  ,@"postId": [NSNumber numberWithInt:[[[latestHashTagPost objectAtIndex:indexPath.row] objectForKey:@"id"] intValue]],
                                                                                                  @"imageData": data}];
                                                   
                                                   
                                                   
                                                   [realm commitWriteTransaction];
                                                   
                                                   [downloadingIds removeObject:[[latestHashTagPost objectAtIndex:indexPath.row] objectForKey:@"id"]];
                                                   // [self reloadData];
                                                   
                                                   [self.hashTagCollectionView reloadItemsAtIndexPaths:@[indexPath]];
                                                   //      });
                                                   
                                               });
                                           }
                                           
                                           
                                       }];
                
            }
            
        }

    }
    
    return cell;
    
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (kind == UICollectionElementKindSectionHeader) {
        
        UICollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:
                                       UICollectionElementKindSectionHeader withReuseIdentifier:@"headerCell" forIndexPath:indexPath];
        
        UILabel *titleLabel= [headerView viewWithTag:101];
        
        
        if(indexPath.section==0 && topHashTagPost.count)
            titleLabel.text=@"Trending Art";
        else if(indexPath.section==1 && latestHashTagPost.count )
            titleLabel.text=@"Latest Art";
        else
        {
            titleLabel.text=@"";
        }
        return headerView;
    }
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    return CGSizeMake(([UIScreen mainScreen].bounds.size.width-2)/3, ([UIScreen mainScreen].bounds.size.width-2)/3);
    
    
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    ExploreTableViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ExploreTableViewController"];
    
    if(indexPath.section==0)
    {
        controller.indexNumber = indexPath.row;
        
        controller.discoverPostArray = topHashTagPost;
        
    }
    else
    {
        controller.indexNumber = indexPath.row;
        
        controller.discoverPostArray = latestHashTagPost;
        
    }
    controller.isComeFromTagView = YES;
    controller.hashTagName = [self.hashTagInfoDic objectForKey:@"tagName"];
    
    [self.navigationController pushViewController:controller animated:YES];
    
    
}
- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

//-(UIImage *)imageManager:(SDWebImageManager *)imageManager transformDownloadedImage:(UIImage *)image withURL:(NSURL *)imageURL
//{
//    // NSLog(@"imageURL : %@",imageURL);
//    UIImage *resizedImage;
//    //  return resizedImage  = [image resizedImage:CGSizeMake(512, 512) interpolationQuality:kCGInterpolationDefault];
//    
//    float MIN_UPLOAD_RESOLUTION=756*756;
//    float factor;
//    float resol = image.size.height*image.size.width;
//    if (resol >MIN_UPLOAD_RESOLUTION){
//        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
//        resizedImage = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
//        return resizedImage;
//        
//    }
//    else
//        return image;
//}
//
//- (UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
//    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
//    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
//    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return scaledImage;
//}

@end
