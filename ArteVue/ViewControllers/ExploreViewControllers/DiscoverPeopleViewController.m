//
//  DiscoverPeopleViewController.m
//  Artegram
//
//  Created by Sujan on 7/17/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "DiscoverPeopleViewController.h"
#import "DiscoverPeopleTableViewCell.h"
#import "Constants.h"
#import "HexColors.h"
#import "ProfileViewController.h"
#import "UserAccount.h"
#import "ServerManager.h"

#import "NSDictionary+NullReplacement.h"

@interface DiscoverPeopleViewController (){

    NSMutableArray *discoverPeopleList;
    int pageIndex;
}

@end

@implementation DiscoverPeopleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //setting delegate
    pageIndex=1;
    
    self.discoverPeopleTableView.delegate = self;
    self.discoverPeopleTableView.dataSource = self;
    
    
    self.discoverPeopleTableView.estimatedRowHeight = 145;
    self.discoverPeopleTableView.rowHeight = UITableViewAutomaticDimension;
    
    self.discoverPeopleTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.discoverPeopleTableView.frame.size.width, 1)];
    
    [self makeRequest];
    
 //   [SDWebImageManager sharedManager].delegate=self;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) makeRequest{
    
    [[ServerManager sharedManager] getDiscoverUsersForPage:pageIndex WithCompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
        
        if ( resultDataArray!=nil) {
            
            NSMutableDictionary *responsedic= [[resultDataArray dictionaryByReplacingNullsWithBlanks] mutableCopy];
            discoverPeopleList=[[NSMutableArray alloc] initWithArray:[responsedic objectForKey:@"data"]];
            NSLog(@"Response: %@", discoverPeopleList);
            
            [self.discoverPeopleTableView reloadData];
            
            
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //[[[UIAlertView alloc] initWithTitle:@"Sorry, unable to fetch user data. Please try again." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                
                
            });
       
        }
        
    }];


}

#pragma mark - UITableView DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return discoverPeopleList.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    DiscoverPeopleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"discoverPeopleCell"];
    
    
    [cell.profileImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[discoverPeopleList objectAtIndex:indexPath.row]objectForKey:@"profile_picture"]]]];
    
    cell.prifileNameLabel.text = [[discoverPeopleList objectAtIndex:indexPath.row]objectForKey:@"username"];
    
    cell.userNameLabel.text = [[discoverPeopleList objectAtIndex:indexPath.row]objectForKey:@"name"];
    
    cell.postImageList = [[discoverPeopleList objectAtIndex:indexPath.row]objectForKey:@"latest_posts"];
    
    if (cell.postImageList.count == 0) {
        
        cell.collectionViewHeight.constant = 0;
    }
    else
    {
        cell.collectionViewHeight.constant = ([UIScreen mainScreen].bounds.size.width)/3;
    }

    //set follow button tag
    
    cell.followButton.tag = indexPath.row;
    
    //tap on header view
    
    UITapGestureRecognizer *tapOnHeader = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHeader:)];
    //[tapOnHeader setDelegate:self];
    tapOnHeader.numberOfTouchesRequired = 1;
    tapOnHeader.numberOfTapsRequired = 1;
    
    [cell addGestureRecognizer:tapOnHeader];
    cell.tag=indexPath.row;
    
    [cell.discoverPeopleCollectionView reloadData];
    
    return cell;
}


#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    
}

//cell header click action

-(void)tapHeader:(UITapGestureRecognizer *)recognizer{
    
    NSLog(@"tap on header %@",[discoverPeopleList objectAtIndex:recognizer.view.tag]);
    
    ProfileViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    
    controller.isUserOrFollower = YES;
    controller.userId = [[[discoverPeopleList objectAtIndex:recognizer.view.tag ] objectForKey:@"id"]intValue];
    
    [self.navigationController pushViewController:controller animated:YES];
    
}


- (IBAction)followButtonAction:(UIButton *)sender {
    
    
    sender.userInteractionEnabled = NO;
    
    
    [[ServerManager sharedManager] postFollowUserId:[[[discoverPeopleList objectAtIndex:sender.tag]objectForKey:@"id"] intValue] WithCompletion:^(BOOL success) {
        
        sender.userInteractionEnabled = YES;
        
        if (success) {
            
            
            sender.selected=!sender.selected;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"loadAllFeed" object:nil];
        }
        
    }];
    
    
}


- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];

}

//-(UIImage *)imageManager:(SDWebImageManager *)imageManager transformDownloadedImage:(UIImage *)image withURL:(NSURL *)imageURL
//{
//    // NSLog(@"imageURL : %@",imageURL);
//    UIImage *resizedImage;
//    //  return resizedImage  = [image resizedImage:CGSizeMake(512, 512) interpolationQuality:kCGInterpolationDefault];
//    
//    float MIN_UPLOAD_RESOLUTION=756*756;
//    float factor;
//    float resol = image.size.height*image.size.width;
//    if (resol >MIN_UPLOAD_RESOLUTION){
//        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
//        resizedImage = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
//        return resizedImage;
//        
//    }
//    else
//        return image;
//}
//
//- (UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
//    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
//    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
//    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return scaledImage;
//}

@end
