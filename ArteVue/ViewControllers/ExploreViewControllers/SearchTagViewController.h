//
//  SearchTagViewController.h
//  Artegram
//
//  Created by Sujan on 7/20/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTagViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *searchTagTableView;

@property(nonatomic,strong) NSString *testDataTags;

@property(nonatomic,strong) NSMutableArray *searchResult;

@end
