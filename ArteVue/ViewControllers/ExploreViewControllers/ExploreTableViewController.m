//
//  ExploreTableViewController.m
//  Artegram
//
//  Created by Sujan on 7/25/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "ExploreTableViewController.h"
#import "FeedTableViewCell.h"
#import "HexColors.h"
#import "LikersViewController.h"
#import "Constants.h"
#import "ProfileViewController.h"
#import "UserAccount.h"
#import "CommentViewController.h"
#import "HashTagViewViewController.h"
#import "MessagingViewController.h"
#import "SharePhotoViewController.h"

#import "ExploreTableViewCell.h"

#import "NSArray+NullReplacement.h"
#import "NSDictionary+NullReplacement.h"

#import <Realm/Realm.h>
#import "Posts.h"
#import "ServerManager.h"
#import "ZHPopupView.h"
@interface ExploreTableViewController (){

    NSMutableDictionary* sharePostDictionary;
    UIImageView* imageViewForShare;

    RLMRealmConfiguration *realmv1Configuration;
    NSMutableArray *downloadingIds;
}


@end

@implementation ExploreTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [[NSFileManager defaultManager] removeItemAtURL:[RLMRealmConfiguration defaultConfiguration].fileURL error:nil];
    
    NSURL *defaultRealmURL = [RLMRealmConfiguration defaultConfiguration].fileURL;
    NSURL *defaultRealmParentURL = [defaultRealmURL URLByDeletingLastPathComponent];
    NSURL *realmv1URL = [defaultRealmParentURL URLByAppendingPathComponent:@"defaultv1.realm"];
    
    realmv1Configuration = [[RLMRealmConfiguration defaultConfiguration] copy];
    realmv1Configuration.fileURL = realmv1URL;
    
    
    if (self.isComeFromTagView) {
        
        self.navigationLabel.text = self.hashTagName;
        
    }else if(self.isComeFromLocationView){
        
        self.navigationLabel.text = self.locationName;
        
    }
    //setting delegate
    self.exploreTableView.delegate = self;
    self.exploreTableView.dataSource = self;
    
    
    self.exploreTableView.estimatedRowHeight = 496;
    self.exploreTableView.rowHeight = UITableViewAutomaticDimension;
    
    self.exploreTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.exploreTableView.frame.size.width, 1)];
    
    //NSLog(@"discoverPostArray %@",self.discoverPostArray);
    
    downloadingIds=[[NSMutableArray alloc] init];
    
    self.discoverPostArray=[[self.discoverPostArray arrayByReplacingNullsWithBlanks] mutableCopy];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:self.indexNumber];
    
    [self.exploreTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
   // [SDWebImageManager sharedManager].delegate=self;
    
    
}

-(void) viewDidAppear:(BOOL)animated
{
     imageViewForShare=[[UIImageView alloc]init];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"followActionHappened"])
    {
        
        [self.navigationController popViewControllerAnimated:YES];
        

    }
}

-(void)viewWillDisappear:(BOOL)animated
{
        downloadingIds=[[NSMutableArray alloc] init];
    
      [[SDWebImageDownloader sharedDownloader] cancelAllDownloads];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return self.discoverPostArray.count;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    static NSString *HeaderCellIdentifier = @"feedHeaderTableCell";
    
    ExploreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:HeaderCellIdentifier];
    if (cell == nil) {
        cell = [[ExploreTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:HeaderCellIdentifier];
    }
//    
//    UIImageView *profileImage = (UIImageView*) [cell viewWithTag:1];
//    UILabel *profileName = (UILabel*) [cell viewWithTag:2];
//    UIButton *moreButton = (UIButton*) [cell viewWithTag:4];
//    UIButton *followButton = (UIButton*) [cell viewWithTag:5];
//    
    cell.followButton.layer.borderWidth = 0.0f;
    cell.followButton.layer.borderColor = [[HXColor hx_colorWithHexString:@"000000"]CGColor];
    
    //set cell header tag
    
    cell.tag= section;
    

    //set follow button tag
    
    if (self.isComeFromTagView || self.isComeFromLocationView)
    {
        cell.followButton.hidden=YES;
        
    }else
        cell.followButton.hidden=NO;
    
    
    cell.followButton.tag=section;
    cell.moreButton.tag=section;
    
    //tap on header view
    
    UITapGestureRecognizer *tapOnHeader = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHeader:)];
    //[tapOnHeader setDelegate:self];
    tapOnHeader.numberOfTouchesRequired = 1;
    tapOnHeader.numberOfTapsRequired = 1;
    [cell addGestureRecognizer:tapOnHeader];

    //[profileName setText:[[[self.discoverPostArray objectAtIndex:section] objectForKey:@"user"] objectForKey:@"username"]];
    NSString *locationString=[[self.discoverPostArray objectAtIndex:section] objectForKey:@"address_title"];
    
    if (locationString.length>0) {
        
        // [profileName setText:[NSString stringWithFormat:@"%@\n%@",[[[feedList objectAtIndex:section] objectForKey:@"user"] objectForKey:@"username"],locationString]];
        
        UIFont *userNameFont = [UIFont fontWithName:@"AzoSans-Medium" size:14];
        UIFont *locationFont = [UIFont fontWithName:@"AzoSans-Regular" size:10];
        
        
        NSDictionary *locationDict = [NSDictionary dictionaryWithObject: locationFont forKey:NSFontAttributeName];
        NSDictionary *userDict = [NSDictionary dictionaryWithObject:userNameFont forKey:NSFontAttributeName ];
        
        NSMutableAttributedString *uAttrString = [[NSMutableAttributedString alloc] initWithString:[[[self.discoverPostArray objectAtIndex:section] objectForKey:@"owner"] objectForKey:@"username"] attributes: userDict];
        NSMutableAttributedString *lAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@",locationString] attributes: locationDict];
        NSMutableAttributedString *dAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@",[self relativeDateStringForDate:[self getDateFromString:[[self.discoverPostArray objectAtIndex:section] objectForKey:@"created_at"]]]] attributes: locationDict];
        
        
        
        
        [uAttrString appendAttributedString:lAttrString];
        [uAttrString appendAttributedString:dAttrString];
        
        cell.userName.attributedText=uAttrString;
        
    }
    else
    {
        UIFont *userNameFont = [UIFont fontWithName:@"AzoSans-Medium" size:14];
        UIFont *locationFont = [UIFont fontWithName:@"AzoSans-Regular" size:10];
        
        
        NSDictionary *locationDict = [NSDictionary dictionaryWithObject: locationFont forKey:NSFontAttributeName];
        NSDictionary *userDict = [NSDictionary dictionaryWithObject:userNameFont forKey:NSFontAttributeName ];
        
        NSMutableAttributedString *uAttrString = [[NSMutableAttributedString alloc] initWithString:[[[self.discoverPostArray objectAtIndex:section] objectForKey:@"owner"] objectForKey:@"username"] attributes: userDict];
        
        NSMutableAttributedString *dAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@",[self relativeDateStringForDate:[self getDateFromString:[[self.discoverPostArray objectAtIndex:section] objectForKey:@"created_at"]]]] attributes: locationDict];
        
        
        NSLog(@"date %@", dAttrString);
        
        [uAttrString appendAttributedString:dAttrString];
        
        cell.userName.attributedText=uAttrString;
        
        
    }
    
    cell.profilePic.layer.cornerRadius = cell.profilePic.bounds.size.width/2;
    //self.profilePicture.layer.borderWidth = 1.1f;
    cell.profilePic.clipsToBounds = YES;
    cell.profilePic.layer.masksToBounds = YES;
    
    if([[[[self.discoverPostArray objectAtIndex:section] objectForKey:@"owner"] objectForKey:@"profile_picture"] isEqual:[NSNull null]] || [[[[self.discoverPostArray objectAtIndex:section] objectForKey:@"owner"] objectForKey:@"profile_picture"]isEqualToString:@""])
        
        [cell.profilePic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,@"img/profile-holder.png"]]];
    
    else
        [cell.profilePic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[[self.discoverPostArray objectAtIndex:section] objectForKey:@"owner"] objectForKey:@"profile_picture"]]]];
    

    return cell;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    FeedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"feedTableCell"];
    
    
    
    //set feed image
    
 //   [cell.feedImage setShowActivityIndicatorView:YES];
 //   [cell.feedImage setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    for (UIView *v in cell.feedImage.subviews) {
        [v removeFromSuperview];
    }
   // [ cell.feedImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"image"] ]]];
    
    cell.feedImage.tag=indexPath.section;
    
    RLMRealm *realmInExplore = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
    RLMResults *result=[Posts objectsInRealm:realmInExplore where:[NSString stringWithFormat:@"postId = %@",[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"id"]]];
    
    if(result.count)
    {
        Posts *post=[[Posts alloc] init];
        post=[result objectAtIndex:0];
       // NSLog(@"result %@",result);
        
        
      //  [UIView transitionWithView:cell.feedImage duration:0.3f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            cell.feedImage.image=[UIImage imageWithData:post.imageData];
            
//        } completion:^(BOOL finished) {
//            ;
//        }];
    }
    else
    {
        cell.feedImage.image=nil;
        
        if([downloadingIds containsObject: [[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"id"]])
        {
            //do nothing
            NSLog(@"downloading in explore collection");
        }
        else
        {
            
            [downloadingIds addObject:[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"id"]];
            
            SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
            [SDWebImageDownloader sharedDownloader].maxConcurrentDownloads=1;
            
            [downloader downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"image"] ]]
                                     options:0
                                    progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                        // progression tracking code
                                        // NSLog(@"receivedSize %ld expectedSize %ld",(long)receivedSize,(long)expectedSize);
                                        
                                    }
                                   completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                       if (image && finished) {
                                           // do something with image
                                           
                                           //   Posts *post = [[Posts alloc] init];
                                           
                                     
                                               // Get new realm and table since we are in a new thread
                                           
                                               
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   //  cell.feedImage=(UIImageView*) [cell viewWithTag:indexPath.section] ;
                                                   //  cell.feedImage.image=image;
                                                   
                                                   
                                                   NSLog(@"done for %@",[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"id"]);
                                                   
                                                   //RLMRealm *realm = [RLMRealm defaultRealm];
                                                   
                                                   RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
                                                   
                                                   
                                                   [realm beginWriteTransaction];
                                                   //     [Posts createOrUpdateInRealm:realm withValue:@{@"postId": [NSNumber numberWithInt:[[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"id"] intValue]],
                                                   //                                            @"imageData": data}];
                                                   [Posts createOrUpdateInRealm:realm withValue:@{@"primId": [NSString stringWithFormat:@"%@",[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"id"]]
                                                                                                  ,@"postId": [NSNumber numberWithInt:[[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"id"] intValue]],
                                                                                                  @"imageData": data}];
                                                   
                                                   
                                                   [realm commitWriteTransaction];
                                                   
                                                   
                                                   [downloadingIds removeObject:[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"id"]];
                                                   //[self reloadData];
                                                   
//                                                   [self.exploreTableView reloadData];
//                                                   
//                                                   [self.exploreTableView layoutIfNeeded];
//
                                                   
                                                   FeedTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                                                   [UIView transitionWithView:cell.feedImage duration:0.15f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                                                       cell.feedImage.image=image;
                                                       
                                                   } completion:^(BOOL finished) {
                                                       ;
                                                   }];
                                                   
                                                   
                                                   
                                                   //[self.feedTableView beginUpdates];
                                                   //[self.feedTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                                                   //[self.feedTableView endUpdates];
                                                   
                                               });
                                               
                                         
                                       }
                                       
                                       
                                   }];
            
        }
        
    }
    
    cell.imageViewHeightConstraint.constant=[UIScreen mainScreen].bounds.size.width/[[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"aspect_ratio"] floatValue];
    
    cell.buyButton.tag=indexPath.section;
    if([[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"has_buy_btn"] intValue])
    {
        cell.buyButtonHeightConstraint.constant=30;
        cell.buyButton.hidden=NO;
        if([[[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"owner"] objectForKey:@"id"] intValue]==[UserAccount sharedManager].userId)
        {
            cell.buyButton.alpha = 0.8;
            cell.buyButton.enabled=NO;
        }
        else{
            cell.buyButton.alpha = 1.0;
            cell.buyButton.enabled=YES;
            
        }
    }
    else
    {
        cell.buyButtonHeightConstraint.constant=0;
        cell.buyButton.hidden=YES;
    }
    
    
   // cell.feedImage.tag=indexPath.section;
   // cell.imageViewHeightConstraint.constant=[UIScreen mainScreen].bounds.size.width/[[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"aspect_ratio"] floatValue];
    
    UITapGestureRecognizer* postImageTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnPostImage:)];
    postImageTapGesture.numberOfTapsRequired = 1;
    [cell.feedImage addGestureRecognizer:postImageTapGesture];
    cell.feedImage.userInteractionEnabled = YES;
    
    //double tap
    UITapGestureRecognizer* postImageDoubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapOnPostImage:)];
    postImageDoubleTap.numberOfTapsRequired = 2;
    [cell.feedImage addGestureRecognizer:postImageDoubleTap];
    cell.feedImage.userInteractionEnabled = YES;
    [postImageTapGesture requireGestureRecognizerToFail:postImageDoubleTap];

    
    
//    cell.feedImageRatio.active = NO;
//    
//    [cell.feedImage addConstraint:[NSLayoutConstraint
//                                   constraintWithItem:cell.feedImage
//                                   attribute:NSLayoutAttributeWidth
//                                   relatedBy:NSLayoutRelationEqual
//                                   toItem:cell.feedImage
//                                   attribute:NSLayoutAttributeHeight
//                                   multiplier:[[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"aspect_ratio"] floatValue]
//                                   constant:0]];
    
    
//    //set like count
    
    int likeCount = [[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"like_count"]intValue];
    
    if (likeCount != 0) {
        
        [cell.likeButton setTitle:[NSString stringWithFormat:@"%@",[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"like_count"]] forState:UIControlStateSelected];
        [cell.likeButton setTitle:[NSString stringWithFormat:@"%@",[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"like_count"]] forState:UIControlStateNormal];
    }
    else
    {
        [cell.likeButton setTitle:@"" forState:UIControlStateNormal];
    }
    
    
    int pinCount = [[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"pin_count"] intValue];
    
    if (pinCount != 0) {
        
        [cell.pinButton setTitle:[NSString stringWithFormat:@"%@",[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"pin_count"]] forState:UIControlStateSelected];
        [cell.pinButton setTitle:[NSString stringWithFormat:@"%@",[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"pin_count"]] forState:UIControlStateNormal];
    }else
    {
        [cell.pinButton setTitle:@"" forState:UIControlStateNormal];
    }
//    if (likeCount == 0) {
//        
//        [self changeConstraintForNoLike:cell];
//        
//    }else if (likeCount == 1) {
//        cell.likeIconTop.constant = 8;
//        cell.likeIconHeight.constant = 15;
//        cell.likeIconWidth.constant = 15;
//        cell.constraintBtLikeIconAndDesView.constant = 10;
//        cell.likeCountLabel.text=[NSString stringWithFormat:@"1 like"];
//        cell.commentCountLeading.constant = 10;
//        
//    }else{
//        cell.likeIconTop.constant = 8;
//        cell.likeIconHeight.constant = 15;
//        cell.likeIconWidth.constant = 15;
//        cell.constraintBtLikeIconAndDesView.constant = 10;
//        cell.likeCountLabel.text=[NSString stringWithFormat:@"%@ likes",[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"post_likes"]];
//        cell.commentCountLeading.constant = 10;
//    }
    
    
    
    //set like button tag
    cell.likeButton.tag=indexPath.section;
    
    
    //set like button state
    if([[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"is_liked"] integerValue])
    {
        cell.likeButton.selected=YES;
    }
    else
        cell.likeButton.selected=NO;
    
    
    
    cell.pinButton.tag=indexPath.section;
    
    if([[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"is_pinned"] integerValue])
    {
        cell.pinButton.selected=YES;
    }
    else
        cell.pinButton.selected=NO;
    
    cell.moreButton.tag = indexPath.section;
    
    
    //set post description
    
    
    
    if (![[[self.discoverPostArray objectAtIndex:indexPath.section]  objectForKey:@"description"] isEqual:[NSNull null]] && ![[[self.discoverPostArray objectAtIndex:indexPath.section]  objectForKey:@"description"] isEqualToString:@""]) {
        
        //        cell.constraintBtDescriptionAndLike.constant = 8;
        //        cell.constraintBtHashTagAndDescription.constant = 8;
        
        cell.descriptionLabel.text = [[self.discoverPostArray objectAtIndex:indexPath.section]  objectForKey:@"description"];
        
        
    }else{
        
        
        cell.descriptionLabel.text= @"";
        //        cell.likeIconTop.constant = 8;
        //
        //        cell.descriptionViewHeight.constant = 0;
        
        NSLog(@"cell.descriptionViewHeight.constant %f",cell.descriptionViewHeight.constant);
        
    }
    [cell.descriptionLabel sizeToFit];
    
    //set hashTag
    
    [cell.hashTagLabel sizeToFit];
    
    
    
    if (![[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"hashtags"] isEqual:[NSNull null]] && ![[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"hashtags"] isEqualToString:@""]) {
        
        cell.constraintBtHashTagAndDescription.constant = 8;
        cell.constraintBtHashTagAndFirstComment.constant = 8;
        
        cell.hashTagLabel.text = [[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"hashtags"];
        
    }else{
        
        cell.hashTagLabel.text= @"";
        cell.hashTagViewHeight.constant = 0;
        
    }
    
    //set comment button tag
    cell.commentButton.tag=indexPath.section;
    
    //comment array
    
//    [cell.firstCommentLabel sizeToFit];
//    [cell.secondCommentLabel sizeToFit];
//    [cell.thirdCommentLabel sizeToFit];
    
    if ( [[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"comment_count"] intValue]) {
        
        // [cell.commentButton setTitle:[NSString stringWithFormat:@"%lu comments",(unsigned long)commentArray.count] forState:UIControlStateSelected];
        [cell.commentButton setTitle:[NSString stringWithFormat:@"%@",[[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"comment_count"]] forState:UIControlStateNormal];
    }else{
        
        [cell.commentButton setTitle:@"" forState:UIControlStateNormal];
    }
    
//    if (commentArray.count == 0) {
//        
//        cell.commentCountLabel.text = @"";
//        cell.firstCommentLabel.text = @"";
//        cell.secondCommentLabel.text= @"";
//        cell.thirdCommentLabel.text = @"";
//        cell.firstCommentViewHeight.constant = 0;
//        cell.secondCommentViewHeight.constant = 0;
//        cell.thirdCommentViewHeight.constant = 0;
//        
//        
//    }else{
//        
//        if (commentArray.count>=3) {
//            
//            
//            cell.commentCountLabel.text = [NSString stringWithFormat:@"%lu comments",(unsigned long)commentArray.count];
//            
//            cell.firstCommentLabel.attributedText = [self getComentsTextConfigured:[[[commentArray objectAtIndex:commentArray.count - 3]objectForKey:@"user"] objectForKey:@"username"] toComment:[[commentArray objectAtIndex:commentArray.count - 3]objectForKey:@"comment"]] ;
//            cell.secondCommentLabel.attributedText= [self getComentsTextConfigured:[[[commentArray objectAtIndex:commentArray.count - 2]objectForKey:@"user"] objectForKey:@"username"] toComment:[[commentArray objectAtIndex:commentArray.count - 2]objectForKey:@"comment"]];
//            cell.thirdCommentLabel.attributedText = [self getComentsTextConfigured:[[[commentArray objectAtIndex:commentArray.count - 1]objectForKey:@"user"] objectForKey:@"username"] toComment:[[commentArray objectAtIndex:commentArray.count - 1]objectForKey:@"comment"]];
//            
//            
//            
//        }else if (commentArray.count==2){
//            
//            
//            cell.thirdCommentViewHeight.constant = 0;
//            
//            cell.commentCountLabel.text = @"";
//            cell.firstCommentLabel.attributedText = [self getComentsTextConfigured:[[[commentArray objectAtIndex:0]objectForKey:@"user"] objectForKey:@"username"] toComment:[[commentArray objectAtIndex:0]objectForKey:@"comment"]] ;
//            cell.secondCommentLabel.attributedText= [self getComentsTextConfigured:[[[commentArray objectAtIndex:1]objectForKey:@"user"] objectForKey:@"username"] toComment:[[commentArray objectAtIndex:1]objectForKey:@"comment"]];;
//            cell.thirdCommentLabel.text = @"";
//            
//            
//        }else if (commentArray.count==1){
//            
//            cell.secondCommentViewHeight.constant = 0;
//            cell.thirdCommentViewHeight.constant = 0;
//            
//            cell.commentCountLabel.text = @"";
//            cell.firstCommentLabel.attributedText = [self getComentsTextConfigured:[[[commentArray objectAtIndex:0]objectForKey:@"user"] objectForKey:@"username"] toComment:[[commentArray objectAtIndex:0]objectForKey:@"comment"]] ;
//            cell.secondCommentLabel.text= @"";
//            cell.thirdCommentLabel.text = @"";
//            
//        }
//        
//        
//    }
//
//    
//    //NSLog(@"comment array %@",commentArray);
//    
//    //make comment label clickable
//    self.tapFirstcommentLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnCommentLabel:)];
//    self.tapFirstcommentLabel.numberOfTapsRequired = 1;
//    [cell.firstCommentLabel addGestureRecognizer:self.tapFirstcommentLabel];
//    cell.firstCommentLabel.userInteractionEnabled = YES;
//    
//    self.tapSecondcommentLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnCommentLabel:)];
//    self.tapSecondcommentLabel.numberOfTapsRequired = 1;
//    [cell.secondCommentLabel addGestureRecognizer:self.tapSecondcommentLabel];
//    cell.secondCommentLabel.userInteractionEnabled = YES;
//    
//    self.tapThirdcommentLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnCommentLabel:)];
//    self.tapThirdcommentLabel.numberOfTapsRequired = 1;
//    [cell.thirdCommentLabel addGestureRecognizer:self.tapThirdcommentLabel];
//    cell.thirdCommentLabel.userInteractionEnabled = YES;
//    
//    self.tapcommentCountLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnCommentLabel:)];
//    self.tapcommentCountLabel.numberOfTapsRequired = 1;
//    [cell.commentCountLabel addGestureRecognizer:self.tapcommentCountLabel];
//    cell.commentCountLabel.userInteractionEnabled = YES;
//    
//    //set comment label  tag
//    cell.firstCommentLabel.tag=indexPath.section;
//    cell.secondCommentLabel.tag=indexPath.section;
//    cell.thirdCommentLabel.tag=indexPath.section;
//    cell.commentCountLabel.tag=indexPath.section;
//
//
//    
//    PatternTapResponder hashTagTapAction = ^(NSString *tappedString){
//        NSLog(@"You have tapped hashTag: %@",tappedString);
//        [self moveToHashTagExploreView:tappedString];
//    };
//    
//    [cell.hashTagLabel enableHashTagDetectionWithAttributes:@{NSForegroundColorAttributeName:[UIColor blueColor],
//                                                              RLHighlightedBackgroundColorAttributeName:[UIColor clearColor],NSBackgroundColorAttributeName:[UIColor clearColor],RLHighlightedBackgroundCornerRadius:@5,
//                                                              RLTapResponderAttributeName:hashTagTapAction}];
//
//    
//    
//    PatternTapResponder userTapAction = ^(NSString *tappedString){
//        NSLog(@"You have tapped user: %@",tappedString);
//        [self moveToUserView:tappedString];
//    };
//    
//    [cell.hashTagLabel enableUserHandleDetectionWithAttributes:@{NSForegroundColorAttributeName:[UIColor blueColor],
//                                                                 RLHighlightedBackgroundColorAttributeName:[UIColor clearColor],NSBackgroundColorAttributeName:[UIColor clearColor],RLHighlightedBackgroundCornerRadius:@5,
//                                                                 RLTapResponderAttributeName:userTapAction}];
    
    
    PatternTapResponder hashTagTapAction = ^(NSString *tappedString){
        NSLog(@"You have tapped hashTag: %@",tappedString);
        [self moveToHashTagExploreView:tappedString];
    };
    PatternTapResponder userTapAction = ^(NSString *tappedString){
        NSLog(@"You have tapped user: %@",tappedString);
        [self moveToUserView:tappedString];
    };
    
    [cell.descriptionLabel enableHashTagDetectionWithAttributes:@{NSForegroundColorAttributeName:[UIColor blueColor],
                                                                  RLHighlightedBackgroundColorAttributeName:[UIColor clearColor],NSBackgroundColorAttributeName:[UIColor clearColor],RLHighlightedBackgroundCornerRadius:@5,
                                                                  RLTapResponderAttributeName:hashTagTapAction}];
    
    [cell.descriptionLabel enableUserHandleDetectionWithAttributes:@{NSForegroundColorAttributeName:[UIColor blueColor],
                                                                     RLHighlightedBackgroundColorAttributeName:[UIColor clearColor],NSBackgroundColorAttributeName:[UIColor clearColor],RLHighlightedBackgroundCornerRadius:@5,
                                                                     RLTapResponderAttributeName:userTapAction}];
    
    
//    //setting date label
    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    NSDateFormatter *serverDateFormatter = [[NSDateFormatter alloc] init];
    
//    NSString* formatString = [[self.discoverPostArray objectAtIndex:indexPath.section] objectForKey:@"created"];
//    
////    [serverDateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ssz"];
////    
////    NSDate *postDate = [serverDateFormatter dateFromString:formatString];
////    
////    [dateFormatter setDateFormat:@"EEE MMM d yyyy, HH:mm"];
////    
//    cell.dateLabel.text =[self relativeDateStringForDate:[self getDateFromString:formatString]];
//    
//    
//    
////    //make likecount label clickable
//    self.taplikeLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnLikeLabel:)];
//    self.taplikeLabel.numberOfTapsRequired = 1;
//    [cell.likeCountLabel addGestureRecognizer:self.taplikeLabel];
//    cell.likeCountLabel.userInteractionEnabled = YES;
    
    self.taplikeLabel = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnLikeLabel:)];
    //    self.taplikeLabel.minimumPressDuration = 0.1;
    [cell.likeButton addGestureRecognizer:self.taplikeLabel];
//    
//
////    //set like count label tag
//    cell.likeCountLabel.tag=indexPath.section;
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 58;
    
}


#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}

-(void) changeConstraintForNoLike:(FeedTableViewCell*)cell {
    
    cell.likeCountLabel.text = @"";
    cell.likeIconHeight.constant = 0;
    cell.likeIconWidth.constant = 0;
    cell.likeIconTop.constant = 0;
    cell.constraintBtLikeIconAndDesView.constant = 0;
    
    cell.commentCountLeading.constant = -5;
    
}


-(IBAction)moveToHashTagExploreView:(NSString*)hashTag {
    
    NSMutableDictionary *hashTagTempDic=[[NSMutableDictionary alloc] init];
    [hashTagTempDic setObject:@"0" forKey:@"tagId"];
    [hashTagTempDic setObject:hashTag forKey:@"tagName"];
    
    
    HashTagViewViewController * htController = [self.storyboard instantiateViewControllerWithIdentifier:@"HashTagViewViewController"];
    
    htController.hashTagInfoDic = hashTagTempDic;
    [self.navigationController pushViewController:htController animated:YES];
    
}

//likecount label click action

-(void)tapOnLikeLabel:(UILongPressGestureRecognizer *)recognizer{
    
    if (recognizer.state == UIGestureRecognizerStateChanged)
    {
        NSLog(@"Change");
    }
    else if (recognizer.state == UIGestureRecognizerStateEnded) {
        NSLog(@"Long press Ended");
        
       

    } else if (recognizer.state == UIGestureRecognizerStateBegan) {
        NSLog(@"Long press detected.");
        LikersViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"LikersViewController"];
        controller.postId =[[[self.discoverPostArray objectAtIndex:recognizer.view.tag ] objectForKey:@"id"]intValue];
        
        [self.navigationController pushViewController:controller animated:YES];
    }
    
    
    
}

//comment label click action

-(void)tapOnCommentLabel:(UITapGestureRecognizer *)recognizer{
    
    
    CommentViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentViewController"];
    
    controller.postId = [[[self.discoverPostArray objectAtIndex:recognizer.view.tag ] objectForKey:@"id"]intValue];
    
    controller.postBelongsToUserId =[[[[self.discoverPostArray objectAtIndex:recognizer.view.tag ] objectForKey:@"owner"]objectForKey:@"id" ] intValue];
    
    
    
    
    [self.navigationController pushViewController:controller animated:YES];
    
 
}

//cell header click action

-(void)tapHeader:(UITapGestureRecognizer *)recognizer{
    
    NSLog(@"tap on header");
    
    ProfileViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    
    controller.isUserOrFollower = YES;
    controller.userId = [[[[self.discoverPostArray objectAtIndex:recognizer.view.tag ] objectForKey:@"owner"]objectForKey:@"id"]intValue];
    
    [self.navigationController pushViewController:controller animated:YES];
    
}


- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


-(void)tapOnPostImage:(UITapGestureRecognizer *)recognizer
{
    if(recognizer.view.subviews.count)
    {
        for (UIView *v in recognizer.view.subviews) {
            [v removeFromSuperview];
        }
    }
    else
        [self setupTaggedUserView:[[self.discoverPostArray objectAtIndex:recognizer.view.tag] objectForKey:@"tagged_users"] inImageView:recognizer.view];
}

-(void)doubleTapOnPostImage:(UITapGestureRecognizer *)recognizer
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:recognizer.view.tag];
    
    FeedTableViewCell *cell = (FeedTableViewCell*) [self.exploreTableView cellForRowAtIndexPath:indexPath];
    
    [self performSelector:@selector(likeButtonAction:) withObject:cell.likeButton afterDelay:0];
    
}


-(void) setupTaggedUserView:(NSMutableArray*) taggedUsers inImageView:(UIView*)targetedImage
{
    
    
    for (int i=0; i<taggedUsers.count; i++) {
        
        NSLog(@"targetedImage %ld",(long)targetedImage.tag);
        
        UIFont *font = [UIFont fontWithName:@"AzoSans-Regular" size:12];
        NSDictionary *userAttributes = @{NSFontAttributeName: font,
                                         NSForegroundColorAttributeName: [UIColor blackColor]};
        
        NSString *text =[[[taggedUsers objectAtIndex:i] objectForKey:@"user"] objectForKey:@"username"];
        const CGSize textSize = [text sizeWithAttributes: userAttributes];
        
        //NSLog(@"width = %f, height = %f", textSize.width, textSize.height);
        //
        float xposition=[[[taggedUsers objectAtIndex:i] objectForKey:@"x"] floatValue];
        float yposition=[[[taggedUsers objectAtIndex:i] objectForKey:@"y"]floatValue ];
        
        NSLog(@"relative X position:%f,Y position %f",xposition,yposition);
        
        
        UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(xposition*targetedImage.frame.size.width - textSize.width/2, yposition*targetedImage.frame.size.height, textSize.width +10, 30)];
        fromLabel.text = text;
        fromLabel.font = [UIFont fontWithName:@"AzoSans-Regular" size:12];
        fromLabel.numberOfLines = 1;
        fromLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines; // or UIBaselineAdjustmentAlignCenters, or UIBaselineAdjustmentNone
        fromLabel.adjustsFontSizeToFitWidth = YES;
        fromLabel.adjustsLetterSpacingToFitWidth = YES;
        fromLabel.minimumScaleFactor = 10.0f/12.0f;
        fromLabel.clipsToBounds = YES;
        fromLabel.layer.cornerRadius=3.0;
        fromLabel.backgroundColor = [UIColor blackColor];
        fromLabel.textColor = [UIColor whiteColor];
        fromLabel.textAlignment = NSTextAlignmentCenter;
        fromLabel.userInteractionEnabled=YES;
        fromLabel.tag=[[[taggedUsers objectAtIndex:i] objectForKey:@"user_id"] integerValue];
        
        
        //        UIImageView *arrowIcon = [[UIImageView alloc]initWithFrame:CGRectMake(textSize.width+10 -5, yPosition +10, 10, 10)];
        //        arrowIcon.image = [UIImage imageNamed:@"Arrow"];
        //        [fromLabel addSubview:arrowIcon];
        //
        [targetedImage addSubview:fromLabel];
        
        
        UITapGestureRecognizer* labelTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnTaggedLabel:)];
        labelTapGesture.numberOfTapsRequired = 1;
        [fromLabel addGestureRecognizer:labelTapGesture];
    }
    
    //get text size
    
}
-(void)tapOnTaggedLabel:(UITapGestureRecognizer *)recognizer{
    
    NSLog(@"tap on label");
    
    ProfileViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    
    controller.isUserOrFollower = YES;
    controller.userId = (int)recognizer.view.tag;
    
    [self.navigationController pushViewController:controller animated:YES];
    
}

- (IBAction)likeButtonAction:(UIButton *)sender {
    
    NSLog(@"like button clicked");
    
    sender.userInteractionEnabled = NO;
    sender.selected=!sender.selected;
    
    NSMutableDictionary *temDic = [[NSMutableDictionary alloc]initWithDictionary:[self.discoverPostArray objectAtIndex:sender.tag]];
    [temDic removeObjectForKey:@"is_liked"];
    [temDic setObject:[NSNumber numberWithBool:sender.selected] forKey:@"is_liked"];
    int counter;
    if(sender.selected)
        counter=[[temDic objectForKey:@"like_count"] intValue]+1;
    else
        counter=[[temDic objectForKey:@"like_count"] intValue]-1;
    
    [temDic removeObjectForKey:@"like_count"];
    [temDic setObject:[NSNumber numberWithInt:counter] forKey:@"like_count"];
    
    
    [self.discoverPostArray replaceObjectAtIndex:sender.tag withObject:temDic];
    [self.exploreTableView reloadData];
    
    if(!sender.selected)
    {
        
        [[ServerManager sharedManager] deleteLikeForPostId:[[[self.discoverPostArray objectAtIndex:sender.tag] objectForKey:@"id"] intValue] withcompletion:^(BOOL success) {
            
            sender.userInteractionEnabled = YES;
            
            if (success) {
                
                
            }
            else{
                sender.selected=!sender.selected;
                
                NSMutableDictionary *temDic = [[NSMutableDictionary alloc]initWithDictionary:[self.discoverPostArray objectAtIndex:sender.tag]];
                [temDic removeObjectForKey:@"is_liked"];
                [temDic setObject:[NSNumber numberWithBool:sender.selected] forKey:@"is_liked"];
                
                int counter;
                if(sender.selected)
                    counter=[[temDic objectForKey:@"like_count"] intValue]+1;
                else
                    counter=[[temDic objectForKey:@"like_count"] intValue]-1;
                
                [temDic removeObjectForKey:@"like_count"];
                [temDic setObject:[NSNumber numberWithInt:counter] forKey:@"like_count"];
                
                
                [self.discoverPostArray replaceObjectAtIndex:sender.tag withObject:temDic];
                [self.exploreTableView reloadData];
                
                
                
            }
            
        }];
    }else
    {
        
        [[ServerManager sharedManager] postLikePostId:[[[self.discoverPostArray objectAtIndex:sender.tag] objectForKey:@"id"] intValue] withcompletion:^(BOOL success) {
            
            sender.userInteractionEnabled = YES;
            
            if (success) {
                
                
            }
            else{
                
                sender.selected=!sender.selected;
                
                NSMutableDictionary *temDic = [[NSMutableDictionary alloc]initWithDictionary:[self.discoverPostArray objectAtIndex:sender.tag]];
                [temDic removeObjectForKey:@"is_liked"];
                [temDic setObject:[NSNumber numberWithBool:sender.selected] forKey:@"is_liked"];
                
                int counter;
                if(sender.selected)
                    counter=[[temDic objectForKey:@"like_count"] intValue]+1;
                else
                    counter=[[temDic objectForKey:@"like_count"] intValue]-1;
                
                [temDic removeObjectForKey:@"like_count"];
                [temDic setObject:[NSNumber numberWithInt:counter] forKey:@"like_count"];
                
                
                [self.discoverPostArray replaceObjectAtIndex:sender.tag withObject:temDic];
                [self.exploreTableView reloadData];
                
                
                
            }
            
        }];
    }

//    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
//    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
//    
//    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
//    [postData setObject:ACCESS_KEY forKey:@"access_key"];
//    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"user_id"];
//    [postData setObject:[NSNumber numberWithInt:[[[self.discoverPostArray objectAtIndex:sender.tag] objectForKey:@"id"]intValue]] forKey:@"post_id"] ;
//    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
//    
//    NSLog(@"postData %@",postData);
//    
//    [apiLoginManager POST:[NSString stringWithFormat:@"%@/posts/api-swap-like",SERVER_BASE_API_URL] parameters:postData
//                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                      
//                      NSLog(@"Response: %@", responseObject);
//                      
//                      sender.userInteractionEnabled = YES;
//                      
//                      NSString * successMsg = [responseObject objectForKey:@"success"];
//                      
//                      if ([successMsg integerValue] == 1 ) {
//                          
//                          NSLog(@"success msg %@",[responseObject objectForKey:@"success"]);
//                          
//                         
//                      }
//                      
//                      
//                  }
//                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                      
//                      NSLog(@"Error: %@", error);
//                      
//                      sender.userInteractionEnabled = YES;
//                      
//                    //  NSLog(@"%@",operation.responseString);
//                      
//                      
//                      sender.selected=!sender.selected;
//                      
//                      
//                      NSMutableDictionary *temDic = [[NSMutableDictionary alloc]initWithDictionary:[self.discoverPostArray objectAtIndex:sender.tag]];
//                      [temDic removeObjectForKey:@"is_liked"];
//                      [temDic setObject:[NSNumber numberWithBool:sender.selected] forKey:@"is_liked"];
//                      int counter;
//                      if(sender.selected)
//                          counter=[[temDic objectForKey:@"like_count"] intValue]+1;
//                      else
//                          counter=[[temDic objectForKey:@"like_count"] intValue]-1;
//                      
//                      [temDic removeObjectForKey:@"like_count"];
//                      [temDic setObject:[NSNumber numberWithInt:counter] forKey:@"like_count"];
//
//                      [self.discoverPostArray replaceObjectAtIndex:sender.tag withObject:temDic];
//                      
//                      [self.exploreTableView reloadData];
//                      
//                  }];

    
}
- (IBAction)commentButtonAction:(UIButton *)sender {

    
    CommentViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentViewController"];
    
    //controller.commentList = tempCommentArray;
    controller.postId = [[[self.discoverPostArray objectAtIndex:sender.tag ] objectForKey:@"id"]intValue];
    controller.postBelongsToUserId =[[[[self.discoverPostArray objectAtIndex:sender.tag ] objectForKey:@"owner"] objectForKey:@"id"] intValue];
    NSLog(@"comments in home %@",controller.commentList);
    
    
    [self.navigationController pushViewController:controller animated:YES];

    
    
    
}

- (IBAction)sendButtonAction:(UIButton *)sender {
    
    
}
- (IBAction)pinButtonAction:(UIButton *)sender {
    NSLog(@"Pin button clicked");
    
    sender.userInteractionEnabled = NO;
    sender.selected=!sender.selected;
    
    NSMutableDictionary *temDic = [[NSMutableDictionary alloc]initWithDictionary:[self.discoverPostArray objectAtIndex:sender.tag]];
    [temDic removeObjectForKey:@"is_pinned"];
    [temDic setObject:[NSNumber numberWithBool:sender.selected] forKey:@"is_pinned"];
    
    int pinCount=[[temDic objectForKey:@"pin_count"]intValue];
    
    if(sender.isSelected)
    {
        [temDic setObject:[NSNumber numberWithInt:pinCount+1] forKey:@"pin_count"];
        
        NSLog(@"slected %i",[[temDic objectForKey:@"pin_count"]intValue]);
        
    }else
    {
        
        [temDic setObject:[NSNumber numberWithInt:pinCount-1] forKey:@"pin_count"];
        NSLog(@"not slected %i",[[temDic objectForKey:@"pin_count"]intValue]);
        
    }

    
    
    [self.discoverPostArray replaceObjectAtIndex:sender.tag withObject:temDic];
    
    
    [self.exploreTableView reloadData];
    
    
    if(!sender.selected)
    {
        
        [[ServerManager sharedManager] deletePinForPostId:[[[self.discoverPostArray  objectAtIndex:sender.tag] objectForKey:@"id"] intValue] withcompletion:^(BOOL success) {
            
            sender.userInteractionEnabled = YES;
            
            if (success) {
                
                //  [self.feedTableView reloadData];
                
            }
            else{
                sender.userInteractionEnabled = YES;
                
                sender.selected=!sender.selected;
                
                NSMutableDictionary *temDic = [[NSMutableDictionary alloc]initWithDictionary:[self.discoverPostArray  objectAtIndex:sender.tag]];
                [temDic removeObjectForKey:@"is_pinned"];
                [temDic setObject:[NSNumber numberWithBool:sender.selected] forKey:@"is_pinned"];
                
                if(sender.selected)
                {
                    [temDic setObject:[NSNumber numberWithInt:[[temDic objectForKey:@"pin_count"]intValue]+1] forKey:@"pin_count"];
                    
                }else
                {
                    [temDic setObject:[NSNumber numberWithInt:[[temDic objectForKey:@"pin_count"]intValue]-1] forKey:@"pin_count"];
                    
                }
                
                
                
                [self.discoverPostArray  replaceObjectAtIndex:sender.tag withObject:temDic];
                [self.exploreTableView reloadData];
                
                
                
            }
            
        }];
    }else
    {
        
        [[ServerManager sharedManager] postPinForPostId:[[[self.discoverPostArray  objectAtIndex:sender.tag] objectForKey:@"id"] intValue] withcompletion:^(BOOL success) {
            
            sender.userInteractionEnabled = YES;
            
            if (success) {
                
                //[self.feedTableView reloadData];
                
            }
            else{
                sender.userInteractionEnabled = YES;
                
                sender.selected=!sender.selected;
                
                NSMutableDictionary *temDic = [[NSMutableDictionary alloc]initWithDictionary:[self.discoverPostArray  objectAtIndex:sender.tag]];
                [temDic removeObjectForKey:@"is_pinned"];
                [temDic setObject:[NSNumber numberWithBool:sender.selected] forKey:@"is_pinned"];
                
                if(sender.selected)
                {
                    [temDic setObject:[NSNumber numberWithInt:[[temDic objectForKey:@"pin_count"]intValue]+1] forKey:@"pin_count"];
                    
                }else
                {
                    [temDic setObject:[NSNumber numberWithInt:[[temDic objectForKey:@"pin_count"]intValue]-1] forKey:@"pin_count"];
                    
                }
                
                
                
                [self.discoverPostArray  replaceObjectAtIndex:sender.tag withObject:temDic];
                [self.exploreTableView reloadData];
                
                
                
            }
            
        }];
    }

    
//    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
//    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
//    
//    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
//    [postData setObject:ACCESS_KEY forKey:@"access_key"];
//    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"user_id"];
//    [postData setObject:[NSNumber numberWithInt:[[[self.discoverPostArray objectAtIndex:sender.tag] objectForKey:@"id"] intValue]] forKey:@"post_id"] ;
//    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
//    NSLog(@"postData %@",postData);
//    
//    [apiLoginManager POST:[NSString stringWithFormat:@"%@/posts/api-pin-post",SERVER_BASE_API_URL] parameters:postData
//                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                      
//                      NSLog(@"Response: %@", responseObject);
//                      
//                      sender.userInteractionEnabled = YES;
//                      
//                      NSString * successMsg = [responseObject objectForKey:@"success"];
//                      
//                      if ([successMsg integerValue] == 1 ) {
//                          
//                          NSLog(@"success msg %@",[responseObject objectForKey:@"success"]);
//                          
//                          
//                      }
//                      
//                  }
//                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                      
//                      NSLog(@"Error: %@", error);
//                      
//                      sender.userInteractionEnabled = YES;
//                      
//                      sender.selected=!sender.selected;
//                      
//                      NSMutableDictionary *temDic = [[NSMutableDictionary alloc]initWithDictionary:[self.discoverPostArray objectAtIndex:sender.tag]];
//                      [temDic removeObjectForKey:@"is_pinned"];
//                      [temDic setObject:[NSNumber numberWithBool:sender.selected] forKey:@"is_pinned"];
//                      
//                      if(sender.selected)
//                      {
//                          [temDic setObject:[NSNumber numberWithInt:[[temDic objectForKey:@"pin_count"]intValue]+1] forKey:@"pin_count"];
//                          
//                      }else
//                      {
//                          [temDic setObject:[NSNumber numberWithInt:[[temDic objectForKey:@"pin_count"]intValue]-1] forKey:@"pin_count"];
//                          
//                      }
//
//                      
//                      [self.discoverPostArray replaceObjectAtIndex:sender.tag withObject:temDic];
//                      [self.exploreTableView reloadData];
//                      
//                      //NSLog(@"%@",operation);
//                      
//                  }];
//    

    
}

- (IBAction)followButtonAction:(UIButton *)sender {
    
    sender.userInteractionEnabled = NO;
    
    
    [[ServerManager sharedManager] postFollowUserId:[[[[self.discoverPostArray objectAtIndex:sender.tag ] objectForKey:@"owner"] objectForKey:@"id"]intValue] WithCompletion:^(BOOL success) {
        
        sender.userInteractionEnabled = YES;
        
        if (success) {
            
            sender.selected=!sender.selected;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"loadAllFeed" object:nil];
        }
        else{
            
            sender.userInteractionEnabled = YES;
            
            
            
        }
        
    }];
    
    
}

-(IBAction)moveToUserView:(NSString*)userName {
    
  //  [self.activityIndicatorView startAnimating];
    
    
    
    [[ServerManager sharedManager] getUserByUserName:[userName substringFromIndex:1] withCompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
        
        
        if ( resultDataArray!=nil) {
            
            NSMutableDictionary *responsedic= [[resultDataArray dictionaryByReplacingNullsWithBlanks] mutableCopy];
            //NSLog(@"responseDic %@",responsedic);
            
            
            ProfileViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
            
            controller.isUserOrFollower = YES;
            controller.userId = [[responsedic objectForKey:@"id"] intValue];
            
            [self.navigationController pushViewController:controller animated:YES];
        }
        else{
            
            
        }
        
    }];
    
    
}


-(NSMutableAttributedString*) getComentsTextConfigured:(NSString*)user toComment:(NSString*)comment
{
    UIFont *commentFont = [UIFont fontWithName:@"AzoSans-Regular" size:12];
    UIFont *userFont = [UIFont fontWithName:@"AzoSans-Medium" size:13];
    
    NSDictionary *userDict = [NSDictionary dictionaryWithObject: userFont forKey:NSFontAttributeName];
    NSDictionary *commentDict = [NSDictionary dictionaryWithObject:commentFont forKey:NSFontAttributeName];
    
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:user attributes: userDict];
    
    NSMutableAttributedString *vAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",comment] attributes:commentDict];
    
    
    [aAttrString appendAttributedString:vAttrString];
    
    return aAttrString;
}


- (IBAction)buyButtonInPostAction:(UIButton*)sender {
    
    NSMutableDictionary *tempPost=[[NSMutableDictionary alloc] initWithDictionary:[self.discoverPostArray objectAtIndex:sender.tag]];
    
    
           NSString *alertMessage;
        
        if ([[tempPost objectForKey:@"price"] floatValue]) {
            
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
            [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
            // NSNumber *number = [formatter numberFromString:];
            
            
            NSString *formattedString = [formatter stringFromNumber:[NSNumber numberWithFloat:[[tempPost objectForKey:@"price"] floatValue]]];
            
            alertMessage=[NSString stringWithFormat:@"Hi, I am interested in this work, which is priced at US$%@. Please can you let me know if it is available and provide further details. Thank you.",formattedString];
            
        }
        else
            alertMessage=@"Hi, I am interested in this work. Please can you let me know if it is available and provide further details";
        
        ZHPopupView *popupView = [ZHPopupView popUpDialogViewInView:nil
                                                            iconImg:[UIImage imageNamed:@"correct_icon"]
                                                    backgroundStyle:ZHPopupViewBackgroundType_SimpleOpacity
                                                              title:@"Send Message"
                                                            content:alertMessage
                                                       buttonTitles:@[@"Cancel", @"Send"]
                                                confirmBtnTextColor:[UIColor blackColor] otherBtnTextColor:nil
                                                 buttonPressedBlock:^(NSInteger btnIdx) {
                                                     
                                                     if(btnIdx==1)
                                                     {
                                                         NSLog(@"sending message");
                                                         [self sendMessages:alertMessage forPost:tempPost];
                                                     }
                                                     
                                                     
                                                 }];
        [popupView present];
        
  
}


-(void) sendMessages: (NSString *) message forPost:(NSMutableDictionary*)post
{
    
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    [postData setObject:[NSNumber numberWithInt:[[post objectForKey:@"user_id"] intValue]] forKey:@"receiver_id"];
    [postData setObject:message forKey:@"message"];
    
    //for post share
    if([message isEqualToString:@"Attachment"])
    {
        [postData setObject:[NSNumber numberWithInt:[[post objectForKey:@"id"] intValue]] forKey:@"is_post"];
        [postData setObject:[post objectForKey:@"image"] forKey:@"url"];
        
        
    }
    else
    {
        [postData setObject:@"0" forKey:@"is_post"];
        [postData setObject:@"" forKey:@"url"];
    }
    
    
    [postData setObject:@"0" forKey:@"is_file"];
    

    
    [[ServerManager sharedManager] postSingleMessageWithData:postData withCompletion:^(BOOL success) {
        
        
        if (success) {
            if(![message isEqualToString:@"Attachment"])
                [self sendMessages:@"Attachment" forPost:post];
        }
        
        
    }];

}

- (IBAction)moreButtonAction:(UIButton *)sender {
    
    NSLog(@"%ld",(long)sender.tag);
    sharePostDictionary = [[NSMutableDictionary alloc]initWithDictionary:[self.discoverPostArray objectAtIndex:sender.tag]];
    
    NSLog(@"more button dic %@",sharePostDictionary);
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    
    FeedTableViewCell *cell = (FeedTableViewCell*) [self.exploreTableView cellForRowAtIndexPath:indexPath];
    imageViewForShare=cell.feedImage;
    

    if ([[[sharePostDictionary objectForKey:@"owner"]objectForKey:@"id"]integerValue] == [UserAccount sharedManager].userId) {
        
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                             destructiveButtonTitle:nil
                                                  otherButtonTitles:NSLocalizedString(@"Delete", nil),NSLocalizedString(@"Edit", nil),NSLocalizedString(@"Share", nil),NSLocalizedString(@"Share via Whatsapp", nil),NSLocalizedString(@"Send via message", nil), nil];
        
        
        sheet.tag = 1;
        
        [sheet showInView:self.view];
        
    }else{
    
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                             destructiveButtonTitle:nil
                                                  otherButtonTitles:NSLocalizedString(@"Report user", nil),NSLocalizedString(@"Share to Facebook", nil),NSLocalizedString(@"Share to Instagram", nil),NSLocalizedString(@"Share via Whatsapp", nil),NSLocalizedString(@"Send via message", nil), nil];
        
        
        sheet.tag = 2;
        
        [sheet showInView:self.view];

    }
  
    
   
    
    
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
//    UIImageView *postImage = [[UIImageView alloc]init];
//    
//    [postImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL,[sharePostDictionary objectForKey:@"image"]]]];
//
    
    
    if (actionSheet.tag == 1) {
        
        if (buttonIndex == 0) {
            
            
            [[ServerManager sharedManager] deletePostForPostId:[[sharePostDictionary objectForKey:@"id"] intValue] withcompletion:^(BOOL success) {
                
                if (success) {
                    
                    [self.discoverPostArray removeObject:sharePostDictionary];
                    
                    [self.exploreTableView reloadData];

                    
                }
                else{
                    [self.exploreTableView reloadData];
                    
                    
                    
                }
                
            }];
            
            
        }
        else if (buttonIndex == 1)
        {
            
            
            SharePhotoViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SharePhotoViewController"];
            controller.selectedImageForShare=imageViewForShare.image;
            controller.isForEdit=YES;
            controller.isForShare=NO;
            controller.postDetails=sharePostDictionary;
            //[self presentViewController:controller animated:NO completion:nil];
            
            [self.navigationController pushViewController:controller animated:YES];
            
            
        }
        else if (buttonIndex == 2)
        {
            
            SharePhotoViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SharePhotoViewController"];
            controller.selectedImageForShare=imageViewForShare.image;
            controller.isForShare=YES;
            controller.isForEdit=NO;
            controller.postDetails=sharePostDictionary;
            //[self presentViewController:controller animated:NO completion:nil];
            
            [self.navigationController pushViewController:controller animated:YES];
        }
        else if (buttonIndex == 3)
        {
            if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"whatsapp://app"]]){
                
                UIImage     * iconImage = imageViewForShare.image;
                NSString    * savePath  = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/whatsAppTmp.wai"];
                
                [UIImageJPEGRepresentation(iconImage, 1.0) writeToFile:savePath atomically:YES];
                
                self.documentController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:savePath]];
                self.documentController.UTI = @"net.whatsapp.image";
                self.documentController.delegate = self;
                
                [self.documentController presentOpenInMenuFromRect:CGRectMake(0, 0, 0, 0) inView:self.view animated: YES];
                
                
            } else {
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"WhatsApp not installed." message:@"Your device has no WhatsApp installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
        }
        else if (buttonIndex == 4)
        {
            
            MessagingViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"MessagingViewController"];
            controller.dataPayload=[[NSMutableDictionary alloc] initWithDictionary:sharePostDictionary];
            
            [UIPasteboard generalPasteboard].image=imageViewForShare.image;
            UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:controller];
            navCon.navigationBar.hidden=YES;
            
            [self presentViewController:navCon animated:YES completion:nil];
            
            
            
            
        }
        
    }
    else
    {
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        
        pasteboard.string = [NSString stringWithFormat:@"%@\n%@",[sharePostDictionary objectForKey:@"description"],[sharePostDictionary objectForKey:@"hashtags"]];
        
        
        if (buttonIndex == 0) {
            
            //[self performSelector:@selector(secondMethod) withObject:nil afterDelay:1.0 ];
            [[ServerManager sharedManager] postReportAgainstUserId:[[[sharePostDictionary objectForKey:@"owner"] objectForKey:@"id"] intValue] WithCompletion:^(BOOL success) {
                
                if (success) {
                    
                    //  [self.feedTableView reloadData];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                    message:@"Your report has been submitted."
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                    
                }
                else{
                    
                }
                
            }];
            
           
        }
//        else if (buttonIndex == 1)
//        {
//            //Block user
//        }
        
        else if (buttonIndex == 1)
        {
            FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
            photo.image = imageViewForShare.image;
            photo.userGenerated = YES;
            photo.caption = [NSString stringWithFormat:@"%@",[sharePostDictionary objectForKey:@"description"]];
            FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
            content.photos = @[photo];
            [FBSDKShareDialog showFromViewController:self
                                         withContent:content
                                            delegate:nil];
            
            NSLog(@"photo %@",photo);
            
            // NSLog(@"post dictionary  %@",sharePostDictionary);
            
        }else if (buttonIndex == 2)
        {
            
            NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
            if([[UIApplication sharedApplication] canOpenURL:instagramURL]) //check for App is install or not
            {
                NSData *imageData = UIImagePNGRepresentation(imageViewForShare.image); //convert image into .png format.
                NSFileManager *fileManager = [NSFileManager defaultManager];//create instance of NSFileManager
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //create an array and store result of our search for the documents directory in it
                NSString *documentsDirectory = [paths objectAtIndex:0]; //create NSString object, that holds our exact path to the documents directory
                NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"insta.igo"]]; //add our image to the path
                [fileManager createFileAtPath:fullPath contents:imageData attributes:nil]; //finally save the path (image)
                NSLog(@"image saved");
                
                CGRect rect = CGRectMake(0 ,0 , 0, 0);
                UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, self.view.opaque, 0.0);
                [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
                UIGraphicsEndImageContext();
                NSString *fileNameToSave = [NSString stringWithFormat:@"Documents/insta.igo"];
                NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:fileNameToSave];
                NSLog(@"jpg path %@",jpgPath);
                //            NSString *newJpgPath = [NSString stringWithFormat:@"file://%@",jpgPath];
                //            NSLog(@"with File path %@",newJpgPath);
                NSURL *igImageHookFile = [[NSURL alloc] initWithString:[[NSString alloc] initWithFormat:@"file://%@", jpgPath]];;
                NSLog(@"url Path %@",igImageHookFile);
                
                self.documentController.UTI = @"com.instagram.exclusivegram";
                // self.documentController.URL=[NSURL URLWithString:@"instagram://"];
                self.documentController = [self setupControllerWithURL:igImageHookFile usingDelegate:self];
                self.documentController=[UIDocumentInteractionController interactionControllerWithURL:igImageHookFile];
                NSString *caption = @"#Your Text"; //settext as Default Caption
                // self.documentController.annotation=[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",caption],@"InstagramCaption", nil];
                
                self.documentController.annotation=[NSDictionary dictionaryWithObject:caption forKey:@"InstagramCaption"];
                [self.documentController presentOpenInMenuFromRect:rect inView: self.view animated:YES];
                
                //[self.documentController presentOpenInMenuFromRect:CGRectMake(0, 0, 320, 480) inView:self.view animated:YES];
                
                //
                //            if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
                //                [[UIApplication sharedApplication] openURL:instagramURL];
                //            }
            }
            else
            {
                NSLog (@"Instagram not found");
            }
            
            
        }else if (buttonIndex == 3)
        {
            
            if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"whatsapp://app"]]){
                
                UIImage     * iconImage = imageViewForShare.image;
                NSString    * savePath  = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/whatsAppTmp.wai"];
                
                [UIImageJPEGRepresentation(iconImage, 1.0) writeToFile:savePath atomically:YES];
                
                self.documentController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:savePath]];
                self.documentController.UTI = @"net.whatsapp.image";
                self.documentController.delegate = self;
                
                [self.documentController presentOpenInMenuFromRect:CGRectMake(0, 0, 0, 0) inView:self.view animated: YES];
                
                
            } else {
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"WhatsApp not installed." message:@"Your device has no WhatsApp installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            
            
        }
        else if (buttonIndex == 4)
        {
            
            MessagingViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"MessagingViewController"];
            controller.dataPayload=[[NSMutableDictionary alloc] initWithDictionary:sharePostDictionary];
            
            [UIPasteboard generalPasteboard].image=imageViewForShare.image;
            UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:controller];
            navCon.navigationBar.hidden=YES;
            
            [self presentViewController:navCon animated:YES completion:nil];
            
            
            
            
        }

    }
    
}

//temporary



- (UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate {
    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL: fileURL];
    interactionController.delegate = interactionDelegate;
    return interactionController;
}


-(NSDate *)getDateFromString:(NSString *)dateString
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy'-'MM'-'dd' 'HH':'mm':'ss"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    
    //[formatter setDateFormat:@"EEE, MMM dd, yyyy"];
    NSDate *currentDate=[formatter dateFromString:dateString];
    return currentDate;
}

-(NSString *)getStringFromDate:(NSDate *)date
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd, YYYY"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    NSLog(@"%@", stringFromDate);
    return stringFromDate;
}


- (NSString *)relativeDateStringForDate:(NSDate *)date
{
    
    NSCalendarUnit units = NSCalendarUnitDay | NSCalendarUnitWeekOfYear |
    NSCalendarUnitMonth | NSCalendarUnitYear|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond;
    
    // if `date` is before "now" (i.e. in the past) then the components will be positive
    NSDateComponents *components = [[NSCalendar currentCalendar] components:units
                                                                   fromDate:date
                                                                     toDate:[NSDate date]
                                                                    options:0];
    
    //NSLog(@"components %@",components);
    if (components.year > 0) {
        
        return [self getStringFromDate:date];
        
        //        if (components.year > 1) {
        //            return [NSString stringWithFormat:@"%ld years ago", (long)components.year];
        //        } else {
        //            return [NSString stringWithFormat:@"%ld year ago", (long)components.year];
        //
        //        }
        
        
    } else if (components.month > 0) {
        
        if (components.month > 1) {
            return [self getStringFromDate:date];
            
            // return [NSString stringWithFormat:@"%ld months ago", (long)components.month];
        } else {
            return [NSString stringWithFormat:@"%ld month ago", (long)components.month];
            
        }
        
        
        
    } else if (components.weekOfYear > 0) {
        if (components.weekOfYear > 1) {
            return [NSString stringWithFormat:@"%ld weeks ago", (long)components.weekOfYear];
        } else {
            return [NSString stringWithFormat:@"%ld week ago", (long)components.weekOfYear];
        }
        
        
        
    } else if (components.day > 0) {
        if (components.day > 1) {
            return [NSString stringWithFormat:@"%ld days ago", (long)components.day];
        } else {
            return @"Yesterday";
        }
    } else {
        if (components.hour > 1) {
            return [NSString stringWithFormat:@"%ld hours ago", (long)components.hour];
        }
        else if(components.minute > 1){
            return [NSString stringWithFormat:@"%ld minutes ago", (long)components.minute];
        }
        else
        {
            // return [NSString stringWithFormat:@"less than a minutes"];
            return [NSString stringWithFormat:@"%ld seconds ago", (long)components.second];
        }
        
    }
}

//-(UIImage *)imageManager:(SDWebImageManager *)imageManager transformDownloadedImage:(UIImage *)image withURL:(NSURL *)imageURL
//{
//    // NSLog(@"imageURL : %@",imageURL);
//    UIImage *resizedImage;
//    //  return resizedImage  = [image resizedImage:CGSizeMake(512, 512) interpolationQuality:kCGInterpolationDefault];
//    
//    float MIN_UPLOAD_RESOLUTION=756*756;
//    float factor;
//    float resol = image.size.height*image.size.width;
//    if (resol >MIN_UPLOAD_RESOLUTION){
//        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
//        resizedImage = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
//        return resizedImage;
//        
//    }
//    else
//        return image;
//}
//
//- (UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
//    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
//    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
//    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return scaledImage;
//}


@end
