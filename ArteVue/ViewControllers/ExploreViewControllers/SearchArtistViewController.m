//
//  SearchPeopleViewController.m
//  Artegram
//
//  Created by Sujan on 7/20/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "SearchArtistViewController.h"
#import "Constants.h"

@interface SearchArtistViewController ()

@end

@implementation SearchArtistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTableView:) name:@"reloadTableWithArtistSearch" object:nil];
    
    self.searchPeopleTableView.delegate = self;
    self.searchPeopleTableView.dataSource = self;
    
    
    self.searchPeopleTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.searchPeopleTableView.frame.size.width, 1)];


    
}

-(void) viewDidAppear:(BOOL)animated
{

   NSLog(@"testing %@",self.testData);

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}


-(void)reloadTableView:(NSNotification*) notification
{
    self.searchResult=[[NSMutableArray alloc] init];
    self.searchResult=[[notification object] mutableCopy];
    NSLog(@"search result  %@",self.searchResult);
    
    
    if(self.searchResult.count)
    {
        [self.searchPeopleTableView setHidden:NO];
        [self.searchPeopleTableView reloadData];
        
    }else
    {
        [self.searchPeopleTableView setHidden:YES];
    }
}


#pragma mark - UITableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.searchResult.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
 
 
    static NSString *CellIdentifier = @"searchArtistCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    UILabel *profileName = (UILabel*) [cell viewWithTag:2];
    
    
    [self.view layoutIfNeeded];
    
    profileName.text = [self.searchResult objectAtIndex:indexPath.row];
    
    return cell;
}

#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableDictionary *peopleInfo = [[NSMutableDictionary alloc]init];
    
    [peopleInfo setObject:[self.searchResult objectAtIndex:indexPath.row] forKey:@"ArtistName"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pushArtistView" object:peopleInfo];
    
    
}


@end
