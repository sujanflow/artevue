//
//  DiscoverPeopleTableViewCell.h
//  Artegram
//
//  Created by Sujan on 7/17/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"

@interface DiscoverPeopleTableViewCell : UITableViewCell<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,SDWebImageManagerDelegate>


@property(nonatomic,strong) NSMutableArray *postImageList;

@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;

@property (weak, nonatomic) IBOutlet UILabel *prifileNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;

@property (weak, nonatomic) IBOutlet UIButton *followButton;

@property (weak, nonatomic) IBOutlet UICollectionView *discoverPeopleCollectionView;

@property (weak, nonatomic) IBOutlet UIView *privateAccountView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewBottomSpace;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *privateViewHeight;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *privateAccountBottomSpace;

@end
