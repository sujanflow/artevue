//
//  HashTagViewViewController.h
//  Artegrams
//
//  Created by Sujan on 8/4/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"

@interface ArtistPostViewController : UIViewController<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,SDWebImageManagerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *navigationLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *artistCollectionView;

@property (nonatomic,strong) NSString *artistName;
//@property (nonatomic,strong) NSString *artistId;

@property (weak, nonatomic) IBOutlet UILabel *noPostLabel;

@end
