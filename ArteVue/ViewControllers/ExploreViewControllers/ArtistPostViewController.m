//
//  HashTagViewViewController.m
//  Artegrams
//
//  Created by Sujan on 8/4/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "ArtistPostViewController.h"
#import "ExploreTableViewController.h"
#import "Constants.h"
#import "UserAccount.h"

#import "NSArray+NullReplacement.h"
#import "NSDictionary+NullReplacement.h"

#import "DGActivityIndicatorView.h"

#import <Realm/Realm.h>
#import "Posts.h"
#import "ServerManager.h"

@interface ArtistPostViewController ()
{
    NSInteger pageIndex;
   // BOOL islastItemReached;
    
    NSMutableArray *artistsTagPost;
    
    RLMRealmConfiguration *realmv1Configuration;
    NSMutableArray *downloadingIds;
}

@property (nonatomic) DGActivityIndicatorView *activityIndicatorView;


@end

@implementation ArtistPostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)DGActivityIndicatorAnimationTypeTriplePulse tintColor:[UIColor blackColor]];
    CGFloat width = self.view.bounds.size.width / 5.0f;
    CGFloat height = self.view.bounds.size.width / 5.0f;
    
    self.activityIndicatorView.frame = CGRectMake(self.view.bounds.size.width/2-width/2,self.view.bounds.size.height/2-height/2 - 40, width, height);
    [self.view addSubview:self.activityIndicatorView];
    
    NSURL *defaultRealmURL = [RLMRealmConfiguration defaultConfiguration].fileURL;
    NSURL *defaultRealmParentURL = [defaultRealmURL URLByDeletingLastPathComponent];
    NSURL *realmv1URL = [defaultRealmParentURL URLByAppendingPathComponent:@"defaultv1.realm"];
    
    realmv1Configuration = [[RLMRealmConfiguration defaultConfiguration] copy];
    realmv1Configuration.fileURL = realmv1URL;
    
    downloadingIds=[[NSMutableArray alloc] init];
    
    self.artistCollectionView.delegate = self;
    self.artistCollectionView.dataSource = self;
    
    artistsTagPost = [[NSMutableArray alloc]init];
    
    self.navigationLabel.text = self.artistName;
    
    [self.activityIndicatorView startAnimating];

    pageIndex=1;
    [self getArtistPost];
    
    //[SDWebImageManager sharedManager].delegate=self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.artistCollectionView reloadData];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    
    downloadingIds=[[NSMutableArray alloc] init];
    [[SDWebImageDownloader sharedDownloader] cancelAllDownloads];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(void) getArtistPost{
    
    [self.activityIndicatorView startAnimating];
    
    [[ServerManager sharedManager] getArtistPostByName:self.artistName withCompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
        
        [self.activityIndicatorView stopAnimating];
        
        if ( resultDataArray!=nil) {
           
            
            NSMutableArray *responsedic= [[NSMutableArray alloc] initWithArray:resultDataArray];
            artistsTagPost = [[NSMutableArray alloc] initWithArray:[responsedic arrayByReplacingNullsWithBlanks]];
            
            if(artistsTagPost.count)
            {
                
                self.noPostLabel.hidden=YES;
            }
            else
            {
                
                self.noPostLabel.hidden=NO;
            }

            
            [self.artistCollectionView reloadData];

            
        }
        else{
            
            self.view.userInteractionEnabled=YES;
       
        }
        
    }];

}




#pragma mark - CollectionView data source

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return  artistsTagPost.count;
    
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    static NSString *identifier = @"ArtistPhotoCell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    UIImageView *searchImage = (UIImageView*) [cell viewWithTag:1];
    UIActivityIndicatorView *activityInd = (UIActivityIndicatorView*) [cell viewWithTag:2];
    
    activityInd.hidden=NO;
    [activityInd startAnimating];
    
    [searchImage setShowActivityIndicatorView:YES];
    [searchImage setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    
   // [searchImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[artistsTagPost objectAtIndex:indexPath.row] objectForKey:@"image"]]]];
    
    RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
    //    NSLog(@"realmv1Configuration %@",realmv1Configuration.fileURL);
    
    RLMResults *result=[Posts objectsInRealm:realm where:[NSString stringWithFormat:@"postId = %@",[[artistsTagPost objectAtIndex:indexPath.row] objectForKey:@"id"]]];
    
    
    if(result.count)
    {
        Posts *post=[[Posts alloc] init];
        post=[result objectAtIndex:0];
        //NSLog(@"result %@",result);
        
       // [UIView transitionWithView:searchImage duration:0.2f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            [activityInd stopAnimating];
            searchImage.image=[UIImage imageWithData:post.imageData];
//        } completion:^(BOOL finished) {
//            ;
//        }];
        
        
    }
    else
    {
        searchImage.image=nil;
        //searchImage.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"square_image" ofType:@"png"]];
        
        if([downloadingIds containsObject: [[artistsTagPost objectAtIndex:indexPath.row] objectForKey:@"id"]])
        {
            //do nothing
        }
        else
        {
            
            [downloadingIds addObject:[[artistsTagPost objectAtIndex:indexPath.row] objectForKey:@"id"]];
            
            SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
            [SDWebImageDownloader sharedDownloader].maxConcurrentDownloads=1;
            [downloader downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[artistsTagPost objectAtIndex:indexPath.row] objectForKey:@"image"] ]]
                                     options:0
                                    progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                        // progression tracking code
                                        NSLog(@"receivedSize %ld expectedSize %ld",(long)receivedSize,(long)expectedSize);
                                        
                                    }
                                   completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                       if (image && finished) {
                                           // do something with image
                                           
                                           
                                           //                                           dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                                           //                                           dispatch_async(queue, ^{
                                           //                                               // Get new realm and table since we are in a new thread
                                           
                                           NSLog(@"done in explore collection %@",[[artistsTagPost objectAtIndex:indexPath.row] objectForKey:@"id"]);
                                           
                                           //RLMRealm *realm = [RLMRealm defaultRealm];
                                           
                                           
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               //searchImage.image=image;
                                               
                                               
                                               RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
                                               
                                               [realm beginWriteTransaction];
                                               //                                                   [Posts createOrUpdateInRealm:realm withValue:@{@"postId": [NSNumber numberWithInt:[[[discoverPost objectAtIndex:indexPath.row] objectForKey:@"id"] intValue]],
                                               //                                                                                          @"imageData": data}];
                                               //
                                               [Posts createOrUpdateInRealm:realm withValue:@{@"primId": [NSString stringWithFormat:@"%@",[[artistsTagPost objectAtIndex:indexPath.row] objectForKey:@"id"]]
                                                                                              ,@"postId": [NSNumber numberWithInt:[[[artistsTagPost objectAtIndex:indexPath.row] objectForKey:@"id"] intValue]],
                                                                                              @"imageData": data}];
                                               
                                               
                                               
                                               [realm commitWriteTransaction];
                                               
                                               [downloadingIds removeObject:[[artistsTagPost objectAtIndex:indexPath.row] objectForKey:@"id"]];
                                               // [self reloadData];
                                               
                                               [self.artistCollectionView reloadItemsAtIndexPaths:@[indexPath]];
                                               //      });
                                               
                                           });
                                       }
                                       
                                       
                                   }];
            
        }
        
    }

    
    return cell;
    
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    return CGSizeMake(([UIScreen mainScreen].bounds.size.width-2)/3, ([UIScreen mainScreen].bounds.size.width-2)/3);
    
    
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    ExploreTableViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ExploreTableViewController"];
    
    controller.indexNumber = indexPath.row;
    controller.discoverPostArray = artistsTagPost;
    controller.isComeFromTagView = YES;
    controller.hashTagName = self.artistName;
    
    [self.navigationController pushViewController:controller animated:YES];
    
    
}

//-(UIImage *)imageManager:(SDWebImageManager *)imageManager transformDownloadedImage:(UIImage *)image withURL:(NSURL *)imageURL
//{
//    // NSLog(@"imageURL : %@",imageURL);
//    UIImage *resizedImage;
//    //  return resizedImage  = [image resizedImage:CGSizeMake(512, 512) interpolationQuality:kCGInterpolationDefault];
//    
//    float MIN_UPLOAD_RESOLUTION=756*756;
//    float factor;
//    float resol = image.size.height*image.size.width;
//    if (resol >MIN_UPLOAD_RESOLUTION){
//        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
//        resizedImage = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
//        return resizedImage;
//        
//    }
//    else
//        return image;
//}
//
//- (UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
//    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
//    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
//    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return scaledImage;
//}

- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
