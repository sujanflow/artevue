//
//  LocationSearchResultViewController.m
//  Artegrams
//
//  Created by Sujan on 8/14/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "LocationSearchResultViewController.h"
#import "Constants.h"
#import "UserAccount.h"
#import "ExploreTableViewController.h"

#import "DGActivityIndicatorView.h"

@interface LocationSearchResultViewController (){
    
    NSMutableArray *locationPost;
}

@property (nonatomic) DGActivityIndicatorView *activityIndicatorView;

@end

@implementation LocationSearchResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"locationDictionary in result view %@",self.locationInfoDic);
    
    self.activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)DGActivityIndicatorAnimationTypeTriplePulse tintColor:[UIColor blackColor]];
    CGFloat width = self.view.bounds.size.width / 5.0f;
    CGFloat height = self.view.bounds.size.width / 5.0f;
    
    self.activityIndicatorView.frame = CGRectMake(self.view.bounds.size.width/2-width/2,self.view.bounds.size.height/2-height/2, width, height);
    [self.view addSubview:self.activityIndicatorView];
    
    
    
    

    
    self.locationCollectionView.delegate = self;
    self.locationCollectionView.dataSource = self;
    
    locationPost = [[NSMutableArray alloc]init];
    
    self.navigationLabel.text = [self.locationInfoDic objectForKey:@"place_name"];
    
    [self.activityIndicatorView startAnimating];
    
    
//    if([[self.locationInfoDic objectForKey:@"tagId"] isEqualToString:@"0"])
//    {
//        [self getLocationId];
//    }
//    else{
        [self makeRequest];
//    }
    
    
  //  [SDWebImageManager sharedManager].delegate=self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) makeRequest{
    
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    [postData setObject:ACCESS_KEY forKey:@"access_key"];
    [postData setObject:[self.locationInfoDic objectForKey:@"place_id"] forKey:@"google_place_id"];
    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
    
    NSLog(@"postData %@",postData);
    
    [apiLoginManager POST:[NSString stringWithFormat:@"%@/posts/api-get-posts-by-place-id",SERVER_BASE_API_URL] parameters:postData
                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                      
                      
                       NSLog(@"Response: %@", responseObject);
                      
                      locationPost = [responseObject mutableCopy];
                      
                      if(locationPost.count)
                      {
                          self.locationCollectionView.hidden = NO;
                          self.noPostLabel.hidden=YES;
                          
                          
                      }
                      else
                      {
                          self.locationCollectionView.hidden = YES;
                          
                          self.noPostLabel.hidden=NO;
                      }

                      
                      [self.locationCollectionView reloadData];
                      
                      [self.activityIndicatorView stopAnimating];
                  }
     
                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                      
                      NSLog(@"Error: %@", error);
                      
                      self.view.userInteractionEnabled=YES;
                      
                      NSLog(@"%@",operation.responseString);
                      
                      [self.activityIndicatorView stopAnimating];
                      
                  }];
    
    
    
}


#pragma mark - CollectionView data source

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return  locationPost.count;
    
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    static NSString *identifier = @"locationPhotoCell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    UIImageView *searchImage = (UIImageView*) [cell viewWithTag:1];
    //UIActivityIndicatorView *activityInd = (UIActivityIndicatorView*) [cell viewWithTag:2];
    
    //activityInd.hidden=NO;
    //[activityInd startAnimating];
    
    [searchImage setShowActivityIndicatorView:YES];
    [searchImage setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    
    [searchImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[locationPost objectAtIndex:indexPath.row] objectForKey:@"image"]]]];
    
    return cell;
    
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    return CGSizeMake(([UIScreen mainScreen].bounds.size.width-2)/3, ([UIScreen mainScreen].bounds.size.width-2)/3);
    
    
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    ExploreTableViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ExploreTableViewController"];
    
    controller.indexNumber = indexPath.row;
    controller.discoverPostArray = locationPost;
    controller.isComeFromLocationView = YES;
    controller.locationName = [self.locationInfoDic objectForKey:@"place_name"];
    
    [self.navigationController pushViewController:controller animated:YES];
    
    
}
- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
//
//-(UIImage *)imageManager:(SDWebImageManager *)imageManager transformDownloadedImage:(UIImage *)image withURL:(NSURL *)imageURL
//{
//    // NSLog(@"imageURL : %@",imageURL);
//    UIImage *resizedImage;
//    //  return resizedImage  = [image resizedImage:CGSizeMake(512, 512) interpolationQuality:kCGInterpolationDefault];
//    
//    float MIN_UPLOAD_RESOLUTION=756*756;
//    float factor;
//    float resol = image.size.height*image.size.width;
//    if (resol >MIN_UPLOAD_RESOLUTION){
//        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
//        resizedImage = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
//        return resizedImage;
//        
//    }
//    else
//        return image;
//}
//
//- (UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
//    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
//    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
//    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return scaledImage;
//}

@end
