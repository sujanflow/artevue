//
//  SearchPeopleViewController.h
//  Artegram
//
//  Created by Sujan on 7/20/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"

@interface SearchPeopleViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,SDWebImageManagerDelegate>


@property (weak, nonatomic) IBOutlet UITableView *searchPeopleTableView;

@property(nonatomic,strong) NSString *testData;

@property(nonatomic,strong) NSMutableArray *searchResult;

@end
