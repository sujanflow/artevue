//
//  SearchPeopleViewController.m
//  Artegram
//
//  Created by Sujan on 7/20/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "SearchPeopleViewController.h"
#import "Constants.h"

@interface SearchPeopleViewController ()

@end

@implementation SearchPeopleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTableView:) name:@"reloadTableWithPeopleSearch" object:nil];
    
    self.searchPeopleTableView.delegate = self;
    self.searchPeopleTableView.dataSource = self;
    
    
    self.searchPeopleTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.searchPeopleTableView.frame.size.width, 1)];


  //  [SDWebImageManager sharedManager].delegate=self;
}

-(void) viewDidAppear:(BOOL)animated
{

   NSLog(@"testing %@",self.testData);

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}


-(void)reloadTableView:(NSNotification*) notification
{
    self.searchResult=[[NSMutableArray alloc] init];
    self.searchResult=[[notification object] mutableCopy];
    NSLog(@"search result  %@",self.searchResult);
    
    
    if(self.searchResult.count)
    {
        [self.searchPeopleTableView setHidden:NO];
        [self.searchPeopleTableView reloadData];
        
    }else
    {
        [self.searchPeopleTableView setHidden:YES];
    }
}


#pragma mark - UITableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.searchResult.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
 
 
    static NSString *CellIdentifier = @"searchPeopleCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    UIImageView *profileImage = (UIImageView*) [cell viewWithTag:1];
    UILabel *profileName = (UILabel*) [cell viewWithTag:2];
    UILabel *userName = (UILabel*) [cell viewWithTag:3];
    
    [profileImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[self.searchResult objectAtIndex:indexPath.row] objectForKey:@"profile_picture"]]]];
    
    [self.view layoutIfNeeded];
    profileImage.layer.cornerRadius =  profileImage.bounds.size.width/2;
    //self.profilePicture.layer.borderWidth = 1.1f;
    profileImage.clipsToBounds = YES;
    profileImage.layer.masksToBounds = YES;
    
     profileName.text = [[self.searchResult objectAtIndex:indexPath.row] objectForKey:@"username"];
     userName.text = [[self.searchResult objectAtIndex:indexPath.row] objectForKey:@"first_name"];
    
    return cell;
}

#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableDictionary *peopleInfo = [[NSMutableDictionary alloc]init];
    
    [peopleInfo setObject:[[self.searchResult objectAtIndex:indexPath.row] objectForKey:@"username"] forKey:@"username"];
    [peopleInfo setObject:[[self.searchResult objectAtIndex:indexPath.row] objectForKey:@"id"] forKey:@"user_id"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"peopleProfileView" object:peopleInfo];
    
    
}
//
//-(UIImage *)imageManager:(SDWebImageManager *)imageManager transformDownloadedImage:(UIImage *)image withURL:(NSURL *)imageURL
//{
//    // NSLog(@"imageURL : %@",imageURL);
//    UIImage *resizedImage;
//    //  return resizedImage  = [image resizedImage:CGSizeMake(512, 512) interpolationQuality:kCGInterpolationDefault];
//    
//    float MIN_UPLOAD_RESOLUTION=756*756;
//    float factor;
//    float resol = image.size.height*image.size.width;
//    if (resol >MIN_UPLOAD_RESOLUTION){
//        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
//        resizedImage = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
//        return resizedImage;
//        
//    }
//    else
//        return image;
//}
//
//- (UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
//    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
//    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
//    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return scaledImage;
//}


@end
