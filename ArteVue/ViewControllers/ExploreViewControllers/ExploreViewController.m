//
//  ExploreViewController.m
//  Artegram
//
//  Created by Tanvir Palash on 6/10/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#define COLOR_LIGHT_RED         [UIColor colorWithRed:1 green:204.0/255 blue:204.0/255.0 alpha:1.0]
#define COLOR_LIGHT_GREEN       [UIColor colorWithRed:204.0/255 green:1 blue:204.0/255 alpha:1.0]
#define COLOR_LIGHT_BLUE        [UIColor colorWithRed:204.0/255 green:204.0/255 blue:1 alpha:1.0]
#define COLOR_LIGHT_YELLOW      [UIColor colorWithRed:1 green:1 blue:204.0/255 alpha:1.0]


#import "ExploreViewController.h"
#import "DiscoverPeopleViewController.h"
#import "ADPageControl.h"
#import "SearchPeopleViewController.h"
#import "SearchTagViewController.h"
#import "SearchArtistViewController.h"
#import "SearchLocationViewController.h"
#import "AdvancedSearchViewController.h"
#import "ExploreTableViewController.h"
#import "Constants.h"
#import "UserAccount.h"
#import "HashTagViewViewController.h"
#import "ProfileViewController.h"
#import "ArtistPostViewController.h"
#import "LocationSearchResultViewController.h"
#import "AdvancedSearchResultViewController.h"

#import "DGActivityIndicatorView.h"

#import "NSArray+NullReplacement.h"
#import "NSDictionary+NullReplacement.h"

#import <Realm/Realm.h>
#import "Posts.h"

#import "ServerManager.h"

@interface ExploreViewController ()<ADPageControlDelegate>
{
    ADPageControl   *_pageControl;
    
    ADPageModel *pageModel0;
    ADPageModel *pageModel1;
    ADPageModel *pageModel2;
    ADPageModel *pageModel3;
    
    NSMutableArray *discoverPost;
    int pageNumber;
    NSMutableArray *hashTagSearchResult;
    NSMutableArray *peopleSearchResult;
    NSMutableArray *artistSearchResult;

    RLMRealmConfiguration *realmv1Configuration;
    NSMutableArray *downloadingIds;
    
    int pageIndex;

}
@property (nonatomic) DGActivityIndicatorView *activityIndicatorView;


@end

@implementation ExploreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    pageIndex=1;
    self.activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)DGActivityIndicatorAnimationTypeTriplePulse tintColor:[UIColor blackColor]];
    CGFloat width = self.view.bounds.size.width / 5.0f;
    CGFloat height = self.view.bounds.size.width / 5.0f;
    
    //self.activityIndicatorView.backgroundColor=[UIColor redColor];
    self.activityIndicatorView.frame = CGRectMake(self.view.bounds.size.width/2-width/2,self.view.bounds.size.height/2-height/2, width, height);
    [self.view addSubview:self.activityIndicatorView];
    
    
    [[NSFileManager defaultManager] removeItemAtURL:[RLMRealmConfiguration defaultConfiguration].fileURL error:nil];
    
    NSURL *defaultRealmURL = [RLMRealmConfiguration defaultConfiguration].fileURL;
    NSURL *defaultRealmParentURL = [defaultRealmURL URLByDeletingLastPathComponent];
    NSURL *realmv1URL = [defaultRealmParentURL URLByAppendingPathComponent:@"defaultv1.realm"];
    
    realmv1Configuration = [[RLMRealmConfiguration defaultConfiguration] copy];
    realmv1Configuration.fileURL = realmv1URL;
    
    pageNumber =0;
    

    discoverPost = [[NSMutableArray alloc]init];
    downloadingIds=[[NSMutableArray alloc] init];
    
    //setting delegate

    
    self.exploreCollectionView.delegate = self;
    self.exploreCollectionView.dataSource = self;

    self.searchBar.delegate = self;
 
    
    //setting searchBar
    
    self.searchBar.placeholder = @"Search";
    [self.searchBar setTintColor:[UIColor whiteColor]];
    
    self.pagerView.hidden = YES;
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hashTagView:) name:@"pushHashTagView" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(profileView:) name:@"peopleProfileView" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(artistView:) name:@"pushArtistView" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationResultView:) name:@"pushLocationresultView" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AdvancedResultView:) name:@"pushAdvancedResultView" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openlocationView:) name:@"pushLocationFromAdvancedView" object:nil];
    
    
    
    self.searchQuery = [HNKGooglePlacesAutocompleteQuery sharedQuery];
    
//    self.exploreCollectionView.refreshControl=[[UIRefreshControl alloc] init];
//    self.exploreCollectionView.refreshControl.backgroundColor=[UIColor whiteColor];
//    self.exploreCollectionView.refreshControl.tintColor=[UIColor grayColor];
//    [self.exploreCollectionView.refreshControl addTarget:self action:@selector(makeRequest) forControlEvents:UIControlEventValueChanged];
    
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor=[UIColor whiteColor];
    self.refreshControl.tintColor=[UIColor grayColor];
    [self.refreshControl addTarget:self action:@selector(makeRequest) forControlEvents:UIControlEventValueChanged];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 10.0) {
        
        NSLog(@"below ios 10");
        self.exploreCollectionView.backgroundView = self.refreshControl;
        
        
    } else {
        
        NSLog(@"above ios 10");
        self.exploreCollectionView.refreshControl = self.refreshControl;
    }

    
    [self makeRequest];
    
   // [SDWebImageManager sharedManager].delegate=self;

}



-(void) viewDidAppear:(BOOL)animated
{

    if([[NSUserDefaults standardUserDefaults] boolForKey:@"followActionHappened"])
    {
        [self makeRequest];
    }
    //api call
   //  [[SDImageCache sharedImageCache] clearDisk];

    //[self makeRequest];''
    
   // [self.activityIndicatorView startAnimatingWithInteraction];
    [self reloadData];

}

-(void)viewWillDisappear:(BOOL)animated
{
    
    downloadingIds=[[NSMutableArray alloc] init];
    [[SDWebImageDownloader sharedDownloader] cancelAllDownloads];
    //[self.activityIndicatorView stopAnimating];
    
}

- (void)reloadData
{
    // Reload table data
    [self.exploreCollectionView reloadData];
    
    // End the refreshing
    if (self.refreshControl) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
        
        [self.refreshControl endRefreshing];
    }
   // [self.activityIndicatorView stopAnimating];
}

#pragma mark - UISearchBar Delegate

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self.activityIndicatorView stopAnimating];
    
    self.discoverButton.hidden = YES;
    self.discoverButtonView.hidden =YES;
    self.exploreCollectionView.hidden = YES;
    self.pagerView.hidden = NO;
    
    self.searchBarLeading.constant =40;
    self.backButton.hidden = NO;
    
    //[searchBar setShowsCancelButton:YES animated:YES];
    
    //set view pager
    
    [self setupPageControl];
    
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSLog(@"text did change");
    
    NSLog(@"page number %d",pageNumber);
    
//    if (searchText.length == 0) {
//        
//        NSLog(@"no search text");
//        
//       [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTableWithSearch" object:nil];
//    
//        
//    }else{
    
        if (pageNumber == 0) {
            
            if (searchText.length == 0) {
                
                        NSLog(@"no search text");
                
                       [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTableWithPeopleSearch" object:nil];
                    
                        
            }
            else
                [self searchPeople:pageNumber];
        }
        else if (pageNumber == 1) {
            if (searchText.length == 0) {
                
                NSLog(@"no search text");
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTableWithTagSearch" object:nil];
                
                
            }
            else
                [self searchTags:pageNumber];
            
        }else if (pageNumber == 2){
            
//            if (searchText.length == 0) {
//                
//                NSLog(@"no search text");
//                
//                [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTableWithLocationSearch" object:nil];
//                
//                
//            }
//            else
//            {
//                [self.searchQuery fetchPlacesForSearchQuery: searchText
//                                                 completion:^(NSArray *places, NSError *error) {
//                                                     if (error) {
//                                                         NSLog(@"ERROR: %@", error);
//                                                         [self handleSearchError:error];
//                                                     } else {
//                                                         
//                                                         self.searchResults = places;
//                                                         
//                                                     }
//                                                 }];
//                
//                [self searchLocations:pageNumber];
//            
//            }
            
            if (searchText.length == 0) {
                
                NSLog(@"no search text");
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTableWithArtistSearch" object:nil];
                
                
            }
            else
                [self searchArtist];
            
        }
   
        
    
    
    
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    self.discoverButton.hidden = NO;
     self.discoverButtonView.hidden =NO;
    self.exploreCollectionView.hidden = NO;
    self.pagerView.hidden = YES;
    
    searchBar.text = @"";
    self.searchBar.placeholder = @"Search";
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    
   
   
    
}

- (void)handleSearchError:(NSError *)error
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                   message:error.localizedDescription
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void) makeRequest{

    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"followActionHappened"];
    [self.activityIndicatorView startAnimatingWithInteraction];
    
    
    [[ServerManager sharedManager] getDiscoverPostForPage:pageIndex WithCompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
        
        if ( resultDataArray!=nil) {
            
            NSMutableDictionary *responsedic= [[resultDataArray dictionaryByReplacingNullsWithBlanks] mutableCopy];
            discoverPost=[[NSMutableArray alloc] initWithArray:[responsedic objectForKey:@"data"]];
            
           
             // [self.exploreCollectionView reloadData];
            [self reloadData];
            
            [self.activityIndicatorView stopAnimating];
            
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //[[[UIAlertView alloc] initWithTitle:@"Sorry, unable to fetch user data. Please try again." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                [self reloadData];
                
                [self.activityIndicatorView stopAnimating];
                
                
                
            });
            
        }
        
    }];
}

#pragma mark - CollectionView data source

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    
    if (discoverPost) {
        
        return 1;
        
    } else {
        
        // Display a message when the table is empty
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        
        messageLabel.text = @"There is no post currently available. Please pull down to refresh.";
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont fontWithName:@"Azosans-Regular" size:18];
        [messageLabel sizeToFit];
        self.exploreCollectionView.backgroundView = messageLabel;
        
    }
    
    return 0;
   // return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
        
        return  discoverPost.count;
        
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    static NSString *identifier = @"explorePhotoCell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    UIImageView *searchImage = (UIImageView*) [cell viewWithTag:1];
    UIActivityIndicatorView *activityInd = (UIActivityIndicatorView*) [cell viewWithTag:2];
    
    activityInd.hidden=NO;
    [activityInd startAnimating];
    
    [searchImage setShowActivityIndicatorView:YES];
    [searchImage setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    
    

//   [searchImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL,[[discoverPost objectAtIndex:indexPath.row] objectForKey:@"image"]]]];
//   [searchImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[discoverPost objectAtIndex:indexPath.row] objectForKey:@"image"]]]];

    RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
//    NSLog(@"realmv1Configuration %@",realmv1Configuration.fileURL);
    
    RLMResults *result=[Posts objectsInRealm:realm where:[NSString stringWithFormat:@"postId = %@",[[discoverPost objectAtIndex:indexPath.row] objectForKey:@"id"]]];
    
    
    if(result.count)
    {
        Posts *post=[[Posts alloc] init];
        post=[result objectAtIndex:0];
       // NSLog(@"result %@",result);
        [activityInd stopAnimating];

        searchImage.image=[UIImage imageWithData:post.imageData];
        
//        [UIView transitionWithView:searchImage duration:0.2f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
//            [activityInd stopAnimating];
//            
//             searchImage.image=[UIImage imageWithData:post.imageData];
//        } completion:^(BOOL finished) {
//            ;
//        }];
        
        
    }
    else
    {
        searchImage.image=nil;
        //searchImage.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"square_image" ofType:@"png"]];
        
        
        if([downloadingIds containsObject: [[discoverPost objectAtIndex:indexPath.row] objectForKey:@"id"]])
        {
            //do nothing
        }
        else
        {
            
            [downloadingIds addObject:[[discoverPost objectAtIndex:indexPath.row] objectForKey:@"id"]];
            
            SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
            [SDWebImageDownloader sharedDownloader].maxConcurrentDownloads=1;
            [downloader downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[discoverPost objectAtIndex:indexPath.row] objectForKey:@"image"] ]]
                                     options:0
                                    progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                        // progression tracking code
                                         NSLog(@"receivedSize %ld expectedSize %ld",(long)receivedSize,(long)expectedSize);
                                        
                                    }
                                   completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                       if (image && finished) {
                                           // do something with image
                                           
                                           
//                                           dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//                                           dispatch_async(queue, ^{
//                                               // Get new realm and table since we are in a new thread
                                           
                                               NSLog(@"done in explore collection %@",[[discoverPost objectAtIndex:indexPath.row] objectForKey:@"id"]);
                                               
                                               //RLMRealm *realm = [RLMRealm defaultRealm];
                                               
                                           
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                     //searchImage.image=image;
                                                   
                                                   
                                                   RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
                                                   
                                                   [realm beginWriteTransaction];
//                                                   [Posts createOrUpdateInRealm:realm withValue:@{@"postId": [NSNumber numberWithInt:[[[discoverPost objectAtIndex:indexPath.row] objectForKey:@"id"] intValue]],
//                                                                                          @"imageData": data}];
//                                                   
                                                   [Posts createOrUpdateInRealm:realm withValue:@{@"primId": [NSString stringWithFormat:@"%@",[[discoverPost objectAtIndex:indexPath.row] objectForKey:@"id"]]
                                                                                                  ,@"postId": [NSNumber numberWithInt:[[[discoverPost objectAtIndex:indexPath.row] objectForKey:@"id"] intValue]],
                                                                                                  @"imageData": data}];
                                                   
                                                   
                                                   
                                                   [realm commitWriteTransaction];
                                                   
                                                   [downloadingIds removeObject:[[discoverPost objectAtIndex:indexPath.row] objectForKey:@"id"]];
                                                  // [self reloadData];
                                                   
                                                   [self.exploreCollectionView reloadItemsAtIndexPaths:@[indexPath]];
                                         //      });
                                               
                                           });
                                       }
                                       
                                       
                                   }];
            
        }
        
    }
    
    
    return cell;
   
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(([UIScreen mainScreen].bounds.size.width-2)/3, ([UIScreen mainScreen].bounds.size.width-2)/3);
    
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
   
    ExploreTableViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ExploreTableViewController"];
    
    controller.indexNumber = indexPath.row;
    controller.discoverPostArray = [[NSMutableArray alloc] initWithArray:discoverPost];
   // controller.downloadingIds=[[NSMutableArray alloc] init];
    
    //controller.downloadingIds=[[NSMutableArray alloc] initWithArray:downloadingIds];
    
    [self.navigationController pushViewController:controller animated:YES];
    
    
}


-(void)setupPageControl
{
    
    /**** 1. Setup pages using model class "ADPageModel" ****/
    
    
    //page 0
    pageModel0 =       [[ADPageModel alloc] init];
    //UIViewController *page0 =       [UIViewController new];
    
   // SearchPeopleViewController *page0 =       [self.storyboard instantiateViewControllerWithIdentifier:@"SearchPeopleViewController"];
    
    pageModel0.strPageTitle =       @"People";
    pageModel0.iPageNumber =        0;
    pageModel0.bShouldLazyLoad = YES; //or
   // pageModel0.viewController =     page0;//You can provide view controller in prior OR use flag "bShouldLazyLoad" to load only when required
    
    //page 1
    pageModel1 =       [[ADPageModel alloc] init];
    
   // SearchTagViewController *page1 =       [self.storyboard instantiateViewControllerWithIdentifier:@"SearchTagViewController"];
    
    pageModel1.strPageTitle =       @"Tags";
    pageModel1.iPageNumber =        1;
    pageModel1.bShouldLazyLoad =    YES;
    //pageModel1.viewController =     page1;
    
    //page 2
    pageModel2 =       [[ADPageModel alloc] init];
    
   // SearchLocationViewController *page2 =       [self.storyboard instantiateViewControllerWithIdentifier:@"SearchLocationViewController"];

    pageModel2.strPageTitle =       @"Artist";
    pageModel2.iPageNumber =        2;
    pageModel2.bShouldLazyLoad =    YES;
    
   // pageModel2.viewController =     page2;
    
    //page 3
    pageModel3 =       [[ADPageModel alloc] init];
    
    //AdvancedSearchViewController *page3 =       [self.storyboard instantiateViewControllerWithIdentifier:@"AdvancedSearchViewController"];

    pageModel3.strPageTitle =       @"Advanced";
    pageModel3.iPageNumber =        3;
    pageModel3.bShouldLazyLoad =    YES;
   // pageModel3.viewController =     page3;
    
    
    
    /**** 2. Initialize page control ****/
    
    _pageControl = [[ADPageControl alloc] init];
    _pageControl.delegateADPageControl = self;
    _pageControl.arrPageModel = [[NSMutableArray alloc] initWithObjects:
                                 pageModel0,
                                 pageModel1,
                                 pageModel2,
                                 pageModel3, nil];
    
    
    
    /**** 3. Customize parameters (Optinal, as all have default value set) ****/
    NSLog(@"page number in begining %d",pageNumber);
    if (pageNumber == 3) {
        
        _pageControl.iFirstVisiblePageNumber =  0;
        
    }else{
        
      _pageControl.iFirstVisiblePageNumber =  pageNumber;
    }
    _pageControl.iTitleViewHeight =         40;
    _pageControl.iPageIndicatorHeight =     2;
    _pageControl.fontTitleTabText =         [UIFont fontWithName:@"AzoSans-Regular" size:13];
    
    _pageControl.bEnablePagesEndBounceEffect =  NO;
    _pageControl.bEnableTitlesEndBounceEffect = NO;
    
    _pageControl.colorTabText =                     [UIColor blackColor];
    _pageControl.colorTitleBarBackground =          [UIColor whiteColor];
    _pageControl.colorPageIndicator =               [UIColor blackColor];
    _pageControl.colorPageOverscrollBackground =    [UIColor whiteColor];
    
    _pageControl.bShowMoreTabAvailableIndicator =   NO;
    _pageControl.bHideShadow                    =   NO;
    
    
    
    //Uncomment to check custom fixed width tabs
    
    _pageControl.iCustomFixedTabWidth    =               [[UIScreen mainScreen] bounds].size.width/4;
    
    
    /**** 3. Add as subview ****/
    
    [self.pagerView addSubview:_pageControl.view];
    
    _pageControl.view.translatesAutoresizingMaskIntoConstraints = NO;
    UIView *subview = _pageControl.view;
    
    //Leading margin 0, Trailing margin 0
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[subview]-0-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(subview)]];
    
    //Top margin 20 for status bar, Bottom margin 0
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[subview]-0-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(subview)]];
    
    
}

#pragma mark - ADPageControlDelegate

//LAZY LOADING

-(UIViewController *)adPageControlGetViewControllerForPageModel:(ADPageModel *) pageModel
{
   // NSLog(@"ADPageControl :: Lazy load asking for page %d",pageModel.iPageNumber);
    
    
    
    if(pageModel.iPageNumber == 0)
    {
        //UIViewController *page1 =       [UIViewController new];

        SearchPeopleViewController *page0 =       [self.storyboard instantiateViewControllerWithIdentifier:@"SearchPeopleViewController"];
        
        page0.testData = @"test data...........";
        
    
        
        return page0;
    }
    else if(pageModel.iPageNumber == 1)
    {
       // UIViewController *page1 =       [UIViewController new];

        
        SearchTagViewController *page1 =       [self.storyboard instantiateViewControllerWithIdentifier:@"SearchTagViewController"];
        
        page1.testDataTags = @"test data in tag view";
        
        
       // page1.searchResult = hashTagSearchResult;
        
        NSLog(@"page1 %@",page1);
        
        return page1;
    }
    else if(pageModel.iPageNumber == 2)
    {
       // UIViewController *page2 =       [UIViewController new];

//        SearchLocationViewController *page2 =       [self.storyboard instantiateViewControllerWithIdentifier:@"SearchLocationViewController"];
//        
//        page2.testDataLocation = @"test data in location view";
        
        SearchArtistViewController *page2 =       [self.storyboard instantiateViewControllerWithIdentifier:@"SearchArtistViewController"];
        
        page2.testData = @"test data in artist view";
        
        return page2;
    }
    else if(pageModel.iPageNumber == 3)
    {
        //UIViewController *page3 =       [UIViewController new];
        
        AdvancedSearchViewController *page3 =       [self.storyboard instantiateViewControllerWithIdentifier:@"AdvancedSearchViewController"];

        page3.testDataSearch = @"test data in advance search";

        return page3;
    }
    
    return nil;
}

//CURRENT PAGE INDEX

-(void)adPageControlCurrentVisiblePageIndex:(int) iCurrentVisiblePage
{
   
  

     
    
    NSLog(@"ADPageControl :: Current visible page index : %d",iCurrentVisiblePage);
    
    if (iCurrentVisiblePage == 0) {
        
       
        pageNumber = iCurrentVisiblePage;
        self.searchBar.placeholder = @"Search people";
        
        [self.searchBar becomeFirstResponder];
        
        if(self.searchBar.text.length)
            [self searchPeople:pageNumber];
        else
            [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTableWithPeopleSearch" object:nil];
        
        
        
    }else if (iCurrentVisiblePage == 1) {
        pageNumber = iCurrentVisiblePage;
        self.searchBar.placeholder = @"Search hashtags";
        
        [self.searchBar becomeFirstResponder];
        
        if (self.searchBar.text.length) {
            [self searchTags:pageNumber];
            
 
        }else
            [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTableWithTagSearch" object:nil];
        
        
    }else if (iCurrentVisiblePage == 2)
    {
//        pageNumber = iCurrentVisiblePage;
//        self.searchBar.placeholder = @"Search location";
//        
//        [self.searchBar becomeFirstResponder];
        
        pageNumber = iCurrentVisiblePage;
        self.searchBar.placeholder = @"Search by artist";
        
        [self.searchBar becomeFirstResponder];
        
        if(self.searchBar.text.length)
            [self searchArtist];
        else
            [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTableWithArtistSearch" object:nil];
        
        
        
    }
    else if (iCurrentVisiblePage == 3)
    {
        self.searchBarLeading.constant =0;
        
        pageNumber = iCurrentVisiblePage;
        self.searchBar.placeholder = @"Advanced search";
        
        self.searchBar.text = @"";
        [self.searchBar setShowsCancelButton:NO animated:YES];
        [self.searchBar resignFirstResponder];
        [self.view resignFirstResponder];
    }
}

-(void) searchPeople:(int)tag
{
    
    [[ServerManager sharedManager] searchUserByString:self.searchBar.text forPage:pageIndex withcompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
        
        if ( resultDataArray!=nil) {
            
             peopleSearchResult = [[NSMutableArray alloc]init];
             peopleSearchResult = [resultDataArray objectForKey:@"data"];
            
            
             [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTableWithPeopleSearch" object:peopleSearchResult];
            
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
            });
        }
    }];



   
//    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
//    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
//    
//    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
//    [postData setObject:ACCESS_KEY forKey:@"access_key"];
//    [postData setObject:self.searchBar.text forKey:@"search_string"];
//    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
//    NSLog(@"postData %@",postData);
//    
//    [apiLoginManager POST:[NSString stringWithFormat:@"%@/users/api-search-users",SERVER_BASE_API_URL] parameters:postData
//                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                      
//                      NSLog(@"Response: %@", responseObject);
//                      
//                      peopleSearchResult = [[NSMutableArray alloc]init];
//                      peopleSearchResult = [responseObject mutableCopy];
//                      
//                      
//                    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTableWithPeopleSearch" object:peopleSearchResult];
//                      
//                  }
//     
//                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                      
//                      NSLog(@"Error: %@", error);
//                      
//                      
//                      NSLog(@"%@",operation.responseString);
//                      
//                      
//                      
//                  }];
    

}


-(void) searchTags:(int)tag
{

    
    [[ServerManager sharedManager] searchHashtagByString:self.searchBar.text withcompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
        
        if ( resultDataArray!=nil) {
            
            hashTagSearchResult = [[NSMutableArray alloc]init];
            hashTagSearchResult = [resultDataArray objectForKey:@"data"];
            
           
             //SearchTagViewController* vc= (SearchTagViewController*)[self adPageControlGetViewControllerForPageModel:pageModel1];
           
           [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTableWithTagSearch" object:hashTagSearchResult];
            
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
            });
        }

        
        
    }];
    
     
     
//    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
//    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
//    
//    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
//    [postData setObject:ACCESS_KEY forKey:@"access_key"];
//    [postData setObject:self.searchBar.text forKey:@"hashtag"];
//    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
//    NSLog(@"postData %@",postData);
//    
//    [apiLoginManager POST:[NSString stringWithFormat:@"%@/hashtags/api-search-hashtags",SERVER_BASE_API_URL] parameters:postData
//                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
//
//                      NSLog(@"Response: %@", responseObject);
//                      
//                      hashTagSearchResult = [[NSMutableArray alloc]init];
//                      hashTagSearchResult = [responseObject mutableCopy];
//  
//                      
//                      //SearchTagViewController* vc= (SearchTagViewController*)[self adPageControlGetViewControllerForPageModel:pageModel1];
//                      
//                      [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTableWithTagSearch" object:hashTagSearchResult];
//
//                  }
//     
//                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                      
//                      NSLog(@"Error: %@", error);
//                    
//                      
//                      NSLog(@"%@",operation.responseString);
//                      
//                      
//                      
//                  }];
    


}

-(void) searchArtist
{
    
    [[ServerManager sharedManager] searchArtistByString:self.searchBar.text withcompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
        
        if ( resultDataArray!=nil) {
            
            artistSearchResult = [[NSMutableArray alloc]init];
            artistSearchResult = [[resultDataArray objectForKey:@"data"] valueForKey:@"name"];
            
            
            //SearchTagViewController* vc= (SearchTagViewController*)[self adPageControlGetViewControllerForPageModel:pageModel1];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTableWithArtistSearch" object:artistSearchResult];
            
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
            });
        }
        
        

        
    }];
    
//    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
//    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
//    
//    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
//    [postData setObject:ACCESS_KEY forKey:@"access_key"];
//    [postData setObject:self.searchBar.text forKey:@"search_string"];
//    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
//    NSLog(@"postData %@",postData);
//    
//    [apiLoginManager POST:[NSString stringWithFormat:@"%@/posts/api-search-artists",SERVER_BASE_API_URL] parameters:postData
//                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                      
//                      NSLog(@"Response: %@", responseObject);
//                      
//                      artistSearchResult = [[NSMutableArray alloc]init];
//                      artistSearchResult = [responseObject mutableCopy];
//                      
//                      
//                      [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTableWithArtistSearch" object:artistSearchResult];
//                      
//                  }
//     
//                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                      
//                      NSLog(@"Error: %@", error);
//                      
//                      
//                      NSLog(@"%@",operation.responseString);
//                      
//                      
//                      
//                  }];
    
    
    
    
}

-(void) searchLocations:(int)tag
{

    
     NSLog(@"location search result in explore view %@",self.searchResults);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTableWithLocationSearch" object:self.searchResults];



}

- (IBAction)discoverButtonAction:(id)sender {
    
    DiscoverPeopleViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"DiscoverPeopleViewController"];
    
    [self.navigationController pushViewController:controller animated:YES];
    
    
}

-(void)hashTagView:(NSNotification*) notification
{

    self.hashTagDictionary = [notification object];
    
    NSLog(@"hash tag id in explore %@",self.hashTagDictionary);
    
    HashTagViewViewController * htController = [self.storyboard instantiateViewControllerWithIdentifier:@"HashTagViewViewController"];
    
    htController.hashTagInfoDic = self.hashTagDictionary;
    
    [self.navigationController pushViewController:htController animated:YES];
    
}

-(void)profileView:(NSNotification*) notification
{

    self.userDictionary = [notification object];
    
    NSLog(@"hash tag id in explore %@",self.userDictionary);
    
    ProfileViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    
    controller.isUserOrFollower = YES;
    controller.userId = [[self.userDictionary objectForKey:@"user_id"]intValue];
    
    [self.navigationController pushViewController:controller animated:YES];


}

-(void) artistView: (NSNotification*) notification
{
    self.userDictionary = [notification object];
    
    ArtistPostViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ArtistPostViewController"];
    controller.artistName=[self.userDictionary objectForKey:@"ArtistName"];
    [self.navigationController pushViewController:controller animated:YES];

}

-(void) locationResultView:(NSNotification*) notification
{
    self.locationDictionary = [notification object];

    LocationSearchResultViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"LocationSearchResultViewController"];
    
    controller.locationInfoDic = self.locationDictionary;
    
    [self.navigationController pushViewController:controller animated:YES];


  

}
-(void) AdvancedResultView:(NSNotification*) notification
{
    self.advancedDictionary = [notification object];
    
    AdvancedSearchResultViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"AdvancedSearchResultViewController"];
    
    controller.searchInfoDic = self.advancedDictionary;
    
    [self.navigationController pushViewController:controller animated:YES];
    
    
    
}

- (IBAction)openlocationView:(id)sender {
    
    LocationsViewController *vc  = [self.storyboard instantiateViewControllerWithIdentifier:@"LocationsViewController"];
    vc.delegate=self;
    [self presentViewController:vc animated:YES completion:nil];
}

-(void)didFinishPickingLocation:(NSMutableDictionary *)location
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pushLocationToAdvancedView" object:location];
    
    NSLog(@"selectedLocation %@",location);
   // [self showLocationView];
    
}


- (IBAction)backButtonAction:(id)sender {
    
    
    NSLog(@"back button click");
    self.searchBarLeading.constant =0;
    self.backButton.hidden = YES;
    
    self.discoverButton.hidden = NO;
    self.discoverButtonView.hidden =NO;
    self.exploreCollectionView.hidden = NO;
    self.pagerView.hidden = YES;
    
    self.searchBar.text = @"";
    self.searchBar.placeholder = @"Search";
    [self.searchBar setShowsCancelButton:NO animated:YES];
    [self.searchBar resignFirstResponder];
    
    
    [self.view resignFirstResponder];
    [self.view endEditing:YES];
    
    if(!discoverPost.count)
    {
        [self.activityIndicatorView startAnimatingWithInteraction];
        
    }
    
    
}
-(void)dealloc{
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    NSLog(@"Memory Warning");
    //[[SDImageCache sharedImageCache] setValue:nil forKey:@"memCache"];

    // [[SDImageCache sharedImageCache] clearMemory];
}


//-(UIImage *)imageManager:(SDWebImageManager *)imageManager transformDownloadedImage:(UIImage *)image withURL:(NSURL *)imageURL
//{
//    // NSLog(@"imageURL : %@",imageURL);
//    UIImage *resizedImage;
//    //  return resizedImage  = [image resizedImage:CGSizeMake(512, 512) interpolationQuality:kCGInterpolationDefault];
//    
//    float MIN_UPLOAD_RESOLUTION=756*756;
//    float factor;
//    float resol = image.size.height*image.size.width;
//    if (resol >MIN_UPLOAD_RESOLUTION){
//        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
//        resizedImage = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
//        return resizedImage;
//        
//    }
//    else
//        return image;
//}
//
//- (UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
//    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
//    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
//    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return scaledImage;
//}

@end
