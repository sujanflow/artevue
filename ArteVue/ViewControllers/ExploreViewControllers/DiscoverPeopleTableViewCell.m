//
//  DiscoverPeopleTableViewCell.m
//  Artegram
//
//  Created by Sujan on 7/17/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "DiscoverPeopleTableViewCell.h"
#import "Constants.h"

@implementation DiscoverPeopleTableViewCell


- (void)awakeFromNib {
    [super awakeFromNib];
    
   
//    self.followButton.layer.borderWidth = 1.1f;
//    self.followButton.layer.borderColor = [[UIColor blackColor]CGColor];
//    
    [self layoutIfNeeded];
    self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width/2;
    
    self.profileImageView.clipsToBounds = YES;
    self.profileImageView.layer.masksToBounds = YES;
    
    self.discoverPeopleCollectionView.delegate = self;
    self.discoverPeopleCollectionView.dataSource = self;
    
    self.collectionViewHeight.constant= (([UIScreen mainScreen].bounds.size.width-2)/3);
    //self.privateViewHeight.constant= (([UIScreen mainScreen].bounds.size.width-18)/3);
    
    self.privateAccountView.hidden = YES;
    
 //   [SDWebImageManager sharedManager].delegate=self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    
}


#pragma mark - CollectionView data source

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return  self.postImageList.count;
    
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

 //   NSLog(@"post count %d for index %ld",self.postImageList.count,(long)indexPath.row);
    
 //   NSLog(@"post image %@",self.postImageList);
    
    static NSString *identifier = @"discoverPeopleImageCell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    UIImageView *postsImage = (UIImageView*) [cell viewWithTag:1];
    
    [postsImage setShowActivityIndicatorView:YES];
    [postsImage setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    [postsImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[self.postImageList objectAtIndex:indexPath.row] objectForKey:@"image"]]]];

    
    return cell;
    
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{

    return CGSizeMake(([UIScreen mainScreen].bounds.size.width-2)/3, ([UIScreen mainScreen].bounds.size.width-2)/3);
    
    
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
}

//-(UIImage *)imageManager:(SDWebImageManager *)imageManager transformDownloadedImage:(UIImage *)image withURL:(NSURL *)imageURL
//{
//    // NSLog(@"imageURL : %@",imageURL);
//    UIImage *resizedImage;
//    //  return resizedImage  = [image resizedImage:CGSizeMake(512, 512) interpolationQuality:kCGInterpolationDefault];
//    
//    float MIN_UPLOAD_RESOLUTION=756*756;
//    float factor;
//    float resol = image.size.height*image.size.width;
//    if (resol >MIN_UPLOAD_RESOLUTION){
//        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
//        resizedImage = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
//        return resizedImage;
//        
//    }
//    else
//        return image;
//}
//
//- (UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
//    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
//    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
//    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return scaledImage;
//}

@end
