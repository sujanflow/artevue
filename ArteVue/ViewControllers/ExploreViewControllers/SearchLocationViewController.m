//
//  SearchLocationViewController.m
//  Artegram
//
//  Created by Sujan on 7/20/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "SearchLocationViewController.h"

@interface SearchLocationViewController ()

@end

@implementation SearchLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTableView:) name:@"reloadTableWithLocationSearch" object:nil];
    
    self.searchLocationTableView.delegate = self;
    self.searchLocationTableView.dataSource = self;
    
    self.searchLocationTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.searchLocationTableView.frame.size.width, 1)];

    
}

-(void) viewDidAppear:(BOOL)animated
{
    
    NSLog(@"testing %@",self.testDataLocation);
    
}

-(void)dealloc{
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}


-(void)reloadTableView:(NSNotification*) notification
{
    self.searchResults=[[NSMutableArray alloc] init];
    self.searchResults=[[notification object] mutableCopy];
    NSLog(@"search result  %@",self.searchResults);
    
    

    if(self.searchResults.count)
    {
        [self.searchLocationTableView setHidden:NO];
        [self.searchLocationTableView reloadData];
        
    }else
    {
        [self.searchLocationTableView setHidden:YES];
    }


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.searchResults.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"searchLocationCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    HNKGooglePlacesAutocompletePlace *thisPlace = self.searchResults[indexPath.row];
    
    //UIImageView *locationIcon = (UIImageView*) [cell viewWithTag:1];
    UILabel *locationName = (UILabel*) [cell viewWithTag:2];
    UILabel *locationDetail = (UILabel*) [cell viewWithTag:3];
    
    locationName.text =@"";
    locationDetail.text = @"";
    
    for (int i =0; i<thisPlace.terms.count; i++) {
        
        HNKGooglePlacesAutocompletePlaceTerm *tempTerm=[HNKGooglePlacesAutocompletePlaceTerm new ];
        
        tempTerm=[thisPlace.terms objectAtIndex:i];
        
        NSLog(@"Search result %@ place Id:%@",tempTerm, thisPlace.placeId);
        
        if(i==0)
            locationName.text =[NSString stringWithFormat:@"%@",tempTerm.value];
        else if(i==1)
            locationDetail.text =[NSString stringWithFormat:@"%@",tempTerm.value];
        
        
        else if(i>1 && i>=thisPlace.terms.count-3)
        {
            if (locationDetail.text.length) {
                locationDetail.text =[NSString stringWithFormat:@"%@,%@", locationDetail.text,tempTerm.value];
            }else{
                locationDetail.text =[NSString stringWithFormat:@"%@",tempTerm.value];
            }
        }
        
    }
    
    return cell;
}

#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HNKGooglePlacesAutocompletePlace *thisPlace = self.searchResults[indexPath.row];
    
    NSMutableDictionary *locationInfo = [[NSMutableDictionary alloc]init];

    HNKGooglePlacesAutocompletePlaceTerm *tempTerm=[HNKGooglePlacesAutocompletePlaceTerm new ];
    
    tempTerm=[thisPlace.terms objectAtIndex:0];
    
    [locationInfo setObject:tempTerm.value forKey:@"place_name"];
    [locationInfo setObject:thisPlace.placeId forKey:@"place_id"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pushLocationresultView" object:locationInfo];
    
    NSLog(@"locationInfo %@",locationInfo);
}

@end
