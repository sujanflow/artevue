//
//  LikeListTableViewCell.m
//  Artegrams
//
//  Created by Tanvir Palash on 8/14/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "LikeListTableViewCell.h"

@implementation LikeListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self layoutIfNeeded];
    self.profilePic.layer.cornerRadius =  self.profilePic.bounds.size.width/2;
    //self.profilePicture.layer.borderWidth = 1.1f;
    self.profilePic.clipsToBounds = YES;
    self.profilePic.layer.masksToBounds = YES;
    // Initialization code
}
-(void) layoutSubviews
{
    //[super layoutSubviews];
 
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
