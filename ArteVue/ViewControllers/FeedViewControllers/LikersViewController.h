//
//  LikersViewController.h
//  Artegram
//
//  Created by Sujan on 7/21/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"

@interface LikersViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>


@property (weak, nonatomic) IBOutlet UITableView *likersTableView;

@property (weak, nonatomic) IBOutlet UILabel *navigationHeaderLabel;


@property int postId;
@property int userId;

@property BOOL isFollower;
@property BOOL isFolloweing;

@end
