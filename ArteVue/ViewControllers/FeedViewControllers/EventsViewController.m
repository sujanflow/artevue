//
//  EventsViewController.m
//  Artegrams
//
//  Created by Sujan on 12/7/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "EventsViewController.h"
#import "EventsTableViewCell.h"

#import "Constants.h"
#import "UserAccount.h"
#import "ServerManager.h"
#import "News.h"
#import "EventRealm.h"

#import <Realm/Realm.h>

#import "NSDate+SameDay.h"
#import "NSArray+NullReplacement.h"
#import "NSDictionary+NullReplacement.h"
#import "DGActivityIndicatorView.h"

@interface EventsViewController ()
{
    NSMutableArray *eventsList;
    
    RLMRealmConfiguration *realmv1Configuration;
    NSMutableArray *downloadingIds;
    
    int pageIndex;
}

@property (nonatomic) DGActivityIndicatorView *activityIndicatorView;
@property UIRefreshControl *refreshControl;

@end



@implementation EventsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    pageIndex=1;
    self.comingsoonLabel.hidden = YES;
    
    
    self.activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)DGActivityIndicatorAnimationTypeTriplePulse tintColor:[UIColor blackColor]];
    CGFloat width = self.view.bounds.size.width / 5.0f;
    CGFloat height = self.view.bounds.size.width / 5.0f;
    
    //self.activityIndicatorView.backgroundColor=[UIColor redColor];
    self.activityIndicatorView.frame = CGRectMake(self.view.bounds.size.width/2-width/2,self.view.bounds.size.height/2-height/2, width, height);
    [self.view addSubview:self.activityIndicatorView];
    
    eventsList=[[NSMutableArray alloc] init];
    downloadingIds=[[NSMutableArray alloc] init];
    
    self.eventsTableView.estimatedRowHeight = 400;
    self.eventsTableView.rowHeight = UITableViewAutomaticDimension;
    self.eventsTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.eventsTableView.frame.size.width, 1)];
    
    
    self.eventsTableView.delegate = self;
    self.eventsTableView.dataSource = self;
    
    
    [[NSFileManager defaultManager] removeItemAtURL:[RLMRealmConfiguration defaultConfiguration].fileURL error:nil];
    
    //  NSURL *v1URL = [[NSBundle mainBundle] URLForResource:@"defaultv1" withExtension:@"realm"];
    //  [[NSFileManager defaultManager] copyItemAtURL:v1URL toURL:realmv1URL error:nil];
    
    NSURL *defaultRealmURL = [RLMRealmConfiguration defaultConfiguration].fileURL;
    NSURL *defaultRealmParentURL = [defaultRealmURL URLByDeletingLastPathComponent];
    NSURL *realmv1URL = [defaultRealmParentURL URLByAppendingPathComponent:@"defaultv1.realm"];
    
    realmv1Configuration = [[RLMRealmConfiguration defaultConfiguration] copy];
    realmv1Configuration.fileURL = realmv1URL;
    
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor=[UIColor whiteColor];
    self.refreshControl.tintColor=[UIColor grayColor];
    [self.refreshControl addTarget:self action:@selector(loadEventsFeed) forControlEvents:UIControlEventValueChanged];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 10.0) {
        
        NSLog(@"below ios 10");
        self.eventsTableView.backgroundView = self.refreshControl;
        
        
    } else {
        
        NSLog(@"above ios 10");
        self.eventsTableView.refreshControl = self.refreshControl;
    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self loadEventsFeed];
}
-(void)viewWillDisappear:(BOOL)animated
{
    downloadingIds=[[NSMutableArray alloc] init];
    [[SDWebImageDownloader sharedDownloader] cancelAllDownloads];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadEventsFeed{
    
    //[[SDWebImageDownloader sharedDownloader] cancelAllDownloads];
    [self.activityIndicatorView startAnimating];
    
    [[ServerManager sharedManager] getEventListForPage:pageIndex WithCompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
      
        [self.activityIndicatorView stopAnimating];
        if ( resultDataArray!=nil) {
            
            NSMutableDictionary *responsedic= [[resultDataArray dictionaryByReplacingNullsWithBlanks] mutableCopy];
            eventsList=[[NSMutableArray alloc] initWithArray:[responsedic objectForKey:@"data"]];
            
            if(eventsList.count)
            {
                // [self configueUserPostModel:responseArray];
                //  [self.artNewsTableView reloadData];
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                    //Background Thread
                    
                    [self configueUserPostModel:eventsList];
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //Run UI Updates
                        [self reloadData];
                        
                    });
                });
                //
            }
            
            
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.activityIndicatorView stopAnimating];
                
                //[[[UIAlertView alloc] initWithTitle:@"Sorry, unable to fetch user data. Please try again." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                
                
            });

            
        }
        
    }];

    
}


-(void)configueUserPostModel:(NSMutableArray *)newsArray
{
    
    
    eventsList=[[NSMutableArray alloc] init];
    
    
    for (int i=0; i<newsArray.count; i++) {
        
        News *singleNews=[News new];
        singleNews.newsId=[[[newsArray objectAtIndex:i] objectForKey:@"id"] intValue];
        
        singleNews.headLine=[[newsArray objectAtIndex:i] objectForKey:@"headline"];
        singleNews.newsDescription=[[newsArray objectAtIndex:i] objectForKey:@"description"];
        singleNews.publishTime=[self getStringFromDate:[self getPublishDateFromString:[[newsArray objectAtIndex:i] objectForKey:@"publish_date"]]];;
        singleNews.newsUrl=[[newsArray objectAtIndex:i] objectForKey:@"url"];
        singleNews.newsImage=nil;
        singleNews.imageUrl=[[newsArray objectAtIndex:i] objectForKey:@"image"];
        singleNews.location=[[newsArray objectAtIndex:i] objectForKey:@"location"];
        
        
        NSDate *startDate, *endDate;
        startDate=[self getDateFromString:[[newsArray objectAtIndex:i] objectForKey:@"start_date"]];
        endDate=[self getDateFromString:[[newsArray objectAtIndex:i] objectForKey:@"end_date"]];
        if ([startDate isSameDayAsDate:endDate]) {
            // The two dates are on the same day...
            singleNews.eventDates=[NSString stringWithFormat:@"%@",[self getEventsFromDate:startDate]];
            
        }
        else
            singleNews.eventDates=[NSString stringWithFormat:@"%@ to %@",[self getEventsFromDate:startDate],[self getEventsFromDate:endDate]];
        
        RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
        RLMResults *result=[EventRealm objectsInRealm:realm where:[NSString stringWithFormat:@"eventId = %i",singleNews.newsId]];
        
        //NSLog(@"result %@",result);
        if(result.count)
        {
            EventRealm *newsData=[[EventRealm alloc] init];
            newsData=[result objectAtIndex:0];
            singleNews.newsImage=[UIImage imageWithData:newsData.imageData];
            
        }
        
        [eventsList addObject:singleNews];
    }
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return eventsList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    EventsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"eventCell"];
    
    News *singleEvent=[News new];
    singleEvent=[eventsList objectAtIndex:indexPath.row];
    NSLog(@"singleNews %@",singleEvent);
    
    cell.eventNameLabel.text=singleEvent.headLine;
    cell.eventPicture.image=singleEvent.newsImage;
    cell.descriptionLabel.text=singleEvent.newsDescription;
    cell.dateLabel.text=[singleEvent.publishTime uppercaseString];
    cell.placeAndDateLabel.text=[NSString stringWithFormat:@"%@: %@",singleEvent.location,singleEvent.eventDates];
    
    
    cell.facebookButton.tag=indexPath.row;
    cell.twetterButton.tag=indexPath.row;
    if(singleEvent.newsImage == Nil)
    {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            
            if(![downloadingIds containsObject: [NSNumber numberWithInt:singleEvent.newsId]])
            {
                [downloadingIds addObject:[NSNumber numberWithInt:singleEvent.newsId]];
                
                SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
                [SDWebImageDownloader sharedDownloader].maxConcurrentDownloads=1;
                
                [downloader downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,singleEvent.imageUrl ]]
                                         options:0
                                        progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                            // progression tracking code
                                            NSLog(@"receivedSize %ld expectedSize %ld",(long)receivedSize,(long)expectedSize);
                                            
                                        }
                                       completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                           if (image && finished) {
                                               // do something with image
                                               
                                               //   Posts *post = [[Posts alloc] init];
                                               
                                               RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
                                               
                                               
                                               [realm beginWriteTransaction];
                                               [EventRealm createOrUpdateInRealm:realm withValue:@{@"primId": [NSString stringWithFormat:@"%i",singleEvent.newsId]
                                                                                                  ,@"eventId": [NSNumber numberWithInt:singleEvent.newsId],
                                                                                                  @"imageData": data}];
                                               
                                               [realm commitWriteTransaction];
                                               
                                               [downloadingIds removeObject:[NSNumber numberWithInt:singleEvent.newsId]];
                                               
                                               
                                               if(indexPath.row < eventsList.count)
                                               {
                                                   singleEvent.newsImage=[UIImage imageWithData:data];
                                                   
                                                   
                                                   [eventsList replaceObjectAtIndex:indexPath.row withObject:singleEvent];
                                                   
                                               }
                                               
                                               
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   
                                                   EventsTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                                                   
                                                   [UIView transitionWithView:cell.eventPicture duration:0.15f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                                                       cell.eventPicture.image=image;
                                                       
                                                       
                                                   } completion:^(BOOL finished) {
                                                       ;
                                                   }];
                                                   
                                                   
                                               });
                                               
                                           }
                                           
                                       }];
                
            }
            
        });
        
        
    }

    
    
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    News *singleEvent=[News new];
    singleEvent=[eventsList objectAtIndex:indexPath.row];
    
    NSLog(@"singleNews %@",singleEvent.newsUrl);
    if([singleEvent.newsUrl containsString:@"http"])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:singleEvent.newsUrl]];
    }
    else
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",singleEvent.newsUrl]]];

    
}

-(NSDate *)getDateFromString:(NSString *)dateString
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy'-'MM'-'dd' 'HH':'mm':'ss"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    //NSLog(@"dateString %@", dateString);
    
    //[formatter setDateFormat:@"EEE, MMM dd, yyyy"];
    NSDate *currentDate=[formatter dateFromString:dateString];
    return currentDate;
}

-(NSDate *)getPublishDateFromString:(NSString *)dateString
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy'-'MM'-'dd'"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    //NSLog(@"dateString %@", dateString);
    
    //[formatter setDateFormat:@"EEE, MMM dd, yyyy"];
    NSDate *currentDate=[formatter dateFromString:dateString];
    return currentDate;
}

-(NSString *)getStringFromDate:(NSDate *)date
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [formatter setDateFormat:@"d"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
    [formatter2 setFormatterBehavior:NSDateFormatterBehavior10_4];
    [formatter2 setDateFormat:@"MMMM YYYY"];
    NSString *monthFromDate = [formatter2 stringFromDate:date];
    
    
    NSDateFormatter *monthDayFormatter = [[NSDateFormatter alloc] init];
    [monthDayFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [monthDayFormatter setDateFormat:@"d"];
    int date_day = [[monthDayFormatter stringFromDate:date] intValue];
    NSString *suffix_string = @"|st|nd|rd|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|st|nd|rd|th|th|th|th|th|th|th|st";
    NSArray *suffixes = [suffix_string componentsSeparatedByString: @"|"];
    NSString *suffix = [[suffixes objectAtIndex:date_day] uppercaseString];
    NSString *dateString = [NSString stringWithFormat:@"%@ %@",[stringFromDate stringByAppendingString:suffix],monthFromDate];
    NSLog(@"dateString %@", dateString);
    return dateString;
}

-(NSString *)getEventsFromDate:(NSDate *)date
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd MMMM"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    // NSLog(@"stringFromDate %@", stringFromDate);
    return stringFromDate;
}

- (void)reloadData
{
    
    // CGPoint offset = self.feedTableView.contentOffset;
    
    // Reload table data
    [self.eventsTableView reloadData];
    
    [self.eventsTableView layoutIfNeeded];
    
    // End the refreshing
    if (self.refreshControl) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
        
        [self.refreshControl endRefreshing];
    }
}

- (IBAction)twitterShareButtonAction:(UIButton*)sender {
    
    News *singleEvent=[News new];
    singleEvent=[eventsList objectAtIndex:sender.tag];
    NSLog(@"singleNews %@",singleEvent);
    
    
    SLComposeViewController *twSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    [twSheet setInitialText:singleEvent.headLine];
    
    [twSheet addImage:singleEvent.newsImage];
    
    NSString *urlString;
    if([singleEvent.newsUrl containsString:@"http"])
    {
        urlString=singleEvent.newsUrl;
    }
    else
        urlString=[NSString stringWithFormat:@"http://%@",singleEvent.newsUrl];
    

    
    
    [twSheet addURL:[NSURL URLWithString:urlString]];
    [self presentViewController:twSheet animated:YES completion:nil];

}
- (IBAction)facebookShareEvent:(UIButton*)sender {
    
    News *singleNews=[News new];
    singleNews=[eventsList objectAtIndex:sender.tag];
    NSLog(@"singleNews %@",singleNews);
    
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    
    
    NSString *urlString;
    if([singleNews.newsUrl containsString:@"http"])
    {
        urlString=singleNews.newsUrl;
    }
    else
        urlString=[NSString stringWithFormat:@"http://%@",singleNews.newsUrl];
    

    
    content.contentURL = [NSURL URLWithString:urlString];
    content.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,singleNews.imageUrl ]];
    content.contentTitle = singleNews.headLine;
    content.contentDescription = singleNews.newsDescription;
    
    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
    dialog.fromViewController = self;
    dialog.shareContent = content;
    dialog.mode = FBSDKShareDialogModeNative; // if you don't set this before canShow call, canShow would always return YES
    if (![dialog canShow]) {
        // fallback presentation when there is no FB app
        dialog.mode = FBSDKShareDialogModeFeedBrowser;
    }
    [dialog show];
}

- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
