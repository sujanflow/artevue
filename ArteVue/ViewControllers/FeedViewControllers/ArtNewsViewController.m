//
//  ArtNewsViewController.m
//  Artegrams
//
//  Created by Sujan on 12/7/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "ArtNewsViewController.h"
#import "EventsTableViewCell.h"
#import "Constants.h"
#import "UserAccount.h"
#import "News.h"
#import "NewsRealm.h"

#import "ServerManager.h"

#import <Realm/Realm.h>

#import "NSArray+NullReplacement.h"
#import "NSDictionary+NullReplacement.h"
#import "DGActivityIndicatorView.h"

@interface ArtNewsViewController ()

{
    NSMutableArray *newsList;
    
    RLMRealmConfiguration *realmv1Configuration;
    NSMutableArray *downloadingIds;
    
    int pageIndex;
}


@property (nonatomic) DGActivityIndicatorView *activityIndicatorView;
@property UIRefreshControl *refreshControl;

@end

@implementation ArtNewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    pageIndex=1;
    self.activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)DGActivityIndicatorAnimationTypeTriplePulse tintColor:[UIColor blackColor]];
    CGFloat width = self.view.bounds.size.width / 5.0f;
    CGFloat height = self.view.bounds.size.width / 5.0f;
    
    //self.activityIndicatorView.backgroundColor=[UIColor redColor];
    self.activityIndicatorView.frame = CGRectMake(self.view.bounds.size.width/2-width/2,self.view.bounds.size.height/2-height/2, width, height);
    [self.view addSubview:self.activityIndicatorView];
    
    newsList=[[NSMutableArray alloc] init];
    downloadingIds=[[NSMutableArray alloc] init];
    
    
    self.artNewsTableView.estimatedRowHeight = 400;
    self.artNewsTableView.rowHeight = UITableViewAutomaticDimension;
    self.artNewsTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.artNewsTableView.frame.size.width, 1)];
    
    
    self.artNewsTableView.delegate = self;
    self.artNewsTableView.dataSource = self;
    
    [[NSFileManager defaultManager] removeItemAtURL:[RLMRealmConfiguration defaultConfiguration].fileURL error:nil];
    
    //  NSURL *v1URL = [[NSBundle mainBundle] URLForResource:@"defaultv1" withExtension:@"realm"];
    //  [[NSFileManager defaultManager] copyItemAtURL:v1URL toURL:realmv1URL error:nil];
    
    NSURL *defaultRealmURL = [RLMRealmConfiguration defaultConfiguration].fileURL;
    NSURL *defaultRealmParentURL = [defaultRealmURL URLByDeletingLastPathComponent];
    NSURL *realmv1URL = [defaultRealmParentURL URLByAppendingPathComponent:@"defaultv1.realm"];
    
    realmv1Configuration = [[RLMRealmConfiguration defaultConfiguration] copy];
    realmv1Configuration.fileURL = realmv1URL;
    
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor=[UIColor whiteColor];
    self.refreshControl.tintColor=[UIColor grayColor];
    [self.refreshControl addTarget:self action:@selector(loadNewsFeed) forControlEvents:UIControlEventValueChanged];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 10.0) {
        
        NSLog(@"below ios 10");
        self.artNewsTableView.backgroundView = self.refreshControl;
        
        
    } else {
        
        NSLog(@"above ios 10");
        self.artNewsTableView.refreshControl = self.refreshControl;
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
     [self loadNewsFeed];
}
-(void)viewWillDisappear:(BOOL)animated
{
    downloadingIds=[[NSMutableArray alloc] init];
    [[SDWebImageDownloader sharedDownloader] cancelAllDownloads];

}

-(void)configueUserPostModel:(NSMutableArray *)newsArray
{
    
    
    newsList=[[NSMutableArray alloc] init];
    
    
    for (int i=0; i<newsArray.count; i++) {
        
        News *singleNews=[News new];
        singleNews.newsId=[[[newsArray objectAtIndex:i] objectForKey:@"id"] intValue];
        
        singleNews.headLine=[[newsArray objectAtIndex:i] objectForKey:@"headline"];
        singleNews.newsDescription=[[newsArray objectAtIndex:i] objectForKey:@"description"];
        singleNews.publishTime=[self getStringFromDate:[self getDateFromString:[[newsArray objectAtIndex:i] objectForKey:@"publish_date"]]];;
        singleNews.newsUrl=[[newsArray objectAtIndex:i] objectForKey:@"url"];
        singleNews.newsImage=nil;
        singleNews.imageUrl=[[newsArray objectAtIndex:i] objectForKey:@"image"];

        
        
        RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
        RLMResults *result=[NewsRealm objectsInRealm:realm where:[NSString stringWithFormat:@"newsId = %i",singleNews.newsId]];
   
       // NSLog(@"result %@",result);
        if(result.count)
        {
            NewsRealm *newsData=[[NewsRealm alloc] init];
            newsData=[result objectAtIndex:0];
            singleNews.newsImage=[UIImage imageWithData:newsData.imageData];
            
        }
        
        [newsList addObject:singleNews];
    }
}



-(void)loadNewsFeed{
    
    [self.activityIndicatorView startAnimating];
    
    [[ServerManager sharedManager] getNewsListForPage:pageIndex WithCompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
        
        [self.activityIndicatorView stopAnimating];
        if ( resultDataArray!=nil) {
            
            NSMutableDictionary *responsedic= [[resultDataArray dictionaryByReplacingNullsWithBlanks] mutableCopy];
            newsList=[[NSMutableArray alloc] initWithArray:[responsedic objectForKey:@"data"]];
            
            if(newsList.count)
            {
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                    //Background Thread
                    
                    [self configueUserPostModel:newsList];
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //Run UI Updates
                        [self reloadData];
                        
                    });
                });

            }
            
            
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.activityIndicatorView stopAnimating];
                
                //[[[UIAlertView alloc] initWithTitle:@"Sorry, unable to fetch user data. Please try again." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                
                
            });
            
            
        }
        
    }];

}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return newsList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    EventsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"eventCell"];
    
    News *singleNews=[News new];
    singleNews=[newsList objectAtIndex:indexPath.row];
    NSLog(@"singleNews %@",singleNews);
    
    cell.eventNameLabel.text=singleNews.headLine;
    cell.eventPicture.image=singleNews.newsImage;
    cell.descriptionLabel.text=singleNews.newsDescription;
    cell.dateLabel.text=[singleNews.publishTime uppercaseString];
    cell.facebookButton.tag=indexPath.row;
    cell.twetterButton.tag=indexPath.row;
    
    if(singleNews.newsImage == Nil)
    {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            
            if(![downloadingIds containsObject: [NSNumber numberWithInt:singleNews.newsId]])
            {
                [downloadingIds addObject:[NSNumber numberWithInt:singleNews.newsId]];
                
                SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
                [SDWebImageDownloader sharedDownloader].maxConcurrentDownloads=1;
                
                [downloader downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,singleNews.imageUrl ]]
                                         options:0
                                        progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                            // progression tracking code
                                             NSLog(@"receivedSize %ld expectedSize %ld",(long)receivedSize,(long)expectedSize);
                                            
                                        }
                                       completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                           if (image && finished) {
                                               // do something with image
                                               
                                               //   Posts *post = [[Posts alloc] init];
                                               
                                               RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
                                               
                                               
                                               [realm beginWriteTransaction];
                                               [NewsRealm createOrUpdateInRealm:realm withValue:@{@"primId": [NSString stringWithFormat:@"%i",singleNews.newsId]
                                                                                              ,@"newsId": [NSNumber numberWithInt:singleNews.newsId],
                                                                                              @"imageData": data}];
                                               
                                               [realm commitWriteTransaction];
                                               
                                               [downloadingIds removeObject:[NSNumber numberWithInt:singleNews.newsId]];
                                               
                                               
                                               if(indexPath.row < newsList.count)
                                               {
                                                   singleNews.newsImage=[UIImage imageWithData:data];
                                                   
                                                   
                                                   [newsList replaceObjectAtIndex:indexPath.row withObject:singleNews];
                                                   
                                               }
                                               
                                               
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   
//                                                   [self.artNewsTableView beginUpdates];
//                                                   [self.artNewsTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
//                                                   [self.artNewsTableView endUpdates];
//                                                   
                                                   
                                                   EventsTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                                                   
                                                   [UIView transitionWithView:cell.eventPicture duration:0.15f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                                                       cell.eventPicture.image=image;
                                                       
                                                       
                                                   } completion:^(BOOL finished) {
                                                       ;
                                                   }];
                                                   
                                                   
                                               });
                                               
                                               
                                               
                                           }
                                           
                                       }];
                
            }
            
            
            
        });
        
        
    }
  //  */
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    News *singleNews=[News new];
    singleNews=[newsList objectAtIndex:indexPath.row];
    
    NSLog(@"singleNews %@",singleNews.newsUrl);
    if([singleNews.newsUrl containsString:@"http"])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:singleNews.newsUrl]];
    }
    else
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",singleNews.newsUrl]]];

    
}


-(NSDate *)getDateFromString:(NSString *)dateString
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy'-'MM'-'dd'"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    //NSLog(@"dateString %@", dateString);
    
    //[formatter setDateFormat:@"EEE, MMM dd, yyyy"];
    NSDate *currentDate=[formatter dateFromString:dateString];
    return currentDate;
}

-(NSString *)getStringFromDate:(NSDate *)date
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [formatter setDateFormat:@"d"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
    [formatter2 setFormatterBehavior:NSDateFormatterBehavior10_4];
    [formatter2 setDateFormat:@"MMMM YYYY"];
    NSString *monthFromDate = [formatter2 stringFromDate:date];
    
    
    NSDateFormatter *monthDayFormatter = [[NSDateFormatter alloc] init];
    [monthDayFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [monthDayFormatter setDateFormat:@"d"];
    int date_day = [[monthDayFormatter stringFromDate:date] intValue];
    NSString *suffix_string = @"|st|nd|rd|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|st|nd|rd|th|th|th|th|th|th|th|st";
    NSArray *suffixes = [suffix_string componentsSeparatedByString: @"|"];
    NSString *suffix = [[suffixes objectAtIndex:date_day] uppercaseString];
    NSString *dateString = [NSString stringWithFormat:@"%@ %@",[stringFromDate stringByAppendingString:suffix],monthFromDate];
     NSLog(@"dateString %@", dateString);
    return dateString;
}

- (void)reloadData
{
    
    // CGPoint offset = self.feedTableView.contentOffset;
    
    // Reload table data
    [self.artNewsTableView reloadData];
    
    [self.artNewsTableView layoutIfNeeded];
    
    // End the refreshing
    if (self.refreshControl) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
        
        [self.refreshControl endRefreshing];
    }
}

- (IBAction)facebookShareAction:(UIButton*)sender {
    
    News *singleNews=[News new];
    singleNews=[newsList objectAtIndex:sender.tag];
    NSLog(@"singleNews %@",singleNews);
    
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    NSString *urlString;
    
    if([singleNews.newsUrl containsString:@"http"])
    {
        urlString=singleNews.newsUrl;
    }
    else
        urlString=[NSString stringWithFormat:@"http://%@",singleNews.newsUrl];
    
    content.contentURL = [NSURL URLWithString:urlString];
    content.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,singleNews.imageUrl ]];
    content.contentTitle = singleNews.headLine;
    content.contentDescription = singleNews.newsDescription;
    
    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
    dialog.fromViewController = self;
    dialog.shareContent = content;
    dialog.mode = FBSDKShareDialogModeNative; // if you don't set this before canShow call, canShow would always return YES
    if (![dialog canShow]) {
        // fallback presentation when there is no FB app
        dialog.mode = FBSDKShareDialogModeFeedBrowser;
    }
    [dialog show];
    
//    SLComposeViewController *twSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
//    [twSheet setInitialText:singleNews.headLine];
//    [twSheet addImage:singleNews.newsImage];
//    [twSheet addURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",singleNews.newsUrl]]];
//    [self presentViewController:twSheet animated:YES completion:nil];

    
    
    
}
- (IBAction)twitterShareAction:(UIButton*)sender {
    
    News *singleNews=[News new];
    singleNews=[newsList objectAtIndex:sender.tag];
    NSLog(@"singleNews %@",singleNews);
    
    
    SLComposeViewController *twSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    [twSheet setInitialText:singleNews.headLine];

    [twSheet addImage:singleNews.newsImage];
    NSString *urlString;
    if([singleNews.newsUrl containsString:@"http"])
    {
        urlString=singleNews.newsUrl;
    }
    else
        urlString=[NSString stringWithFormat:@"http://%@",singleNews.newsUrl];
    
    [twSheet addURL:[NSURL URLWithString:urlString]];
    [self presentViewController:twSheet animated:YES completion:nil];
    
}

- (IBAction)backbuttonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
