//
//  ExistingMessageViewController.m
//  Artegrams
//
//  Created by Tanvir Palash on 8/16/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "ExistingMessageViewController.h"

#import "MessagingViewController.h"
#import "DemoMessagesViewController.h"
#import "NSDictionary+NullReplacement.h"
#import "Constants.h"
#import "Messages.h"
#import "ChatCell.h"
#import "ServerManager.h"

@interface ExistingMessageViewController ()
{
    
    NSMutableArray* messagesList;
    int pageIndex;
}

@end

@implementation ExistingMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    pageIndex=1;
    [self setTableView];    
    self.title = @"Direct";
    
    [UserAccount sharedManager].lastMessageDate=[self getDateStringFromDate:[NSDate date]];
   // [SDWebImageManager sharedManager].delegate=self;
}



-(void)viewWillAppear:(BOOL)animated
{
    
    
    [super viewWillAppear:animated];
    
    [self makeRequest];
    [self.existingMessagesTableView reloadData];
}
-(void)setTableView
{
    self.existingMessagesTableView.delegate = self;
    self.existingMessagesTableView.dataSource = self;
    self.existingMessagesTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f,self.view.frame.size.width, 10.0f)];
    self.existingMessagesTableView.backgroundColor = [UIColor clearColor];
}


#pragma mark - TableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [messagesList count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ChatListCell";
    ChatCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell)
    {
        cell = [[ChatCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSMutableDictionary *singleData=[[NSMutableDictionary alloc] initWithDictionary:[messagesList objectAtIndex:indexPath.row]];
    
    //NSLog(@"singleData %@",singleData);
    
    cell.nameLabel.text = [singleData objectForKey:@"username"];
    cell.messageLabel.text = [[singleData objectForKey:@"last_message"] objectForKey:@"message"];
    
    if([[[singleData objectForKey:@"last_message"] objectForKey:@"is_read"] intValue] == 1 || [[[singleData objectForKey:@"last_message"] objectForKey:@"sender_id"] intValue] == [UserAccount sharedManager].userId)
    {
        cell.backgroundColor=[UIColor clearColor];
    }else
    {
         cell.backgroundColor=[UIColor colorWithRed:0.9254901960784314 green:0.9411764705882353 blue:0.98 alpha:1.0];
    
    }
    cell.timeLabel.text=[self getStringFromDate:[self getDateFromString:[[singleData objectForKey:@"last_message"] objectForKey:@"created_at"]]];
    
    [cell.picture sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[singleData objectForKey:@"profile_picture"]]]];
    
    return cell;
    
}


#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
  
    NSMutableDictionary *singleData=[[NSMutableDictionary alloc] initWithDictionary:[messagesList objectAtIndex:indexPath.row]];
    
    DemoMessagesViewController *vc = [DemoMessagesViewController messagesViewController];
    vc.chatFollowerId=[NSString stringWithFormat:@"%@",[singleData objectForKey:@"user_id"]];
    vc.chatFollowerName=[singleData objectForKey:@"username"];
    [self.navigationController pushViewController:vc animated:YES];
    
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        
        [self deleteMessage:indexPath];
        
       
    }
}


-(void) makeRequest{
    
    [[ServerManager sharedManager] getMessageHistoryforPage:pageIndex WithCompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
        if ( resultDataArray!=nil) {
            
            NSMutableDictionary *responsedic= [[resultDataArray dictionaryByReplacingNullsWithBlanks] mutableCopy];
            messagesList=[[NSMutableArray alloc] initWithArray:[responsedic objectForKey:@"data"]];
            
            NSLog(@"messagesList: %@", messagesList);
            
            [self.existingMessagesTableView reloadData];
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                //[[[UIAlertView alloc] initWithTitle:@"Sorry, unable to fetch messages. Please try again." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                
                
                
            });
            
        }
        
    }];

    
}

-(void) deleteMessage:(NSIndexPath *)index{
    
    NSMutableDictionary *singleData=[[NSMutableDictionary alloc] initWithDictionary:[messagesList objectAtIndex:index.row]];
    
    
    [messagesList removeObject:singleData];
    [self.existingMessagesTableView reloadData];
    
    [[ServerManager sharedManager] deleteMessageForId:[[[singleData objectForKey:@"last_message"] objectForKey:@"receiver_id"]intValue] withCompletion:^(BOOL success) {
      
        if ( success) {
            
            
            NSLog(@"messagesList: %@", messagesList);
            
          }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                //[[[UIAlertView alloc] initWithTitle:@"Sorry, unable to fetch messages. Please try again." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                
                [messagesList insertObject:singleData atIndex:index.row];
                
                [self.existingMessagesTableView reloadData];
                
            });
            
        }

    }];
    

    
}




-(NSDate *)getDateFromString:(NSString *)dateString
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy'-'MM'-'dd' 'HH':'mm':'ss"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    
    //[formatter setDateFormat:@"EEE, MMM dd, yyyy"];
    NSDate *currentDate=[formatter dateFromString:dateString];
    return currentDate;
}

-(NSString *)getStringFromDate:(NSDate *)date
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    NSLog(@"%@", stringFromDate);
    return stringFromDate;
}


- (IBAction)closeButtonAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)addButtonAction:(id)sender {
        MessagingViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"MessagingViewController"];
        [self.navigationController pushViewController:controller animated:YES];
    

}

-(NSString *)getDateStringFromDate:(NSDate *)date
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy'-'MM'-'dd' 'HH':'mm':'ss"];
      [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    NSLog(@"getDateStringFromDate %@", stringFromDate);
    return stringFromDate;
    
}

//-(UIImage *)imageManager:(SDWebImageManager *)imageManager transformDownloadedImage:(UIImage *)image withURL:(NSURL *)imageURL
//{
//    // NSLog(@"imageURL : %@",imageURL);
//    UIImage *resizedImage;
//    //  return resizedImage  = [image resizedImage:CGSizeMake(512, 512) interpolationQuality:kCGInterpolationDefault];
//    
//    float MIN_UPLOAD_RESOLUTION=756*756;
//    float factor;
//    float resol = image.size.height*image.size.width;
//    if (resol >MIN_UPLOAD_RESOLUTION){
//        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
//        resizedImage = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
//        return resizedImage;
//        
//    }
//    else
//        return image;
//}
//
//- (UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
//    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
//    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
//    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return scaledImage;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
