//
//  ChatListCell.m
//  Whatsapp
//
//  Created by Rafael Castro on 7/24/15.
//  Copyright (c) 2015 HummingBird. All rights reserved.
//

#import "ChatCell.h"

@interface ChatCell()

@end



@implementation ChatCell

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.picture.layer.cornerRadius = self.picture.frame.size.width/2;
    //self.picture.layer.cornerRadius = 2;
    
    self.picture.layer.masksToBounds = YES;
    self.notificationLabel.layer.cornerRadius = self.notificationLabel.frame.size.width/2;
    self.notificationLabel.layer.masksToBounds = YES;
    self.nameLabel.text = @"";
    self.messageLabel.text = @"";
    self.timeLabel.text = @"";
    

 //   [self updateTimeLabelWithDate:[_chat objectForKey:@"date"]];
 //   [self updateUnreadMessagesIcon:[_chat objectForKey:@"unread"]];
}
-(void)updateTimeLabelWithDate:(NSDate *)date
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.timeStyle = NSDateFormatterShortStyle;
    df.dateStyle = NSDateFormatterNoStyle;
    df.doesRelativeDateFormatting = NO;
    self.timeLabel.text = [df stringFromDate:date];
}
-(void)updateUnreadMessagesIcon:(NSInteger)numberOfUnreadMessages
{
    self.notificationLabel.hidden = numberOfUnreadMessages == 0;
    self.notificationLabel.text = [NSString stringWithFormat:@"%ld", (long)numberOfUnreadMessages];
}
-(UIImageView *)imageView
{
    return _picture;
}

@end
