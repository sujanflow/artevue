//
//  CommentViewController.m
//  Artegrams
//
//  Created by Sujan on 8/1/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "CommentViewController.h"
#import "Constants.h"
#import "HexColors.h"
#import "UserAccount.h"

#import "CommentTableViewCell.h"
#import "HashTagViewViewController.h"

#import "DGActivityIndicatorView.h"
#import "ProfileViewController.h"

#import "NSDictionary+NullReplacement.h"
#import "ServerManager.h"

@interface CommentViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    int pageIndex;
}

@property (weak, nonatomic) IBOutlet UITableView *commentTableView;
@property (nonatomic) DGActivityIndicatorView *activityIndicatorView;



@end

@implementation CommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    pageIndex=1;
    
    self.activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)DGActivityIndicatorAnimationTypeTriplePulse tintColor:[UIColor blackColor]];
    CGFloat width = self.view.bounds.size.width / 5.0f;
    CGFloat height = self.view.bounds.size.width / 5.0f;
    
    self.activityIndicatorView.frame = CGRectMake(self.view.bounds.size.width/2-width/2,self.view.bounds.size.height/2-height/2, width, height);
    [self.view addSubview:self.activityIndicatorView];
    
    
    
    //NSLog(@"comments %@",self.commentList);
    NSLog(@"post user id %d",self.postBelongsToUserId);
    
    self.commentTableView.delegate = self;
    self.commentTableView.dataSource =self;
    
    self.commentTableView.estimatedRowHeight = 54;
    self.commentTableView.rowHeight = UITableViewAutomaticDimension;
    
    self.commentTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.commentTableView.frame.size.width, 1)];

   // [self registerForKeyboardNotifications];
    
    NSDictionary *attrDict = @{
                               NSFontAttributeName : [UIFont fontWithName:@"AzoSans-Regular" size:13],
                               NSForegroundColorAttributeName : [UIColor hx_colorWithHexString:@"#5A5A5A"]
                               };
    
    NSAttributedString *labelText = [[NSAttributedString alloc] initWithString : @"Add a comment..."
                                                                    attributes : attrDict];
    
    self.inputView.layer.zPosition=101;
    self.inputTextField.backgroundColor=[UIColor colorWithWhite:1.0 alpha:1];
    self.inputTextField.placeholderAttributedText=labelText;
    self.inputTextField.textView.delegate=self;
    
    
    [self registerForKeyboardNotifications];
    
    NSLog(@"self.inputTextField %@",self.inputTextField);
    
    self.inputTextField.textColor=[UIColor hx_colorWithHexString:@"#5A5A5A"];
 
   // [SDWebImageManager sharedManager].delegate=self;
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self loadComments];

}

-(void)loadComments
{
    [[ServerManager sharedManager] getCommentListForPost:self.postId inPage:pageIndex WithCompletion:^(BOOL success, NSMutableDictionary *responseObject) {
        
        [self.activityIndicatorView stopAnimating];
        
        if ( responseObject!=nil) {
            
            NSMutableDictionary *responsedic= [[responseObject dictionaryByReplacingNullsWithBlanks] mutableCopy];
            
            self.commentList=[[NSMutableArray alloc] initWithArray:[responsedic objectForKey:@"data"]];
            
            
            
            [self.activityIndicatorView stopAnimating];
            [self.commentTableView reloadData];
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.activityIndicatorView stopAnimating];
                
                //[[[UIAlertView alloc] initWithTitle:@"Sorry, unable to fetch user data. Please try again." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                
                
                
            });
            
        }
    }];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"#"] || [text isEqualToString:@"@"]) {
        GetUserHashtagViewController *vc  = [self.storyboard instantiateViewControllerWithIdentifier:@"GetUserHashtagViewController"];
        vc.delegate=self;
        
        if([text isEqualToString:@"#"])
            vc.isSearchUser=NO;
        else
            vc.isSearchUser=YES;
        
        vc.initialText=@"";
        
        [self.inputTextField.textView resignFirstResponder];
        
        [self presentViewController:vc animated:YES completion:nil];
        
        return NO;
        
    }

    NSUInteger oldLength = [textView.text length];
    NSUInteger replacementLength = [text length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [text rangeOfString: @"\n"].location != NSNotFound;
    
    NSLog(@"%lu %lu %lu %lu",(unsigned long)oldLength, (unsigned long)replacementLength,(unsigned long)rangeLength,(unsigned long)newLength);
    
    if(newLength<=oldLength)
        return YES;
    else
        return newLength <= 150 || returnKey;
}

-(void)textViewDidChange:(UITextView *)textView
{
    
    [self.inputTextField fitToScrollView];
    
    
    if(textView.text.length == 0){
        
        NSDictionary *attrDict = @{
                                   NSFontAttributeName : [UIFont fontWithName:@"AzoSans-Regular" size:13],
                                   NSForegroundColorAttributeName : [UIColor hx_colorWithHexString:@"#5A5A5A"]
                                   };
        
        NSAttributedString *labelText = [[NSAttributedString alloc] initWithString : @"Add a comment..."
                                                                        attributes : attrDict];
        
        self.inputTextField.placeholderAttributedText=labelText;
        
        [textView resignFirstResponder];
    }
//    else
//    {
//        NSArray *words = [textView.text componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//        NSInteger wordCount = [words count];
//        
//        if(wordCount>=30)
//        {
//            isTagSelectable=NO;
//            
//        }
//        else
//            isTagSelectable=YES;
//        
//    }


}

-(void) didFinishPickingItem:(NSMutableDictionary *)item forUser:(BOOL)test
{
    NSLog(@"item %@",item);
    
    [self.inputTextField.textView becomeFirstResponder];
    
    if(item.allKeys.count)
    {
        if(test)
        {
            if(self.inputTextField.textView.text.length>0)
                self.inputTextField.textView.text=[NSString stringWithFormat:@"%@@%@",self.inputTextField.textView.text,[item objectForKey:@"username"]];
            else
                self.inputTextField.textView.text=[NSString stringWithFormat:@"@%@",[item objectForKey:@"username"]];
            
        }
        else
        {
            if(self.inputTextField.textView.text.length>0)
                self.inputTextField.textView.text=[NSString stringWithFormat:@"%@%@",self.inputTextField.textView.text,[item objectForKey:@"hashtag"]];
            else
                self.inputTextField.textView.text=[item objectForKey:@"hashtag"];
            
        }
        
        
  
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)registerForKeyboardNotifications{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillShow:)
     
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillHide:)
     
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)keyboardWillShow:(NSNotification*)aNotification{
    
    NSDictionary* info = [aNotification userInfo];
    
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    self.inPutViewBottomConstraint.constant = kbSize.height - 45;
    
    [UIView animateWithDuration:0.1
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
    
}

- (void)keyboardWillHide:(NSNotification*)aNotification{
    
    self.inPutViewBottomConstraint.constant = 0;
    
    [UIView animateWithDuration:0.2
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    
    [self.inputView endEditing:YES];
    [self.view endEditing:YES];
    
}


#pragma mark - UITableView DataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;

}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.commentList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"commentCell";
    
    CommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[CommentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
//    UIImageView *profileImage = (UIImageView*) [cell viewWithTag:1];
//    UILabel *profileName = (UILabel*) [cell viewWithTag:2];
//    UILabel *comment = (UILabel*) [cell viewWithTag:3];
//    UILabel *date = (UILabel*) [cell viewWithTag:4];
    
    [cell.profilePic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[self.commentList objectAtIndex:indexPath.row] objectForKey:@"profile_picture"]]]];
    
    cell.userName.text = [[self.commentList objectAtIndex:indexPath.row] objectForKey:@"username"];
    
    cell.commentLabel.text = [[self.commentList objectAtIndex:indexPath.row] objectForKey:@"comment"];
    
    PatternTapResponder hashTagTapAction = ^(NSString *tappedString){
        NSLog(@"You have tapped hashTag: %@",tappedString);
        [self moveToHashTagExploreView:tappedString];
    };
    
    PatternTapResponder userTapAction = ^(NSString *tappedString){
        NSLog(@"You have tapped user: %@",tappedString);
        [self moveToUserView:tappedString];
    };
    
    [cell.commentLabel enableHashTagDetectionWithAttributes:@{NSForegroundColorAttributeName:[UIColor blueColor],
                                                              RLHighlightedBackgroundColorAttributeName:[UIColor clearColor],NSBackgroundColorAttributeName:[UIColor clearColor],RLHighlightedBackgroundCornerRadius:@5,
                                                              RLTapResponderAttributeName:hashTagTapAction}];
    
    [cell.commentLabel enableUserHandleDetectionWithAttributes:@{NSForegroundColorAttributeName:[UIColor blueColor],
                                                                 RLHighlightedBackgroundColorAttributeName:[UIColor clearColor],NSBackgroundColorAttributeName:[UIColor clearColor],RLHighlightedBackgroundCornerRadius:@5,
                                                                 RLTapResponderAttributeName:userTapAction}];
    


    //setting date label
    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    NSDateFormatter *serverDateFormatter = [[NSDateFormatter alloc] init];
//    
    NSString* formatString = [[self.commentList objectAtIndex:indexPath.row] objectForKey:@"created_at"];
    
//    ;
//    
//    
//    [serverDateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ssz"];
//    
//    NSDate *postDate = [serverDateFormatter dateFromString:formatString];
//    
//    [dateFormatter setDateFormat:@"EEE MMM d"];

//    date.text = [self getStringFromDate:[self getDateFromString:formatString]];
    
    NSLog(@"formatString %@",formatString);
    
    cell.dateLabel.text = [self relativeDateStringForDate:[self getDateFromString:formatString]];

    
    return cell;
}

#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    if ([[[self.commentList objectAtIndex:indexPath.row ] objectForKey:@"user_id"] intValue] == [UserAccount sharedManager].userId ||  [UserAccount sharedManager].userId==self.postBelongsToUserId) {
        
        return YES;
        
    }else
        
        return NO;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {

        if (editingStyle == UITableViewCellEditingStyleDelete) {
            //add code here for when you hit delete
            
            [self deleteComment:indexPath];
            
        }
    
}

-(void) deleteComment:(NSIndexPath *)index{
   
    NSMutableDictionary *singleData=[[NSMutableDictionary alloc] initWithDictionary:[self.commentList objectAtIndex:index.row]];
    
    NSLog(@"comment list %@",self.commentList);
    
    NSLog(@"comment singleData %@",singleData);
    
    [self.commentList removeObject:singleData];
    [self.commentTableView reloadData];
    
    
    
    [[ServerManager sharedManager] deleteCommentForPostId:[[singleData objectForKey:@"id"] intValue] withcompletion:^(BOOL success) {
        if (success) {
            
            [self.commentTableView reloadData];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshPost" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshFeed" object:nil userInfo:nil];
            
            
        }
        else{
            [self.commentList insertObject:singleData atIndex:index.row];
            
            [self.commentTableView reloadData];
            
        }
        
    }];
    
    
}


-(IBAction)moveToHashTagExploreView:(NSString*)hashTag {
    
    NSMutableDictionary *hashTagTempDic=[[NSMutableDictionary alloc] init];
    [hashTagTempDic setObject:@"0" forKey:@"tagId"];
    [hashTagTempDic setObject:hashTag forKey:@"tagName"];
    
    
    HashTagViewViewController * htController = [self.storyboard instantiateViewControllerWithIdentifier:@"HashTagViewViewController"];
    
    htController.hashTagInfoDic = hashTagTempDic;
    [self.navigationController pushViewController:htController animated:YES];
    
}


- (IBAction)sendButtonAction:(id)sender {
    
    self.sendButton.userInteractionEnabled = NO;
    [self.inputTextField.textView resignFirstResponder];
    
    NSMutableDictionary *tempMessage=[[NSMutableDictionary alloc] init];
    
    NSMutableDictionary *tempUser=[[NSMutableDictionary alloc] init];
    [tempUser setObject:[UserAccount sharedManager].userImageName forKey:@"profile_picture"];
    [tempUser setObject:[UserAccount sharedManager].userName forKey:@"username"];
    [tempUser setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"user_id"];
    
    [tempMessage setObject:self.inputTextField.text forKey:@"comment"];
    [tempMessage setObject:[self getDateStringFromDate:[NSDate date]] forKey:@"created_at"];
    
    
    [self.commentList addObject:tempMessage];
    [self.commentTableView reloadData];

    
    [[ServerManager sharedManager] postCommentInPostId:self.postId withComment:self.inputTextField.text withcompletion:^(BOOL success) {
        if (success) {
            
            self.inputTextField.text=@"";
            self.sendButton.userInteractionEnabled = YES;
            self.inputTextField.text=@"";
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshPost" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshFeed" object:nil userInfo:nil];
            
            
            [self.navigationController popViewControllerAnimated:YES];

        }
        else{
            
            self.sendButton.userInteractionEnabled = YES;
            
            
            [self.commentList removeObject:tempMessage];
            [self.commentTableView reloadData];
            
        }

    }];
  
}

-(IBAction)moveToUserView:(NSString*)userName {
    
    [self.activityIndicatorView startAnimating];
    
    
    
    [[ServerManager sharedManager] getUserByUserName:[userName substringFromIndex:1] withCompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
        
        [self.activityIndicatorView stopAnimating];
        
        if ( resultDataArray!=nil) {
            
            NSMutableDictionary *responsedic= [[resultDataArray dictionaryByReplacingNullsWithBlanks] mutableCopy];
            
            NSLog(@"responseDic %@",responsedic);
            
            
            ProfileViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
            
            controller.isUserOrFollower = YES;
            controller.userId = [[responsedic objectForKey:@"id"] intValue];
            
            [self.navigationController pushViewController:controller animated:YES];
        }
        else{
            
            
        }
        
    }];


    
}


-(NSString *)getDateStringFromDate:(NSDate *)date
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy'-'MM'-'dd' 'HH':'mm':'ss"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    NSLog(@"%@", stringFromDate);
    return stringFromDate;
}

-(NSString *)getStringFromDate:(NSDate *)date
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd, YYYY"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    NSLog(@"stringFromDate %@", stringFromDate);
    return stringFromDate;
}


-(NSDate *)getDateFromString:(NSString *)dateString
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy'-'MM'-'dd' 'HH':'mm':'ss"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    
    //[formatter setDateFormat:@"EEE, MMM dd, yyyy"];
    NSDate *currentDate=[formatter dateFromString:dateString];
    return currentDate;
}

- (NSString *)relativeDateStringForDate:(NSDate *)date
{
    
    NSLog(@"date %@ [NSDate date] %@",date,[NSDate date]);
    NSCalendarUnit units = NSCalendarUnitDay | NSCalendarUnitWeekOfYear |
    NSCalendarUnitMonth | NSCalendarUnitYear|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond;
    
    // if `date` is before "now" (i.e. in the past) then the components will be positive
    NSDateComponents *components = [[NSCalendar currentCalendar] components:units
                                                                   fromDate:date
                                                                     toDate:[NSDate date]
                                                                    options:0];
    
    NSLog(@"components %@",components);
    if (components.year > 0) {
        
        return [self getStringFromDate:date];
        
        //        if (components.year > 1) {
        //            return [NSString stringWithFormat:@"%ld years ago", (long)components.year];
        //        } else {
        //            return [NSString stringWithFormat:@"%ld year ago", (long)components.year];
        //
        //        }
        
        
    } else if (components.month > 0) {
        
        if (components.month > 1) {
            return [self getStringFromDate:date];
            
            // return [NSString stringWithFormat:@"%ld months ago", (long)components.month];
        } else {
            return [NSString stringWithFormat:@"%ld month ago", (long)components.month];
            
        }
        
        
        
    } else if (components.weekOfYear > 0) {
        if (components.weekOfYear > 1) {
            return [NSString stringWithFormat:@"%ld weeks ago", (long)components.weekOfYear];
        } else {
            return [NSString stringWithFormat:@"%ld week ago", (long)components.weekOfYear];
        }
        
        
        
    } else if (components.day > 0) {
        if (components.day > 1) {
            return [NSString stringWithFormat:@"%ld days ago", (long)components.day];
        } else {
            return @"Yesterday";
        }
    } else {
        if (components.hour > 1) {
            return [NSString stringWithFormat:@"%ld hours ago", (long)components.hour];
        }
        else if(components.minute > 1){
            return [NSString stringWithFormat:@"%ld minutes ago", (long)components.minute];
        }
        else
        {
            // return [NSString stringWithFormat:@"less than a minutes"];
            return [NSString stringWithFormat:@"%ld seconds ago", (long)components.second];
        }
        
    }
}

- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

//-(UIImage *)imageManager:(SDWebImageManager *)imageManager transformDownloadedImage:(UIImage *)image withURL:(NSURL *)imageURL
//{
//    // NSLog(@"imageURL : %@",imageURL);
//    UIImage *resizedImage;
//    //  return resizedImage  = [image resizedImage:CGSizeMake(512, 512) interpolationQuality:kCGInterpolationDefault];
//    
//    float MIN_UPLOAD_RESOLUTION=756*756;
//    float factor;
//    float resol = image.size.height*image.size.width;
//    if (resol >MIN_UPLOAD_RESOLUTION){
//        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
//        resizedImage = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
//        return resizedImage;
//        
//    }
//    else
//        return image;
//}
//
//- (UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
//    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
//    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
//    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return scaledImage;
//}
@end
