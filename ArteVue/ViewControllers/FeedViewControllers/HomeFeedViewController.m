//
//  HomeFeedViewController.m
//  Artegram
//
//  Created by Tanvir Palash on 6/10/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "HomeFeedViewController.h"
#import "FeedTableViewCell.h"
#import "Constants.h"
#import "LikersViewController.h"
#import "ProfileViewController.h"

#import "CommentViewController.h"

#import "MessagingViewController.h"
#import "ExistingMessageViewController.h"

#import "NSArray+NullReplacement.h"
#import "NSDictionary+NullReplacement.h"

#import "DGActivityIndicatorView.h"
#import "SharePhotoViewController.h"
#import "HashTagViewViewController.h"
#import "ArtistPostViewController.h"
#import "ArtNewsViewController.h"
#import "EventsViewController.h"

#import "ExploreTableViewCell.h"

#import "GIBadgeView.h"

#import <Realm/Realm.h>
#import "UserAccount.h"
#import "UserPost.h"
#import "ServerManager.h"
#import "Posts.h"

#import "ZHPopupView.h"

@import GooglePlaces;



@interface HomeFeedViewController ()
{
    NSMutableArray *feedList;
    NSMutableDictionary*sharePostDictionary;
    UserPost *shareUser;
    UIImageView* imageViewForShare;
    PTPusher *pusherClient;
    GIBadgeView* badgeView;
    GMSPlacesClient *_placesClient;
    NSArray *feedType;
    NSMutableArray *imageNameArray;
    
    int pageIndex;
    BOOL islastItemReached;
    
    int selectedTab;
    
    RLMRealmConfiguration *realmv1Configuration;
    NSMutableArray *downloadingIds;
    
    NSMutableArray *feedDataSource;
}

@property (nonatomic) DGActivityIndicatorView *activityIndicatorView;


@end

@implementation HomeFeedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     _placesClient = [GMSPlacesClient sharedClient];
    
    //setting delegate
    badgeView = [GIBadgeView new];
    
    badgeView.font = [UIFont fontWithName:@"AzoSans-Medium" size:12];
    badgeView.rightOffset = 5.0f;
    badgeView.topOffset=5.0f;
    [badgeView setHidden:YES];
    
    [self.inboxActionButton addSubview:badgeView];
    NSLog(@"badgeView %@",badgeView);
    
    
    pusherClient = [PTPusher pusherWithKey:pusher_app_key delegate:self encrypted:YES cluster:@"ap1"];
    
    PTPusherChannel *channel = [pusherClient subscribeToChannelNamed:[NSString stringWithFormat:@"%i-message-channel",[UserAccount sharedManager].userId]];
    
    [channel bindToEventNamed:@"new-message" handleWithBlock:^(PTPusherEvent *channelEvent) {
        
        NSLog(@"message received in home : %@", channelEvent.data);
        [UserAccount sharedManager].pendingMessageCount++;
        
        [badgeView setBadgeValue: [UserAccount sharedManager].pendingMessageCount];
        [badgeView setHidden:NO];
        
       
        
    }];
    
    
    [pusherClient connect];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBadgeValue) name:@"updateMessageBubble" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadInitialFeed) name:@"loadAllFeed" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(makeRequestwithPage) name:@"RefreshFeed" object:nil];
    
    
    self.activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)DGActivityIndicatorAnimationTypeTriplePulse tintColor:[UIColor blackColor]];
    CGFloat width = self.view.bounds.size.width / 5.0f;
    CGFloat height = self.view.bounds.size.width / 5.0f;
    
    //self.activityIndicatorView.backgroundColor=[UIColor redColor];
    self.activityIndicatorView.frame = CGRectMake(self.view.bounds.size.width/2-width/2,self.view.bounds.size.height/2-height/2, width, height);
    [self.view addSubview:self.activityIndicatorView];
    
    //   NSLog(@"RLMRealm.defaultRealm().path %@",[RLMRealmConfiguration defaultConfiguration].fileURL);
    //   [[RLMRealm defaultRealm] writeCopyToURL:[NSURL URLWithString:@"`/Users/tanvirpalash/desktop/post.realm"] encryptionKey:nil error:nil];
    
    [[NSFileManager defaultManager] removeItemAtURL:[RLMRealmConfiguration defaultConfiguration].fileURL error:nil];
    
    //  NSURL *v1URL = [[NSBundle mainBundle] URLForResource:@"defaultv1" withExtension:@"realm"];
    //  [[NSFileManager defaultManager] copyItemAtURL:v1URL toURL:realmv1URL error:nil];
    
    NSURL *defaultRealmURL = [RLMRealmConfiguration defaultConfiguration].fileURL;
    NSURL *defaultRealmParentURL = [defaultRealmURL URLByDeletingLastPathComponent];
    NSURL *realmv1URL = [defaultRealmParentURL URLByAppendingPathComponent:@"defaultv1.realm"];
    
    realmv1Configuration = [[RLMRealmConfiguration defaultConfiguration] copy];
    realmv1Configuration.fileURL = realmv1URL;
    
    
    
    feedList=[[NSMutableArray alloc] init];
    feedDataSource=[[NSMutableArray alloc] init];
    
    downloadingIds=[[NSMutableArray alloc] init];
    self.feedTableView.delegate = self;
    self.feedTableView.dataSource = self;
    
   //self.postTypeCollectionView.hidden = YES;
    
    self.feedTableView.estimatedRowHeight = 543;
    self.feedTableView.rowHeight = UITableViewAutomaticDimension;
    
    self.feedTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.feedTableView.frame.size.width, 1)];
    
    selectedTab=0;
    
    //feedType = [[NSArray alloc]initWithObjects:@"Trending Artists",@"Top Posts",@"Favourites",@"Events",@"Art News", nil];
    feedType = [[NSArray alloc]initWithObjects:@"Art News",@"Events", nil];
    
    //imageNameArray= [[NSArray alloc]initWithObjects:@"trendingArtist",@"topPost",@"favouritePost",@"eventIcon",@"aiweiwei", nil];
    
    
    
    self.postTypeCollectionView.delegate =  self;
    self.postTypeCollectionView.dataSource = self;
    
    pageIndex=1;
    
    
    
//    self.feedTableView.refreshControl=[[UIRefreshControl alloc] init];
//    self.feedTableView.refreshControl.backgroundColor=[UIColor whiteColor];
//    self.feedTableView.refreshControl.tintColor=[UIColor grayColor];
//    [self.feedTableView.refreshControl addTarget:self action:@selector(loadInitialFeed) forControlEvents:UIControlEventValueChanged];
    
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor=[UIColor whiteColor];
    self.refreshControl.tintColor=[UIColor grayColor];
    [self.refreshControl addTarget:self action:@selector(loadInitialFeed) forControlEvents:UIControlEventValueChanged];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 10.0) {
        
        NSLog(@"below ios 10");
        self.feedTableView.backgroundView = self.refreshControl;
        
        
    } else {
        
        NSLog(@"above ios 10");
        self.feedTableView.refreshControl = self.refreshControl;
    }
    
    
  //  [SDWebImageManager sharedManager].delegate=self;
    [self makeRequestwithPage];
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) viewDidAppear:(BOOL)animated
{
    //api call
    
    imageViewForShare=[[UIImageView alloc]init];
    //[self makeRequest];
    
    self.comingSoonLabel.hidden=YES;
    self.newsLabel.hidden=YES;
    
    
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        
        [self configueUserPostModel];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            [self.feedTableView reloadData];
            
        });
    });
    
    // NSLog(@"RefreshNeededInFeed %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"RefreshNeededInFeed"]);
    
//    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"RefreshNeededInFeed"] isEqualToString:@"1"])
//    {
//        [self loadInitialFeed];
//        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"RefreshNeededInFeed"];
//    }
//  
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[SDWebImageDownloader sharedDownloader] cancelAllDownloads];
    downloadingIds=[[NSMutableArray alloc] init];
    feedDataSource=[[NSMutableArray alloc] init];
}

- (void)reloadData
{
    
   // CGPoint offset = self.feedTableView.contentOffset;

    // Reload table data
    [self.feedTableView reloadData];
    
    [self.feedTableView layoutIfNeeded];
    //[self.feedTableView setContentOffset:offset animated:YES];
    
    // End the refreshing
    if (self.refreshControl) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
        
        [self.refreshControl endRefreshing];
    }
}

-(void)loadInitialFeed
{
    if(feedDataSource.count)
    {
        NSIndexPath* index=[NSIndexPath indexPathForRow:0 inSection:0];
        [self.feedTableView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
    }
    
    imageViewForShare=[[UIImageView alloc]init];
    //[self makeRequest];
    pageIndex=1;
    self.comingSoonLabel.hidden=YES;
    self.newsLabel.hidden=YES;
    
    
    selectedTab=0;
    
    self.artNewsButton.selected=NO;
    self.eventsButton.selected=NO;
    
    
    self.topSelectedView.hidden=YES;
    self.bottomSelectedView.hidden=YES;
    
    [self.postTypeCollectionView reloadData];
    
    [self.activityIndicatorView startAnimating];
    
    [[ServerManager sharedManager] getCurrentUserFeedForPage:pageIndex WithCompletion:^(BOOL success, NSMutableDictionary *responseObject) {
        
        [self.activityIndicatorView stopAnimating];
        
        if ( responseObject!=nil) {
            
            
            NSMutableDictionary *responsedic= [[responseObject dictionaryByReplacingNullsWithBlanks] mutableCopy];
            NSMutableArray *responseArray=[[NSMutableArray alloc] initWithArray:[responsedic objectForKey:@"data"]];
            
            
            
            
            if(responseArray.count)
            {
                feedList=[[NSMutableArray alloc] initWithArray:responseArray];
                
                self.feedTableView.hidden = NO;
                self.topSegmentView.hidden = NO;
                
                self.noDataLabel.hidden=YES;
                
                islastItemReached=NO;
                //  [self.feedTableView reloadData];
                
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                    //Background Thread
                    
                    [self configueUserPostModel];
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //Run UI Updates
                        [self reloadData];
                        
                    });
                });
            
            }
            else
            {
                self.topSegmentView.hidden = YES;
                
                self.feedTableView.hidden = YES;
                self.noDataLabel.hidden=NO;
                
                //[self.feedTableView reloadData];
                [self reloadData];
                
                
            }
            
            // [self backgroundAdd];
            
            [self.activityIndicatorView stopAnimating];
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.activityIndicatorView stopAnimating];
                
                //[[[UIAlertView alloc] initWithTitle:@"Sorry, unable to fetch user data. Please try again." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                
                [self reloadData];
              
            });
            
        }
    }];

}


-(void) makeRequestwithPage{
    
    //[[SDWebImageDownloader sharedDownloader] cancelAllDownloads];
    
    [self.activityIndicatorView startAnimating];
    
    if (pageIndex<=0) {
        pageIndex=1;
    }
    
    [[ServerManager sharedManager] getCurrentUserFeedForPage:pageIndex WithCompletion:^(BOOL success, NSMutableDictionary *responseObject) {
        
        [self.activityIndicatorView stopAnimating];
        
        if ( responseObject!=nil) {
            
            NSMutableDictionary *responsedic= [[responseObject dictionaryByReplacingNullsWithBlanks] mutableCopy];
            
            
            NSMutableArray *responseArray=[[NSMutableArray alloc] initWithArray:[responsedic objectForKey:@"data"]];
            
            if(selectedTab!=1 && selectedTab != 2 && selectedTab !=3)
            {
                imageNameArray = [[NSMutableArray alloc] init];
                
                [[ServerManager sharedManager] getTopImageInFeedWithCompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
                    if ( resultDataArray!=nil) {
                        
                        NSMutableDictionary *responsedic= [[resultDataArray dictionaryByReplacingNullsWithBlanks] mutableCopy];
                        
                        [imageNameArray addObject:[[responsedic objectForKey:@"data"] objectForKey:@"news_image"]];
                        [imageNameArray addObject:[[responsedic objectForKey:@"data"] objectForKey:@"events_image"]];
                        
                        [self.postTypeCollectionView reloadData];
                        
                        
                    }
                    
                }];

            }
            
            // NSLog(@"responseArray %d",responseArray.count);
            if(responseArray.count)
            {
                if(pageIndex==1)
                {
                    feedList=[[NSMutableArray alloc] init];
                    feedList=responseArray;
                    
                }else
                {
                    [feedList addObjectsFromArray:responseArray];
                    
                }
                pageIndex++;
                islastItemReached=NO;
            }
            else
            {
                islastItemReached=YES;
            }
            
            
            //   NSLog(@"Response: %@ %@", urlString,feedList);
            
            if(feedList.count)
            {
                self.feedTableView.hidden = NO;
                self.topSegmentView.hidden = NO;
                
                self.noDataLabel.hidden=YES;
                
                
            }
            else
            {
                self.topSegmentView.hidden = YES;
                
                self.feedTableView.hidden = YES;
                self.noDataLabel.hidden=NO;
            }
            
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                //Background Thread
                
                [self configueUserPostModel];
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    //Run UI Updates
                    [self.feedTableView reloadData];
                });
            });
            
            
            //   CGPoint offset = self.feedTableView.contentOffset;
            
            // Reload table data
            
            
            //                      [self.feedTableView layoutIfNeeded];
            //                      [self.feedTableView setContentOffset:offset animated:YES];
            
            
            [self.activityIndicatorView stopAnimating];
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.activityIndicatorView stopAnimating];
                
                //[[[UIAlertView alloc] initWithTitle:@"Sorry, unable to fetch user data. Please try again." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                
                [self reloadData];
                
            });
            
        }
    }];
    
}

-(void)configueUserPostModel
{
   
    
    feedDataSource=[[NSMutableArray alloc] init];
    
    //NSLog(@"configueUserPostModel %@",feedList);
      for (int i=0; i<feedList.count; i++) {
          
          UserPost *singlePost=[UserPost new];
          
          singlePost.postId=[[[feedList objectAtIndex:i] objectForKey:@"id"] intValue];
          
         
          singlePost.aspectRatio=[[[feedList objectAtIndex:i] objectForKey:@"aspect_ratio"] floatValue];
          singlePost.price=[[[feedList objectAtIndex:i] objectForKey:@"price"] floatValue];
          singlePost.has_buy_btn=[[[feedList objectAtIndex:i] objectForKey:@"has_buy_btn"] intValue];
          if (![[[feedList objectAtIndex:i] objectForKey:@"description"] isEqual:[NSNull null]] && ![[[feedList objectAtIndex:i] objectForKey:@"description"] isEqualToString:@""]) {
              
              singlePost.postDescription = [[feedList objectAtIndex:i] objectForKey:@"description"];
              
          }else{
               singlePost.postDescription = @"";
          }
          
          singlePost.artistName=[[feedList objectAtIndex:i] objectForKey:@"artist"];
          singlePost.userId=[[[[feedList objectAtIndex:i] objectForKey:@"owner"] objectForKey:@"id"] intValue];
          singlePost.userName=[[[feedList objectAtIndex:i] objectForKey:@"owner"] objectForKey:@"username"];
          singlePost.addressTitle=[[feedList objectAtIndex:i] objectForKey:@"address_title"];
          singlePost.postTime=[[feedList objectAtIndex:i] objectForKey:@"created_at"];
          singlePost.postImageUrl=[[feedList objectAtIndex:i] objectForKey:@"image"];
          
          UIFont *userNameFont = [UIFont fontWithName:@"AzoSans-Medium" size:14];
          UIFont *locationFont = [UIFont fontWithName:@"AzoSans-Regular" size:10];
          
          NSDictionary *userDict = [NSDictionary dictionaryWithObject:userNameFont forKey:NSFontAttributeName ];
          NSDictionary *locationDict = [NSDictionary dictionaryWithObject: locationFont forKey:NSFontAttributeName];
          
          NSMutableAttributedString *uAttrString = [[NSMutableAttributedString alloc] initWithString:singlePost.userName attributes: userDict];
          NSMutableAttributedString *dAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@",[self relativeDateStringForDate:[self getDateFromString:singlePost.postTime]]] attributes: locationDict];
          
          
          if (singlePost.addressTitle.length>0) {
              
        
              NSMutableAttributedString *lAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@",singlePost.addressTitle] attributes: locationDict];
              [uAttrString appendAttributedString:lAttrString];
              [uAttrString appendAttributedString:dAttrString];
              singlePost.headerTitle=uAttrString;
              
          }
          else
          {
              [uAttrString appendAttributedString:dAttrString];
              singlePost.headerTitle=uAttrString;
          }
          
          if(![[[[feedList objectAtIndex:i] objectForKey:@"owner"] objectForKey:@"profile_picture"] isEqual:[NSNull null]] && ![[[[feedList objectAtIndex:i] objectForKey:@"owner"] objectForKey:@"profile_picture"]isEqualToString:@""])
                singlePost.userImage=[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[[feedList objectAtIndex:i] objectForKey:@"owner"] objectForKey:@"profile_picture"]];
          else
              NSLog(@"[[[feedList objectAtIndex:i] objectForKey:@""] objectForKey:@""] %@",[[[feedList objectAtIndex:i] objectForKey:@"owner"] objectForKey:@"profile_picture"]);
          //NSLog(@"singlePost.userImage %@",singlePost.userImage);
         
          
          singlePost.postCommentsCount = [[[feedList objectAtIndex:i] objectForKey:@"comment_count"] intValue];
          
          singlePost.postLikesCount=[[[feedList objectAtIndex:i] objectForKey:@"like_count"] intValue];
          singlePost.pinCount=[[[feedList objectAtIndex:i] objectForKey:@"pin_count"] intValue];
          
          singlePost.is_liked=[[[feedList objectAtIndex:i] objectForKey:@"is_liked"] intValue];
          singlePost.is_pinned=[[[feedList objectAtIndex:i] objectForKey:@"is_pinned"] intValue];
          
//          
//          singlePost.googlePlaceId;
//          singlePost.longitude;
//          singlePost.latitude;
          
          
          RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
          
          RLMResults *result=[Posts objectsInRealm:realm where:[NSString stringWithFormat:@"postId = %i",singlePost.postId]];
          
          
         // NSLog(@"result result %@",result);
          if(result.count)
          {
              Posts *post=[[Posts alloc] init];
              post=[result objectAtIndex:0];
             
              
              singlePost.postImage=[UIImage imageWithData:post.imageData];
              
          }
          else
          {
              singlePost.postImage=nil;
              
        }

          
      
          [feedDataSource addObject:singlePost];
      }
}



- (void)backgroundAdd
{
    
    //[[NSFileManager defaultManager] removeItemAtURL:[RLMRealmConfiguration defaultConfiguration].fileURL error:nil];
    
    for (int i=0; i<feedList.count; i++) {
        
        
        
        // NSLog(@"backgroundAdd with %@",[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[feedList objectAtIndex:i] objectForKey:@"image"] ]]);
        
        // RLMRealm *realmTest = [RLMRealm defaultRealm];
        RLMRealm *realmv1 = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
        
        
        RLMResults *result=[Posts objectsInRealm:realmv1 where:[NSString stringWithFormat:@"postId = %@",[[feedList objectAtIndex:i] objectForKey:@"id"]]];
        
        if(result.count)
        {
            
            [self reloadData];
            
        }
        else
        {
            SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
            [SDWebImageDownloader sharedDownloader].maxConcurrentDownloads=1;
            
            [downloader downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[feedList objectAtIndex:i] objectForKey:@"image"] ]]
                                     options:0
                                    progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                        // progression tracking code
                                        NSLog(@"receivedSize %ld expectedSize %ld",(long)receivedSize,(long)expectedSize);
                                        
                                        
                                    }
                                   completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                       if (image && finished) {
                                           // do something with image
                                           
                                           //   Posts *post = [[Posts alloc] init];
                                           
                                           dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                                           dispatch_async(queue, ^{
                                               // Get new realm and table since we are in a new thread
                                               
                                               NSLog(@"done %@",[[feedList objectAtIndex:i] objectForKey:@"id"]);
                                               
                                               //RLMRealm *realm = [RLMRealm defaultRealm];
                                               
                                               RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
                                               
                                               
                                               [realm beginWriteTransaction];
                                               [Posts createInRealm:realm withValue:@{@"postId": [NSNumber numberWithInt:[[[feedList objectAtIndex:i] objectForKey:@"id"] intValue]],
                                                                                      @"imageData": data}];
                                               
                                               [realm commitWriteTransaction];
                                               
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   [self reloadData];
                                                   
                                               });
                                           });
                                           
                                           
                                       }
                                   }];
            
            
            
        }
    }
    
    
    
}

#pragma mark - CollectionView data source

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return  imageNameArray.count;
    
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    //  NSLog(@"artArray.............  %@",_artArray);
    
    static NSString *identifier = @"postTypeCell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    UIImageView *postTypeImage = (UIImageView*) [cell viewWithTag:1];
    UILabel *postTypeLabel = (UILabel*) [cell viewWithTag:2];
    
    
    [cell layoutIfNeeded];
    postTypeImage.layer.cornerRadius = 3;
    //self.profilePicture.layer.borderWidth = 1.1f;
    postTypeImage.clipsToBounds = YES;
    postTypeImage.layer.masksToBounds = YES;
   
    postTypeLabel.text =[NSString stringWithFormat:@"%@",[feedType objectAtIndex:indexPath.row]];
    
//    NSLog(@"[imageNameArray objectAtIndex:indexPath.row] %@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SERVER_BASE_API_URL_FOR_IMAGE,[imageNameArray objectAtIndex:indexPath.row] ]]);
//    if(selectedTab!=1 && selectedTab != 2 && selectedTab !=3)
//    {
//        NSLog(@"%@",[imageNameArray objectAtIndex:indexPath.row]);
        [ postTypeImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SERVER_BASE_API_URL_FOR_IMAGE,[imageNameArray objectAtIndex:indexPath.row] ]]];
//    }
//    else
//    {
//        NSString * fileName= [ [ NSBundle mainBundle] pathForResource:[imageNameArray objectAtIndex:indexPath.row] ofType:@"jpg"];
//        
//        postTypeImage.image=[UIImage imageWithContentsOfFile:fileName];
//       
//        
//    }
    
    postTypeImage.layer.borderColor=[UIColor clearColor].CGColor;
    if(selectedTab==indexPath.row+1)
    {
        postTypeImage.layer.borderWidth=2.0f;
        
    }else
    {
        postTypeImage.layer.borderWidth=0.0f;
        
    }
    
    
    NSLog(@"collectionview %@ cell %@",collectionView,cell);
    return cell;
    
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake((collectionView.frame.size.width-5)/2, collectionView.frame.size.height);
    
    
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
  //  [self.postTypeCollectionView reloadData];
    
    self.comingSoonLabel.hidden=YES;
    self.newsLabel.hidden=YES;
    self.feedTableView.hidden = NO;
    
    pageIndex=1;
    selectedTab=(int)indexPath.item+1;
    
//    if (indexPath.item==0 || indexPath.item==1 || indexPath.item==2) {
//       
//        [self makeRequestwithPage];
//        
//    }
//    else
//    
    if (indexPath.item==0)
    {
        ArtNewsViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ArtNewsViewController"];
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    else if (indexPath.item==1)
    {
        
        EventsViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"EventsViewController"];
        [self.navigationController pushViewController:controller animated:YES];
    }

    
    
    
    
}

-(void)collectionView:(UICollectionView *)collectionView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"didDeselectRowAtIndexPath");
//    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
//    UIImageView *postTypeImage = (UIImageView*) [cell viewWithTag:1];
//    
//    postTypeImage.layer.borderColor=[UIColor whiteColor].CGColor;
//    postTypeImage.layer.borderWidth=2.0f;
    
    
    
}




#pragma mark - UITableView DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

  return feedDataSource.count;

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{

    static NSString *HeaderCellIdentifier = @"feedHeaderTableCell";
    
    ExploreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:HeaderCellIdentifier];
    if (cell == nil) {
        cell = [[ExploreTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:HeaderCellIdentifier];
    }
    
    cell.tag= section;
    cell.profilePic.layer.cornerRadius = cell.profilePic.bounds.size.width/2;
    //self.profilePicture.layer.borderWidth = 1.1f;
    cell.profilePic.clipsToBounds = YES;
    cell.profilePic.layer.masksToBounds = YES;
    
    UserPost *singlePost=[UserPost new];
    singlePost=[feedDataSource objectAtIndex:section];
    cell.userName.attributedText=singlePost.headerTitle;
    
    NSLog(@"artistName %@ in section %li for count %lu feedlist %lu",singlePost.artistName, (long)section, (unsigned long)feedDataSource.count,(unsigned long)feedList.count);
    NSLog(@"singlePost.userImage %@",singlePost.userImage);
    [cell.profilePic sd_setImageWithURL:[NSURL URLWithString:singlePost.userImage]];
    
    
    [cell.artistName setText:singlePost.artistName];
    if(singlePost.artistName.length)
    {
        //   NSLog(@" writing artist %@",[[feedList objectAtIndex:section] objectForKey:@"artist"]);
        
        
        
   
        cell.arrowLabel.hidden=NO;
        cell.artistNameInteractionView.hidden=NO;
        
        
        UITapGestureRecognizer *tapOnHeader = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnArtistName:)];
        //[tapOnHeader setDelegate:self];
        tapOnHeader.numberOfTouchesRequired = 1;
        tapOnHeader.numberOfTapsRequired = 1;
        cell.artistName.userInteractionEnabled=YES;
        [cell.artistName addGestureRecognizer:tapOnHeader];
        
        UITapGestureRecognizer *tapOnArtist = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnArtistNameInView:)];
        //[tapOnHeader setDelegate:self];
        tapOnArtist.numberOfTouchesRequired = 1;
        tapOnArtist.numberOfTapsRequired = 1;
        cell.artistNameInteractionView.tag=section;
        [cell.artistNameInteractionView addGestureRecognizer:tapOnArtist];
    }
    else
    {
        cell.arrowLabel.hidden=YES;
        cell.artistNameInteractionView.hidden=YES;
      
        
    }
    
    cell.moreButton.tag= section;
    
    //tap on header view
    
    UITapGestureRecognizer *tapOnHeader = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHeader:)];
    //[tapOnHeader setDelegate:self];
    tapOnHeader.numberOfTouchesRequired = 1;
    tapOnHeader.numberOfTapsRequired = 1;
    [cell addGestureRecognizer:tapOnHeader];
    


    
    return cell;

}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    FeedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"feedTableCell"];
    
    if (cell == nil) {
        cell = [[FeedTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"feedTableCell"];
    }
    
    
    for (UIView *v in cell.feedImage.subviews) {
        [v removeFromSuperview];
    }
   // [ cell.feedImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[feedList objectAtIndex:indexPath.section] objectForKey:@"image"] ]]];
    cell.feedImage.tag=indexPath.section;

    UserPost *singlePost=[UserPost new];
    singlePost=[feedDataSource objectAtIndex:indexPath.section];
    
    cell.feedImage.image=singlePost.postImage;
    
    if(singlePost.postImage == Nil)
    {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            
            if(![downloadingIds containsObject: [NSNumber numberWithInt:singlePost.postId]])
            {
                [downloadingIds addObject:[NSNumber numberWithInt:singlePost.postId]];
                
                SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
                [SDWebImageDownloader sharedDownloader].maxConcurrentDownloads=1;
                
                [downloader downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[feedList objectAtIndex:indexPath.section] objectForKey:@"image"] ]]
                                         options:0
                                        progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                            // progression tracking code
                                           // NSLog(@"receivedSize %ld expectedSize %ld",(long)receivedSize,(long)expectedSize);
                                            
                                        }
                                       completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                           if (image && finished) {
                                               // do something with image
                                               
                                               //   Posts *post = [[Posts alloc] init];
                                               
                                               RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
                                               
                                               
                                               [realm beginWriteTransaction];
                                               [Posts createOrUpdateInRealm:realm withValue:@{@"primId": [NSString stringWithFormat:@"%i",singlePost.postId]
                                                                                              ,@"postId": [NSNumber numberWithInt:singlePost.postId],
                                                                                              @"imageData": data}];
                                               
                                               [realm commitWriteTransaction];
                                               
                                               [downloadingIds removeObject:[NSNumber numberWithInt:singlePost.postId]];
                                               
                                               
                                               if(indexPath.section < feedDataSource.count)
                                               {
                                                   singlePost.postImage=[UIImage imageWithData:data];
                                                   
                                                   
                                                   [feedDataSource replaceObjectAtIndex:indexPath.section withObject:singlePost];
                                                   
                                               }
                                               
                                               NSLog(@"singlePost %i feedList %@",singlePost.postId,[[feedList objectAtIndex:indexPath.section] objectForKey:@"id"]);
                                               
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   
                                                   
                                                   //                                               NSArray *visible       = [self.feedTableView indexPathsForVisibleRows];
                                                   //                                               [self.feedTableView reloadRowsAtIndexPaths:visible withRowAnimation:UITableViewRowAnimationNone];
                                                   //
                                                
                                                   FeedTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                                                   
                                                   [UIView transitionWithView:cell.feedImage duration:0.15f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                                                       cell.feedImage.image=image;
                                                       
                                                       
                                                   } completion:^(BOOL finished) {
                                                       ;
                                                   }];
                                                   
                                                   
                                               });

                                               
                                               
                                           }
                                           
                                       }];
                
            }

            
           
        });
        
        
    }
    
    cell.imageViewHeightConstraint.constant=[UIScreen mainScreen].bounds.size.width/singlePost.aspectRatio;
   
    NSLog(@"singlePost.price %lf",singlePost.price);
    cell.buyButton.tag=indexPath.section;
    if(singlePost.has_buy_btn)
    {
        cell.buyButtonHeightConstraint.constant=30;
        cell.buyButton.hidden=NO;
        
        if(singlePost.userId==[UserAccount sharedManager].userId)
        {
            cell.buyButton.alpha = 0.8;
            cell.buyButton.enabled=NO;
        }
        else{
            cell.buyButton.alpha = 1.0;
            cell.buyButton.enabled=YES;
            
        }
    }
    else
    {
        cell.buyButtonHeightConstraint.constant=0;
        cell.buyButton.hidden=YES;
    }
    

    //single tap
    UITapGestureRecognizer* postImageTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnPostImage:)];
    postImageTapGesture.numberOfTapsRequired = 1;
    [cell.feedImage addGestureRecognizer:postImageTapGesture];
    cell.feedImage.userInteractionEnabled = YES;
    
    //double tap
    UITapGestureRecognizer* postImageDoubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapOnPostImage:)];
    postImageDoubleTap.numberOfTapsRequired = 2;
    [cell.feedImage addGestureRecognizer:postImageDoubleTap];
    cell.feedImage.userInteractionEnabled = YES;
    [postImageTapGesture requireGestureRecognizerToFail:postImageDoubleTap];

  
    
    
    
//    [ cell. sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL,[[feedList objectAtIndex:indexPath.section] objectForKey:@"image"] ]]];
//    
    //set like count
    
    
    if (singlePost.postLikesCount>0) {
        
        [cell.likeButton setTitle:[NSString stringWithFormat:@"%i",singlePost.postLikesCount] forState:UIControlStateSelected];
        [cell.likeButton setTitle:[NSString stringWithFormat:@"%i",singlePost.postLikesCount] forState:UIControlStateNormal];
    }else
    {
        [cell.likeButton setTitle:@"" forState:UIControlStateNormal];
        [cell.likeButton setTitle:@"" forState:UIControlStateSelected];
    }
    
    
    if (singlePost.pinCount > 0) {
        
        [cell.pinButton setTitle:[NSString stringWithFormat:@"%i",singlePost.pinCount] forState:UIControlStateSelected];
        [cell.pinButton setTitle:[NSString stringWithFormat:@"%i",singlePost.pinCount] forState:UIControlStateNormal];
    }else
    {
        [cell.pinButton setTitle:@"" forState:UIControlStateNormal];
         [cell.pinButton setTitle:@"" forState:UIControlStateSelected];
    }
    

    cell.likeButton.tag=indexPath.section;
    
    cell.likeButton.selected=singlePost.is_liked;
   

    
    cell.pinButton.tag=indexPath.section;
    cell.pinButton.selected=singlePost.is_pinned;
    

    
    cell.descriptionLabel.text = singlePost.postDescription;
    [cell.descriptionLabel sizeToFit];


    

    cell.commentButton.tag=indexPath.section;
    
    cell.moreButton.tag = indexPath.section;
   
    
    if ( singlePost.postCommentsCount) {
        
       // [cell.commentButton setTitle:[NSString stringWithFormat:@"%lu comments",(unsigned long)commentArray.count] forState:UIControlStateSelected];
        [cell.commentButton setTitle:[NSString stringWithFormat:@"%i",singlePost.postCommentsCount] forState:UIControlStateNormal];
    }else{
    
        [cell.commentButton setTitle:@"" forState:UIControlStateNormal];
    }
    

    
    PatternTapResponder hashTagTapAction = ^(NSString *tappedString){
        NSLog(@"You have tapped hashTag: %@",tappedString);
        [self moveToHashTagExploreView:tappedString];
    };
    PatternTapResponder userTapAction = ^(NSString *tappedString){
        NSLog(@"You have tapped user: %@",tappedString);
        [self moveToUserView:tappedString];
    };
    
    cell.descriptionLabel.userInteractionEnabled=YES;
    [cell.descriptionLabel enableHashTagDetectionWithAttributes:@{NSForegroundColorAttributeName:[UIColor blueColor],
                                                                  RLHighlightedBackgroundColorAttributeName:[UIColor clearColor],NSBackgroundColorAttributeName:[UIColor clearColor],RLHighlightedBackgroundCornerRadius:@5,
                                                                  RLTapResponderAttributeName:hashTagTapAction}];
    
    [cell.descriptionLabel enableUserHandleDetectionWithAttributes:@{NSForegroundColorAttributeName:[UIColor blueColor],
                                                                     RLHighlightedBackgroundColorAttributeName:[UIColor clearColor],NSBackgroundColorAttributeName:[UIColor clearColor],RLHighlightedBackgroundCornerRadius:@5,
                                                                     RLTapResponderAttributeName:userTapAction}];

    

    

    self.taplikeLabel = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnLikeLabel:)];
    
    [cell.likeButton addGestureRecognizer:self.taplikeLabel];

    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
   
    return 58;
    
}


#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    

    
    
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if(selectedTab!=4 && selectedTab!=5)
    {
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    // Change 10.0 to adjust the distance from bottom
 
        if (maximumOffset - currentOffset <= 40.0 && !islastItemReached && self.refreshControl.hidden) {
            NSLog(@"true");
            
            [self makeRequestwithPage];
        }
    }
    
}

//-(void) changeConstraintForNoLike:(FeedTableViewCell*)cell {
//
//    cell.likeCountLabel.text = @"";
//    cell.likeIconHeight.constant = 0;
//    cell.likeIconWidth.constant = 0;
//    cell.likeIconTop.constant = 0;
//    cell.constraintBtLikeIconAndDesView.constant = 0;
//    
//    cell.commentCountLeading.constant = -5;
//
//}




-(void)tapOnPostImage:(UITapGestureRecognizer *)recognizer
{
    if(recognizer.view.subviews.count)
    {
        for (UIView *v in recognizer.view.subviews) {
            [v removeFromSuperview];
        }
    }
    else
        [self setupTaggedUserView:[[feedList objectAtIndex:recognizer.view.tag] objectForKey:@"tagged_users"] inImageView:recognizer.view];
}

-(void)doubleTapOnPostImage:(UITapGestureRecognizer *)recognizer
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:recognizer.view.tag];
  
    FeedTableViewCell *cell = (FeedTableViewCell*) [self.feedTableView cellForRowAtIndexPath:indexPath];
    
    [self performSelector:@selector(likeButtonAction:) withObject:cell.likeButton afterDelay:0];
    
}



-(void) setupTaggedUserView:(NSMutableArray*) taggedUsers inImageView:(UIView*)targetedImage
{
    
    
    for (int i=0; i<taggedUsers.count; i++) {
        
        NSLog(@"targetedImage %ld",(long)targetedImage.tag);
        
        UIFont *font = [UIFont fontWithName:@"AzoSans-Regular" size:12];
        NSDictionary *userAttributes = @{NSFontAttributeName: font,
                                         NSForegroundColorAttributeName: [UIColor blackColor]};
        
        NSString *text =[[taggedUsers objectAtIndex:i] objectForKey:@"username"];
        const CGSize textSize = [text sizeWithAttributes: userAttributes];
        
        //NSLog(@"width = %f, height = %f", textSize.width, textSize.height);
        //
        float xposition=[[[taggedUsers objectAtIndex:i] objectForKey:@"x"] floatValue];
        float yposition=[[[taggedUsers objectAtIndex:i] objectForKey:@"y"]floatValue ];
        
       // NSLog(@"relative X position:%f,Y position %f",xposition,yposition);
        NSLog(@"relative %@",[taggedUsers objectAtIndex:i]);
        
        
        UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(xposition*targetedImage.frame.size.width - textSize.width/2, yposition*targetedImage.frame.size.height, textSize.width +10, 30)];
        fromLabel.text = text;
        fromLabel.font = [UIFont fontWithName:@"AzoSans-Regular" size:12];
        fromLabel.numberOfLines = 1;
        fromLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines; // or UIBaselineAdjustmentAlignCenters, or UIBaselineAdjustmentNone
        fromLabel.adjustsFontSizeToFitWidth = YES;
        fromLabel.adjustsLetterSpacingToFitWidth = YES;
        fromLabel.minimumScaleFactor = 10.0f/12.0f;
        fromLabel.clipsToBounds = YES;
        fromLabel.layer.cornerRadius=3.0;
        fromLabel.backgroundColor = [UIColor blackColor];
        fromLabel.textColor = [UIColor whiteColor];
        fromLabel.textAlignment = NSTextAlignmentCenter;
        fromLabel.userInteractionEnabled=YES;
        fromLabel.tag=[[[taggedUsers objectAtIndex:i] objectForKey:@"user_id"] integerValue];
        
//        UIImageView *arrowIcon = [[UIImageView alloc]initWithFrame:CGRectMake(textSize.width+10 -5, yPosition +10, 10, 10)];
//        arrowIcon.image = [UIImage imageNamed:@"Arrow"];
//        [fromLabel addSubview:arrowIcon];
//        
        [targetedImage addSubview:fromLabel];
        
        UITapGestureRecognizer* labelTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnTaggedLabel:)];
        labelTapGesture.numberOfTapsRequired = 1;
        [fromLabel addGestureRecognizer:labelTapGesture];
        
    }
    
    //get text size
    
}

//likecount label click action

-(void)tapOnLikeLabel:(UILongPressGestureRecognizer *)recognizer{
    
    if (recognizer.state == UIGestureRecognizerStateChanged)
    {
        NSLog(@"Change");
    }
    else if (recognizer.state == UIGestureRecognizerStateEnded) {
        NSLog(@"Long press Ended");
        
        
        
    } else if (recognizer.state == UIGestureRecognizerStateBegan) {
        NSLog(@"Long press detected.");
        LikersViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"LikersViewController"];
        controller.postId =[[[feedList objectAtIndex:recognizer.view.tag ] objectForKey:@"id"]intValue];
        
        [self.navigationController pushViewController:controller animated:YES];
    }
    
    
    
}


//comment label click action

-(void)tapOnCommentLabel:(UITapGestureRecognizer *)recognizer{

    
    //Warning need to update
    //NSMutableArray* tempCommentArray = [[NSMutableArray alloc]initWithArray:[[feedList objectAtIndex:[recognizer view].tag] objectForKey:@"post_comments"]];

    CommentViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentViewController"];

   
    controller.postId = [[[feedList objectAtIndex:recognizer.view.tag ] objectForKey:@"id"]intValue];
    controller.postBelongsToUserId =[[[[feedList objectAtIndex:recognizer.view.tag ] objectForKey:@"owner"] objectForKey:@"id"] intValue];
    
    NSLog(@"comments in home %@",controller.commentList);
   
    
    [self.navigationController pushViewController:controller animated:YES];


}

//cell header click action

-(void)tapHeader:(UITapGestureRecognizer *)recognizer{
    
    NSLog(@"tap on header %@",[feedList objectAtIndex:recognizer.view.tag ]);
    
    ProfileViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    
    controller.isUserOrFollower = YES;
    controller.userId = [[[[feedList objectAtIndex:recognizer.view.tag ] objectForKey:@"owner"] objectForKey:@"id"]intValue];
    
    [self.navigationController pushViewController:controller animated:YES];
    
}

-(void)tapOnTaggedLabel:(UITapGestureRecognizer *)recognizer{
    
    NSLog(@"tap on label");
    
    ProfileViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    
    controller.isUserOrFollower = YES;
    controller.userId = (int)recognizer.view.tag;
    
    [self.navigationController pushViewController:controller animated:YES];
    
}

-(void)tapOnArtistName:(UITapGestureRecognizer *)recognizer{
    
    NSLog(@"tap on artist");
    
    UILabel *artistLabel=(UILabel*)[recognizer view];
    
    ArtistPostViewController * htController = [self.storyboard instantiateViewControllerWithIdentifier:@"ArtistPostViewController"];
    
    htController.artistName = artistLabel.text;
    [self.navigationController pushViewController:htController animated:YES];
    
    
}

-(void)tapOnArtistNameInView:(UITapGestureRecognizer *)recognizer{
    
    NSLog(@"tap on artist");
    
    
    ArtistPostViewController * htController = [self.storyboard instantiateViewControllerWithIdentifier:@"ArtistPostViewController"];
    
    htController.artistName = [[feedList objectAtIndex:[recognizer view].tag] objectForKey:@"artist"];
    [self.navigationController pushViewController:htController animated:YES];
    
    
}



- (IBAction)likeButtonAction:(UIButton*)sender {
    
    NSLog(@"like button clicked");
    sender.userInteractionEnabled = NO;
    
    
    sender.selected=!sender.selected;
    
    NSMutableDictionary *temDic = [[NSMutableDictionary alloc]initWithDictionary:[feedList objectAtIndex:sender.tag]];
    [temDic removeObjectForKey:@"is_liked"];
    [temDic setObject:[NSNumber numberWithBool:sender.selected] forKey:@"is_liked"];
    
    
    int counter;
    if(sender.selected)
        counter=[[temDic objectForKey:@"like_count"] intValue]+1;
    else
        counter=[[temDic objectForKey:@"like_count"] intValue]-1;
    
    [temDic removeObjectForKey:@"like_count"];
    [temDic setObject:[NSNumber numberWithInt:counter] forKey:@"like_count"];
    
    [feedList replaceObjectAtIndex:sender.tag withObject:temDic];

    
    UserPost *singlePost=[UserPost new];
    singlePost=[feedDataSource objectAtIndex:sender.tag];
    
    singlePost.is_liked=[[temDic objectForKey:@"is_liked"] intValue];
    singlePost.postLikesCount=[[temDic objectForKey:@"like_count"] intValue];
    
    [feedDataSource replaceObjectAtIndex:sender.tag withObject:singlePost];
    
    
    [self.feedTableView reloadData];
    
    if(!sender.selected)
    {
        
        [[ServerManager sharedManager] deleteLikeForPostId:[[[feedList objectAtIndex:sender.tag] objectForKey:@"id"] intValue] withcompletion:^(BOOL success) {
          
            sender.userInteractionEnabled = YES;
            
            if (success) {
                
                
            }
            else{
                sender.selected=!sender.selected;
                
                NSMutableDictionary *temDic = [[NSMutableDictionary alloc]initWithDictionary:[feedList objectAtIndex:sender.tag]];
                [temDic removeObjectForKey:@"is_liked"];
                [temDic setObject:[NSNumber numberWithBool:sender.selected] forKey:@"is_liked"];
                
                int counter;
                if(sender.selected)
                    counter=[[temDic objectForKey:@"like_count"] intValue]+1;
                else
                    counter=[[temDic objectForKey:@"like_count"] intValue]-1;
                
                [temDic removeObjectForKey:@"like_count"];
                [temDic setObject:[NSNumber numberWithInt:counter] forKey:@"like_count"];
                
                
                [feedList replaceObjectAtIndex:sender.tag withObject:temDic];
                
                UserPost *singlePost=[UserPost new];
                singlePost=[feedDataSource objectAtIndex:sender.tag];
                
                singlePost.is_liked=[[temDic objectForKey:@"is_liked"] intValue];
                singlePost.postLikesCount=[[temDic objectForKey:@"like_count"] intValue];
                
                [feedDataSource replaceObjectAtIndex:sender.tag withObject:singlePost];
                
                [self.feedTableView reloadData];

                
            }
            
        }];
    }
    else
    {
        
        [[ServerManager sharedManager] postLikePostId:[[[feedList objectAtIndex:sender.tag] objectForKey:@"id"] intValue] withcompletion:^(BOOL success) {
            
            sender.userInteractionEnabled = YES;
            
            if (success) {
                
                
            }
            else{
                sender.selected=!sender.selected;
                
                NSMutableDictionary *temDic = [[NSMutableDictionary alloc]initWithDictionary:[feedList objectAtIndex:sender.tag]];
                [temDic removeObjectForKey:@"is_liked"];
                [temDic setObject:[NSNumber numberWithBool:sender.selected] forKey:@"is_liked"];
                
                int counter;
                if(sender.selected)
                    counter=[[temDic objectForKey:@"like_count"] intValue]+1;
                else
                    counter=[[temDic objectForKey:@"like_count"] intValue]-1;
                
                [temDic removeObjectForKey:@"like_count"];
                [temDic setObject:[NSNumber numberWithInt:counter] forKey:@"like_count"];
                
                
                [feedList replaceObjectAtIndex:sender.tag withObject:temDic];
                
                UserPost *singlePost=[UserPost new];
                singlePost=[feedDataSource objectAtIndex:sender.tag];
                
                singlePost.is_liked=[[temDic objectForKey:@"is_liked"] intValue];
                singlePost.postLikesCount=[[temDic objectForKey:@"like_count"] intValue];
                
                [feedDataSource replaceObjectAtIndex:sender.tag withObject:singlePost];
                
                [self.feedTableView reloadData];
                
                
            }
            
        }];
    }
}

- (IBAction)commentButtonAction:(UIButton*)sender {
    //Warning need to update
    
    pageIndex--;
    //NSMutableArray* tempCommentArray = [[NSMutableArray alloc]initWithArray:[[feedList objectAtIndex:sender.tag] objectForKey:@"post_comments"]];
    
    CommentViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentViewController"];
    
    //controller.commentList = tempCommentArray;
    controller.postId = [[[feedList objectAtIndex:sender.tag ] objectForKey:@"id"]intValue];
    controller.postBelongsToUserId =[[[[feedList objectAtIndex:sender.tag ] objectForKey:@"owner"]objectForKey:@"id"] intValue];
    NSLog(@"comments in home %d",controller.postBelongsToUserId);
    
    
    [self.navigationController pushViewController:controller animated:YES];
    
    
}

- (IBAction)sendButtonAction:(id)sender {
    
    
}
- (IBAction)pinButtonAction:(UIButton*)sender {
    
    
    NSLog(@"Pin button clicked");
    sender.userInteractionEnabled = NO;
    sender.selected=!sender.selected;
    
    NSMutableDictionary *temDic = [[NSMutableDictionary alloc]initWithDictionary:[feedList objectAtIndex:sender.tag]];
    [temDic removeObjectForKey:@"is_pinned"];
    [temDic setObject:[NSNumber numberWithBool:sender.selected] forKey:@"is_pinned"];
    
    int pinCount=[[temDic objectForKey:@"pin_count"]intValue];
    
    if(sender.isSelected)
    {
        [temDic setObject:[NSNumber numberWithInt:pinCount+1] forKey:@"pin_count"];
        
        NSLog(@"slected %i",[[temDic objectForKey:@"pin_count"]intValue]);
        
    }else
    {
        
        [temDic setObject:[NSNumber numberWithInt:pinCount-1] forKey:@"pin_count"];
        NSLog(@"not slected %i",[[temDic objectForKey:@"pin_count"]intValue]);
        
    }
    
    
    [feedList replaceObjectAtIndex:sender.tag withObject:temDic];
    
    UserPost *singlePost=[UserPost new];
    singlePost=[feedDataSource objectAtIndex:sender.tag];
    
    singlePost.is_pinned=[[temDic objectForKey:@"is_pinned"] intValue];
    singlePost.pinCount=[[temDic objectForKey:@"pin_count"] intValue];
    
    [feedDataSource replaceObjectAtIndex:sender.tag withObject:singlePost];
    
    
    
    [self.feedTableView reloadData];
    
    if(!sender.selected)
    {
        
        [[ServerManager sharedManager] deletePinForPostId:[[[feedList objectAtIndex:sender.tag] objectForKey:@"id"] intValue] withcompletion:^(BOOL success) {
            
            sender.userInteractionEnabled = YES;
            
            if (success) {
                
              //  [self.feedTableView reloadData];
                
            }
            else{
                sender.userInteractionEnabled = YES;
                
                sender.selected=!sender.selected;
                
                NSMutableDictionary *temDic = [[NSMutableDictionary alloc]initWithDictionary:[feedList objectAtIndex:sender.tag]];
                [temDic removeObjectForKey:@"is_pinned"];
                [temDic setObject:[NSNumber numberWithBool:sender.selected] forKey:@"is_pinned"];
                
                if(sender.selected)
                {
                    [temDic setObject:[NSNumber numberWithInt:[[temDic objectForKey:@"pin_count"]intValue]+1] forKey:@"pin_count"];
                    
                }else
                {
                    [temDic setObject:[NSNumber numberWithInt:[[temDic objectForKey:@"pin_count"]intValue]-1] forKey:@"pin_count"];
                    
                }
                
                
                
                [feedList replaceObjectAtIndex:sender.tag withObject:temDic];
                
                
                UserPost *singlePost=[UserPost new];
                singlePost=[feedDataSource objectAtIndex:sender.tag];
                
                singlePost.is_pinned=[[temDic objectForKey:@"is_pinned"] intValue];
                singlePost.pinCount=[[temDic objectForKey:@"pin_count"] intValue];
                
                [feedDataSource replaceObjectAtIndex:sender.tag withObject:singlePost];
                
                
                [self.feedTableView reloadData];

                
                
            }
            
        }];
    }
    else
    {
        
        [[ServerManager sharedManager] postPinForPostId:[[[feedList objectAtIndex:sender.tag] objectForKey:@"id"] intValue] withcompletion:^(BOOL success) {
            
            sender.userInteractionEnabled = YES;
            
            if (success) {
                
                //[self.feedTableView reloadData];
                
            }
            else{
                sender.userInteractionEnabled = YES;
                
                sender.selected=!sender.selected;
                
                NSMutableDictionary *temDic = [[NSMutableDictionary alloc]initWithDictionary:[feedList objectAtIndex:sender.tag]];
                [temDic removeObjectForKey:@"is_pinned"];
                [temDic setObject:[NSNumber numberWithBool:sender.selected] forKey:@"is_pinned"];
                
                if(sender.selected)
                {
                    [temDic setObject:[NSNumber numberWithInt:[[temDic objectForKey:@"pin_count"]intValue]+1] forKey:@"pin_count"];
                    
                }else
                {
                    [temDic setObject:[NSNumber numberWithInt:[[temDic objectForKey:@"pin_count"]intValue]-1] forKey:@"pin_count"];
                    
                }
                
                
                
                [feedList replaceObjectAtIndex:sender.tag withObject:temDic];
                
                
                UserPost *singlePost=[UserPost new];
                singlePost=[feedDataSource objectAtIndex:sender.tag];
                
                singlePost.is_pinned=[[temDic objectForKey:@"is_pinned"] intValue];
                singlePost.pinCount=[[temDic objectForKey:@"pin_count"] intValue];
                
                [feedDataSource replaceObjectAtIndex:sender.tag withObject:singlePost];
                
                
                [self.feedTableView reloadData];
                
                
            }
            
        }];
    }
    
}

-(NSMutableAttributedString*) getComentsTextConfigured:(NSString*)user toComment:(NSString*)comment
{
    UIFont *commentFont = [UIFont fontWithName:@"AzoSans-Regular" size:12];
    UIFont *userFont = [UIFont fontWithName:@"AzoSans-Medium" size:13];
    
    NSDictionary *userDict = [NSDictionary dictionaryWithObject: userFont forKey:NSFontAttributeName];
    NSDictionary *commentDict = [NSDictionary dictionaryWithObject:commentFont forKey:NSFontAttributeName];
    
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:user attributes: userDict];
    
    NSMutableAttributedString *vAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",comment] attributes:commentDict];

    
    [aAttrString appendAttributedString:vAttrString];
    
    return aAttrString;
}

-(void)updateBadgeValue
{
    [badgeView setBadgeValue: [UserAccount sharedManager].pendingMessageCount];
    if([UserAccount sharedManager].pendingMessageCount==0)
        [badgeView setHidden:YES];
    else
         [badgeView setHidden:NO];

}

- (IBAction)moveToInbox:(id)sender {
  
    [UserAccount sharedManager].pendingMessageCount=0;
    
    [badgeView setBadgeValue: [UserAccount sharedManager].pendingMessageCount];
    [badgeView setHidden:YES];
    
    
    
    ExistingMessageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ExistingMessageViewController"];
    
    UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:controller];
    navCon.navigationBar.hidden=YES;
    
    [self presentViewController:navCon animated:YES completion:nil];
    
    
//    MessagingViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"MessagingViewController"];
//    [self.navigationController pushViewController:controller animated:YES];
//    
}
- (IBAction)buyButtonAction:(UIButton*)sender {
    
    UserPost *singlePost=[UserPost new];
    singlePost=[feedDataSource objectAtIndex:sender.tag];
    

        NSString *alertMessage;
        
        if (singlePost.price) {
            
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
            [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
            // NSNumber *number = [formatter numberFromString:];
            
            
            NSString *formattedString = [formatter stringFromNumber:[NSNumber numberWithFloat:singlePost.price]];
            
            alertMessage=[NSString stringWithFormat:@"Hi, I am interested in this work, which is priced at US$%@. Please can you let me know if it is available and provide further details. Thank you.",formattedString];
            
        }
        else
            alertMessage=@"Hi, I am interested in this work. Please can you let me know if it is available and provide further details";
        
        ZHPopupView *popupView = [ZHPopupView popUpDialogViewInView:nil
                                                            iconImg:[UIImage imageNamed:@"correct_icon"]
                                                    backgroundStyle:ZHPopupViewBackgroundType_SimpleOpacity
                                                              title:@"Send Message"
                                                            content:alertMessage
                                                       buttonTitles:@[@"Cancel", @"Send"]
                                                confirmBtnTextColor:[UIColor whiteColor] otherBtnTextColor:[UIColor whiteColor]
                                                 buttonPressedBlock:^(NSInteger btnIdx) {
                                                     
                                                     if(btnIdx==1)
                                                     {
                                                         NSLog(@"sending message");
                                                         [self sendMessages:alertMessage forPost:singlePost];
                                                     }
                                                     
                                                     
                                                 }];
        [popupView present];

}


-(void) sendMessages: (NSString *) message forPost:(UserPost*)post
{
    
    
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    [postData setObject:[NSNumber numberWithInt:post.userId] forKey:@"receiver_id"];
    [postData setObject:message forKey:@"message"];
   //for post share
    if([message isEqualToString:@"Attachment"])
    {
        [postData setObject:[NSNumber numberWithInt:post.postId] forKey:@"is_post"];
        [postData setObject:post.postImageUrl forKey:@"url"];
        
        
    }
    else
    {
        [postData setObject:@"0" forKey:@"is_post"];
        [postData setObject:@"" forKey:@"url"];
    }
    
    
    [postData setObject:@"0" forKey:@"is_file"];
    
     [[ServerManager sharedManager] postSingleMessageWithData:postData withCompletion:^(BOOL success) {
        
        
        if (success) {
            if(![message isEqualToString:@"Attachment"])
                [self sendMessages:@"Attachment" forPost:post];
        }
        
        
    }];


    
}



- (IBAction)moreButtonAction:(UIButton *)sender {
    
    
    NSLog(@"%ld",(long)sender.tag);
    
     sharePostDictionary = [[NSMutableDictionary alloc]initWithDictionary:[feedList objectAtIndex:sender.tag]];
    shareUser=[UserPost new];
    shareUser=[feedDataSource objectAtIndex:sender.tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    
    FeedTableViewCell *cell = (FeedTableViewCell*) [self.feedTableView cellForRowAtIndexPath:indexPath];
    imageViewForShare=cell.feedImage;

    
    if ([[[sharePostDictionary objectForKey:@"owner"] objectForKey:@"id"] integerValue] == [UserAccount sharedManager].userId) {
        
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                             destructiveButtonTitle:nil
                                                  otherButtonTitles:NSLocalizedString(@"Delete", nil),NSLocalizedString(@"Edit", nil),NSLocalizedString(@"Share", nil),NSLocalizedString(@"Share via Whatsapp", nil),NSLocalizedString(@"Send via message", nil), nil];
        
        
        sheet.tag = 1;

//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//            //  device is an iPad.
//          [sheet showFromRect:CGRectMake(([UIScreen mainScreen].bounds.size.width)/3, ([UIScreen mainScreen].bounds.size.height)/2, 200, 200) inView:self.view animated:YES];
//        }
//        else{
        
          [sheet showInView:self.view];
        //}
        
    }else{
    
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                             destructiveButtonTitle:nil
                                                  otherButtonTitles:NSLocalizedString(@"Report user", nil),NSLocalizedString(@"Share to Facebook", nil),NSLocalizedString(@"Share to Instagram", nil),NSLocalizedString(@"Share via Whatsapp", nil), NSLocalizedString(@"Send via message", nil),nil];
        
        
        sheet.tag = 2;
        
//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//            //  device is an iPad.
//            //window.rootViewController = [[UIViewController alloc] init];
//            
//            [sheet showFromRect:CGRectMake(([UIScreen mainScreen].bounds.size.width)/3, ([UIScreen mainScreen].bounds.size.height)/2, 200, 200) inView:self.view animated:YES];
//
//        }
//        else{
        
            [sheet showInView:self.view];
       // }
    }
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (actionSheet.tag == 1) {
        
        if (buttonIndex == 0) {
            
            NSLog(@"dic  %@",sharePostDictionary);
            
            [[ServerManager sharedManager] deletePostForPostId:[[sharePostDictionary objectForKey:@"id"] intValue] withcompletion:^(BOOL success) {
                
                if (success) {
                    
                      if(feedDataSource.count)
                        [feedDataSource removeObject:shareUser];
                    
                    [feedList removeObject:sharePostDictionary];
                    [self.feedTableView reloadData];
                    
                }
                else{
                    [self.feedTableView reloadData];
                    
                    
                    
                }
                
            }];
            
        }else if (buttonIndex == 1)
        {
            
            
            SharePhotoViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SharePhotoViewController"];
            controller.selectedImageForShare=imageViewForShare.image;
            controller.isForEdit=YES;
            controller.isForShare=NO;
            controller.postDetails=sharePostDictionary;
            //[self presentViewController:controller animated:NO completion:nil];
            
            [self.navigationController pushViewController:controller animated:YES];
            
            
        }else if (buttonIndex == 2)
        {
            
            SharePhotoViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SharePhotoViewController"];
            controller.selectedImageForShare=imageViewForShare.image;
            controller.isForShare=YES;
            controller.isForEdit=NO;
            controller.postDetails=sharePostDictionary;
            //[self presentViewController:controller animated:NO completion:nil];
            
            [self.navigationController pushViewController:controller animated:YES];
        }
        else if (buttonIndex == 3)
        {
            if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"whatsapp://app"]]){
                
                UIImage     * iconImage = imageViewForShare.image;
                NSString    * savePath  = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/whatsAppTmp.wai"];
                
                [UIImageJPEGRepresentation(iconImage, 1.0) writeToFile:savePath atomically:YES];
                
                self.documentController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:savePath]];
                self.documentController.UTI = @"net.whatsapp.image";
                self.documentController.delegate = self;
                
                [self.documentController presentOpenInMenuFromRect:CGRectMake(0, 0, 0, 0) inView:self.view animated: YES];
                
                
            } else {
                
                UIAlertController * alertController=   [UIAlertController
                                                        alertControllerWithTitle:@"WhatsApp not installed."
                                                        message:@"Your device has no WhatsApp installed."
                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action) {
                                                               
                                                               
                                                               
                                                           }];
               
                
                [alertController addAction:ok];
                
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }
        else if (buttonIndex == 4)
        {
            
            MessagingViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"MessagingViewController"];
            controller.dataPayload=[[NSMutableDictionary alloc] initWithDictionary:sharePostDictionary];
            
            [UIPasteboard generalPasteboard].image=imageViewForShare.image;
            UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:controller];
            navCon.navigationBar.hidden=YES;
            
            [self presentViewController:navCon animated:YES completion:nil];
            
            
        }
        
    }
    else{
        
//        UIImageView *postImage = [[UIImageView alloc]init];
//        
//        [postImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL,[sharePostDictionary objectForKey:@"image"]]]];

       // NSLog(@"sharePostDictionary %@ %@",sharePostDictionary,postImage.image);
        
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        
        pasteboard.string = [NSString stringWithFormat:@"%@\n%@",[sharePostDictionary objectForKey:@"description"],[sharePostDictionary objectForKey:@"hashtags"]];
        
        
        if (buttonIndex == 0) {
            
            //[self performSelector:@selector(secondMethod) withObject:nil afterDelay:1.0 ];
            
            
            [[ServerManager sharedManager] postReportAgainstUserId:[[[sharePostDictionary objectForKey:@"owner"]objectForKey:@"id"] intValue] WithCompletion:^(BOOL success) {
                
                if (success) {
                    
                    //  [self.feedTableView reloadData];
                   
                    
                    UIAlertController * alertController=   [UIAlertController
                                                            alertControllerWithTitle:nil
                                                            message:@"Your report has been submitted."
                                                            preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action) {
                                                                   
                                                                   
                                                                   
                                                               }];
                    
                    
                    [alertController addAction:ok];
                    
                    [self presentViewController:alertController animated:YES completion:nil];

                }
                else{
                    
//                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
//                                                                    message:@"You have already reported this user."
//                                                                   delegate:nil
//                                                          cancelButtonTitle:@"OK"
//                                                          otherButtonTitles:nil];
//                    [alert show];
                }
                
            }];
            
            
            
            
        }
//        else if (buttonIndex == 1)
//        {
//        //Block user
//            
//            AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
//            apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
//            
//            NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
//            [postData setObject:ACCESS_KEY forKey:@"access_key"];
//            [postData setObject:[NSNumber numberWithInt:[[sharePostDictionary objectForKey:@"user_id"] intValue]] forKey:@"suspect_id"] ;
//            [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"user_id"] ;
//            
//          //  NSLog(@"postData %@",postData);
//            
//            [apiLoginManager POST:[NSString stringWithFormat:@"%@/users/api-swap-block-user",SERVER_BASE_API_URL] parameters:postData
//                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                              
//                              //NSLog(@"response object %@",responseObject);
//                              
//                              if ([[responseObject objectForKey:@"success"] intValue] == 1 ) {
//                                  
//                                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
//                                                                                  message:[NSString stringWithFormat:@"%@ has been blocked from being able to access your profile and posts. You will also no longer appear in any searches initiated by %@.",[sharePostDictionary objectForKey:@"user_id"],[sharePostDictionary objectForKey:@"user_id"]]
//                                                                                 delegate:nil
//                                                                        cancelButtonTitle:@"OK"
//                                                                        otherButtonTitles:nil];
//                                  [alert show];
//                              }
//                              
//                              else {
//                                  
//                                }
//                              
//                          }
//                          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                              
//                              NSLog(@"Error: %@", error);
//                              
//                              
//                              NSLog(@"responseString: %@", operation.responseString);
//                              
//                              
//                              
//                          }];
//
//        }
        
        else if (buttonIndex == 1)
        {
            FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
            photo.image = imageViewForShare.image;
            photo.userGenerated = YES;
            photo.caption = [NSString stringWithFormat:@"%@",[sharePostDictionary objectForKey:@"description"]];
            FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
            content.photos = @[photo];
            [FBSDKShareDialog showFromViewController:self
                                         withContent:content
                                            delegate:nil];
            
           // NSLog(@"photo %@",photo);
            
           // NSLog(@"post dictionary  %@",sharePostDictionary);
            
        }else if (buttonIndex == 2)
        {
            
           
            NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
            if([[UIApplication sharedApplication] canOpenURL:instagramURL]) //check for App is install or not
            {
                NSData *imageData = UIImagePNGRepresentation(imageViewForShare.image); //convert image into .png format.
                NSFileManager *fileManager = [NSFileManager defaultManager];//create instance of NSFileManager
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //create an array and store result of our search for the documents directory in it
                NSString *documentsDirectory = [paths objectAtIndex:0]; //create NSString object, that holds our exact path to the documents directory
                NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"insta.igo"]]; //add our image to the path
                [fileManager createFileAtPath:fullPath contents:imageData attributes:nil]; //finally save the path (image)
                NSLog(@"image saved");
                
                
                
                
                CGRect rect = CGRectMake(0 ,0 , 0, 0);
                UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, self.view.opaque, 0.0);
                [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
                UIGraphicsEndImageContext();
                NSString *fileNameToSave = [NSString stringWithFormat:@"Documents/insta.igo"];
                NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:fileNameToSave];
                NSLog(@"jpg path %@",jpgPath);
                //            NSString *newJpgPath = [NSString stringWithFormat:@"file://%@",jpgPath];
                //            NSLog(@"with File path %@",newJpgPath);
                NSURL *igImageHookFile = [[NSURL alloc] initWithString:[[NSString alloc] initWithFormat:@"file://%@", jpgPath]];;
                NSLog(@"url Path %@",igImageHookFile);
                
                self.documentController.UTI = @"com.instagram.exclusivegram";
                // self.documentController.URL=[NSURL URLWithString:@"instagram://"];
                self.documentController = [self setupControllerWithURL:igImageHookFile usingDelegate:self];
                self.documentController=[UIDocumentInteractionController interactionControllerWithURL:igImageHookFile];
                NSString *caption = @"Add a caption"; //settext as Default Caption
                // self.documentController.annotation=[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",caption],@"InstagramCaption", nil];
                
                self.documentController.annotation=[NSDictionary dictionaryWithObject:caption forKey:@"InstagramCaption"];
                [self.documentController presentOpenInMenuFromRect:rect inView: self.view animated:YES];
                
                //[self.documentController presentOpenInMenuFromRect:CGRectMake(0, 0, 320, 480) inView:self.view animated:YES];
                
                //
                //            if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
                //                [[UIApplication sharedApplication] openURL:instagramURL];
                //            }
            }
            else
            {
                NSLog (@"Instagram not found");
            }
            
            
        }else if (buttonIndex == 3)
        {
            
            if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"whatsapp://app"]]){
                
                UIImage     * iconImage = imageViewForShare.image;
                NSString    * savePath  = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/whatsAppTmp.wai"];
                
                [UIImageJPEGRepresentation(iconImage, 1.0) writeToFile:savePath atomically:YES];
                
                self.documentController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:savePath]];
                self.documentController.UTI = @"net.whatsapp.image";
                self.documentController.delegate = self;
                
                [self.documentController presentOpenInMenuFromRect:CGRectMake(0, 0, 0, 0) inView:self.view animated: YES];
                
                
            } else {
                
//                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"WhatsApp not installed." message:@"Your device has no WhatsApp installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                [alert show];
                
                UIAlertController * alertController=   [UIAlertController
                                                        alertControllerWithTitle:@"WhatsApp not installed."
                                                        message:@"Your device has no WhatsApp installed."
                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action) {
                                                               
                                                               
                                                               
                                                           }];
                
                
                [alertController addAction:ok];
                
                [self presentViewController:alertController animated:YES completion:nil];
            }
            
        }
        else if (buttonIndex == 4)
        {
            
            MessagingViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"MessagingViewController"];
            controller.dataPayload=[[NSMutableDictionary alloc] initWithDictionary:sharePostDictionary];
            
            [UIPasteboard generalPasteboard].image=imageViewForShare.image;
            UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:controller];
            navCon.navigationBar.hidden=YES;
            
            [self presentViewController:navCon animated:YES completion:nil];

            
            
            
        }
    }
  
}

//temporary

-(IBAction)moveToHashTagExploreView:(NSString*)hashTag {
    
    NSMutableDictionary *hashTagTempDic=[[NSMutableDictionary alloc] init];
    [hashTagTempDic setObject:@"0" forKey:@"tagId"];
    [hashTagTempDic setObject:hashTag forKey:@"tagName"];

    
    HashTagViewViewController * htController = [self.storyboard instantiateViewControllerWithIdentifier:@"HashTagViewViewController"];
   
    htController.hashTagInfoDic = hashTagTempDic;
    [self.navigationController pushViewController:htController animated:YES];
    
}





-(IBAction)moveToUserView:(NSString*)userName {
    [self.activityIndicatorView startAnimating];
    
    
    [[ServerManager sharedManager] getUserByUserName:[userName substringFromIndex:1] withCompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
        
        [self.activityIndicatorView stopAnimating];
        
        if ( resultDataArray!=nil) {
            
            NSMutableDictionary *responsedic= [[resultDataArray dictionaryByReplacingNullsWithBlanks] mutableCopy];
            

            NSLog(@"responseDic %@",responsedic);
            
            
            ProfileViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
            
            controller.isUserOrFollower = YES;
            controller.userId = [[responsedic objectForKey:@"id"] intValue];
            
            [self.navigationController pushViewController:controller animated:YES];
        }
        else{
            
            
        }
        
    }];
    
//    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
//    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
//    
//    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
//    [postData setObject:ACCESS_KEY forKey:@"access_key"];
//    [postData setObject:[userName substringFromIndex:1] forKey:@"username"] ;
//    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
//    NSLog(@"postData %@",postData);
//    
//    [apiLoginManager POST:[NSString stringWithFormat:@"%@/users/api-get-user-id-by-username",SERVER_BASE_API_URL] parameters:postData
//                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                      
//                    //  NSLog(@"Response: %@", responseObject);
//                      
//                      
//                      [self.activityIndicatorView stopAnimating];
//                      
//                      NSString * successMsg = [responseObject objectForKey:@"success"];
//                      
//                      if ([successMsg integerValue] == 1 ) {
//                          
//                          NSLog(@"user_id %@",[responseObject objectForKey:@"user_id"]);
//                          
//                              ProfileViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
//                          
//                              controller.isUserOrFollower = YES;
//                              controller.userId = [[responseObject objectForKey:@"user_id"] intValue];
//                          
//                              [self.navigationController pushViewController:controller animated:YES];
//                          
//                          
//                      }
//                      
//                      
//                  }
//                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                      
//                      NSLog(@"Error: %@", error);
//                      
//                      [self.activityIndicatorView stopAnimating];
//                      
//                      
//                      //NSLog(@"%@",operation);
//                      
//                      
//                      
//                  }];
//    
  
}


- (UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate {
    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL: fileURL];
    interactionController.delegate = interactionDelegate;
    return interactionController;
}

- (IBAction)EventsTapped:(id)sender {
    
    self.topSelectedView.hidden=NO;
    self.bottomSelectedView.hidden=NO;
    self.newsLabel.hidden=YES;
    self.feedTableView.hidden = NO;
    
    pageIndex=1;
    selectedTab=1;
    
    self.comingSoonLabel.hidden=NO;
    feedList=[[NSMutableArray alloc] init];
    [self.feedTableView reloadData];

    
    
    [UIView animateWithDuration:0.5f animations:^{
        self.selectedTabXPosition.constant=0;
        self.artNewsButton.selected=NO;
        self.eventsButton.selected=YES;
        [self.view layoutIfNeeded];
    }];
}

- (IBAction)newsTapped:(id)sender {
    
    
    self.topSelectedView.hidden=NO;
    self.bottomSelectedView.hidden=NO;
    
    self.comingSoonLabel.hidden=YES;
    self.feedTableView.hidden = NO;
    
    pageIndex=1;
    selectedTab=2;
    self.newsLabel.hidden=NO;
    feedList=[[NSMutableArray alloc] init];
    [self.feedTableView reloadData];
    
    
    [UIView animateWithDuration:0.5f animations:^{
        self.selectedTabXPosition.constant=([UIScreen mainScreen].bounds.size.width)/2;
        self.artNewsButton.selected=YES;
        self.eventsButton.selected=NO;
        
        [self.view layoutIfNeeded];
    }];
}



-(NSDate *)getDateFromString:(NSString *)dateString
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy'-'MM'-'dd' 'HH':'mm':'ss"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    //NSLog(@"dateString %@", dateString);
    
    //[formatter setDateFormat:@"EEE, MMM dd, yyyy"];
    NSDate *currentDate=[formatter dateFromString:dateString];
    return currentDate;
    
}

-(NSString *)getStringFromDate:(NSDate *)date
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd, YYYY"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
   // NSLog(@"stringFromDate %@", stringFromDate);
    return stringFromDate;
}


- (NSString *)relativeDateStringForDate:(NSDate *)date
{
    
    NSCalendarUnit units = NSCalendarUnitDay | NSCalendarUnitWeekOfYear |
    NSCalendarUnitMonth | NSCalendarUnitYear|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond;
    
    // if `date` is before "now" (i.e. in the past) then the components will be positive
    NSDateComponents *components = [[NSCalendar currentCalendar] components:units
                                                                   fromDate:date
                                                                     toDate:[NSDate date]
                                                                    options:0];
    
    //NSLog(@"components %@",components);
    if (components.year > 0) {
        
        return [self getStringFromDate:date];
        
//        if (components.year > 1) {
//            return [NSString stringWithFormat:@"%ld years ago", (long)components.year];
//        } else {
//            return [NSString stringWithFormat:@"%ld year ago", (long)components.year];
//            
//        }
        
        
    } else if (components.month > 0) {
     
        if (components.month > 1) {
            return [self getStringFromDate:date];
            
           // return [NSString stringWithFormat:@"%ld months ago", (long)components.month];
        } else {
            return [NSString stringWithFormat:@"%ld month ago", (long)components.month];
            
        }
        
        
        
    } else if (components.weekOfYear > 0) {
        if (components.weekOfYear > 1) {
            return [NSString stringWithFormat:@"%ld weeks ago", (long)components.weekOfYear];
        } else {
            return [NSString stringWithFormat:@"%ld week ago", (long)components.weekOfYear];
        }
        
        
        
    } else if (components.day > 0) {
        if (components.day > 1) {
            return [NSString stringWithFormat:@"%ld days ago", (long)components.day];
        } else {
            return @"Yesterday";
        }
    } else {
        if (components.hour > 1) {
            return [NSString stringWithFormat:@"%ld hours ago", (long)components.hour];
        }
        else if(components.minute > 1){
            return [NSString stringWithFormat:@"%ld minutes ago", (long)components.minute];
        }
        else
        {
             // return [NSString stringWithFormat:@"less than a minutes"];
           return [NSString stringWithFormat:@"%ld seconds ago", (long)components.second];
        }
        
    }
}

#pragma mark - Pusher Delegate Connection
//////////////////////////////////

- (void)pusher:(PTPusher *)pusher connectionDidConnect:(PTPusherConnection *)connection
{
    NSLog(@"[Pusher] connected to %@", [connection.URL absoluteString]);
}

- (void)pusher:(PTPusher *)pusher connection:(PTPusherConnection *)connection failedWithError:(NSError *)error
{
    if (error) {
        NSLog(@"[Pusher] connection failed: %@", [error localizedDescription]);
    } else {
        NSLog(@"[Pusher] connection failed");
    }
}

- (void)pusher:(PTPusher *)pusher connection:(PTPusherConnection *)connection didDisconnectWithError:(NSError *)error willAttemptReconnect:(BOOL)reconnect
{
    if (error) {
        NSLog(@"[Pusher] didDisconnectWithError: %@ willAttemptReconnect: %@", [error localizedDescription], (reconnect ? @"YES" : @"NO"));
    } else {
        NSLog(@"[Pusher] disconnected");
    }
}

-(void)dealloc{
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

//-(UIImage *)imageManager:(SDWebImageManager *)imageManager transformDownloadedImage:(UIImage *)image withURL:(NSURL *)imageURL
//{
//   // NSLog(@"imageURL : %@",imageURL);
//    UIImage *resizedImage;
//  //  return resizedImage  = [image resizedImage:CGSizeMake(512, 512) interpolationQuality:kCGInterpolationDefault];
//    
//    float MIN_UPLOAD_RESOLUTION=756*756;
//    float factor;
//    float resol = image.size.height*image.size.width;
//    if (resol >MIN_UPLOAD_RESOLUTION){
//        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
//        resizedImage = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
//        return resizedImage;
//
//    }
//    else
//        return image;
//}
//
//- (UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
//    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
//    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
//    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return scaledImage;
//}

@end
