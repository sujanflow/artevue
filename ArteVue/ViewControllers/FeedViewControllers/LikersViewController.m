//
//  LikersViewController.m
//  Artegram
//
//  Created by Sujan on 7/21/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "LikersViewController.h"
#import "Constants.h"
#import "HexColors.h"
#import "ProfileViewController.h"
#import "UserAccount.h"
#import "ServerManager.h"
#import "NSDictionary+NullReplacement.h"
#import "LikeListTableViewCell.h"

@interface LikersViewController ()
{
    NSMutableArray *likersList;
    int pageIndex;
    
}
@end

@implementation LikersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    pageIndex=0;
    self.likersTableView.delegate = self;
    self.likersTableView.dataSource = self;
    
    self.likersTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.likersTableView.frame.size.width, 1)];

    
    NSLog(@"post id %d",self.postId);
    
    if (self.isFollower) {
        
        self.navigationHeaderLabel.text = @"Followers";
        
    }else if (self.isFolloweing)
    {
    
        self.navigationHeaderLabel.text = @"Following";
        
    }else
    {
    
        self.navigationHeaderLabel.text = @"Likers";
    
    }
    
    
  //  [SDWebImageManager sharedManager].delegate=self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    if (self.isFollower)
    {
        [self followersRequest];
        
    }else if (self.isFolloweing)
    {
       [self followingRequest];
    }else
    {
      [self makeRequest];
    }
}

-(void) makeRequest {
    
    [[ServerManager sharedManager] getLikeListForPost:self.postId inPage:pageIndex WithCompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
        if ( resultDataArray!=nil) {
            
            NSMutableDictionary *responsedic= [[resultDataArray dictionaryByReplacingNullsWithBlanks] mutableCopy];
             likersList=[[NSMutableArray alloc] initWithArray:[responsedic objectForKey:@"data"]];
            
            [self.likersTableView reloadData];

            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[UIAlertView alloc] initWithTitle:@"Sorry, unable to fetch like data. Please try again." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                
                
                
            });
            
        }

    }];
 
   
}

-(void)followersRequest{

    [[ServerManager sharedManager] getFollowersListOfUser:self.userId InPage:pageIndex WithCompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
        if ( resultDataArray!=nil) {
            
            NSMutableDictionary *responsedic= [[resultDataArray dictionaryByReplacingNullsWithBlanks] mutableCopy];
            likersList=[[NSMutableArray alloc] initWithArray:[responsedic objectForKey:@"data"]];
            
            [self.likersTableView reloadData];
            
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[UIAlertView alloc] initWithTitle:@"Sorry, unable to fetch followers data. Please try again." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                
                
                
            });
            
        }
        
    }];
    

}

-(void)followingRequest{

    
    [[ServerManager sharedManager] getFollowingListOfUser:self.userId WithCompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
        if ( resultDataArray!=nil) {
            
            NSMutableDictionary *responsedic= [[resultDataArray dictionaryByReplacingNullsWithBlanks] mutableCopy];
            likersList=[[NSMutableArray alloc] initWithArray:[responsedic objectForKey:@"data"]];
            
            [self.likersTableView reloadData];
            
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[UIAlertView alloc] initWithTitle:@"Sorry, unable to fetch following data. Please try again." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                
                
                
            });
            
        }
        
    }];


}


#pragma mark - UITableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return likersList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"likersCell";
    
    LikeListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[LikeListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    

    
    cell.followButton.tag = indexPath.row;
    // set follow button state
    
//    if (self.isFolloweing) {
//        
//        cell.followButton.selected = YES;
//        cell.followButton.backgroundColor = [HXColor hx_colorWithHexString:@"2DCC70"];
//        cell.followButton.layer.borderColor = [[HXColor hx_colorWithHexString:@"2DCC70"]CGColor];
//        
//    }else{
    
    NSLog(@"likersList %@",likersList);
    
    if([[[likersList objectAtIndex:indexPath.row] objectForKey:@"user_id"] intValue]==[UserAccount sharedManager].userId)
    {
        cell.followButton.hidden = YES;
    //    cell.followButton.backgroundColor = [HXColor hx_colorWithHexString:@"2DCC70"];
    //    cell.followButton.layer.borderColor = [[HXColor hx_colorWithHexString:@"2DCC70"]CGColor];
        
    }
    else if([[[likersList objectAtIndex:indexPath.row] objectForKey:@"is_following"] integerValue])
    {
        cell.followButton.hidden = NO;
        cell.followButton.selected = YES;
//        cell.followButton.backgroundColor = [HXColor hx_colorWithHexString:@"2DCC70"];
//        cell.followButton.layer.borderColor = [[HXColor hx_colorWithHexString:@"2DCC70"]CGColor];
        
    }else
    {
        
        cell.followButton.hidden = NO;
        
        cell.followButton.selected = NO;
//        cell.followButton.backgroundColor = [UIColor whiteColor];
//        cell.followButton.layer.borderColor = [[HXColor hx_colorWithHexString:@"000000"]CGColor];
    }

    //    }
    
    cell.profileName.text = [[likersList objectAtIndex:indexPath.row] objectForKey:@"username"];
    
    if ([[[likersList objectAtIndex:indexPath.row] objectForKey:@"username"] isEqualToString:@""]) {
        
        cell.userName.text=@"";
    }
    else
    {
        cell.userName.text=@"";
    }
    [cell.profilePic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[likersList objectAtIndex:indexPath.row] objectForKey:@"profile_picture"]]]];
    
    return cell;
}

#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self.likersTableView deselectRowAtIndexPath:indexPath animated:NO];
    
    ProfileViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    
    controller.isUserOrFollower = YES;
    
    if (self.isFollower) {
        
        controller.userId = [[[likersList objectAtIndex:indexPath.row] objectForKey:@"user_id"]intValue];
        
    }else if (self.isFolloweing)
    {
        controller.userId = [[[likersList objectAtIndex:indexPath.row] objectForKey:@"user_id"]intValue];

    
    }else{
       controller.userId = [[[likersList objectAtIndex:indexPath.row] objectForKey:@"user_id"]intValue];
    }
    [self.navigationController pushViewController:controller animated:YES];
    
    
}
- (IBAction)followButtonAction:(UIButton *)sender {
    sender.userInteractionEnabled = NO;
    
    
    [[ServerManager sharedManager] postFollowUserId:[[[likersList objectAtIndex:sender.tag] objectForKey:@"user_id"]intValue] WithCompletion:^(BOOL success) {
        
        sender.userInteractionEnabled = YES;
        
        if (success) {
            
            sender.selected=!sender.selected;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"loadAllFeed" object:nil];
        }
        else{
            sender.userInteractionEnabled = YES;
            
            
            
        }

    }];
    
    
}

- (IBAction)backButtonAction:(id)sender {
    
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

//-(UIImage *)imageManager:(SDWebImageManager *)imageManager transformDownloadedImage:(UIImage *)image withURL:(NSURL *)imageURL
//{
//    // NSLog(@"imageURL : %@",imageURL);
//    UIImage *resizedImage;
//    //  return resizedImage  = [image resizedImage:CGSizeMake(512, 512) interpolationQuality:kCGInterpolationDefault];
//    
//    float MIN_UPLOAD_RESOLUTION=756*756;
//    float factor;
//    float resol = image.size.height*image.size.width;
//    if (resol >MIN_UPLOAD_RESOLUTION){
//        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
//        resizedImage = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
//        return resizedImage;
//        
//    }
//    else
//        return image;
//}
//
//- (UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
//    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
//    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
//    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return scaledImage;
//}

@end
