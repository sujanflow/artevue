//
//  ExistingMessageViewController.h
//  Artegrams
//
//  Created by Tanvir Palash on 8/16/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIImageView+WebCache.h"
#import "AFNetworking.h"

@interface ExistingMessageViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,SDWebImageManagerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *existingMessagesTableView;

@property (weak, nonatomic) IBOutlet UIButton *addButton;

@end
