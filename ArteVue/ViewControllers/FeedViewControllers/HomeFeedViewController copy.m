//
//  HomeFeedViewController.m
//  Artegram
//
//  Created by Tanvir Palash on 6/10/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "HomeFeedViewController.h"
#import "FeedTableViewCell.h"
#import "Constants.h"
#import "LikersViewController.h"
#import "ProfileViewController.h"
#import "UserAccount.h"
#import "CommentViewController.h"

#import "MessagingViewController.h"
#import "ExistingMessageViewController.h"

#import "NSArray+NullReplacement.h"
#import "NSDictionary+NullReplacement.h"

#import "DGActivityIndicatorView.h"
#import "SharePhotoViewController.h"
#import "HashTagViewViewController.h"
#import "ArtistPostViewController.h"
#import "ArtNewsViewController.h"
#import "EventsViewController.h"

#import "ExploreTableViewCell.h"

#import "GIBadgeView.h"

#import <Realm/Realm.h>
#import "Posts.h"

@import GooglePlaces;



@interface HomeFeedViewController ()
{
    NSMutableArray *feedList;
    NSMutableDictionary*sharePostDictionary;
    UIImageView* imageViewForShare;
    PTPusher *pusherClient;
    GIBadgeView* badgeView;
    GMSPlacesClient *_placesClient;
    NSArray *feedType;
    NSMutableArray *imageNameArray;
    
    NSInteger pageIndex;
    BOOL islastItemReached;
    
    int selectedTab;
    
    RLMRealmConfiguration *realmv1Configuration;
    NSMutableArray *downloadingIds;
}

@property (nonatomic) DGActivityIndicatorView *activityIndicatorView;


@end

@implementation HomeFeedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     _placesClient = [GMSPlacesClient sharedClient];
    
    //setting delegate
    badgeView = [GIBadgeView new];
    
    badgeView.font = [UIFont fontWithName:@"AzoSans-Medium" size:12];
    badgeView.rightOffset = 5.0f;
    badgeView.topOffset=5.0f;
    [badgeView setHidden:YES];
    
    [self.inboxActionButton addSubview:badgeView];
    NSLog(@"badgeView %@",badgeView);
    
    
    pusherClient = [PTPusher pusherWithKey:pusher_app_key delegate:self encrypted:YES cluster:@"ap1"];
    
    PTPusherChannel *channel = [pusherClient subscribeToChannelNamed:[NSString stringWithFormat:@"%i-message-channel",[UserAccount sharedManager].userId]];
    
    [channel bindToEventNamed:@"new-message" handleWithBlock:^(PTPusherEvent *channelEvent) {
        
        NSLog(@"message received in home : %@", channelEvent.data);
        [UserAccount sharedManager].pendingMessageCount++;
        
        [badgeView setBadgeValue: [UserAccount sharedManager].pendingMessageCount];
        [badgeView setHidden:NO];
        
       
        
    }];
    
    
    [pusherClient connect];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBadgeValue) name:@"updateMessageBubble" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadInitialFeed) name:@"loadAllFeed" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(makeRequestwithPage) name:@"RefreshFeed" object:nil];
    
    
    
    self.activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)DGActivityIndicatorAnimationTypeTriplePulse tintColor:[UIColor blackColor]];
    CGFloat width = self.view.bounds.size.width / 5.0f;
    CGFloat height = self.view.bounds.size.height / 5.0f;
    
    //self.activityIndicatorView.backgroundColor=[UIColor redColor];
    self.activityIndicatorView.frame = CGRectMake(self.view.bounds.size.width/2-width/2,self.view.bounds.size.height/2-height/2, width, height);
    [self.view addSubview:self.activityIndicatorView];
    
    //   NSLog(@"RLMRealm.defaultRealm().path %@",[RLMRealmConfiguration defaultConfiguration].fileURL);
    //   [[RLMRealm defaultRealm] writeCopyToURL:[NSURL URLWithString:@"`/Users/tanvirpalash/desktop/post.realm"] encryptionKey:nil error:nil];
    
    [[NSFileManager defaultManager] removeItemAtURL:[RLMRealmConfiguration defaultConfiguration].fileURL error:nil];
    
    //  NSURL *v1URL = [[NSBundle mainBundle] URLForResource:@"defaultv1" withExtension:@"realm"];
    //  [[NSFileManager defaultManager] copyItemAtURL:v1URL toURL:realmv1URL error:nil];
    
    NSURL *defaultRealmURL = [RLMRealmConfiguration defaultConfiguration].fileURL;
    NSURL *defaultRealmParentURL = [defaultRealmURL URLByDeletingLastPathComponent];
    NSURL *realmv1URL = [defaultRealmParentURL URLByAppendingPathComponent:@"defaultv1.realm"];
    
    realmv1Configuration = [[RLMRealmConfiguration defaultConfiguration] copy];
    realmv1Configuration.fileURL = realmv1URL;
    
    
    
    feedList=[[NSMutableArray alloc] init];
    downloadingIds=[[NSMutableArray alloc] init];
    self.feedTableView.delegate = self;
    self.feedTableView.dataSource = self;
    
   //self.postTypeCollectionView.hidden = YES;
    
    self.feedTableView.estimatedRowHeight = 543;
    self.feedTableView.rowHeight = UITableViewAutomaticDimension;
    
    self.feedTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.feedTableView.frame.size.width, 1)];
    
    selectedTab=0;
    
    //feedType = [[NSArray alloc]initWithObjects:@"Trending Artists",@"Top Posts",@"Favourites",@"Events",@"Art News", nil];
    feedType = [[NSArray alloc]initWithObjects:@"Events",@"Art News", nil];
    
    //imageNameArray= [[NSArray alloc]initWithObjects:@"trendingArtist",@"topPost",@"favouritePost",@"eventIcon",@"aiweiwei", nil];
    
    
    
    self.postTypeCollectionView.delegate =  self;
    self.postTypeCollectionView.dataSource = self;
    
    pageIndex=1;
    
    [self makeRequestwithPage];
    
//    self.feedTableView.refreshControl=[[UIRefreshControl alloc] init];
//    self.feedTableView.refreshControl.backgroundColor=[UIColor whiteColor];
//    self.feedTableView.refreshControl.tintColor=[UIColor grayColor];
//    [self.feedTableView.refreshControl addTarget:self action:@selector(loadInitialFeed) forControlEvents:UIControlEventValueChanged];
    
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor=[UIColor whiteColor];
    self.refreshControl.tintColor=[UIColor grayColor];
    [self.refreshControl addTarget:self action:@selector(loadInitialFeed) forControlEvents:UIControlEventValueChanged];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 10.0) {
        
        NSLog(@"below ios 10");
        self.feedTableView.backgroundView = self.refreshControl;
        
        
    } else {
        
        NSLog(@"above ios 10");
        self.feedTableView.refreshControl = self.refreshControl;
    }
    
    
  //  [SDWebImageManager sharedManager].delegate=self;
    
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) viewDidAppear:(BOOL)animated
{
    //api call
    
    imageViewForShare=[[UIImageView alloc]init];
    //[self makeRequest];
    
    self.comingSoonLabel.hidden=YES;
    self.newsLabel.hidden=YES;
    
    
    
   // NSLog(@"RefreshNeededInFeed %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"RefreshNeededInFeed"]);
    
//    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"RefreshNeededInFeed"] isEqualToString:@"1"])
//    {
//        [self loadInitialFeed];
//        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"RefreshNeededInFeed"];
//    }
//  
}

- (void)reloadData
{
    
   // CGPoint offset = self.feedTableView.contentOffset;

    // Reload table data
    [self.feedTableView reloadData];
    
    [self.feedTableView layoutIfNeeded];
    //[self.feedTableView setContentOffset:offset animated:YES];
    
    // End the refreshing
    if (self.refreshControl) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
        
        [self.refreshControl endRefreshing];
    }
}

-(void)loadInitialFeed{
    
    if(feedList.count)
    {
        NSIndexPath* index=[NSIndexPath indexPathForRow:0 inSection:0];
        [self.feedTableView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
    }
    
    imageViewForShare=[[UIImageView alloc]init];
    //[self makeRequest];
    pageIndex=1;
    self.comingSoonLabel.hidden=YES;
    self.newsLabel.hidden=YES;
    
    
    selectedTab=0;
    
    self.artNewsButton.selected=NO;
    self.eventsButton.selected=NO;
    
    
    self.topSelectedView.hidden=YES;
    self.bottomSelectedView.hidden=YES;
    
    [self.postTypeCollectionView reloadData];
    
    [self.activityIndicatorView startAnimating];
    
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    [postData setObject:ACCESS_KEY forKey:@"access_key"];
    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"user_id"];
    [postData setObject:[NSNumber numberWithInteger:1] forKey:@"page"];
    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
    NSLog(@"postData for feed %@ for selected tab %d",postData,selectedTab);
    
    //    [apiLoginManager POST:[NSString stringWithFormat:@"%@/posts/api-feed-list",SERVER_BASE_API_URL] parameters:postData
    //                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
    
    NSString* urlString=[NSString stringWithFormat:@"%@/posts/api-lazy-feed-list/",SERVER_BASE_API_URL];
    
    [apiLoginManager POST:urlString parameters:postData
                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                      
                      NSMutableDictionary *responsedic= [[responseObject dictionaryByReplacingNullsWithBlanks] mutableCopy];
                      
                      
                      NSMutableArray *responseArray=[[NSMutableArray alloc] initWithArray:[responsedic objectForKey:@"posts"]];
                      
                      feedList=[[NSMutableArray alloc] initWithArray:responseArray];
                      
                      
                      
                      if(feedList.count)
                      {
                          self.feedTableView.hidden = NO;
                          self.topSegmentView.hidden = NO;
                          
                          self.noDataLabel.hidden=YES;
                          
                          islastItemReached=NO;
                        //  [self.feedTableView reloadData];
                          [self reloadData];
                          
                          NSIndexPath* index=[NSIndexPath indexPathForRow:0 inSection:0];
                          [self.feedTableView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:YES];
                          

                          
                          
                      }
                      else
                      {
                          self.topSegmentView.hidden = YES;
                          
                          self.feedTableView.hidden = YES;
                          self.noDataLabel.hidden=NO;
                          
                          //[self.feedTableView reloadData];
                          [self reloadData];
                          

                      }
                      
                      // [self backgroundAdd];
                      
                      [self.activityIndicatorView stopAnimating];
                      
                  }
     
                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                      
                      NSLog(@"Error: %@", error);
                      
                      //self.view.userInteractionEnabled=YES;
                      [self reloadData];
                      
                      NSLog(@"%@",operation.responseString);
                      [self.activityIndicatorView stopAnimating];
                      
                      
                  }];
}

-(void) makeRequestwithPage{
    
    
    [self.activityIndicatorView startAnimating];
    
    if (pageIndex<=0) {
        pageIndex=1;
    }
    
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    [postData setObject:ACCESS_KEY forKey:@"access_key"];
    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"user_id"];
    [postData setObject:[NSNumber numberWithInteger:pageIndex] forKey:@"page"];
    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
    NSLog(@"postData for feed %@ for selected tab %d",postData,selectedTab);
    
    //    [apiLoginManager POST:[NSString stringWithFormat:@"%@/posts/api-feed-list",SERVER_BASE_API_URL] parameters:postData
    //                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
    
    NSString* urlString=[NSString stringWithFormat:@"%@/posts/api-lazy-feed-list/",SERVER_BASE_API_URL];
    
    if(selectedTab==1)
    {
        urlString=[NSString stringWithFormat:@"%@/posts/api-get-trending-artist-posts/",SERVER_BASE_API_URL];
    }
    else if(selectedTab==2)
    {
        urlString=[NSString stringWithFormat:@"%@/posts/api-get-trending-posts/",SERVER_BASE_API_URL];
    }
    else if(selectedTab==3)
    {
      //  urlString=[NSString stringWithFormat:@"%@/posts/api-get-trending-posts/",SERVER_BASE_API_URL];
        
        urlString=[NSString stringWithFormat:@"%@/posts/api-get-favourite-users-posts/",SERVER_BASE_API_URL];
    }
    
    
    [apiLoginManager POST:urlString parameters:postData
                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                      
                      NSMutableDictionary *responsedic= [[responseObject dictionaryByReplacingNullsWithBlanks] mutableCopy];
                      
                      
                      NSMutableArray *responseArray=[[NSMutableArray alloc] initWithArray:[responsedic objectForKey:@"posts"]];
                      
                      if(selectedTab!=1 && selectedTab != 2 && selectedTab !=3)
                      {
                          imageNameArray = [[NSMutableArray alloc] init];
                         // [imageNameArray addObject:[responsedic objectForKey:@"trending_artist_image"]];
                         // [imageNameArray addObject:[responsedic objectForKey:@"trending_post_image"]];
                         // [imageNameArray addObject:[responsedic objectForKey:@"favourite_user_image"]];
                          [imageNameArray addObject:[responsedic objectForKey:@"events_image"]];
                          [imageNameArray addObject:[responsedic objectForKey:@"news_image"]];
                          
                          [self.postTypeCollectionView reloadData];
                          
                      }
                      
                      
                      
                      //NSLog(@"responseArray %@",responseArray);
                      if(responseArray.count)
                      {
                          if(pageIndex==1)
                          {
                              feedList=[[NSMutableArray alloc] init];
                              feedList=responseArray;
                              
                          }else
                          {
                              [feedList addObjectsFromArray:responseArray];
                              
                          }
                          pageIndex++;
                          islastItemReached=NO;
                      }
                      else
                      {
                          islastItemReached=YES;
                      }
                      
                      
                       NSLog(@"Response: %@ %@", urlString,feedList);
                      
                      if(feedList.count)
                      {
                          self.feedTableView.hidden = NO;
                          self.topSegmentView.hidden = NO;
                          
                          self.noDataLabel.hidden=YES;
                          

                      }
                      else
                      {
                          self.topSegmentView.hidden = YES;
                          
                          self.feedTableView.hidden = YES;
                          self.noDataLabel.hidden=NO;
                      }
                      
                    //   [self backgroundAdd];
                      
                   //   CGPoint offset = self.feedTableView.contentOffset;

                      // Reload table data
                      
                      
//                      [self.feedTableView layoutIfNeeded];
//                      [self.feedTableView setContentOffset:offset animated:YES];
                      
                      [self.feedTableView reloadData];
                      
                      [self.activityIndicatorView stopAnimating];
                      
                  }
     
                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                      
                      NSLog(@"Error: %@", error);
                      
                      //self.view.userInteractionEnabled=YES;
                      
                      NSLog(@"%@",operation.responseString);
                      [self.activityIndicatorView stopAnimating];
                      
                      
                  }];
    
}

- (void)backgroundAdd
{
    
    //[[NSFileManager defaultManager] removeItemAtURL:[RLMRealmConfiguration defaultConfiguration].fileURL error:nil];
    
    for (int i=0; i<feedList.count; i++) {
        
        
        
        // NSLog(@"backgroundAdd with %@",[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[feedList objectAtIndex:i] objectForKey:@"image"] ]]);
        
        // RLMRealm *realmTest = [RLMRealm defaultRealm];
        RLMRealm *realmv1 = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
        
        
        RLMResults *result=[Posts objectsInRealm:realmv1 where:[NSString stringWithFormat:@"postId = %@",[[feedList objectAtIndex:i] objectForKey:@"id"]]];
        
        if(result.count)
        {
            
            [self reloadData];
            
        }
        else
        {
            SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
            [SDWebImageDownloader sharedDownloader].maxConcurrentDownloads=1;
            
            [downloader downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[feedList objectAtIndex:i] objectForKey:@"image"] ]]
                                     options:0
                                    progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                        // progression tracking code
                                        NSLog(@"receivedSize %ld expectedSize %ld",(long)receivedSize,(long)expectedSize);
                                        
                                        
                                    }
                                   completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                       if (image && finished) {
                                           // do something with image
                                           
                                           //   Posts *post = [[Posts alloc] init];
                                           
                                           dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                                           dispatch_async(queue, ^{
                                               // Get new realm and table since we are in a new thread
                                               
                                               NSLog(@"done %@",[[feedList objectAtIndex:i] objectForKey:@"id"]);
                                               
                                               //RLMRealm *realm = [RLMRealm defaultRealm];
                                               
                                               RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
                                               
                                               
                                               [realm beginWriteTransaction];
                                               [Posts createInRealm:realm withValue:@{@"postId": [NSNumber numberWithInt:[[[feedList objectAtIndex:i] objectForKey:@"id"] intValue]],
                                                                                      @"imageData": data}];
                                               
                                               [realm commitWriteTransaction];
                                               
                                               
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   [self reloadData];
                                                   
                                               });
                                           });
                                           
                                           
                                       }
                                   }];
            
            
            
        }
    }
    
    
    
}

#pragma mark - CollectionView data source

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return  imageNameArray.count;
    
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    //  NSLog(@"artArray.............  %@",_artArray);
    
    static NSString *identifier = @"postTypeCell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    UIImageView *postTypeImage = (UIImageView*) [cell viewWithTag:1];
    UILabel *postTypeLabel = (UILabel*) [cell viewWithTag:2];
    
    
    [cell layoutIfNeeded];
    postTypeImage.layer.cornerRadius = 3;
    //self.profilePicture.layer.borderWidth = 1.1f;
    postTypeImage.clipsToBounds = YES;
    postTypeImage.layer.masksToBounds = YES;
   
    postTypeLabel.text =[NSString stringWithFormat:@"%@",[feedType objectAtIndex:indexPath.row]];
    
    
//    if(selectedTab!=1 && selectedTab != 2 && selectedTab !=3)
//    {
//        NSLog(@"%@",[imageNameArray objectAtIndex:indexPath.row]);
        [ postTypeImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL,[imageNameArray objectAtIndex:indexPath.row] ]]];
//    }
//    else
//    {
//        NSString * fileName= [ [ NSBundle mainBundle] pathForResource:[imageNameArray objectAtIndex:indexPath.row] ofType:@"jpg"];
//        
//        postTypeImage.image=[UIImage imageWithContentsOfFile:fileName];
//       
//        
//    }
    
    postTypeImage.layer.borderColor=[UIColor clearColor].CGColor;
    if(selectedTab==indexPath.row+1)
    {
        postTypeImage.layer.borderWidth=2.0f;
        
    }else
    {
        postTypeImage.layer.borderWidth=0.0f;
        
    }
    
    
    NSLog(@"collectionview %@ cell %@",collectionView,cell);
    return cell;
    
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake((collectionView.frame.size.width-5)/2, collectionView.frame.size.height);
    
    
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
  //  [self.postTypeCollectionView reloadData];
    
    self.comingSoonLabel.hidden=YES;
    self.newsLabel.hidden=YES;
    self.feedTableView.hidden = NO;
    
    pageIndex=1;
    selectedTab=(int)indexPath.item+1;
    
//    if (indexPath.item==0 || indexPath.item==1 || indexPath.item==2) {
//       
//        [self makeRequestwithPage];
//        
//    }
//    else
//    
    if (indexPath.item==0)
    {
        EventsViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"EventsViewController"];
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    else if (indexPath.item==1)
    {
         ArtNewsViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ArtNewsViewController"];
        [self.navigationController pushViewController:controller animated:YES];
        
    }

    
    
    
    
}

-(void)collectionView:(UICollectionView *)collectionView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"didDeselectRowAtIndexPath");
//    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
//    UIImageView *postTypeImage = (UIImageView*) [cell viewWithTag:1];
//    
//    postTypeImage.layer.borderColor=[UIColor whiteColor].CGColor;
//    postTypeImage.layer.borderWidth=2.0f;
    
    
    
}




#pragma mark - UITableView DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

  return feedList.count;

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{

    static NSString *HeaderCellIdentifier = @"feedHeaderTableCell";
    
    ExploreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:HeaderCellIdentifier];
    if (cell == nil) {
        cell = [[ExploreTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:HeaderCellIdentifier];
    }
    
//    UIImageView *profileImage = (UIImageView*) [cell viewWithTag:1];
//    UILabel *profileName = (UILabel*) [cell viewWithTag:2];
//    UIButton *moreButton = (UIButton*) [cell viewWithTag:4];
    
    
    cell.profilePic.layer.cornerRadius = cell.profilePic.bounds.size.width/2;
    //self.profilePicture.layer.borderWidth = 1.1f;
    cell.profilePic.clipsToBounds = YES;
    cell.profilePic.layer.masksToBounds = YES;
    
    NSString *locationString=[[feedList objectAtIndex:section] objectForKey:@"address_title"];
 //   NSLog(@"locationString %@",locationString);
    
    if (locationString.length>0) {
        
       // [profileName setText:[NSString stringWithFormat:@"%@\n%@",[[[feedList objectAtIndex:section] objectForKey:@"user"] objectForKey:@"username"],locationString]];
        
        UIFont *userNameFont = [UIFont fontWithName:@"AzoSans-Medium" size:14];
        UIFont *locationFont = [UIFont fontWithName:@"AzoSans-Regular" size:10];
        
        
        NSDictionary *userDict = [NSDictionary dictionaryWithObject:userNameFont forKey:NSFontAttributeName ];
        NSDictionary *locationDict = [NSDictionary dictionaryWithObject: locationFont forKey:NSFontAttributeName];
        
        NSMutableAttributedString *uAttrString = [[NSMutableAttributedString alloc] initWithString:[[[feedList objectAtIndex:section] objectForKey:@"user"] objectForKey:@"username"] attributes: userDict];
        NSMutableAttributedString *lAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@",locationString] attributes: locationDict];
        NSMutableAttributedString *dAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@",[self relativeDateStringForDate:[self getDateFromString:[[feedList objectAtIndex:section] objectForKey:@"created"]]]] attributes: locationDict];
        
        
        
        
        [uAttrString appendAttributedString:lAttrString];
        [uAttrString appendAttributedString:dAttrString];
        cell.userName.attributedText=uAttrString;
        
    }
    else
    {
        NSString *placeId=[[feedList objectAtIndex:section] objectForKey:@"google_place_id"];
        
     //   NSLog(@"placeId %@",placeId);
        
        if(placeId.length>0)
        {
       //     NSLog(@"place id exist");
            [_placesClient lookUpPlaceID:placeId callback:^(GMSPlace *place, NSError *error) {
                if (error != nil) {
                    NSLog(@"Place Details error %@", [error localizedDescription]);
                    
                }
                
                if (place != nil) {
//                    NSLog(@"Place name %@", place.name);
//                    NSLog(@"Place address %@", place.formattedAddress);
//                    NSLog(@"Place placeID %@", place.placeID);
//                    NSLog(@"Place attributions %@", place.attributions);
//                    
                    
                    
                    UIFont *userNameFont = [UIFont fontWithName:@"AzoSans-Medium" size:14];
                    UIFont *locationFont = [UIFont fontWithName:@"AzoSans-Regular" size:10];
                    
                    
                    NSDictionary *locationDict = [NSDictionary dictionaryWithObject: locationFont forKey:NSFontAttributeName];
                    NSDictionary *userDict = [NSDictionary dictionaryWithObject:userNameFont forKey:NSFontAttributeName ];
                    
                    NSMutableAttributedString *uAttrString = [[NSMutableAttributedString alloc] initWithString:[[[feedList objectAtIndex:section] objectForKey:@"user"] objectForKey:@"username"] attributes: userDict];
                    NSMutableAttributedString *lAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@",place.formattedAddress] attributes: locationDict];
                    NSMutableAttributedString *dAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@",[self relativeDateStringForDate:[self getDateFromString:[[feedList objectAtIndex:section] objectForKey:@"created"]]]] attributes: locationDict];
                    
                    [uAttrString appendAttributedString:lAttrString];
                    [uAttrString appendAttributedString:dAttrString];
                    
                    cell.userName.attributedText=uAttrString;

                } else {
                    NSLog(@"No place details for %@", placeId);
                    UIFont *userNameFont = [UIFont fontWithName:@"AzoSans-Medium" size:14];
                    UIFont *locationFont = [UIFont fontWithName:@"AzoSans-Regular" size:10];
                    
                    
                    NSDictionary *userDict = [NSDictionary dictionaryWithObject:userNameFont forKey:NSFontAttributeName ];
                    NSDictionary *locationDict = [NSDictionary dictionaryWithObject: locationFont forKey:NSFontAttributeName];
                    
                    NSMutableAttributedString *uAttrString = [[NSMutableAttributedString alloc] initWithString:[[[feedList objectAtIndex:section] objectForKey:@"user"] objectForKey:@"username"] attributes: userDict];
                   // NSMutableAttributedString *lAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@",locationString] attributes: locationDict];
                    NSMutableAttributedString *dAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@",[self relativeDateStringForDate:[self getDateFromString:[[feedList objectAtIndex:section] objectForKey:@"created"]]]] attributes: locationDict];
                    
                    
                    
                    
                    //[uAttrString appendAttributedString:lAttrString];
                    [uAttrString appendAttributedString:dAttrString];
                    cell.userName.attributedText=uAttrString;
                }
            }];
        }
        else
        {
            
         //   NSLog(@"no place id");
            
            UIFont *userNameFont = [UIFont fontWithName:@"AzoSans-Medium" size:14];
            UIFont *locationFont = [UIFont fontWithName:@"AzoSans-Regular" size:10];
            
            
            NSDictionary *userDict = [NSDictionary dictionaryWithObject:userNameFont forKey:NSFontAttributeName ];
            NSDictionary *locationDict = [NSDictionary dictionaryWithObject: locationFont forKey:NSFontAttributeName];
            
            NSMutableAttributedString *uAttrString = [[NSMutableAttributedString alloc] initWithString:[[[feedList objectAtIndex:section] objectForKey:@"user"] objectForKey:@"username"] attributes: userDict];
          //  NSMutableAttributedString *lAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@",locationString] attributes: locationDict];
            NSMutableAttributedString *dAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@",[self relativeDateStringForDate:[self getDateFromString:[[feedList objectAtIndex:section] objectForKey:@"created"]]]] attributes: locationDict];
            
            
            //[uAttrString appendAttributedString:lAttrString];
            [uAttrString appendAttributedString:dAttrString];
            cell.userName.attributedText=uAttrString;
            
        }
    }
    
    
    if([[[[feedList objectAtIndex:section] objectForKey:@"user"] objectForKey:@"profile_picture"] isEqual:[NSNull null]] || [[[[feedList objectAtIndex:section] objectForKey:@"user"] objectForKey:@"profile_picture"]isEqualToString:@""])
        
        [cell.profilePic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL,@"img/profile-holder.png"]]];
    
    else
        [cell.profilePic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL,[[[feedList objectAtIndex:section] objectForKey:@"user"] objectForKey:@"profile_picture"]]]];

    
    //set cell header tag
    
    cell.tag= section;
    
    //set more button tag
    
//    UIButton *moreButtonDummuy=[UIButton buttonWithType:UIButtonTypeCustom];
//    moreButtonDummuy=moreButton;
//    moreButtonDummuy.tag=section;
    
 //   NSLog(@" checking artist");
    
    if(![[[feedList objectAtIndex:section] objectForKey:@"artist"] isEqual:[NSNull null]])
    {
     //   NSLog(@" writing artist %@",[[feedList objectAtIndex:section] objectForKey:@"artist"]);
        
        [cell.artistName setText:[[feedList objectAtIndex:section] objectForKey:@"artist"]];
        
        if(cell.artistName.text.length)
        {
            cell.arrowLabel.hidden=NO;
            cell.artistNameInteractionView.hidden=NO;
            
        }
        else{
            cell.arrowLabel.hidden=YES;
            cell.artistNameInteractionView.hidden=YES;
            
        }
        
        
        UITapGestureRecognizer *tapOnHeader = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnArtistName:)];
        //[tapOnHeader setDelegate:self];
        tapOnHeader.numberOfTouchesRequired = 1;
        tapOnHeader.numberOfTapsRequired = 1;
        cell.artistName.userInteractionEnabled=YES;
        [cell.artistName addGestureRecognizer:tapOnHeader];
        
        UITapGestureRecognizer *tapOnArtist = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnArtistNameInView:)];
        //[tapOnHeader setDelegate:self];
        tapOnArtist.numberOfTouchesRequired = 1;
        tapOnArtist.numberOfTapsRequired = 1;
        cell.artistNameInteractionView.tag=section;
        [cell.artistNameInteractionView addGestureRecognizer:tapOnArtist];
    }
    else
    {
        cell.arrowLabel.hidden=YES;
        cell.artistNameInteractionView.hidden=YES;
        
    }
    
    
    cell.moreButton.tag= section;
    
    //tap on header view
    
    UITapGestureRecognizer *tapOnHeader = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHeader:)];
    //[tapOnHeader setDelegate:self];
    tapOnHeader.numberOfTouchesRequired = 1;
    tapOnHeader.numberOfTapsRequired = 1;
    [cell addGestureRecognizer:tapOnHeader];
    
    return cell;

}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    FeedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"feedTableCell"];
    
    //set feed image
    
//    cell.feedImageRatio.active = NO;
//    
//    [cell.feedImage addConstraint:[NSLayoutConstraint
//                                   constraintWithItem:cell.feedImage
//                                   attribute:NSLayoutAttributeWidth
//                                   relatedBy:NSLayoutRelationEqual
//                                   toItem:cell.feedImage
//                                   attribute:NSLayoutAttributeHeight
//                                   multiplier:[[[feedList objectAtIndex:indexPath.section] objectForKey:@"aspect_ratio"] floatValue]
//                                   constant:0]];
    
    for (UIView *v in cell.feedImage.subviews) {
        [v removeFromSuperview];
    }
   // [ cell.feedImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[feedList objectAtIndex:indexPath.section] objectForKey:@"image"] ]]];
    cell.feedImage.tag=indexPath.section;

    RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
    NSLog(@"realmv1Configuration %@",realmv1Configuration.fileURL);
    
    RLMResults *result=[Posts objectsInRealm:realm where:[NSString stringWithFormat:@"postId = %@",[[feedList objectAtIndex:indexPath.section] objectForKey:@"id"]]];
    
    
    //NSLog(@"current indexpath %@",indexPath);
    if(result.count)
    {
        Posts *post=[[Posts alloc] init];
        post=[result objectAtIndex:0];
       // NSLog(@"result %@",result);
        
       
        [UIView transitionWithView:cell.feedImage duration:0.3f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            cell.feedImage.image=[UIImage imageWithData:post.imageData];
        } completion:^(BOOL finished) {
            ;
        }];
        
    }
    else
    {
        cell.feedImage.image=nil;
        
        if([downloadingIds containsObject: [[feedList objectAtIndex:indexPath.section] objectForKey:@"id"]])
        {
            //do nothing
        }
        else
        {
            
            [downloadingIds addObject:[[feedList objectAtIndex:indexPath.section] objectForKey:@"id"]];
            
            SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
            [SDWebImageDownloader sharedDownloader].maxConcurrentDownloads=1;
            
            [downloader downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[feedList objectAtIndex:indexPath.section] objectForKey:@"image"] ]]
                                     options:0
                                    progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                        // progression tracking code
                                        // NSLog(@"receivedSize %ld expectedSize %ld",(long)receivedSize,(long)expectedSize);
                                        
                                    }
                                   completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                       if (image && finished) {
                                           // do something with image
                                           
                                           //   Posts *post = [[Posts alloc] init];
                                           
                                           
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   //  cell.feedImage=(UIImageView*) [cell viewWithTag:indexPath.section] ;
                                                 //  cell.feedImage.image=image;
                                                   
                                                 //  NSLog(@"done for %@ and indexpath %@",[[feedList objectAtIndex:indexPath.section] objectForKey:@"id"],indexPath);
                                                   
                                                   //RLMRealm *realm = [RLMRealm defaultRealm];
                                                   
                                                   RLMRealm *realm = [RLMRealm realmWithConfiguration:realmv1Configuration error:nil];
                                                   
                                                   
                                                   [realm beginWriteTransaction];
                                                   [Posts createOrUpdateInRealm:realm withValue:@{@"primId": [NSString stringWithFormat:@"%@",[[feedList objectAtIndex:indexPath.section] objectForKey:@"id"]]
                                                                                                  ,@"postId": [NSNumber numberWithInt:[[[feedList objectAtIndex:indexPath.section] objectForKey:@"id"] intValue]],
                                                                                          @"imageData": data}];
                                                   
                                                   [realm commitWriteTransaction];
                                                   
                                                   [downloadingIds removeObject:[[feedList objectAtIndex:indexPath.section] objectForKey:@"id"]];
                                                   //[self reloadData];
                                                   
                                                   FeedTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                                                   
                                                   [UIView transitionWithView:cell.feedImage duration:0.15f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                                                       cell.feedImage.image=image;
                                                       
                                                   } completion:^(BOOL finished) {
                                                       ;
                                                   }];
                                                   
                                                   
                                                   //                                               [self.feedTableView beginUpdates];
                                                   //                                               [self.feedTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                                                   //                                               [self.feedTableView endUpdates];
                                                   
                                               });
                                           
                                       }
                                       
                                   }];

        }
        
    }
    
    cell.imageViewHeightConstraint.constant=[UIScreen mainScreen].bounds.size.width/[[[feedList objectAtIndex:indexPath.section] objectForKey:@"aspect_ratio"] floatValue];
    

    //single tap
    UITapGestureRecognizer* postImageTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnPostImage:)];
    postImageTapGesture.numberOfTapsRequired = 1;
    [cell.feedImage addGestureRecognizer:postImageTapGesture];
    cell.feedImage.userInteractionEnabled = YES;
    
    //double tap
    UITapGestureRecognizer* postImageDoubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapOnPostImage:)];
    postImageDoubleTap.numberOfTapsRequired = 2;
    [cell.feedImage addGestureRecognizer:postImageDoubleTap];
    cell.feedImage.userInteractionEnabled = YES;
    [postImageTapGesture requireGestureRecognizerToFail:postImageDoubleTap];

  
    
    
    
//    [ cell. sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL,[[feedList objectAtIndex:indexPath.section] objectForKey:@"image"] ]]];
//    
    //set like count
    
    int likeCount = [[[feedList objectAtIndex:indexPath.section] objectForKey:@"post_likes"] intValue];
    
    if (likeCount != 0) {
        
        [cell.likeButton setTitle:[NSString stringWithFormat:@"%@",[[feedList objectAtIndex:indexPath.section] objectForKey:@"post_likes"]] forState:UIControlStateSelected];
        [cell.likeButton setTitle:[NSString stringWithFormat:@"%@",[[feedList objectAtIndex:indexPath.section] objectForKey:@"post_likes"]] forState:UIControlStateNormal];
    }else
    {
        [cell.likeButton setTitle:@"" forState:UIControlStateNormal];
    }
    
    int pinCount = [[[feedList objectAtIndex:indexPath.section] objectForKey:@"pin_count"] intValue];
    
    //NSLog(@"pinCount %i",pinCount);
    
    if (pinCount != 0) {
        
        [cell.pinButton setTitle:[NSString stringWithFormat:@"%@",[[feedList objectAtIndex:indexPath.section] objectForKey:@"pin_count"]] forState:UIControlStateSelected];
        [cell.pinButton setTitle:[NSString stringWithFormat:@"%@",[[feedList objectAtIndex:indexPath.section] objectForKey:@"pin_count"]] forState:UIControlStateNormal];
    }else
    {
        [cell.pinButton setTitle:@"" forState:UIControlStateNormal];
    }
    
//    if (likeCount == 0) {
//  
//        [self changeConstraintForNoLike:cell];
//
//    }else if (likeCount == 1) {
//        cell.likeIconTop.constant = 8;
//        cell.likeIconHeight.constant = 15;
//        cell.likeIconWidth.constant = 15;
//        cell.constraintBtLikeIconAndDesView.constant = 10;
//        cell.likeCountLabel.text=[NSString stringWithFormat:@"1 like"];
//        cell.commentCountLeading.constant = 10;
//        
//    }else{
//        cell.likeIconTop.constant = 8;
//        cell.likeIconHeight.constant = 15;
//        cell.likeIconWidth.constant = 15;
//       cell.constraintBtLikeIconAndDesView.constant = 10;
//        cell.likeCountLabel.text=[NSString stringWithFormat:@"%@ likes",[[feedList objectAtIndex:indexPath.section] objectForKey:@"post_likes"]];
//        cell.commentCountLeading.constant = 10;
//    }
    
    
    //set like button tag
    cell.likeButton.tag=indexPath.section;
    
    
    //set like button state
    if([[[feedList objectAtIndex:indexPath.section] objectForKey:@"is_liked"] integerValue])
    {
        cell.likeButton.selected=YES;
        
    }
    else{
  
        cell.likeButton.selected=NO;
    }
    
    cell.pinButton.tag=indexPath.section;
    
    if([[[feedList objectAtIndex:indexPath.section] objectForKey:@"is_pinned"] integerValue])
    {
        cell.pinButton.selected=YES;
    }
    else
        cell.pinButton.selected=NO;
    
    //set post description
    
   

    
    
    
    if (![[[feedList objectAtIndex:indexPath.section] objectForKey:@"description"] isEqual:[NSNull null]] && ![[[feedList objectAtIndex:indexPath.section] objectForKey:@"description"] isEqualToString:@""]) {
        
//        cell.constraintBtDescriptionAndLike.constant = 8;
//        cell.constraintBtHashTagAndDescription.constant = 8;
        
        cell.descriptionLabel.text = [[feedList objectAtIndex:indexPath.section] objectForKey:@"description"];

        
    }else{
     
        
        cell.descriptionLabel.text= @"";
//        cell.likeIconTop.constant = 8;
//        
//        cell.descriptionViewHeight.constant = 0;
        
   //     NSLog(@"cell.descriptionViewHeight.constant %f",cell.descriptionViewHeight.constant);

    }
    [cell.descriptionLabel sizeToFit];
    
    //set hashTag
    
    //[cell.hashTagLabel sizeToFit];
    

    
//    if (![[[feedList objectAtIndex:indexPath.section] objectForKey:@"hashtags"] isEqual:[NSNull null]] && ![[[feedList objectAtIndex:indexPath.section] objectForKey:@"hashtags"] isEqualToString:@""]) {
//        
//        cell.constraintBtHashTagAndDescription.constant = 8;
//        cell.constraintBtHashTagAndFirstComment.constant = 8;
//        
//        cell.hashTagLabel.text = [[feedList objectAtIndex:indexPath.section] objectForKey:@"hashtags"];
//
//    }else{
//
//        cell.hashTagLabel.text= @"";
//        cell.hashTagViewHeight.constant = 0;
//        
//    }
    
    //set comment button tag
    cell.commentButton.tag=indexPath.section;
    
    cell.moreButton.tag = indexPath.section;
    
    
    //comment array
    
//    [cell.firstCommentLabel sizeToFit];
//    [cell.secondCommentLabel sizeToFit];
//    [cell.thirdCommentLabel sizeToFit];
    
    NSMutableArray *commentArray = [[NSMutableArray alloc]init];
    commentArray = [[feedList objectAtIndex:indexPath.section] objectForKey:@"post_comments"];
    
    if (commentArray.count !=0) {
        
       // [cell.commentButton setTitle:[NSString stringWithFormat:@"%lu comments",(unsigned long)commentArray.count] forState:UIControlStateSelected];
        [cell.commentButton setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)commentArray.count] forState:UIControlStateNormal];
    }else{
    
        [cell.commentButton setTitle:@"" forState:UIControlStateNormal];
    }
    
//    if (commentArray.count == 0) {
//
//        cell.commentCountLabel.text = @"";
//        cell.firstCommentLabel.text = @"";
//        cell.secondCommentLabel.text= @"";
//        cell.thirdCommentLabel.text = @"";
//        cell.firstCommentViewHeight.constant = 0;
//        cell.secondCommentViewHeight.constant = 0;
//        cell.thirdCommentViewHeight.constant = 0;
//
//        
//    }else{
//        
//        if (commentArray.count>=3) {
//          
//
//            cell.commentCountLabel.text = [NSString stringWithFormat:@"%lu comments",(unsigned long)commentArray.count];
//            
//            cell.firstCommentLabel.attributedText = [self getComentsTextConfigured:[[[commentArray objectAtIndex:commentArray.count - 3]objectForKey:@"user"] objectForKey:@"username"] toComment:[[commentArray objectAtIndex:commentArray.count - 3]objectForKey:@"comment"]] ;
//            cell.secondCommentLabel.attributedText= [self getComentsTextConfigured:[[[commentArray objectAtIndex:commentArray.count - 2]objectForKey:@"user"] objectForKey:@"username"] toComment:[[commentArray objectAtIndex:commentArray.count - 2]objectForKey:@"comment"]];
//            cell.thirdCommentLabel.attributedText = [self getComentsTextConfigured:[[[commentArray objectAtIndex:commentArray.count - 1]objectForKey:@"user"] objectForKey:@"username"] toComment:[[commentArray objectAtIndex:commentArray.count - 1]objectForKey:@"comment"]];
//            
//          
//            
//        }else if (commentArray.count==2){
//           
//            
//            cell.thirdCommentViewHeight.constant = 0;
//            
//            cell.commentCountLabel.text = @"";
//            cell.firstCommentLabel.attributedText = [self getComentsTextConfigured:[[[commentArray objectAtIndex:0]objectForKey:@"user"] objectForKey:@"username"] toComment:[[commentArray objectAtIndex:0]objectForKey:@"comment"]] ;
//            cell.secondCommentLabel.attributedText= [self getComentsTextConfigured:[[[commentArray objectAtIndex:1]objectForKey:@"user"] objectForKey:@"username"] toComment:[[commentArray objectAtIndex:1]objectForKey:@"comment"]];;
//            cell.thirdCommentLabel.text = @"";
//            
//            
//        }else if (commentArray.count==1){
//            
//            cell.secondCommentViewHeight.constant = 0;
//            cell.thirdCommentViewHeight.constant = 0;
//            
//            cell.commentCountLabel.text = @"";
//            cell.firstCommentLabel.attributedText = [self getComentsTextConfigured:[[[commentArray objectAtIndex:0]objectForKey:@"user"] objectForKey:@"username"] toComment:[[commentArray objectAtIndex:0]objectForKey:@"comment"]] ;
//            cell.secondCommentLabel.text= @"";
//            cell.thirdCommentLabel.text = @"";
//
//        }
//        
//        
//    }
//    
//    //NSLog(@"comment array %@",commentArray);
//    
//    //make comment label clickable
//    self.tapFirstcommentLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnCommentLabel:)];
//    self.tapFirstcommentLabel.numberOfTapsRequired = 1;
//    [cell.firstCommentLabel addGestureRecognizer:self.tapFirstcommentLabel];
//    cell.firstCommentLabel.userInteractionEnabled = YES;
//    
//    self.tapSecondcommentLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnCommentLabel:)];
//    self.tapSecondcommentLabel.numberOfTapsRequired = 1;
//    [cell.secondCommentLabel addGestureRecognizer:self.tapSecondcommentLabel];
//    cell.secondCommentLabel.userInteractionEnabled = YES;
//    
//    self.tapThirdcommentLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnCommentLabel:)];
//    self.tapThirdcommentLabel.numberOfTapsRequired = 1;
//    [cell.thirdCommentLabel addGestureRecognizer:self.tapThirdcommentLabel];
//    cell.thirdCommentLabel.userInteractionEnabled = YES;
//    
//    self.tapcommentCountLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnCommentLabel:)];
//    self.tapcommentCountLabel.numberOfTapsRequired = 1;
//    [cell.commentCountLabel addGestureRecognizer:self.tapcommentCountLabel];
//    cell.commentCountLabel.userInteractionEnabled = YES;
//    
//    //set comment label  tag
//    cell.firstCommentLabel.tag=indexPath.section;
//    cell.secondCommentLabel.tag=indexPath.section;
//    cell.thirdCommentLabel.tag=indexPath.section;
//    cell.commentCountLabel.tag=indexPath.section;
//
    
       //
    
    PatternTapResponder hashTagTapAction = ^(NSString *tappedString){
        NSLog(@"You have tapped hashTag: %@",tappedString);
        [self moveToHashTagExploreView:tappedString];
    };
    PatternTapResponder userTapAction = ^(NSString *tappedString){
        NSLog(@"You have tapped user: %@",tappedString);
        [self moveToUserView:tappedString];
    };
    
    cell.descriptionLabel.userInteractionEnabled=YES;
    [cell.descriptionLabel enableHashTagDetectionWithAttributes:@{NSForegroundColorAttributeName:[UIColor blueColor],
                                                                  RLHighlightedBackgroundColorAttributeName:[UIColor clearColor],NSBackgroundColorAttributeName:[UIColor clearColor],RLHighlightedBackgroundCornerRadius:@5,
                                                                  RLTapResponderAttributeName:hashTagTapAction}];
    
    [cell.descriptionLabel enableUserHandleDetectionWithAttributes:@{NSForegroundColorAttributeName:[UIColor blueColor],
                                                                     RLHighlightedBackgroundColorAttributeName:[UIColor clearColor],NSBackgroundColorAttributeName:[UIColor clearColor],RLHighlightedBackgroundCornerRadius:@5,
                                                                     RLTapResponderAttributeName:userTapAction}];

    
//
//    PatternTapResponder hashTagTapAction = ^(NSString *tappedString){
//        NSLog(@"You have tapped hashTag: %@",tappedString);
//        [self moveToHashTagExploreView:tappedString];
//    };
//    
//    PatternTapResponder userTapAction = ^(NSString *tappedString){
//        NSLog(@"You have tapped user: %@",tappedString);
//        [self moveToUserView:tappedString];
//    };
//
//    
//    [cell.hashTagLabel enableHashTagDetectionWithAttributes:@{NSForegroundColorAttributeName:[UIColor blueColor],
//                                                                 RLHighlightedBackgroundColorAttributeName:[UIColor clearColor],NSBackgroundColorAttributeName:[UIColor clearColor],RLHighlightedBackgroundCornerRadius:@5,
//                                                                 RLTapResponderAttributeName:hashTagTapAction}];
//
//    
//    [cell.hashTagLabel enableUserHandleDetectionWithAttributes:@{NSForegroundColorAttributeName:[UIColor blueColor],
//                                                                 RLHighlightedBackgroundColorAttributeName:[UIColor clearColor],NSBackgroundColorAttributeName:[UIColor clearColor],RLHighlightedBackgroundCornerRadius:@5,
//                                                                 RLTapResponderAttributeName:userTapAction}];
    



    //setting date label
    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    NSDateFormatter *serverDateFormatter = [[NSDateFormatter alloc] init];
    
    NSString* formatString = [[feedList objectAtIndex:indexPath.section] objectForKey:@"created"];
    
//    [serverDateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ssz"];
//    
//    NSDate *postDate = [serverDateFormatter dateFromString:formatString];
//    
//    [dateFormatter setDateFormat:@"EEE MMM d yyyy, HH:mm"];
//    
    cell.dateLabel.text =[self relativeDateStringForDate:[self getDateFromString:formatString]];
    
    
    
    //make likecount label clickable
    
    self.taplikeLabel = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnLikeLabel:)];
//    self.taplikeLabel.minimumPressDuration = 0.1;
    [cell.likeButton addGestureRecognizer:self.taplikeLabel];
//    cell.likeCountLabel.userInteractionEnabled = YES;
//
//    //set label button tag
//    cell.likeCountLabel.tag=indexPath.section;
    
    
    //for test
//    NSLog(@"desctiption view : %f",cell.descriptionViewHeight.constant);
//    NSLog(@"hashtag view : %f",cell.hashTagViewHeight.constant);
//    NSLog(@"1st coment : %f",cell.firstCommentViewHeight.constant);
//    NSLog(@"2nd comment : %f",cell.secondCommentViewHeight.constant
//          );
//

  
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
   
    return 58;
    
}


#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    

    
    
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    // Change 10.0 to adjust the distance from bottom
    if(selectedTab!=4 && selectedTab!=5)
    {
        if (maximumOffset - currentOffset <= 40.0 && !islastItemReached) {
            NSLog(@"true");
            
            [self makeRequestwithPage];
        }
    }
    
}

//-(void) changeConstraintForNoLike:(FeedTableViewCell*)cell {
//
//    cell.likeCountLabel.text = @"";
//    cell.likeIconHeight.constant = 0;
//    cell.likeIconWidth.constant = 0;
//    cell.likeIconTop.constant = 0;
//    cell.constraintBtLikeIconAndDesView.constant = 0;
//    
//    cell.commentCountLeading.constant = -5;
//
//}




-(void)tapOnPostImage:(UITapGestureRecognizer *)recognizer
{
    if(recognizer.view.subviews.count)
    {
        for (UIView *v in recognizer.view.subviews) {
            [v removeFromSuperview];
        }
    }
    else
        [self setupTaggedUserView:[[feedList objectAtIndex:recognizer.view.tag] objectForKey:@"tagged_users"] inImageView:recognizer.view];
}

-(void)doubleTapOnPostImage:(UITapGestureRecognizer *)recognizer
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:recognizer.view.tag];
  
    FeedTableViewCell *cell = (FeedTableViewCell*) [self.feedTableView cellForRowAtIndexPath:indexPath];
    
    [self performSelector:@selector(likeButtonAction:) withObject:cell.likeButton afterDelay:0];
    
}



-(void) setupTaggedUserView:(NSMutableArray*) taggedUsers inImageView:(UIView*)targetedImage
{
    
    
    for (int i=0; i<taggedUsers.count; i++) {
        
        NSLog(@"targetedImage %ld",(long)targetedImage.tag);
        
        UIFont *font = [UIFont fontWithName:@"AzoSans-Regular" size:12];
        NSDictionary *userAttributes = @{NSFontAttributeName: font,
                                         NSForegroundColorAttributeName: [UIColor blackColor]};
        
        NSString *text =[[[taggedUsers objectAtIndex:i] objectForKey:@"user"] objectForKey:@"username"];
        const CGSize textSize = [text sizeWithAttributes: userAttributes];
        
        //NSLog(@"width = %f, height = %f", textSize.width, textSize.height);
        //
        float xposition=[[[taggedUsers objectAtIndex:i] objectForKey:@"x"] floatValue];
        float yposition=[[[taggedUsers objectAtIndex:i] objectForKey:@"y"]floatValue ];
        
       // NSLog(@"relative X position:%f,Y position %f",xposition,yposition);
        NSLog(@"relative %@",[taggedUsers objectAtIndex:i]);
        
        
        UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(xposition*targetedImage.frame.size.width - textSize.width/2, yposition*targetedImage.frame.size.height, textSize.width +10, 30)];
        fromLabel.text = text;
        fromLabel.font = [UIFont fontWithName:@"AzoSans-Regular" size:12];
        fromLabel.numberOfLines = 1;
        fromLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines; // or UIBaselineAdjustmentAlignCenters, or UIBaselineAdjustmentNone
        fromLabel.adjustsFontSizeToFitWidth = YES;
        fromLabel.adjustsLetterSpacingToFitWidth = YES;
        fromLabel.minimumScaleFactor = 10.0f/12.0f;
        fromLabel.clipsToBounds = YES;
        fromLabel.layer.cornerRadius=3.0;
        fromLabel.backgroundColor = [UIColor blackColor];
        fromLabel.textColor = [UIColor whiteColor];
        fromLabel.textAlignment = NSTextAlignmentCenter;
        fromLabel.userInteractionEnabled=YES;
        fromLabel.tag=[[[taggedUsers objectAtIndex:i] objectForKey:@"user_id"] integerValue];
        
//        UIImageView *arrowIcon = [[UIImageView alloc]initWithFrame:CGRectMake(textSize.width+10 -5, yPosition +10, 10, 10)];
//        arrowIcon.image = [UIImage imageNamed:@"Arrow"];
//        [fromLabel addSubview:arrowIcon];
//        
        [targetedImage addSubview:fromLabel];
        
        UITapGestureRecognizer* labelTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnTaggedLabel:)];
        labelTapGesture.numberOfTapsRequired = 1;
        [fromLabel addGestureRecognizer:labelTapGesture];
        
    }
    
    //get text size
    
}

//likecount label click action

-(void)tapOnLikeLabel:(UILongPressGestureRecognizer *)recognizer{
    
    if (recognizer.state == UIGestureRecognizerStateChanged)
    {
        NSLog(@"Change");
    }
    else if (recognizer.state == UIGestureRecognizerStateEnded) {
        NSLog(@"Long press Ended");
        
        
        
    } else if (recognizer.state == UIGestureRecognizerStateBegan) {
        NSLog(@"Long press detected.");
        LikersViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"LikersViewController"];
        controller.postId =[[[feedList objectAtIndex:recognizer.view.tag ] objectForKey:@"id"]intValue];
        
        [self.navigationController pushViewController:controller animated:YES];
    }
    
    
    
}


//comment label click action

-(void)tapOnCommentLabel:(UITapGestureRecognizer *)recognizer{

    NSMutableArray* tempCommentArray = [[NSMutableArray alloc]initWithArray:[[feedList objectAtIndex:[recognizer view].tag] objectForKey:@"post_comments"]];

    CommentViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentViewController"];

    controller.commentList = tempCommentArray;
    controller.postId = [[[feedList objectAtIndex:recognizer.view.tag ] objectForKey:@"id"]intValue];
    controller.postBelongsToUserId =[[[feedList objectAtIndex:recognizer.view.tag ] objectForKey:@"user_id"] intValue];
    
    NSLog(@"comments in home %@",controller.commentList);
   
    
    [self.navigationController pushViewController:controller animated:YES];


}

//cell header click action

-(void)tapHeader:(UITapGestureRecognizer *)recognizer{
    
    NSLog(@"tap on header");
    
    ProfileViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    
    controller.isUserOrFollower = YES;
    controller.userId = [[[feedList objectAtIndex:recognizer.view.tag ] objectForKey:@"user_id"]intValue];
    
    [self.navigationController pushViewController:controller animated:YES];
    
}

-(void)tapOnTaggedLabel:(UITapGestureRecognizer *)recognizer{
    
    NSLog(@"tap on label");
    
    ProfileViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    
    controller.isUserOrFollower = YES;
    controller.userId = (int)recognizer.view.tag;
    
    [self.navigationController pushViewController:controller animated:YES];
    
}

-(void)tapOnArtistName:(UITapGestureRecognizer *)recognizer{
    
    NSLog(@"tap on artist");
    
    UILabel *artistLabel=(UILabel*)[recognizer view];
    
    ArtistPostViewController * htController = [self.storyboard instantiateViewControllerWithIdentifier:@"ArtistPostViewController"];
    
    htController.artistName = artistLabel.text;
    [self.navigationController pushViewController:htController animated:YES];
    
    
}

-(void)tapOnArtistNameInView:(UITapGestureRecognizer *)recognizer{
    
    NSLog(@"tap on artist");
    
    
    ArtistPostViewController * htController = [self.storyboard instantiateViewControllerWithIdentifier:@"ArtistPostViewController"];
    
    htController.artistName = [[feedList objectAtIndex:[recognizer view].tag] objectForKey:@"artist"];
    [self.navigationController pushViewController:htController animated:YES];
    
    
}



- (IBAction)likeButtonAction:(UIButton*)sender {
    
    NSLog(@"like button clicked");
    sender.userInteractionEnabled = NO;
    
    
    sender.selected=!sender.selected;
    
    NSMutableDictionary *temDic = [[NSMutableDictionary alloc]initWithDictionary:[feedList objectAtIndex:sender.tag]];
    [temDic removeObjectForKey:@"is_liked"];
    [temDic setObject:[NSNumber numberWithBool:sender.selected] forKey:@"is_liked"];
    
    
    int counter;
    if(sender.selected)
        counter=[[temDic objectForKey:@"post_likes"] intValue]+1;
    else
        counter=[[temDic objectForKey:@"post_likes"] intValue]-1;
    
    [temDic removeObjectForKey:@"post_likes"];
    [temDic setObject:[NSNumber numberWithInt:counter] forKey:@"post_likes"];
    
    [feedList replaceObjectAtIndex:sender.tag withObject:temDic];

    [self.feedTableView reloadData];
    
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    [postData setObject:ACCESS_KEY forKey:@"access_key"];
    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"user_id"];
    [postData setObject:[NSNumber numberWithInt:[[[feedList objectAtIndex:sender.tag] objectForKey:@"id"] intValue]] forKey:@"post_id"] ;
    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];

    NSLog(@"postData %@",postData);
    
    [apiLoginManager POST:[NSString stringWithFormat:@"%@/posts/api-swap-like",SERVER_BASE_API_URL] parameters:postData
                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                  
                  //NSLog(@"Response: %@", responseObject);
                      
                      sender.userInteractionEnabled = YES;
                      
                      NSString * successMsg = [responseObject objectForKey:@"success"];

                      if ([successMsg integerValue] == 1 ) {
                          
                          NSLog(@"success msg %@",[responseObject objectForKey:@"success"]);
                          
                          
                         // [self.feedTableView reloadData];
                          
                      }
                  
                  
                  }
                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                      
                      NSLog(@"Error: %@", error);
                      
                      sender.userInteractionEnabled = YES;
                      
                      sender.selected=!sender.selected;
                      
                      NSMutableDictionary *temDic = [[NSMutableDictionary alloc]initWithDictionary:[feedList objectAtIndex:sender.tag]];
                      [temDic removeObjectForKey:@"is_liked"];
                      [temDic setObject:[NSNumber numberWithBool:sender.selected] forKey:@"is_liked"];
                      
                      int counter;
                      if(sender.selected)
                          counter=[[temDic objectForKey:@"post_likes"] intValue]+1;
                      else
                          counter=[[temDic objectForKey:@"post_likes"] intValue]-1;
                      
                      [temDic removeObjectForKey:@"post_likes"];
                      [temDic setObject:[NSNumber numberWithInt:counter] forKey:@"post_likes"];
                      
                      
                      [feedList replaceObjectAtIndex:sender.tag withObject:temDic];
                      [self.feedTableView reloadData];

                      //NSLog(@"%@",operation);
                      
                      
                      
                  }];
    
    
}

- (IBAction)commentButtonAction:(UIButton*)sender {
    
    pageIndex--;
    NSMutableArray* tempCommentArray = [[NSMutableArray alloc]initWithArray:[[feedList objectAtIndex:sender.tag] objectForKey:@"post_comments"]];
    
    CommentViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentViewController"];
    
    controller.commentList = tempCommentArray;
    controller.postId = [[[feedList objectAtIndex:sender.tag ] objectForKey:@"id"]intValue];
    controller.postBelongsToUserId =[[[feedList objectAtIndex:sender.tag ] objectForKey:@"user_id"] intValue];
    NSLog(@"comments in home %@",controller.commentList);
    
    
    [self.navigationController pushViewController:controller animated:YES];
    
    
}

- (IBAction)sendButtonAction:(id)sender {
    
    
}
- (IBAction)pinButtonAction:(UIButton*)sender {
    

        NSLog(@"Pin button clicked");
        sender.userInteractionEnabled = NO;
        sender.selected=!sender.selected;
        
        NSMutableDictionary *temDic = [[NSMutableDictionary alloc]initWithDictionary:[feedList objectAtIndex:sender.tag]];
        [temDic removeObjectForKey:@"is_pinned"];
        [temDic setObject:[NSNumber numberWithBool:sender.selected] forKey:@"is_pinned"];
    
    int pinCount=[[temDic objectForKey:@"pin_count"]intValue];
    
    if(sender.isSelected)
    {
        [temDic setObject:[NSNumber numberWithInt:pinCount+1] forKey:@"pin_count"];
        
        NSLog(@"slected %i",[[temDic objectForKey:@"pin_count"]intValue]);
        
    }else
    {
        
        [temDic setObject:[NSNumber numberWithInt:pinCount-1] forKey:@"pin_count"];
        NSLog(@"not slected %i",[[temDic objectForKey:@"pin_count"]intValue]);
        
    }

    
        [feedList replaceObjectAtIndex:sender.tag withObject:temDic];
    
    
    
        
         [self.feedTableView reloadData];
        
        AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
        apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
        [postData setObject:ACCESS_KEY forKey:@"access_key"];
        [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"user_id"];
        [postData setObject:[NSNumber numberWithInt:[[[feedList objectAtIndex:sender.tag] objectForKey:@"id"] intValue]] forKey:@"post_id"] ;
        [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
        NSLog(@"postData %@",postData);
        
        [apiLoginManager POST:[NSString stringWithFormat:@"%@/posts/api-pin-post",SERVER_BASE_API_URL] parameters:postData
                      success:^(AFHTTPRequestOperation *operation, id responseObject) {
                          
                          NSLog(@"Response: %@", responseObject);
                          
                          sender.userInteractionEnabled = YES;
                          
                          NSString * successMsg = [responseObject objectForKey:@"success"];
                          
                          if ([successMsg integerValue] == 1 ) {
                              
                              NSLog(@"success msg %@",[responseObject objectForKey:@"success"]);
                              [self.feedTableView reloadData];
                              
                              
                          }
                          
                      }
                      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                          
                          NSLog(@"Error: %@", error);
                          
                          sender.userInteractionEnabled = YES;
                          
                          sender.selected=!sender.selected;
                          
                          NSMutableDictionary *temDic = [[NSMutableDictionary alloc]initWithDictionary:[feedList objectAtIndex:sender.tag]];
                         [temDic removeObjectForKey:@"is_pinned"];
                          [temDic setObject:[NSNumber numberWithBool:sender.selected] forKey:@"is_pinned"];
                          
                          if(sender.selected)
                          {
                              [temDic setObject:[NSNumber numberWithInt:[[temDic objectForKey:@"pin_count"]intValue]+1] forKey:@"pin_count"];
                              
                          }else
                          {
                              [temDic setObject:[NSNumber numberWithInt:[[temDic objectForKey:@"pin_count"]intValue]-1] forKey:@"pin_count"];
                              
                          }
                          

                          
                          [feedList replaceObjectAtIndex:sender.tag withObject:temDic];
                          [self.feedTableView reloadData];
                          
                          //NSLog(@"%@",operation);
                          
                      }];
    

}

-(NSMutableAttributedString*) getComentsTextConfigured:(NSString*)user toComment:(NSString*)comment
{
    UIFont *commentFont = [UIFont fontWithName:@"AzoSans-Regular" size:12];
    UIFont *userFont = [UIFont fontWithName:@"AzoSans-Medium" size:13];
    
    NSDictionary *userDict = [NSDictionary dictionaryWithObject: userFont forKey:NSFontAttributeName];
    NSDictionary *commentDict = [NSDictionary dictionaryWithObject:commentFont forKey:NSFontAttributeName];
    
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:user attributes: userDict];
    
    NSMutableAttributedString *vAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",comment] attributes:commentDict];

    
    [aAttrString appendAttributedString:vAttrString];
    
    return aAttrString;
}

-(void)updateBadgeValue
{
    [badgeView setBadgeValue: [UserAccount sharedManager].pendingMessageCount];
    if([UserAccount sharedManager].pendingMessageCount==0)
        [badgeView setHidden:YES];
    else
         [badgeView setHidden:NO];

}

- (IBAction)moveToInbox:(id)sender {
  
    [UserAccount sharedManager].pendingMessageCount=0;
    
    [badgeView setBadgeValue: [UserAccount sharedManager].pendingMessageCount];
    [badgeView setHidden:YES];
    
    
    
    ExistingMessageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ExistingMessageViewController"];
    
    UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:controller];
    navCon.navigationBar.hidden=YES;
    
    [self presentViewController:navCon animated:YES completion:nil];
    
    
//    MessagingViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"MessagingViewController"];
//    [self.navigationController pushViewController:controller animated:YES];
//    
}

- (IBAction)moreButtonAction:(UIButton *)sender {
    
    
    NSLog(@"%ld",(long)sender.tag);
    
     sharePostDictionary = [[NSMutableDictionary alloc]initWithDictionary:[feedList objectAtIndex:sender.tag]];

    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    
    FeedTableViewCell *cell = (FeedTableViewCell*) [self.feedTableView cellForRowAtIndexPath:indexPath];
    imageViewForShare=cell.feedImage;

    
    if ([[sharePostDictionary objectForKey:@"user_id"]integerValue] == [UserAccount sharedManager].userId) {
        
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                             destructiveButtonTitle:nil
                                                  otherButtonTitles:NSLocalizedString(@"Delete", nil),NSLocalizedString(@"Edit", nil),NSLocalizedString(@"Share", nil),NSLocalizedString(@"Share via Whatsapp", nil),NSLocalizedString(@"Send via message", nil), nil];
        
        
        sheet.tag = 1;

//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//            //  device is an iPad.
//          [sheet showFromRect:CGRectMake(([UIScreen mainScreen].bounds.size.width)/3, ([UIScreen mainScreen].bounds.size.height)/2, 200, 200) inView:self.view animated:YES];
//        }
//        else{
        
          [sheet showInView:self.view];
        //}
        
    }else{
    
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                             destructiveButtonTitle:nil
                                                  otherButtonTitles:NSLocalizedString(@"Report user", nil),NSLocalizedString(@"Share to Facebook", nil),NSLocalizedString(@"Share to Instagram", nil),NSLocalizedString(@"Share via Whatsapp", nil), NSLocalizedString(@"Send via message", nil),nil];
        
        
        sheet.tag = 2;
        
//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//            //  device is an iPad.
//            //window.rootViewController = [[UIViewController alloc] init];
//            
//            [sheet showFromRect:CGRectMake(([UIScreen mainScreen].bounds.size.width)/3, ([UIScreen mainScreen].bounds.size.height)/2, 200, 200) inView:self.view animated:YES];
//
//        }
//        else{
        
            [sheet showInView:self.view];
       // }
    }
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (actionSheet.tag == 1) {
        
        if (buttonIndex == 0) {
            
            NSLog(@"dic  %@",sharePostDictionary);
            
            AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
            apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
            
            NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
            [postData setObject:ACCESS_KEY forKey:@"access_key"];
            [postData setObject:[NSNumber numberWithInt:[[sharePostDictionary objectForKey:@"id"] intValue]] forKey:@"post_id"] ;
            [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
            NSLog(@"postData %@",postData);
            
            [apiLoginManager POST:[NSString stringWithFormat:@"%@/posts/apiDeletePost",SERVER_BASE_API_URL] parameters:postData
                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
                              
                              NSLog(@"Response: %@", responseObject);
                              
                              NSString * successMsg = [responseObject objectForKey:@"success"];
                              if ([successMsg integerValue] == 1 ) {
                                  
                                  NSLog(@"success msg %@",[responseObject objectForKey:@"success"]);
                                  
                                  [feedList removeObject:sharePostDictionary];
                                  
                                  [self.feedTableView reloadData];
                                  
                              }
                              
                              
                          }
                          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                              
                              NSLog(@"Error: %@", error);

                              [self.feedTableView reloadData];
                              
                              //NSLog(@"%@",operation);
                              
                              
                              
                          }];


            
        }else if (buttonIndex == 1)
        {
            
            
            SharePhotoViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SharePhotoViewController"];
            controller.selectedImageForShare=imageViewForShare.image;
            controller.isForEdit=YES;
            controller.isForShare=NO;
            controller.postDetails=sharePostDictionary;
            //[self presentViewController:controller animated:NO completion:nil];
            
            [self.navigationController pushViewController:controller animated:YES];
            
            
        }else if (buttonIndex == 2)
        {
            
            SharePhotoViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SharePhotoViewController"];
            controller.selectedImageForShare=imageViewForShare.image;
            controller.isForShare=YES;
            controller.isForEdit=NO;
            controller.postDetails=sharePostDictionary;
            //[self presentViewController:controller animated:NO completion:nil];
            
            [self.navigationController pushViewController:controller animated:YES];
        }
        else if (buttonIndex == 3)
        {
            if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"whatsapp://app"]]){
                
                UIImage     * iconImage = imageViewForShare.image;
                NSString    * savePath  = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/whatsAppTmp.wai"];
                
                [UIImageJPEGRepresentation(iconImage, 1.0) writeToFile:savePath atomically:YES];
                
                self.documentController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:savePath]];
                self.documentController.UTI = @"net.whatsapp.image";
                self.documentController.delegate = self;
                
                [self.documentController presentOpenInMenuFromRect:CGRectMake(0, 0, 0, 0) inView:self.view animated: YES];
                
                
            } else {
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"WhatsApp not installed." message:@"Your device has no WhatsApp installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
        }
        else if (buttonIndex == 4)
        {
            
            MessagingViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"MessagingViewController"];
            controller.dataPayload=[[NSMutableDictionary alloc] initWithDictionary:sharePostDictionary];
            
            [UIPasteboard generalPasteboard].image=imageViewForShare.image;
            UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:controller];
            navCon.navigationBar.hidden=YES;
            
            [self presentViewController:navCon animated:YES completion:nil];
            
            
        }
        
    }
    else{
        
//        UIImageView *postImage = [[UIImageView alloc]init];
//        
//        [postImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL,[sharePostDictionary objectForKey:@"image"]]]];

       // NSLog(@"sharePostDictionary %@ %@",sharePostDictionary,postImage.image);
        
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        
        pasteboard.string = [NSString stringWithFormat:@"%@\n%@",[sharePostDictionary objectForKey:@"description"],[sharePostDictionary objectForKey:@"hashtags"]];
        
        
        if (buttonIndex == 0) {
            
            //[self performSelector:@selector(secondMethod) withObject:nil afterDelay:1.0 ];
            
            AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
            apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
            
            NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
            [postData setObject:ACCESS_KEY forKey:@"access_key"];
            [postData setObject:[NSNumber numberWithInt:[[sharePostDictionary objectForKey:@"user_id"] intValue]] forKey:@"suspect_id"] ;
            [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"user_id"] ;
            [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
            NSLog(@"postData %@",postData);
            
            [apiLoginManager POST:[NSString stringWithFormat:@"%@/users/api-report-user",SERVER_BASE_API_URL] parameters:postData
                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
                              
                              NSLog(@"response object %@",responseObject);
                              
                              if ([[responseObject objectForKey:@"success"] intValue] == 1 ) {
                               
                                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                                  message:@"Your report has been submitted."
                                                                                 delegate:nil
                                                                        cancelButtonTitle:@"OK"
                                                                        otherButtonTitles:nil];
                                  [alert show];
                              }
                              
                              else if ([[responseObject objectForKey:@"success"] intValue] == 2 ) {
                                  
                                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                                  message:@"You have already reported this user."
                                                                                 delegate:nil
                                                                        cancelButtonTitle:@"OK"
                                                                        otherButtonTitles:nil];
                                  [alert show];
                              }
                              
                          }
                          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                              
                              NSLog(@"Error: %@", error);
                              
                              
                              NSLog(@"responseString: %@", operation.responseString);
                              
                              
                              
                          }];
            
        }
//        else if (buttonIndex == 1)
//        {
//        //Block user
//            
//            AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
//            apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
//            
//            NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
//            [postData setObject:ACCESS_KEY forKey:@"access_key"];
//            [postData setObject:[NSNumber numberWithInt:[[sharePostDictionary objectForKey:@"user_id"] intValue]] forKey:@"suspect_id"] ;
//            [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"user_id"] ;
//            
//          //  NSLog(@"postData %@",postData);
//            
//            [apiLoginManager POST:[NSString stringWithFormat:@"%@/users/api-swap-block-user",SERVER_BASE_API_URL] parameters:postData
//                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                              
//                              //NSLog(@"response object %@",responseObject);
//                              
//                              if ([[responseObject objectForKey:@"success"] intValue] == 1 ) {
//                                  
//                                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
//                                                                                  message:[NSString stringWithFormat:@"%@ has been blocked from being able to access your profile and posts. You will also no longer appear in any searches initiated by %@.",[sharePostDictionary objectForKey:@"user_id"],[sharePostDictionary objectForKey:@"user_id"]]
//                                                                                 delegate:nil
//                                                                        cancelButtonTitle:@"OK"
//                                                                        otherButtonTitles:nil];
//                                  [alert show];
//                              }
//                              
//                              else {
//                                  
//                                }
//                              
//                          }
//                          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                              
//                              NSLog(@"Error: %@", error);
//                              
//                              
//                              NSLog(@"responseString: %@", operation.responseString);
//                              
//                              
//                              
//                          }];
//
//        }
        
        else if (buttonIndex == 1)
        {
            FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
            photo.image = imageViewForShare.image;
            photo.userGenerated = YES;
            photo.caption = [NSString stringWithFormat:@"%@",[sharePostDictionary objectForKey:@"description"]];
            FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
            content.photos = @[photo];
            [FBSDKShareDialog showFromViewController:self
                                         withContent:content
                                            delegate:nil];
            
           // NSLog(@"photo %@",photo);
            
           // NSLog(@"post dictionary  %@",sharePostDictionary);
            
        }else if (buttonIndex == 2)
        {
            
           
            NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
            if([[UIApplication sharedApplication] canOpenURL:instagramURL]) //check for App is install or not
            {
                NSData *imageData = UIImagePNGRepresentation(imageViewForShare.image); //convert image into .png format.
                NSFileManager *fileManager = [NSFileManager defaultManager];//create instance of NSFileManager
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //create an array and store result of our search for the documents directory in it
                NSString *documentsDirectory = [paths objectAtIndex:0]; //create NSString object, that holds our exact path to the documents directory
                NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"insta.igo"]]; //add our image to the path
                [fileManager createFileAtPath:fullPath contents:imageData attributes:nil]; //finally save the path (image)
                NSLog(@"image saved");
                
                
                
                
                CGRect rect = CGRectMake(0 ,0 , 0, 0);
                UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, self.view.opaque, 0.0);
                [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
                UIGraphicsEndImageContext();
                NSString *fileNameToSave = [NSString stringWithFormat:@"Documents/insta.igo"];
                NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:fileNameToSave];
                NSLog(@"jpg path %@",jpgPath);
                //            NSString *newJpgPath = [NSString stringWithFormat:@"file://%@",jpgPath];
                //            NSLog(@"with File path %@",newJpgPath);
                NSURL *igImageHookFile = [[NSURL alloc] initWithString:[[NSString alloc] initWithFormat:@"file://%@", jpgPath]];;
                NSLog(@"url Path %@",igImageHookFile);
                
                self.documentController.UTI = @"com.instagram.exclusivegram";
                // self.documentController.URL=[NSURL URLWithString:@"instagram://"];
                self.documentController = [self setupControllerWithURL:igImageHookFile usingDelegate:self];
                self.documentController=[UIDocumentInteractionController interactionControllerWithURL:igImageHookFile];
                NSString *caption = @"Add a caption"; //settext as Default Caption
                // self.documentController.annotation=[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",caption],@"InstagramCaption", nil];
                
                self.documentController.annotation=[NSDictionary dictionaryWithObject:caption forKey:@"InstagramCaption"];
                [self.documentController presentOpenInMenuFromRect:rect inView: self.view animated:YES];
                
                //[self.documentController presentOpenInMenuFromRect:CGRectMake(0, 0, 320, 480) inView:self.view animated:YES];
                
                //
                //            if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
                //                [[UIApplication sharedApplication] openURL:instagramURL];
                //            }
            }
            else
            {
                NSLog (@"Instagram not found");
            }
            
            
        }else if (buttonIndex == 3)
        {
            
            if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"whatsapp://app"]]){
                
                UIImage     * iconImage = imageViewForShare.image;
                NSString    * savePath  = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/whatsAppTmp.wai"];
                
                [UIImageJPEGRepresentation(iconImage, 1.0) writeToFile:savePath atomically:YES];
                
                self.documentController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:savePath]];
                self.documentController.UTI = @"net.whatsapp.image";
                self.documentController.delegate = self;
                
                [self.documentController presentOpenInMenuFromRect:CGRectMake(0, 0, 0, 0) inView:self.view animated: YES];
                
                
            } else {
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"WhatsApp not installed." message:@"Your device has no WhatsApp installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            
        }
        else if (buttonIndex == 4)
        {
            
            MessagingViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"MessagingViewController"];
            controller.dataPayload=[[NSMutableDictionary alloc] initWithDictionary:sharePostDictionary];
            
            [UIPasteboard generalPasteboard].image=imageViewForShare.image;
            UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:controller];
            navCon.navigationBar.hidden=YES;
            
            [self presentViewController:navCon animated:YES completion:nil];

            
            
            
        }
    }
  
}

//temporary

-(IBAction)moveToHashTagExploreView:(NSString*)hashTag {
    
    NSMutableDictionary *hashTagTempDic=[[NSMutableDictionary alloc] init];
    [hashTagTempDic setObject:@"0" forKey:@"tagId"];
    [hashTagTempDic setObject:hashTag forKey:@"tagName"];

    
    HashTagViewViewController * htController = [self.storyboard instantiateViewControllerWithIdentifier:@"HashTagViewViewController"];
   
    htController.hashTagInfoDic = hashTagTempDic;
    [self.navigationController pushViewController:htController animated:YES];
    
}





-(IBAction)moveToUserView:(NSString*)userName {
    
    [self.activityIndicatorView startAnimating];
    
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    [postData setObject:ACCESS_KEY forKey:@"access_key"];
    [postData setObject:[userName substringFromIndex:1] forKey:@"username"] ;
    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
    NSLog(@"postData %@",postData);
    
    [apiLoginManager POST:[NSString stringWithFormat:@"%@/users/api-get-user-id-by-username",SERVER_BASE_API_URL] parameters:postData
                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                      
                    //  NSLog(@"Response: %@", responseObject);
                      
                      
                      [self.activityIndicatorView stopAnimating];
                      
                      NSString * successMsg = [responseObject objectForKey:@"success"];
                      
                      if ([successMsg integerValue] == 1 ) {
                          
                          NSLog(@"user_id %@",[responseObject objectForKey:@"user_id"]);
                          
                              ProfileViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
                          
                              controller.isUserOrFollower = YES;
                              controller.userId = [[responseObject objectForKey:@"user_id"] intValue];
                          
                              [self.navigationController pushViewController:controller animated:YES];
                          
                          
                      }
                      
                      
                  }
                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                      
                      NSLog(@"Error: %@", error);
                      
                      [self.activityIndicatorView stopAnimating];
                      
                      
                      //NSLog(@"%@",operation);
                      
                      
                      
                  }];
    
  
}


- (UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate {
    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL: fileURL];
    interactionController.delegate = interactionDelegate;
    return interactionController;
}

- (IBAction)EventsTapped:(id)sender {
    
    self.topSelectedView.hidden=NO;
    self.bottomSelectedView.hidden=NO;
    self.newsLabel.hidden=YES;
    self.feedTableView.hidden = NO;
    
    pageIndex=1;
    selectedTab=1;
    
    self.comingSoonLabel.hidden=NO;
    feedList=[[NSMutableArray alloc] init];
    [self.feedTableView reloadData];

    
    
    [UIView animateWithDuration:0.5f animations:^{
        self.selectedTabXPosition.constant=0;
        self.artNewsButton.selected=NO;
        self.eventsButton.selected=YES;
        [self.view layoutIfNeeded];
    }];
}

- (IBAction)newsTapped:(id)sender {
    
    
    self.topSelectedView.hidden=NO;
    self.bottomSelectedView.hidden=NO;
    
    self.comingSoonLabel.hidden=YES;
    self.feedTableView.hidden = NO;
    
    pageIndex=1;
    selectedTab=2;
    self.newsLabel.hidden=NO;
    feedList=[[NSMutableArray alloc] init];
    [self.feedTableView reloadData];
    
    
    [UIView animateWithDuration:0.5f animations:^{
        self.selectedTabXPosition.constant=([UIScreen mainScreen].bounds.size.width)/2;
        self.artNewsButton.selected=YES;
        self.eventsButton.selected=NO;
        
        [self.view layoutIfNeeded];
    }];
}



-(NSDate *)getDateFromString:(NSString *)dateString
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    //NSLog(@"dateString %@", dateString);
    
    //[formatter setDateFormat:@"EEE, MMM dd, yyyy"];
    NSDate *currentDate=[formatter dateFromString:dateString];
    return currentDate;
}

-(NSString *)getStringFromDate:(NSDate *)date
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd, YYYY"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
   // NSLog(@"stringFromDate %@", stringFromDate);
    return stringFromDate;
}


- (NSString *)relativeDateStringForDate:(NSDate *)date
{
    
    NSCalendarUnit units = NSCalendarUnitDay | NSCalendarUnitWeekOfYear |
    NSCalendarUnitMonth | NSCalendarUnitYear|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond;
    
    // if `date` is before "now" (i.e. in the past) then the components will be positive
    NSDateComponents *components = [[NSCalendar currentCalendar] components:units
                                                                   fromDate:date
                                                                     toDate:[NSDate date]
                                                                    options:0];
    
    //NSLog(@"components %@",components);
    if (components.year > 0) {
        
        return [self getStringFromDate:date];
        
//        if (components.year > 1) {
//            return [NSString stringWithFormat:@"%ld years ago", (long)components.year];
//        } else {
//            return [NSString stringWithFormat:@"%ld year ago", (long)components.year];
//            
//        }
        
        
    } else if (components.month > 0) {
     
        if (components.month > 1) {
            return [self getStringFromDate:date];
            
           // return [NSString stringWithFormat:@"%ld months ago", (long)components.month];
        } else {
            return [NSString stringWithFormat:@"%ld month ago", (long)components.month];
            
        }
        
        
        
    } else if (components.weekOfYear > 0) {
        if (components.weekOfYear > 1) {
            return [NSString stringWithFormat:@"%ld weeks ago", (long)components.weekOfYear];
        } else {
            return [NSString stringWithFormat:@"%ld week ago", (long)components.weekOfYear];
        }
        
        
        
    } else if (components.day > 0) {
        if (components.day > 1) {
            return [NSString stringWithFormat:@"%ld days ago", (long)components.day];
        } else {
            return @"Yesterday";
        }
    } else {
        if (components.hour > 1) {
            return [NSString stringWithFormat:@"%ld hours ago", (long)components.hour];
        }
        else if(components.minute > 1){
            return [NSString stringWithFormat:@"%ld minutes ago", (long)components.minute];
        }
        else
        {
             // return [NSString stringWithFormat:@"less than a minutes"];
           return [NSString stringWithFormat:@"%ld seconds ago", (long)components.second];
        }
        
    }
}

#pragma mark - Pusher Delegate Connection
//////////////////////////////////

- (void)pusher:(PTPusher *)pusher connectionDidConnect:(PTPusherConnection *)connection
{
    NSLog(@"[Pusher] connected to %@", [connection.URL absoluteString]);
}

- (void)pusher:(PTPusher *)pusher connection:(PTPusherConnection *)connection failedWithError:(NSError *)error
{
    if (error) {
        NSLog(@"[Pusher] connection failed: %@", [error localizedDescription]);
    } else {
        NSLog(@"[Pusher] connection failed");
    }
}

- (void)pusher:(PTPusher *)pusher connection:(PTPusherConnection *)connection didDisconnectWithError:(NSError *)error willAttemptReconnect:(BOOL)reconnect
{
    if (error) {
        NSLog(@"[Pusher] didDisconnectWithError: %@ willAttemptReconnect: %@", [error localizedDescription], (reconnect ? @"YES" : @"NO"));
    } else {
        NSLog(@"[Pusher] disconnected");
    }
}

-(void)dealloc{
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

//-(UIImage *)imageManager:(SDWebImageManager *)imageManager transformDownloadedImage:(UIImage *)image withURL:(NSURL *)imageURL
//{
//   // NSLog(@"imageURL : %@",imageURL);
//    UIImage *resizedImage;
//  //  return resizedImage  = [image resizedImage:CGSizeMake(512, 512) interpolationQuality:kCGInterpolationDefault];
//    
//    float MIN_UPLOAD_RESOLUTION=756*756;
//    float factor;
//    float resol = image.size.height*image.size.width;
//    if (resol >MIN_UPLOAD_RESOLUTION){
//        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
//        resizedImage = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
//        return resizedImage;
//
//    }
//    else
//        return image;
//}
//
//- (UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
//    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
//    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
//    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return scaledImage;
//}

@end
