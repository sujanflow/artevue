//
//  CommentTableViewCell.h
//  Artegrams
//
//  Created by Tanvir Palash on 8/14/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResponsiveLabel.h"

@interface CommentTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet ResponsiveLabel *commentLabel;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end
