//
//  CommentViewController.h
//  Artegrams
//
//  Created by Sujan on 8/1/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"

#import "GetUserHashtagViewController.h"

#import "ArteVue-Swift.h"

@interface CommentViewController : UIViewController<UITextViewDelegate,UserHashtagControllerDelegate,SDWebImageManagerDelegate>

@property(nonatomic,strong) NSMutableArray * commentList;

@property int postId;
@property int postBelongsToUserId;

@property (weak, nonatomic) IBOutlet UIView *inputView;

@property (weak, nonatomic) IBOutlet NextGrowingTextView *inputTextField;

@property (weak, nonatomic) IBOutlet UIButton *sendButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inPutViewBottomConstraint;

@end
