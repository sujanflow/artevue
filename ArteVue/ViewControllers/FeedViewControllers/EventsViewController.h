//
//  EventsViewController.h
//  Artegrams
//
//  Created by Sujan on 12/7/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AFNetworking.h"
#import "UIImageView+WebCache.h"
#import <Social/Social.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>


@interface EventsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>


@property (weak, nonatomic) IBOutlet UITableView *eventsTableView;

@property (weak, nonatomic) IBOutlet UILabel *comingsoonLabel;


@end
