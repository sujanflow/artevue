//
//  MessagingViewController.h
//  Artegrams
//
//  Created by Tanvir Palash on 8/4/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIImageView+WebCache.h"
#import "AFNetworking.h"



@interface MessagingViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *messagesTableView;

@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (strong, nonatomic) NSMutableDictionary *dataPayload;

@property BOOL isSearching;

@end
