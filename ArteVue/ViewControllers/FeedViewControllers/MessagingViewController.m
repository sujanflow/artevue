//
//  MessagingViewController.m
//  Artegrams
//
//  Created by Tanvir Palash on 8/4/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "MessagingViewController.h"


#import "DemoMessagesViewController.h"

#import "Constants.h"
#import "Messages.h"
#import "ChatCell.h"
#import "ServerManager.h"


@interface MessagingViewController ()
{
  
   // NSMutableArray* followersList;
    NSMutableArray *searchResults;
    int pageIndex;
}

@end

@implementation MessagingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    pageIndex=1;
    self.searchBar.placeholder = @"Search";
    [self.searchBar setTintColor:[UIColor blackColor]];
    // [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setFont:[UIFont fontWithName:@"Museo-500" size:14]];
    self.searchBar.delegate = self;
    [self.searchBar becomeFirstResponder];
    
    [self setTableView];
    
    self.title = @"Direct";
    
   // [SDWebImageManager sharedManager].delegate=self;
}
-(void)viewWillAppear:(BOOL)animated
{
    
    
    [super viewWillAppear:animated];
    
    //[self makeRequest];
    [self.messagesTableView reloadData];
}
-(void)setTableView
{
    self.messagesTableView.delegate = self;
    self.messagesTableView.dataSource = self;
    self.messagesTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f,self.view.frame.size.width, 10.0f)];
    self.messagesTableView.backgroundColor = [UIColor clearColor];
}


#pragma mark - TableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    if(self.isSearching)
//    {
        return searchResults.count;
//    }
//    else
//        
//        return [followersList count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ChatListCell";
    ChatCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell)
    {
        cell = [[ChatCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSMutableDictionary *singleData;
//    if(self.isSearching)
//    {
        singleData=[[NSMutableDictionary alloc] initWithDictionary:[searchResults objectAtIndex:indexPath.row]];
    
//    }
//    else
//        singleData=[[NSMutableDictionary alloc] initWithDictionary:[followersList objectAtIndex:indexPath.row]];
//    
    cell.nameLabel.text = [singleData objectForKey:@"username"];
    cell.messageLabel.text = @"";
    
    [cell.picture sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[singleData objectForKey:@"profile_picture"]]]];
    
    return cell;
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    NSMutableDictionary *singleData;
    
//    if(self.isSearching)
//    {
        singleData=[[NSMutableDictionary alloc] initWithDictionary:[searchResults objectAtIndex:indexPath.row]];
        
//    }
//    else
//        singleData=[[NSMutableDictionary alloc] initWithDictionary:[followersList objectAtIndex:indexPath.row]];
//    
    
    
    DemoMessagesViewController *vc = [DemoMessagesViewController messagesViewController];
    vc.chatFollowerId=[NSString stringWithFormat:@"%@",[singleData objectForKey:@"id"]];
    vc.chatFollowerName=[singleData objectForKey:@"username"];
    vc.attachmentDataDictionary=[[NSMutableDictionary alloc] initWithDictionary:self.dataPayload];
    [self.navigationController pushViewController:vc animated:YES];
    
    
}
/*
-(void) makeRequest{
    
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    [postData setObject:ACCESS_KEY forKey:@"access_key"];
    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"user_id"];
    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
    
    
    NSLog(@"postData %@",postData);
    
    [apiLoginManager POST:[NSString stringWithFormat:@"%@/users/apiGetTaggableUsers",SERVER_BASE_API_URL] parameters:postData
                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                      
                     
                      followersList = [[NSMutableArray alloc]init];
                      followersList = [responseObject mutableCopy];
                      
                      NSLog(@"Response: %@", followersList);
                      
                      [self.messagesTableView reloadData];
                  }
     
                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                      
                      NSLog(@"Error: %@", error);
                      
                      
                      NSLog(@"%@",operation.responseString);
                      
                  }];
    
}

*/

#pragma mark - UISearchBar Delegate

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
    //self.isSearching=YES;
    [searchBar setShowsCancelButton:YES animated:YES];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
//    if(searchText.length<=0)
//    {
//        self.isSearching=NO;
//        [self.messagesTableView reloadData];
//    }else
//        self.isSearching=YES;
//        [self filterContentForSearchText:searchText
//                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
//                                      objectAtIndex:[self.searchBar
//                                                     selectedScopeButtonIndex]]];
    
    NSLog(@"searchText %@",searchText);
   
    [self searchUser];
    
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.text = @"";
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    self.isSearching=NO;
    
    [self.messagesTableView reloadData];
}

//- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
//{
//    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"username contains[c] %@ ",searchText];
//    
//    searchResults= [[NSMutableArray alloc] initWithArray:[followersList filteredArrayUsingPredicate:resultPredicate]];
//    
//    NSLog(@"search %@",searchResults);
//    
//    [self.messagesTableView reloadData];
//}


- (void)handleSearchError:(NSError *)error
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                   message:error.localizedDescription
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}


-(void) searchUser
{
    
    [[ServerManager sharedManager] searchUserByString:self.searchBar.text forPage:pageIndex withcompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
        
        if ( resultDataArray!=nil) {
            
            searchResults = [[NSMutableArray alloc]init];
            searchResults = [[resultDataArray objectForKey:@"data"] mutableCopy];
            
            NSUInteger theIndex = [searchResults indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                if ([[obj objectForKey:@"id"] intValue] == [UserAccount sharedManager].userId)
                {
                    *stop = YES;
                    NSLog(@"IDX: %d", idx);
                    [searchResults removeObjectAtIndex:idx];
                    [self.messagesTableView reloadData];
                    
                }
                return *stop;
                
            }];
            
            
            [self.messagesTableView reloadData];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
            });
        }
    }];

    
}

- (IBAction)backButtonAction:(id)sender {
    
    if(self.dataPayload)
    {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    else
        [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(UIImage *)imageManager:(SDWebImageManager *)imageManager transformDownloadedImage:(UIImage *)image withURL:(NSURL *)imageURL
//{
//    // NSLog(@"imageURL : %@",imageURL);
//    UIImage *resizedImage;
//    //  return resizedImage  = [image resizedImage:CGSizeMake(512, 512) interpolationQuality:kCGInterpolationDefault];
//    
//    float MIN_UPLOAD_RESOLUTION=756*756;
//    float factor;
//    float resol = image.size.height*image.size.width;
//    if (resol >MIN_UPLOAD_RESOLUTION){
//        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
//        resizedImage = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
//        return resizedImage;
//        
//    }
//    else
//        return image;
//}
//
//- (UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
//    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
//    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
//    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return scaledImage;
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
