//
//  FeedTableViewCell.h
//  Artegram
//
//  Created by Sujan on 7/14/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ResponsiveLabel.h"

@interface FeedTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *feedImage;

@property (weak, nonatomic) IBOutlet UIButton *likeButton;

@property (weak, nonatomic) IBOutlet UIButton *commentButton;

@property (weak, nonatomic) IBOutlet UIButton *sendButton;

@property (weak, nonatomic) IBOutlet UIButton *pinButton;

@property (weak, nonatomic) IBOutlet UIButton *moreButton;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buyButtonHeightConstraint;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sendButtonHeightConstraint;

@property (weak, nonatomic) IBOutlet UILabel *likeCountLabel;

@property (weak, nonatomic) IBOutlet UIImageView *likeIcon;

@property (weak, nonatomic) IBOutlet UILabel *commentCountLabel;

@property (weak, nonatomic) IBOutlet ResponsiveLabel *descriptionLabel;

@property (weak, nonatomic) IBOutlet UILabel *hashTagLabelNormal;


@property (weak, nonatomic) IBOutlet ResponsiveLabel *hashTagLabel;

@property (weak, nonatomic) IBOutlet ResponsiveLabel *firstCommentLabel;

@property (weak, nonatomic) IBOutlet ResponsiveLabel *secondCommentLabel;

@property (weak, nonatomic) IBOutlet ResponsiveLabel *thirdCommentLabel;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *feedImageRatio;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewHeightConstraint;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *likeIconHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *likeIconWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *likeIconTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentCountLeading;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionViewHeight;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hashTagViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstCommentViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secondCommentViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *thirdCommentViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBtLikeIconAndDesView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBtDescriptionAndLike;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBtHashTagAndDescription;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBtHashTagAndFirstComment;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBtFirstAndSecindComment;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBtSecondAndThirdComment;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBtThirdAndDateLabel;

@end
