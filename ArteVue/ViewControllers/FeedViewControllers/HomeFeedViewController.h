//
//  HomeFeedViewController.h
//  Artegram
//
//  Created by Tanvir Palash on 6/10/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"

#import <Social/Social.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <QuartzCore/QuartzCore.h>

//#import "ResponsiveLabel.h"

//#import "NSAttributedString+Processing.h"

#import "Pusher.h"

@interface HomeFeedViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate,PTPusherDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,SDWebImageManagerDelegate>


@property (weak, nonatomic) IBOutlet UITableView *feedTableView;

@property (weak, nonatomic) IBOutlet UICollectionView *postTypeCollectionView;
@property (weak, nonatomic) IBOutlet UIView *topSegmentView;

@property UILongPressGestureRecognizer *taplikeLabel;
@property UITapGestureRecognizer *tapFirstcommentLabel;
@property UITapGestureRecognizer *tapSecondcommentLabel;
@property UITapGestureRecognizer *tapThirdcommentLabel;

@property (weak, nonatomic) IBOutlet UILabel *noDataLabel;
@property (weak, nonatomic) IBOutlet UILabel *comingSoonLabel;
@property (weak, nonatomic) IBOutlet UILabel *newsLabel;

@property (nonatomic, retain) UIDocumentInteractionController *documentController;

@property UITapGestureRecognizer *tapcommentCountLabel;

@property (weak, nonatomic) IBOutlet UIButton *inboxActionButton;

@property (weak, nonatomic) IBOutlet UIButton *eventsButton;
@property (weak, nonatomic) IBOutlet UIButton *artNewsButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectedTabXPosition;
@property (weak, nonatomic) IBOutlet UIView *topSelectedView;
@property (weak, nonatomic) IBOutlet UIView *bottomSelectedView;

@property UIRefreshControl *refreshControl;
@end
