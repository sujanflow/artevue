//
//  Created by Jesse Squires
//  http://www.jessesquires.com
//
//
//  Documentation
//  http://cocoadocs.org/docsets/JSQMessagesViewController
//
//
//  GitHub
//  https://github.com/jessesquires/JSQMessagesViewController
//
//
//  License
//  Copyright (c) 2014 Jesse Squires
//  Released under an MIT license: http://opensource.org/licenses/MIT
//

#import "DemoMessagesViewController.h"
#import "JSQMessagesViewAccessoryButtonDelegate.h"
#import "ViewPostViewController.h"
#import "VIPhotoView.h"

#import "Constants.h"
#import "Messages.h"

#import "Pusher.h"
#import "ServerManager.h"

@interface DemoMessagesViewController () <JSQMessagesViewAccessoryButtonDelegate,PTPusherDelegate>
{
    VIPhotoView *photoView;
    PTPusher *pusherClient;
    PTPusher *pusherClient2;
    
    UIButton *crossButton;
    FusumaViewController *fusumaCameraViewController;

    int pageIndex;
}


@end

@implementation DemoMessagesViewController

#pragma mark - View lifecycle

/**
 *  Override point for customization.
 *
 *  Customize your view.
 *  Look at the properties on `JSQMessagesViewController` and `JSQMessagesCollectionView` to see what is possible.
 *
 *  Customize your layout.
 *  Look at the properties on `JSQMessagesCollectionViewFlowLayout` to see what is possible.
 */
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"self.chatFollowerId %@ %@",self.chatFollowerId,self.chatFollowerName);
    
    NSLog(@"attachmentDataDictionary %@",self.attachmentDataDictionary);
    pageIndex=1;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadChatview) name:@"updateSharedImage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showInputBar) name:@"ShowInputToolbar" object:nil];
    
    
    self.titleLabel.text=self.chatFollowerName;
    
    pusherClient = [PTPusher pusherWithKey:pusher_app_key delegate:self encrypted:YES cluster:@"ap1"];
    pusherClient2 = [PTPusher pusherWithKey:pusher_app_key delegate:self encrypted:YES cluster:@"ap1"];
    
    
    PTPusherChannel *channel = [pusherClient subscribeToChannelNamed:@"message_channel"];
    PTPusherChannel *readChannel = [pusherClient2 subscribeToChannelNamed:[NSString stringWithFormat:@"%i-message-read-channel",[UserAccount sharedManager].userId]];
    
    
    [readChannel bindToEventNamed:@"message-read" handleWithBlock:^(PTPusherEvent *channelEvent) {
        
        NSLog(@"message readby: %@", channelEvent.data);
        
//        if([[channelEvent.data objectForKey:@"receiver_id"] integerValue]== [UserAccount sharedManager].userId)
//        {
//            
//        }
        
        for (JSQMessage *message in self.demoData.messages) {
            message.isRead=YES;
        }
    
        [self.collectionView reloadData];
        
    }];
    
    [channel bindToEventNamed:@"personal-messaging" handleWithBlock:^(PTPusherEvent *channelEvent) {
        
        NSLog(@"message received: %@", channelEvent.data);
        
        JSQMessage*  message;
        
        if([[channelEvent.data objectForKey:@"receiver_id"] integerValue]== [UserAccount sharedManager].userId)
        {
//            message  = [[JSQMessage alloc] initWithSenderId:self.chatFollowerId
//                                                       senderDisplayName:self.chatFollowerName
//                                                                    date:[NSDate date]
//                                                                    text:[channelEvent.data objectForKey:@"message"]];
//            [self.demoData.messages addObject:message];
//            
//            he
            
            [self sendAckowledgement:[[channelEvent.data objectForKey:@"message_id"] intValue]];
            
            if([[channelEvent.data objectForKey:@"is_post"] intValue])
            {
                JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImageUrl:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[channelEvent.data objectForKey:@"url"]] withPostId:[[channelEvent.data objectForKey:@"is_post"] intValue]];
                photoItem.appliesMediaViewMaskAsOutgoing = NO;
                
                message = [JSQMessage messageWithSenderId:self.chatFollowerId
                                              displayName:self.chatFollowerName
                                                    media:photoItem];
                
                
            }
            else
            {
                if([[channelEvent.data objectForKey:@"is_file"] intValue])
                {
                    JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImageUrl:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[channelEvent.data objectForKey:@"url"]] withPostId:[[channelEvent.data objectForKey:@"is_post"] intValue]];
                    photoItem.appliesMediaViewMaskAsOutgoing = NO;
                    
                    message = [JSQMessage messageWithSenderId:self.chatFollowerId
                                                  displayName:self.chatFollowerName
                                                        media:photoItem];
                    
                    
                }
                else
                {
                    message  = [[JSQMessage alloc] initWithSenderId:self.chatFollowerId
                                                  senderDisplayName:self.chatFollowerName
                                                               date:[NSDate date]
                                                               text:[channelEvent.data objectForKey:@"message"]];
                    
                }
                
                
            }
            
            [self.demoData.messages addObject:message];
            self.indexForVisibleDate=self.demoData.messages.count-1;
            
            [self.collectionView reloadData];
            
            

        }
        
        if (self.automaticallyScrollsToMostRecentMessage) {
            [self scrollToBottomAnimated:YES];
        }
//        CGSize r = self.collectionView.contentSize;
//        [self.collectionView scrollRectToVisible:CGRectMake(0, r.height-10, r.width, 10) animated:YES];
//
        
     
     
    }];
    [pusherClient connect];
    [pusherClient2 connect];
    
    self.title = @"ArteVueMessaging";

    self.inputToolbar.contentView.textView.pasteDelegate = self;
    //self.inputToolbar.contentView.leftBarButtonContainerViewWidthConstraint.constant=0;
    /**
     *  Load up our fake data for the demo
     */
    self.demoData = [[DemoModelData alloc] init];
    

    /**
     *  Set up message accessory button delegate and configuration
     */
    self.collectionView.accessoryDelegate = self;

    /**
     *  You can set custom avatar sizes
     */
    if (![NSUserDefaults incomingAvatarSetting]) {
        self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
    }
    
    if (![NSUserDefaults outgoingAvatarSetting]) {
        self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
    }
    
    self.showLoadEarlierMessagesHeader = NO;
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage jsq_defaultTypingIndicatorImage]
//                                                                              style:UIBarButtonItemStyleBordered
//                                                                             target:self
//                                                                             action:@selector(receiveMessagePressed:)];
//
    /**
     *  Register custom menu actions for cells.
     */
    [JSQMessagesCollectionViewCell registerMenuAction:@selector(customAction:)];

	
    /**
     *  OPT-IN: allow cells to be deleted
     */
    [JSQMessagesCollectionViewCell registerMenuAction:@selector(delete:)];

    /**
     *  Customize your toolbar buttons
     *
     *  self.inputToolbar.contentView.leftBarButtonItem = custom button or nil to remove
     *  self.inputToolbar.contentView.rightBarButtonItem = custom button or nil to remove
     */

    /**
     *  Set a maximum height for the input toolbar
     *
     *  self.inputToolbar.maximumHeight = 150;
     */
    
    [self loadMessages];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.delegateModal) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop
                                                                                              target:self
                                                                                              action:@selector(closePressed:)];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    /**
     *  Enable/disable springy bubbles, default is NO.
     *  You must set this from `viewDidAppear:`
     *  Note: this feature is mostly stable, but still experimental
     
     */
    
   
    self.collectionView.collectionViewLayout.springinessEnabled = [NSUserDefaults springinessSetting];
}

//-(void)viewWillDisappear:(BOOL)animated
//{
// 
//    [super viewWillDisappear:YES];
//    [[SDWebImageDownloader sharedDownloader] cancelAllDownloads];
//    
//}

#pragma mark - Custom menu actions for cells

- (void)didReceiveMenuWillShowNotification:(NSNotification *)notification
{
    /**
     *  Display custom menu actions for cells.
     */
    UIMenuController *menu = [notification object];
    menu.menuItems = @[ [[UIMenuItem alloc] initWithTitle:@"Custom Action" action:@selector(customAction:)] ];
    
    [super didReceiveMenuWillShowNotification:notification];
}



#pragma mark - Testing

- (void)pushMainViewController
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *nc = [sb instantiateInitialViewController];
    [self.navigationController pushViewController:nc.topViewController animated:YES];
}

-(void)reloadChatview
{
    [self.collectionView reloadData];
}
-(void)showInputBar
{
    self.inputToolbar.hidden=NO;
    [crossButton removeFromSuperview];
    [photoView removeFromSuperview];
}

#pragma mark - Actions

- (void)receiveMessagePressed:(UIBarButtonItem *)sender
{
     /**
     *  DEMO ONLY
     *
     *  The following is simply to simulate received messages for the demo.
     *  Do not actually do this.
     */
    
    
    /**
     *  Show the typing indicator to be shown
     */
    self.showTypingIndicator = !self.showTypingIndicator;
    
    /**
     *  Scroll to actually view the indicator
     */
    [self scrollToBottomAnimated:YES];
    
    /**
     *  Copy last sent message, this will be the new "received" message
     */
    JSQMessage *copyMessage = [[self.demoData.messages lastObject] copy];
    
    if (!copyMessage) {
        
        copyMessage = [JSQMessage messageWithSenderId:kJSQDemoAvatarIdJobs
                                          displayName:kJSQDemoAvatarDisplayNameJobs
                                                 text:@"First received!"];
    
    }
    
    /**
     *  Allow typing indicator to show
     */
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        NSMutableArray *userIds = [[self.demoData.users allKeys] mutableCopy];
        [userIds removeObject:self.senderId];
        NSString *randomUserId = userIds[arc4random_uniform((int)[userIds count])];
        
        JSQMessage *newMessage = nil;
        id<JSQMessageMediaData> newMediaData = nil;
        id newMediaAttachmentCopy = nil;
        
        if (copyMessage.isMediaMessage) {
            /**
             *  Last message was a media message
             */
            id<JSQMessageMediaData> copyMediaData = copyMessage.media;
            
            if ([copyMediaData isKindOfClass:[JSQPhotoMediaItem class]]) {
                JSQPhotoMediaItem *photoItemCopy = [((JSQPhotoMediaItem *)copyMediaData) copy];
                photoItemCopy.appliesMediaViewMaskAsOutgoing = NO;
                newMediaAttachmentCopy = [UIImage imageWithCGImage:photoItemCopy.image.CGImage];
                
                /**
                 *  Set image to nil to simulate "downloading" the image
                 *  and show the placeholder view
                 */
                photoItemCopy.image = nil;
                
                newMediaData = photoItemCopy;
            }
            else if ([copyMediaData isKindOfClass:[JSQLocationMediaItem class]]) {
                JSQLocationMediaItem *locationItemCopy = [((JSQLocationMediaItem *)copyMediaData) copy];
                locationItemCopy.appliesMediaViewMaskAsOutgoing = NO;
                newMediaAttachmentCopy = [locationItemCopy.location copy];
                
                /**
                 *  Set location to nil to simulate "downloading" the location data
                 */
                locationItemCopy.location = nil;
                
                newMediaData = locationItemCopy;
            }
            else if ([copyMediaData isKindOfClass:[JSQVideoMediaItem class]]) {
                
                JSQVideoMediaItem *videoItemCopy = [((JSQVideoMediaItem *)copyMediaData) copy];
                videoItemCopy.appliesMediaViewMaskAsOutgoing = NO;
                newMediaAttachmentCopy = [videoItemCopy.fileURL copy];
                
                /**
                 *  Reset video item to simulate "downloading" the video
                 */
                videoItemCopy.fileURL = nil;
                videoItemCopy.isReadyToPlay = NO;
                
                newMediaData = videoItemCopy;
            }
            else if ([copyMediaData isKindOfClass:[JSQAudioMediaItem class]]) {
                JSQAudioMediaItem *audioItemCopy = [((JSQAudioMediaItem *)copyMediaData) copy];
                audioItemCopy.appliesMediaViewMaskAsOutgoing = NO;
                newMediaAttachmentCopy = [audioItemCopy.audioData copy];
                
                /**
                 *  Reset audio item to simulate "downloading" the audio
                 */
                audioItemCopy.audioData = nil;
                
                newMediaData = audioItemCopy;
            }
            else {
                NSLog(@"%s error: unrecognized media item", __PRETTY_FUNCTION__);
            }
            
            newMessage = [JSQMessage messageWithSenderId:randomUserId
                                             displayName:self.demoData.users[randomUserId]
                                                   media:newMediaData];
        }
        else {
            /**
             *  Last message was a text message
             */
            newMessage = [JSQMessage messageWithSenderId:randomUserId
                                             displayName:self.demoData.users[randomUserId]
                                                    text:copyMessage.text];
        }
        
        /**
         *  Upon receiving a message, you should:
         *
         *  1. Play sound (optional)
         *  2. Add new id<JSQMessageData> object to your data source
         *  3. Call `finishReceivingMessage`
         */

        // [JSQSystemSoundPlayer jsq_playMessageReceivedSound];

        [self.demoData.messages addObject:newMessage];
        [self finishReceivingMessageAnimated:YES];
        
        
        if (newMessage.isMediaMessage) {
            /**
             *  Simulate "downloading" media
             */
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                /**
                 *  Media is "finished downloading", re-display visible cells
                 *
                 *  If media cell is not visible, the next time it is dequeued the view controller will display its new attachment data
                 *
                 *  Reload the specific item, or simply call `reloadData`
                 */
                
                if ([newMediaData isKindOfClass:[JSQPhotoMediaItem class]]) {
                    ((JSQPhotoMediaItem *)newMediaData).image = newMediaAttachmentCopy;
                    [self.collectionView reloadData];
                }
                else if ([newMediaData isKindOfClass:[JSQLocationMediaItem class]]) {
                    [((JSQLocationMediaItem *)newMediaData)setLocation:newMediaAttachmentCopy withCompletionHandler:^{
                        [self.collectionView reloadData];
                    }];
                }
                else if ([newMediaData isKindOfClass:[JSQVideoMediaItem class]]) {
                    ((JSQVideoMediaItem *)newMediaData).fileURL = newMediaAttachmentCopy;
                    ((JSQVideoMediaItem *)newMediaData).isReadyToPlay = YES;
                    [self.collectionView reloadData];
                }
                else if ([newMediaData isKindOfClass:[JSQAudioMediaItem class]]) {
                    ((JSQAudioMediaItem *)newMediaData).audioData = newMediaAttachmentCopy;
                    [self.collectionView reloadData];
                }
                else {
                    NSLog(@"%s error: unrecognized media item", __PRETTY_FUNCTION__);
                }
                
            });
        }
        
    });
}

- (void)closePressed:(UIBarButtonItem *)sender
{
    
        [self.delegateModal didDismissJSQDemoViewController:self];
   
}

-(void) loadMessages
{
    [[ServerManager sharedManager] getMessageWithUser:self.chatFollowerId inPage:pageIndex WithCompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
       
        if ( resultDataArray!=nil) {
            
         
            NSLog(@"Response: %@", resultDataArray);
            NSMutableArray *messagesArray=[[[[resultDataArray objectForKey:@"data"] reverseObjectEnumerator] allObjects] mutableCopy];
            
            self.demoData.messages =[[NSMutableArray alloc] init];
            
            for (int i = 0; i < messagesArray.count; i++) {
                
                JSQMessage *message;
                
                if([[[messagesArray objectAtIndex:i] objectForKey:@"sender_id"] integerValue]== [self.chatFollowerId integerValue])
                {
                    
                    if([[[messagesArray objectAtIndex:i] objectForKey:@"is_post"] intValue])
                    {
                        JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImageUrl:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[messagesArray objectAtIndex:i] objectForKey:@"url"]] withPostId:[[[messagesArray objectAtIndex:i] objectForKey:@"is_post"] intValue]];
                        photoItem.appliesMediaViewMaskAsOutgoing = NO;
                        
                        message = [JSQMessage messageWithSenderId:self.chatFollowerId
                                                      displayName:self.chatFollowerName
                                                            media:photoItem];
                        
                        
                    }
                    else
                    {
                        if([[[messagesArray objectAtIndex:i] objectForKey:@"is_file"] intValue])
                        {
                            JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImageUrl:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[messagesArray objectAtIndex:i] objectForKey:@"url"]] withPostId:[[[messagesArray objectAtIndex:i] objectForKey:@"is_post"] intValue]];
                            photoItem.appliesMediaViewMaskAsOutgoing = NO;
                            
                            message = [JSQMessage messageWithSenderId:self.chatFollowerId
                                                          displayName:self.chatFollowerName
                                                                media:photoItem];
                            
                        }
                        else
                        {
                            message  = [[JSQMessage alloc] initWithSenderId:self.chatFollowerId
                                                          senderDisplayName:self.chatFollowerName
                                                                       date:[self getDateFromString:[[messagesArray objectAtIndex:i] objectForKey:@"updated_at"]]
                                                                       text:[[messagesArray objectAtIndex:i] objectForKey:@"message"]];
                            
                        }
                    }
                    
                }
                else
                {
                    NSLog(@"i am the sender");
                    if([[[messagesArray objectAtIndex:i] objectForKey:@"is_post"] intValue])
                    {
                        JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImageUrl:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[messagesArray objectAtIndex:i] objectForKey:@"url"]] withPostId:[[[messagesArray objectAtIndex:i] objectForKey:@"is_post"] intValue]];
                        
                        message = [JSQMessage messageWithSenderId:[NSString stringWithFormat:@"%i",[UserAccount sharedManager].userId]
                                                      displayName:[UserAccount sharedManager].userName
                                                            media:photoItem];
                        
                    }
                    else
                    {
                        
                        if([[[messagesArray objectAtIndex:i] objectForKey:@"is_file"] intValue])
                        {
                            JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImageUrl:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[messagesArray objectAtIndex:i] objectForKey:@"url"]] withPostId:[[[messagesArray objectAtIndex:i] objectForKey:@"is_post"] intValue]];
                            
                            message = [JSQMessage messageWithSenderId:[NSString stringWithFormat:@"%i",[UserAccount sharedManager].userId]
                                                          displayName:[UserAccount sharedManager].userName
                                                                media:photoItem];
                            
                        }else
                        {
                            message  = [[JSQMessage alloc] initWithSenderId:[NSString stringWithFormat:@"%i",[UserAccount sharedManager].userId]
                                                          senderDisplayName:[UserAccount sharedManager].userName
                                                                       date:[self getDateFromString:[[messagesArray objectAtIndex:i] objectForKey:@"updated_at"]]
                                                                       text:[[messagesArray objectAtIndex:i] objectForKey:@"message"]];
                            
                        }
                        
                    }
                    
                    if([[[messagesArray objectAtIndex:i] objectForKey:@"is_read"] intValue])
                    {
                        message.isRead=YES;
                    }
                    else
                        message.isRead=NO;
                    
                    
                }
                
                
                [self.demoData.messages addObject:message];
                
                [self.collectionView reloadData];
                
                if (self.automaticallyScrollsToMostRecentMessage) {
                    [self scrollToBottomAnimated:YES];
                }
                
                if(i==messagesArray.count-1)
                {
                    if([self.attachmentDataDictionary allKeys].count)
                    {
                        [self sendPost];
                        //[self sendPhoto];
                    }
                }
                //NSLog(@"self.demoData.messages  %@",self.demoData.messages );
            }
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                //[[[UIAlertView alloc] initWithTitle:@"Sorry, unable to fetch data. Please try again." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                
                
                
            });
            
        }
        
    }];
    

}

-(void) sendAckowledgement: (int) messageId
{
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    [postData setObject:ACCESS_KEY forKey:@"access_key"];
    [postData setObject:[NSNumber numberWithInt:messageId] forKey:@"message_id"];
    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
    NSLog(@"postData %@",postData);
    
    [apiLoginManager POST:[NSString stringWithFormat:@"%@/messages/api-set-message-to-read",SERVER_BASE_API_URL] parameters:postData
                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                      
                      NSLog(@"Response: %@", responseObject);
        
                  }
     
                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                      
                      NSLog(@"Error: %@", error);
                      
                  }];
}


-(void) sendMessages: (NSString *) message
{
    
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    [postData setObject:self.chatFollowerId forKey:@"receiver_id"];
    [postData setObject:message forKey:@"message"];
    
    //for post share
    if([self.attachmentDataDictionary allKeys].count)
    {
        [postData setObject:[self.attachmentDataDictionary objectForKey:@"id"] forKey:@"is_post"];
        [postData setObject:[self.attachmentDataDictionary objectForKey:@"image"] forKey:@"url"];
    }
    else
    {
        [postData setObject:@"0" forKey:@"is_post"];
        [postData setObject:@"" forKey:@"url"];
        
    }
    [postData setObject:@"0" forKey:@"is_file"];
    NSLog(@"postData %@",postData);
    
    
    [[ServerManager sharedManager] postSingleMessageWithData:postData withCompletion:^(BOOL success) {
        
        
        if (success) {
            
            UIPasteboard *pb = [UIPasteboard generalPasteboard];
            [pb setValue:@"" forPasteboardType:UIPasteboardNameGeneral];
            self.attachmentDataDictionary=nil;
            

        }
    
    }];

    
}

-(void) sendMessageWithPhoto : (UIImage *)image
{
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    [postData setObject:self.chatFollowerId forKey:@"receiver_id"];
    [postData setObject:@"Attached Photo" forKey:@"message"];
    [postData setObject:@1 forKey:@"is_file"];
    //for post share
    
    [postData setObject:@"0" forKey:@"is_post"];
    
    [[ServerManager sharedManager] postSingleMessageWithImage:image AndData:postData withCompletion:^(BOOL success) {
        
        if (success) {
           
            
            
        }
        
    }];

    

}

-(void) sendPost
{
    
//    JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImageUrl:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL,[self.attachmentDataDictionary objectForKey:@"image"]] withPostId:[[self.attachmentDataDictionary objectForKey:@"id"] intValue]];;
//    
    
    JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:[UIPasteboard generalPasteboard].image];
    photoItem.postId=[[self.attachmentDataDictionary objectForKey:@"id"] intValue];
    
    JSQMessage *message = [JSQMessage messageWithSenderId:[NSString stringWithFormat:@"%i",[UserAccount sharedManager].userId]
                                  displayName:[UserAccount sharedManager].userName
                                        media:photoItem];
    
    [self.demoData.messages addObject:message];
    self.indexForVisibleDate=self.demoData.messages.count-1;
    
    
    [self sendMessages:@"Attached Post"];
    
    [self finishSendingMessageAnimated:YES];
    
}

#pragma mark - JSQMessagesViewController method overrides

- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date
{
    /**
     *  Sending a message. Your implementation of this method should do *at least* the following:
     *
     *  1. Play sound (optional)
     *  2. Add new id<JSQMessageData> object to your data source
     *  3. Call `finishSendingMessage`
     */

    // [JSQSystemSoundPlayer jsq_playMessageSentSound];
    
    JSQMessage *message = [[JSQMessage alloc] initWithSenderId:senderId
                                             senderDisplayName:senderDisplayName
                                                          date:date
                                                          text:text];
    
    
    [self.demoData.messages addObject:message];
   // self.indexForVisibleDate=self.demoData.messages.count-1;
    
    NSLog(@"self.demoData.messages %@",self.demoData.messages);
    
    [self sendMessages:text];
    
    [self finishSendingMessageAnimated:YES];
}

- (void)didPressAccessoryButton:(UIButton *)sender
{
    
    fusumaCameraViewController=[[FusumaViewController alloc]
                                initWithNibName:@"FusumaViewController" bundle:nil];;
    UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:fusumaCameraViewController];
    navCon.navigationBar.hidden=YES;
    
    fusumaCameraViewController.delegate=self;
    fusumaCameraViewController.hasVideo=NO;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:fusumaCameraViewController];
    navigationController.navigationBarHidden=YES;
    [self presentViewController:navigationController animated:YES completion:nil];
    
//    [self.inputToolbar.contentView.textView resignFirstResponder];
//
//    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Attachments", nil)
//                                                       delegate:self
//                                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
//                                         destructiveButtonTitle:nil
//                                              otherButtonTitles:NSLocalizedString(@"Send photo", nil), NSLocalizedString(@"Send location", nil), NSLocalizedString(@"Send video", nil), NSLocalizedString(@"Send audio", nil), nil];
//    
//    [sheet showFromToolbar:self.inputToolbar];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        [self.inputToolbar.contentView.textView becomeFirstResponder];
        return;
    }
    
    switch (buttonIndex) {
        case 0:
        {
            
            
            
            //[self.demoData addPhotoMediaMessage];
            
//            if ([UIPasteboard generalPasteboard].image) {
//                // If there's an image in the pasteboard, construct a media item with that image and `send` it.
//                JSQPhotoMediaItem *item = [[JSQPhotoMediaItem alloc] initWithImage:[UIPasteboard generalPasteboard].image];
//                JSQMessage *message = [[JSQMessage alloc] initWithSenderId:self.senderId
//                                                         senderDisplayName:self.senderDisplayName
//                                                                      date:[NSDate date]
//                                                                     media:item];
//                [self.demoData.messages addObject:message];
//                [self finishSendingMessage];
//                
//            }
            
            
            
            fusumaCameraViewController=[[FusumaViewController alloc]
                                        initWithNibName:@"FusumaViewController" bundle:nil];;
            UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:fusumaCameraViewController];
            navCon.navigationBar.hidden=YES;
            
            fusumaCameraViewController.delegate=self;
            fusumaCameraViewController.hasVideo=NO;
            
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:fusumaCameraViewController];
            navigationController.navigationBarHidden=YES;
            [self presentViewController:navigationController animated:YES completion:nil];
            
            
            
        }
            break;
            
//        case 1:
//        {
//            __weak UICollectionView *weakView = self.collectionView;
//            
//            [self.demoData addLocationMediaMessageCompletion:^{
//                [weakView reloadData];
//            }];
//        }
//            break;
//            
//        case 2:
//            [self.demoData addVideoMediaMessage];
//            break;
//            
//        case 3:
//            [self.demoData addAudioMediaMessage];
//            break;
    }
    
    // [JSQSystemSoundPlayer jsq_playMessageSentSound];
    
    [self finishSendingMessageAnimated:YES];
}



#pragma mark - JSQMessages CollectionView DataSource

- (NSString *)senderId {
    return [NSString stringWithFormat:@"%i",[UserAccount sharedManager].userId];
}

- (NSString *)senderDisplayName {
    return [UserAccount sharedManager].userName;
}

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.demoData.messages objectAtIndex:indexPath.item];
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didDeleteMessageAtIndexPath:(NSIndexPath *)indexPath
{
    [self.demoData.messages removeObjectAtIndex:indexPath.item];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  You may return nil here if you do not want bubbles.
     *  In this case, you should set the background color of your collection view cell's textView.
     *
     *  Otherwise, return your previously created bubble image data objects.
     */
    
    JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
  //  NSLog(@"issue 1");
    
    if ([message.senderId isEqualToString:self.senderId]) {
        return self.demoData.outgoingBubbleImageData;
    }
    else
    {
        return self.demoData.incomingBubbleImageData;
    }
   //   NSLog(@"nope");
    
  //  return self.demoData.incomingBubbleImageData;
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Return `nil` here if you do not want avatars.
     *  If you do return `nil`, be sure to do the following in `viewDidLoad`:
     *
     *  self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
     *  self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
     *
     *  It is possible to have only outgoing avatars or only incoming avatars, too.
     */
    
    /**
     *  Return your previously created avatar image data objects.
     *
     *  Note: these the avatars will be sized according to these values:
     *
     *  self.collectionView.collectionViewLayout.incomingAvatarViewSize
     *  self.collectionView.collectionViewLayout.outgoingAvatarViewSize
     *
     *  Override the defaults in `viewDidLoad`
     */
    JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
    
   // NSLog(@"issue 2");
    if ([message.senderId isEqualToString:self.senderId]) {
        if (![NSUserDefaults outgoingAvatarSetting]) {
            return nil;
        }
    }
    else {
        if (![NSUserDefaults incomingAvatarSetting]) {
            return nil;
        }
    }
    
 
    return [self.demoData.avatars objectForKey:message.senderId];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
     *  The other label text delegate methods should follow a similar pattern.
     *
     *  Show a timestamp for every 3rd message
     */
    
    //if (indexPath.item % 3 == 0) {
        JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
    //}
    
    //return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
    
    /**
     *  iOS7-style sender name labels
     */
    //NSLog(@"issue 3");
    if ([message.senderId isEqualToString:self.senderId]) {
        return nil;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.demoData.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:message.senderId]) {
            return nil;
        }
    }
    
    /**
     *  Don't specify attributes to use the defaults.
     */
    return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.demoData.messages count];
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Override point for customizing cells
     */
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    /**
     *  Configure almost *anything* on the cell
     *
     *  Text colors, label text, label colors, etc.
     *
     *
     *  DO NOT set `cell.textView.font` !
     *  Instead, you need to set `self.collectionView.collectionViewLayout.messageBubbleFont` to the font you want in `viewDidLoad`
     *
     *
     *  DO NOT manipulate cell layout information!
     *  Instead, override the properties you want on `self.collectionView.collectionViewLayout` from `viewDidLoad`
     */
    
    JSQMessage *msg = [self.demoData.messages objectAtIndex:indexPath.item];
    
   // NSLog(@"issue 4");
    if (!msg.isMediaMessage) {
        
        if ([msg.senderId isEqualToString:self.senderId]) {
            cell.textView.textColor = [UIColor blackColor];
        }
        else {
            cell.textView.textColor = [UIColor whiteColor];
        }
        
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
                                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    }

    cell.accessoryButton.hidden = ![self shouldShowAccessoryButtonForMessage:msg];
    
    if (msg.isRead) {
        cell.statusButton.selected=YES;
        
    }
    else
    {
        cell.statusButton.selected=NO;
    }
    
    return cell;
}

- (BOOL)shouldShowAccessoryButtonForMessage:(id<JSQMessageData>)message
{
    return ([message isMediaMessage] && [NSUserDefaults accessoryButtonForMediaMessages]);
}


#pragma mark - UICollectionView Delegate

#pragma mark - Custom menu items

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    //Changed by Palash
    return NO;

    
//    if (action == @selector(customAction:)) {
//        return YES;
//    }
//
//    return [super collectionView:collectionView canPerformAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    if (action == @selector(customAction:)) {
        [self customAction:sender];
        return;
    }

    [super collectionView:collectionView performAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)customAction:(id)sender
{
    NSLog(@"Custom action received! Sender: %@", sender);

    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Custom Action", nil)
                                message:nil
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                      otherButtonTitles:nil]
     show];
}



#pragma mark - JSQMessages collection view flow layout delegate

#pragma mark - Adjusting cell label heights

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
     */
    
    /**
     *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
     *  The other label height delegate methods should follow similarly
     *
     *  Show a timestamp for every 3rd message
     */
    if (indexPath.item == self.indexForVisibleDate) {
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    
    return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  iOS7-style sender name labels
     */
   
    JSQMessage *currentMessage = [self.demoData.messages objectAtIndex:indexPath.item];
    
    // NSLog(@"issue 5 %@ %@",[currentMessage senderId],self.senderId);
    if ([[currentMessage senderId] isEqualToString:self.senderId]) {
        return 0.0f;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.demoData.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:[currentMessage senderId]]) {
            return 0.0f;
        }
    }
    
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.0f;
}

#pragma mark - Responding to collection view tap events

- (void)collectionView:(JSQMessagesCollectionView *)collectionView
                header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender
{
    NSLog(@"Load earlier messages!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapAvatarImageView:(UIImageView *)avatarImageView atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Tapped avatar!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Tapped message bubble!");
    self.indexForVisibleDate=indexPath.item;
    
    JSQMessage *currentMessage = [self.demoData.messages objectAtIndex:indexPath.item];
    
    if(currentMessage.media)
    {
        JSQPhotoMediaItem *photoMedia=(JSQPhotoMediaItem*) currentMessage.media;
        NSLog(@"photoMedia %d",photoMedia.postId);
        
        if(photoMedia.postId)
        {
            UIStoryboard *storyboard= [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            ViewPostViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"ViewPostViewController"];
            
            controller.isFromActivity = YES;
            controller.postId = photoMedia.postId;
            
            NSLog(@"storyboard %@ controller %@",self.storyboard,controller);
            
            [self.navigationController pushViewController:controller animated:YES];
            
        }
        else
        {
//            if(self.photoView)
//            {
//                [self.photoView removeFromSuperview];
//                
//            }
//            self.photoView = [[VIPhotoView alloc] initWithFrame:self.bounds andImage:self.pageImage];
//            self.photoView.autoresizingMask = (1 << 6) -1;
//            [self addSubview:self.photoView];
            
            if(photoMedia.image)
            {
                photoView = [[VIPhotoView alloc] initWithFrame:self.view.bounds andImage:photoMedia.image];
                photoView.autoresizingMask = (1 << 6) -1;
                
                [self.view addSubview:photoView];
                
                //add cross Button
                
                crossButton = [UIButton buttonWithType:UIButtonTypeCustom];
                
                [crossButton addTarget:self
                                action:@selector(crossButtonAction:)
                      forControlEvents:UIControlEventTouchUpInside];
                [crossButton setTitle:@"" forState:UIControlStateNormal];
                crossButton.frame = CGRectMake(15.0, 20.0, 35.0, 35.0);
                UIImage *btnImage = [UIImage imageNamed:@"blackCloseButton.png"];
                [crossButton setImage:btnImage forState:UIControlStateNormal];
                
                [self.view addSubview:crossButton];
                

                self.inputToolbar.hidden=YES;
                [self.inputToolbar.contentView.textView resignFirstResponder];
                
                
            }
            
            
        }

    }
    else
    {
        //[collectionView reloadItemsAtIndexPaths:@[indexPath]];
        [collectionView reloadData];
    }
    
   
    
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapCellAtIndexPath:(NSIndexPath *)indexPath touchLocation:(CGPoint)touchLocation
{
    NSLog(@"Tapped cell at %@!", NSStringFromCGPoint(touchLocation));
}

#pragma mark - JSQMessagesComposerTextViewPasteDelegate methods

- (BOOL)composerTextView:(JSQMessagesComposerTextView *)textView shouldPasteWithSender:(id)sender
{
    if ([UIPasteboard generalPasteboard].image) {
        // If there's an image in the pasteboard, construct a media item with that image and `send` it.
        JSQPhotoMediaItem *item = [[JSQPhotoMediaItem alloc] initWithImage:[UIPasteboard generalPasteboard].image];
        JSQMessage *message = [[JSQMessage alloc] initWithSenderId:self.senderId
                                                 senderDisplayName:self.senderDisplayName
                                                              date:[NSDate date]
                                                             media:item];
        [self.demoData.messages addObject:message];
        [self finishSendingMessage];
        return NO;
    }
    return YES;
}

#pragma mark - JSQMessagesViewAccessoryDelegate methods

- (void)messageView:(JSQMessagesCollectionView *)view didTapAccessoryButtonAtIndexPath:(NSIndexPath *)path
{
    NSLog(@"Tapped accessory button!");
}

-(NSDate *)getDateFromString:(NSString *)dateString
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy'-'MM'-'dd' 'HH':'mm':'ss"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    
    //[formatter setDateFormat:@"EEE, MMM dd, yyyy"];
    NSDate *currentDate=[formatter dateFromString:dateString];
    return currentDate;
}


#pragma mark: FusumaDelegate Protocol
-(void) fusumaImageSelected:(UIImage *)image
{
    NSLog(@"fusumaImageSelected");
    
    [fusumaCameraViewController dismissViewControllerAnimated:YES completion:^{
        NSLog(@"closed");
       
        JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:image];
                                        
        JSQMessage *message = [JSQMessage messageWithSenderId:[NSString stringWithFormat:@"%i",[UserAccount sharedManager].userId]
                                                                                  displayName:[UserAccount sharedManager].userName
                                                                                        media:photoItem];
                                        
        [self.demoData.messages addObject:message];
        self.indexForVisibleDate=self.demoData.messages.count-1;
        
        [self sendMessageWithPhoto:image];
        
        [self finishSendingMessageAnimated:YES];
        
    }];
    
}
-(void)fusumaVideoCompletedWithFileURL:(NSURL *)fileURL
{
    NSLog(@"video completed and output to file: \(%@)",fileURL);
    
}

-(void)fusumaDismissedWithImage:(UIImage *)image
{
    NSLog(@"Called just after dismissed FusumaViewController");
    
    
    ////for test
    //    UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
    //    UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
    //    [pvc presentViewController: editorController animated: NO completion:nil];
    
    //   [self presentViewController:editorController animated:NO completion:nil];
    //  [self.navigationController pushViewController:editorController animated:YES];
    
    
}

-(void) fusumaCameraRollUnauthorized
{
    NSLog(@"Camera roll unauthorized");
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Access Requested" message:@"Please click OK to allow access to your photo library" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* settingAction = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if (&UIApplicationOpenSettingsURLString != NULL) {
            
            [fusumaCameraViewController dismissViewControllerAnimated:YES completion:nil];
            
            NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:appSettings];
        }
        
    }];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        //    [fusumaCameraViewController dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    
    [alertController addAction:settingAction];
    [alertController addAction:cancelAction];
    
    UINavigationController *navigationController = (UINavigationController*)self.presentedViewController;
    [navigationController presentViewController:alertController animated:YES completion:nil];
    
    
}
-(void)fusumaCameraUnauthorized
{
    NSLog(@"Camera unauthorized");
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Access Requested" message:@"Please click OK to allow access to your camera" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* settingAction = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if (&UIApplicationOpenSettingsURLString != NULL) {
            
            [fusumaCameraViewController dismissViewControllerAnimated:YES completion:nil];
            
            NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:appSettings];
        }
        
    }];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        // [fusumaCameraViewController dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    
    [alertController addAction:settingAction];
    [alertController addAction:cancelAction];
    
    UINavigationController *navigationController = (UINavigationController*)self.presentedViewController;
    [navigationController presentViewController:alertController animated:YES completion:nil];
    
    
}


-(void)fusumaClosed
{
    
    NSLog(@"Called when the close button is pressed");
    
}




#pragma mark - Pusher Delegate Connection
//////////////////////////////////

- (void)pusher:(PTPusher *)pusher connectionDidConnect:(PTPusherConnection *)connection
{
    NSLog(@"[Pusher] connected to %@", [connection.URL absoluteString]);
}

- (void)pusher:(PTPusher *)pusher connection:(PTPusherConnection *)connection failedWithError:(NSError *)error
{
    if (error) {
        NSLog(@"[Pusher] connection failed: %@", [error localizedDescription]);
    } else {
        NSLog(@"[Pusher] connection failed");
    }
}

- (void)pusher:(PTPusher *)pusher connection:(PTPusherConnection *)connection didDisconnectWithError:(NSError *)error willAttemptReconnect:(BOOL)reconnect
{
    if (error) {
        NSLog(@"[Pusher] didDisconnectWithError: %@ willAttemptReconnect: %@", [error localizedDescription], (reconnect ? @"YES" : @"NO"));
    } else {
        NSLog(@"[Pusher] disconnected");
    }
}

- (UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}
-(NSData*)compressImage: (UIImage *)img
{
    float MAX_UPLOAD_SIZE=100;
    float MIN_UPLOAD_RESOLUTION=756*756;
    float factor;
    float resol = img.size.height*img.size.width;
    if (resol >MIN_UPLOAD_RESOLUTION){
        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
        img = [self scaleDown:img withSize:CGSizeMake(img.size.width/factor, img.size.height/factor)];
    }
    
    //Compress the image
    CGFloat compression = 1.0f;
    CGFloat maxCompression = 0.5f;
    
    NSData *imageData = UIImageJPEGRepresentation(img, compression);
    
    while ([imageData length] > MAX_UPLOAD_SIZE && compression > maxCompression)
    {
        compression -= 0.10;
        imageData = UIImageJPEGRepresentation(img, compression);
        NSLog(@"Compress : %lu",(unsigned long)imageData.length);
    }
    return imageData;
}

- (IBAction)backButtonAction:(id)sender {
    
    
    if (self.isFromProfile) {
        
         [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        
    }else
    {
         [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}

- (void)crossButtonAction:(UIButton*)button
{
    NSLog(@" cross Button  clicked.");
    
    self.inputToolbar.hidden=NO;
    [crossButton removeFromSuperview];
    [photoView removeFromSuperview];
    
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}


@end
