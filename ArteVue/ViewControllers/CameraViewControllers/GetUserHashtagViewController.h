//
//  TagPeopleViewController.h
//  Artegram
//
//  Created by Sujan on 6/28/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"

@protocol TagPeopleControllerDelegate;

@interface GetUserHashtagViewController : UIViewController<UISearchBarDelegate, UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, assign) id delegate;

@property BOOL isSearchUser;

@property (strong,nonatomic) NSString *initialText;
@property (weak, nonatomic) IBOutlet UIView *searchPeopleView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *searchPeopleTableView;

@end

@protocol UserHashtagControllerDelegate
- (void)didFinishPickingItem:(NSMutableDictionary *)item forUser:(BOOL)test;

@end
