//
//  CameraLayerViewController.m
//  Artegram
//
//  Created by Tanvir Palash on 6/10/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "CameraLayerViewController.h"
#import "SharePhotoViewController.h"

@interface CameraLayerViewController ()

@end

@implementation CameraLayerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sharePhitoView:(id)sender {
    
    SharePhotoViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SharePhotoViewController"];
    
    [self.navigationController pushViewController:controller animated:YES];
}


@end
