//
//  LocationsViewController.h
//  Artegrams
//
//  Created by Sujan on 8/10/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "HNKGooglePlacesAutocomplete.h"

@protocol LocationPostDelegate;

@interface LocationsViewController : UIViewController<CLLocationManagerDelegate,UISearchBarDelegate, UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, assign) id delegate;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (weak, nonatomic) IBOutlet UITableView *searchResultTable;

@property (strong, nonatomic) HNKGooglePlacesAutocompleteQuery *searchQuery;

@property (strong, nonatomic) NSArray *searchResults;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBarLeading;


@end

@protocol LocationPostDelegate
- (void)didFinishPickingLocation:(NSMutableDictionary*)location;
@end
