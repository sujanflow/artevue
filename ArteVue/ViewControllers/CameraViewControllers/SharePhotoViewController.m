//
//  SharePhotoViewController.m
//  Artegram
//
//  Created by Sujan on 6/26/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "SharePhotoViewController.h"

#import "Constants.h"
#import "UserAccount.h"
#import "ServerManager.h"

#import "DGActivityIndicatorView.h"

@interface SharePhotoViewController (){

    NSArray* socialMediaName;
    NSArray* socialMediaLogoName;
    NSMutableArray* taggedUsers;
    NSMutableArray* topHashTags;
    NSMutableArray* artistList;
    
    NSMutableDictionary* selectedLocation;
    
    BOOL isTagSelectable;
    BOOL isSearchUser;
    BOOL isSearchTag;
    BOOL shouldUpload;
    
    NSMutableArray *hashTagSearchResult;
    NSMutableArray *peopleSearchResult;
    
    
}
@property (nonatomic) DGActivityIndicatorView *activityIndicatorView;



@end

@implementation SharePhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    
    self.activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)DGActivityIndicatorAnimationTypeTriplePulse tintColor:[UIColor blackColor]];
    CGFloat width = self.view.bounds.size.width / 5.0f;
    CGFloat height = self.view.bounds.size.width / 5.0f;
    
    self.activityIndicatorView.frame = CGRectMake(self.view.bounds.size.width/2-width/2,self.view.bounds.size.height/2-height/2, width, height);
    [self.view addSubview:self.activityIndicatorView];
    
    
    isTagSelectable=YES;
    //setting delegate
    
    self.captionTextView.delegate = self;
    self.hashTagTextView.delegate = self;
    
    self.artistTextfield.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    self.artistTextfield.delegate=self;
    self.priceTextField.delegate=self;
    
    self.hashTagTextView.textColor = [UIColor lightGrayColor];
    self.hashTagTextView.text = @"Add some hashtags and @users to enhance your post";

    
    self.selectedPhoto.image=[self.selectedImageForShare normalizedImage];
    
  //  NSLog(@"%f %f",self.selectedPhoto.image.size.width, self.selectedPhoto.image.size.height);
    
    //socialMediaCollectionView
    
    self.socialMediaCollectionView.delegate = self;
    self.socialMediaCollectionView.dataSource = self;
    
    self.autoFillTableView.delegate=self;
    self.autoFillTableView.dataSource=self;
    
    socialMediaName = [[NSArray alloc]initWithObjects:@"Instagram",@"Facebook",@"Pinterest",@"Twitter", nil];
    socialMediaLogoName = [[NSArray alloc]initWithObjects:@"instagramShareButton",@"facebookShareButton",@"pinterestShareButton",@"twitterShareButton", nil];
    
    taggedUsers=[[NSMutableArray alloc] init];
    selectedLocation=[[NSMutableDictionary alloc] init];
    
    //[self loadHashTags];
    [self loadArtist];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isInGallery"])
    {
        NSLog(@"from gallery");
        [self.saveToMyGalleryButton setOn:YES animated:NO];
        
        [self.publicPostToggle setOn:NO animated:NO];
    }
    
    if(self.isForShare)
    {
        self.shareBoxTopInactiveConstraint.active=NO;
        self.shareBoxLayoutConstraint.constant=0;
        [self.view layoutIfNeeded];
        
        self.shareLabelButton.hidden=NO;
        self.socialMediaCollectionView.hidden=NO;
        
        self.submitButton.hidden=YES;
        
        //changes
        self.arteScanButton.userInteractionEnabled=NO;
        self.captionDetailsView.userInteractionEnabled=NO;
        
        if(![[self.postDetails objectForKey:@"artist"] isEqual:[NSNull null]])
            self.artistTextfield.text=[self.postDetails objectForKey:@"artist"];
        self.artistTextfield.userInteractionEnabled=NO;
        
        
        self.captionTextView.text=[self.postDetails objectForKey:@"description"];
        self.captionHintLabel.hidden=YES;
        //self.captionTextView.userInteractionEnabled=NO;
        
        self.hashTagTextView.text=[self.postDetails objectForKey:@"hashtags"];
        self.hashTagTextView.userInteractionEnabled=NO;
        
        self.hashTagPicker.hidden=YES;
        
        self.tagPeopleButton.hidden=YES;
        self.locationDetailsView.hidden=YES;
       
        
        
        self.saveToMyGalleryButton.hidden=YES;
        self.saveToGalleryLabel.hidden=YES;
        
        self.publicPostToggle.hidden=YES;
        self.publicPostLabel.hidden=YES;
        
        self.buyButtonLabel.hidden=YES;
        self.buyButtonSwitch.hidden=YES;
        
        self.priceViewHeightConstraint.constant=0;
        
    }
    else if(self.isForEdit)
    {
        NSLog(@"self.postDetails %@",self.postDetails);
        self.captionTextView.text=[self.postDetails objectForKey:@"description"];
        if(self.captionTextView.text.length>0)
            self.captionHintLabel.hidden=YES;
       else
           self.captionHintLabel.hidden=NO;
        
        NSString * str = [self.captionTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        //NSString * str = [str2 stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
        
        int currentLimit = 250 - (int)str.length;
        if (currentLimit<=0) {
            //[self.view endEditing:YES];
            self.descriptionCounter.textColor=[UIColor redColor];
        }
        else
            self.descriptionCounter.textColor=[UIColor whiteColor];
        
        
        [self.descriptionCounter setText:[NSString stringWithFormat:@"%i/250", currentLimit]];
        
        
        if(![[self.postDetails objectForKey:@"artist"] isEqual:[NSNull null]])
            self.artistTextfield.text=[self.postDetails objectForKey:@"artist"];
        else
        {
        
        }
            
        self.hashTagTextView.text=[self.postDetails objectForKey:@"hashtags"];
        
        if(self.hashTagTextView.text.length<=0)
            self.hashTagTextView.text=@"Add some hashtags and @users to enhance your post";
        
        
        self.saveToMyGalleryButton.hidden=YES;
        self.saveToGalleryLabel.hidden=YES;
        
        self.publicPostToggle.hidden=YES;
        self.publicPostLabel.hidden=YES;
        
        
        
        NSArray *tempTaggedUser=[self.postDetails objectForKey:@"tagged_users"];
        
        for (int i=0; i<tempTaggedUser.count; i++) {
            
            NSMutableDictionary *tagListDic = [[NSMutableDictionary alloc]init];
            [tagListDic setObject:[[tempTaggedUser objectAtIndex:i] objectForKey:@"user_id"] forKey:@"user_id"];
            [tagListDic setObject:[NSNumber numberWithFloat:[[[tempTaggedUser objectAtIndex:i] objectForKey:@"x"] floatValue]]  forKey:@"x"];
            [tagListDic setObject:[NSNumber numberWithFloat:[[[tempTaggedUser objectAtIndex:i] objectForKey:@"y"] floatValue]] forKey:@"y"];
            [tagListDic setObject:[[tempTaggedUser objectAtIndex:i] objectForKey:@"username"] forKey:@"username"];
            
            [taggedUsers addObject:tagListDic];
        }
        
        if(taggedUsers.count==1)
        {
            self.taggedPeopleLabel.text=[[taggedUsers objectAtIndex:0] objectForKey:@"username"];
        }
        else if(taggedUsers.count>1)
        {
            self.taggedPeopleLabel.text=[NSString stringWithFormat:@"%lu people",(unsigned long)taggedUsers.count];
        }
        else
        {
            self.taggedPeopleLabel.text=@"";
        }

        NSString *placeId=[self.postDetails objectForKey:@"google_place_id"];
        NSString *placeTitle=[self.postDetails objectForKey:@"address_title"];
        if(placeId.length>0 && placeTitle.length>0)
        {
            
            [selectedLocation setObject:[self.postDetails objectForKey:@"address_title"]forKey:@"title"];
            [selectedLocation setObject:[self.postDetails objectForKey:@"address"] forKey:@"address"];
            [selectedLocation setObject:placeId forKey:@"placeId"];
            
            
            self.locationName.text=[selectedLocation objectForKey:@"title"];
            self.locationAddress.text=[selectedLocation objectForKey:@"address"];
            
            [self showLocationView];
            
        }
        
        self.shareLabelButton.hidden=YES;
        self.socialMediaCollectionView.hidden=YES;
        
        [self.submitButton setTitle:@"Update" forState:UIControlStateNormal];
    
      
        if ([[self.postDetails objectForKey:@"has_buy_btn"] intValue]) {
            
            [self.buyButtonSwitch setOn:YES];
            [self.buyButtonSwitch setHidden:NO];
            
            self.priceViewHeightConstraint.constant=35;
            
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
            [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
           // NSNumber *number = [formatter numberFromString:];
            
          //  NSString *formattedString = [formatter stringFromNumber:[NSNumber numberWithFloat:[self.postDetails objectForKey:@"price"]]];

            NSString *formattedString = [formatter stringFromNumber:[self.postDetails objectForKey:@"price"]];
            
            
            self.priceTextField.text=formattedString;
            
        }
        else
        {
            [self.buyButtonSwitch setOn:NO];
            self.priceViewHeightConstraint.constant=0;
            self.priceTextField.text=@"";
            
        }
    }
    
    
    if([[UserAccount sharedManager].userType isEqualToString:@"Collector"] || [[UserAccount sharedManager].userType isEqualToString:@"Gallery"] || [[UserAccount sharedManager].userType isEqualToString:@"Artist"] ||[[UserAccount sharedManager].userType isEqualToString:@"Art Professional"] )
    {
//    if ([[UserAccount sharedManager].userType intValue]==3 || [[UserAccount sharedManager].userType intValue]==4 || [[UserAccount sharedManager].userType intValue]==6 || [[UserAccount sharedManager].userType intValue]==7) {
//        
        self.buyButtonLabel.hidden=NO;
        self.buyButtonSwitch.hidden=NO;
        
    }
    else
    {
        
        self.buyButtonLabel.hidden=YES;
        self.buyButtonSwitch.hidden=YES;
        
    }
    
    
 //   [SDWebImageManager sharedManager].delegate=self;
}

-(void)loadArtist
{
    
    [[ServerManager sharedManager] getAllArtistWithCompletion:^(BOOL success, NSMutableDictionary *responseObject) {
        
        
        if ( responseObject!=nil) {
            
            
            
            artistList=[responseObject valueForKey:@"name"];
            [self configureSimpleSearchTextField];
            
            
            //NSLog(@"artistList %@",artistList);
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
            });
            
        }
    }];
    

}


-(void)configureSimpleSearchTextField
{
    // Start visible - Default: false
    self.artistTextfield.startVisible = true;
    // Set data source
    
    [self.artistTextfield filterStrings:artistList];
}

/*
-(void) loadHashTags{
    
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    [postData setObject:ACCESS_KEY forKey:@"access_key"];
    [postData setObject:[ NSNumber numberWithInt:[[UserAccount sharedManager] userId]] forKey:@"user_id"];
    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
    
   // NSLog(@"postData %@",postData);
    
    [apiLoginManager POST:[NSString stringWithFormat:@"%@/hashtags/apiGetTopHashtags",SERVER_BASE_API_URL] parameters:postData
                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                      
                     // NSLog(@"Response: %@", responseObject);
                      topHashTags=[[NSMutableArray alloc] initWithArray:responseObject];
                     
                      if(topHashTags.count)
                          [self configueHashtagPicker];
                      else
                      {
                          self.hashTagPicker.hidden=YES;
                      }
                }
     
                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                      
                      NSLog(@"Error: %@", error);
                     // NSLog(@"%@",operation.responseString);
                      
                  }];
    
}
 */

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(void) configueHashtagPicker
{
    self.hashTagPicker.delegate = self;
    self.hashTagPicker.dataSource = self;
    self.hashTagPicker.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    //[self.view addSubview:self.pickerView];
    
    self.hashTagPicker.font = [UIFont fontWithName:@"AzoSans-Regular" size:14];
    self.hashTagPicker.highlightedFont = [UIFont fontWithName:@"AzoSans-Regular" size:14];
    self.hashTagPicker.interitemSpacing = 20.0;
    self.hashTagPicker.fisheyeFactor = 0.001;
    self.hashTagPicker.pickerViewStyle = AKPickerViewStyle3D;
   // pickerView.selectedItem=[[[childAges objectAtIndex:indexPath.row] objectForKey:@"age"] integerValue]-1;
    self.hashTagPicker.maskDisabled = false;
    
    [self.hashTagPicker reloadData];

}

// to hide keyBoard

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.captionTextView resignFirstResponder];
    [self.hashTagTextView resignFirstResponder];
    [self.artistTextfield resignFirstResponder];
    [self.priceTextField resignFirstResponder];
    
    
    if (self.captionTextView.text.length == 0) {
        
        self.captionHintLabel.hidden = NO;
    }
    
    NSLog(@"Is it called");
}

// to hide hint label

#pragma mark - AKPickerViewDataSource

- (NSUInteger)numberOfItemsInPickerView:(AKPickerView *)pickerView
{
    return [topHashTags count];
}

/*
 * AKPickerView now support images!
 *
 * Please comment '-pickerView:titleForItem:' entirely
 * and uncomment '-pickerView:imageForItem:' to see how it works.
 *
 */

- (NSString *)pickerView:(AKPickerView *)pickerView titleForItem:(NSInteger)item
{
    return [NSString stringWithFormat:@"%@", [topHashTags[item]objectForKey:@"hashtag"]];
}

/*
 - (UIImage *)pickerView:(AKPickerView *)pickerView imageForItem:(NSInteger)item
 {
	return [UIImage imageNamed:self.titles[item]];
 }
 */

#pragma mark - AKPickerViewDelegate

- (void)pickerView:(AKPickerView *)pickerView didSelectItem:(NSInteger)item
{
    // [childAges objectAtIndex:pickerView.tag-1]=item;
    
    //
    //
    //NSMutableDictionary *childAge=[[NSMutableDictionary alloc] init];
    //[childAge setObject:topHashTags[item] forKey:@"age"];
    
    
    //[childAges replaceObjectAtIndex:pickerView.pickerId withObject:childAge];
    
    //  NSLog(@"childAges %@",childAges );
    
    if(isTagSelectable)
    {
        if([self.hashTagTextView.text isEqualToString:@"Add some hashtags and @users to enhance your post"])
        {
            self.hashTagTextView.text=[NSString stringWithFormat:@"%@",[topHashTags[item]objectForKey:@"hashtag"]];
        }
        else
            self.hashTagTextView.text=[NSString stringWithFormat:@"%@ %@",self.hashTagTextView.text,[topHashTags[item]objectForKey:@"hashtag"]];
        
        self.hashTagTextView.textColor = [UIColor whiteColor];
        
        
        
        [topHashTags removeObject:topHashTags[item]];
        NSLog(@"hastag count %lu",(unsigned long)topHashTags.count);
        [self.hashTagPicker reloadData];
        
        
        NSArray *words = [self.hashTagTextView.text componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSInteger wordCount = [words count];
        
        if(wordCount>=30)
        {
            isTagSelectable=NO;
            
        }
    }
    else
    {
        
    }
    
   
    
}



/*
 * Label Customization
 *
 * You can customize labels by their any properties (except font,)
 * and margin around text.
 * These methods are optional, and ignored when using images.
 *
 */


- (void)pickerView:(AKPickerView *)pickerView configureLabel:(UILabel *const)label forItem:(NSInteger)item
{
    label.textColor = [UIColor whiteColor];
    label.highlightedTextColor = [UIColor whiteColor];
    //label.backgroundColor = [UIColor colorWithHue:(float)item/(float)self.titles.count
    //									   saturation:1.0
    //									   brightness:1.0
    //											alpha:1.0];
}


/*
 - (CGSize)pickerView:(AKPickerView *)pickerView marginForItem:(NSInteger)item
 {
	return CGSizeMake(40, 20);
 }
 */

- (IBAction)postToFeedToggle:(UISwitch *)sender {
    
     [sender setOn:!sender.isOn animated:YES];
    
    if (![sender isOn])
    {
        [self.saveToMyGalleryButton setOn:YES animated:YES];
    }

    
    
}
- (IBAction)saveToGalleryToggle:(UISwitch *)sender {
    
    if([self.publicPostToggle isOn])
    {
        [sender setOn:!sender.isOn animated:YES];
        
    }else
    {
        [sender setOn:YES animated:YES];
        
    }
    
}
- (IBAction)addBuyingPriceToggle:(UISwitch*)sender {
    if([self.buyButtonSwitch isOn])
    {
        self.priceViewHeightConstraint.constant=35;
        
    }else
    {
        
        self.priceViewHeightConstraint.constant=0;
        
    }

}


#pragma mark - Textfield delegate

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    UIResponder* nextResponder = self.captionTextView;
    if (nextResponder && textField==self.artistTextfield) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == self.priceTextField)
    {
        if (([string isEqualToString:@"0"] || [string isEqualToString:@""]) && [textField.text rangeOfString:@"."].location < range.location) {
            return YES;
        }
        
        // First check whether the replacement string's numeric...
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        bool isNumeric = [string isEqualToString:filtered];
        
        // Then if the replacement string's numeric, or if it's
        // a backspace, or if it's a decimal point and the text
        // field doesn't already contain a decimal point,
        // reformat the new complete number using
        // NSNumberFormatterDecimalStyle
        
        if (isNumeric ||
            [string isEqualToString:@""] ||
            ([string isEqualToString:@"."] &&
             [textField.text rangeOfString:@"."].location == NSNotFound)) {
                
                // Create the decimal style formatter
                NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
                //[formatter setMaximumFractionDigits:10];
                
                // Combine the new text with the old; then remove any
                // commas from the textField before formatting
                NSString *combinedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
                
                NSString *numberWithoutCommas = [combinedText stringByReplacingOccurrencesOfString:@"," withString:@""];
                if(numberWithoutCommas.length>=20)
                    return NO;
                NSNumber *number = [formatter numberFromString:numberWithoutCommas];
                
                
                NSString *formattedString = [formatter stringFromNumber:number];
                
                
                // If the last entry was a decimal or a zero after a decimal,
                // re-add it here because the formatter will naturally remove
                // it.
                if ([string isEqualToString:@"."] &&
                    range.location == textField.text.length) {
                    formattedString = [formattedString stringByAppendingString:@"."];
                }
                
                textField.text = formattedString;
                
            }
        
        // Return no, because either the replacement string is not 
        // valid or it is and the textfield has already been updated
        // accordingly
        return NO;

    }
    else
        return YES;

}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == self.priceTextField && textField.text.length>0 && textField.text.length<4)
    {
       
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        NSNumber *number = [formatter numberFromString:self.priceTextField.text];
        
        if([number intValue]<100)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning!"
                                                            message:@"The price must be higher than US$100"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
            [alert show];
            
        }
    }

}

#pragma mark - Textview delegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    if ([textView isEqual:self.captionTextView]) {
        self.captionHintLabel.hidden = YES;
        [self.artistTextfield resignFirstResponder];
    }
    
}


- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if ([textView isEqual:self.hashTagTextView]) {
        if([textView.text isEqualToString:@"Add some hashtags and @users to enhance your post"])
            textView.text = @"";
       
        textView.textColor = [UIColor whiteColor];
        
    }
    return YES;
}

-(void) textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    
}
-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)aRange replacementText:(NSString*)aText {
    
    if([aText isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    else
    {
        if ([textView isEqual:self.hashTagTextView]) {
            
            if ([aText isEqualToString:@" "]) {
                NSMutableCharacterSet *workingSet = [[NSCharacterSet whitespaceAndNewlineCharacterSet]   mutableCopy];
                
                NSRange newRange = [self.hashTagTextView.text rangeOfCharacterFromSet:workingSet
                                                                              options:NSBackwardsSearch
                                                                                range:NSMakeRange(0, (aRange.location - 1))];
                
                //The below code could be done in a better way...
                UITextPosition *beginning = self.hashTagTextView.beginningOfDocument;
                UITextPosition *start = [self.hashTagTextView positionFromPosition:beginning offset:aRange.location];
                UITextPosition *end = [self.hashTagTextView positionFromPosition:beginning   offset:newRange.location+1];
                UITextRange *textRange = [self.hashTagTextView textRangeFromPosition:end toPosition:start];
                
                NSString* str = [self.hashTagTextView textInRange:textRange];
                if(![str containsString:@"#"] && ![str containsString:@"@"])
                {
                    [textView replaceRange:textRange withText:[NSString stringWithFormat:@"#%@",str]];
                    
                }
                
                NSLog(@"str %@",str);
            }
            
            
            NSString* newText = [textView.text stringByReplacingCharactersInRange:aRange withString:aText];
            NSString *trimmedText = [newText stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            if (newText.length - trimmedText.length > 29) {
                isTagSelectable=NO;
                return NO;
            } else {
                isTagSelectable=YES;
                return YES;
            }
            
        }else
        {

            NSString * stringToRange = [textView.text substringWithRange:NSMakeRange(0,aRange.location)];
            
            // Appending the currently typed charactor
            stringToRange = [stringToRange stringByAppendingString:aText];
            
            // Processing the last typed word
            NSArray *wordArray       = [stringToRange componentsSeparatedByString:@"\n"];
            NSString * wordTyped     = [wordArray lastObject];
            NSString * lastWord    =  [[wordTyped componentsSeparatedByString:@" "] lastObject];
            // wordTyped will give you the last typed object
            //NSLog(@"\nWordTyped :  %@ lastWord %@",wordTyped,lastWord);

            if ([lastWord containsString:@"@"] && lastWord.length>1) {
                isSearchUser=YES;
                [self searchUser:[lastWord substringFromIndex:1]];
                
            }else if ([lastWord containsString:@"#"] && lastWord.length>1)
            {
                isSearchTag=YES;
                
                [self searchTags:[lastWord substringFromIndex:1]];
                
            }
            else
            {
                NSLog(@"searchTags ");
                isSearchUser=NO;
                isSearchTag=NO;
                
                self.autoFillSuggestionView.hidden=YES;
                
            }
            
            
            NSUInteger oldLength = [textView.text length];
            NSUInteger replacementLength = [aText length];
            NSUInteger rangeLength = aRange.length;
            
            NSUInteger newLength = oldLength - rangeLength + replacementLength;
            
            BOOL returnKey = [aText rangeOfString: @"\n"].location != NSNotFound;
            
            NSLog(@"%lu %lu %@ %lu",(unsigned long)oldLength, (unsigned long)replacementLength,aText,(unsigned long)newLength);
            
            if(newLength<=oldLength)
                return YES;
            else
                return newLength <= 250 || returnKey;
         
        }

    }
    
}


- (void)textViewDidChange:(UITextView *)textView{
    
    if ([textView isEqual:self.hashTagTextView]) {
            if(textView.text.length == 0){
                textView.textColor = [UIColor lightGrayColor];
                textView.text = @"Add some hashtags and @users to enhance your post";
                [textView resignFirstResponder];
            }else
            {
                NSArray *words = [textView.text componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSInteger wordCount = [words count];
                
                if(wordCount>=30)
                {
                    isTagSelectable=NO;
                    
                }
                else
                    isTagSelectable=YES;

            }
    }
    else if ([textView isEqual:self.captionTextView]){
        
     
        
        NSString * str = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        //NSString * str = [str2 stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
        
        int currentLimit = 250 - (int)str.length;
        if (currentLimit<=0) {
            //[self.view endEditing:YES];
            self.descriptionCounter.textColor=[UIColor redColor];
        }
        else
            self.descriptionCounter.textColor=[UIColor whiteColor];
        
        
        [self.descriptionCounter setText:[NSString stringWithFormat:@"%i/250", currentLimit]];

    }
    
    
}

-(void) searchUser: (NSString *)searchText
{
    
    [[ServerManager sharedManager] searchUserByString:searchText forPage:1 withcompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
        
        if ( resultDataArray!=nil) {
            
            peopleSearchResult = [[NSMutableArray alloc]init];
            peopleSearchResult = [resultDataArray objectForKey:@"data"];
            
            if(peopleSearchResult.count)
                [self.autoFillSuggestionView setHidden:NO];
            else
                [self.autoFillSuggestionView setHidden:YES];
            
            [self.autoFillTableView reloadData];
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.autoFillSuggestionView setHidden:YES];
                
                
            });
        }
    }];
    
}


-(void) searchTags : (NSString *)searchTag
{
    
    
    [[ServerManager sharedManager] searchHashtagByString:searchTag withcompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
        
        if ( resultDataArray!=nil) {
            
            hashTagSearchResult = [[NSMutableArray alloc]init];
            hashTagSearchResult = [[resultDataArray objectForKey:@"data"] mutableCopy];
            
            
            if (hashTagSearchResult.count && [searchTag isEqualToString:[[hashTagSearchResult objectAtIndex:0] objectForKey:@"hashtag"]]) {
                NSLog(@"do nothing");
            }
            else
            {
                NSMutableDictionary *tempDic=[[NSMutableDictionary alloc] init];
                [tempDic setObject:[NSString stringWithFormat:@"#%@",searchTag] forKey:@"hashtag"];
                [tempDic setObject:@"0" forKey:@"user_count"];
                [hashTagSearchResult insertObject:tempDic atIndex:0];
            }
            
            if(hashTagSearchResult.count)
                [self.autoFillSuggestionView setHidden:NO];
            else
                [self.autoFillSuggestionView setHidden:YES];
            
            
            [self.autoFillTableView reloadData];

            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                [self.autoFillSuggestionView setHidden:YES];

            });
        }
        
        
        
    }];
    

    
}

#pragma mark - UITableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(isSearchUser)
    {
        return peopleSearchResult.count;
    }
    else if (isSearchTag)
    {
        return hashTagSearchResult.count;
    }
    else
        return 0;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if(isSearchUser)
    {
        static NSString *CellIdentifier = @"tagPeopleCell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        
        UIImageView *profileImage = (UIImageView*) [cell viewWithTag:1];
        UILabel *profileName = (UILabel*) [cell viewWithTag:2];
        UILabel *userName = (UILabel*) [cell viewWithTag:3];
        
        [profileImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[peopleSearchResult objectAtIndex:indexPath.row] objectForKey:@"profile_picture"]]]];
        profileName.text = [[peopleSearchResult objectAtIndex:indexPath.row] objectForKey:@"username"];
        userName.text = [[peopleSearchResult objectAtIndex:indexPath.row] objectForKey:@"first_name"];
        
        return cell;
        
    }
    else
    {
        static NSString *CellIdentifier = @"hashTagCell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        
        UILabel *profileName = (UILabel*) [cell viewWithTag:2];
        UILabel *userName = (UILabel*) [cell viewWithTag:3];
        
        
        
        profileName.text = [[hashTagSearchResult objectAtIndex:indexPath.row] objectForKey:@"hashtag"];
        userName.text = [NSString stringWithFormat:@"%d posts",[[[hashTagSearchResult objectAtIndex:indexPath.row] objectForKey:@"use_count"]intValue]];
        
        return cell;
        
    }
    
    
}

#pragma mark - UITableView Delegate


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if(isSearchUser)
    {
            [self didFinishPickingItem:[peopleSearchResult objectAtIndex:indexPath.row] forUser:YES];
    }
    else
    {
            [self didFinishPickingItem:[hashTagSearchResult objectAtIndex:indexPath.row] forUser:NO];
    }
    
}


#pragma mark - CollectionView data source

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
        return  2;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
        static NSString *identifier = @"socialMediaCell";
        
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        UIImageView *socialMediaLogo = (UIImageView*) [cell viewWithTag:4];
        UILabel *socialMediaNameLabel = (UILabel*) [cell viewWithTag:5];
        
        socialMediaNameLabel.text = [socialMediaName objectAtIndex:indexPath.row];
        socialMediaLogo.image=[UIImage imageNamed:[socialMediaLogoName objectAtIndex:indexPath.row]];
    
        return cell;
    
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
        NSLog(@" collection view %@ %lf",collectionView,([UIScreen mainScreen].bounds.size.width-2)/2);
        
        return CGSizeMake(([UIScreen mainScreen].bounds.size.width-2)/2, 42);
        
    
}


#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    if([self.hashTagTextView.text isEqualToString:@"Add some hashtags and @users to enhance your post"])
    {
        pasteboard.string = [NSString stringWithFormat:@"%@",self.captionTextView.text];
        
    }
    else
    {
        pasteboard.string = [NSString stringWithFormat:@"%@\n%@",self.captionTextView.text,self.hashTagTextView.text];
        
    }

   
    if(indexPath.item==0)
    {
//        if ([[[UIDevice currentDevice] systemVersion] floatValue] > 5.0) {
//            float i = [[[UIDevice currentDevice] systemVersion] floatValue];
//            NSString *str = [NSString stringWithFormat:@"We're sorry, but Instagram is not supported with your iOS %.1f version.", i];
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:str delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alert show];
//        }
//        else{
//            CGRect rect = CGRectMake(0 ,0 , 0, 0);
//            NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Test.ig"];
//            
//            NSURL *igImageHookFile = [[NSURL alloc] initWithString:[[NSString alloc] initWithFormat:@"file://%@", jpgPath]];
//            NSLog(@"JPG path %@", jpgPath);
//            NSLog(@"URL Path %@", igImageHookFile);
//            self.docFile.UTI = @"com.instagram.photo";
//            self.docFile = [self setupControllerWithURL:igImageHookFile usingDelegate:self];
//            self.docFile=[UIDocumentInteractionController interactionControllerWithURL:igImageHookFile];
//            [self.docFile presentOpenInMenuFromRect: rect    inView: self.view animated: YES ];
//            NSURL *instagramURL = [NSURL URLWithString:@"instagram://media?id=MEDIA_ID"];
//            if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
//                [self.docFile presentOpenInMenuFromRect: rect    inView: self.view animated: YES ];
//            }
//            else {
//                NSLog(@"No Instagram Found");
//            }
        
    //    }
        
        
        NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
        if([[UIApplication sharedApplication] canOpenURL:instagramURL]) //check for App is install or not
        {
            NSData *imageData = UIImagePNGRepresentation(self.selectedPhoto.image); //convert image into .png format.
            NSFileManager *fileManager = [NSFileManager defaultManager];//create instance of NSFileManager
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //create an array and store result of our search for the documents directory in it
            NSString *documentsDirectory = [paths objectAtIndex:0]; //create NSString object, that holds our exact path to the documents directory
            NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"insta.igo"]]; //add our image to the path
            [fileManager createFileAtPath:fullPath contents:imageData attributes:nil]; //finally save the path (image)
            NSLog(@"image saved");
            
            CGRect rect = CGRectMake(0 ,0 , 0, 0);
            UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, self.view.opaque, 0.0);
            [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
            UIGraphicsEndImageContext();
            NSString *fileNameToSave = [NSString stringWithFormat:@"Documents/insta.igo"];
            NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:fileNameToSave];
            NSLog(@"jpg path %@",jpgPath);
//            NSString *newJpgPath = [NSString stringWithFormat:@"file://%@",jpgPath];
//            NSLog(@"with File path %@",newJpgPath);
            NSURL *igImageHookFile = [[NSURL alloc] initWithString:[[NSString alloc] initWithFormat:@"file://%@", jpgPath]];;
            NSLog(@"url Path %@",igImageHookFile);
            
            self.documentController.UTI = @"com.instagram.exclusivegram";
           // self.documentController.URL=[NSURL URLWithString:@"instagram://"];
            self.documentController = [self setupControllerWithURL:igImageHookFile usingDelegate:self];
            self.documentController=[UIDocumentInteractionController interactionControllerWithURL:igImageHookFile];
            NSString *caption = self.captionTextView.text; //settext as Default Caption
            
           // self.documentController.annotation=[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",caption],@"InstagramCaption", nil];
            
            
            //self.documentController.name=@"Please paste the caption";
            self.documentController.annotation=[NSDictionary dictionaryWithObject:caption forKey:@"InstagramCaption"];
            [self.documentController presentOpenInMenuFromRect:rect inView: self.view animated:YES];
            
            //[self.documentController presentOpenInMenuFromRect:CGRectMake(0, 0, 320, 480) inView:self.view animated:YES];
            
//          
//            if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
//                [[UIApplication sharedApplication] openURL:instagramURL];
//            }
        }
        else
        {
            NSLog (@"Instagram not found");
        }
    
    }
    else if(indexPath.item==1)
    {
        
//        SLComposeViewController *fbVC = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
//        
//        //            NSString *str=[NSString stringWithFormat:@"%@",currentTextViewString];
//        [fbVC addImage:self.selectedPhoto.image];
//        [fbVC setInitialText:self.captionTextView.text];
//        [fbVC addURL:[NSURL URLWithString:@"https://www.artegrams.com/"]];
//        [self presentViewController:fbVC animated:YES completion:nil];
//        
//        [fbVC setCompletionHandler:^(SLComposeViewControllerResult result){
//            
//            NSString *outout = [[NSString alloc] init];
//            
//            switch (result) {
//                case SLComposeViewControllerResultCancelled:
//                    outout = @"Post canceled";
//                    break;
//                case SLComposeViewControllerResultDone:
//                {
//                    outout = @"Successfully Shared";
//                    
//                    
//                   
//                }
//                    
//                    
//                default:
//                    break;
//            }
//        }];
        
//        FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
//        content.contentURL = [NSURL URLWithString:@"https://www.artegrams.com/"];
//        content.imageURL = [NSURL URLWithString:@"https://www.artegrams.com/"];
//        content.contentTitle = self.captionTextView.text;
//        content.contentDescription = self.captionTextView.text;
//        
//        FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
//        dialog.fromViewController = self;
//        dialog.shareContent = content;
//        dialog.mode = FBSDKShareDialogModeNative; // if you don't set this before canShow call, canShow would always return YES
//        if (![dialog canShow]) {
//            // fallback presentation when there is no FB app
//            dialog.mode = FBSDKShareDialogModeFeedBrowser;
//        }
//        [dialog show];
        
        FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
        photo.image = self.selectedPhoto.image;
        photo.userGenerated = YES;
        photo.caption = self.captionTextView.text;
        FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
        content.photos = @[photo];
        [FBSDKShareDialog showFromViewController:self
                                     withContent:content
                                        delegate:nil];

    }
}

- (UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate {
    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL: fileURL];
    interactionController.delegate = interactionDelegate;
    return interactionController;
}

- (void)documentInteractionControllerWillPresentOpenInMenu:(UIDocumentInteractionController *)controller {
    
}

-(void) didFinishPickingItem:(NSMutableDictionary *)item forUser:(BOOL)test
{
    NSLog(@"item %@",item);
    
    self.autoFillSuggestionView.hidden=YES;
    
    if(item.allKeys.count)
    {
        
            __block NSString *lastWord = nil;
            
            [self.captionTextView.text enumerateSubstringsInRange:NSMakeRange(0, [self.captionTextView.text length]) options:NSStringEnumerationByWords | NSStringEnumerationReverse usingBlock:^(NSString *substring, NSRange subrange, NSRange enclosingRange, BOOL *stop) {
                lastWord = substring;
                *stop = YES;
            }];
            NSLog(@"lastWord %@",lastWord);
        
        
        if(test)
        {
            peopleSearchResult=[[NSMutableArray alloc] init];
            
//            if(self.captionTextView.text.length>0)
//                self.captionTextView.text=[NSString stringWithFormat:@"%@@%@",self.captionTextView.text,[item objectForKey:@"username"]];
//            else
//                self.captionTextView.text=[NSString stringWithFormat:@"@%@",[item objectForKey:@"username"]];
//
            
            NSError *error = NULL;
            if(!error) {
               
                NSRange lastComma = [self.captionTextView.text rangeOfString:lastWord options:NSBackwardsSearch];
                    
                    if(lastComma.location != NSNotFound) {
                        self.captionTextView.text = [self.captionTextView.text stringByReplacingCharactersInRange:lastComma
                                                                                                       withString:[NSString stringWithFormat:@"%@ ",[item objectForKey:@"username"]]];
                        
                    }
                    else
                    {
                        NSLog(@"not found");
                    }
                    
            }

            
            isSearchUser=NO;
            
        }
        else
        {
            
            hashTagSearchResult=[[NSMutableArray alloc] init];
            
//            if(self.captionTextView.text.length>0)
//                self.captionTextView.text=[NSString stringWithFormat:@"%@%@",self.captionTextView.text,[item objectForKey:@"hashtag"]];
//            else
//                self.captionTextView.text=[item objectForKey:@"hashtag"];
//
            
            NSError *error = NULL;
            if(!error) {
                    NSRange lastComma = [self.captionTextView.text rangeOfString:lastWord options:NSBackwardsSearch];
                    
                    if(lastComma.location != NSNotFound) {
                        self.captionTextView.text = [self.captionTextView.text stringByReplacingCharactersInRange:lastComma
                                                                                                       withString:[NSString stringWithFormat:@"%@ ",[[item objectForKey:@"hashtag"] substringFromIndex:1]]];
                        
                    
                }
            }

            
            isSearchTag=NO;
            
        }
        

        int currentLimit = 250 - (int)self.captionTextView.text.length;
        
        
        if (currentLimit<=0) {
            self.descriptionCounter.textColor=[UIColor redColor];
        }
        else
            self.descriptionCounter.textColor=[UIColor whiteColor];
        
        
        [self.descriptionCounter setText:[NSString stringWithFormat:@"%i/250", currentLimit]];
    }
    
    
    
   

}

-(void)didFinishPickingTags:(NSMutableArray *)tags
{
    NSLog(@"tags %@",tags);
    
    taggedUsers=[[NSMutableArray alloc]initWithArray:tags];
    
    if(taggedUsers.count==1)
    {
        self.taggedPeopleLabel.text=[[taggedUsers objectAtIndex:0] objectForKey:@"username"];
    }
    else if(taggedUsers.count>1)
    {
        self.taggedPeopleLabel.text=[NSString stringWithFormat:@"%lu people",(unsigned long)taggedUsers.count];
    }
    else
    {
        self.taggedPeopleLabel.text=@"";
    }

}

-(void)didFinishPickingLocation:(NSMutableDictionary *)location
{
    selectedLocation=[[NSMutableDictionary alloc] initWithDictionary:location];
    NSLog(@"selectedLocation %@",selectedLocation);
    
    self.locationName.text=[selectedLocation objectForKey:@"title"];
    self.locationAddress.text=[selectedLocation objectForKey:@"address"];
    
    [self showLocationView];
    
}
- (IBAction)closeLocationView:(id)sender {
    
    [self hideLocationView];
    
}

-(void)showLocationView
{
    self.addLocationButton.hidden=YES;
    
    self.locationName.hidden=NO;
    self.locationAddress.hidden=NO;
    self.locationCloseButton.hidden=NO;
    
   // self.locationHeightConstraint.constant=50;32/5
//    NSLayoutConstraint *constraint =[NSLayoutConstraint
//                                     constraintWithItem:self.locationDetailsView
//                                     attribute:NSLayoutAttributeWidth
//                                     relatedBy:NSLayoutRelationEqual
//                                     toItem:self.locationDetailsView
//                                     attribute:NSLayoutAttributeHeight
//                                     multiplier:8.0/32.0 //Aspect ratio: 4*height = 3*width
//                                     constant:0.0f];
//     self.locationAspectRatio=constraint;
//    [self.view layoutIfNeeded];
//    [self.view updateConstraintsIfNeeded];
}

-(void)hideLocationView
{
    self.addLocationButton.hidden=NO;
    
    self.locationName.hidden=YES;
    self.locationAddress.hidden=YES;
    self.locationCloseButton.hidden=YES;
    
    self.locationName.text=@"";
    self.locationAddress.text=@"";
    
    
//   NSLayoutConstraint *constraint =[NSLayoutConstraint
//                                     constraintWithItem:self.locationDetailsView
//                                     attribute:NSLayoutAttributeWidth
//                                     relatedBy:NSLayoutRelationEqual
//                                     toItem:self.locationDetailsView
//                                     attribute:NSLayoutAttributeHeight
//                                     multiplier:7.0/64.0 //Aspect ratio: 4*height = 3*width
//                                     constant:0.0f];
//    
//    self.locationAspectRatio=constraint;
//    [self.view layoutIfNeeded];
//    [self.view updateConstraintsIfNeeded];
    
    
  //  self.locationHeightConstraint.constant=35;64/7
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize{
    
    NSLog(@"newSize %lf %lf",newSize.width,newSize.height);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
    
}

- (UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}
-(NSData*)compressImage: (UIImage *)img
{
    float MAX_UPLOAD_SIZE=100;
    float MIN_UPLOAD_RESOLUTION=756*756;
    float factor;
    float resol = img.size.height*img.size.width;
    if (resol >MIN_UPLOAD_RESOLUTION){
        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
        img = [self scaleDown:img withSize:CGSizeMake(img.size.width/factor, img.size.height/factor)];
    }
    
    //Compress the image
    CGFloat compression = 1.0f;
    CGFloat maxCompression = 0.1f;
    
    NSData *imageData = UIImageJPEGRepresentation(img, compression);
    
    while ([imageData length] > MAX_UPLOAD_SIZE && compression > maxCompression)
    {
        compression -= 0.10;
        imageData = UIImageJPEGRepresentation(img, compression);
        NSLog(@"Compress : %lu",(unsigned long)imageData.length);
    }
    return imageData;
}

//-(UIImage *)imageManager:(SDWebImageManager *)imageManager transformDownloadedImage:(UIImage *)image withURL:(NSURL *)imageURL
//{
//    // NSLog(@"imageURL : %@",imageURL);
//    UIImage *resizedImage;
//    //  return resizedImage  = [image resizedImage:CGSizeMake(512, 512) interpolationQuality:kCGInterpolationDefault];
//    
//    float MIN_UPLOAD_RESOLUTION=756*756;
//    float factor;
//    float resol = image.size.height*image.size.width;
//    if (resol >MIN_UPLOAD_RESOLUTION){
//        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
//        resizedImage = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
//        return resizedImage;
//        
//    }
//    else
//        return image;
//}

-(void) addNewPost
{
    shouldUpload=YES;
  
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    if ([self.buyButtonSwitch isOn]) {
        
        [postData setObject:[NSNumber numberWithInt:1] forKey:@"has_buy_btn"];
        
        
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        NSNumber *number = [formatter numberFromString:self.priceTextField.text];
        
        if([number intValue]>0 && [number intValue]<100)
        {
            shouldUpload=NO;
            [self.activityIndicatorView stopAnimating];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning!"
                                                            message:@"The price must be higher than US$100"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
            [alert show];
            
        }
        else
        {
            if(self.priceTextField.text.length)
            {
                [postData setObject:number forKey:@"price"];
                
            }
            else
            {
                [postData setObject:[NSNumber numberWithInt:0] forKey:@"price"];
            }
            
        }
        
        
    }else{
        
        [postData setObject:[NSNumber numberWithInt:0] forKey:@"has_buy_btn"];
        [postData setObject:[NSNumber numberWithInt:0] forKey:@"price"];
        
    }
    
    
    
    if(shouldUpload)
    {
        
        
        NSError *error = nil;
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"  +" options:NSRegularExpressionCaseInsensitive error:&error];
        
        if(self.captionTextView.text.length>0)
        {
            NSString *descriptionWithoutNewLine = [[self.captionTextView.text componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
            NSString *description= [descriptionWithoutNewLine stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
            
            NSString *trimmedString = [regex stringByReplacingMatchesInString:description options:0 range:NSMakeRange(0, [description length]) withTemplate:@" "];
            
            
            if ([trimmedString length] >= 250)
            {
                [postData setObject:[trimmedString substringToIndex:250] forKey:@"description"];
                
            }else
            {
                [postData setObject:trimmedString forKey:@"description"];
                
            }
            
        }
        else
        {
            [postData setObject:@"" forKey:@"description"];
            
        }
        
        [postData setObject:self.artistTextfield.text forKey:@"artist"];
        
        
        
        NSString *placeId=[selectedLocation objectForKey:@"placeId"];
        if(placeId.length>0)
        {
            [postData setObject:placeId forKey:@"google_place_id"];
            [postData setObject:[selectedLocation objectForKey:@"title"] forKey:@"address_title"];
            [postData setObject:[selectedLocation objectForKey:@"address"] forKey:@"address"];
            
            
        }
        else{
            
            [postData setObject:@"" forKey:@"google_place_id"];
            [postData setObject:@"" forKey:@"address_title"];
            [postData setObject:@"" forKey:@"address"];
        }
        
        if([self.hashTagTextView.text isEqualToString:@"Add some hashtags and @users to enhance your post"])
            [postData setObject:@"" forKey:@"hashtags"];
        else if(self.hashTagTextView.text.length)
        {
            __block NSString *lastWord = nil;
            
            [self.hashTagTextView.text enumerateSubstringsInRange:NSMakeRange(0, [self.hashTagTextView.text length]) options:NSStringEnumerationByWords | NSStringEnumerationReverse usingBlock:^(NSString *substring, NSRange subrange, NSRange enclosingRange, BOOL *stop) {
                lastWord = substring;
                *stop = YES;
            }];
            NSLog(@"lastWord %@",lastWord);
            
            
            if(![self.hashTagTextView.text containsString:@"#"] && ![self.hashTagTextView.text containsString:@"@"])
            {
                NSLog(@"no #");
                
                
                NSRange lastComma = [self.hashTagTextView.text rangeOfString:lastWord options:NSBackwardsSearch];
                
                if(lastComma.location != NSNotFound) {
                    self.hashTagTextView.text = [self.hashTagTextView.text stringByReplacingCharactersInRange:lastComma
                                                                                                   withString:[NSString stringWithFormat:@"#%@",lastWord]];
                    
                    
                }
            }
            else
            {
                NSLog(@"end of the string");
                
                NSError *error = NULL;
                NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\\s\\w+$" options:NSRegularExpressionCaseInsensitive error:&error];
                if(!error) {
                    NSUInteger numberOfMatches = [regex numberOfMatchesInString:self.hashTagTextView.text options:0 range:NSMakeRange(0, [self.hashTagTextView.text length])];
                    if(numberOfMatches > 0)
                    {
                        //yes
                        NSRange lastComma = [self.hashTagTextView.text rangeOfString:lastWord options:NSBackwardsSearch];
                        
                        if(lastComma.location != NSNotFound) {
                            self.hashTagTextView.text = [self.hashTagTextView.text stringByReplacingCharactersInRange:lastComma
                                                                                                           withString:[NSString stringWithFormat:@"#%@",lastWord]];
                            
                        }
                        
                    }
                    
                }
                
            }
            [postData setObject:self.hashTagTextView.text forKey:@"hashtags"];
            
        }
        else
            [postData setObject:@"" forKey:@"hashtags"];
        
        
        if ([self.saveToMyGalleryButton isOn]) {
            [postData setObject:[NSNumber numberWithInt:1] forKey:@"is_gallery_item"];
            [postData setObject:[NSNumber numberWithInt:1] forKey:@"is_pinned"];
            
            
        }else{
            [postData setObject:[NSNumber numberWithInt:0] forKey:@"is_gallery_item"];
            [postData setObject:[NSNumber numberWithInt:0] forKey:@"is_pinned"];
            
        }
        
        if ([self.publicPostToggle isOn]) {
            [postData setObject:[NSNumber numberWithInt:1] forKey:@"is_public"];
            
        }else{
            [postData setObject:[NSNumber numberWithInt:0] forKey:@"is_public"];
            
        }
        
        
        
        
        double aspectRatio= self.selectedPhoto.image.size.width/self.selectedPhoto.image.size.height;
        [postData setObject:[NSNumber numberWithFloat:aspectRatio] forKey:@"aspect_ratio"];
        
        NSError * err;
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:taggedUsers options:0 error:&err];
        NSString * taggedString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [postData setObject:taggedString forKey:@"tagged_users"];
        
        NSLog(@"postData %@",postData);
        
    
        [[ServerManager sharedManager] postNewImage:self.selectedPhoto.image withDetails:postData withcompletion:^(BOOL success) {
            [self.activityIndicatorView stopAnimating];
            
            if (success) {
                
                if([UserAccount sharedManager].isSavetoAlbum)
                    UIImageWriteToSavedPhotosAlbum(self.selectedPhoto.image, nil, nil, nil);
                
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"loadAllFeed" object:nil userInfo:nil];
                
                UINavigationController* navController = self.navigationController;
                NSArray *viewControllers=[navController viewControllers];
                NSLog(@"viewControllers %@",viewControllers);
                UIViewController* controller = [viewControllers objectAtIndex:0];
                
                [controller dismissViewControllerAnimated:NO completion:nil];

                
            }else
            {
                [self.activityIndicatorView stopAnimating];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed!"
                                                                message:@"Something went wrong, Please try again"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles: nil];
                [alert show];
            }
        }];
        
    }

}

-(void)updateNewPost
{
    self.view.userInteractionEnabled=NO;
    
    
    
    shouldUpload=YES;
    
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    if ([self.buyButtonSwitch isOn]) {
        
        [postData setObject:[NSNumber numberWithInt:1] forKey:@"has_buy_btn"];
        
        
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        NSNumber *number = [formatter numberFromString:self.priceTextField.text];
        
        if([number intValue]>0 && [number intValue]<100)
        {
            shouldUpload=NO;
            [self.activityIndicatorView stopAnimating];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning!"
                                                            message:@"The price must be higher than US$100"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
            [alert show];
            
        }
        else
        {
            if(self.priceTextField.text.length)
            {
                [postData setObject:number forKey:@"price"];
                
            }
            else
            {
                [postData setObject:[NSNumber numberWithInt:0] forKey:@"price"];
            }
            
        }
        
        
    }else{
        
        [postData setObject:[NSNumber numberWithInt:0] forKey:@"has_buy_btn"];
        [postData setObject:[NSNumber numberWithInt:0] forKey:@"price"];
        
    }
    
    
    
    if(shouldUpload)
    {
        
        
        [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"user_id"];
        [postData setObject:[NSNumber numberWithInt:[[self.postDetails objectForKey:@"id"] intValue]] forKey:@"post_id"];
        [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
        
        //        [postData setObject:[self.captionTextView.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"description"];
        //
        
        NSError *error = nil;
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"  +" options:NSRegularExpressionCaseInsensitive error:&error];
        
        if(self.captionTextView.text.length>0)
        {
            NSString *descriptionWithoutNewLine = [[self.captionTextView.text componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
            NSString *description= [descriptionWithoutNewLine stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
            
            NSString *trimmedString = [regex stringByReplacingMatchesInString:description options:0 range:NSMakeRange(0, [description length]) withTemplate:@" "];
            
            
            if ([trimmedString length] >= 250)
            {
                NSLog(@"greater than 250");
                [postData setObject:[trimmedString substringToIndex:250] forKey:@"description"];
                
            }else
            {
                [postData setObject:trimmedString forKey:@"description"];
                
            }
            
        }
        else
        {
            [postData setObject:@"" forKey:@"description"];
            
        }
        
        
        [postData setObject:self.artistTextfield.text forKey:@"artist"];
        
        
        NSString *placeId=[selectedLocation objectForKey:@"placeId"];
        if(placeId.length>0)
        {
            [postData setObject:placeId forKey:@"google_place_id"];
            [postData setObject:[selectedLocation objectForKey:@"title"] forKey:@"address_title"];
            [postData setObject:[selectedLocation objectForKey:@"address"] forKey:@"address"];
            
            
        }
        else{
            
            [postData setObject:@"" forKey:@"google_place_id"];
            [postData setObject:@"" forKey:@"address_title"];
            [postData setObject:@"" forKey:@"address"];
        }
        
        //        NSError *error = nil;
        //
        //        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"  +" options:NSRegularExpressionCaseInsensitive error:&error];
        //
        //
        //        NSString *descriptionWithoutNewLine = [[self.captionTextView.text componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
        //        NSString *description= [descriptionWithoutNewLine stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
        //
        //        NSString *trimmedString = [regex stringByReplacingMatchesInString:description options:0 range:NSMakeRange(0, [description length]) withTemplate:@" "];
        //
        //        [postData setObject:trimmedString forKey:@"description"];
        //
        
        
        
        [postData setObject:self.hashTagTextView.text forKey:@"hashtags"];
        
        
        if([self.hashTagTextView.text isEqualToString:@"Add some hashtags and @users to enhance your post"])
            [postData setObject:@"" forKey:@"hashtags"];
        else if(self.hashTagTextView.text.length)
        {
            __block NSString *lastWord = nil;
            
            [self.hashTagTextView.text enumerateSubstringsInRange:NSMakeRange(0, [self.hashTagTextView.text length]) options:NSStringEnumerationByWords | NSStringEnumerationReverse usingBlock:^(NSString *substring, NSRange subrange, NSRange enclosingRange, BOOL *stop) {
                lastWord = substring;
                *stop = YES;
            }];
            NSLog(@"lastWord %@",lastWord);
            
            
            if(![self.hashTagTextView.text containsString:@"#"] && ![self.hashTagTextView.text containsString:@"@"])
            {
                NSLog(@"no #");
                
                
                NSRange lastComma = [self.hashTagTextView.text rangeOfString:lastWord options:NSBackwardsSearch];
                
                if(lastComma.location != NSNotFound) {
                    self.hashTagTextView.text = [self.hashTagTextView.text stringByReplacingCharactersInRange:lastComma
                                                                                                   withString:[NSString stringWithFormat:@"#%@",lastWord]];
                    
                    
                }
            }
            else
            {
                NSLog(@"end of the string");
                
                NSError *error = NULL;
                NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\\s\\w+$" options:NSRegularExpressionCaseInsensitive error:&error];
                if(!error) {
                    NSUInteger numberOfMatches = [regex numberOfMatchesInString:self.hashTagTextView.text options:0 range:NSMakeRange(0, [self.hashTagTextView.text length])];
                    if(numberOfMatches > 0)
                    {
                        //yes
                        NSRange lastComma = [self.hashTagTextView.text rangeOfString:lastWord options:NSBackwardsSearch];
                        
                        if(lastComma.location != NSNotFound) {
                            self.hashTagTextView.text = [self.hashTagTextView.text stringByReplacingCharactersInRange:lastComma
                                                                                                           withString:[NSString stringWithFormat:@"#%@",lastWord]];
                            
                        }
                        
                    }
                    
                }
                
            }
            [postData setObject:self.hashTagTextView.text forKey:@"hashtags"];
            
        }
        else
            [postData setObject:@"" forKey:@"hashtags"];
        
        
        NSError * err;
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:taggedUsers options:0 error:&err];
        NSString * taggedString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [postData setObject:taggedString forKey:@"tagged_users"];
        
        NSLog(@"postData %@",postData);
        
        
        [[ServerManager sharedManager] updatePostForId:[[self.postDetails objectForKey:@"id"] intValue ] withDetails:postData withcompletion:^(BOOL success) {
            [self.activityIndicatorView stopAnimating];
            
            if (success) {
                self.view.userInteractionEnabled=YES;
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshPost" object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"loadAllFeed" object:nil userInfo:nil];
                
                [self.navigationController popViewControllerAnimated:YES];

                
                
            }else
            {
                self.view.userInteractionEnabled=YES;
                
                [self.activityIndicatorView stopAnimating];
                
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed!"
                                                                message:@"Something went wrong, Please check your network connection"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles: nil];
                [alert show];

            }
        }];
        
    }
    
}

-(void) createPost
{
    
//    if(self.captionTextView.text.length>0)
//    {
      //  self.view.userInteractionEnabled=NO;
    shouldUpload=YES;
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.requestSerializer = [AFJSONRequestSerializer serializer];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    [postData setObject:ACCESS_KEY forKey:@"access_key"];

    
    if ([self.buyButtonSwitch isOn]) {
        
        [postData setObject:[NSNumber numberWithInt:1] forKey:@"has_buy_btn"];
        
        
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        NSNumber *number = [formatter numberFromString:self.priceTextField.text];
        
        if([number intValue]>0 && [number intValue]<100)
        {
            shouldUpload=NO;
            [self.activityIndicatorView stopAnimating];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning!"
                                                            message:@"The price must be higher than US$100"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
            [alert show];
            
        }
        else
        {
            if(self.priceTextField.text.length)
            {
                [postData setObject:number forKey:@"price"];
                
            }
            else
            {
                [postData setObject:[NSNumber numberWithInt:0] forKey:@"price"];
            }

        }

        
    }else{
        
        [postData setObject:[NSNumber numberWithInt:0] forKey:@"has_buy_btn"];
        [postData setObject:[NSNumber numberWithInt:0] forKey:@"price"];
        
    }
    
    
   
    if(shouldUpload)
    {
    
        
        NSError *error = nil;
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"  +" options:NSRegularExpressionCaseInsensitive error:&error];
    
    if(self.captionTextView.text.length>0)
    {
        NSString *descriptionWithoutNewLine = [[self.captionTextView.text componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
        NSString *description= [descriptionWithoutNewLine stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
        
        NSString *trimmedString = [regex stringByReplacingMatchesInString:description options:0 range:NSMakeRange(0, [description length]) withTemplate:@" "];
        
        
        if ([trimmedString length] >= 250)
        {
            [postData setObject:[trimmedString substringToIndex:250] forKey:@"description"];
            
        }else
        {
            [postData setObject:trimmedString forKey:@"description"];
            
        }
        
    }
    else
    {
        [postData setObject:@"" forKey:@"description"];
        
    }
        
        [postData setObject:self.artistTextfield.text forKey:@"artist"];
        
        
        
        [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"user_id"];
        [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
       // [postData setObject:selectedLocation forKey:@"location"];

        NSString *placeId=[selectedLocation objectForKey:@"placeId"];
        if(placeId.length>0)
        {
            [postData setObject:placeId forKey:@"google_place_id"];
            [postData setObject:[selectedLocation objectForKey:@"title"] forKey:@"address_title"];
            [postData setObject:[selectedLocation objectForKey:@"address"] forKey:@"address"];

            
        }
        else{
            
            [postData setObject:@"" forKey:@"google_place_id"];
            [postData setObject:@"" forKey:@"address_title"];
            [postData setObject:@"" forKey:@"address"];
        }
        
        if([self.hashTagTextView.text isEqualToString:@"Add some hashtags and @users to enhance your post"])
            [postData setObject:@"" forKey:@"hashtags"];
        else if(self.hashTagTextView.text.length)
        {
            __block NSString *lastWord = nil;
            
            [self.hashTagTextView.text enumerateSubstringsInRange:NSMakeRange(0, [self.hashTagTextView.text length]) options:NSStringEnumerationByWords | NSStringEnumerationReverse usingBlock:^(NSString *substring, NSRange subrange, NSRange enclosingRange, BOOL *stop) {
                lastWord = substring;
                *stop = YES;
            }];
            NSLog(@"lastWord %@",lastWord);
            
            
            if(![self.hashTagTextView.text containsString:@"#"] && ![self.hashTagTextView.text containsString:@"@"])
            {
                NSLog(@"no #");
                
                
                NSRange lastComma = [self.hashTagTextView.text rangeOfString:lastWord options:NSBackwardsSearch];
                
                if(lastComma.location != NSNotFound) {
                    self.hashTagTextView.text = [self.hashTagTextView.text stringByReplacingCharactersInRange:lastComma
                                                                                                   withString:[NSString stringWithFormat:@"#%@",lastWord]];
                    
                    
                }
            }
            else
            {
                NSLog(@"end of the string");
                
                NSError *error = NULL;
                NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\\s\\w+$" options:NSRegularExpressionCaseInsensitive error:&error];
                if(!error) {
                    NSUInteger numberOfMatches = [regex numberOfMatchesInString:self.hashTagTextView.text options:0 range:NSMakeRange(0, [self.hashTagTextView.text length])];
                    if(numberOfMatches > 0)
                    {
                        //yes
                        NSRange lastComma = [self.hashTagTextView.text rangeOfString:lastWord options:NSBackwardsSearch];
                        
                        if(lastComma.location != NSNotFound) {
                            self.hashTagTextView.text = [self.hashTagTextView.text stringByReplacingCharactersInRange:lastComma
                                                                                                           withString:[NSString stringWithFormat:@"#%@",lastWord]];
                            
                        }
                        
                    }
                    
                }
                
            }
            [postData setObject:self.hashTagTextView.text forKey:@"hashtags"];
            
        }
        else
             [postData setObject:@"" forKey:@"hashtags"];
        
        
        if ([self.saveToMyGalleryButton isOn]) {
            [postData setObject:[NSNumber numberWithInt:1] forKey:@"is_gallery_item"];
            [postData setObject:[NSNumber numberWithInt:1] forKey:@"is_pinned"];
            
            
        }else{
            [postData setObject:[NSNumber numberWithInt:0] forKey:@"is_gallery_item"];
            [postData setObject:[NSNumber numberWithInt:0] forKey:@"is_pinned"];

        }
        
        if ([self.publicPostToggle isOn]) {
            [postData setObject:[NSNumber numberWithInt:1] forKey:@"is_public"];
            
        }else{
            [postData setObject:[NSNumber numberWithInt:0] forKey:@"is_public"];
            
        }
        
    
    
    
        double aspectRatio= self.selectedPhoto.image.size.width/self.selectedPhoto.image.size.height;
        [postData setObject:[NSNumber numberWithFloat:aspectRatio] forKey:@"aspect_ratio"];
        
        NSError * err;
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:taggedUsers options:0 error:&err];
        NSString * taggedString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [postData setObject:taggedString forKey:@"TaggedUsers"];
        
        NSLog(@"postData %@",postData);
        
        [apiLoginManager POST:[NSString stringWithFormat:@"%@/posts/api-add-post",SERVER_BASE_API_URL] parameters:
         //[apiLoginManager POST:[NSString stringWithFormat:@"http://192.168.1.2:8888/tmp/jsons/api-submit-content"] parameters:
         postData constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
             
             //NSData *imageData = UIImageJPEGRepresentation(self.selectedPhoto.image, 0.5);
            
            // NSData *imageData = UIImageJPEGRepresentation([self  imageWithImage:self.selectedPhoto.image scaledToSize:CGSizeMake(self.selectedPhoto.image.size.width/4, self.selectedPhoto.image.size.height/4)], 0.5f);
             NSData *imageData = [self compressImage:self.selectedPhoto.image];
             
             [formData appendPartWithFileData:imageData
                                         name:@"image"
                                     fileName:@"image.jpg" mimeType:@"image/jpeg"];
             
             
             
             
         } success:^(AFHTTPRequestOperation *operation, id responseObject) {
             
             [self.activityIndicatorView stopAnimating];
             
             
             if([[responseObject objectForKey:@"success"] integerValue]==1)
             {
                 if([UserAccount sharedManager].isSavetoAlbum)
                     UIImageWriteToSavedPhotosAlbum(self.selectedPhoto.image, nil, nil, nil);
                 
                 NSLog(@"Response: %@", responseObject);
                 
                 
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"loadAllFeed" object:nil userInfo:nil];
                 
                 UINavigationController* navController = self.navigationController;
                 NSArray *viewControllers=[navController viewControllers];
                 NSLog(@"viewControllers %@",viewControllers);
                 UIViewController* controller = [viewControllers objectAtIndex:0];
                 
                 [controller dismissViewControllerAnimated:NO completion:nil];
                 
                 //self.view.userInteractionEnabled=YES;
                 //[self.activityIndicatorView stopAnimating];
             
             }
             else
             {
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed!"
                                                                 message:@"Something went wrong"
                                                                delegate:nil
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles: nil];
                 [alert show];
                 
             }
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             
             //self.view.userInteractionEnabled=YES;
             
             NSLog(@"...........%@",operation.responseString);
             
             [self.activityIndicatorView stopAnimating];
             
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed!"
                                                             message:@"Something went wrong, Please check your network connection"
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles: nil];
             [alert show];
             
         }];

//    }
//    else
//    {
//        
//        [self.activityIndicatorView stopAnimating];
//        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
//                                                        message:@"Please add a post description"
//                                                       delegate:nil
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles: nil];
//        [alert show];
//
//    }
    
    }
    
}


-(void) updatePost
{
    shouldUpload=YES;
//    if(self.captionTextView.text.length>0)
//    {
        self.view.userInteractionEnabled=NO;
        
        AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
        apiLoginManager.requestSerializer = [AFJSONRequestSerializer serializer];
        apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        
        NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
        [postData setObject:ACCESS_KEY forKey:@"access_key"];
    
    
    if ([self.buyButtonSwitch isOn]) {
        
        [postData setObject:[NSNumber numberWithInt:1] forKey:@"has_buy_btn"];
        
        
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        NSNumber *number = [formatter numberFromString:self.priceTextField.text];
        
        if([number intValue]>0 && [number intValue]<100)
        {
            shouldUpload=NO;
            [self.activityIndicatorView stopAnimating];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning!"
                                                            message:@"The price must be higher than US$100"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
            [alert show];
            
        }
        else
        {
            if(self.priceTextField.text.length)
            {
                [postData setObject:number forKey:@"price"];
                
            }
            else
            {
                [postData setObject:[NSNumber numberWithInt:0] forKey:@"price"];
            }
            
        }
        
        
    }else{
        
        [postData setObject:[NSNumber numberWithInt:0] forKey:@"has_buy_btn"];
        [postData setObject:[NSNumber numberWithInt:0] forKey:@"price"];
        
    }
    
   
    
    if(shouldUpload)
    {
    
        [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"user_id"];
        [postData setObject:[NSNumber numberWithInt:[[self.postDetails objectForKey:@"id"] intValue]] forKey:@"post_id"];
        [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"current_user_id"];
        
//        [postData setObject:[self.captionTextView.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"description"];
//        
        
        NSError *error = nil;
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"  +" options:NSRegularExpressionCaseInsensitive error:&error];
        
        if(self.captionTextView.text.length>0)
        {
            NSString *descriptionWithoutNewLine = [[self.captionTextView.text componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
            NSString *description= [descriptionWithoutNewLine stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
            
            NSString *trimmedString = [regex stringByReplacingMatchesInString:description options:0 range:NSMakeRange(0, [description length]) withTemplate:@" "];
            
            
            if ([trimmedString length] >= 250)
            {
                NSLog(@"greater than 250");
                [postData setObject:[trimmedString substringToIndex:250] forKey:@"description"];
                
            }else
            {
                [postData setObject:trimmedString forKey:@"description"];
                
            }
            
        }
        else
        {
            [postData setObject:@"" forKey:@"description"];
            
        }
       
    
        [postData setObject:self.artistTextfield.text forKey:@"artist"];
        
        
        NSString *placeId=[selectedLocation objectForKey:@"placeId"];
        if(placeId.length>0)
        {
            [postData setObject:placeId forKey:@"google_place_id"];
            [postData setObject:[selectedLocation objectForKey:@"title"] forKey:@"address_title"];
            [postData setObject:[selectedLocation objectForKey:@"address"] forKey:@"address"];
            
            
        }
        else{
            
            [postData setObject:@"" forKey:@"google_place_id"];
            [postData setObject:@"" forKey:@"address_title"];
            [postData setObject:@"" forKey:@"address"];
        }

//        NSError *error = nil;
//        
//        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"  +" options:NSRegularExpressionCaseInsensitive error:&error];
//        
//        
//        NSString *descriptionWithoutNewLine = [[self.captionTextView.text componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
//        NSString *description= [descriptionWithoutNewLine stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
//        
//        NSString *trimmedString = [regex stringByReplacingMatchesInString:description options:0 range:NSMakeRange(0, [description length]) withTemplate:@" "];
//        
//        [postData setObject:trimmedString forKey:@"description"];
//        

        
        
        [postData setObject:self.hashTagTextView.text forKey:@"hashtags"];

        
        if([self.hashTagTextView.text isEqualToString:@"Add some hashtags and @users to enhance your post"])
            [postData setObject:@"" forKey:@"hashtags"];
        else if(self.hashTagTextView.text.length)
        {
            __block NSString *lastWord = nil;
            
            [self.hashTagTextView.text enumerateSubstringsInRange:NSMakeRange(0, [self.hashTagTextView.text length]) options:NSStringEnumerationByWords | NSStringEnumerationReverse usingBlock:^(NSString *substring, NSRange subrange, NSRange enclosingRange, BOOL *stop) {
                lastWord = substring;
                *stop = YES;
            }];
            NSLog(@"lastWord %@",lastWord);
            
            
            if(![self.hashTagTextView.text containsString:@"#"] && ![self.hashTagTextView.text containsString:@"@"])
            {
                NSLog(@"no #");
                
                
                NSRange lastComma = [self.hashTagTextView.text rangeOfString:lastWord options:NSBackwardsSearch];
                
                if(lastComma.location != NSNotFound) {
                    self.hashTagTextView.text = [self.hashTagTextView.text stringByReplacingCharactersInRange:lastComma
                                                                                                   withString:[NSString stringWithFormat:@"#%@",lastWord]];
                    
                    
                }
            }
            else
            {
                NSLog(@"end of the string");
                
                NSError *error = NULL;
                NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\\s\\w+$" options:NSRegularExpressionCaseInsensitive error:&error];
                if(!error) {
                    NSUInteger numberOfMatches = [regex numberOfMatchesInString:self.hashTagTextView.text options:0 range:NSMakeRange(0, [self.hashTagTextView.text length])];
                    if(numberOfMatches > 0)
                    {
                        //yes
                        NSRange lastComma = [self.hashTagTextView.text rangeOfString:lastWord options:NSBackwardsSearch];
                        
                        if(lastComma.location != NSNotFound) {
                            self.hashTagTextView.text = [self.hashTagTextView.text stringByReplacingCharactersInRange:lastComma
                                                                                                           withString:[NSString stringWithFormat:@"#%@",lastWord]];
                            
                        }
                        
                    }
                    
                }
                
            }
            [postData setObject:self.hashTagTextView.text forKey:@"hashtags"];
            
        }
        else
            [postData setObject:@"" forKey:@"hashtags"];
    

    
        
    
        [postData setObject:taggedUsers forKey:@"TaggedUsers"];
        
        NSLog(@"postData %@",postData);
        
        [apiLoginManager POST:[NSString stringWithFormat:@"%@/posts/api-edit-post",SERVER_BASE_API_URL] parameters:postData
                      success:^(AFHTTPRequestOperation *operation, id responseObject) {
                          
                          self.view.userInteractionEnabled=YES;
                          [self.activityIndicatorView stopAnimating];
                          
                          
                          NSLog(@"Response of update: %@", responseObject);
                          
                          NSString * successMsg = [responseObject objectForKey:@"success"];
                          
                          if ([successMsg integerValue] == 1 ) {
                              
                              // NSLog(@"success msg %@",[responseObject objectForKey:@"success"]);
                              
//                              UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success!"
//                                                    
//                                                                              message:@"Your post has been updated"
//                                                                             delegate:self
//                                                                    cancelButtonTitle:@"OK"
//                                                                    otherButtonTitles: nil];
//                              [alert show];
                              
                              
                          
                              [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshPost" object:nil];
                              [[NSNotificationCenter defaultCenter] postNotificationName:@"loadAllFeed" object:nil userInfo:nil];
                              
                              [self.navigationController popViewControllerAnimated:YES];
                            }
                          
                          
                      }
                      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                          
                          NSLog(@"Error: %@", error);
                          
                          self.view.userInteractionEnabled=YES;
                          
                          NSLog(@"%@",operation.responseString);
                          [self.activityIndicatorView stopAnimating];
                          
                          
                          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed!"
                                                                          message:@"Something went wrong, Please check your network connection"
                                                                         delegate:nil
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles: nil];
                          [alert show];
                          
                          
                          
                      }];


    }
    
}


- (IBAction)arteSconButtonAction:(id)sender {
    
//    YCameraViewController *camController = [[YCameraViewController alloc] initWithNibName:@"YCameraViewController" bundle:nil];
//    camController.delegate=self;
//    [self presentViewController:camController animated:YES completion:^{
//        // completion code
//    }];
    
//    MMCameraPickerController *cameraPicker=[self.storyboard instantiateViewControllerWithIdentifier:@"MMCameraPickerController"];
//    cameraPicker.camdelegate=self;
//    
//    [self presentViewController:cameraPicker animated:YES completion:nil];
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:NSLocalizedString(@"Scan", nil),NSLocalizedString(@"Select from Library", nil), nil];
    
    
    
    [sheet showInView:self.view];
    
    
   
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    
    if (buttonIndex == 0) {
        
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            
            UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                  message:@"Device has no camera"
                                                                 delegate:nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles: nil];
            
            [myAlertView show];
            
        }
        else
        {
            
            //        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            //        picker.delegate = self;
            //        picker.allowsEditing = NO;
            //        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            //
            //        [self presentViewController:picker animated:YES completion:NULL];
            
            
            
            UIImagePickerControllerSourceType source = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] ? UIImagePickerControllerSourceTypeCamera : UIImagePickerControllerSourceTypeSavedPhotosAlbum;
            UIImagePickerController *cameraController = [[UIImagePickerController alloc] init];
            cameraController.delegate = self;
            cameraController.sourceType = source;
            cameraController.allowsEditing = NO;
            [self presentViewController:cameraController animated:YES completion:^{
                //iOS 8 bug.  the status bar will sometimes not be hidden after the camera is displayed, which causes the preview after an image is captured to be black
                if (source == UIImagePickerControllerSourceTypeCamera) {
                    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
                }
            }];
            
        }
        
        
    }else if(buttonIndex ==1)
    {
        UIImagePickerControllerSourceType source = UIImagePickerControllerSourceTypePhotoLibrary;
        UIImagePickerController *cameraController = [[UIImagePickerController alloc] init];
        cameraController.delegate = self;
        cameraController.sourceType = source;
        cameraController.allowsEditing = NO;
        [self presentViewController:cameraController animated:YES completion:^{
            //iOS 8 bug.  the status bar will sometimes not be hidden after the camera is displayed, which causes the preview after an image is captured to be black
            if (source == UIImagePickerControllerSourceTypeCamera) {
                [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
            }
        }];
        
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [picker dismissViewControllerAnimated:NO completion:^{
        
    }];
    
    TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:TOCropViewCroppingStyleDefault image:[info objectForKey:UIImagePickerControllerOriginalImage]];
    cropController.delegate = self;
    [self presentViewController:cropController animated:YES completion:nil];

  
  
    
}

#pragma mark Camera Delegate
-(void)didFinishCaptureImage:(UIImage *)capturedImage withMMCam:(MMCameraPickerController*)cropcam{
    
    [self.activityIndicatorView startAnimating];
    
    [cropcam closeWithCompletion:^{
       
        if(capturedImage!=nil){
           // [self OCR:capturedImage];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self performImageRecognition:capturedImage];
                

            });
        }
        else
            [self.activityIndicatorView stopAnimating];
        
    }];
    
    
}
//
//#pragma mark - YCameraViewController Delegate
//- (void)didFinishPickingImage:(UIImage *)image{
//  // [self.imageView setImage:image];
//   // UIImage* scaledImage = [self scaleImage:image withMaxDimension:640];
//
//    
//    [self performImageRecognition:image];
//    
//   // dismissViewControllerAnimated(true, completion: {
//       // self.performImageRecognition(scaledImage)
//    
//   // })
//}
//
//- (void)yCameraControllerdidSkipped{
//  //  [self.imageView setImage:nil];
//}
//
//- (void)yCameraControllerDidCancel{
//    
//}

-(void)performImageRecognition:(UIImage*) image
{
   
    G8RecognitionOperation *operation = [[G8RecognitionOperation alloc] init];
    
    //G8Tesseract *tesseract = [[G8Tesseract alloc]init];
    // 2
    operation.tesseract.language = @"eng";
    // 3
    operation.tesseract.engineMode = G8OCREngineModeTesseractCubeCombined;
    // 4
    operation.tesseract.pageSegmentationMode = G8PageSegmentationModeAuto;
    // 5
    operation.tesseract.maximumRecognitionTime = 60.0;
    // 6
    operation.tesseract.image = [image g8_blackAndWhite];
    operation.tesseract.delegate=self;
    
    //[tesseract recognize];
    // 7
    
    operation.recognitionCompleteBlock = ^(G8Tesseract *recognizedTesseract) {
        // Retrieve the recognized text upon completion
       
       NSLog(@" OCR TEXT %@", [recognizedTesseract recognizedText]);
       NSString *OCRText = [recognizedTesseract recognizedText];
       
       NSString *trimmedString = [OCRText stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
       
       [self.activityIndicatorView stopAnimating];
       if(trimmedString.length>0)
       {
           if(self.captionTextView.text.length>0){
               
               self.captionTextView.text=[NSString stringWithFormat:@"%@\n%@",self.captionTextView.text,trimmedString];
           
              
           
                int currentLimit = 250 - (int)self.captionTextView.text.length;
               
                NSLog(@"Current text lenth %d",currentLimit);
                     
                 if (currentLimit<=0) {
               //[self.view endEditing:YES];
                    self.descriptionCounter.textColor=[UIColor redColor];
                 }
                  else
                    self.descriptionCounter.textColor=[UIColor whiteColor];
           
           
                 [self.descriptionCounter setText:[NSString stringWithFormat:@"%i/250", currentLimit]];
           }
           else
           {
               self.captionHintLabel.hidden = YES;
               self.captionTextView.text=trimmedString;
               
               int currentLimit = 250 - (int)trimmedString.length;
               
               NSLog(@"Current text lenth %d",currentLimit);
               
               if (currentLimit<=0) {
                   //[self.view endEditing:YES];
                   self.descriptionCounter.textColor=[UIColor redColor];
               }
               else
                   self.descriptionCounter.textColor=[UIColor whiteColor];
               
               [self.descriptionCounter setText:[NSString stringWithFormat:@"%i/250", currentLimit]];
           }
           
       }else
       {
           UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"We're sorry!"
                                 
                                                           message:@"Unfortunately we were unable to detect your text. Please try again with a clearer image."
                                                          delegate:self
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles: nil];
           [alert show];
       }
       
       
       
    };
    
    // Add operation to queue
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [queue addOperation:operation];
    
    
    
    
   
}
#pragma mark OCR delegate
- (void)progressImageRecognitionForTesseract:(G8Tesseract *)tesseract {
    //    NSLog(@"progress: %lu", (unsigned long)tesseract.progress);
}

- (BOOL)shouldCancelImageRecognitionForTesseract:(G8Tesseract *)tesseract {
    return NO;  // return YES, if you need to interrupt tesseract before it finishes
}



//-(UIImage*) scaleImage:(UIImage*) targetedImage withMaxDimension:(CGFloat) size
//{
//    CGSize scaledSize= CGSizeMake(size, size);
//    CGFloat scaleFactor;
//    
//    if (targetedImage.size.width > targetedImage.size.height) {
//        scaleFactor = targetedImage.size.height / targetedImage.size.width;
//        scaledSize.width = size;
//        scaledSize.height = scaledSize.width * scaleFactor;
//    } else {
//        scaleFactor = targetedImage.size.width / targetedImage.size.height;
//        scaledSize.height = size;
//        scaledSize.width = scaledSize.height * scaleFactor;
//    }
//    
//    UIGraphicsBeginImageContext(scaledSize);
//    [targetedImage drawInRect:(CGRectMake(0, 0, scaledSize.width, scaledSize.height))];
//    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    
//    return scaledImage;
//}


- (IBAction)tagPeopleButtonAction:(id)sender {
    
    TagPeopleViewController *vc  = [self.storyboard instantiateViewControllerWithIdentifier:@"TagPeopleViewController"];
    vc.selectedImage = self.selectedPhoto.image;
    vc.delegate=self;
    
    
    if(self.isForEdit){
        
        NSLog(@"taggedUsers in sharePhoto %@",taggedUsers);
        
        vc.taggedUsers = taggedUsers;
        vc.isForEdit = YES;
        
    }else{
        
        vc.taggedUsers = taggedUsers;
        
        vc.isForEdit = YES;
    }
        
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)addLocationButtonAction:(id)sender {
    
    LocationsViewController *vc  = [self.storyboard instantiateViewControllerWithIdentifier:@"LocationsViewController"];
    vc.delegate=self;
    [self presentViewController:vc animated:YES completion:nil];
    
}

- (IBAction)shareButtonAction:(id)sender {
    [self.activityIndicatorView startAnimating];
    
    
    if(self.isForEdit)
        [self updateNewPost];
    else
        [self addNewPost];
}

- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    //[self dismissViewControllerAnimated:YES completion:nil];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
   
    //[self.navigationController popViewControllerAnimated:NO];
    
    if([alertView.title isEqualToString:@"We're sorry!"])
    {
        
    }
    else
    {
        UINavigationController* navController = self.navigationController;
        NSArray *viewControllers=[navController viewControllers];
        NSLog(@"viewControllers %@",viewControllers);
        UIViewController* controller = [viewControllers objectAtIndex:0];
        
        if(self.isForEdit)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshPost" object:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [controller dismissViewControllerAnimated:NO completion:nil];
        }
    }
    
}

#pragma mark - Cropper Delegate -
-(void)cropViewController:(TOCropViewController *)cropViewController didFinishCancelled:(BOOL)cancelled
{
   // [self updateImageViewWithImage:_capturedImageV.image fromCropViewController:cropViewController];
    [cropViewController dismissViewControllerAnimated:YES completion:^{
        NSLog(@"cancel");
        
    }];
    
}

- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    
    //
    //    self.croppedFrame = cropRect;
    //    self.angle = angle;
    //    [self updateImageViewWithImage:image fromCropViewController:cropViewController];
    //
    //
    

    [cropViewController dismissViewControllerAnimated:YES completion:^{
        
            [self.activityIndicatorView startAnimating];
        
      //  self.selectedPhoto.image=image;
        
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self performImageRecognition:image];
                });
        
    }];
    
    //    [cropViewController dismissAnimatedFromParentViewController:self
    //                                               withCroppedImage:image
    //                                                         toView:_capturedImageV
    //                                                        toFrame:CGRectZero
    //                                                          setup:^{ [self layoutImageView]; }
    //                                                     completion:
    //     ^{
    //         _capturedImageV.hidden = NO;
    //
    //
    //     }];
    
    
}

- (void)updateImageViewWithImage:(UIImage *)image fromCropViewController:(TOCropViewController *)cropViewController
{
    NSLog(@"updateImageViewWithImage");
    
    [self layoutImageView];
    
    
}

#pragma mark - Image Layout -
- (void)layoutImageView
{
    
    NSLog(@"layoutImageView");
    //    if (self.imageView.image == nil)
    //        return;
    //
    //    CGFloat padding = 20.0f;
    //
    //    CGRect viewFrame = self.view.bounds;
    //    viewFrame.size.width -= (padding * 2.0f);
    //    viewFrame.size.height -= ((padding * 2.0f));
    //
    //    CGRect imageFrame = CGRectZero;
    //    imageFrame.size = self.imageView.image.size;
    //
    //    if (self.imageView.image.size.width > viewFrame.size.width ||
    //        self.imageView.image.size.height > viewFrame.size.height)
    //    {
    //        CGFloat scale = MIN(viewFrame.size.width / imageFrame.size.width, viewFrame.size.height / imageFrame.size.height);
    //        imageFrame.size.width *= scale;
    //        imageFrame.size.height *= scale;
    //        imageFrame.origin.x = (CGRectGetWidth(self.view.bounds) - imageFrame.size.width) * 0.5f;
    //        imageFrame.origin.y = (CGRectGetHeight(self.view.bounds) - imageFrame.size.height) * 0.5f;
    //        self.imageView.frame = imageFrame;
    //    }
    //    else {
    //        self.imageView.frame = imageFrame;
    //        self.imageView.center = (CGPoint){CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds)};
    //    }
}


@end
