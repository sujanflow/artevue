//
//  LocationsViewController.m
//  Artegrams
//
//  Created by Sujan on 8/10/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "LocationsViewController.h"
#import "CLPlacemark+HNKAdditions.h"
#import "AFNetworking.h"

static NSString *const kHNKDemoSearchResultsCellIdentifier = @"Cell";


@interface LocationsViewController ()

@end

@implementation LocationsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.searchBar.delegate = self;
    [self.searchBar setTintColor:[UIColor whiteColor]];
    self.searchBar.placeholder = @"Find a location";
    [self.searchBar becomeFirstResponder];
    
    self.searchResultTable.delegate = self;
    self.searchResultTable.dataSource = self;
    self.searchQuery = [HNKGooglePlacesAutocompleteQuery sharedQuery];
    self.searchResultTable.hidden = YES;
    
    self.searchResultTable.estimatedRowHeight = 54;
    self.searchResultTable.rowHeight = UITableViewAutomaticDimension;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.searchResults.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kHNKDemoSearchResultsCellIdentifier forIndexPath:indexPath];
    
    HNKGooglePlacesAutocompletePlace *thisPlace = self.searchResults[indexPath.row];
    
    //NSLog(@"Search result %@",thisPlace);
    
    UILabel *placeName = (UILabel*) [cell viewWithTag:1];
    UILabel *address = (UILabel*) [cell viewWithTag:2];
    
    placeName.text=@"";
    address.text=@"";
    
    for (int i =0; i<thisPlace.terms.count; i++) {
        
        HNKGooglePlacesAutocompletePlaceTerm *tempTerm=[HNKGooglePlacesAutocompletePlaceTerm new ];
        tempTerm=[thisPlace.terms objectAtIndex:i];
       
        //NSLog(@"Search result %@",tempTerm);
        
        if(i==0)
            placeName.text =[NSString stringWithFormat:@"%@",tempTerm.value];
        else if(i==1)
            address.text =[NSString stringWithFormat:@"%@",tempTerm.value];
        

        else if(i>1 && i>=thisPlace.terms.count-3)
        {
            if (address.text.length) {
                address.text =[NSString stringWithFormat:@"%@,%@", address.text,tempTerm.value];
            }else{
                address.text =[NSString stringWithFormat:@"%@",tempTerm.value];
            }
        }
        
    }
    
    return cell;
}

#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    HNKGooglePlacesAutocompletePlace *selectedPlace = self.searchResults[indexPath.row];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    UILabel *placeName = (UILabel*) [cell viewWithTag:1];
    UILabel *address = (UILabel*) [cell viewWithTag:2];
    
    NSMutableDictionary *tempLocation=[[NSMutableDictionary alloc] init];
    
    [tempLocation setObject:placeName.text forKey:@"title"];
    [tempLocation setObject:address.text forKey:@"address"];
    [tempLocation setObject:selectedPlace.placeId forKey:@"placeId"];


    
    self.searchBarLeading.constant =0;
    [self.searchBar setShowsCancelButton:NO animated:YES];
    [self.searchBar resignFirstResponder];
    _searchBar.text = @"";

    
    
    
//    [CLPlacemark hnk_placemarkFromGooglePlace: selectedPlace
//                                       apiKey:@"AIzaSyBMgmglinsDo1dJOxYQDNfcwEKO894LyHo"
//     
//                                   completion:^(CLPlacemark *placemark, NSString *addressString, NSError *error) {
//                                       if (placemark) {
//                                          
//                                               
//                                               [self.searchResultTable deselectRowAtIndexPath:indexPath animated:NO];
//                                               
//                                               NSLog(@"Address is:%@",addressString);
//                                               NSLog(@"lat : %f",placemark.location.coordinate.latitude);
//                                               NSLog(@"long : %f",placemark.location.coordinate.longitude);
                                               // NSLog(@"Address is:%@",marker.map);
                                           
//                                           
//                                           [tempLocation setObject:[NSString stringWithFormat:@"%f",placemark.location.coordinate.longitude] forKey:@"lon"];
//                                           [tempLocation setObject:[NSString stringWithFormat:@"%f",placemark.location.coordinate.latitude] forKey:@"lat"];
//                                           
    
//                                           
//                                        }
//                                       
//                                       
//                                   }];
    
    if ([self.delegate respondsToSelector:@selector(didFinishPickingLocation:)]) {
        [self.delegate didFinishPickingLocation:tempLocation];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
   
}

#pragma mark - UISearchBar Delegate

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    self.searchResultTable.hidden = NO;
    self.searchBarLeading.constant =40;
    //self.backButton.hidden = NO;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchText.length > 0)
    {
        [self.searchResultTable setHidden:NO];
        
        [self.searchQuery fetchPlacesForSearchQuery: searchText
                                         completion:^(NSArray *places, NSError *error) {
                                             if (error) {
                                                 NSLog(@"ERROR: %@", error);
                                                 [self handleSearchError:error];
                                             } else {
                                                 self.searchResults = places;
                                                 [self.searchResultTable reloadData];
                                             }
                                         }];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.text = @"";
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    [self.searchResultTable setHidden:YES];
}

- (void)handleSearchError:(NSError *)error
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                   message:error.localizedDescription
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}



- (IBAction)cancleButtonAction:(id)sender {
    
    [self.searchBar resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
