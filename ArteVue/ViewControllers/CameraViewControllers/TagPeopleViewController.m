//
//  TagPeopleViewController.m
//  Artegram
//
//  Created by Sujan on 6/28/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "TagPeopleViewController.h"
#import "Constants.h"
#import "UserAccount.h"
#import "TagPeopleNameView.h"
#import "ServerManager.h"
#import "NSDictionary+NullReplacement.h"

@interface TagPeopleViewController (){

    NSMutableArray *tagPeopleList;
    NSMutableArray *searchResults;
    NSMutableArray *selectedTagList;
    float xPosition;
    float yPosition;

   
}

@end

@implementation TagPeopleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    selectedTagList=[[NSMutableArray alloc] init];
    //tagPeopleList = [[NSMutableArray alloc]init];
    
    //Hide
    
    self.tagPeopleButton.hidden = YES;
    self.dragToMoveLabel.hidden = YES;
    self.searchPeopleTableView.hidden = YES;
    self.searchBar.hidden = YES;
    self.searchPeopleView.hidden = YES;
    
    
    //set image in fullImageView
    double aspectRatio= self.selectedImage.size.width/self.selectedImage.size.height;
    
    self.imageHeightConstraint.constant=[UIScreen mainScreen].bounds.size.width/aspectRatio;
    self.fullImageView.image = self.selectedImage;
   
    NSLog(@"self.imageHeightConstraint.constant %f",self.imageHeightConstraint.constant);
    
    //image tap Recognize
    
    self.tapImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnImage:)];
    self.tapImage.numberOfTapsRequired = 1;
    [self.fullImageView addGestureRecognizer:self.tapImage];
    self.fullImageView.userInteractionEnabled = YES;
    
    
    // drag to move label tap
    
    UITapGestureRecognizer *tapLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTapped)];
    tapLabel.numberOfTapsRequired = 1;
    [self.dragToMoveLabel addGestureRecognizer:tapLabel];
    self.dragToMoveLabel.userInteractionEnabled = YES;
    
    //setting delegate
    
    self.searchBar.delegate = self;
    
    self.searchPeopleTableView.delegate = self;
    self.searchPeopleTableView.dataSource = self;
    self.searchPeopleTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.searchPeopleTableView.frame.size.width, 1)];
    

    
    //setting searchBar
    
    self.searchBar.placeholder = @"Search for a person";
    [self.searchBar setTintColor:[UIColor blackColor]];
   // [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setFont:[UIFont fontWithName:@"Museo-500" size:14]];
    
    
    
   
    if (self.isForEdit) {
        
        NSLog(@"NSMutableArray* taggedUsers %@",self.taggedUsers);
        
        [self showTagedPeople];
        [self makeRequest];
        
    }else
        [self makeRequest];
    
    
    
   // [SDWebImageManager sharedManager].delegate=self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showTagedPeople{

    for (int i = 0; i < self.taggedUsers.count; i++) {
        
   
        //get text size
        UIFont *font = [UIFont fontWithName:@"AzoSans-Regular" size:12];
        NSDictionary *userAttributes = @{NSFontAttributeName: font,
                                         NSForegroundColorAttributeName: [UIColor blackColor]};
        
        NSString *text =[[self.taggedUsers objectAtIndex:i] objectForKey:@"username"];
        const CGSize textSize = [text sizeWithAttributes: userAttributes];
        
        
        NSLog(@"width = %f, height = %f", textSize.width, textSize.height);
        
        float xposition=[[[self.taggedUsers objectAtIndex:i] objectForKey:@"x"] floatValue];
        float yposition=[[[self.taggedUsers objectAtIndex:i] objectForKey:@"y"]floatValue ];
        
        NSLog(@"xposition = %f, yposition = %f", xposition, yposition);

        [self.fullImageView layoutIfNeeded];
        
        TagPeopleNameView *labelview=[[TagPeopleNameView alloc] initWithFrame:CGRectMake(xposition*self.fullImageView.frame.size.width - textSize.width/2, yposition*self.fullImageView.frame.size.width, textSize.width +10, 30)];
        NSLog(@"lebelview %@",labelview);
        
        
        
        labelview.closeButtonWidth.constant = 0;
        labelview.nameLabel.text = [[self.taggedUsers objectAtIndex:i] objectForKey:@"username"];
        
        [self.fullImageView addSubview:labelview];
        
        [labelview.closeButton addTarget:self
                                  action:@selector(closeButtonAction:)
                        forControlEvents:UIControlEventTouchUpInside];
        
        
        
        UITapGestureRecognizer *singleTapOnView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapOnLabelView:)];
        singleTapOnView.numberOfTapsRequired = 1;
        [labelview addGestureRecognizer:singleTapOnView];
        labelview.userInteractionEnabled = YES;
        
        UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(moveViewWithGestureRecognizer:)];
        [labelview addGestureRecognizer:panGestureRecognizer];
        
        
        NSMutableDictionary *tagListDic = [[NSMutableDictionary alloc]initWithDictionary:[self.taggedUsers objectAtIndex:i]];
        // [tagListDic setObject:[[searchResults objectAtIndex:indexPath.row] objectForKey:@"id"] forKey:@"user_id"];
        [tagListDic setObject:[NSNumber numberWithFloat:xposition]  forKey:@"x"];
        [tagListDic setObject:[NSNumber numberWithFloat:yposition] forKey:@"y"];
        // [tagListDic setObject:[[searchResults objectAtIndex:indexPath.row] objectForKey:@"username"] forKey:@"username"];
        
        
        
        [selectedTagList addObject:tagListDic];
        
        labelview.closeButton.tag =[[tagListDic objectForKey:@"user_id"]integerValue];
        labelview.tag =[[tagListDic objectForKey:@"user_id"]integerValue];
        
      //  [tagPeopleList removeObjectAtIndex:i];


    }


}

-(void) makeRequest{

    [[ServerManager sharedManager] getFollowingListOfUser:[UserAccount sharedManager].userId WithCompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
        if ( resultDataArray!=nil) {
            
            NSMutableDictionary *responsedic= [[resultDataArray dictionaryByReplacingNullsWithBlanks] mutableCopy];
            
            NSMutableArray* tempArray = [[NSMutableArray alloc]init];
            
            tempArray =[[NSMutableArray alloc] initWithArray:[responsedic objectForKey:@"data"]];
            
            
            
            tagPeopleList = [[NSMutableArray alloc]init];
            
            
            for (int i = 0; i < tempArray.count; i++) {
                
                
                NSMutableDictionary* tempDic = [[NSMutableDictionary alloc]init];
                [tempDic setObject:[[tempArray objectAtIndex:i]objectForKey:@"user_id"] forKey:@"user_id"];
                [tempDic setObject:[[tempArray objectAtIndex:i] objectForKey:@"username"] forKey:@"username"];
                //[tempDic setObject:[[tempArray objectAtIndex:i] objectForKey:@"first_name"] forKey:@"first_name"];
                [tempDic setObject:[[tempArray objectAtIndex:i] objectForKey:@"profile_picture"] forKey:@"profile_picture"];
                
                [tagPeopleList addObject:tempDic];
                
            }
            
            NSLog(@"tagPeopleList %@",tagPeopleList);
            
            searchResults=[[NSMutableArray alloc] initWithArray:tagPeopleList];
            
            if(selectedTagList.count)
            {
                for (int i=0 ; i<selectedTagList.count; i++) {
                    for (int j=0; j<tagPeopleList.count; j++) {
                        
                        
                        if([[selectedTagList objectAtIndex:i] objectForKey:@"user_id"] == [[tagPeopleList objectAtIndex:j] objectForKey:@"user_id"])
                        {
                            NSMutableDictionary *tagListDic = [selectedTagList objectAtIndex:i];
                            [tagListDic setObject:[[tagPeopleList objectAtIndex:j] objectForKey:@"profile_picture"] forKey:@"profile_picture"];
                            
                            [selectedTagList replaceObjectAtIndex:i withObject:tagListDic];
                            
                            [tagPeopleList removeObject:[tagPeopleList objectAtIndex:j]];
                            
                            break;
                        }
                        
                    }
                }
            }
            
            [self.searchPeopleTableView reloadData];

        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                //[[[UIAlertView alloc] initWithTitle:@"Sorry, unable to fetch following data. Please try again." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                
                
                
            });
            
        }
        
    }];



}

- (IBAction)doneButtonAction:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(didFinishPickingTags:)]) {
        [self.delegate didFinishPickingTags:selectedTagList];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

//image tap action

-(void)tapOnImage:(UITapGestureRecognizer *)recognizer{
    
    //NSLog(@"single Tap on imageview");
    
    
    
    NSLog(@"X position:%f,Y position %f",[self.tapImage locationInView:self.fullImageView].x,[self.tapImage locationInView:self.fullImageView].y);
    
//    tapXPosition = [self.tapImage locationInView:self.fullImageView].x;
//    tapYPosition = [self.tapImage locationInView:self.fullImageView].y;
    
    if ([self.tapImage locationInView:self.fullImageView].y>= self.fullImageView.frame.size.height-20) {
        
       xPosition =[self.tapImage locationInView:self.fullImageView].x/self.fullImageView.frame.size.width;
       yPosition = ([self.tapImage locationInView:self.fullImageView].y-20)/self.fullImageView.frame.size.height;
        
       NSLog(@"yPosition %f",yPosition);
        
    }else{
    
       xPosition =[self.tapImage locationInView:self.fullImageView].x/self.fullImageView.frame.size.width;
       yPosition = [self.tapImage locationInView:self.fullImageView].y/self.fullImageView.frame.size.height;
    
    }
    
    NSLog(@"relative X position:%f,Y position %f",xPosition,yPosition);
    
    self.searchPeopleView.hidden = NO;
    self.searchBar.hidden = NO;
    self.searchPeopleTableView.hidden = NO;
    
    searchResults=[[NSMutableArray alloc] initWithArray:tagPeopleList];
    
    [self.searchPeopleTableView reloadData];
    
    [self.searchBar becomeFirstResponder];

    
}




//label tap action

-(void)labelTapped{
    
    NSLog(@"single Tap on lavel");
    
    self.dragToMoveLabel.textColor = [UIColor blackColor];
    
}



#pragma mark - UISearchBar Delegate

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
    
    [searchBar setShowsCancelButton:YES animated:YES];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSLog(@"text did change");
    NSLog(@"search result %@",searchResults);
    
    [self filterContentForSearchText:searchText
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchBar
                                                     selectedScopeButtonIndex]]];
    
    
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.text = @"";
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    
    [self.searchPeopleTableView setHidden:YES];
    [self.searchBar setHidden:YES];
    self.searchPeopleView.hidden = YES;
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"username contains[c] %@ ",searchText];

    searchResults= [[NSMutableArray alloc] initWithArray:[tagPeopleList filteredArrayUsingPredicate:resultPredicate]];
   
     NSLog(@"search %@",searchResults);
    
    [self.searchPeopleTableView reloadData];
}


- (void)handleSearchError:(NSError *)error
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                   message:error.localizedDescription
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark - UITableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return searchResults.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"tagPeopleCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    UIImageView *profileImage = (UIImageView*) [cell viewWithTag:1];
    UILabel *profileName = (UILabel*) [cell viewWithTag:2];
    //UILabel *userName = (UILabel*) [cell viewWithTag:3];
    
    [profileImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[searchResults objectAtIndex:indexPath.row] objectForKey:@"profile_picture"]]]];
    profileName.text = [[searchResults objectAtIndex:indexPath.row] objectForKey:@"username"];
  //  userName.text = [[searchResults objectAtIndex:indexPath.row] objectForKey:@"first_name"];
    
    
    NSLog(@"profile_picture  %@",[[searchResults objectAtIndex:indexPath.row] objectForKey:@"profile_picture"]);
    
    return cell;
}

#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self.searchPeopleTableView deselectRowAtIndexPath:indexPath animated:NO];
   
    self.searchPeopleView.hidden = YES;
     self.searchBar.hidden = YES;
     self.searchPeopleTableView.hidden = YES;

    [self.searchBar resignFirstResponder];
    
    
    //create label in image view
    
    //get text size
    UIFont *font = [UIFont fontWithName:@"AzoSans-Regular" size:12];
    NSDictionary *userAttributes = @{NSFontAttributeName: font,
                                     NSForegroundColorAttributeName: [UIColor blackColor]};
    
    NSString *text =[[searchResults objectAtIndex:indexPath.row] objectForKey:@"username"];
    const CGSize textSize = [text sizeWithAttributes: userAttributes];
    
    
    NSLog(@"width = %f, height = %f", textSize.width, textSize.height);
    
    TagPeopleNameView *labelview=[[TagPeopleNameView alloc] initWithFrame:CGRectMake(xPosition*self.fullImageView.frame.size.width - (textSize.width + 20)/2, yPosition*self.fullImageView.frame.size.height , textSize.width + 20, 30)];
    NSLog(@"lebelview %@",labelview);
    
//    CGRect rec = labelview.frame;
//    CGRect imgview = self.fullImageView.frame;
//
//    if (rec.origin.x < imgview.origin.x) {
//        
//        rec = CGRectMake(0, yPosition*self.fullImageView.frame.size.height , textSize.width + 20, 30);
//
//    }else if (rec.origin.x + rec.size.width > imgview.origin.x + imgview.size.width ){
//        
//        rec.origin.x = imgview.origin.x + imgview.size.width - rec.size.width;
//    }
    
    labelview.closeButtonWidth.constant = 0;
    labelview.nameLabel.text = [[searchResults objectAtIndex:indexPath.row] objectForKey:@"username"];
    
    [self.fullImageView addSubview:labelview];
    
    [labelview.closeButton addTarget:self
                              action:@selector(closeButtonAction:)
       forControlEvents:UIControlEventTouchUpInside];
    

    
    UITapGestureRecognizer *singleTapOnView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapOnLabelView:)];
    singleTapOnView.numberOfTapsRequired = 1;
    [labelview addGestureRecognizer:singleTapOnView];
    labelview.userInteractionEnabled = YES;
    
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(moveViewWithGestureRecognizer:)];
    [labelview addGestureRecognizer:panGestureRecognizer];
    
    
     NSMutableDictionary *tagListDic = [[NSMutableDictionary alloc]initWithDictionary:[searchResults objectAtIndex:indexPath.row]];
    // [tagListDic setObject:[[searchResults objectAtIndex:indexPath.row] objectForKey:@"id"] forKey:@"user_id"];
     [tagListDic setObject:[NSNumber numberWithFloat:xPosition]  forKey:@"x"];
     [tagListDic setObject:[NSNumber numberWithFloat:yPosition] forKey:@"y"];
   // [tagListDic setObject:[[searchResults objectAtIndex:indexPath.row] objectForKey:@"username"] forKey:@"username"];
    
    
    
     [selectedTagList addObject:tagListDic];
    
     labelview.closeButton.tag =[[tagListDic objectForKey:@"user_id"]integerValue];
     labelview.tag =[[tagListDic objectForKey:@"user_id"]integerValue];
    
     [tagPeopleList removeObjectAtIndex:indexPath.row];
   

    NSLog(@"tagList %@",selectedTagList);

}
//TagPeopleNameView

-(void) singleTapOnLabelView:(UITapGestureRecognizer *)recognizer{

    
    NSLog(@"tap on view");
  
    
    TagPeopleNameView* levelView= (TagPeopleNameView*)[recognizer view];
    
    
    //get text size
    UIFont *font = [UIFont fontWithName:@"AzoSans-Regular" size:12];
    NSDictionary *userAttributes = @{NSFontAttributeName: font,
                                     NSForegroundColorAttributeName: [UIColor blackColor]};
    
    NSString *text = levelView.nameLabel.text;
    const CGSize textSize = [text sizeWithAttributes: userAttributes];

    
    CGFloat newWidth = textSize.width + 20 ;

    levelView.closeButtonWidth.constant = 0;

    
    if (levelView.frame.size.width == newWidth) {
        
        newWidth = textSize.width + 40;
        levelView.closeButtonWidth.constant = 20;
    }
    
    levelView.frame = CGRectMake(levelView.frame.origin.x, levelView.frame.origin.y, newWidth, 30);
    
  
    NSLog(@"labelView frame width after tap %f",levelView.layer.bounds.size.width);
    

    
    //[selectedTagList removeObjectAtIndex:[recognizer view].tag];
}

-(void)closeButtonAction:(UIButton*)sender{

     NSLog(@"sender.tag %ld",(long)sender.tag);
    
   // [[sender superview] removeFromSuperview];
     NSArray *idsArray=[selectedTagList valueForKey:@"user_id"];
    
     NSInteger atIndex=[idsArray indexOfObject:[NSNumber numberWithInteger:sender.tag]];
    
    // NSLog(@"after delete anIndex %ld",(long)atIndex);
    
     [tagPeopleList addObject:[selectedTagList objectAtIndex:atIndex]];
    
     [selectedTagList removeObjectAtIndex:atIndex];

//    NSLog(@"after delete tagList %@",selectedTagList);
}

-(void)moveViewWithGestureRecognizer:(UIPanGestureRecognizer *)panGestureRecognizer{

    if(panGestureRecognizer.state == UIGestureRecognizerStateBegan || panGestureRecognizer.state == UIGestureRecognizerStateChanged || panGestureRecognizer.state == UIGestureRecognizerStateEnded)
    {
        CGRect rec = panGestureRecognizer.view.frame;
        CGRect imgview = self.fullImageView.frame;
        
        if((rec.origin.x >= imgview.origin.x && (rec.origin.x + rec.size.width <= imgview.origin.x + imgview.size.width)) || (rec.origin.y >= imgview.origin.y && (rec.origin.y + rec.size.height <= imgview.origin.y + imgview.size.height)))
        {
            CGPoint translation = [panGestureRecognizer translationInView:self.fullImageView];
            panGestureRecognizer.view.center = CGPointMake(panGestureRecognizer.view.center.x + translation.x, panGestureRecognizer.view.center.y + translation.y);
            
            rec = panGestureRecognizer.view.frame;
            
            if( rec.origin.x < imgview.origin.x )
                rec.origin.x = imgview.origin.x;
            
            if( rec.origin.y <= imgview.origin.y-66)
                
                rec.origin.y = imgview.origin.y-66 ;
            
            NSLog(@"orizin y: %f ", rec.origin.y);
            
            if( rec.origin.x + rec.size.width > imgview.origin.x + imgview.size.width )
                rec.origin.x = imgview.origin.x + imgview.size.width - rec.size.width;
            
            
            
            if( rec.origin.y + rec.size.height > imgview.origin.y - 66 + imgview.size.height )
                rec.origin.y = imgview.origin.y- 66 + imgview.size.height - rec.size.height ;
            
            panGestureRecognizer.view.frame = rec;
            
            [panGestureRecognizer setTranslation:CGPointZero inView:self.fullImageView];
           
        }
        
        CGPoint finalPoint = CGPointMake(panGestureRecognizer.view.center.x , panGestureRecognizer.view.center.y );
        
        finalPoint.x = MIN(MAX(finalPoint.x, 0), self.fullImageView.bounds.size.width);
        finalPoint.y = MIN(MAX(finalPoint.y, 0), self.fullImageView.bounds.size.height);
        
        
        NSLog(@"new x  point: %f ", finalPoint.x);
        
        NSLog(@"new x  point %f", finalPoint.y);
        
        xPosition = finalPoint.x/self.fullImageView.frame.size.width;
        yPosition = finalPoint.y/self.fullImageView.frame.size.height;
        
        
        NSLog(@" final relative X position:%f,Y position %f",xPosition,yPosition);
        
        
        
        NSArray *idsArray=[selectedTagList valueForKey:@"user_id"];
        
        NSInteger atIndex=[idsArray indexOfObject:[NSNumber numberWithInteger:panGestureRecognizer.view.tag]];

        NSMutableDictionary *tagListDic = [[NSMutableDictionary alloc] initWithDictionary:[selectedTagList objectAtIndex:atIndex]];
        [tagListDic setObject:[NSNumber numberWithFloat:xPosition]  forKey:@"x"];
        [tagListDic setObject:[NSNumber numberWithFloat:yPosition] forKey:@"y"];
        
        
        [selectedTagList replaceObjectAtIndex:atIndex withObject:tagListDic];
        
       // NSLog(@"after move selectedTagList %@",selectedTagList);
        
    }


    NSLog(@"final tagList %@",selectedTagList);

    
}

//-(UIImage *)imageManager:(SDWebImageManager *)imageManager transformDownloadedImage:(UIImage *)image withURL:(NSURL *)imageURL
//{
//    // NSLog(@"imageURL : %@",imageURL);
//    UIImage *resizedImage;
//    //  return resizedImage  = [image resizedImage:CGSizeMake(512, 512) interpolationQuality:kCGInterpolationDefault];
//    
//    float MIN_UPLOAD_RESOLUTION=756*756;
//    float factor;
//    float resol = image.size.height*image.size.width;
//    if (resol >MIN_UPLOAD_RESOLUTION){
//        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
//        resizedImage = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
//        return resizedImage;
//        
//    }
//    else
//        return image;
//}
//
//- (UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
//    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
//    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
//    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return scaledImage;
//}


@end
