//
//  TagPeopleViewController.h
//  Artegram
//
//  Created by Sujan on 6/28/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"

@protocol TagPeopleControllerDelegate;

@interface TagPeopleViewController : UIViewController<UISearchBarDelegate, UITableViewDataSource,UITableViewDelegate,SDWebImageManagerDelegate>

@property (nonatomic, assign) id delegate;

@property (weak, nonatomic) IBOutlet UIImageView *fullImageView;
@property (weak, nonatomic) UIImage *selectedImage;
@property UITapGestureRecognizer *tapImage;

@property (weak, nonatomic) IBOutlet UIButton *tagPeopleButton;

@property (weak, nonatomic) IBOutlet UILabel *tapPhotoLabel;

@property (weak, nonatomic) IBOutlet UILabel *dragToMoveLabel;

@property (weak, nonatomic) IBOutlet UIView *searchPeopleView;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (weak, nonatomic) IBOutlet UITableView *searchPeopleTableView;

@property( nonatomic, strong) NSMutableArray* taggedUsers;

@property BOOL isForEdit;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageHeightConstraint;


@end

@protocol TagPeopleControllerDelegate
- (void)didFinishPickingTags:(NSMutableArray *)tags;

@end
