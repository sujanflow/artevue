//
//  SharePhotoViewController.h
//  Artegram
//
//  Created by Sujan on 6/26/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UIImage+Resize.h"

#import "TagPeopleViewController.h"
#import "LocationsViewController.h"

#import <TesseractOCR/TesseractOCR.h>
//#import "YCameraViewController.h"
#import "MMCameraPickerController.h"

#import <Social/Social.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <QuartzCore/QuartzCore.h>

#import "AKPickerView.h"
#import "ArteVue-Swift.h"
#import "TOCropViewController.h"


@interface SharePhotoViewController : UIViewController<UITextViewDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UIAlertViewDelegate,UIDocumentInteractionControllerDelegate,TagPeopleControllerDelegate,AKPickerViewDataSource, AKPickerViewDelegate,MMCameraDelegate,G8TesseractDelegate,LocationPostDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate,TOCropViewControllerDelegate,SDWebImageManagerDelegate>


@property (strong) UIImage* selectedImageForShare;
@property (weak, nonatomic) IBOutlet UIImageView *selectedPhoto;

@property (weak, nonatomic) IBOutlet SearchTextField *artistTextfield;
@property (weak, nonatomic) IBOutlet UITextView *captionTextView;
@property (weak, nonatomic) IBOutlet UILabel *captionHintLabel;

@property (weak, nonatomic) IBOutlet UILabel *taggedPeopleLabel;

@property (weak, nonatomic) IBOutlet UIButton *addLocationButton;
@property (weak, nonatomic) IBOutlet UILabel *locationName;
@property (weak, nonatomic) IBOutlet UILabel *locationAddress;
@property (weak, nonatomic) IBOutlet UIButton *locationCloseButton;

@property (weak, nonatomic) IBOutlet UIView *locationDetailsView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationAspectRatio;


@property (weak, nonatomic) IBOutlet UIView *hashTagView;
@property (weak, nonatomic) IBOutlet UITextView *hashTagTextView;

@property (weak, nonatomic) IBOutlet UISwitch *saveToMyGalleryButton;
@property (weak, nonatomic) IBOutlet UILabel *saveToGalleryLabel;

@property (weak, nonatomic) IBOutlet UISwitch *publicPostToggle;
@property (weak, nonatomic) IBOutlet UILabel *publicPostLabel;

@property (weak, nonatomic) IBOutlet UISwitch *buyButtonSwitch;
@property (weak, nonatomic) IBOutlet UILabel *buyButtonLabel;

@property (weak, nonatomic) IBOutlet UITextField *priceTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *priceViewHeightConstraint;


@property (weak, nonatomic) IBOutlet UICollectionView *socialMediaCollectionView;

@property (nonatomic, retain) UIDocumentInteractionController *documentController;

@property (weak, nonatomic) IBOutlet AKPickerView *hashTagPicker;
@property (weak, nonatomic) IBOutlet UIButton *shareLabelButton;

@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@property BOOL isForEdit;
@property BOOL isForShare;
@property (strong,nonatomic) NSMutableDictionary *postDetails;

@property (weak, nonatomic) IBOutlet UIButton *tagPeopleButton;
@property (weak, nonatomic) IBOutlet UIView *captionDetailsView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *shareBoxLayoutConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *shareBoxTopInactiveConstraint;

@property (weak, nonatomic) IBOutlet UILabel *descriptionCounter;
@property (weak, nonatomic) IBOutlet UIButton *arteScanButton;

@property (weak, nonatomic) IBOutlet UIView *autoFillSuggestionView;
@property (weak, nonatomic) IBOutlet UITableView *autoFillTableView;


@end
