//
//  TagPeopleViewController.m
//  Artegram
//
//  Created by Sujan on 6/28/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "GetUserHashtagViewController.h"
#import "Constants.h"
#import "UserAccount.h"
#import "ServerManager.h"

@interface GetUserHashtagViewController (){

    NSMutableArray *hashTagSearchResult;
    NSMutableArray *peopleSearchResult;
    int pageIndex;
   
}

@end

@implementation GetUserHashtagViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    pageIndex=1;
    peopleSearchResult = [[NSMutableArray alloc]init];
    hashTagSearchResult = [[NSMutableArray alloc]init];
    
    self.searchBar.hidden = NO;
    self.searchPeopleView.hidden = NO;
    
    //setting searchBar
    
    if(self.isSearchUser)
    {
        self.searchBar.placeholder = @"Search for User";
        
    }
    else
    {
        self.searchBar.placeholder = @"Search for Hashtags";
        
    }
    
    [self.searchBar setTintColor:[UIColor blackColor]];
   
    [self.searchBar becomeFirstResponder];
    self.searchBar.delegate = self;
    
    NSLog(@"initialText %@",self.initialText);
    
    //self.searchBar.text=self.initialText;
    
    if(self.isSearchUser)
    {
        [self searchUser];
    }
    else
    {
        [self searchTags];
    }
    //setting delegate
    
    
    self.searchPeopleTableView.delegate = self;
    self.searchPeopleTableView.dataSource = self;
    self.searchPeopleTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.searchPeopleTableView.frame.size.width, 1)];
  
    
  //  [SDWebImageManager sharedManager].delegate=self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UISearchBar Delegate

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
    [searchBar setShowsCancelButton:YES animated:YES];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
//    [self filterContentForSearchText:searchText
//                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
//                                      objectAtIndex:[self.searchBar
//                                                     selectedScopeButtonIndex]]];
//
    
    NSLog(@"searchText %@",searchText);
    if(self.isSearchUser)
    {
        [self searchUser];
    }
    else
    {
        [self searchTags];
    }
    
   //
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    
    //dismiss view
    
    NSMutableDictionary *tempDic=[[NSMutableDictionary alloc] init];
    if(self.isSearchUser)
    {
        [tempDic setObject:self.searchBar.text forKey:@"username"];
        
        if ([self.delegate respondsToSelector:@selector(didFinishPickingItem:forUser:)]) {
            [self.delegate didFinishPickingItem:tempDic forUser:YES];
        }
    }
    else
    {
        [tempDic setObject:[NSString stringWithFormat:@"#%@",self.searchBar.text] forKey:@"hashtag"];

        if ([self.delegate respondsToSelector:@selector(didFinishPickingItem:forUser:)]) {
            [self.delegate didFinishPickingItem:nil forUser:NO];
        }

    }
    
    
    searchBar.text = @"";
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)handleSearchError:(NSError *)error
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                   message:error.localizedDescription
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

//- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
//{
//    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"username contains[c] %@ ",searchText];
//
//    searchResults= [[NSMutableArray alloc] initWithArray:[tagPeopleList filteredArrayUsingPredicate:resultPredicate]];
//   
//     NSLog(@"search %@",searchResults);
//    
//    [self.searchPeopleTableView reloadData];
//}

-(void) searchUser
{
    
    [[ServerManager sharedManager] searchUserByString:self.searchBar.text forPage:pageIndex withcompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
        
        if ( resultDataArray!=nil) {
            
            peopleSearchResult = [[NSMutableArray alloc]init];
            peopleSearchResult = [resultDataArray objectForKey:@"data"];
            

        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
            });
        }
    }];
    
}


-(void) searchTags
{
    
    [[ServerManager sharedManager] searchHashtagByString:self.searchBar.text withcompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
        
        if ( resultDataArray!=nil) {
            
            hashTagSearchResult = [[NSMutableArray alloc]init];
            hashTagSearchResult = [resultDataArray objectForKey:@"data"];
            
            
            if (hashTagSearchResult.count && [self.searchBar.text isEqualToString:[[[hashTagSearchResult objectAtIndex:0] objectForKey:@"hashtag"] substringFromIndex:1]]) {
                NSLog(@"do nothing");
            }
            else
            {
                NSMutableDictionary *tempDic=[[NSMutableDictionary alloc] init];
                [tempDic setObject:[NSString stringWithFormat:@"#%@",self.searchBar.text] forKey:@"hashtag"];
                [tempDic setObject:@"0" forKey:@"user_count"];
                [hashTagSearchResult insertObject:tempDic atIndex:0];
            }
            
            
            
            [self.searchPeopleTableView reloadData];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
            });
        }
        
        
        
    }];

    
}

#pragma mark - UITableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.isSearchUser)
    {
        return peopleSearchResult.count;
    }
    else
    {
        return hashTagSearchResult.count;
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if(self.isSearchUser)
    {
        static NSString *CellIdentifier = @"tagPeopleCell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        
        UIImageView *profileImage = (UIImageView*) [cell viewWithTag:1];
        UILabel *profileName = (UILabel*) [cell viewWithTag:2];
        UILabel *userName = (UILabel*) [cell viewWithTag:3];
        
        [profileImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL_FOR_IMAGE,[[peopleSearchResult objectAtIndex:indexPath.row] objectForKey:@"profile_picture"]]]];
        profileName.text = [[peopleSearchResult objectAtIndex:indexPath.row] objectForKey:@"username"];
        userName.text = [[peopleSearchResult objectAtIndex:indexPath.row] objectForKey:@"first_name"];
        
        return cell;

    }
    else
    {
        static NSString *CellIdentifier = @"hashTagCell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        
        UILabel *profileName = (UILabel*) [cell viewWithTag:2];
        UILabel *userName = (UILabel*) [cell viewWithTag:3];
        
       
        
        profileName.text = [[hashTagSearchResult objectAtIndex:indexPath.row] objectForKey:@"hashtag"];
        userName.text = [NSString stringWithFormat:@"%d posts",[[[hashTagSearchResult objectAtIndex:indexPath.row] objectForKey:@"use_count"]intValue]];
        
        return cell;

    }
    

}

#pragma mark - UITableView Delegate


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self.searchPeopleTableView deselectRowAtIndexPath:indexPath animated:NO];
   

    [self.searchBar resignFirstResponder];
    
    if(self.isSearchUser)
    {
        if ([self.delegate respondsToSelector:@selector(didFinishPickingItem:forUser:)]) {
            [self.delegate didFinishPickingItem:[peopleSearchResult objectAtIndex:indexPath.row] forUser:YES];
        }
    }
    else
    {
        if ([self.delegate respondsToSelector:@selector(didFinishPickingItem:forUser:)]) {
            [self.delegate didFinishPickingItem:[hashTagSearchResult objectAtIndex:indexPath.row] forUser:NO];
        }
    }
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //create label in image view
    
   
}

//-(UIImage *)imageManager:(SDWebImageManager *)imageManager transformDownloadedImage:(UIImage *)image withURL:(NSURL *)imageURL
//{
//    // NSLog(@"imageURL : %@",imageURL);
//    UIImage *resizedImage;
//    //  return resizedImage  = [image resizedImage:CGSizeMake(512, 512) interpolationQuality:kCGInterpolationDefault];
//    
//    float MIN_UPLOAD_RESOLUTION=756*756;
//    float factor;
//    float resol = image.size.height*image.size.width;
//    if (resol >MIN_UPLOAD_RESOLUTION){
//        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
//        resizedImage = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
//        return resizedImage;
//        
//    }
//    else
//        return image;
//}
//
//- (UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
//    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
//    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
//    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return scaledImage;
//}
@end
