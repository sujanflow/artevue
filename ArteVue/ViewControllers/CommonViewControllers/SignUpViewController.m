//
//  SignUpViewController.m
//  Artegram
//
//  Created by Sujan on 6/6/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "SignUpViewController.h"
#import "EmailSignUpViewController.h"
#import "LogInViewController.h"
#import "TabBarViewController.h"
#import "GetUserNameViewController.h"
#import "Constants.h"
#import "UserAccount.h"
#import "ServerManager.h"

@interface SignUpViewController ()
{
    CGRect popupGoToFrame,popupGoFromFrame;
    UIView *termConditionView;
    
    IBOutlet UIWebView *webview;
    OAConsumer* consumer;
    OAToken* requestToken;
    OAToken* accessToken;
    NSMutableData *receivedData;
    
    NSString *client_id;
    NSString *secret;
    NSString *callback;

}

@property (nonatomic, retain) NSString *isLogin;
@property (assign, nonatomic) Boolean isReader;

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    label.text = @"Fork me on GitHub! (https://github.com/mattt/TTTAttributedLabel/)"; // Repository URL will be automatically detected and linked
//
//    client_id = @"1c3e33123972472b8e9b6cb62d3e5915";
//    secret = @"39a730a77eb34bb5a08d86fd72bd5468";
//    callback = @"http://54.93.178.244/"; // sample call back URL
//    
   
    client_id = @"600a132590df40f59cbc8dfaae0a0084";
    secret = @"6fc1abd1fd89441eaf74734f0616211b";
    callback = [NSString stringWithFormat:@"%@/",SERVER_BASE_API_URL]; // sample call back URL
    

    self.disclaimerLabel.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    self.disclaimerLabel.delegate = self;
    NSRange range = [self.disclaimerLabel.text rangeOfString:@"terms"];
    
    NSLog(@"%@",self.disclaimerLabel);
    
    [self.disclaimerLabel addLinkToURL:[NSURL URLWithString:@"terms"] withRange:range]; // Embedding a custom link in a substring
    
    self.loginButton.readPermissions =
    @[@"public_profile", @"email", @"user_friends"];
    self.loginButton.delegate=self;
}


- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithTextCheckingResult:(NSTextCheckingResult *)result {
    //do whatever you need
    NSLog(@"result %@",result.URL);
    if ([result.URL isEqual:[NSURL URLWithString:@"terms"]]) {
        //[self openPopupAnimation];
        
       
        [self createTermsWebview:@"Terms of use"];
        
        
    }
    
}

- (IBAction)faceBookButtonAction:(id)sender {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile",@"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             // NSLog(@"Logged in");
             NSLog(@"fetched user:%@", result);
             
             NSLog(@"User name: %@",[FBSDKProfile currentProfile].name);
             NSLog(@"User ID: %@",[FBSDKProfile currentProfile].userID);
             
             [self fetchUserInfo];
             // NSLog(@"email %@",result[@"email"]);
         }
     }];

    
}


- (IBAction)signUpEmailButtonAction:(id)sender {
    
    EmailSignUpViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"EmailSignUpViewController"];
    
    [self.navigationController pushViewController:controller animated:YES];
    
    
}

- (IBAction)signInButtonAction:(id)sender {
    
//    LogInViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"LogInViewController"];
//    
//    [self.navigationController pushViewController:controller animated:YES];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void) createTermsWebview:(NSString*) title
{
   
    
    popupGoToFrame = CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height);
    popupGoFromFrame = CGRectMake(0, SCREEN_SIZE.height, SCREEN_SIZE.width, SCREEN_SIZE.height);
   
    termConditionView = [[UIView alloc] initWithFrame:popupGoFromFrame];
    [termConditionView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
//    
    [self.view addSubview:termConditionView];
//    
    
    UIView *bgImageview = [[UIView alloc] initWithFrame:CGRectMake(15, IPHONE_5?92:48, SCREEN_SIZE.width-30,  SCREEN_SIZE.height-200)];
//    
   //[bgImageview setImage:[UIImage imageNamed:@"terms_use_popup.png"]];
//    
    bgImageview.layer.cornerRadius=3;
    bgImageview.layer.borderColor=[UIColor blackColor].CGColor;
    bgImageview.backgroundColor=[UIColor whiteColor];
    
    [termConditionView addSubview:bgImageview];
    
    webview = [[UIWebView alloc] initWithFrame:CGRectMake(2, 40, bgImageview.frame.size.width-4
                                                                      , bgImageview.frame.size.height-45)];
    
    [webview setUserInteractionEnabled:YES];
    [webview setDelegate:self];
    webview.scalesPageToFit = YES;
    
    [webview setBackgroundColor:[UIColor clearColor]];
    [bgImageview addSubview:webview];
    
    if([title isEqualToString:@"Sign up"])
    {
        NSString *url = [NSString stringWithFormat:@"https://api.instagram.com/oauth/authorize/?client_id=%@&redirect_uri=%@&response_type=code",client_id,callback];
        
        [webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    }else
    {
        NSError *error = nil;
        NSString *html = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"artevue_t&c"ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
        [webview loadHTMLString:html baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
        
        NSLog(@"error %@",error);
    }
   
    
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(webview.frame.origin.x+15, -5 ,150, 50)];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    titleLabel.numberOfLines = 0;
    [titleLabel setText:title];
    [titleLabel setFont:[UIFont fontWithName:@"AzoSans-Regular" size:18.0f]];
    [bgImageview addSubview:titleLabel];
    
    
    UIButton *cancleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [cancleButton setBackgroundColor:[UIColor blackColor]];
    [cancleButton setImage:[UIImage imageNamed:@"blackCloseButton"] forState:UIControlStateNormal];
    [cancleButton setFrame:CGRectMake(bgImageview.frame.size.width - 60,-5, 50, 50)];
    [cancleButton addTarget:self
                     action:@selector(cancleAction:)
           forControlEvents:UIControlEventTouchUpInside];
    [bgImageview addSubview:cancleButton];
    
     [self openPopupAnimation];
}




-(IBAction)cancleAction:(id)sender{
    
    //self.webViewContainer.hidden=YES;
   [self closePopupAnimation];
}

-(void)openPopupAnimation{
    [UIView animateWithDuration:0.6f animations:^{
        [termConditionView setFrame:popupGoToFrame];
    } completion:^(BOOL finished) {
    }];
    
}

-(void)closePopupAnimation{
    [UIView animateWithDuration:0.6f animations:^{
        [termConditionView setFrame:popupGoFromFrame];
    } completion:^(BOOL finished) {
    }];
}


-(void)fetchUserInfo
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        NSLog(@"Token is available : %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email, birthday, bio ,location ,friends ,hometown , friendlists"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error)
             {
                 NSLog(@"resultis:%@",result);
                 
                 //[KSToastView ks_showToast:@"Logged in with Facebook" duration:2.0f];
                 //[self socialApiCalledForFacebook:result];
                 
                 GetUserNameViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"GetUserNameViewController"];
                 controller.faceBookResult= result;
                 [self.navigationController pushViewController:controller animated:YES];
                 
                 
                 
                 //
             }
             else
             {
                 NSLog(@"Error %@",error);
             }
         }];
        
    }
    
}


-(void) socialApiCalledForFacebook: (id) result
{
    
    
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    [postData setObject:[result objectForKey:@"email"] forKey:@"email"];
    [postData setObject:[result objectForKey:@"first_name"] forKey:@"username"];
    [postData setObject:[result objectForKey:@"first_name"] forKey:@"first_name"];
    [postData setObject:[result objectForKey:@"last_name"] forKey:@"last_name"];
    [postData setObject:@"5" forKey:@"user_type_id"];
    [postData setObject:@"facebook" forKey:@"social_provider"];
    [postData setObject:[result objectForKey:@"id"] forKey:@"social_id"];
    [postData setObject:ACCESS_KEY forKey:@"access_key"];
    
    NSLog(@"postData %@",postData);
    [apiLoginManager POST:[NSString stringWithFormat:@"%@/users/api-facebook-signup",SERVER_BASE_API_URL] parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSLog(@"signup result %@",responseObject);
        if([[responseObject objectForKey:@"success"] intValue]==1)
        {
           // [self followArteVueWithId:[[[responseObject objectForKey:@"UserDetail"]objectForKey:@"id"] intValue]];
            
            [UserAccount sharedManager].userId=[[[responseObject objectForKey:@"UserDetail"]objectForKey:@"id"] intValue];
            [UserAccount sharedManager].userName=[[responseObject objectForKey:@"UserDetail"] objectForKey:@"first_name"] ;
            [UserAccount sharedManager].userFirstName=[[responseObject objectForKey:@"UserDetail"] objectForKey:@"last_name"] ;
              [UserAccount sharedManager].userType=[[responseObject objectForKey:@"UserDetail"] objectForKey:@"user_type"];
            //  [UserAccount sharedManager].userLastName=[[responseObject objectForKey:@"UserDetail"] objectForKey:@"last_name"];
            //   [UserAccount sharedManager].userImageName=[[responseObject objectForKey:@"UserDetail"] objectForKey:@"profile_picture"];
            
            [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"socialLogin"];
            
            if([[[responseObject objectForKey:@"UserDetail"] objectForKey:@"profile_picture"] isEqual:[NSNull null]])
            {
                [UserAccount sharedManager].userImageName=@"img/profile-holder.png";
                
            }
            else if ([[[responseObject objectForKey:@"UserDetail"] objectForKey:@"profile_picture"] isEqualToString:@""] || ![[responseObject objectForKey:@"UserDetail"] objectForKey:@"profile_picture"])
            {
                [UserAccount sharedManager].userImageName=@"img/profile-holder.png";
                
            }
            else
                
                [UserAccount sharedManager].userImageName=[[responseObject objectForKey:@"UserDetail"] objectForKey:@"profile_picture"];
            
            [UserAccount sharedManager].isSavetoAlbum=0;
            [UserAccount sharedManager].isAccountPrivate=0;
            [UserAccount sharedManager].isNotificationEnabled=1;
            
            
            TabBarViewController *viewController=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
            
            [self.navigationController pushViewController:viewController animated:NO];
        }
        else{
           
            
            UIAlertController * alertController=   [UIAlertController
                                                    alertControllerWithTitle:nil
                                                    message:@"Please try again"
                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           
                                                       }];
            
            
            [alertController addAction:ok];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
  
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error %@",error);
        
   
        UIAlertController * alertController=   [UIAlertController
                                                alertControllerWithTitle:nil
                                                message:@"Server error"
                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       
                                                       
                                                   }];
        
        
        [alertController addAction:ok];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }];
    
}

-(void) socialApiCalledForInstagram: (NSDictionary*) result
{
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    [postData setObject:[[result objectForKey:@"user"] objectForKey:@"username"]forKey:@"username"];
     [postData setObject:[[result objectForKey:@"user"] objectForKey:@"full_name"]forKey:@"first_name"];
     [postData setObject:[[result objectForKey:@"user"] objectForKey:@"full_name"]forKey:@"last_name"];
     [postData setObject:[[result objectForKey:@"user"] objectForKey:@"id"]forKey:@"social_id"];
     [postData setObject:@"5" forKey:@"user_type_id"];
    [postData setObject:@"instagram" forKey:@"social_provider"];
    
    [postData setObject:ACCESS_KEY forKey:@"access_key"];
    
    [apiLoginManager POST:[NSString stringWithFormat:@"%@/users/api-instagram-signup",SERVER_BASE_API_URL] parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSLog(@"signup result %@",responseObject);
        
        
        if([[responseObject objectForKey:@"success"] intValue]==1)
        {
        
           // [self followArteVueWithId:[[[responseObject objectForKey:@"UserDetail"]objectForKey:@"id"] intValue]];
            
            [UserAccount sharedManager].userId=[[responseObject objectForKey:@"user_id"] intValue];
            [UserAccount sharedManager].userName=[[result objectForKey:@"user"]objectForKey:@"username"];
            [UserAccount sharedManager].userFirstName=[[result objectForKey:@"user"] objectForKey:@"full_name"];
            //  [UserAccount sharedManager].userType=[[responseObject objectForKey:@"UserDetail"] objectForKey:@"user_type"];
            //  [UserAccount sharedManager].userLastName=[[responseObject objectForKey:@"UserDetail"] objectForKey:@"last_name"];
            //   [UserAccount sharedManager].userImageName=[[responseObject objectForKey:@"UserDetail"] objectForKey:@"profile_picture"];
            
            [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"socialLogin"];
            
            if([[[responseObject objectForKey:@"UserDetail"] objectForKey:@"profile_picture"] isEqual:[NSNull null]])
            {
                [UserAccount sharedManager].userImageName=@"img/profile-holder.png";
                
            }
            else if ([[[responseObject objectForKey:@"UserDetail"] objectForKey:@"profile_picture"] isEqualToString:@""] || ![[responseObject objectForKey:@"UserDetail"] objectForKey:@"profile_picture"])
            {
                [UserAccount sharedManager].userImageName=@"img/profile-holder.png";
                
            }
            else
                
                [UserAccount sharedManager].userImageName=[[responseObject objectForKey:@"UserDetail"] objectForKey:@"profile_picture"];
            
            [UserAccount sharedManager].isSavetoAlbum=0;
            [UserAccount sharedManager].isAccountPrivate=0;
            [UserAccount sharedManager].isNotificationEnabled=1;
            
            
            TabBarViewController *viewController=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
            
            [self.navigationController pushViewController:viewController animated:NO];
            
        }else
        {
            if([[responseObject objectForKey:@"message"] isEqualToString:@"This username is already taken. Someone else is using your instagram username to use artevue, so please go and create a new Instagram account"])
            {
                
                UIAlertController * alertController=   [UIAlertController
                                                        alertControllerWithTitle:nil
                                                        message:@"User exist, please try login"
                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action) {
                                                               
                                                               
                                                           }];
                
                
                [alertController addAction:ok];
                
                [self presentViewController:alertController animated:YES completion:nil];
                
            }else
            {
                
                UIAlertController * alertController=   [UIAlertController
                                                        alertControllerWithTitle:nil
                                                        message:@"Something went wrong, Please try again"
                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action) {
                                                               
                                                               
                                                           }];
                
                
                [alertController addAction:ok];
                
                [self presentViewController:alertController animated:YES completion:nil];
            }
           
            

        }

        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error %@",error);
        
    }];
    
}




- (IBAction)instagramButtonAction:(id)sender {
    
    NSLog(@"instagram called %@",webview);
    [self createTermsWebview:@"Sign up"];
    
   
}

//- (void)followArteVueWithId:(int)userId {
//    
//    
//    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
//    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
//    
//    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
//    [postData setObject:ACCESS_KEY forKey:@"access_key"];
//    [postData setObject:[NSNumber numberWithInt:33] forKey:@"user_id"];
//    [postData setObject:[NSNumber numberWithInt:userId] forKey:@"current_user_id"];
//    
//    NSLog(@"postData %@",postData);
//    
//    [apiLoginManager POST:[NSString stringWithFormat:@"%@/users/apiSwapFollowing",SERVER_BASE_API_URL] parameters:postData
//                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                      
//                      NSLog(@"Response: %@", responseObject);
//                      
//                      NSString * successMsg = [responseObject objectForKey:@"success"];
//                      
//                      if ([successMsg integerValue] == 1 ) {
//                          
//                          
//                          NSLog(@"success msg %@",[responseObject objectForKey:@"success"]);
//                          
//                          
//                      }
//                      
//                  }
//     
//                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                      
//                      NSLog(@"Error: %@", error);
//                      
//                      
//                      NSLog(@"%@",operation.responseString);
//                      
//                  }];
//    
//    
//}


- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType
{
    //    [indicator startAnimating];
    if ([[[request URL] host] isEqualToString:@"52.15.139.172"]) {
        
        // Extract oauth_verifier from URL query
        NSString* verifier = nil;
        NSArray* urlParams = [[[request URL] query] componentsSeparatedByString:@"&"];
        for (NSString* param in urlParams) {
            NSArray* keyValue = [param componentsSeparatedByString:@"="];
            NSString* key = [keyValue objectAtIndex:0];
            if ([key isEqualToString:@"code"]) {
                verifier = [keyValue objectAtIndex:1];
                break;
            }
        }
        
        if (verifier) {
            
            NSString *data = [NSString stringWithFormat:@"client_id=%@&client_secret=%@&grant_type=authorization_code&redirect_uri=%@&code=%@",client_id,secret,callback,verifier];
            
            NSString *url = [NSString stringWithFormat:@"https://api.instagram.com/oauth/access_token"];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
            [request setHTTPMethod:@"POST"];
            [request setHTTPBody:[data dataUsingEncoding:NSUTF8StringEncoding]];
        
            NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:request delegate:self];
            receivedData = [[NSMutableData alloc] init];
        } else {
            // ERROR!
        }
        
        
        
        return NO;
    }
    return YES;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    // [indicator stopAnimating];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data

{
    [receivedData appendData:data];
    
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{

    
    UIAlertController * alertController=   [UIAlertController
                                            alertControllerWithTitle:@"Error"
                                            message:[NSString stringWithFormat:@"%@", error]
                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   
                                                   
                                               }];
    
    
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
   // NSString *response = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    
    NSError *jsonError;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:receivedData
                                                         options:NSJSONReadingMutableContainers
                                                           error:&jsonError];
    
    //  WebServiceSocket *dconnection = [[WebServiceSocket alloc] init];
    //   dconnection.delegate = self;
    
    NSLog(@"json %@",json);
    
    NSString *pdata = [NSString stringWithFormat:@"type=3&token=%@&secret=123&login=%@", [json objectForKey:@"access_token"], self.isLogin];
    //  NSString *pdata = [NSString stringWithFormat:@"type=3&token=%@&secret=123&login=%@",[tokenData accessToken.secret,self.isLogin];
    //  [dconnection fetch:1 withPostdata:pdata withGetData:@"" isSilent:NO];
    
    NSLog(@"access Token %@",[json objectForKey:@"access_token"]);
    [self closePopupAnimation];
    
    [self socialApiCalledForInstagram:json];
    
//    UIAlertView *alertView = [[UIAlertView alloc]
//                              initWithTitle:@"Instagram Access TOken"
//                              message:pdata
//                              delegate:nil
//                              cancelButtonTitle:@"OK"
//                              otherButtonTitles:nil];
//    [alertView show];
}


- (void) loginButton:	(FBSDKLoginButton *)loginButton
didCompleteWithResult:	(FBSDKLoginManagerLoginResult *)result
               error:	(NSError *)error
{
    
    NSLog(@"Login");
    
    //self.profileView.hidden=NO;
    
    
    if ([FBSDKAccessToken currentAccessToken]) {
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"email,first_name"}]
         
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 
                 
                 NSLog(@"fetched user:%@", result);
                 NSLog(@"email %@",result[@"email"]);
                 
                 
             }else
             {
                 NSLog(@"No data");
             }
         }];
        
    }
    
    
}



- (void) loginButtonDidLogOut:(FBSDKLoginButton *)loginButton;
{
    
    NSLog(@"fb Logout");
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
