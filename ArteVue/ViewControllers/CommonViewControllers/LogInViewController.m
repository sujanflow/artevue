//
//  LogInViewController.m
//  Artegram
//
//  Created by Sujan on 6/7/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "LogInViewController.h"

#import "SignUpViewController.h"
#import "TabBarViewController.h"
#import "ForGotPasswordViewController.h"


#import "DGActivityIndicatorView.h"

#import "Constants.h"
#import "UserAccount.h"
#import "ServerManager.h"
#import "AppDelegate.h"

@interface LogInViewController ()
{
    CGRect popupGoToFrame,popupGoFromFrame;
    UIView *termConditionView;
    
    IBOutlet UIWebView *webview;
    OAConsumer* consumer;
    OAToken* requestToken;
    OAToken* accessToken;
    NSMutableData *receivedData;
    
    NSString *client_id;
    NSString *secret;
    NSString *callback;
}

@property (nonatomic, retain) NSString *isLogin;
@property (assign, nonatomic) Boolean isReader;

@property (nonatomic) DGActivityIndicatorView *activityIndicatorView;

@end

@implementation LogInViewController

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  
//    client_id = @"1c3e33123972472b8e9b6cb62d3e5915";
//    secret = @"39a730a77eb34bb5a08d86fd72bd5468";
//
    self.activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)DGActivityIndicatorAnimationTypeTriplePulse tintColor:[UIColor whiteColor]];
    CGFloat width = self.view.bounds.size.width / 5.0f;
    CGFloat height = self.view.bounds.size.width / 5.0f;
    
    //self.activityIndicatorView.backgroundColor=[UIColor redColor];
    self.activityIndicatorView.frame = CGRectMake(self.view.bounds.size.width/2-width/2,self.view.bounds.size.height/2-height/2, width, height);
    [self.view addSubview:self.activityIndicatorView];
    

    
    client_id = @"600a132590df40f59cbc8dfaae0a0084";
    secret = @"6fc1abd1fd89441eaf74734f0616211b";
    callback = [NSString stringWithFormat:@"%@/",SERVER_BASE_API_URL]; // sample call back URL
    
    
    
    
    // set color for placeholder
    UIColor *color = [UIColor lightGrayColor];
    self.userNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: color}];
    self.passWordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
    
    // Border in Button
    self.logInButton.layer.borderWidth = 0.5f;
    self.logInButton.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    
    //set delegate
    self.userNameTextField.delegate = self;
    self.passWordTextField.delegate = self;
    
    self.loginButton.readPermissions =
    @[@"public_profile", @"email", @"user_friends"];
    self.loginButton.delegate=self;
    
    //self.userNameTextField.text=@"user1";
    //self.passWordTextField.text=@"123456";
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [self.userNameTextField resignFirstResponder];
    [self.passWordTextField resignFirstResponder];
    
    return YES;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    
    [self.userNameTextField resignFirstResponder];
    [self.passWordTextField resignFirstResponder];
    
}

-(void) createTermsWebview
{
    
    
    popupGoToFrame = CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height);
    popupGoFromFrame = CGRectMake(0, SCREEN_SIZE.height, SCREEN_SIZE.width, SCREEN_SIZE.height);
    
    termConditionView = [[UIView alloc] initWithFrame:popupGoFromFrame];
    [termConditionView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
    //
    [self.view addSubview:termConditionView];
    //
    
    UIView *bgImageview = [[UIView alloc] initWithFrame:CGRectMake(15, IPHONE_5?92:48, SCREEN_SIZE.width-30,  SCREEN_SIZE.height-200)];
    //
    //[bgImageview setImage:[UIImage imageNamed:@"terms_use_popup.png"]];
    //
    bgImageview.layer.cornerRadius=3;
    bgImageview.layer.borderColor=[UIColor blackColor].CGColor;
    bgImageview.backgroundColor=[UIColor whiteColor];
    
    [termConditionView addSubview:bgImageview];
    
    webview = [[UIWebView alloc] initWithFrame:CGRectMake(2, 40, bgImageview.frame.size.width-4
                                                          , bgImageview.frame.size.height-45)];
    
    [webview setUserInteractionEnabled:YES];
    [webview setDelegate:self];
    webview.scalesPageToFit = YES;
    
    [webview setBackgroundColor:[UIColor clearColor]];
    [bgImageview addSubview:webview];
    
   
        NSString *url = [NSString stringWithFormat:@"https://api.instagram.com/oauth/authorize/?client_id=%@&redirect_uri=%@&response_type=code",client_id,callback];
    NSLog(@"url %@",url);
        [webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    
    
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(webview.frame.origin.x+15, -5 ,150, 50)];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    titleLabel.numberOfLines = 0;
    [titleLabel setText:@"Login"];
    [titleLabel setFont:[UIFont fontWithName:@"AzoSans-Regular" size:18.0f]];
    [bgImageview addSubview:titleLabel];
    
    
    UIButton *cancleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [cancleButton setBackgroundColor:[UIColor blackColor]];
    [cancleButton setImage:[UIImage imageNamed:@"blackCloseButton"] forState:UIControlStateNormal];
    [cancleButton setFrame:CGRectMake(bgImageview.frame.size.width - 60,-5, 50, 50)];
    [cancleButton addTarget:self
                     action:@selector(cancleAction:)
           forControlEvents:UIControlEventTouchUpInside];
    [bgImageview addSubview:cancleButton];
    
    [self openPopupAnimation];
}

-(IBAction)cancleAction:(id)sender{
    
    //self.webViewContainer.hidden=YES;
    [self closePopupAnimation];
}

-(void)openPopupAnimation{
    [UIView animateWithDuration:0.6f animations:^{
        [termConditionView setFrame:popupGoToFrame];
    } completion:^(BOOL finished) {
    }];
    
}

-(void)closePopupAnimation{
    [UIView animateWithDuration:0.6f animations:^{
        [termConditionView setFrame:popupGoFromFrame];
    } completion:^(BOOL finished) {
    }];
}

- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType
{
    //    [indicator startAnimating];
    if ([[[request URL] host] isEqualToString:@"52.15.139.172"]) {
        
        // Extract oauth_verifier from URL query
        NSString* verifier = nil;
        NSArray* urlParams = [[[request URL] query] componentsSeparatedByString:@"&"];
        for (NSString* param in urlParams) {
            NSArray* keyValue = [param componentsSeparatedByString:@"="];
            NSString* key = [keyValue objectAtIndex:0];
            if ([key isEqualToString:@"code"]) {
                verifier = [keyValue objectAtIndex:1];
                break;
            }
        }
        
        if (verifier) {
            
            NSString *data = [NSString stringWithFormat:@"client_id=%@&client_secret=%@&grant_type=authorization_code&redirect_uri=%@&code=%@",client_id,secret,callback,verifier];
            
            NSString *url = [NSString stringWithFormat:@"https://api.instagram.com/oauth/access_token"];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
            [request setHTTPMethod:@"POST"];
            [request setHTTPBody:[data dataUsingEncoding:NSUTF8StringEncoding]];
            NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:request delegate:self];
            receivedData = [[NSMutableData alloc] init];
        } else {
            // ERROR!
        }
        
        
        
        return NO;
    }
    return YES;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    // [indicator stopAnimating];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data

{
    [receivedData appendData:data];
    
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
  
    UIAlertController * alertController=   [UIAlertController
                                            alertControllerWithTitle:@"Error"
                                            message:[NSString stringWithFormat:@"%@", error]
                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   
                                                   
                                               }];
    
    
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // NSString *response = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    
    NSError *jsonError;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:receivedData
                                                         options:NSJSONReadingMutableContainers
                                                           error:&jsonError];
    
    //  WebServiceSocket *dconnection = [[WebServiceSocket alloc] init];
    //   dconnection.delegate = self;
    
    NSLog(@"json %@",json);
    
    NSString *pdata = [NSString stringWithFormat:@"type=3&token=%@&secret=123&login=%@", [json objectForKey:@"access_token"], self.isLogin];
    //  NSString *pdata = [NSString stringWithFormat:@"type=3&token=%@&secret=123&login=%@",[tokenData accessToken.secret,self.isLogin];
    //  [dconnection fetch:1 withPostdata:pdata withGetData:@"" isSilent:NO];
    
    NSLog(@"access Token %@",[json objectForKey:@"access_token"]);
    [self closePopupAnimation];
    [self socialApiCalledForInstagram:json];
    
//    UIAlertView *alertView = [[UIAlertView alloc]
//                              initWithTitle:@"Instagram Access TOken"
//                              message:pdata
//                              delegate:nil
//                              cancelButtonTitle:@"OK"
//                              otherButtonTitles:nil];
//    [alertView show];
}


#pragma mark - Button Actions


- (IBAction)logInButtonAction:(id)sender
{
    [self.view endEditing:YES];
   
    if(self.userNameTextField.text.length && self.passWordTextField.text.length)
    {
     
        [self.activityIndicatorView startAnimating];

        [[ServerManager sharedManager] postLoginUser:self.userNameTextField.text password:self.passWordTextField.text completion:^(BOOL success) {
           
            
            if (success) {
                
                self.userNameTextField.text=@"";
                self.passWordTextField.text=@"";
                
                
                [[ServerManager sharedManager] getCurrentUserDetailsWithCompletion:^(BOOL success, NSMutableDictionary *responseObject) {
                  
                    [self.activityIndicatorView stopAnimating];

                    if ( responseObject!=nil) {
                       
                        NSLog(@"responseObject %@",responseObject);
                        [UserAccount sharedManager].userId=[[responseObject objectForKey:@"id"] intValue];
                        [UserAccount sharedManager].userName=[responseObject objectForKey:@"username"];
                        [UserAccount sharedManager].userFirstName=[responseObject objectForKey:@"name"];
                        [UserAccount sharedManager].userType=[responseObject objectForKey:@"user_type"];
                        [UserAccount sharedManager].email=[responseObject objectForKey:@"email"];
                        
                        
                        if([[responseObject objectForKey:@"profile_picture"] isEqual:[NSNull null]] || [[responseObject objectForKey:@"profile_picture"] isEqualToString:@""])
                        {
                            [UserAccount sharedManager].userImageName=@"img/profile-holder.png";
                        }
                        else
                            [UserAccount sharedManager].userImageName=[responseObject objectForKey:@"profile_picture"];
                        
                        [UserAccount sharedManager].isSavetoAlbum=[[responseObject objectForKey:@"is_save_to_phone"] boolValue];
                        [UserAccount sharedManager].isAccountPrivate=[[responseObject objectForKey:@"is_account_private"] boolValue];
                        [UserAccount sharedManager].isNotificationEnabled=[[responseObject objectForKey:@"is_notification_enabled"] boolValue];
                        
                        if([[responseObject objectForKey:@"gallery_name"] isEqual:[NSNull null]])
                        {
                            [UserAccount sharedManager].galleryTitle=@"";
                            
                        }else
                            [UserAccount sharedManager].galleryTitle=[responseObject objectForKey:@"gallery_name"];
                        
                        if([[responseObject objectForKey:@"gallery_description"] isEqual:[NSNull null]])
                        {
                            [UserAccount sharedManager].galleryDescription=@"";
                            
                        }else
                            [UserAccount sharedManager].galleryDescription=[responseObject objectForKey:@"gallery_description"];
                        
                        AppDelegate *appDelegateTemp = [[UIApplication sharedApplication]delegate];
                        
                        [appDelegateTemp askForNotificationPermission];
                        TabBarViewController *viewController=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
                        appDelegateTemp.window.rootViewController = viewController;
                        
                    }
                    else{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.activityIndicatorView stopAnimating];

                            
                            
                            UIAlertController * alertController=   [UIAlertController
                                                                    alertControllerWithTitle:@"Sorry, unable to fetch user data. Please try again."
                                                                    message:@""
                                                                    preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                       handler:^(UIAlertAction * action) {
                                                                           
                                                                           
                                                                       }];
                            
                            
                            [alertController addAction:ok];
                            
                            [self presentViewController:alertController animated:YES completion:nil];

                            
                                                    });

                    }
                }];
                
                
            }
            else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self.activityIndicatorView stopAnimating];
                    
                    
                    UIAlertController * alertController=   [UIAlertController
                                                            alertControllerWithTitle:@"Sorry, unable to log you in. Please enter a valid username and password."
                                                            message:@""
                                                            preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action) {
                                                                   
                                                                   
                                                               }];
                    
                    
                    [alertController addAction:ok];
                    
                    [self presentViewController:alertController animated:YES completion:nil];
                
                });
                
            }

            
        }];
        
    }
    else
    {

        UIAlertController * alertController=   [UIAlertController
                                                alertControllerWithTitle:@"Please enter both username and password"
                                                message:@""
                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       
                                                       
                                                   }];
        
        
        [alertController addAction:ok];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    


}


- (IBAction)forgotPassAction:(id)sender
{
    
    ForGotPasswordViewController *vc  = [self.storyboard instantiateViewControllerWithIdentifier:@"ForGotPasswordViewController"];
    
    [self presentViewController:vc animated:YES completion:nil];

}



- (IBAction)signInWithInstagram:(id)sender
{
    [self createTermsWebview];
}

- (IBAction)signInWithFacebook:(id)sender
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile",@"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             // NSLog(@"Logged in");
             NSLog(@"fetched user:%@", result);
             
             NSLog(@"User name: %@",[FBSDKProfile currentProfile].name);
             NSLog(@"User ID: %@",[FBSDKProfile currentProfile].userID);
             
             [self fetchUserInfo];
             // NSLog(@"email %@",result[@"email"]);
         }
     }];

    
}

-(void)fetchUserInfo
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        NSLog(@"Token is available : %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email, birthday, bio ,location ,friends ,hometown , friendlists"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error)
             {
                 NSLog(@"resultis:%@",result);
                 
                 //[KSToastView ks_showToast:@"Logged in with Facebook" duration:2.0f];
                 [self socialApiCalledForFacebook:result];
                 
                 //
             }
             else
             {
                 NSLog(@"Error %@",error);
             }
         }];
        
    }
    
}

-(void) socialApiCalledForFacebook: (id) result
{
    
    [self.activityIndicatorView startAnimating];
    
    [[ServerManager sharedManager] postFacebookLoginUsingEmail:[result objectForKey:@"email"] completion:^(BOOL success) {
        
        
        if (success) {
            
            self.userNameTextField.text=@"";
            self.passWordTextField.text=@"";
            
            
            [[ServerManager sharedManager] getCurrentUserDetailsWithCompletion:^(BOOL success, NSMutableDictionary *responseObject) {
                
                [self.activityIndicatorView stopAnimating];
                
                if ( responseObject!=nil) {
                    
                    NSLog(@"responseObject %@",responseObject);
                    [UserAccount sharedManager].userId=[[responseObject objectForKey:@"id"] intValue];
                    [UserAccount sharedManager].userName=[responseObject objectForKey:@"username"];
                    [UserAccount sharedManager].userFirstName=[responseObject objectForKey:@"name"];
                    [UserAccount sharedManager].userType=[responseObject objectForKey:@"user_type"];
                    
                    [UserAccount sharedManager].email=[responseObject objectForKey:@"email"];
                    
                    if([[responseObject objectForKey:@"profile_picture"] isEqual:[NSNull null]] || [[responseObject objectForKey:@"profile_picture"] isEqualToString:@""])
                    {
                        [UserAccount sharedManager].userImageName=@"img/profile-holder.png";
                    }
                    else
                        [UserAccount sharedManager].userImageName=[responseObject objectForKey:@"profile_picture"];
                    
                    [UserAccount sharedManager].isSavetoAlbum=[[responseObject objectForKey:@"is_save_to_phone"] boolValue];
                    [UserAccount sharedManager].isAccountPrivate=[[responseObject objectForKey:@"is_account_private"] boolValue];
                    [UserAccount sharedManager].isNotificationEnabled=[[responseObject objectForKey:@"is_notification_enabled"] boolValue];
                    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"socialLogin"];
                    
                    if([[responseObject objectForKey:@"gallery_name"] isEqual:[NSNull null]])
                    {
                        [UserAccount sharedManager].galleryTitle=@"";
                        
                    }else
                        [UserAccount sharedManager].galleryTitle=[responseObject objectForKey:@"gallery_name"];
                    
                    if([[responseObject objectForKey:@"gallery_description"] isEqual:[NSNull null]])
                    {
                        [UserAccount sharedManager].galleryDescription=@"";
                        
                    }else
                        [UserAccount sharedManager].galleryDescription=[responseObject objectForKey:@"gallery_description"];
                    
                    AppDelegate *appDelegateTemp = [[UIApplication sharedApplication]delegate];
                    
                    [appDelegateTemp askForNotificationPermission];
                    TabBarViewController *viewController=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
                    appDelegateTemp.window.rootViewController = viewController;
                    
                    
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.activityIndicatorView stopAnimating];
                        
                    
                        
                        UIAlertController * alertController=   [UIAlertController
                                                                alertControllerWithTitle:@"Sorry, unable to fetch user data. Please try again."
                                                                message:@""
                                                                preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action) {
                                                                       
                                                                       
                                                                   }];
                        
                        
                        [alertController addAction:ok];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                        
                    });
                    
                }
            }];
            
            
        }
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.activityIndicatorView stopAnimating];
                
                UIAlertController * alertController=   [UIAlertController
                                                        alertControllerWithTitle:@"Sorry, unable to log you in. Please try again."
                                                        message:@""
                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action) {
                                                               
                                                               
                                                           }];
                
                
                [alertController addAction:ok];
                
                [self presentViewController:alertController animated:YES completion:nil];
                
            });
            
        }
        
        
    }];


}
-(void) socialApiCalledForInstagram: (NSDictionary*) result
{
        AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
        apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
        NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
        [postData setObject:[[result objectForKey:@"user"] objectForKey:@"username"]forKey:@"username"];
        [postData setObject:ACCESS_KEY forKey:@"access_key"];
    
        [apiLoginManager POST:[NSString stringWithFormat:@"%@/users/api-instagram-login",SERVER_BASE_API_URL] parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
    
    
        NSLog(@"login result %@",responseObject);
    
        
        if([[responseObject objectForKey:@"success"] intValue]==1)
        {
                
                
                [UserAccount sharedManager].userId=[[[responseObject objectForKey:@"UserDetail"]objectForKey:@"id"] intValue];
                [UserAccount sharedManager].userName=[[result objectForKey:@"user"] objectForKey:@"username"];
                [UserAccount sharedManager].userFirstName=[[result objectForKey:@"user"] objectForKey:@"full_name"];
                //  [UserAccount sharedManager].userType=[[responseObject objectForKey:@"UserDetail"] objectForKey:@"user_type"];
                //  [UserAccount sharedManager].userLastName=[[responseObject objectForKey:@"UserDetail"] objectForKey:@"last_name"];
                //   [UserAccount sharedManager].userImageName=[[responseObject objectForKey:@"UserDetail"] objectForKey:@"profile_picture"];
            
                [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"socialLogin"];
            
            if([[[responseObject objectForKey:@"UserDetail"] objectForKey:@"profile_picture"] isEqual:[NSNull null]])
            {
                [UserAccount sharedManager].userImageName=@"img/profile-holder.png";
                
            }
            else if ([[[responseObject objectForKey:@"UserDetail"] objectForKey:@"profile_picture"] isEqualToString:@""] || ![[responseObject objectForKey:@"UserDetail"] objectForKey:@"profile_picture"])
            {
                [UserAccount sharedManager].userImageName=@"img/profile-holder.png";
                
            }
            else
                
                [UserAccount sharedManager].userImageName=[[responseObject objectForKey:@"UserDetail"] objectForKey:@"profile_picture"];
            
            [UserAccount sharedManager].isSavetoAlbum=[[[responseObject objectForKey:@"UserDetail"] objectForKey:@"is_save_to_phone"] boolValue];
            [UserAccount sharedManager].isAccountPrivate=[[[responseObject objectForKey:@"UserDetail"] objectForKey:@"is_account_private"] boolValue];
            [UserAccount sharedManager].isNotificationEnabled=![[[responseObject objectForKey:@"UserDetail"] objectForKey:@"is_notification_off"] boolValue];
            
            
                TabBarViewController *viewController=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
                [self.navigationController pushViewController:viewController animated:NO];
                
        }
        else
        {
         
            AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
            apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
            
            NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
            [postData setObject:[[result objectForKey:@"user"] objectForKey:@"username"]forKey:@"username"];
            [postData setObject:[[result objectForKey:@"user"] objectForKey:@"full_name"]forKey:@"first_name"];
            [postData setObject:[[result objectForKey:@"user"] objectForKey:@"full_name"]forKey:@"last_name"];
            [postData setObject:[[result objectForKey:@"user"] objectForKey:@"id"]forKey:@"social_id"];
            [postData setObject:@"5" forKey:@"user_type_id"];
            [postData setObject:@"instagram" forKey:@"social_provider"];
            
            [postData setObject:ACCESS_KEY forKey:@"access_key"];
            
            [apiLoginManager POST:[NSString stringWithFormat:@"%@/users/api-instagram-signup",SERVER_BASE_API_URL] parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
               
                
                if([[responseObject objectForKey:@"success"] intValue]==1)
                {
                    
                   // [self followArteVueWithId:[[[responseObject objectForKey:@"UserDetail"]objectForKey:@"id"] intValue]];
                    
                    [UserAccount sharedManager].userId=[[responseObject objectForKey:@"user_id"] intValue];
                    [UserAccount sharedManager].userName=[[result objectForKey:@"user"] objectForKey:@"username"] ;
                    [UserAccount sharedManager].userFirstName=[[result objectForKey:@"user"] objectForKey:@"full_name"];
                    //  [UserAccount sharedManager].userType=[[responseObject objectForKey:@"UserDetail"] objectForKey:@"user_type"];
                    //  [UserAccount sharedManager].userLastName=[[responseObject objectForKey:@"UserDetail"] objectForKey:@"last_name"];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"socialLogin"];
                    
                    if([[[responseObject objectForKey:@"UserDetail"] objectForKey:@"profile_picture"] isEqual:[NSNull null]])
                    {
                        [UserAccount sharedManager].userImageName=@"img/profile-holder.png";
                    }
                    else if ([[[responseObject objectForKey:@"UserDetail"] objectForKey:@"profile_picture"] isEqualToString:@""] || ![[responseObject objectForKey:@"UserDetail"] objectForKey:@"profile_picture"])
                    {
                        [UserAccount sharedManager].userImageName=@"img/profile-holder.png";
                    }
                    else
                        [UserAccount sharedManager].userImageName=[[responseObject objectForKey:@"UserDetail"] objectForKey:@"profile_picture"];
                    
                    
                    [UserAccount sharedManager].isSavetoAlbum=0;
                    [UserAccount sharedManager].isAccountPrivate=0;
                    [UserAccount sharedManager].isNotificationEnabled=1;
                    
                    
                    TabBarViewController *viewController=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
                    
                    [self.navigationController pushViewController:viewController animated:NO];
                    
                }else
                {
                   
                    UIAlertController * alertController=   [UIAlertController
                                                            alertControllerWithTitle:@"Please try again"
                                                            message:@""
                                                            preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action) {
                                                                   
                                                                   
                                                               }];
                    
                    
                    [alertController addAction:ok];
                    
                    [self presentViewController:alertController animated:YES completion:nil];
                    
                }
                
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                NSLog(@"error %@",error);
                
             
                
                UIAlertController * alertController=   [UIAlertController
                                                        alertControllerWithTitle:@"Server error"
                                                        message:@""
                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action) {
                                                               
                                                               
                                                           }];
                
                
                [alertController addAction:ok];
                
                [self presentViewController:alertController animated:YES completion:nil];
            }];

        }

        
    
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"error %@",error);
    
          
            
            UIAlertController * alertController=   [UIAlertController
                                                    alertControllerWithTitle:@"Server error"
                                                    message:@""
                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           
                                                       }];
            
            
            [alertController addAction:ok];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
        }];
    
}



- (IBAction)signUpButtonAction:(id)sender
{
    
    //Sujan Look out
    //[self.navigationController popViewControllerAnimated:YES];
    
    SignUpViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
    
    [self.navigationController pushViewController:controller animated:YES];

    
}

- (void) loginButton:	(FBSDKLoginButton *)loginButton
didCompleteWithResult:	(FBSDKLoginManagerLoginResult *)result
               error:	(NSError *)error
{
    
    NSLog(@"Login");
    
    //self.profileView.hidden=NO;
    
    
    if ([FBSDKAccessToken currentAccessToken]) {
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"email,first_name"}]
         
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 
                 
                 NSLog(@"fetched user:%@", result);
                 NSLog(@"email %@",result[@"email"]);
                 
                 
             }else
             {
                 NSLog(@"No data");
             }
         }];
        
    }
    
    
}



- (void) loginButtonDidLogOut:(FBSDKLoginButton *)loginButton;
{
    
    NSLog(@"fb Logout");
    
}
/*
- (void)followArteVueWithId:(int)userId {
    
    
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    [postData setObject:ACCESS_KEY forKey:@"access_key"];
    [postData setObject:[NSNumber numberWithInt:33] forKey:@"user_id"];
    [postData setObject:[NSNumber numberWithInt:userId] forKey:@"current_user_id"];
    
    NSLog(@"postData %@",postData);
    
    [apiLoginManager POST:[NSString stringWithFormat:@"%@/users/apiSwapFollowing",SERVER_BASE_API_URL] parameters:postData
                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                      
                      NSLog(@"Response: %@", responseObject);
                      
                      NSString * successMsg = [responseObject objectForKey:@"success"];
                      
                      if ([successMsg integerValue] == 1 ) {
                          
                          
                          NSLog(@"success msg %@",[responseObject objectForKey:@"success"]);
                          
                          [[NSNotificationCenter defaultCenter] postNotificationName:@"loadAllFeed" object:nil];
                          
                      }
                      
                  }
     
                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                      
                      NSLog(@"Error: %@", error);
                      
                      
                      NSLog(@"%@",operation.responseString);
                      
                  }];
    
    
}
*/
@end
