//
//  GetUserNameViewController.m
//  Artegrams
//
//  Created by Sujan on 12/18/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "GetUserNameViewController.h"
#import "Constants.h"
#import "UserAccount.h"
#import "AppDelegate.h"
#import "TabBarViewController.h"
#import "ServerManager.h"

@interface GetUserNameViewController ()

@end

@implementation GetUserNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSLog(@"faceBookResult %@",self.faceBookResult);
    
    // set color for placeholder
    UIColor *color = [UIColor whiteColor];
   
    self.userNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: color}];
    self.userNameTextField.delegate = self;
    [self.userNameTextField addTarget:self action:@selector(textFieldDidChangeInUserName:) forControlEvents:UIControlEventEditingDidEnd];
    
    
    self.joinButton.layer.borderWidth = 0.5f;
    self.joinButton.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL) NSStringIsValidUsername:(NSString *) checkString{
    
    NSString *userNameRegex = @"[A-Za-z0-9._]*";
    NSPredicate *userNameTextTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", userNameRegex];
    
    NSLog(@"username  %hhd",[userNameTextTest evaluateWithObject:checkString]);
    
    return [userNameTextTest evaluateWithObject:checkString];
    
    
    
}
- (void)textFieldDidChangeInUserName:(id)sender
{
    if (self.userNameTextField.text.length > 0) {
        
        if (self.userNameTextField.text.length > 16 ) {
            
         
            
            UIAlertController * alertController=   [UIAlertController
                                                    alertControllerWithTitle:nil
                                                    message:@"Username can't more than 16 characters"
                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           
                                                       }];
            
            
            [alertController addAction:ok];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
        }else if (![self NSStringIsValidUsername:self.userNameTextField.text])
        {

            UIAlertController * alertController=   [UIAlertController
                                                    alertControllerWithTitle:nil
                                                    message:@"Username can contain only letters, numbers, full stops and underscores"
                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           
                                                       }];
            
            
            [alertController addAction:ok];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
        
    }
}

-(void)verifyUserName
{
    
    [[ServerManager sharedManager] checkUserName:self.userNameTextField.text WithCompletion:^(BOOL success) {
        
        if (success) {
            
            
            UIAlertController * alertController=   [UIAlertController
                                                    alertControllerWithTitle:nil
                                                    message:@"This username is already in use on ArteVue. Please use another username"
                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           
                                                       }];
            
            
            [alertController addAction:ok];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
        else{
            
            NSLog(@"username is unique");
            [self socialApiCalledForFacebook:self.faceBookResult];
            
        }
        
        
    }];
    
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self.userNameTextField resignFirstResponder];
    
    return YES;
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    
    [self.userNameTextField resignFirstResponder];
   
    
}

- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];

}


- (IBAction)joinArtvueButtonAction:(id)sender {
    
    [self verifyUserName];
   
}

-(void) socialApiCalledForFacebook: (id) result
{

    [[ServerManager sharedManager] postSignUpForFBUser:self.userNameTextField.text socialId:[result objectForKey:@"id"] email:[result objectForKey:@"email"] withFirstName:[result objectForKey:@"first_name"] lastname:[result objectForKey:@"last_name"] withUserType:@"5" completion:^(BOOL success) {
        
        if (success) {
            
            [[ServerManager sharedManager] getCurrentUserDetailsWithCompletion:^(BOOL success, NSMutableDictionary *responseObject) {
                
               if ( responseObject!=nil) {
                    
                    NSLog(@"responseObject %@",responseObject);
                    [UserAccount sharedManager].userId=[[responseObject objectForKey:@"id"] intValue];
                    [UserAccount sharedManager].userName=[responseObject objectForKey:@"username"];
                    [UserAccount sharedManager].userFirstName=[responseObject objectForKey:@"name"];
                    [UserAccount sharedManager].userType=[responseObject objectForKey:@"user_type"];
                    
                   [UserAccount sharedManager].email=[result objectForKey:@"email"];
                   
                   
                    if([[responseObject objectForKey:@"profile_picture"] isEqual:[NSNull null]] || [[responseObject objectForKey:@"profile_picture"] isEqualToString:@""])
                    {
                        [UserAccount sharedManager].userImageName=@"img/profile-holder.png";
                    }
                    else
                        [UserAccount sharedManager].userImageName=[responseObject objectForKey:@"profile_picture"];
                    
                    [UserAccount sharedManager].isSavetoAlbum=[[responseObject objectForKey:@"is_save_to_phone"] boolValue];
                    [UserAccount sharedManager].isAccountPrivate=[[responseObject objectForKey:@"is_account_private"] boolValue];
                    [UserAccount sharedManager].isNotificationEnabled=[[responseObject objectForKey:@"is_notification_enabled"] boolValue];
                    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"socialLogin"];
                    
                    if([[responseObject objectForKey:@"gallery_name"] isEqual:[NSNull null]])
                    {
                        [UserAccount sharedManager].galleryTitle=@" ";
                        
                    }else
                        [UserAccount sharedManager].galleryTitle=[responseObject objectForKey:@"gallery_name"];
                    
                    if([[responseObject objectForKey:@"gallery_description"] isEqual:[NSNull null]])
                    {
                        [UserAccount sharedManager].galleryDescription=@" ";
                        
                    }else
                        [UserAccount sharedManager].galleryDescription=[responseObject objectForKey:@"gallery_description"];
                    
                    AppDelegate *appDelegateTemp = [[UIApplication sharedApplication]delegate];
                    
                    [appDelegateTemp askForNotificationPermission];
                    TabBarViewController *viewController=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
                    appDelegateTemp.window.rootViewController = viewController;
                    
                    
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                   
                        
                        UIAlertController * alertController=   [UIAlertController
                                                                alertControllerWithTitle:@"Sorry, unable to fetch user data. Please try again."
                                                                message:@""
                                                                preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action) {
                                                                       
                                                                       
                                                                   }];
                        
                        
                        [alertController addAction:ok];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                    });
                    
                }
            }];
            
            
        }
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                
                UIAlertController * alertController=   [UIAlertController
                                                        alertControllerWithTitle:nil
                                                        message:@"Unfortunately an error occurred. Please try signing up again"
                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action) {
                                                               
                                                               
                                                           }];
                
                
                [alertController addAction:ok];
                
                [self presentViewController:alertController animated:YES completion:nil];
            });
            
        }
        
    }];
    

}
/*
- (void)followArteVueWithId:(int)userId {
    
    
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    [postData setObject:ACCESS_KEY forKey:@"access_key"];
    [postData setObject:[NSNumber numberWithInt:33] forKey:@"user_id"];
    [postData setObject:[NSNumber numberWithInt:userId] forKey:@"current_user_id"];
    
    NSLog(@"postData %@",postData);
    
    [apiLoginManager POST:[NSString stringWithFormat:@"%@/users/apiSwapFollowing",SERVER_BASE_API_URL] parameters:postData
                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                      
                      NSLog(@"Response: %@", responseObject);
                      
                      NSString * successMsg = [responseObject objectForKey:@"success"];
                      
                      if ([successMsg integerValue] == 1 ) {
                          
                          
                          NSLog(@"success msg %@",[responseObject objectForKey:@"success"]);
                          
                          [[NSNotificationCenter defaultCenter] postNotificationName:@"loadAllFeed" object:nil];
                          
                      }
                      
                  }
     
                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                      
                      NSLog(@"Error: %@", error);
                      
                      
                      NSLog(@"%@",operation.responseString);
                      
                  }];
    
    
}
 */

@end
