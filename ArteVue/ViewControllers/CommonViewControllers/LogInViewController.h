//
//  LogInViewController.h
//  Artegram
//
//  Created by Sujan on 6/7/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"


#import "OAuthConsumer.h"

#import  <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>


@interface LogInViewController : UIViewController<UITextFieldDelegate,FBSDKLoginButtonDelegate,UIWebViewDelegate>


@property (weak, nonatomic) IBOutlet UIButton *logInButton;

@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passWordTextField;

@property (weak, nonatomic) IBOutlet FBSDKLoginButton *loginButton;


@end
