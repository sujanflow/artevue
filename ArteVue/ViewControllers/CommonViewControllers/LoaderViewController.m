//
//  LoaderViewController.m
//  Artegram
//
//  Created by Tanvir Palash on 6/5/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "LoaderViewController.h"
#import "LogInViewController.h"
#import "TabBarViewController.h"
#import "UserAccount.h"
#import "AppDelegate.h"

@interface LoaderViewController ()

@end



@implementation LoaderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
     self.navigationController.navigationBar.hidden=YES;
    
//    self.images = [[NSMutableArray alloc] init];
//    
//    for (int i = 1; i <= 91; i++) {
//        
//        NSString * fileName= [ [ NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%d",i] ofType:@"png"];
//        [self.images addObject:[UIImage imageWithContentsOfFile:fileName]];
//    }
    
    
    
    [self performSelector:@selector(animationDidFinish) withObject:nil
               afterDelay:5.0];
    //[self animationDidFinish];
}

-(void)viewDidAppear:(BOOL)animated
{
    //[self logoAnimation];
}

-(void) logoAnimation
{
    NSLog(@"logoAnimation");
    
    if(self.animationImageView)
        self.animationImageView.image=nil;
    
    self.animationImageView.animationImages = self.images;
    self.animationImageView.animationDuration = 2.5;
    self.animationImageView.animationRepeatCount=1;
    
    self.animationImageView.image = [self.animationImageView.animationImages lastObject];
    
    [self.animationImageView startAnimating];
    if ([self.animationImageView isAnimating]==NO) {
        // NSLog(@"stop");
    }
    
    [self performSelector:@selector(animationDidFinish) withObject:nil
               afterDelay:self.animationImageView.animationDuration+1.0];
    
}

-(void)animationDidFinish
{
    [self.animationImageView stopAnimating];    //here animation stops
    [self.animationImageView removeFromSuperview];    // here view removes from view hierarchy
    self.animationImageView.animationImages=nil;
    self.animationImageView = nil;
    
    [self.view.layer removeAllAnimations];
    [self.images removeAllObjects ];
    
    if([UserAccount sharedManager].userId>0)
    {
        
//        TabBarViewController *viewController=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
//        [self.navigationController pushViewController:viewController animated:NO];
        
        AppDelegate *appDelegateTemp = [[UIApplication sharedApplication]delegate];
        
         TabBarViewController *viewController=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
        
        appDelegateTemp.window.rootViewController = viewController;

    }
    else
    {
//        TutorialViewController *viewController=[self.storyboard instantiateViewControllerWithIdentifier:@"TutorialViewController"];
//        [self.navigationController pushViewController:viewController animated:YES];
        
        AppDelegate *appDelegateTemp = [[UIApplication sharedApplication]delegate];
        
        LogInViewController* rootController = [self.storyboard  instantiateViewControllerWithIdentifier:@"LogInViewController"];
        
        UINavigationController* navigation = [[UINavigationController alloc] initWithRootViewController:rootController];
        
        navigation.navigationBarHidden=YES;
        appDelegateTemp.window.rootViewController = navigation;
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
