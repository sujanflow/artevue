//
//  JoinArtegramsViewController.h
//  Artegram
//
//  Created by Sujan on 7/31/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"
#import "TTTAttributedLabel.h"

@interface JoinArtegramsViewController : UIViewController<NIDropDownDelegate,TTTAttributedLabelDelegate,UIWebViewDelegate>
{
    IBOutlet UIButton *btnSelect;
    NIDropDown *dropDown;
}

@property (strong, nonatomic) NSMutableDictionary* signUpUser;


@property (weak, nonatomic) IBOutlet UIButton *userTypeButton;

@property (weak, nonatomic) IBOutlet UIButton *joinButton;

@property (weak, nonatomic) IBOutlet UIButton *dropDownButton;

@property (weak, nonatomic) IBOutlet TTTAttributedLabel *disclaimerLabel;

-(void)rel;

@end
