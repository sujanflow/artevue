//
//  JoinArtegramsViewController.m
//  Artegram
//
//  Created by Sujan on 7/31/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "JoinArtegramsViewController.h"
#import "TabBarViewController.h"
#import "DGActivityIndicatorView.h"

#import "Constants.h"
#import "UserAccount.h"
#import "AppDelegate.h"
#import "ServerManager.h"

@interface JoinArtegramsViewController ()
{
    CGRect popupGoToFrame,popupGoFromFrame;
    UIView *termConditionView;
    
    NSMutableArray* userTypes;
}
@property (nonatomic) DGActivityIndicatorView *activityIndicatorView;
@end

@implementation JoinArtegramsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)DGActivityIndicatorAnimationTypeTriplePulse tintColor:[UIColor whiteColor]];
    CGFloat width = self.view.bounds.size.width / 5.0f;
    CGFloat height = self.view.bounds.size.width / 5.0f;
    
    //self.activityIndicatorView.backgroundColor=[UIColor redColor];
    self.activityIndicatorView.frame = CGRectMake(self.view.bounds.size.width/2-width/2,self.view.bounds.size.height/2-height/2, width, height);
    [self.view addSubview:self.activityIndicatorView];
    
    
    [self loadTypes];
    
    self.joinButton.layer.borderWidth = 0.5f;
    self.joinButton.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    
    
    
    self.disclaimerLabel.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    self.disclaimerLabel.delegate = self;
    NSRange range = [self.disclaimerLabel.text rangeOfString:@"terms"];
    
    NSLog(@"%@",self.disclaimerLabel.text);
    
    [self.disclaimerLabel addLinkToURL:[NSURL URLWithString:@"terms"] withRange:range]; // Embedding a custom link in a substring
    
    [self createTermsWebview];
    
}

- (void)viewDidUnload {
    //    [btnSelect release];
    btnSelect = nil;
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)userTypeButtonAction:(id)sender {
    
    NSArray * arr = [[NSArray alloc] init];
    //NSLog(@"userTypes %@",userTypes);
    if(userTypes.count)
    {
        arr=[userTypes valueForKey:@"title"];
        
        if(dropDown == nil) {
            CGFloat height = 160;
            dropDown = [[NIDropDown alloc]showDropDown:sender :&height :arr :nil :@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }

    }
//    else
//        arr = [NSArray arrayWithObjects:@"Artist", @"Enthusiast", @"Collector", @"Gallery",nil];
//    
    
    
//    NSArray * arrImage = [[NSArray alloc] init];
//    arrImage = [NSArray arrayWithObjects:[UIImage imageNamed:@"apple.png"], [UIImage imageNamed:@"apple2.png"], [UIImage imageNamed:@"apple.png"], [UIImage imageNamed:@"apple2.png"], nil];
//    
    
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
    
    NSLog(@"btnSelect %@", btnSelect.titleLabel.text);

    
 }

-(void)rel{
    //    [dropDown release];
    dropDown = nil;
}

- (IBAction)dropDownButtonAction:(id)sender {
    
    NSLog(@"dorp down button clicked");
    
}

-(void) loadTypes
{
    
    [[ServerManager sharedManager] getAllUserTypeWithCompletion:^(BOOL success, NSMutableDictionary *responseObject) {
        
       
        if ( responseObject!=nil) {
            
           // NSLog(@"responseObject %@",responseObject);
            userTypes=[[NSMutableArray alloc] initWithArray:responseObject];
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertController * alertController=   [UIAlertController
                                              alertControllerWithTitle:@"Sorry, unable to fetch user types"
                                              message:@""
                                              preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action) {
                                                               //Do Some action here
                                                               [self.navigationController popViewControllerAnimated:YES];
                                                               
                                                           }];
                UIAlertAction* refresh = [UIAlertAction actionWithTitle:@"refresh" style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action) {
                                                                   [self loadTypes];
                                                                   
                                                               }];

                [alertController addAction:ok];
                [alertController addAction:refresh];
                [self.navigationController presentViewController:alertController animated:YES completion:nil];

            });
            
        }
    }];
}


- (IBAction)joinArtegramButtonAction:(id)sender {

    
        if(btnSelect.titleLabel.text.length)
        {
            [self.activityIndicatorView startAnimating];

            
            for (int i=0; i<userTypes.count ;i++) {
                if([btnSelect.titleLabel.text isEqualToString:[[userTypes objectAtIndex:i] objectForKey:@"title"]])
                {
                    
                    [self.signUpUser setObject:[[userTypes objectAtIndex:i] objectForKey:@"id"] forKey:@"user_type_id"];
                    break;
                }
                
            }
            
         //   [[ServerManager sharedManager] postLoginUser:self.userNameTextField.text password:self.passWordTextField.text completion:^(BOOL success) {
            [[ServerManager sharedManager] postSignUpWithUser:[self.signUpUser objectForKey:@"username"] password:[self.signUpUser objectForKey:@"password"] email:[self.signUpUser objectForKey:@"email"] fullName:[NSString stringWithFormat:@"%@ %@",[self.signUpUser objectForKey:@"first_name"],[self.signUpUser objectForKey:@"last_name"]] userType:[self.signUpUser objectForKey:@"user_type_id"] completion:^(BOOL success) {
                
                
                if (success) {
                    
                    [[ServerManager sharedManager] postLoginUser:[self.signUpUser objectForKey:@"username"] password:[self.signUpUser objectForKey:@"password"] completion:^(BOOL success) {
                        
                       
                        if (success) {
                            
                            
                            [[ServerManager sharedManager] getCurrentUserDetailsWithCompletion:^(BOOL success, NSMutableDictionary *responseObject) {
                                
                                [self.activityIndicatorView stopAnimating];
                                
                                if ( responseObject!=nil) {
                                    
                                    NSLog(@"responseObject %@",responseObject);
                                    [UserAccount sharedManager].userId=[[responseObject objectForKey:@"id"] intValue];
                                    [UserAccount sharedManager].userName=[responseObject objectForKey:@"username"];
                                    [UserAccount sharedManager].userFirstName=[responseObject objectForKey:@"name"];
                                    [UserAccount sharedManager].userType=[responseObject objectForKey:@"user_type"];
                                    
                                    [UserAccount sharedManager].email=[responseObject objectForKey:@"email"];
                                    if([[responseObject objectForKey:@"profile_picture"] isEqual:[NSNull null]] || [[responseObject objectForKey:@"profile_picture"] isEqualToString:@""])
                                    {
                                        [UserAccount sharedManager].userImageName=@"img/profile-holder.png";
                                    }
                                    else
                                        [UserAccount sharedManager].userImageName=[responseObject objectForKey:@"profile_picture"];
                                    
                                    [UserAccount sharedManager].isSavetoAlbum=[[responseObject objectForKey:@"is_save_to_phone"] boolValue];
                                    [UserAccount sharedManager].isAccountPrivate=[[responseObject objectForKey:@"is_account_private"] boolValue];
                                    [UserAccount sharedManager].isNotificationEnabled=[[responseObject objectForKey:@"is_notification_enabled"] boolValue];
                                    
                                    if([[responseObject objectForKey:@"gallery_name"] isEqual:[NSNull null]])
                                    {
                                        [UserAccount sharedManager].galleryTitle=@"";
                                        
                                    }else
                                        [UserAccount sharedManager].galleryTitle=[responseObject objectForKey:@"gallery_name"];
                                    
                                    if([[responseObject objectForKey:@"gallery_description"] isEqual:[NSNull null]])
                                    {
                                        [UserAccount sharedManager].galleryDescription=@"";
                                        
                                    }else
                                        [UserAccount sharedManager].galleryDescription=[responseObject objectForKey:@"gallery_description"];
                                    
                                    
                                    AppDelegate *appDelegateTemp = [[UIApplication sharedApplication]delegate];
                                    
                                    [appDelegateTemp askForNotificationPermission];
                                    TabBarViewController *viewController=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
                                    appDelegateTemp.window.rootViewController = viewController;
                                    
                                }
                                else{
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        
                                       
                                        
                                        UIAlertController * alertController=   [UIAlertController
                                                                                alertControllerWithTitle:@"Sorry, unable to fetch user data. Please try again."
                                                                                message:@""
                                                                                preferredStyle:UIAlertControllerStyleAlert];
                                        
                                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                                   handler:^(UIAlertAction * action) {
                                                                                       
                                                                                       
                                                                                   }];
                                        
                                        
                                        [alertController addAction:ok];
                                        
                                        [self presentViewController:alertController animated:YES completion:nil];
                                    });
                                    
                                }
                            }];
                            
                            
                        }
                        else{
                            [self.activityIndicatorView stopAnimating];
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                             
                                UIAlertController * alertController=   [UIAlertController
                                                                        alertControllerWithTitle:@"Sorry, unable to log you in. Please try manual login."
                                                                        message:@""
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                                
                                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                           handler:^(UIAlertAction * action) {
                                                                               
                                                                               
                                                                           }];
                                
                                
                                [alertController addAction:ok];
                                
                                [self presentViewController:alertController animated:YES completion:nil];
                            });
                            
                        }
                        
                        
                    }];
                    
                    
                }
                else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.activityIndicatorView stopAnimating];
                        

                      
                        UIAlertController * alertController=   [UIAlertController
                                                                alertControllerWithTitle:nil
                                                                message:@"Unfortunately an error occurred. Please try signing up again"
                                                                preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action) {
                                                                       
                                                                       
                                                                   }];
                        
                        
                        [alertController addAction:ok];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                    });
                    
                }

            }];
          
        }
        else
        {
            
            
            UIAlertController * alertController=   [UIAlertController
                                                    alertControllerWithTitle:nil
                                                    message:@"Please select the user type"
                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           
                                                       }];
            
            
            [alertController addAction:ok];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
    
    
  
    
}

//- (void)followArteVueWithId:(int)userId {
//
//
//    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
//    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
//    
//    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
//    [postData setObject:ACCESS_KEY forKey:@"access_key"];
//    [postData setObject:[NSNumber numberWithInt:33] forKey:@"user_id"];
//   // [postData setObject:[NSNumber numberWithInt:userId] forKey:@"current_user_id"];
//    
//    NSLog(@"postData %@",postData);
//    
//    [apiLoginManager POST:[NSString stringWithFormat:@"%@/users/apiSwapFollowing",SERVER_BASE_API_URL] parameters:postData
//                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                      
//                      NSLog(@"Response: %@", responseObject);
//                      
//                      NSString * successMsg = [responseObject objectForKey:@"success"];
//                      
//                      if ([successMsg integerValue] == 1 ) {
//                          
//                          
//                          NSLog(@"success msg %@",[responseObject objectForKey:@"success"]);
//                          
//                          [[NSNotificationCenter defaultCenter] postNotificationName:@"loadAllFeed" object:nil];
//                          
//                      }
//                      
//                  }
//     
//                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                      
//                      NSLog(@"Error: %@", error);
//                      
//                      
//                      NSLog(@"%@",operation.responseString);
//                      
//                  }];
//    
//    
//}

-(void) createTermsWebview
{
    popupGoToFrame = CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height);
    popupGoFromFrame = CGRectMake(0, SCREEN_SIZE.height, SCREEN_SIZE.width, SCREEN_SIZE.height);
    
    
    termConditionView = [[UIView alloc] initWithFrame:popupGoFromFrame];
    [termConditionView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
    
    [self.view addSubview:termConditionView];
    
    
    UIView *bgImageview = [[UIView alloc] initWithFrame:CGRectMake(15, IPHONE_5?92:48, SCREEN_SIZE.width-30,  SCREEN_SIZE.height-200)];
    
    //[bgImageview setImage:[UIImage imageNamed:@"terms_use_popup.png"]];
    
    bgImageview.layer.cornerRadius=3;
    bgImageview.layer.borderColor=[UIColor blackColor].CGColor;
    bgImageview.backgroundColor=[UIColor whiteColor];
    
    [termConditionView addSubview:bgImageview];
    
    UIWebView *showView = [[UIWebView alloc] initWithFrame:CGRectMake(2, 40, bgImageview.frame.size.width-4
                                                                      , bgImageview.frame.size.height-45)];
    
    [showView setUserInteractionEnabled:YES];
    [showView setDelegate:self];
    showView.scalesPageToFit = YES;
    
    [showView setBackgroundColor:[UIColor clearColor]];
    [bgImageview addSubview:showView];
    
    
    NSError *error = nil;
    NSString *html = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"artevue_t&c" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    [showView loadHTMLString:html baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
    
    NSLog(@"error %@",error);
    
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(showView.frame.origin.x+15, -5 ,150, 50)];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    titleLabel.numberOfLines = 0;
    [titleLabel setText:@"Terms of use"];
    [titleLabel setFont:[UIFont fontWithName:@"AzoSans-Regular" size:18.0f]];
    [bgImageview addSubview:titleLabel];
    
    
    UIButton *cancleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [cancleButton setBackgroundColor:[UIColor blackColor]];
    [cancleButton setImage:[UIImage imageNamed:@"blackCloseButton"] forState:UIControlStateNormal];
    [cancleButton setFrame:CGRectMake(bgImageview.frame.size.width - 60,-5, 50, 50)];
    [cancleButton addTarget:self
                     action:@selector(cancleAction:)
           forControlEvents:UIControlEventTouchUpInside];
    [bgImageview addSubview:cancleButton];
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    NSLog(@"WebView Finish Loading.");
    
    
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    NSLog(@"WebView start Loading.");
    
}

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithTextCheckingResult:(NSTextCheckingResult *)result {
    //do whatever you need
    NSLog(@"result %@",result.URL);
    if ([result.URL isEqual:[NSURL URLWithString:@"terms"]]) {
        [self openPopupAnimation];
    }
    
}
-(IBAction)cancleAction:(id)sender{
    [self closePopupAnimation];
}

-(void)openPopupAnimation{
    [UIView animateWithDuration:0.6f animations:^{
        [termConditionView setFrame:popupGoToFrame];
    } completion:^(BOOL finished) {
    }];
    
}

- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)closePopupAnimation{
    [UIView animateWithDuration:0.6f animations:^{
        [termConditionView setFrame:popupGoFromFrame];
    } completion:^(BOOL finished) {
    }];
}


@end
