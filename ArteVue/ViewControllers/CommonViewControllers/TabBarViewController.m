//
//  TabBarViewController.m
//  Artegram
//
//  Created by Tanvir Palash on 6/10/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "AppDelegate.h"
#import "TabBarViewController.h"
#import "UserAccount.h"
#import "Constants.h"
#import "ServerManager.h"

@import imglyKit;

@interface TabBarViewController () <IMGLYToolStackControllerDelegate>
{
    PTPusher *pusherClient;
    FusumaViewController *fusumaCameraViewController;
    CGFloat CentreButtonPosition;
    
    
}
@property (nonatomic) UIButton* centreButton;
@property(nonatomic,retain)NSDate *lastTouchDate;

@end

@implementation TabBarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    pusherClient = [PTPusher pusherWithKey:pusher_app_key delegate:self encrypted:YES cluster:@"ap1"];
//    
//    PTPusherChannel *channel = [pusherClient subscribeToChannelNamed:[NSString stringWithFormat:@"%i-activity-channel",[UserAccount sharedManager].userId]];
//    
//    [channel bindToEventNamed:@"all-activities" handleWithBlock:^(PTPusherEvent *channelEvent) {
//        
//        NSLog(@"message received: %@", channelEvent.data);
//        [UserAccount sharedManager].pendingActivityCount++;
//        [[[[self tabBar] items] objectAtIndex:3] setBadgeValue:[NSString stringWithFormat:@"%i",[UserAccount sharedManager].pendingActivityCount]];
//        
//        
//    }];
//    [pusherClient connect];
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadNotifications) name:@"updateNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCentreButtonInCall) name:@"updateCentreButton" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetCentreButtonPosition) name:@"resetCentreButton" object:nil];
    
    
    
    [[AdobeUXAuthManager sharedManager] setAuthenticationParametersWithClientID:adobe_client_id   withClientSecret:adobe_secret_id];

    

    NSArray *items = [[NSArray alloc] initWithArray:self.tabBar.items];
    
        //Get the height of the tab bar
        
    CGFloat height = CGRectGetHeight(self.tabBar.bounds);
        
        //Calculate the size of the items
    
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                //  device is an iPad.
                
                CGFloat numItems = items.count;
                CGSize itemSize =CGSizeMake((self.tabBar.frame.size.width-220) / numItems, self.tabBar.frame.size.height);
                
                for (int i=1; i<items.count;i++)
                {
                    
                    //We don't want a separator on the left of the first item.
                    
                    if (i > 0) {
                        
                        //Xposition of the item
                        
                        CGFloat xPosition = itemSize.width * (CGFloat)i + 110;
                        
                        /* Create UI view at the Xposition,
                         with a width of 0.5 and height equal
                         to the tab bar height, and give the
                         view a background color
                         */
                        UIView* separator = [[UIView alloc] initWithFrame: CGRectMake(xPosition, 0, 0.5, height)];
                        separator.backgroundColor = [UIColor grayColor];
                        //tabBar.insertSubview(separator, atIndex: 1)
                        [self.tabBar insertSubview:separator atIndex:i];
                    }
                }
    
            }
            else{
            
                CGFloat numItems = items.count;
                CGSize itemSize =CGSizeMake(self.tabBar.frame.size.width / numItems, self.tabBar.frame.size.height);
                
                for (int i=1; i<items.count;i++)
                {
                    
                    //We don't want a separator on the left of the first item.
                    
                    if (i > 0) {
                        
                        //Xposition of the item
                        
                        CGFloat xPosition = itemSize.width * (CGFloat)i;
                        
                        /* Create UI view at the Xposition,
                         with a width of 0.5 and height equal
                         to the tab bar height, and give the
                         view a background color
                         */
                        UIView* separator = [[UIView alloc] initWithFrame: CGRectMake(xPosition, 0, 0.5, height)];
                        separator.backgroundColor = [UIColor grayColor];
                        //tabBar.insertSubview(separator, atIndex: 1)
                        [self.tabBar insertSubview:separator atIndex:i];
                    }
                }

            
            }


    

    //Camera Button
    
    UIImage* buttonImage =[UIImage imageNamed:@"centerCameraIcon@2x.png"];
    
    self.centreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.centreButton.frame = CGRectMake(0.0, 0.0, buttonImage.size.width , buttonImage.size.height);
    [self.centreButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    //[button setBackgroundImage:highlightImage forState:UIControlStateHighlighted];
    self.centreButton.userInteractionEnabled=YES;
    
    [self.centreButton addTarget:self action:@selector(openCamera:) forControlEvents:UIControlEventTouchUpInside];
    
    CGFloat heightDifference = buttonImage.size.height - self.tabBar.frame.size.height;
    NSLog(@"heightDifference %lf",heightDifference);
    
    if (heightDifference < 0)
        self.centreButton.center = self.tabBar.center;
    else
    {
        CGPoint center = self.tabBar.center;
        center.y = center.y - heightDifference/2.0-5;
        self.centreButton.center = center;
        CentreButtonPosition=center.y;
    }
    
    [self.view addSubview:self.centreButton];
    
//    NSLog(@"[UserAccount sharedManager].userImageName %@",[UserAccount sharedManager].userImageName);
//    
//    if([UserAccount sharedManager].userImageName.length)
//    {
//        SDWebImageManager *manager = [SDWebImageManager sharedManager];
//        [manager downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL,[UserAccount sharedManager].userImageName]]
//                              options:0
//                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
//                                 // progression tracking code
//                             }
//                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
//                                
//                                if (image)
//                                {
//                                    
//                                    dispatch_async(dispatch_get_main_queue(), ^{
//                                        
//                                        NSLog(@"image downloaded");
//
//                                        [[self.tabBarController.tabBar.items objectAtIndex:4] setImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
//                                        
//                                    });
//                                    
//                                }
//                                
//                            }];
//
//    }
    
    
    
    self.delegate=self;
    
   
    [self loadNotifications];
}

-(void)changeCentreButtonInCall
{
    UIImage* buttonImage =[UIImage imageNamed:@"centerCameraIcon@2x.png"];
    
    CGFloat heightDifference = buttonImage.size.height - self.tabBar.frame.size.height;
    
    
    [UIView animateWithDuration:0.5 animations:^{
        
        if (heightDifference < 0)
            self.centreButton.center = self.tabBar.center;
        else
        {
            CGPoint center = self.tabBar.center;
            
            center.y = center.y - heightDifference/2.0-5;
            NSLog(@"center.y %lf",center.y);
            
            self.centreButton.center = center;
        }
    }];
    
}
-(void)resetCentreButtonPosition
{
    [UIView animateWithDuration:0.5 animations:^{
        
        CGPoint center = self.tabBar.center;
        
        center.y = CentreButtonPosition;
        NSLog(@"center.y %lf",center.y);
        
        self.centreButton.center = center;
    }];
}

-(void) loadNotifications{
    
    NSMutableDictionary *postData=[[NSMutableDictionary alloc] init];
    if([UserAccount sharedManager].gcmRegKey)
        [postData setObject:[UserAccount sharedManager].gcmRegKey forKey:@"gcm_registration_key"];
    
    [postData setObject:[NSString stringWithFormat:@"%@ %@",[UserAccount sharedManager].userFirstName ,[UserAccount sharedManager].userLastName] forKey:@"name"];
    
    [postData setObject:[UserAccount sharedManager].email forKey:@"email"];
    
    [postData setObject:[UserAccount sharedManager].userType forKey:@"user_type_id"];
    [postData setObject:@1 forKey:@"sex"];
    
    
    
    [[ServerManager sharedManager] updateUserDetailsWithData:postData withCompletion:^(BOOL success) {
        
        if (success) {
            
        }else
        {
        }
    }];
    
   /*
    
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
    [postData setObject:ACCESS_KEY forKey:@"access_key"];
    [postData setObject:[NSNumber numberWithInt:[UserAccount sharedManager].userId] forKey:@"user_id"];
    
    if([UserAccount sharedManager].lastActivityDate)
        [postData setObject:[UserAccount sharedManager].lastActivityDate forKey:@"activity_last_checked"];
    else
        [postData setObject:@"" forKey:@"activity_last_checked"];
    
    if([UserAccount sharedManager].lastMessageDate)
        [postData setObject:[UserAccount sharedManager].lastMessageDate forKey:@"message_last_checked"];
    else
        [postData setObject:@"" forKey:@"message_last_checked"];
    
    if([UserAccount sharedManager].gcmRegKey)
        [postData setObject:[UserAccount sharedManager].gcmRegKey forKey:@"gcm_registration_key"];
    else
        [postData setObject:@"" forKey:@"gcm_registration_key"];
    
    if([UserAccount sharedManager].deviceToken)
        [postData setObject:[UserAccount sharedManager].deviceToken forKey:@"device_token"];
    else
        [postData setObject:@"" forKey:@"device_token"];
    
    
    NSLog(@"postData %@",postData);
    
    [apiLoginManager POST:[NSString stringWithFormat:@"%@/users/api-get-updated-count",SERVER_BASE_API_URL] parameters:postData
                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                      
                      NSLog(@"messagesList: %@", responseObject);
                      
                      int activityCount=[[responseObject objectForKey:@"activity_following_count"] intValue]+[[responseObject objectForKey:@"activity_you_count"] intValue];
                      [UserAccount sharedManager].pendingActivityCount=activityCount;
                      
//                      if(activityCount==0)
//                          [[[[self tabBar] items] objectAtIndex:3] setBadgeValue:nil];
//                      else
//                          [[[[self tabBar] items] objectAtIndex:3] setBadgeValue:[NSString stringWithFormat:@"%i",[UserAccount sharedManager].pendingActivityCount]];
//                      
//                      
                      [UserAccount sharedManager].pendingMessageCount=[[responseObject objectForKey:@"message_count"] intValue];
                     
                      if( [UserAccount sharedManager].pendingMessageCount>0)
                          [[NSNotificationCenter defaultCenter] postNotificationName:@"updateMessageBubble" object:nil];
                      
                }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                      
                      NSLog(@"Error: %@", error);
                      
                      
                      NSLog(@"%@",operation.responseString);
                      
                  }];
    */
}

-(NSString *)getDateStringFromDate:(NSDate *)date
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy'-'MM'-'dd' 'HH':'mm':'ss"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    NSLog(@"%@", stringFromDate);
    return stringFromDate;
    
}

-(void)openCamera:(UIButton*) sender
{
   
    fusumaCameraViewController=[[FusumaViewController alloc]
                              initWithNibName:@"FusumaViewController" bundle:nil];;
    UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:fusumaCameraViewController];
    navCon.navigationBar.hidden=YES;
    
    fusumaCameraViewController.delegate=self;
    fusumaCameraViewController.hasVideo=NO;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:fusumaCameraViewController];
    navigationController.navigationBarHidden=YES;
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (BOOL)tabBarController:(UITabBarController *)theTabBarController shouldSelectViewController:(UIViewController *)viewController
{
    
    
    if (viewController == [theTabBarController.viewControllers objectAtIndex:0])
    {
        NSLog(@"tapped on first tabbar ");
     //   [[NSNotificationCenter defaultCenter] postNotificationName:@"loadAllFeed" object:nil];
        
        
        if(self.lastTouchDate)
        {
            
            NSTimeInterval ti = [[NSDate date] timeIntervalSinceDate:self.lastTouchDate];
            if(ti < 0.7f)
                [[NSNotificationCenter defaultCenter] postNotificationName:@"loadAllFeed" object:nil];
            
        }
         self.lastTouchDate = [NSDate date];
        
    }
    if (viewController == [theTabBarController.viewControllers objectAtIndex:2])
    {
        
       

//        let fusuma = FusumaViewController()
//        
//        fusumaCropImage = true
//        
//        fusuma.delegate = self
//        fusuma.hasVideo = true
//        self.presentViewController(fusuma, animated: true, completion: nil)

      
        return NO;
        
        
    }else
    {
        return (theTabBarController.selectedViewController != viewController);
        
    }
    
    
}

-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate setShouldRotate:NO];

   
    if (viewController == [tabBarController.viewControllers objectAtIndex:3])
    {
//        [UserAccount sharedManager].pendingActivityCount=0;
//        [[[[self tabBar] items] objectAtIndex:3] setBadgeValue:nil];
        
       
    }
//    
//    if (viewController == [tabBarController.viewControllers objectAtIndex:4])
//    {
//        
//    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isInGallery"];

    }
    
 
    
}
#pragma mark: FusumaDelegate Protocol
/*
-(void) fusumaImageSelected:(UIImage *)image
{
    NSLog(@"Image selected");
    
//    SharePhotoViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SharePhotoViewController"];
//    controller.selectedImageForShare=image;
//    [self.navigationController pushViewController:controller animated:YES];

    AdobeUXImageEditorViewController *editorController = [[AdobeUXImageEditorViewController alloc] initWithImage:image];
    [editorController setDelegate:self];
   
    
    [AVYPhotoEditorCustomization setLeftNavigationBarButtonTitle:@"Back"];
    [AVYPhotoEditorCustomization setRightNavigationBarButtonTitle:@"Next"];
    
    
    [AVYPhotoEditorCustomization setCropToolCustomEnabled:YES];
    [AVYPhotoEditorCustomization setCropToolInvertEnabled:NO];
    [AVYPhotoEditorCustomization setCropToolOriginalEnabled:YES];

    //[AVYPhotoEditorCustomization setCropToolPresets:@[]];
    
   
    
    
    // Set the tools to Contrast, Brightness, Enhance, and Crop (to be displayed in that order).
    [AVYPhotoEditorCustomization setToolOrder:@[kAdobeImageEditorEnhance, kAdobeImageEditorEffects, kAdobeImageEditorCrop, kAdobeImageEditorColorAdjust,kAdobeImageEditorLightingAdjust,kAdobeImageEditorSharpness,kAdobeImageEditorFocus,kAdobeImageEditorVignette,kAdobeImageEditorRedeye]];

    
    
    UINavigationController *navigationController = (UINavigationController*)self.presentedViewController;
    [navigationController pushViewController:editorController animated:YES];
}

*/
-(void) fusumaImageSelected:(UIImage *)image
{
    IMGLYConfiguration *sampleConfiguration=[[IMGLYConfiguration alloc] initWithBuilder:^(IMGLYConfigurationBuilder * _Nonnull builder) {
        builder.backgroundColor=[UIColor blackColor];
        builder.separatorColor=[UIColor whiteColor];
        builder.contextMenuBackgroundColor=[UIColor whiteColor];
       
        
        [builder configurePhotoEditorViewController:^(IMGLYPhotoEditViewControllerOptionsBuilder * _Nonnull phBuilder) {
         
//            phBuilder.backgroundColor=[UIColor redColor];
            
            phBuilder.title=@"ArteVue Editor";
        
            
            [phBuilder setAllowedPhotoEditorActionsAsNSNumbers:@[@0,@2,@3,@9]];
            
        
           [phBuilder setActionButtonConfigurationClosure:^(IMGLYIconCaptionCollectionViewCell * _Nonnull cell, enum PhotoEditorAction action ) {
           
               [cell setTintColor:[UIColor whiteColor]];
               
               if(action==PhotoEditorActionFilter)
               {
                  
                   cell.captionLabel.text=@"Filters";
//                   cell.selected=YES;
//               
                   
               }
            
            }];
    
        }];
        
        [builder configureAdjustToolController:^(IMGLYAdjustToolControllerOptionsBuilder * _Nonnull adBuilder) {
            
            
            [adBuilder setAllowedAdjustToolsAsNSNumbers:@[@5,@1,@2,@3,@4,@6]];
            
            
            [adBuilder setAdjustToolButtonConfigurationClosure:^(IMGLYIconCaptionCollectionViewCell * _Nonnull cell, enum AdjustTool tool) {
                if (tool==AdjustToolExposure) {
                    cell.captionLabel.text=@"Brightness";
                }
            }];
          
            [adBuilder setSliderChangedValueClosure:^(IMGLYSlider * _Nonnull slider, enum AdjustTool tool) {
                if (tool==AdjustToolBrightness)
                {
                    [slider setMinimumValue:-0.10f];
                    [slider setMaximumValue:0.10f];
                }
                else if ( tool==AdjustToolContrast)
                {
                    [slider setMinimumValue:0.5f];
                    [slider setNeutralValue:1.0f];
                    [slider setMaximumValue:1.5f];
                    
                   // [slider setMaximumValue:0.8f];
                    
                }
            }];
            
        }];
        
        
        [builder configureFilterToolController:^(IMGLYFilterToolControllerOptionsBuilder * _Nonnull filterBuilder) {
            
            
//            IMGLYPhotoEffect *effect1=[[IMGLYPhotoEffect alloc] initWithIdentifier:@"None" CIFilterName:nil lutURL:nil displayName:@"None" options:nil];
//            IMGLYPhotoEffect *effect2=[[IMGLYPhotoEffect alloc] initWithIdentifier:@"K1" lutURL:[[NSBundle bundleForClass:IMGLYPhotoEffect.self] URLForResource:@"K1" withExtension:@"png"] displayName:@"K1"];
//            IMGLYPhotoEffect *effect3=[[IMGLYPhotoEffect alloc] initWithIdentifier:@"K1" lutURL:[[NSBundle bundleForClass:IMGLYPhotoEffect.self] URLForResource:@"Dynamic" withExtension:@"png"] displayName:@"Dynamic"];
//            
//            [IMGLYPhotoEffect setAllEffects:@[effect1,effect2]];
//
            
            //NSArray *effects=@[[IMGLYPhotoEffect allEffects][0],[IMGLYPhotoEffect allEffects][1]];
            //01
          //  [IMGLYPhotoEffect setAllEffects:effects];
            
            NSMutableArray *effectsArray=[[NSMutableArray alloc] init];
          
            for (int i=0; i<[IMGLYPhotoEffect allEffects].count; i++) {
                if([[IMGLYPhotoEffect allEffects][i].identifier isEqualToString:@"None"] || [[IMGLYPhotoEffect allEffects][i].identifier isEqualToString:@"K1"] || [[IMGLYPhotoEffect allEffects][i].identifier isEqualToString:@"Soft"] || [[IMGLYPhotoEffect allEffects][i].identifier isEqualToString:@"X400"] || [[IMGLYPhotoEffect allEffects][i].identifier isEqualToString:@"Pale"] || [[IMGLYPhotoEffect allEffects][i].identifier isEqualToString:@"Quozi"] || [[IMGLYPhotoEffect allEffects][i].identifier isEqualToString:@"Hicon"] || [[IMGLYPhotoEffect allEffects][i].identifier isEqualToString:@"Front"] || [[IMGLYPhotoEffect allEffects][i].identifier isEqualToString:@"Creamy"] || [[IMGLYPhotoEffect allEffects][i].identifier isEqualToString:@"Summer"] || [[IMGLYPhotoEffect allEffects][i].identifier isEqualToString:@"Breeze"] || [[IMGLYPhotoEffect allEffects][i].identifier isEqualToString:@"Lomo"] || [[IMGLYPhotoEffect allEffects][i].identifier isEqualToString:@"Neat"] || [[IMGLYPhotoEffect allEffects][i].identifier isEqualToString:@"Sunset"] || [[IMGLYPhotoEffect allEffects][i].identifier isEqualToString:@"Cool"] || [[IMGLYPhotoEffect allEffects][i].identifier isEqualToString:@"Settled"] || [[IMGLYPhotoEffect allEffects][i].identifier isEqualToString:@"Dynamic"])
                {
                    [effectsArray addObject:[IMGLYPhotoEffect allEffects][i]];
                }
            }
            
            [IMGLYPhotoEffect setAllEffects:effectsArray];
//
            
            [filterBuilder setFilterCellConfigurationClosure:^(IMGLYFilterCollectionViewCell * _Nonnull cell, IMGLYPhotoEffect * _Nonnull effects) {
                
                if ([effects.identifier isEqualToString:@"K1"]) {
                    
                }
                
            }];
        }];
        
    }];

    //photoEditViewControllerCurrentEditingTool
    
    
    
    IMGLYPhotoEditViewController *editorController = [[IMGLYPhotoEditViewController alloc] initWithPhoto:image configuration:sampleConfiguration];
    IMGLYToolStackController *toolStackController=[[IMGLYToolStackController alloc] initWithPhotoEditViewController:editorController];
    toolStackController.delegate=self;
    
    
    
    
    UINavigationController *navigationController = (UINavigationController*)self.presentedViewController;
    [navigationController pushViewController:toolStackController animated:YES];
}

- (void)toolStackController:(IMGLYToolStackController * _Nonnull)toolStackController didFinishWithImage:(UIImage * _Nonnull)image;
{
    NSLog(@"didFinishWithImage");
    
    
    //[self.navigationController pushViewController:controller animated:YES];
    
    SharePhotoViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SharePhotoViewController"];
    controller.selectedImageForShare=image;
    controller.isForEdit=NO;
    controller.isForShare=NO;
    //[self presentViewController:controller animated:NO completion:nil];
    
    
    UINavigationController *navigationController = (UINavigationController*)self.presentedViewController;
    [navigationController pushViewController:controller animated:YES];
    
    
}
- (void)toolStackControllerDidFail:(IMGLYToolStackController * _Nonnull)toolStackController;
{
    NSLog(@"toolStackControllerDidFail");

}

- (void)toolStackControllerDidCancel:(IMGLYToolStackController * _Nonnull)toolStackController;
{
    NSLog(@"toolStackControllerDidCancel");
    UINavigationController *navigationController = (UINavigationController*)self.presentedViewController;
    [navigationController popViewControllerAnimated:YES];
    

}

-(void)fusumaVideoCompletedWithFileURL:(NSURL *)fileURL
{
    NSLog(@"video completed and output to file: \(%@)",fileURL);
    
}

-(void)fusumaDismissedWithImage:(UIImage *)image
{
    NSLog(@"Called just after dismissed FusumaViewController");
    
    
////for test
    //    UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
    //    UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
    //    [pvc presentViewController: editorController animated: NO completion:nil];
    
    //   [self presentViewController:editorController animated:NO completion:nil];
    //  [self.navigationController pushViewController:editorController animated:YES];
    
    
}

-(void) fusumaCameraRollUnauthorized
{
    NSLog(@"Camera roll unauthorized");
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Access Requested" message:@"Please click OK to allow access to your photo library" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* settingAction = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if (&UIApplicationOpenSettingsURLString != NULL) {
            
            [fusumaCameraViewController dismissViewControllerAnimated:YES completion:nil];
            
            NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:appSettings];
        }
        
    }];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        //    [fusumaCameraViewController dismissViewControllerAnimated:YES completion:nil];
        
    }];

    
    [alertController addAction:settingAction];
    [alertController addAction:cancelAction];
    
    UINavigationController *navigationController = (UINavigationController*)self.presentedViewController;
    [navigationController presentViewController:alertController animated:YES completion:nil];
   
    
}
-(void)fusumaCameraUnauthorized
{
    NSLog(@"Camera unauthorized");
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Access Requested" message:@"Please click OK to allow access to your camera" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* settingAction = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if (&UIApplicationOpenSettingsURLString != NULL) {
            
            [fusumaCameraViewController dismissViewControllerAnimated:YES completion:nil];
            
            NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:appSettings];
        }
        
    }];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
       // [fusumaCameraViewController dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    
    [alertController addAction:settingAction];
    [alertController addAction:cancelAction];
    
    UINavigationController *navigationController = (UINavigationController*)self.presentedViewController;
    [navigationController presentViewController:alertController animated:YES completion:nil];

    
}


-(void)fusumaClosed
{
    
    NSLog(@"Called when the close button is pressed");
  
}

#pragma mark: AdobeUXImageEditorViewControllerDelegate Protocol

- (void)photoEditor:(AdobeUXImageEditorViewController *)editor finishedWithImage:(UIImage *)image
{
    // Handle the result image here
    NSLog(@"finished with image");
    
    
    //[self.navigationController pushViewController:controller animated:YES];
    
    SharePhotoViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SharePhotoViewController"];
    controller.selectedImageForShare=image;
    controller.isForEdit=NO;
    controller.isForShare=NO;
    //[self presentViewController:controller animated:NO completion:nil];
   
    
    UINavigationController *navigationController = (UINavigationController*)self.presentedViewController;
    [navigationController pushViewController:controller animated:YES];
    
    
//    [self dismissViewControllerAnimated:NO completion:^{
//      
//
//    }];
//    
}

- (void)photoEditorCanceled:(AdobeUXImageEditorViewController *)editor
{
    // Handle cancellation here
    UINavigationController *navigationController = (UINavigationController*)self.presentedViewController;
    [navigationController popViewControllerAnimated:YES];
    
    //[self.navigationController popViewControllerAnimated:YES];
    //[self dismissViewControllerAnimated:YES completion:nil];
}

-(void)closeTabbar
{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}


-(void)logOutFromTabbar
{
    
    [UserAccount sharedManager].userId=0;
    [UserAccount sharedManager].userFirstName=@"Guest";
    [UserAccount sharedManager].userLastName=@"";
    [UserAccount sharedManager].accessToken=@"";
    
    [UserAccount sharedManager].userImageName=@"/img/profile-holder.png";
    //
    AppDelegate *appDelegateTemp = [[UIApplication sharedApplication]delegate];
    
    UIViewController* rootController = [self.storyboard  instantiateViewControllerWithIdentifier:@"LogInViewController"];
    
    UINavigationController* navigation = [[UINavigationController alloc] initWithRootViewController:rootController];
    
    navigation.navigationBarHidden=YES;
    appDelegateTemp.window.rootViewController = navigation;
    
//    LogInViewController *viewController=[self.storyboard instantiateViewControllerWithIdentifier:@"LogInViewController"];
//    
//    [self.navigationController pushViewController:viewController animated:NO];
    
    
    //
    //   // [self.presentingViewController  dismissViewControllerAnimated:NO completion:nil];
    
   // NSLog(@"%@ %@",self.navigationController,self.tabBarController);
    // [self.tabBarController.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Pusher Delegate Connection
//////////////////////////////////

- (void)pusher:(PTPusher *)pusher connectionDidConnect:(PTPusherConnection *)connection
{
    NSLog(@"[Pusher] connected to %@", [connection.URL absoluteString]);
}

- (void)pusher:(PTPusher *)pusher connection:(PTPusherConnection *)connection failedWithError:(NSError *)error
{
    if (error) {
        NSLog(@"[Pusher] connection failed: %@", [error localizedDescription]);
    } else {
        NSLog(@"[Pusher] connection failed");
    }
}

- (void)pusher:(PTPusher *)pusher connection:(PTPusherConnection *)connection didDisconnectWithError:(NSError *)error willAttemptReconnect:(BOOL)reconnect
{
    if (error) {
        NSLog(@"[Pusher] didDisconnectWithError: %@ willAttemptReconnect: %@", [error localizedDescription], (reconnect ? @"YES" : @"NO"));
    } else {
        NSLog(@"[Pusher] disconnected");
    }
}

-(void)dealloc{
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
