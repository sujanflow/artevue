//
//  TabBarViewController.h
//  Artegram
//
//  Created by Tanvir Palash on 6/10/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SharePhotoViewController.h"
#import "LogInViewController.h"

#import "UIImageView+WebCache.h"


#import "ArteVue-Swift.h"

#import <AdobeCreativeSDKCore/AdobeCreativeSDKCore.h>
#import <AdobeCreativeSDKImage/AdobeCreativeSDKImage.h>

#import "Pusher.h"

@interface TabBarViewController : UITabBarController<UITabBarControllerDelegate,FusumaDelegate,AdobeUXImageEditorViewControllerDelegate,PTPusherDelegate>

-(void)closeTabbar;
-(void)logOutFromTabbar;


@end
