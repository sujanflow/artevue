//
//  GetUserNameViewController.h
//  Artegrams
//
//  Created by Sujan on 12/18/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"


@interface GetUserNameViewController : UIViewController<UITextFieldDelegate>


@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;

@property id faceBookResult;

@property (weak, nonatomic) IBOutlet UIButton *joinButton;

@end
