//
//  TutorialViewController.m
//  Artegram
//
//  Created by Tanvir Palash on 6/9/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "TutorialViewController.h"

#import "SignUpViewController.h"
#import "LogInViewController.h"

@interface TutorialViewController ()
{
    NSMutableArray* imageNameArray;
    NSInteger pageSelected;
    CGFloat index;
    int page;
}

@end

@implementation TutorialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self loadTutorials];
}

#pragma mark - scrollview configuration

- (void)loadScrollView
{
    NSInteger pageCount = imageNameArray.count;
    pageSelected = 0;
 
    //[self.imageScrollView setBackgroundColor:[UIColor lightGrayColor]];
    [self.imageScrollView setPagingEnabled:YES];
    [self.imageScrollView setBounces:NO];
    [self.imageScrollView setScrollEnabled:YES];
    [self.imageScrollView setShowsHorizontalScrollIndicator:NO];
    [self.imageScrollView setShowsVerticalScrollIndicator:NO];
    [self.imageScrollView setDelegate:self];
   // [self.imageScrollView setContentSize:CGSizeMake(([UIScreen mainScreen].bounds.size.width-60)*pageCount+40+20, [UIScreen mainScreen].bounds.size.height-49)];
    [self.imageScrollView setContentSize:CGSizeMake(([UIScreen mainScreen].bounds.size.width)*pageCount, [UIScreen mainScreen].bounds.size.height-49)];
    
    
    [self.view layoutIfNeeded];
    self.pageControl.numberOfPages = pageCount;
    self.pageControl.currentPage = 0;
    [self.view bringSubviewToFront:self.pageControl];
    
}

- (void)loadTutorials
{
    
    CGFloat left = 0;
    
    if (imageNameArray.count) {
        
    }
    else
    {
        imageNameArray= [[NSMutableArray alloc] init];
        [imageNameArray addObject:@"tutorial1"];
        [imageNameArray addObject:@"tutorial2"];
        [imageNameArray addObject:@"tutorial3"];
        [imageNameArray addObject:@"tutorial4"];
        [imageNameArray addObject:@"tutorial5"];
        [imageNameArray addObject:@"tutorial6"];
        
        
        for (int i=0; i<imageNameArray.count; i++) {
            
            
             UIImageView* imageView=[[UIImageView alloc] initWithFrame:CGRectMake(left,0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-49)];
            
            //imageView.image=[UIImage imageNamed:[imageNameArray objectAtIndex:i]];
            NSString * fileName= [ [ NSBundle mainBundle] pathForResource:[imageNameArray objectAtIndex:i] ofType:@"png"];
            imageView.image=[UIImage imageWithContentsOfFile:fileName];
            
            [self.imageScrollView addSubview:imageView];
            
            left += [UIScreen mainScreen].bounds.size.width;
        }
        
    }
    
    [self loadScrollView];
    
}

/*
- (void)loadTutorials
{
    
    CGFloat left = 40;
    
    if (imageNameArray.count) {
        
    }
    else
    {
        imageNameArray= [[NSMutableArray alloc] init];
        [imageNameArray addObject:@"tutorial1"];
        [imageNameArray addObject:@"tutorial2"];
        [imageNameArray addObject:@"tutorial3"];
        [imageNameArray addObject:@"tutorial4"];
        [imageNameArray addObject:@"tutorial5"];
        [imageNameArray addObject:@"tutorial6"];
        
        
        for (int i=0; i<imageNameArray.count; i++) {
            
            UIView* imageContainerView=[[UIImageView alloc] initWithFrame:CGRectMake(left,40, [UIScreen mainScreen].bounds.size.width-80, [UIScreen mainScreen].bounds.size.height-139)];
            
            UIImageView* imageView=[[UIImageView alloc] initWithFrame:CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width-80, [UIScreen mainScreen].bounds.size.height-139)];
            
            //imageView.image=[UIImage imageNamed:[imageNameArray objectAtIndex:i]];
            NSString * fileName= [ [ NSBundle mainBundle] pathForResource:[imageNameArray objectAtIndex:i] ofType:@"png"];
            imageView.image=[UIImage imageWithContentsOfFile:fileName];
            
            imageView.layer.masksToBounds=YES;
            imageView.layer.cornerRadius=5;
            
            imageContainerView.backgroundColor = [UIColor lightGrayColor];
            imageContainerView.layer.cornerRadius=5;
            imageContainerView.layer.masksToBounds = NO;
            imageContainerView.layer.shadowOffset = CGSizeMake(0, 0);
            imageContainerView.layer.shadowRadius = 15;
            imageContainerView.layer.shadowOpacity = 0.3;
            
            
            
            [imageContainerView addSubview:imageView];
            [self.imageScrollView addSubview:imageContainerView];
            
            left += ([UIScreen mainScreen].bounds.size.width - 60);
        }
        
    }
    
    [self loadScrollView];
    
}
*/

#pragma mark - scrollview delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
  //  NSLog(@"self.imageScrollView.contentOffset.x %lf",self.imageScrollView.contentOffset.x);
    NSInteger pageIndex = self.imageScrollView.contentOffset.x /( [UIScreen mainScreen].bounds.size.width);
    
    self.pageControl.currentPage = pageIndex;
}


//- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView
//                     withVelocity:(CGPoint)velocity
//              targetContentOffset:(inout CGPoint *)targetContentOffset
//{
//    CGFloat pageWidth = ( [UIScreen mainScreen].bounds.size.width-60);
//    page = (floor((self.imageScrollView.contentOffset.x - (pageWidth/2)) / pageWidth) + 1);
//    
//    index = targetContentOffset->x/pageWidth;
//    
//    if(velocity.x > 0)
//        index = ceil(index);
//    else if(velocity.x < 0)
//        index = floor(index);
//    else
//        index = round(index);
//    
//   // NSLog(@"index %lf %d",index, page);
//    
//  
//    [self.imageScrollView setContentOffset:CGPointMake(page*pageWidth,0) animated:YES];
//    
//    
//    
//    //targetContentOffset->x = index * pageWidth;
//}
//
//-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
//{
//    CGFloat pageWidth = ( [UIScreen mainScreen].bounds.size.width-60);
//
//    if(index<page)
//    {
//        //left
//        [self.imageScrollView setContentOffset:CGPointMake((page-1)*pageWidth,0) animated:YES];
//        
//        
//    }
//    else if(index>page)
//    {
//        [self.imageScrollView setContentOffset:CGPointMake((page+1)*pageWidth,0) animated:YES];
//        
//    }
////    CGFloat pageWidth = ( [UIScreen mainScreen].bounds.size.width-60);
////    int page = (floor((self.imageScrollView.contentOffset.x - (pageWidth/2)) / pageWidth) + 1);
////    
//   // NSLog(@"scrollViewDidEndDecelerating %d",page);
////    [self.imageScrollView setContentOffset:CGPointMake(page*pageWidth,0) animated:YES];
//}



#pragma mark - Button Actions
- (IBAction)signUpButtonAction:(id)sender {
    
    [self.imageScrollView removeFromSuperview];
    [imageNameArray removeAllObjects ];
    
    
    SignUpViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)loginButtonAction:(id)sender {
    
    [self.imageScrollView removeFromSuperview];
    [imageNameArray removeAllObjects ];
    
    
    LogInViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"LogInViewController"];
    
    [self.navigationController pushViewController:controller animated:YES];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



@end
