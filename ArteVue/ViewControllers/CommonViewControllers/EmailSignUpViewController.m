//
//  EmailSignUpViewController.m
//  Artegram
//
//  Created by Sujan on 6/7/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "EmailSignUpViewController.h"
#import "LogInViewController.h"
#import "JoinArtegramsViewController.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "ServerManager.h"

@interface EmailSignUpViewController ()
{
    CGRect popupGoToFrame,popupGoFromFrame;
    UIView *termConditionView;

}

@end

@implementation EmailSignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // set color for placeholder
    UIColor *color = [UIColor whiteColor];
    self.emailTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: color}];
    self.userNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: color}];
    self.passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
    self.firstNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"First name" attributes:@{NSForegroundColorAttributeName: color}];
    self.lastNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Last name" attributes:@{NSForegroundColorAttributeName: color}];
    
    // Border in Button
    self.nextButton.layer.borderWidth = 0.5f;
    self.nextButton.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    
    //set delegate
    self.emailTextField.delegate = self;
    self.userNameTextField.delegate = self;
    self.passwordTextField.delegate = self;
    self.firstNameTextField.delegate = self;
    self.lastNameTextField.delegate = self;
    
    //set action
    
    [self.emailTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingDidEnd];
    [self.userNameTextField addTarget:self action:@selector(textFieldDidChangeInUserName:) forControlEvents:UIControlEventEditingDidEnd];
    [self.passwordTextField addTarget:self action:@selector(textFieldDidChangeInPassword:) forControlEvents:UIControlEventEditingDidEnd];

    
    //To hide keyBoard
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    
    [self.scrollView addGestureRecognizer:tapGesture];
    
    //move textField when keyboard show
    [self registerForKeyboardNotifications];
    
    self.termsLabel.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    self.termsLabel.delegate = self;
    NSRange range = [self.termsLabel.text rangeOfString:@"terms"];
    
    NSLog(@"%@",self.termsLabel.text);
    
    [self.termsLabel addLinkToURL:[NSURL URLWithString:@"terms"] withRange:range]; // Embedding a custom link in a substring
    
    [self createTermsWebview];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textFieldDidChange:(id)sender
{
    if (self.emailTextField.text.length > 0) {
      if (![self NSStringIsValidEmail:self.emailTextField.text]) {
        
        
          UIAlertController * alertController=   [UIAlertController
                                                  alertControllerWithTitle:nil
                                                  message:@"Please enter a valid email address"
                                                  preferredStyle:UIAlertControllerStyleAlert];
          
          UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         
                                                         
                                                     }];
         
          
          [alertController addAction:ok];
          
          [self presentViewController:alertController animated:YES completion:nil];
        
        
      }
        
        [self verifyEmail];
    }
}

- (void)textFieldDidChangeInUserName:(id)sender
{
   if (self.userNameTextField.text.length > 0) {
       
     if (self.userNameTextField.text.length > 16 ) {
        
      
         
         UIAlertController * alertController=   [UIAlertController
                                                 alertControllerWithTitle:nil
                                                 message:@"Username can't more than 16 characters"
                                                 preferredStyle:UIAlertControllerStyleAlert];
         
         UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action) {
                                                        
                                                        
                                                    }];
         
         
         [alertController addAction:ok];
         
         [self presentViewController:alertController animated:YES completion:nil];
        
     }
     else if (![self NSStringIsValidUsername:self.userNameTextField.text])
     {

         UIAlertController * alertController=   [UIAlertController
                                                 alertControllerWithTitle:nil
                                                 message:@"Username can contain only letters, numbers, full stops and underscores"
                                                 preferredStyle:UIAlertControllerStyleAlert];
         
         UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action) {
                                                        
                                                        
                                                    }];
         
         
         [alertController addAction:ok];
         
         [self presentViewController:alertController animated:YES completion:nil];
     }
       
       [self verifyUserName];
   }
}


- (void)textFieldDidChangeInPassword:(id)sender
{
    if (self.passwordTextField.text.length > 0) {
        
        NSString *str = self.passwordTextField.text;
        if (str.length < 8) {
            

            
            UIAlertController * alertController=   [UIAlertController
                                                    alertControllerWithTitle:nil
                                                    message:@"Passwords must be between 8 and 16 characters long"
                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           
                                                       }];
            
            
            [alertController addAction:ok];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
            
        }else if (!([str rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]].location != NSNotFound))
        {
        


            UIAlertController * alertController=   [UIAlertController
                                                    alertControllerWithTitle:nil
                                                    message:@"Passwords must contain at least one number"
                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           
                                                       }];
            
            
            [alertController addAction:ok];
            
            [self presentViewController:alertController animated:YES completion:nil];
        
        }
    }
}

-(void)verifyUserName
{
    
    [[ServerManager sharedManager] checkUserName:self.userNameTextField.text WithCompletion:^(BOOL success) {
        
        if (success) {
            
          
            
            UIAlertController * alertController=   [UIAlertController
                                                    alertControllerWithTitle:nil
                                                    message:@"This username is already in use on ArteVue. Please use another username"
                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           
                                                       }];
            
            
            [alertController addAction:ok];
            
            [self presentViewController:alertController animated:YES completion:nil];

        }
        else{
           
            NSLog(@"username is unique");
            
        }
        
        
    }];
    
    
}

-(void)verifyEmail
{
    
    [[ServerManager sharedManager] checkEmail:self.emailTextField.text WithCompletion:^(BOOL success) {
        
        if (success) {
          
            
            UIAlertController * alertController=   [UIAlertController
                                                    alertControllerWithTitle:nil
                                                    message:@"This email address is already in use on ArteVue. Please use another email address"
                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           
                                                       }];
            
            
            [alertController addAction:ok];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
        }
        else{
           
           
            NSLog(@"Email is not in use");
            
            
        }
        
        
    }];
 
}

//// method to hide keyboard when user taps on a scrollview
-(void)hideKeyboard
{
    [self.emailTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.userNameTextField resignFirstResponder];
    [self.firstNameTextField resignFirstResponder];
    [self.lastNameTextField resignFirstResponder];
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self.emailTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.userNameTextField resignFirstResponder];
    [self.firstNameTextField resignFirstResponder];
    [self.lastNameTextField resignFirstResponder];
    
    return YES;
}

-(BOOL) NSStringIsValidEmail:(NSString *) checkString{
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
    
}

-(BOOL) NSStringIsValidUsername:(NSString *) checkString{
    
    NSString *userNameRegex = @"[A-Za-z0-9._]*";
    NSPredicate *userNameTextTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", userNameRegex];
    
    NSLog(@"username  %hhd",[userNameTextTest evaluateWithObject:checkString]);
    
    return [userNameTextTest evaluateWithObject:checkString];
    
    
    
}

- (void)registerForKeyboardNotifications{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWasShown:)
     
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillBeHidden:)
     
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}



// Called when the UIKeyboardDidShowNotification is sent.

- (void)keyboardWasShown:(NSNotification*)aNotification{
    
    NSDictionary* info = [aNotification userInfo];
    
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    
    self.scrollView.contentInset = contentInsets;
    
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    
    // Your app might not need or want this behavior.
    
    CGRect aRect = self.view.frame;
    
    aRect.size.height -= kbSize.height;
    
    if (!CGRectContainsPoint(aRect, self.emailTextField.frame.origin) ) {
        
        [self.scrollView scrollRectToVisible:self.emailTextField.frame animated:YES];
        
    }else if (!CGRectContainsPoint(aRect, self.passwordTextField.frame.origin) ) {
        
        [self.scrollView scrollRectToVisible:self.passwordTextField.frame animated:YES];
        
    }else if (!CGRectContainsPoint(aRect, self.userNameTextField.frame.origin) ) {
        
        [self.scrollView scrollRectToVisible:self.userNameTextField.frame animated:YES];
        
    }else if (!CGRectContainsPoint(aRect, self.firstNameTextField.frame.origin) ) {
        
        [self.scrollView scrollRectToVisible:self.firstNameTextField.frame animated:YES];
        
    }else if (!CGRectContainsPoint(aRect, self.lastNameTextField.frame.origin) ) {
        
        [self.scrollView scrollRectToVisible:self.lastNameTextField.frame animated:YES];
        
    }
    
}



// Called when the UIKeyboardWillHideNotification is sent

- (void)keyboardWillBeHidden:(NSNotification*)aNotification{
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    self.scrollView.contentInset = contentInsets;
    
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
}



- (IBAction)nextButtonAction:(id)sender {
    
    
    if(self.emailTextField.text.length && self.userNameTextField.text.length && self.passwordTextField.text.length && self.firstNameTextField.text.length && self.lastNameTextField.text.length)
    {
        NSMutableDictionary* postData=[[NSMutableDictionary alloc] init];
        
        [postData setObject:self.emailTextField.text forKey:@"email"];
        [postData setObject:self.passwordTextField.text forKey:@"password"];
        [postData setObject:self.userNameTextField.text forKey:@"username"];
        [postData setObject:self.firstNameTextField.text forKey:@"first_name"];
        [postData setObject:self.lastNameTextField.text forKey:@"last_name"];
        
        JoinArtegramsViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"JoinArtegramsViewController"];
        controller.signUpUser=[[NSMutableDictionary alloc] initWithDictionary:postData];
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    else
    {
        

        
        UIAlertController * alertController=   [UIAlertController
                                                alertControllerWithTitle:nil
                                                message:@"Please enter all the details"
                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       
                                                       
                                                   }];
        
        
        [alertController addAction:ok];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
        
           }
    
    
    
   
}




- (IBAction)signInButtonAction:(id)sender {
    
//    AppDelegate *appDelegateTemp = [[UIApplication sharedApplication] delegate];
//    
//    UIViewController* rootController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"LogInViewController"];
//    
//    UINavigationController* navigation = [[UINavigationController alloc] initWithRootViewController:rootController];
//    navigation.navigationBarHidden=YES;
//    appDelegateTemp.window.rootViewController = navigation;
    
    [self.navigationController popToRootViewControllerAnimated:YES];
   
}
- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithTextCheckingResult:(NSTextCheckingResult *)result {
    //do whatever you need
    NSLog(@"result %@",result.URL);
    if ([result.URL isEqual:[NSURL URLWithString:@"terms"]]) {
        [self openPopupAnimation];
    }
    
}

-(void) createTermsWebview
{
    popupGoToFrame = CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height);
    popupGoFromFrame = CGRectMake(0, SCREEN_SIZE.height, SCREEN_SIZE.width, SCREEN_SIZE.height);
    
    
    termConditionView = [[UIView alloc] initWithFrame:popupGoFromFrame];
    [termConditionView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
    
    [self.view addSubview:termConditionView];
    
    
    UIView *bgImageview = [[UIView alloc] initWithFrame:CGRectMake(15, IPHONE_5?92:48, SCREEN_SIZE.width-30,  SCREEN_SIZE.height-200)];
    
    //[bgImageview setImage:[UIImage imageNamed:@"terms_use_popup.png"]];
    
    bgImageview.layer.cornerRadius=3;
    bgImageview.layer.borderColor=[UIColor blackColor].CGColor;
    bgImageview.backgroundColor=[UIColor whiteColor];
    
    [termConditionView addSubview:bgImageview];
    
    UIWebView *showView = [[UIWebView alloc] initWithFrame:CGRectMake(2, 40, bgImageview.frame.size.width-4
                                                                      , bgImageview.frame.size.height-45)];
    
    [showView setUserInteractionEnabled:YES];
    [showView setDelegate:self];
    showView.scalesPageToFit = YES;
    
    [showView setBackgroundColor:[UIColor clearColor]];
    [bgImageview addSubview:showView];
    
    
    NSError *error = nil;
    NSString *html = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"artevue_t&c" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    [showView loadHTMLString:html baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
    
    NSLog(@"error %@",error);
    
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(showView.frame.origin.x+15, -5 ,150, 50)];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    titleLabel.numberOfLines = 0;
    [titleLabel setText:@"Terms of use"];
    [titleLabel setFont:[UIFont fontWithName:@"AzoSans-Regular" size:18.0f]];
    [bgImageview addSubview:titleLabel];
    
    UIButton *cancleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [cancleButton setBackgroundColor:[UIColor blackColor]];
    [cancleButton setImage:[UIImage imageNamed:@"blackCloseButton"] forState:UIControlStateNormal];
    [cancleButton setFrame:CGRectMake(bgImageview.frame.size.width - 60,-5, 50, 50)];
    [cancleButton addTarget:self
                     action:@selector(cancleAction:)
           forControlEvents:UIControlEventTouchUpInside];
    [bgImageview addSubview:cancleButton];
    
    
}



- (void)webViewDidFinishLoad:(UIWebView *)webView{
    NSLog(@"WebView Finish Loading.");
    
    
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    NSLog(@"WebView start Loading.");
    
}
-(IBAction)cancleAction:(id)sender{
    [self closePopupAnimation];
}

-(void)openPopupAnimation{
    [UIView animateWithDuration:0.6f animations:^{
        [termConditionView setFrame:popupGoToFrame];
    } completion:^(BOOL finished) {
    }];
    
}

-(void)closePopupAnimation{
    [UIView animateWithDuration:0.6f animations:^{
        [termConditionView setFrame:popupGoFromFrame];
    } completion:^(BOOL finished) {
    }];
}


@end
