//
//  LoaderViewController.h
//  Artegram
//
//  Created by Tanvir Palash on 6/5/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoaderViewController : UIViewController<UIApplicationDelegate>


@property (weak,nonatomic) IBOutlet UIImageView *animationImageView;
@property (strong,nonatomic) NSMutableArray *images;

@end
