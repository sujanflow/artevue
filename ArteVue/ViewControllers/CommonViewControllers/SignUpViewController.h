//
//  SignUpViewController.h
//  Artegram
//
//  Created by Sujan on 6/6/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"

#import "OAuthConsumer.h"

#import  <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface SignUpViewController : UIViewController<TTTAttributedLabelDelegate,UIWebViewDelegate,FBSDKLoginButtonDelegate>

@property (weak, nonatomic) IBOutlet TTTAttributedLabel *disclaimerLabel;
@property (weak, nonatomic) IBOutlet FBSDKLoginButton *loginButton;

@property (strong, nonatomic)  UILabel* titleLabel;

@end
