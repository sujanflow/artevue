//
//  ForGotPasswordViewController.m
//  Artegrams
//
//  Created by Sujan on 8/3/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "ForGotPasswordViewController.h"
#import "Constants.h"
#import "ServerManager.h"

@interface ForGotPasswordViewController ()

@end

@implementation ForGotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.sendPasswordButton.layer.borderWidth = 0.5f;
    self.sendPasswordButton.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    
    self.emailTextField.delegate = self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    
    [self.emailTextField resignFirstResponder];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self.emailTextField resignFirstResponder];
    
    return YES;
}



- (IBAction)doneButtonAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)sendPasswordButtonAction:(id)sender {
    
    [self.emailTextField resignFirstResponder];
    
    
    if (self.emailTextField.text.length == 0) {
        
        
        UIAlertController * alertController=   [UIAlertController
                                                alertControllerWithTitle:@"Please provide your email address"
                                                message:@""
                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       
                                                       
                                                   }];
        
        
        [alertController addAction:ok];
        
        [self.navigationController presentViewController:alertController animated:YES completion:nil];

        
        
    }else{
        
        [[ServerManager sharedManager] getForgetPasswordForEmail:self.emailTextField.text withCompletion:^(BOOL success, NSMutableDictionary *resultDataArray) {
            
            if ( resultDataArray!=nil) {
                
                [self dismissViewControllerAnimated:YES completion:nil];
                
                
                
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    UIAlertController * alertController=   [UIAlertController
                                                            alertControllerWithTitle:@"This email address is currently not registered on ArteVue. Please use a registered email address"
                                                            message:@""
                                                            preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action) {
                                                                   
                                                                   
                                                               }];
                    
                    
                    [alertController addAction:ok];
                    
                    [self presentViewController:alertController animated:YES completion:nil];
                    
                    
                });
                
            }
            
        }];

    }
}

@end
