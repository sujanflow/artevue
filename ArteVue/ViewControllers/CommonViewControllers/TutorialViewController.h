//
//  TutorialViewController.h
//  Artegram
//
//  Created by Tanvir Palash on 6/9/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TAPageControl.h"



@interface TutorialViewController : UIViewController<UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *imageScrollView;
@property (weak, nonatomic) IBOutlet TAPageControl *pageControl;


@end
