//
//  DGActivityIndicatorView.m
//  DGActivityIndicatorExample
//
//  Created by Danil Gontovnik on 5/23/15.
//  Copyright (c) 2015 Danil Gontovnik. All rights reserved.
//

#import "DGActivityIndicatorView.h"

#import "DGActivityIndicatorNineDotsAnimation.h"
#import "DGActivityIndicatorTriplePulseAnimation.h"
#import "DGActivityIndicatorFiveDotsAnimation.h"
#import "DGActivityIndicatorRotatingSquaresAnimation.h"
#import "DGActivityIndicatorDoubleBounceAnimation.h"
#import "DGActivityIndicatorTwoDotsAnimation.h"
#import "DGActivityIndicatorThreeDotsAnimation.h"
#import "DGActivityIndicatorBallPulseAnimation.h"
#import "DGActivityIndicatorBallClipRotateAnimation.h"
#import "DGActivityIndicatorBallClipRotatePulseAnimation.h"
#import "DGActivityIndicatorBallClipRotateMultipleAnimation.h"
#import "DGActivityIndicatorBallRotateAnimation.h"
#import "DGActivityIndicatorBallZigZagAnimation.h"
#import "DGActivityIndicatorBallZigZagDeflectAnimation.h"
#import "DGActivityIndicatorBallTrianglePathAnimation.h"
#import "DGActivityIndicatorBallScaleAnimation.h"
#import "DGActivityIndicatorLineScaleAnimation.h"
#import "DGActivityIndicatorLineScalePartyAnimation.h"
#import "DGActivityIndicatorBallScaleMultipleAnimation.h"
#import "DGActivityIndicatorBallPulseSyncAnimation.h"
#import "DGActivityIndicatorBallBeatAnimation.h"
#import "DGActivityIndicatorLineScalePulseOutAnimation.h"
#import "DGActivityIndicatorLineScalePulseOutRapidAnimation.h"
#import "DGActivityIndicatorBallScaleRippleAnimation.h"
#import "DGActivityIndicatorBallScaleRippleMultipleAnimation.h"
#import "DGActivityIndicatorTriangleSkewSpinAnimation.h"
#import "DGActivityIndicatorBallGridBeatAnimation.h"
#import "DGActivityIndicatorBallGridPulseAnimation.h"
#import "DGActivityIndicatorRotatingSandglassAnimation.h"
#import "DGActivityIndicatorRotatingTrigonAnimation.h"
#import "DGActivityIndicatorTripleRingsAnimation.h"
#import "DGActivityIndicatorCookieTerminatorAnimation.h"

static const CGFloat kDGActivityIndicatorDefaultSize = 45.0f;

@implementation DGActivityIndicatorView

#pragma mark -
#pragma mark Constructors

- (id)initWithType:(DGActivityIndicatorAnimationType)type {
    return [self initWithType:type tintColor:[UIColor whiteColor] size:kDGActivityIndicatorDefaultSize];
}

- (id)initWithType:(DGActivityIndicatorAnimationType)type tintColor:(UIColor *)tintColor {
   
   
    return [self initWithType:type tintColor:tintColor size:kDGActivityIndicatorDefaultSize];
}

- (id)initWithType:(DGActivityIndicatorAnimationType)type tintColor:(UIColor *)tintColor size:(CGFloat)size {
    self = [super init];
    if (self) {
        self.hidden=YES;
        
        _type = type;
        _size = size;
        _tintColor = tintColor;
    }
    return self;
}

#pragma mark -
#pragma mark Methods

- (void)setupAnimation {
    self.layer.sublayers = nil;
  //  self.backgroundColor=[UIColor colorWithWhite:1.0 alpha:0.9f];
  //  self.layer.cornerRadius=self.bounds.size.width/2;
    
    id<DGActivityIndicatorAnimationProtocol> animation = [DGActivityIndicatorView activityIndicatorAnimationForAnimationType:_type];
    
    if ([animation respondsToSelector:@selector(setupAnimationInLayer:withSize:tintColor:)]) {
        [animation setupAnimationInLayer:self.layer withSize:CGSizeMake(_size, _size) tintColor:_tintColor];
        self.layer.speed = 0.0f;
    }
}

- (void)startAnimatingInGallery {
    if (!self.layer.sublayers) {
        
        [self setupAnimation];
    }
    self.hidden=NO;
    self.superview.userInteractionEnabled=NO;
    self.layer.speed = 1.0f;
    _animating = YES;
    self.backgroundColor=[UIColor clearColor];
    
    [self.superview bringSubviewToFront:self];
}


- (void)startAnimating {
    if (!self.layer.sublayers) {
        
        [self setupAnimation];
    }
    self.hidden=NO;
    self.superview.userInteractionEnabled=NO;
    self.layer.speed = 1.0f;
    _animating = YES;
    
   // self.tintColor=[self GetCurrentPixelColorAtPoint:self.superview.center];
    
//    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
//    self.blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
//    self.blurEffectView.frame = self.superview.bounds;
//    self.blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//    
//    [self.superview addSubview:self.blurEffectView];
    [self.superview bringSubviewToFront:self];
}

- (void)startAnimatingWithInteraction {
    if (!self.layer.sublayers) {
        
        [self setupAnimation];
    }
    self.hidden=NO;
    self.superview.userInteractionEnabled=YES;
    self.layer.speed = 1.0f;
    _animating = YES;
    
    [self.superview bringSubviewToFront:self];
}

- (void)stopAnimating {
    self.superview.userInteractionEnabled=YES;
   // [self.blurEffectView removeFromSuperview];
    self.layer.speed = 0.0f;
    _animating = NO;
    self.hidden=YES;
}


- (UIColor *) GetCurrentPixelColorAtPoint:(CGPoint)point
{
    unsigned char pixel[4] = {0};
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGContextRef context = CGBitmapContextCreate(pixel, 1, 1, 8, 4, colorSpace, kCGBitmapAlphaInfoMask & kCGImageAlphaPremultipliedLast);
    
    CGContextTranslateCTM(context, -point.x, -point.y);
    
    [self.superview.layer renderInContext:context];
    
    CGContextRelease(context);
    
    CGColorSpaceRelease(colorSpace);
    
    NSLog(@"pixel: %d %d %d %d", pixel[0], pixel[1], pixel[2], pixel[3]);
    
    //UIColor *color = [UIColor colorWithRed:pixel[0]/255.0 green:pixel[1]/255.0 blue:pixel[2]/255.0 alpha:pixel[3]/255.0];
    
    
    if ((pixel[0]*0.299 + pixel[1]*0.587 + pixel[2]*0.114)>186)
    {
        return [UIColor blackColor];
    }
    else
        return [UIColor whiteColor];
    
   
}
#pragma mark -
#pragma mark Setters

- (void)setType:(DGActivityIndicatorAnimationType)type {
    if (_type != type) {
        _type = type;
        
        [self setupAnimation];
    }
}

- (void)setSize:(CGFloat)size {
    if (_size != size) {
        _size = size;
        
        [self setupAnimation];
    }
}

- (void)setTintColor:(UIColor *)tintColor {
    if (![_tintColor isEqual:tintColor]) {
        _tintColor = tintColor;
        
        for (CALayer *sublayer in self.layer.sublayers) {
            sublayer.backgroundColor = tintColor.CGColor;
        }
    }
}

#pragma mark -
#pragma mark Getters

+ (id<DGActivityIndicatorAnimationProtocol>)activityIndicatorAnimationForAnimationType:(DGActivityIndicatorAnimationType)type {
    switch (type) {
        case DGActivityIndicatorAnimationTypeNineDots:
            return [[DGActivityIndicatorNineDotsAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeTriplePulse:
            return [[DGActivityIndicatorTriplePulseAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeFiveDots:
            return [[DGActivityIndicatorFiveDotsAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeRotatingSquares:
            return [[DGActivityIndicatorRotatingSquaresAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeDoubleBounce:
            return [[DGActivityIndicatorDoubleBounceAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeTwoDots:
            return [[DGActivityIndicatorTwoDotsAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeThreeDots:
            return [[DGActivityIndicatorThreeDotsAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeBallPulse:
            return [[DGActivityIndicatorBallPulseAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeBallClipRotate:
            return [[DGActivityIndicatorBallClipRotateAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeBallClipRotatePulse:
            return [[DGActivityIndicatorBallClipRotatePulseAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeBallClipRotateMultiple:
            return [[DGActivityIndicatorBallClipRotateMultipleAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeBallRotate:
            return [[DGActivityIndicatorBallRotateAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeBallZigZag:
            return [[DGActivityIndicatorBallZigZagAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeBallZigZagDeflect:
            return [[DGActivityIndicatorBallZigZagDeflectAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeBallTrianglePath:
            return [[DGActivityIndicatorBallTrianglePathAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeBallScale:
            return [[DGActivityIndicatorBallScaleAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeLineScale:
            return [[DGActivityIndicatorLineScaleAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeLineScaleParty:
            return [[DGActivityIndicatorLineScalePartyAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeBallScaleMultiple:
            return [[DGActivityIndicatorBallScaleMultipleAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeBallPulseSync:
            return [[DGActivityIndicatorBallPulseSyncAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeBallBeat:
            return [[DGActivityIndicatorBallBeatAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeLineScalePulseOut:
            return [[DGActivityIndicatorLineScalePulseOutAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeLineScalePulseOutRapid:
            return [[DGActivityIndicatorLineScalePulseOutRapidAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeBallScaleRipple:
            return [[DGActivityIndicatorBallScaleRippleAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeBallScaleRippleMultiple:
            return [[DGActivityIndicatorBallScaleRippleMultipleAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeTriangleSkewSpin:
            return [[DGActivityIndicatorTriangleSkewSpinAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeBallGridBeat:
            return [[DGActivityIndicatorBallGridBeatAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeBallGridPulse:
            return [[DGActivityIndicatorBallGridPulseAnimation alloc] init];
        case DGActivityIndicatorAnimationTypeRotatingSandglass:
            return [[DGActivityIndicatorRotatingSandglassAnimation alloc]init];
        case DGActivityIndicatorAnimationTypeRotatingTrigons:
            return [[DGActivityIndicatorRotatingTrigonAnimation alloc]init];
        case DGActivityIndicatorAnimationTypeTripleRings:
            return [[DGActivityIndicatorTripleRingsAnimation alloc]init];
        case DGActivityIndicatorAnimationTypeCookieTerminator:
            return [[DGActivityIndicatorCookieTerminatorAnimation alloc]init];
    }
    return nil;
}

@end
