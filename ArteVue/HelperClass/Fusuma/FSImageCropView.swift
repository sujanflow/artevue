//
//  FZImageCropView.swift
//  Fusuma
//
//  Created by Yuta Akizuki on 2015/11/16.
//  Copyright © 2015年 ytakzk. All rights reserved.
//

import UIKit

final class FSImageCropView: UIScrollView, UIScrollViewDelegate {
    
    var imageView = UIImageView()
    
    var imageSize: CGSize?
    
    var image: UIImage! = nil {
        
        didSet {
            
            if image != nil {
                
                if !imageView.isDescendant(of: self) {
                    self.imageView.alpha = 1.0
                    self.addSubview(imageView)
                }
                
            } else {
                
                imageView.image = nil
                return
            }
            
            let imageSize = self.imageSize ?? image.size
            
            if imageSize.width < self.frame.width || imageSize.height < self.frame.height {
                
                // The width or height of the image is smaller than the frame size
                
                if imageSize.width > imageSize.height {
                    
                    // Width > Height
                    
                    let ratio = self.frame.width / imageSize.width
                    
                    imageView.frame = CGRect(
                        origin: CGPoint.zero,
                        size: CGSize(width: self.frame.width, height: imageSize.height * ratio)
                    )
                    
                } else {
                    
                    // Width <= Height
                    
                    let ratio = self.frame.height / imageSize.height
                    
                    imageView.frame = CGRect(
                        origin: CGPoint.zero,
                        size: CGSize(width: imageSize.width * ratio, height: self.frame.size.height)
                    )
                    
                }
                
                imageView.center = self.center
                
            } else {

                // The width or height of the image is bigger than the frame size

                if imageSize.width > imageSize.height {
                    
                    // Width > Height
                    
                    let ratio = self.frame.height / imageSize.height
                    
                    imageView.frame = CGRect(
                        origin: CGPoint.zero,
                        size: CGSize(width: imageSize.width * ratio, height: self.frame.height)
                    )
                    
                } else {
                    
                    // Width <= Height

                    let ratio = self.frame.width / imageSize.width
                    
                    imageView.frame = CGRect(
                        origin: CGPoint.zero,
                        size: CGSize(width: self.frame.width, height: imageSize.height * ratio)
                    )
                }
                
                self.contentOffset = CGPoint(
                    x: imageView.center.x - self.center.x,
                    y: imageView.center.y - self.center.y
                )
            }
            
            self.contentSize = CGSize(width: imageView.frame.width + 1, height: imageView.frame.height + 1)
            
            imageView.image = image
            
            
           
            let minZoom = min(self.bounds.size.width / self.contentSize.width, self.bounds.size.height / self.contentSize.height);
            print("Minzoom", minZoom)
//            print("Bounds size ",self.bounds.size.width,(imageView.image?.size.width)!);
//            print("Bounds height ",self.bounds.size.height,(imageView.image?.size.height)!);
//            
            self.minimumZoomScale=minZoom;
            self.zoomScale = 1.0
            
//            print("self : ",self)
//            print("Imageview : ",imageView)
//            print("Image : ",image.size)
            
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)!
        
        self.backgroundColor = fusumaBackgroundColor
        self.frame.size      = CGSize.zero
        self.clipsToBounds   = true
        self.imageView.alpha = 0.0
        
        imageView.frame = CGRect(origin: CGPoint.zero, size: CGSize.zero)
        
   //     let minZoom = min(self.bounds.size.width / (self.imageSize?.width)!, self.bounds.size.height / (self.imageSize?.height)!);

        self.delegate = self
        self.maximumZoomScale = 2.0
        self.minimumZoomScale = 1.0 //changed from 1.0 for rectangular size
        self.showsHorizontalScrollIndicator = false
        self.showsVerticalScrollIndicator   = false
        self.bouncesZoom = true
        self.bounces = true
        
       
    }
    
    
    func changeScrollable(_ isScrollable: Bool) {
        
        self.isScrollEnabled = isScrollable
    }
    
    // MARK: UIScrollViewDelegate Protocol
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        
        return imageView

    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        
        let boundsSize = scrollView.bounds.size
        var contentsFrame = imageView.frame
        
        if contentsFrame.size.width < boundsSize.width {
            
            contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0
            
        } else {
            contentsFrame.origin.x = 0.0
        }
        
        if contentsFrame.size.height < boundsSize.height {
            
            contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0
        } else {
            
            contentsFrame.origin.y = 0.0
        }
        
        imageView.frame = contentsFrame
        
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        
        self.contentSize = CGSize(width: imageView.frame.width + 1, height: imageView.frame.height + 1)
    }
    
    func fitToImage()
    {
        
        
        
        let imageSize = self.imageSize ?? image.size
        
        if imageView.frame.width < self.frame.width || imageView.frame.height < self.frame.height {
            
            // The width or height of the imageview is smaller than the frame size
            
         print("fitted to small")
            self.imageView.transform = CGAffineTransform.identity
            
            // The width or height of the image is bigger than the frame size
            
            if imageSize.width > imageSize.height {
                
                // Width > Height
                
                let ratio = imageSize.width / imageSize.height
                
                print("ratio : ",ratio)
                print("Imageview : ",imageView)
                
                
                imageView.frame = CGRect(
                    origin: CGPoint.zero,
                    size: CGSize(width: self.frame.width * ratio, height: self.frame.height )
                )
                
                print("Imageview : ",imageView)
                
                self.contentSize = CGSize(width: imageView.frame.width + 1, height: imageView.frame.height + 1)
              
                
            } else {
                
                // Width <= Height
                
                let ratio = imageSize.height / imageSize.width
                
                imageView.frame = CGRect(
                    origin: CGPoint.zero,
                    size: CGSize(width: self.frame.width , height: self.frame.height * ratio)
                )
                
                self.contentSize = CGSize(width: imageView.frame.width + 1, height: imageView.frame.height + 1)
          
            }
            let minZoom = min(self.bounds.size.width / self.contentSize.width, self.bounds.size.height / self.contentSize.height);
            self.minimumZoomScale=minZoom;
           
            
        }
        else {
            
            print ("Zoomed")
            self.imageView.transform = CGAffineTransform.identity
            
            // The width or height of the image is bigger than the frame size
            
            if imageSize.width > imageSize.height {
                
                // Width > Height
                
                let ratio = imageSize.height / imageSize.width
                
                print("ratio : ",ratio)
                print("Imageview : ",imageView)
                
                
                imageView.frame = CGRect(
                    origin: CGPoint.zero,
                    size: CGSize(width: self.frame.width, height: self.frame.height * ratio)
                )
                
                print("Imageview : ",imageView)
                
                
                
            } else {
                
                // Width <= Height
                
                let ratio = imageSize.width / imageSize.height
                
                imageView.frame = CGRect(
                    origin: CGPoint.zero,
                    size: CGSize(width: self.frame.width * ratio, height: self.frame.height)
                )
                
            }
            
            //         }
            
            
            self.contentSize = CGSize(width: imageView.frame.width + 1, height: imageView.frame.height + 1)
            imageView.center=self.center

            self.minimumZoomScale = 1.0
        
        }
        
        
        print("zoom scale ", self.maximumZoomScale,self.minimumZoomScale)

        print("self : ",self)
        print("Image : ",image.size)
        
        
    }
    
}
