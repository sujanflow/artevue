//
//  uzysGroupViewCell.m
//  UzysAssetsPickerController
//
//  Created by Uzysjung on 2014. 2. 13..
//  Copyright (c) 2014년 Uzys. All rights reserved.
//
#import "UzysAssetsPickerController_Configuration.h"
#import "UzysGroupViewCell.h"
#import "UzysAppearanceConfig.h"

@implementation UzysGroupViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.textLabel.font = [UIFont fontWithName:@"AzoSans-Medium" size:16];
        self.detailTextLabel.font = [UIFont fontWithName:@"AzoSans-Medium" size:11];
        UzysAppearanceConfig *appearanceConfig = [UzysAppearanceConfig sharedConfig];
        self.accessoryView = [[UIImageView alloc] initWithImage:[UIImage Uzys_imageNamed:appearanceConfig.assetsGroupSelectedImageName]];
        self.selectedBackgroundView = nil;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    if(selected)
    {
        self.accessoryView.hidden = NO;
    }
    else
    {
        self.accessoryView.hidden = YES;
    }

}

- (void)applyData:(PHAssetCollection *)assetsCollectionGroup
{
    self.assetsGroup            = assetsCollectionGroup;
    
    //print(" count: \(fetchResult.count)")
    
    
    
    PHFetchResult *assetsFetchResult = [PHAsset fetchAssetsInAssetCollection:assetsCollectionGroup options:nil];
  //  NSLog(@"assetsFetchResult %lu",(unsigned long)assetsFetchResult.count);
    if(assetsFetchResult.count)
    {
        PHAsset *asset = [assetsFetchResult objectAtIndex:0];
        NSInteger retinaMultiplier = [UIScreen mainScreen].scale;
        CGSize retinaSquare = CGSizeMake(80 * retinaMultiplier, 80 * retinaMultiplier);
        
        PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
        options.resizeMode = PHImageRequestOptionsResizeModeExact;
        
        [[PHImageManager defaultManager] requestImageForAsset:asset targetSize:retinaSquare contentMode:PHImageContentModeAspectFill options:options resultHandler:^(UIImage *result, NSDictionary *info) {
            
            CGImageRef posterImage      = result.CGImage;
            size_t height               = CGImageGetHeight(posterImage);
            float scale                 = height / kThumbnailLength;
            
            self.imageView.image        = [UIImage imageWithCGImage:posterImage scale:scale orientation:UIImageOrientationUp];
            self.textLabel.text         = assetsCollectionGroup.localizedTitle;
            self.detailTextLabel.text   = [NSString stringWithFormat:@"%ld", assetsFetchResult.count];
            self.accessoryType          = UITableViewCellAccessoryDisclosureIndicator;
            
        }];
    }
    else
    {
        self.imageView.image        = nil;
        
        self.textLabel.text         = assetsCollectionGroup.localizedTitle;
        self.detailTextLabel.text   = [NSString stringWithFormat:@"%ld", assetsFetchResult.count];
        self.accessoryType          = UITableViewCellAccessoryDisclosureIndicator;
      
    }
    
    
    
//    [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:asset.localIdentifier] options:SDWebImageProgressiveDownload targetLocalAssetSize:retinaSquare progress:^(NSInteger receivedSize, NSInteger expectedSize) {
//        
//    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
//        if (image) {
//            albumCoverImg.image = image;
//        }
//    }];
    
    
}

//- (void)applyData:(ALAssetsGroup *)assetsGroup
//{
//    self.assetsGroup            = assetsGroup;
//    
//    CGImageRef posterImage      = assetsGroup.posterImage;
//    size_t height               = CGImageGetHeight(posterImage);
//    float scale                 = height / kThumbnailLength;
//    
//    self.imageView.image        = [UIImage imageWithCGImage:posterImage scale:scale orientation:UIImageOrientationUp];
//    self.textLabel.text         = [assetsGroup valueForProperty:ALAssetsGroupPropertyName];
//    self.detailTextLabel.text   = [NSString stringWithFormat:@"%ld", (long)[assetsGroup numberOfAssets]];
//    self.accessoryType          = UITableViewCellAccessoryDisclosureIndicator;
//}
@end
