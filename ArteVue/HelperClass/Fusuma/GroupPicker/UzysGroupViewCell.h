//
//  UzysGroupViewCell.h
//  UzysAssetsPickerController
//
//  Created by Uzysjung on 2014. 2. 13..
//  Copyright (c) 2014년 Uzys. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
#import "UzysAssetsPickerController_Configuration.h"

@interface UzysGroupViewCell : UITableViewCell
@property (nonatomic, strong) PHAssetCollection *assetsGroup;
- (void)applyData:(PHAssetCollection *)assetsGroup;
@end
