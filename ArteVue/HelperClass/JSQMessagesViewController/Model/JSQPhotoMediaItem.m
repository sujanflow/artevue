//
//  Created by Jesse Squires
//  http://www.jessesquires.com
//
//
//  Documentation
//  http://cocoadocs.org/docsets/JSQMessagesViewController
//
//
//  GitHub
//  https://github.com/jessesquires/JSQMessagesViewController
//
//
//  License
//  Copyright (c) 2014 Jesse Squires
//  Released under an MIT license: http://opensource.org/licenses/MIT
//

#import "JSQPhotoMediaItem.h"

#import "JSQMessagesMediaPlaceholderView.h"
#import "JSQMessagesMediaViewBubbleImageMasker.h"

#import <MobileCoreServices/UTCoreTypes.h>

#import <Realm/Realm.h>
#import "Posts.h"


@interface JSQPhotoMediaItem ()

@property (strong, nonatomic) RLMRealmConfiguration *realmv1Configuration;

@property (strong, nonatomic) UIImageView *cachedImageView;

@end


@implementation JSQPhotoMediaItem

#pragma mark - Initialization

- (instancetype)initWithImage:(UIImage *)image
{
    self = [super init];
    if (self) {
        _image = [image copy];
        _cachedImageView = nil;
        _postId=0;
    }
    return self;
}

- (instancetype)initWithImageUrl:(NSString *)imageUrl withPostId:(int)postId
{
    self = [super init];
    if (self) {
        
        NSURL *defaultRealmURL = [RLMRealmConfiguration defaultConfiguration].fileURL;
        NSURL *defaultRealmParentURL = [defaultRealmURL URLByDeletingLastPathComponent];
        NSURL *realmv1URL = [defaultRealmParentURL URLByAppendingPathComponent:@"defaultv1.realm"];
        
        self.realmv1Configuration = [[RLMRealmConfiguration defaultConfiguration] copy];
        self.realmv1Configuration.fileURL = realmv1URL;
       
       // [SDWebImageManager sharedManager].delegate=self;
        _postId=postId;
        _image = nil;
        _cachedImageView = nil;
        
        if(postId)
        {
            RLMRealm *realmInExplore = [RLMRealm realmWithConfiguration:self.realmv1Configuration error:nil];
            RLMResults *result=[Posts objectsInRealm:realmInExplore where:[NSString stringWithFormat:@"postId = %d",postId]];
            
            if(result.count)
            {
                Posts *post=[[Posts alloc] init];
                post=[result objectAtIndex:0];
                //NSLog(@"result %@",result);
                
                _image = [[UIImage imageWithData:post.imageData] copy];
                _cachedImageView = nil;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"updateSharedImage" object:nil userInfo:nil];
                
                
            }
            else
            {
                    SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
                    [SDWebImageDownloader sharedDownloader].maxConcurrentDownloads=1;
                    
                    [downloader downloadImageWithURL:[NSURL URLWithString:imageUrl]
                                             options:0
                                            progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                // progression tracking code
                                          //       NSLog(@"receivedSize %ld expectedSize %ld",(long)receivedSize,(long)expectedSize);
                                                
                                            }
                                           completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                               if (image && finished) {
                                                   // do something with image
                                                   
                                                   //   Posts *post = [[Posts alloc] init];
                                                   
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       //searchImage.image=image;
                                                       
                                                       
                                                       RLMRealm *realm = [RLMRealm realmWithConfiguration:self.realmv1Configuration error:nil];
                                                       
                                                       [realm beginWriteTransaction];
                                                       [Posts createOrUpdateInRealm:realm withValue:@{@"primId": [NSString stringWithFormat:@"%d",postId]
                                                                                                      ,@"postId": [NSNumber numberWithInt:postId],
                                                                                                      @"imageData": data}];
                                                       
                                                       
                                                       
                                                       [realm commitWriteTransaction];
                                                       
                                                       _image = [image copy];
                                                       _cachedImageView = nil;
                                                       [[NSNotificationCenter defaultCenter] postNotificationName:@"updateSharedImage" object:nil userInfo:nil];
                                                       
                                                   });
                                               }
                                               
                                               
                                           }];
                    
                }
                
            

        }
        else
        {
            NSLog(@"imageUrl %@",imageUrl);
            SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
            [SDWebImageDownloader sharedDownloader].maxConcurrentDownloads=1;
            
            [downloader downloadImageWithURL:[NSURL URLWithString:imageUrl]
                                     options:0
                                    progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                        // progression tracking code
                                     //   NSLog(@"receivedSize %ld expectedSize %ld",(long)receivedSize,(long)expectedSize);
                                    }
                                   completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                       if (image && finished) {
                                           // do something with image
                                           
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               _image = [image copy];
                                               _cachedImageView = nil;
                                               [[NSNotificationCenter defaultCenter] postNotificationName:@"updateSharedImage" object:nil userInfo:nil];
                                           });
                                           
                                       }
                                   }];

        }
        
    }
    
    return self;
}

- (void)clearCachedMediaViews
{
    [super clearCachedMediaViews];
    _cachedImageView = nil;
}

#pragma mark - Setters

- (void)setImage:(UIImage *)image
{
    _image = [image copy];
    _cachedImageView = nil;
}

- (void)setAppliesMediaViewMaskAsOutgoing:(BOOL)appliesMediaViewMaskAsOutgoing
{
    [super setAppliesMediaViewMaskAsOutgoing:appliesMediaViewMaskAsOutgoing];
    _cachedImageView = nil;
}

#pragma mark - JSQMessageMediaData protocol

- (UIView *)mediaView
{
    if (self.image == nil) {
        return nil;
    }
    
    if (self.cachedImageView == nil) {
        CGSize size = [self mediaViewDisplaySize];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:self.image];
        imageView.frame = CGRectMake(0.0f, 0.0f, size.width, size.height);
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        [JSQMessagesMediaViewBubbleImageMasker applyBubbleImageMaskToMediaView:imageView isOutgoing:self.appliesMediaViewMaskAsOutgoing];
        self.cachedImageView = imageView;
    }
    
    return self.cachedImageView;
}

- (NSUInteger)mediaHash
{
    return self.hash;
}

- (NSString *)mediaDataType
{
    return (NSString *)kUTTypeJPEG;
}

- (id)mediaData
{
    return UIImageJPEGRepresentation(self.image, 1);
}

#pragma mark - NSObject

- (NSUInteger)hash
{
    return super.hash ^ self.image.hash;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@: image=%@, appliesMediaViewMaskAsOutgoing=%@>",
            [self class], self.image, @(self.appliesMediaViewMaskAsOutgoing)];
}

#pragma mark - NSCoding

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _image = [aDecoder decodeObjectForKey:NSStringFromSelector(@selector(image))];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.image forKey:NSStringFromSelector(@selector(image))];
}

#pragma mark - NSCopying

- (instancetype)copyWithZone:(NSZone *)zone
{
    JSQPhotoMediaItem *copy = [[JSQPhotoMediaItem allocWithZone:zone] initWithImage:self.image];
    copy.appliesMediaViewMaskAsOutgoing = self.appliesMediaViewMaskAsOutgoing;
    return copy;
}

//-(UIImage *)imageManager:(SDWebImageManager *)imageManager transformDownloadedImage:(UIImage *)image withURL:(NSURL *)imageURL
//{
//     NSLog(@"imageURL : %@",imageURL);
//    UIImage *resizedImage;
//    //  return resizedImage  = [image resizedImage:CGSizeMake(512, 512) interpolationQuality:kCGInterpolationDefault];
//    
//    float MIN_UPLOAD_RESOLUTION=756*756;
//    float factor;
//    float resol = image.size.height*image.size.width;
//    if (resol >MIN_UPLOAD_RESOLUTION){
//        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
//        resizedImage = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
//        return resizedImage;
//        
//    }
//    else
//        return image;
//}
//
//- (UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
//    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
//    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
//    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return scaledImage;
//}

@end
