//
//  TagPeopleNameView.m
//  Artegrams
//
//  Created by Sujan on 8/21/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "TagPeopleNameView.h"

@implementation TagPeopleNameView



-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [[NSBundle mainBundle] loadNibNamed:@"TagPeopleNameView" owner:self options:nil];
        self.nameView.frame = self.bounds;
        NSLog(@"%@",self.nameView);
        [self addSubview: self.nameView];
        
    }
    
    
    return self;
}

- (IBAction)closeButtonAction:(id)sender {
    
    [self removeFromSuperview];
    
}


@end
