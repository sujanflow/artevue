//
//  TagPeopleNameView.h
//  Artegrams
//
//  Created by Sujan on 8/21/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TagPeopleNameView : UIView



@property (weak, nonatomic) IBOutlet UIView *nameView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *closeButtonWidth;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@end
