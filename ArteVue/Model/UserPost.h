//
//  Recipe.h
//  RecipeApp
//
//  Created by Simon on 25/12/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "UserPost.h"


@interface UserPost : NSObject

@property int postId;

@property UIImage* postImage;
@property float aspectRatio;
@property NSString * postDescription;

@property int userId;
@property NSString *userName;
@property NSString *userImage;
@property NSString * artistName;
@property NSString* postImageUrl;

@property float price;

@property NSString * addressTitle;
@property NSString * postTime;
@property NSMutableAttributedString *headerTitle;

@property int postCommentsCount;
@property int postLikesCount;
@property int pinCount;
 
@property BOOL is_liked;
@property BOOL is_pinned;
@property BOOL has_buy_btn;


@property NSString * googlePlaceId;
@property NSString * longitude;
@property NSString * latitude;


@end
