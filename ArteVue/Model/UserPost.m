//
//  Recipe.m
//  RecipeApp
//
//  Created by Simon on 25/12/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import "UserPost.h"
#import "Constants.h"

@implementation UserPost

#pragma mark - init
- (id)init{
    if (self = [super init]){
        
      
        // Set some defaults for the first run of the application
        
        self.postId=0;
        self.postImage=nil;
        self.aspectRatio=1.0;
        self.postDescription=@"";
        
        self.price=0;
        self.userId=0;
        self.userName=@"";
        self.addressTitle=@"";
        self.artistName=@"";
        self.postTime=@"";
        self.userImage=[NSString stringWithFormat:@"%@/%@",SERVER_BASE_API_URL,@"img/profile-holder.png"];
        
        self.postCommentsCount=0;
        self.is_liked=NO;
        self.is_pinned=NO;
        self.has_buy_btn=NO;
        self.postLikesCount=0;
        self.pinCount=0;
    
    }
    return self;
}

@end
