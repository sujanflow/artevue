//
//  Recipe.h
//  RecipeApp
//
//  Created by Simon on 25/12/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SynthesizeSingleton.h"


@interface UserAccount : NSObject

@property (nonatomic) int userId;
@property (nonatomic, strong) NSString *userFirstName;
@property (nonatomic, strong) NSString *userLastName;
@property (nonatomic, strong) NSString *userImageName;
@property (nonatomic, strong) NSString *userType;

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *userPassword;

@property (nonatomic, strong) NSString *accessToken;
@property (nonatomic, strong) NSString *refreshToken;

@property (nonatomic, strong) NSString *lastActivityDate;
@property (nonatomic, strong) NSString *lastMessageDate;

@property (nonatomic, strong) NSString *deviceToken;
@property (nonatomic, strong) NSString *gcmRegKey;

@property (nonatomic, strong) NSString *galleryTitle;
@property (nonatomic, strong) NSString *galleryDescription;


@property (nonatomic)  BOOL isNotificationEnabled;
@property (nonatomic)  BOOL isAccountPrivate;
@property (nonatomic)  BOOL isSavetoAlbum;

@property (nonatomic) int pendingActivityCount;
@property (nonatomic) int pendingMessageCount;



+ (UserAccount *)sharedManager;

-(NSMutableDictionary *) toNSDictionary;


@end
