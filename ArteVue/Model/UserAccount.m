//
//  Recipe.m
//  RecipeApp
//
//  Created by Simon on 25/12/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import "UserAccount.h"


@interface UserAccount ()

DECLARE_SINGLETON_FOR_CLASS(UserAccount)
@property (nonatomic, retain) NSUserDefaults *userDefaults;

@end


@implementation UserAccount

SYNTHESIZE_SINGLETON_FOR_CLASS(UserAccount)
@synthesize userDefaults = _userDefaults;


#pragma mark - init
- (id)init{
    if (self = [super init]){
        
        self.userDefaults = [NSUserDefaults standardUserDefaults];
        // Set some defaults for the first run of the application
    
       // self.lastMessageDate=@"";
       // self.lastActivityDate=@"";
        
        [self.userDefaults synchronize];
    }
    return self;
}

-(int)userId{
    return [[self.userDefaults stringForKey:@"userId"] intValue];
}

- (void)setUserId:(int)value
{
    [self.userDefaults setInteger:value forKey:@"userId"];
    [self.userDefaults synchronize];
}

-(int)pendingActivityCount{
    return [[self.userDefaults stringForKey:@"pendingActivityCount"] intValue];
}

- (void)setPendingActivityCount:(int)value
{
    [self.userDefaults setInteger:value forKey:@"pendingActivityCount"];
    [self.userDefaults synchronize];
}

-(int)pendingMessageCount{
    return [[self.userDefaults stringForKey:@"pendingMessageCount"] intValue];
}

- (void)setPendingMessageCount:(int)value
{
    [self.userDefaults setInteger:value forKey:@"pendingMessageCount"];
    [self.userDefaults synchronize];
}

-(NSString*) userFirstName
{
    return [self.userDefaults objectForKey:@"userFirstName"];
}

- (void)setUserFirstName:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"userFirstName"];
    [self.userDefaults synchronize];
}

-(NSString*)userLastName
{
    return [self.userDefaults objectForKey:@"userLastName"];
}

- (void)setUserLastName:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"userLastName"];
    [self.userDefaults synchronize];
}

-(NSString*) userImageName
{
    return [self.userDefaults objectForKey:@"userImageName"];
}

- (void)setUserImageName:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"userImageName"];
    [self.userDefaults synchronize];
}

-(NSString*) userType
{
    return [self.userDefaults objectForKey:@"userType"];
}

- (void)setUserType:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"userType"];
    [self.userDefaults synchronize];
}

-(NSString*)email
{
    return [self.userDefaults objectForKey:@"email"];
}

- (void)setEmail:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"email"];
    [self.userDefaults synchronize];
}

-(NSString*)userName
{
    return [self.userDefaults objectForKey:@"userName"];
}

- (void)setUserName:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"userName"];
    [self.userDefaults synchronize];
}

-(NSString*)userPassword
{
    return [self.userDefaults objectForKey:@"userPassword"];
}

- (void)setUserPassword:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"userPassword"];
    [self.userDefaults synchronize];
}

-(NSString*)accessToken
{
    return [self.userDefaults objectForKey:@"accessToken"];
}

- (void)setAccessToken:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"accessToken"];
    [self.userDefaults synchronize];
}

-(NSString*)refreshToken
{
    return [self.userDefaults objectForKey:@"refreshToken"];
}

- (void)setRefreshToken:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"refreshToken"];
    [self.userDefaults synchronize];
}
-(NSString*)galleryTitle
{
    return [self.userDefaults objectForKey:@"galleryTitle"];
}

- (void)setGalleryTitle:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"galleryTitle"];
    [self.userDefaults synchronize];
}
-(NSString*)galleryDescription
{
    return [self.userDefaults objectForKey:@"galleryDescription"];
}

- (void)setGalleryDescription:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"galleryDescription"];
    [self.userDefaults synchronize];
}
-(NSString*)lastMessageDate
{
    return [self.userDefaults objectForKey:@"lastMessageDate"];
}

- (void)setLastMessageDate:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"lastMessageDate"];
    [self.userDefaults synchronize];
}

-(BOOL)isSavetoAlbum
{
    return [self.userDefaults boolForKey:@"isSavetoAlbum"];

}

-(void)setIsSavetoAlbum:(BOOL)isSavetoGallery
{
    //[self.userDefaults setObject:[NSNumber numberWithBool:isSavetoGallery] forKey:@"isSavetoAlbum"];
    [self.userDefaults setBool:isSavetoGallery forKey:@"isSavetoAlbum"];
    [self.userDefaults synchronize];
}


-(BOOL)isNotificationEnabled
{
    return [self.userDefaults boolForKey:@"isNotificationEnabled"];
    
}

-(void)setIsNotificationEnabled:(BOOL)value
{
    //[self.userDefaults setObject:[NSNumber numberWithBool:isSavetoGallery] forKey:@"isSavetoAlbum"];
    [self.userDefaults setBool:value forKey:@"isNotificationEnabled"];
    [self.userDefaults synchronize];
}

-(BOOL)isAccountPrivate
{
    return [self.userDefaults boolForKey:@"isAccountPrivate"];
    
}

-(void)setIsAccountPrivate:(BOOL)value
{
    //[self.userDefaults setObject:[NSNumber numberWithBool:isSavetoGallery] forKey:@"isSavetoAlbum"];
    [self.userDefaults setBool:value forKey:@"isAccountPrivate"];
    [self.userDefaults synchronize];
}

-(NSString*)lastActivityDate
{
    return [self.userDefaults objectForKey:@"lastActivityDate"];
}

- (void)setLastActivityDate:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"lastActivityDate"];
    [self.userDefaults synchronize];
}

-(NSString*)gcmRegKey
{
    return [self.userDefaults objectForKey:@"gcmRegKey"];
}

- (void)setGcmRegKey:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"gcmRegKey"];
    [self.userDefaults synchronize];
}


-(NSString*)deviceToken
{
    return [self.userDefaults objectForKey:@"deviceToken"];
}

- (void)setDeviceToken:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"deviceToken"];
    [self.userDefaults synchronize];
}

- (NSMutableDictionary *)toNSDictionary
{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    [dictionary setObject:@"flowdigital" forKey:@"access_key"];
    [dictionary setValue:[NSNumber numberWithInt:self.userId] forKey:@"user_id"];
    return dictionary;

}




@end
