////////////////////////////////////////////////////////////////////////////
//
// Copyright 2014 Realm Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface Posts : RLMObject
@property NSString *primId;
@property int postId;
@property NSData * imageData;

/*
@property NSString* userId;
@property NSString * postDescription;
@property NSString * hashTags;

@property float aspectRatio;

@property NSString * artistName;

@property BOOL isPublic;
@property BOOL isGallery;
@property BOOL islocked;


@property NSString * googlePlaceId;
@property NSString * address;
@property NSString * addressTitle;

@property NSString * longitude;
@property NSString * latitude;
*/

@end
